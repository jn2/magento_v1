<?php header("Content-type: text/css; charset: UTF-8"); ?>
/********GROUP HEADER**********/

header{
  background: #<?php echo $_GET['headerColor1']; ?>;
}


header .help-links a,
header .customer-links a,
header .cart-header{
  color:#<?php echo $_GET['headerColor2']; ?>;
}
header #search_mini_form .form-search{
border:1px solid #<?php echo $_GET['headerColor2']; ?>;
}
header #search_mini_form .form-search button{
border-left:1px solid #<?php echo $_GET['headerColor2']; ?>;
}
header #search_mini_form .form-search input[type="text"]{
  /*border:1px solid #<?php echo $_GET['headerColor2']; ?>;*/
}

/********GROUP MENU**********/
menu,
menu .nav-container,
menu .nav-container-mobile,
menu .mega-menu-jn2,
menu .sf-menu,
menu .sf-menu li,
menu .sf-menu li.active,
.nav-container, .mega-menu-jn2{
  background: #<?php echo $_GET['menuColor1']; ?> !important;
}

menu,
menu .nav-container,
menu .nav-container-mobile,
menu .mega-menu-jn2,
menu .sf-menu,
menu .sf-menu > li > a span,
menu .sf-menu > li.active > a span,
menu .sf-menu > li.parent > a:after,
.mega-menu-jn2 .content-mega-menu .inner-content ul li a,
.nav-container .content-mega-menu ul li a, .mega-menu-jn2 .content-mega-menu ul li a{
 color: #<?php echo $_GET['menuColor2']; ?>;
}

menu .sf-menu > li:hover{
  background: #<?php echo $_GET['menuColor2']; ?>;
}
menu .sf-menu > li:hover > a span,
menu .sf-menu > li.parent > a:after{
  color: #<?php echo $_GET['menuColor1']; ?>;
}

menu .mega-menu-jn2 .inner-content{
  border-color: #<?php echo $_GET['menuColor1']; ?>;
}

/********GROUP FOOTER**********/
footer {
  background: #<?php echo $_GET['footerColor1']; ?>;
  color: #<?php echo $_GET['footerColor2']; ?>;
}
footer h2,
footer ul li a {
  color: #<?php echo $_GET['footerColor2']; ?>;
}

button.btn-cart.button > span,
button.btn-checkout.button > span,
.button > span > span{
  background: #<?php echo $_GET['buyButton']; ?>!important;
}
.button > span > span{
  color: #<?php echo $_GET['buyButtonText']; ?>;
  border-color: #<?php echo $_GET['buyButton']; ?>;
}


button.btn-checkout {
  background: #<?php echo $_GET['buyButton']; ?>!important;
  color: #<?php echo $_GET['buyButtonText']; ?>;
}

/*.button > span > span:hover{
  border:none;
}*/

/********GROUP COLOR 1**********/

.aw-blog-read-more,
.grid-full > li > a:before,
.grid-full .level span.cat-label.hot,
.opc .step-title .number,
.opc .active .step-title,
.checkout-progress li.active,
.list-1 .title-1:before,
.label-product > span.sale,
.newsletter .icon-email,
.newsletter form .btn-send,
.button:hover > span > span
{
  background: #<?php echo $_GET['color1']; ?>;
}

.widget-latest li .all-blog:hover,
.widget-latest-title a,
.lof_camera_title,
.grid-full .level1 .catagory-level1,
a:hover,
a:focus,
.form-list label.required em,
.buttons-set p.required,
.fieldset .legend,
.pager .pages .current,
.header .welcome-msg,
.header .links li a:before,
.header-button ul li a:hover,
.block-layered-nav dd li,
#product_tabs_review_tabbed_contents #review-form h3 span,
.block-poll .votes,
#newsletterpopup .newsletter-slog span,
#newsletterpopup .close,
.products-list .product-name a:hover,
.products-list .desc > p strong,
#tabquickshowcontainer .special-price .price,
.box-collateral.box-tags .note,
.form-adv-search .fieldset h2,
.about-col-3 h3,
.about-col-3 p,
.about-col-6 p,
.about-col-7 p,
.custom-servis-ul li h3,
.item-options li em,
.gift-messages h3,
.gift-messages-form h4,
#checkout-step-login h3,
#checkout-step-login .col2-set > .col-1 h4,
#checkout-shipping-method-load .sp-methods dt,
#shipping_method_form h2,
.multiple-checkout .grand-total big,
.multiple-checkout h2,
.account-login .new-users h2,
.my-account .box .box-title h3,
.block-order-return label em,
.order-info-box h2,
.order-items h2,
.col-1.addresses-primary h3,
.contact-left h3,
.list-1 .title-2,
.regular-price .price,
.product-view .product-name
{
    color: #<?php echo $_GET['color1']; ?>;
}

#product_comparison .product-shop-row td .product-image:hover,

.newsletter form
{
  border:1px  solid #<?php echo $_GET['color1']; ?>;
}
.label-product > span.sale:after {
  border-color: #<?php echo $_GET['color1']; ?> transparent transparent transparent;
}
.label-product > span.sale:before {
  border-color: transparent transparent transparent #<?php echo $_GET['color1']; ?>;
}

/************GROUP COLOR 2*************/

/*BUSCA, MINICART, HOVER BTS, PRICE DESTQUE, BG NEWS, FOOTER LINES, BTS NAV LINKS TOP*/

.form-search button.button > span,
.cart .title-buttons .checkout-types li button.button span:hover,
.container-bg,
.lof_camera_slog:before,
.camera_caption .camera-link,
.footer h4:before,
.page-title:after, .product-view .product-name:after
{
  background:#<?php echo $_GET['color2']; ?>;
}
.products-grid .product-name a:hover,
.products-list .product-name a:hover,
.block-cart-header:before,
.price-parcelado .price,
.header .links li a:before,
.footer ul li a:hover{
  color: #<?php echo $_GET['color2']; ?>;
}



/************GROUP COLOR 3*************/


.page-sitemap .links a,
.cart .title-buttons .checkout-types li button.button span
 {
  background: #<?php echo $_GET['color3']; ?>;
}


 /*{
  color: #<?php echo $_GET['color3']; ?>
}*/


/************GROUP COLOR 4*************/

/*{
  color:#<?php echo $_GET['color4']; ?>
}*/

/************GROUP COLOR 5*************/


.page-sitemap .links a:hover,
.demo-notice{
  background: #<?php echo $_GET['color5']; ?>;
}
