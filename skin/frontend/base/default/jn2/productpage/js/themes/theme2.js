jQuery(document).ready(function($){


  if($(window).width() >= 992){

    if($('.theme2-product-wrapper').outerHeight() <= 450){

      $('.theme2-product-wrapper').scrollToFixed({
        marginTop: jQuery('header').outerHeight() ,
        //marginTop: 0,
        limit: function() {
            if($('.newsletter').length){
              return $('.newsletter').offset().top - $('.theme2-product-wrapper').outerHeight();
          }
          return $('footer').offset().top - $('.theme2-product-wrapper').outerHeight();
        },
          removeOffsets: true,
      });
    }
  }


});
