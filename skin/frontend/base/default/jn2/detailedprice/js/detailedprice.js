jQuery(document).ready(function ($) {
    jQuery('.price-box:has(.price-parcelado)').addClass('has_parcel');

    var detailedprice = {
        calcInstallmentValueCielo: function (total, interest, periods) {
            total = parseFloat(total);
            if (interest <= 0) {
                return (total / periods);
            }

            // calcula o coeficiente, seguindo a formula acima
            var coefficient = Math.pow((1 + interest), periods);
            coefficient = 1 / coefficient;
            coefficient = 1 - coefficient;
            coefficient = interest / coefficient;

            // retorna o valor da parcela
            return (total * coefficient);
        },
        calcInstallmentValuePagarme: function (total, interest, periods) {
            total = parseFloat(total);
            if (interest <= 0) {
                return (total / periods);
            }

            return (total + total * interest * periods) / periods;
        },
        calcBilletValue: function (total, discount) {
            total = parseFloat(total);
            discount = parseFloat(discount);
            return (total - ((total * discount) / 100)).formatMoney(2, ',', '.');
        },
        calcInstallment: function (total, interest, periods, noinstalments, installment_type) {
            var installment_value;
            periods = parseInt(periods);
            noinstalments = parseInt(noinstalments);
            if (periods > noinstalments) {
                if (installment_type === 'cielo' || !installment_type.length) {
                    installment_value = detailedprice.calcInstallmentValueCielo(total, interest / 100, periods);
                } else if (installment_type === 'pagarme') {
                    installment_value = detailedprice.calcInstallmentValuePagarme(total, interest / 100, periods);
                }
            } else {
                installment_value = total / periods;
            }

            return installment_value.formatMoney(2, ',', '.');
        },
        updateBillet: function () {
            $('.price-billet .price').each(function () {
                var billet_total = parseFloat($(this).parent().parent().find('.special-price .price, .regular-price .price').text().replace("R$", "").replace(".", "").replace(",", "."));
                var billet_discount = $(this).attr('data-billetdiscount');
                $(this).html('R$' + detailedprice.calcBilletValue(billet_total, billet_discount));
            });
        },
        updateInstallment: function () {
            $('.price-parcelado .price').each(function () {
                var parent = $(this).parent();
                var total = parseFloat(parent.parent().find('.special-price .price, .regular-price .price').text().replace("R$", "").replace(".", "").replace(",", "."));
                var interest = parseFloat(parent.attr('data-interest'));
                var periods = parseInt(parent.attr('data-periods'));
                var noinstalments = parseFloat(parent.attr('data-noinstalments'));
                var type = parent.attr('data-type');
                var min_installment_value = parseFloat(parent.attr('data-min-installment-value'));
                var max_periods = parseInt(parent.attr('data-max-periods'));

                max_periods = Math.max(1, max_periods);
                periods = Math.max(1, parseInt(total / min_installment_value));

                if (periods > max_periods) {
                    periods = max_periods;
                }

                parent.find('.periods').html(periods);
                $(this).html('R$' + detailedprice.calcInstallment(total, interest, periods, noinstalments, type));
            });
        },
        updateInstallmentList: function (option) {
            $('#installment-options-popup .installment-option, #installment-options-table .installment-option').each(function () {
                var option = $(this);
                var parent = option.closest('.installment-options');
                if (typeof $('#product-price-' + parent.attr('data-id') + ' .price').html() != 'undefined') {
                    var total = $('#product-price-' + parent.attr('data-id') + ' .price').html().replace("R$", "").replace(".", "").replace(",", ".");
                }
                var interest_value = parent.attr('data-interest-value').replace(',', '.');
                var installment = option.attr('data-installment');
                var max_installments = parent.attr('data-max-installments');
                var type = parent.attr('data-type');

                $(this).find('.installment-value').html('R$ ' + detailedprice.calcInstallment(total, interest_value, installment, max_installments, type));
            });
        }
    };

    Number.prototype.formatMoney = function (c, d, t) {
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };


    $('.product-options select, .product-options input').on('change', function () {
        detailedprice.updateBillet();
        detailedprice.updateInstallment();
        detailedprice.updateInstallmentList(this);
    }).change();

    $('.product-options li.swatchContainer img').on('click', function () {
        detailedprice.updateBillet();
        detailedprice.updateInstallment();
        detailedprice.updateInstallmentList(this);
    }).change();

});
