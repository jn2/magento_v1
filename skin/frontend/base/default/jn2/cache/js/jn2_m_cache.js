/**
 * Provides requestAnimationFrame in a cross browser way.
 * @author paulirish / http://paulirish.com/
 */

if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = (function () {
        return window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
                window.setTimeout(callback, 1000 / 60);
            };

    })();
}

(function ($) {
    var keyName = '_cache_blocks_';
    var blockWithCacheSelector = '[data-cache-bypass="1"]';
    var loadingClassMark = 'loading-cache';

    function CacheBypass() {
        this.init();
        this.clearLocalStorage();
    }

    CacheBypass.prototype = {
        constructor: CacheBypass,
        _data: null,
        _intervalDuration: 500,
        _replaceFormKeys: function (formKey) {
            //replace form_key inputs
            $('[name="form_key"]').val(formKey);
            //replace formkey on form action attrs
            var properties = ['action', 'href', 'onclick'];
            $.each(properties, function (idx, property) {
                var selector = '[#property#*="form_key/"]'.replace('#property#', property);
                $(selector).each(function () {
                    var value = $(this).attr(property);
                    value = value.replace(/(form_key\/)[^/]+/, "$1" + formKey);
                    $(this).attr(property, value);
                });
            });
        },
        _replaceBlocks: function (blocks) {
            $.each(blocks, function (key, value) {
                var aliasSelector = '[data-block-alias="#{block-alias}"]'.replace('#{block-alias}', key);
                var selector = [blockWithCacheSelector, aliasSelector].join('');
                var $block = $(selector);
                requestAnimationFrame(function () {
                    var $value = $(value);
                    $value.removeClass(loadingClassMark);
                    $block.replaceWith($value);
                });
            });
        },
        doReplacing: function () {
            var data = this._data;
            if (!$.isPlainObject(data)) {
                return;
            }
            //formkeys
            try {
                this._replaceFormKeys(data['form_key']);
            } catch (e) {

            }
            //replace each blocks
            try {
                this._replaceBlocks(data[keyName]);
            } catch (e) {
            }

        },
        clearLocalStorage: function () {
            //every thing that can change some info on cache
            $(document).on('click', 'a, .add-to-cart, button, .add-to-cart-buttons, #onestepcheckout-place-order', function (event) {
                $target = $(event.currentTarget);

                //if click on button inside a cache
                if ($target.is('div')) {
                    sessionStorage.removeItem('jn2_cache');
                }

                //conditions
                if (($target.attr('href') && ($target.attr('href').includes('form_key') || $target.attr('href').includes('logout'))) ||
                    ($target.attr('onclick') && ($target.attr('onclick').includes('form_key') || $target.attr('onclick').includes('submit'))) ||
                    ($target.attr('type') && $target.attr('type') == 'submit') &&
                    $target.attr('id') != 'button-search' ||
                    ($target.attr('class') == 'pslogin-button-link')
                ) {
                    sessionStorage.removeItem('jn2_cache');
                }
            });
        },
        init: function () {
            var self = this;
            $(blockWithCacheSelector).addClass(loadingClassMark);
            var timer;//used to repeat when cache/page not fully loaded

            //disable form-clicks
            var fnBlockSubmit = function (evt) {
                evt.preventDefault();
                evt.stopImmediatePropagation();
            };
            var fnOnError = function () {
                clearInterval(timer);
            };

            //block submit on all forms
            timer = setInterval(function () {
                $('form').off('submit', fnBlockSubmit).on('submit', fnBlockSubmit);
            }, self._intervalDuration);

            if (sessionStorage.getItem("jn2_cache") === null) {
                $.ajax(document.location.href, {
                    'type': 'GET',
                    'data': {"cache-get": '_cache_blocks_'},
                    'success': function (data, status, xhr) {
                        self._data = data;

                        if(data.hasOwnProperty('_cache_blocks_')) {
                            sessionStorage.setItem('jn2_cache', JSON.stringify(data));
                        }

                        $(document).trigger('cache:loaded', {data: data});
                        self.doReplacing();

                        //unblock form submits && stop interval
                        clearInterval(timer);
                        $('form').off('submit', fnBlockSubmit);

                        $(window).off('error', fnOnError).on('error', fnOnError);

                        var fnReplaceWithAnimation = function () {
                            requestAnimationFrame(function () {
                                self.doReplacing();
                            });
                        };
                        //do replacing when page not fully loaded
                        timer = setInterval(fnReplaceWithAnimation, self._intervalDuration);
                        //                    setTimeout(fnReplaceWithAnimation, 150);

                        //page fully loaded
                        $(function () {
                            self.doReplacing();//dom loaded
                            requestAnimationFrame(function () {
                                $(document).trigger('cache:finish', {data: data});
                            });
                            clearInterval(timer);
                        });
                    },
                    'error': function () {
                        clearInterval(timer);
                        $(document).trigger('cache:error');
                        fnOnError();
                    }
                });
            } else {
                self._data = JSON.parse(sessionStorage.getItem('jn2_cache'));

                $(document).trigger('cache:loaded', {data: self._data});
                self.doReplacing();

                //unblock form submits && stop interval
                clearInterval(timer);
                $('form').off('submit', fnBlockSubmit);

                $(window).off('error', fnOnError).on('error', fnOnError);

                var fnReplaceWithAnimation = function () {
                    requestAnimationFrame(function () {
                        self.doReplacing();
                    });
                };
                //do replacing when page not fully loaded
                timer = setInterval(fnReplaceWithAnimation, self._intervalDuration);

                //page fully loaded
                $(function () {
                    self.doReplacing();//dom loaded
                    requestAnimationFrame(function () {
                        $(document).trigger('cache:finish', {data: self._data});
                    });
                    clearInterval(timer);
                });

            }
        }
    };
    window.Jn2 = window.Jn2 || {};
    window.Jn2.CacheBypass = window.cacheBypass = new CacheBypass();
})(jQuery);
