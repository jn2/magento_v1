Validation.add('validate-fullname', 'Por favor informe o nome completo.', function (value) {
    return value.trim().split(' ').filter(function(v){return v.length >= 2}).length >= 2;
});
Validation.add('validate-razaosocial', 'Por favor informe a razão social.', function (value) {
    // Se for pessoa jurídica e tiver menos que 4 caracteres, faz validação
    return !($$('input:checked[type="radio"][name="radio_tipopessoa"]').pluck('value')[0] == 10
           && $('company').getValue().length <= 3);
});
