jQuery(function(){
  if (window.screen.width > 760){
    jQuery('.image_container').hover(function(){
        var hover_image = jQuery(this).find('.hover');
        hover_image.attr('data-src') && hover_image.attr('src', hover_image.attr('data-src')).removeAttr('data-src');
    });
  }
});
