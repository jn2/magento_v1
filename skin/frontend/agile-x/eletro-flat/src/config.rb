#
# JN2
# @package: Jn2 Clean Theme
# @author: Igor Gottschalg <igorgottsweb@gmail.com>
#

require 'compass/import-once/activate'
require 'sass-globbing'

http_path = "/"
css_dir = "../css"
sass_dir = "sass"
additional_import_paths = "../../default/src/sass/"

output_style = :compressed
line_comments = false
sourcemap = true
relative_assets = true
