/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Footer Module
*/


if(typeof magento == "undefined"){
  magento = {
    modules: {}
  }
}

magento.modules.register = function(){
  (function($){

    var customer_form = $("#customer-wizard");

    $(".account-create").removeClass("loading");

    $("#check-pessoa-juridica").removeAttr("class");
    $(".show-pessoa-juridica").hide();
    $(".show-pessoa-fisica").show();

    $(".tipopessoa").on("click", "label", function(){
      var $_tipo = $(".tipopessoa").find("input:checked");
      $(".tipopessoa").find(".checked").removeClass("checked");
      $(this).addClass("checked");
      if( $_tipo.val() == 9 ){
        $(".show-pessoa-juridica").hide();
        $(".show-pessoa-fisica").show();
      }else{
        $(".show-pessoa-fisica").hide();
        $(".show-pessoa-juridica").show();
      }
    });

    $("#region").removeAttr("class");

    $("input.required-entry, select.required-entry, input.validate-one-required-by-name").blur(function(){
      if( $(this).val() == "" ){
        $(this).closest(".form-group").removeClass(["has-success","has-danger"]).addClass("has-danger");
      }else{
        $(this).closest(".form-group").removeClass(["has-success","has-danger"]).addClass("has-success");
      }

      var step = $(this).closest("fieldset");
      var requireds = step.find("input.required-entry, select.required-entry, input.validate-one-required-by-name");

      var $emptyFields = requireds.filter(function(){
        return $.trim(this.value) == "";
      });

      if (!$emptyFields.length) {
        step.prev().addClass("ok");
      }else{
        step.prev().removeClass("ok");
      }

    });

    $("#btnBuscaCep").click(function(){
      $('#buscaCep').appendTo("body").modal('toggle');
    })

    if($("#postcode").val() == ""){
      $(".show-after-cep").hide();
    }else{
      $(".show-after-cep").show();
    }

    $("#postcode").mask("99999-999");
    $("#postcode").keyup(function(){
        var val = $(this).val();
        if(val.indexOf("_") == -1 ){
          buscaCep('');
          $(".show-after-cep").show();
        }else{
          $(".show-after-cep").hide();
        }
    });

  }(jQuery))
};
