/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Header
*/


if(typeof magento == "undefined"){
  magento = {
    modules: {}
  }
}

magento.modules.header = function(){
  (function($){

    if(magento.mobile()){
      $("#btn-search-mobile").on("click", function(){
        $('.search-content').slideToggle();
      });
    }

  })(jQuery);
};
