/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Footer Module
*/


if(typeof magento == "undefined"){
  magento = {
    modules: {}
  }
}

magento.modules.footer = function(){
  (function($){
    if(!magento.mobile || $(window).width() >= 992){
      $("footer .footer-row").each(function(index, row){
        var columnHeight = 0;
        $(row).find(".column").each(function(index, column){
          if ($(column).height() > columnHeight) {
            columnHeight = $(column).height();
          }
        }).height(columnHeight);
      });
    }
    $("#back-top").click(function(){
      $("html, body").animate({ scrollTop: "0px" });
    })


    $(".footer-menu-mobile").on("click", ".colapse", function(){
      $(this).parent().find(".sub-menu").first().slideToggle();
      $(this).text($(this).text() == "+" ? "-" : "+");
    });


  }(jQuery))
};
