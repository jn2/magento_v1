/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Products Grid Module
*/


if(typeof magento == "undefined"){
  magento = {
    framework: {},
    modules: {}
  }
}

magento.modules.category_view = function(){
  (function($){
    if ($('body').hasClass('catalog-category-view') || $('body').hasClass('catalogsearch-result-index')) {

      if($(".page").find(".category-image img").length > 0){
        $(".breadcrumbs").css({"position": "absolute"})
      }

      if(magento.mobile()){
        $(".sidebar").height( $(document).height() );
        if($(".mobile-filter").offset()){
          $(".sidebar").css({
            "top": $(".mobile-filter").offset().top + $(".mobile-filter").height()+"px"
          });
        }
      }

      $("#btn-filter, #btn-close-filter").on("click", function(){
        $(".sidebar").toggleClass("show");
      });

      var session = localStorage.getItem('jn2_filter_session');
      var $_filter = session ? JSON.parse(session) : null;

      if($_filter != null){

        var session_expiry = new Date($_filter.expery_at);
        var now = new Date();
        if($_filter != null && ( session_expiry >= now)){
          $.each($_filter.filters,function(index, filter){
            if (filter.open) {
              $_collapse = $("dt." + filter.name).find(".collapse");
              $_collapse.text($_collapse.text() == "+" ? "-" : "+");
              $(filter.content).addClass("open");
            }
          });
        }

      }else if( $_filter == null || ($_filter.expery_at > new Date()) ){
        $_filter = {
          filters: [],
          expery_at: new Date(new Date().getTime() + 60 * 60 * 24 * 1000)
        };

        $(".sidebar").find("dt").each(function(index, element){
          var filter_name = $(element).attr("class").replace("label ","").replace("colorselectorplus", "").trim();
          var this_filter = {
            name: filter_name,
            content: "dd." + filter_name,
            open: true
          }
          $_filter.filters.push(this_filter);
        });

        $.each($_filter.filters,function(index, filter){
          if (filter.open) {
            $_collapse = $("dt." + filter.name).find(".collapse");
            $_collapse.text($_collapse.text() == "+" ? "-" : "+");
            $(filter.content).addClass("open");
          }
        });
        localStorage.setItem('jn2_filter_session', JSON.stringify($_filter));
      }

      $("#narrow-by-list dt").on("click", function(){
        var $_filterName = $(this).attr("class").replace("label ","").replace("colorselectorplus", "").trim();
        if($_filter != null){
          $.each($_filter.filters, function(index, filter){
            if(filter.name == $_filterName){
              $_collapse = $("dt." + filter.name).find(".collapse");
              $_collapse.text($_collapse.text() == "+" ? "-" : "+");
              filter.open = !filter.open;
              $(filter.content).slideToggle().toggleClass("open");
              localStorage.setItem('jn2_filter_session', JSON.stringify($_filter));
              console.log($_filter);
            }
          });
        }
      });
    }
  }(jQuery));
}
