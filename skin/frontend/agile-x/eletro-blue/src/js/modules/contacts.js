/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Products Grid Module
*/


if(typeof magento == "undefined"){
  magento = {
    framework: {},
    modules: {}
  }
}

magento.modules.contacts = function(){
  (function($){
    if ($('body').hasClass('contacts-index-index')) {
      if(!magento.mobile()){
        $("#comment").height($(".same-height-default").height()-46);
      }
    }
  }(jQuery));
}
