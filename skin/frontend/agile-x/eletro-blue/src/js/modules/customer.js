/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Products Grid Module
*/


if(typeof magento == "undefined"){
  magento = {
    framework: {},
    modules: {}
  }
}

magento.modules.customer = function(){
  (function($){

    if ($('body').hasClass('customer-account')) {

      if(magento.mobile()){
        $(".sidebar .block-title").click(function(){
          $(".sidebar").toggleClass("open");
        });
      }

    }

    }(jQuery));
  }
