/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Products Grid Module
*/


if(typeof magento == "undefined"){
  magento = {
    modules: {}
  }
}

magento.modules.login = function(){
  (function($){
    if(!magento.mobile()){
      if($(".account-login").length > -1){
        var max_height = 0;
        $(".account-login .frame").each(function(index, element){
          if($(element).height() > max_height){
            max_height = $(element).height();
          }
        }).height(max_height);
      }
    }

  })(jQuery);
};
