/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Products Page
*/


magento.modules.product_page=function(){

  (function($){

    if($("body").hasClass("catalog-product-view")){


      $("body").append('<div class="nicemodal fade" id="option-modal"><div class="body"></div></div>');

      $(document).on("click",".nicemodal", function(){
        $("#option-modal").nicemodal('close');
      });

      $(document).on("click", ".qty-wrapper-input .btn-minus", function(){
        if($("#qty").val() == 1){
          return false;
        }else{
          $("#qty").val(parseInt($("#qty").val())-1)
        }
      });

      $(document).on("click", ".qty-wrapper-input .btn-plus", function(){
        $("#qty").val(parseInt($("#qty").val())+1);
      });

      $('.floating-addtocart').scrollToFixed({
        marginTop: $('header').outerHeight() ,
        //marginTop: 0,
        limit: function() {
          if($('.reviews').length){
            return $('.reviews').offset().top - $('.floating-addtocart').outerHeight();
          }
          return $('footer').offset().top - $('.floating-addtocart').outerHeight();
        },
        removeOffsets: true,
      });

      if($("body").hasClass("catalog-product-view")){
        var carousel = $(".media .product-image").owlCarousel({
          loop: true,
          nav: true,
          dots: true,
          singleItem: true,
          items: 1,
          lazyLoad: true,
          navText: ["&nbsp;","&nbsp;"],
          onInitialized: function(){
            setTimeout(function(){
              $(".media").removeClass("loading");
            },100);
          },
          onChanged: function () {
            if (window.innerWidth >= screensize.md) {
              setTimeout(function () {

                $('.easyzoom').easyZoom({
                  linkAttribute: 'data-zoom-image'
                });
              }, 100);
            }
          }
        });

        $(".more-views img").click(function(){
          carousel.trigger("to.owl.carousel", [$(this).index(), 500, true]);
        });
      }

      var $_related_products_settings_carousel = {
        loop: false,
        nav: true,
        navText: ["&nbsp;","&nbsp;"],
        dots: true,
        singleItem: false,
        items: 3,
        margin: 30,
        responsive : {
          0: { items: 2 },
          480: { items: 2 },
          768: { items: 3 },
          992: { items: 3 },
          1200: { items: 3 }
        },
        autoPlay: true,
        lazyLoad: true,
        onInitialized: function(){
          $(document).trigger("owl-carousel-initialized", [$($(this)[0].$element[0]).attr("id")]);
          product_grid.calculate("#"+$($(this)[0].$element[0]).attr("id"));
          setTimeout(function(){
            $('#related-products').find(".checkbox").css({"position": "absolute"})
          },100);
        }
      }
      if(magento.mobile()){
        var $_related_products_settings_carousel_mobile = $_related_products_settings_carousel;
        $_related_products_settings_carousel_mobile.responsive = {
          0: { items: 1 },
          480: { items: 1 },
          768: { items: 3 },
          992: { items: 3 },
          1200: { items: 3 }
        };
        $('#related-products-mobile').owlCarousel($_related_products_settings_carousel_mobile);
        // setTimeout(function(){
        //   $("#collapseListGroupHeading4 a").trigger("click");
        // },100);
      }else{
        $('#related-products').owlCarousel($_related_products_settings_carousel);
      }

      var $_upsell_settings_carousel = {
        loop: false,
        nav: true,
        navText: ["&nbsp;","&nbsp;"],
        dots: true,
        singleItem: false,
        items: 3,
        margin: 30,
        responsive : {
          0: { items: 2 },
          480: { items: 2 },
          768: { items: 3 },
          992: { items: 3 },
          1200: { items: 3 }
        },
        autoPlay: true,
        lazyLoad: true,
        onInitialized: function(){
          $(document).trigger("owl-carousel-initialized", [$($(this)[0].$element[0]).attr("id")]);
          product_grid.calculate("#"+$($(this)[0].$element[0]).attr("id"));
        }
      }
      if(magento.mobile()){
        var $_upsell_settings_carousel = $_upsell_settings_carousel;
        $_upsell_settings_carousel.responsive = {
          0: { items: 1 },
          480: { items: 1 },
          768: { items: 3 },
          992: { items: 3 },
          1200: { items: 3 }
        };
      
        $('#up-sell-mobile').owlCarousel($_upsell_settings_carousel_mobile);
        // setTimeout(function(){
        //   $("#collapseListGroupHeading5 a").trigger("click");
        // },100);
      }else{
        $('#up-sell').owlCarousel($_upsell_settings_carousel);
      }



      if(!magento.mobile()){

        var $_floating_section = $(".same-height-default");
        $(".same-height").height($_floating_section).parent().height($_floating_section);
        $(".btn-floating-addtocart").on("click", function(){
          $("#btn-addtocart").trigger("click");
        })

      }



      /******* Validation ********/
      $(document).on("click","#btn-floating-addtocart, #btn-addtocart, #btn-calc-frete", function(){

        var html = getEmptyOptions().map(function(){
          var optionBlock = jQuery("#ul-" + this.id);
          if(optionBlock.length){
            var textBlock   = jQuery("#div" + this.id).parent().find("label.required").clone().children().remove().end().text();
            return '<strong>'+textBlock + '</strong><div class="clearfix"><ul>' + optionBlock.html() + '</ul></div>';
          }else{
            var textBlock  = jQuery(this).parent().parent().prev("dt").find("label.required").clone().children().remove().end().text();
            return '<strong>'+textBlock + '</strong><div class="clearfix"><select onchange="attribute_select('+this.id+', this.value)">' + $(this).html() + '</select></div>';
          }
        });

        if(html.length > 0){
          html = Array.prototype.slice.call(html).join('');
          $("#option-modal .body").html(html);
          $("#option-modal").nicemodal('open');
          if($(this).hasClass("btn-addtocart")){
            $("#option-modal").attr("data-type", "cart");
          }else{
            $("#option-modal").attr("data-type", "estimate");
          }
        }
      });
      $("#option-modal").on("click", "li", checkOptions);
      $("#option-modal").on("change", "select", checkOptions);

      function attribute_select(id, value){
        jQuery(id).val(value);
      }



      /********** Declare Vars **************/
      function getEmptyOptions(){
        return jQuery('[name^="super_attribute"]').filter(function(){
          return !this.value;
        });
      }

      /************ Option click on modal *********************/
      function checkOptions(){
        eval($(this).attr("onclick"));
        if(getEmptyOptions().length == 0){
          $("#option-modal").nicemodal('close', function(){
            if($("#option-modal").attr("data-type") == "cart"){
              productAddToCartForm.submit();
            }else{
              estimateProductShipping();
            }
          });
        }
        if($(this).closest("a").attr("href")!= ""){
          return false;
        }
      }

    }

  }(jQuery));
}
