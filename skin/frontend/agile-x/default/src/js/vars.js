/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Jn2 Application
*/

if(typeof magento == "undefined"){
  magento = {
    framework: {},
    modules: {}
  }
}
