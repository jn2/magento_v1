/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Products Grid Module
*/


if(typeof magento == "undefined"){
  magento = {
    modules: {}
  }
}

var product_grid = {
  responsiveItems: function(){
    if(jQuery(window).width() < screensize.sm){
      return 2;      // Versão Mobile
    }else if(jQuery(window).width() >= screensize.sm && jQuery(window).width() < screensize.md){
      return 3;      // Versão Tablet
    }else if(jQuery(window).width() >= screensize.md){
      if(jQuery("body").hasClass("catalog-category-view") || jQuery("body").hasClass("catalogsearch-result-index")){
        return 4;      // Versão Desktop
      }else{
        if(jQuery(window).width() >= screensize.md && jQuery(window).width() < screensize.lg){
          return 4;      // Versão Desktop
        }else{
          return 5;
        }
      }
    }
    return false;
  },
  elementForEqualHeight: ".item",
  init: function(){
    (function($){
      if($(".products-grid") && $(".products-grid").length > 0){
        $(".products-grid").each(function(index, grid){
          if(!$(grid).hasClass("owl-carousel")){
            product_grid.calculate(grid);
          }else{
            // Ajustes da altura dos produtos quando o slide estiver pronto
            $(grid).on('initialized.owl.carousel', function(){
              product_grid.calculate(this);
            });
          }
        });
      }
    }(jQuery));
  },
  calculate: function(grid){
    (function($){
      // Caso o grid não exista no DOM a função será encerrada
      if(!$(grid) && !$(grid).find(product_grid.elementForEqualHeight)){
        return;
      }

      if(!$(grid).hasClass("owl-carousel")){
        var originalGrid = $(grid).find(product_grid.elementForEqualHeight);  // Grid com os itens originais
        var tempGrid = Array(); // Grid temporário
        var items = product_grid.responsiveItems();
        var init = 0;
        var end = items;

        // Divide os itens em sessões por tamanho de itens responsivo
        for(i=0 ; i < originalGrid.length / items; i++){
          tempGrid.push(originalGrid.slice(init,end));
          init += items;
          end += items;
        }

        // Percorre todo as sessoes do grid
        $.each(tempGrid, function(index,section){
          // Reseta o valor para o valor máximo
          var max_height = 0;
          // Percorre todos os produtos da sessao
          var count = 0;
          $(section).each(function(index, product){
            // Caso o valor da altura do produto seja maior que o valor maximo anterior, ele será setado como o maior
            if($(product).height() > max_height){
              max_height = $(product).height() + $(product).find(".text-hide-price").height() + 50;
            }
            count++;
            // if(count == $(section).length ){
            //   $(product).after('<div class="clearfix"></div>');
            // }
          }).height(max_height);

        });
      }else{
        var max_height = 0; // Reseta o valor para o valor máximo
        $(grid).find(product_grid.elementForEqualHeight).each(function(index, product){
          // Caso o valor da altura do produto seja maior que o valor maximo anterior, ele será setado como o maior
          if($(product).height() > max_height){
            max_height = $(product).height() + 50;
          }
        }).height(max_height);
      }

    }(jQuery));
  }
}

magento.modules.products_grid = function(){
  (function($){
    // Ajustes da altura dos produtos
    product_grid.init();

    // Quando a janela for redimencionada a altura será ajustada novamente
    // $(window).on("resize", function(){
    //   product_grid.init();
    // });

  }(jQuery));
};
