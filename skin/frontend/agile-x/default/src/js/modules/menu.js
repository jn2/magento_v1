/**
 * JN2
 * @package: Jn2 Default Theme
 * @author: Igor Gottschalg <igorgottsweb@gmail.com>
 * @class: Products Grid Module
 */


if (typeof magento == "undefined") {
    magento = {
        modules: {}
    }
}

magento.modules.menu = function () {
    (function ($) {

        // Verifica se o mouse ficou mais que 200ms em cima do menu
        var _in = function () {

            var dis = this;

            // Cria as ações do timer
            var t = setTimeout(function () {
                $(".layered").show().siblings('.layered').hide();
                $(dis).addClass("open");
            }, 200);

            // Inicia o timer para o element
            $(this).data("timer", t);

        }

        // Remover o timer e a classe open no menu
        var _ot = function () {

            // Limpa o timer do elemento
            if ($(this).data("timer")) {
                clearInterval($(this).data("timer"));
                $(this).removeData("timer");
                $(".layered").hide().siblings('.layered').show();
            }

            //Remover a classe open
            $(this).removeClass("open");
        }

        // Habilita função hober do menu
        $('.content-mega-menu > ul > li.with-subcategories').hover(_in, _ot);


        // Adiciona o fundo preto quando menu está aberto
        $(".wrapper").append('<div class="layered">&nbsp;</div>');


        // Iguala o tamanho da imagem da categoria ao tamanho do menu
        $(".category-img-display").height($(".category-img-display").parent().height() + 70);


        // if(magento.mobile()){
        //   alert('entrou aqui');
        // Abre o menu mobile
        $(document).ready(function () {
            $(document).on("click", "#btn-menu",function(){
                $("#menu-mobile").toggleClass("open");
                $(".wrapper").toggleClass("open-menu");
                $("#btn-menu").toggleClass("open");
            });
        });

        // $(".wrapper").on("click", function(){
        //   $("#menu-mobile").removeClass("open");
        //   $(".wrapper").removeClass("open-menu");
        //   $("#btn-menu").removeClass("open");
        // });


        // Analiza o swipe na tela
        // $("body").swipe({
        //
        //   // Abre o menu mobile
        //   swipeRight: function(event, direction, distance, duration, fingerCount) {
        //     $("#menu-mobile").addClass("open");
        //     $(".wrapper").addClass("open-menu");
        //     $("#btn-menu").addClass("open");
        //   },
        //
        //   // Fecha o menu mobile
        //   swipeLeft:function(event, direction, distance, duration, fingerCount) {
        //     $("#menu-mobile").removeClass("open");
        //     $(".wrapper").removeClass("open-menu");
        //     $("#btn-menu").removeClass("open");
        //   },
        //   allowPageScroll:"vertical",
        //   threshold:200,
        //   excludedElements:$.fn.swipe.defaults.excludedElements+", .owl-carousel"
        // });

        // }

    })(jQuery);
};
