/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Header
*/


if(typeof magento == "undefined"){
  magento = {
    modules: {}
  }
}

magento.modules.header = function(){
  (function($){

    if(magento.mobile()){
      jQuery(document).ready(function($){
        $(document).on('click', ".ico-busca", function() {
          $('.search-content').slideToggle();
        });       
      });   
    }

  })(jQuery);
};
