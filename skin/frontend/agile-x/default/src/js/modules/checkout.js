/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Products Grid Module
*/


if(typeof magento == "undefined"){
  magento = {
    framework: {},
    modules: {}
  }
}

magento.modules.checkout = function(){
  (function($){

    if ($('body').hasClass('onestepcheckout-index-index')) {

      if(magento.mobile()){

        $(".btn-step-1").click(function (e) {
          $('html,body').animate({scrollTop: $("#step2").offset().top},'slow');
        });
        $(".btn-step-2").click(function (e) {
          $('html,body').animate({scrollTop: $("#step3").offset().top},'slow');
        });

        $(".bt-prev-2").click(function (e) {
          $('html,body').animate({scrollTop: $("#step1").offset().top},'slow');
        });

        $(".bt-prev-3").click(function (e) {
          $('html,body').animate({scrollTop: $("#step2").offset().top},'slow');
        });

      }

    }

    }(jQuery));
  }

if (jQuery('#telephone').hasClass('remove-mask')
    || jQuery('#fax').hasClass('remove-mask')
    || jQuery('#shipping\\:telephone').hasClass('remove-mask')) {

    jQuery('#telephone, #fax, #shipping\\:telephone').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    jQuery("#telephone, #fax, #shipping\\:telephone").not(".international-fields").on('focusout', (function (event) {
        if (jQuery(this).val() == '') return;
        jQuery(this).val(jQuery(this).val().replace(/\D/g, ''));
        primeiroNum = jQuery(this).val().substring(0, 1);
        if (primeiroNum != 0) {
            jQuery(this).val('0' + jQuery(this).val());
        }
        if (jQuery(this).val().length == 12) {
            jQuery(this).val(jQuery(this).val().substring(0, 8) + '-' + jQuery(this).val().substring(8));
        } else {
            jQuery(this).val(jQuery(this).val().substring(0, 7) + '-' + jQuery(this).val().substring(7));
        }
        jQuery(this).val('(' + jQuery(this).val().substring(0, 3) + ') ' + jQuery(this).val().substring(3));
    }));

    jQuery(document).ready(function () {
        jQuery("#telephone, #fax, #shipping\\:telephone").focusout();
    });

    jQuery(document).on('focusout', '.validate-on-focusout', function (event) {
        window.focusoutValidation = true;
        Validation.validate(event.target);
        window.focusoutValidation = false;
    });
}