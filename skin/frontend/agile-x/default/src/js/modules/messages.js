/**
 * JN2
 * @package: Jn2 Default Theme
 * @author: Igor Gottschalg <igorgottsweb@gmail.com>
 * @class: Products Grid Module
 */


if (typeof magento == "undefined") {
    magento = {
        modules: {}
    }
}

magento.modules.messages = function() {
    (function($) {

        $(".messages > li").append('<div class="btn-close">&times</div>');
        $(".messages").on("click", ".btn-close", function() {
            $(".messages").addClass("close").delay(1000).remove();
        });

        setTimeout(function() {
            $(".messages").addClass("close").delay(1000).remove();
        }, 50000);

    })(jQuery);
};