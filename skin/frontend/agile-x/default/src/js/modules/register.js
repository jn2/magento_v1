/**
 * JN2
 * @package: Jn2 Default Theme
 * @author: Igor Gottschalg <igorgottsweb@gmail.com>
 * @class: Footer Module
 */


if (typeof magento == "undefined") {
  magento = {
    modules: {}
  }
}

magento.modules.register = function() {
  (function($) {

    var customer_form = $("#customer-wizard");

    $(".account-create").removeClass("loading");

    $("#check-pessoa-juridica").removeAttr("class");
    $(".show-pessoa-juridica").hide();
    $(".show-pessoa-fisica").show();

    $(".tipopessoa").on("click", "label", function() {
      var $_tipo = $(".tipopessoa").find("input:checked");
      $(".tipopessoa").find(".checked").removeClass("checked");
      $(this).addClass("checked");
      if ($_tipo.val() == 9) { //pessoal fisica
        $(".show-pessoa-juridica").hide();
        $(".show-pessoa-fisica").show();

        if ($('#ie').attr('data-show-rg') == '0') {
          $('#ie').closest('.form-group').hide();
        }
      } else { // pessoa juridica
        $(".show-pessoa-fisica").hide();
        $(".show-pessoa-juridica").show();
        $('#ie').closest('.form-group').show();
      }
    });

    $("#region").removeAttr("class");

    $("input.required-entry, select.required-entry, input.validate-one-required-by-name").blur(function() {
      if ($(this).val() == "") {
        $(this).closest(".form-group").removeClass(["has-success", "has-danger"]).addClass("has-danger");
      } else {
        $(this).closest(".form-group").removeClass(["has-success", "has-danger"]).addClass("has-success");
      }

      var step = $(this).closest("fieldset");
      var requireds = step.find("input.required-entry, select.required-entry, input.validate-one-required-by-name");

      var $emptyFields = requireds.filter(function() {
        return $.trim(this.value) == "";
      });

      if (!$emptyFields.length) {
        step.prev().addClass("ok");
      } else {
        step.prev().removeClass("ok");
      }

    });

    $("#btnBuscaCep").click(function() {
      $('#buscaCep').appendTo("body").modal('toggle');
    })

    if ($("#postcode").val() == "") {
      $(".show-after-cep").hide();
    } else {
      $(".show-after-cep").show();
    }

    //$("#postcode").mask("99999-999");
    $("#postcode").not('.remove-mask').keyup(function() {
      var val = $(this).val();
      if (val.indexOf("_") == -1) {
        buscaCep('');
        $(".show-after-cep").show();
      } else {
        $(".show-after-cep").hide();
      }
    });

      /*var SPMaskBehavior = function (val) {
              return val.replace(/\D/g, '').length === 12 ? '(000) 00000-0000' : '(000) 0000-00009';
          },
          spOptions = {
              onKeyPress: function(val, e, field, options) {
                  field.mask(SPMaskBehavior.apply({}, arguments), options);
              }
          };

      $("#telephone, #fax, #shipping\\:telephone").mask(SPMaskBehavior, spOptions);*/


      // $('.crazy_cep').mask('00000-000', options);

    $("#telephone, #fax, #shipping\\:telephone").not('.remove-mask')
    .masked("(099) 9999-9999?9")
    .focusout(function(event) {
        var phone, element;
        element = $(this);
        element.unmasked();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 11) {
              element.masked("(099) 99999-999?9");
            } else {
              element.masked("(099) 9999-9999?9");
            }
      }).trigger('focusout');


      $("#postcode, #shipping\\:postcode, #billing\\:postcode").not('.international-fields').mask("00000-000");
  }(jQuery))
};