/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Jn2 Application
*/

if(typeof magento == "undefined"){
  magento = {
    framework: {},
    modules: {}
  }
}

jQuery(document).ready(function($){

  // Carregar todos os módulos do Framework
  $.each(magento.framework, function (index, module) {
    if(typeof module == "function"){
      module.call();
    }
  });

  // Aguardar carregamento completo da página
  $(window).load(function () {
    // Carregar todos os módulos
    $.each(magento.modules, function (index, module) {
      if(typeof module == "function"){
        module.call();
      }
    });

  });

  var chkReadyState = setInterval(function() {
    if (document.readyState == "complete") {
      clearInterval(chkReadyState);
      jQuery(".pace").hide();
    }
  }, 100);

});
