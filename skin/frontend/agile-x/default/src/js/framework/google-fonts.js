/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Google Fonts Load
*/

if(typeof magento == "undefined"){
  magento = {
    framework: {}
  }
}
magento.framework.googleFonts = function(){
  //Carregando Google Web Fonts
  jQuery(window).load(function(){

    WebFontConfig = {
      google: {
        families: fonts
      },
      timeout: 2000
    };

    (function (d) {
      var wf = d.createElement('script'), s = d.scripts[0];
      wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
      wf.async = true;
      s.parentNode.insertBefore(wf, s);
    })(document);

  });
}
