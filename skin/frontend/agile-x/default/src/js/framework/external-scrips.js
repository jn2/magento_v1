/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: External script load
*/

if(typeof magento == "undefined"){
  magento = {
    framework: {}
  }
}
magento.framework.externalScripts = function(){
  (function($){
    //Carregando scripts externos
    jQuery(window).load(function(){
      jQuery.each(external_script, function(index, script){
        (function (d) {
          var wf = d.createElement('script'), s = d.scripts[0];
          wf.src = script;
          wf.async = true;
          s.parentNode.insertBefore(wf, s);
        })(document);
      });
    });

  })(jQuery);
};
