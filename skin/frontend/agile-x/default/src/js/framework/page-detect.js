/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Jn2 Page Detect
*/

if(typeof magento == "undefined"){
  magento = {
    framework: {}
  }
}

magento.getPage = {
  isHome: function(){
    return jQuery("body").hasClass("cms-index-index");
  },
  isCategory: function(){
    return jQuery("body").hasClass("catalog-category-view");
  },
  isProduct: function(){
    return jQuery("body").hasClass("catalog-product-view");
  },
  isCart: function(){
    return jQuery("body").hasClass("checkout-cart-index");
  },
  isCheckout: function(){
    return jQuery("body").hasClass("onestepcheckout-index-index");
  },
  isLogin: function(){
    return jQuery("body").hasClass("customer-account-login");
  }
}
