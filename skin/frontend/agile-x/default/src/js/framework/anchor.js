/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Jn2 jQuery Anchor
*/
if(typeof magento == "undefined"){
  magento = {
    framework: {}
  }
}
magento.framework.anchor = function(){
  (function($){
    // Carrega todo os link que possuem #url no como href
    jQuery('a[data-anchor]').click(function() {

      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : jQuery('[id=' + this.hash.slice(1) +']');
        if (target.length) {
          jQuery('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });

  })(jQuery);
};
