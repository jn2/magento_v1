/**
* JN2
* @package: Jn2 Default Theme
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Jn2 Image lazy Load
*/

if(typeof magento == "undefined"){
  magento = {
    framework: {}
  }
}

magento.framework.images_load = function(){
  (function($){

    $("img[data-src]").each(function (index, element) {
      // $(element).attr("src", "");
    });

    // Aguarda o carregamento completo da pÃ¡gina
    $(window).load(function(){

      // Pega todas imagens com o atributo data-src
      $("img[data-src]").each(function (index, element) {
        var url = $(element).data("src");
        $(element).attr("src", url);
      }).promise().done( function() {
        // Dispara o gatilho image:load ao finalizar o carregamento das imagens
        $(document).trigger("image:load");
      });
    });

  })(jQuery);
};
