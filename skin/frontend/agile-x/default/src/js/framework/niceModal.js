/**
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
* @class: Nice Modal
*/

jQuery.fn.nicemodal = function(options, callback){
  var me = this;
  switch (options) {
    case "close":
    this.removeClass("open");
    jQuery("body").find(".backed-nicemodal").remove();
    break;
    case "toogle":
    if(jQuery(this).hasClass("open")){
      jQuery(this).nicemodal("close");
    }else{
      jQuery(this).nicemodal();
    }
    break;
    default:
    this.css({
      "margin-left": -(this.width()/2)+"px",
      "margin-top": -(this.height()/2)+"px",
      "top": (jQuery(window).height()/2)+"px",
      "left": (jQuery(window).width()/2)-30+"px"
    }).addClass("open");
    this.before("<div class='backed-nicemodal'></div>");
    break;
  }
  jQuery(this).on("click", "button[data-dismiss='true']", function(){
    jQuery(me).nicemodal("close");
  });
  if(callback instanceof Function) callback();
  return this;
}
