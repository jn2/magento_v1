# JN2 Default Theme
***

## Preperação do ambiente

### NPM
Npm é o gerenciador de pacotes para JavaScript. Encontre, compartilhe e reutilize pacotes de código de Centenas de milhares de desenvolvedores - e montá-los em novas e poderosas maneiras.

Para instalar o NPM acesse o link abaixo <br>
https://www.npmjs.com/

### Sass
Sass é a linguagem de extensão CSS mais madura, estável e poderosa do mundo.

Para instalar o Compass (compilador sass) acesso o link abaixo<br>
http://compass-style.org/install/


### Gulp
Automatize e aprimore seu fluxo de trabalho

Para instalar o Gulp acesso o link abaixo<br>
http://gulpjs.com/

***
## Estutura de arquivos

A estrutura foi pensada de forma que a manutenção seja modular e a mais simple possível.

### Pastas

```
├── .
├── ..
├── skin +
├──── frontend +
├────── jn2 +
├──────── default +
├────────── css
├────────── js
├────────── images
├────────── fonts
├────────── src +
├──────────── fonts
├──────────── images
├──────────── js +
├────────────── framework
├────────────── modules
├──────────── sass +
├────────────── framework
├────────────── modules
```

#### Compilados

Abixo estão as pastas com os arquivos que serão chamados pelo tema:

* default/css
* default/js
* default/images
* default/fonts


**CSS** receberá todos os arquivo de css compilado e minificado, sendo o style.css o arquivo principal com o core, cores, módulos, framework, etc...

**JS** receberá todos os arquivo de javascript compilado e minificado, sendo o scripts.js o arquivo principal com o core, cores, módulos, framework, etc...

**IMAGES** receberá todos os arquivo de imagens já otimizados

**FONTS** receberá todos os arquivo de fonts já otimizados

***
### Arquivo fontes

Os arquivos fontes ficam dentra da pasta **SRC**

#### Javascript
Os códigos javascript estão dentra na pasta **SRC/JS**, sendo o arquivo **application.js** o arquivo principal e responsavel pela execução de todo o javascript do tema.

##### Estutura de pastas **JavaScript**
```
├─ .
├─ ..
├─ js +
├─── framework
├─── modules
├─── application.js
```

##### Pasta **Framework**
Nesta pasta deverá conter todos os scripts que serão usados em todas as páginas do tema, como plugins criados especificamente para uso do **core** ex: Animação do link âncora *(#link)*, Lazy load imagem para catálogo de produtos, etc...
Os modulos do framework serão carregados e executados no momento em que **SCRITS.JS** for carregado.

#### Como criar um plugin de Framework?
Para criar um novo plugin para o framework basta seguir os passos abaixo:

* Criar o arquivo na pasta default/src/js/framework/**nome_do_plugin.js**
* Adicionar o código abaixo como skeleton para o plugin

```javascript
jn2.framework.nome_do_plugin = function(){
  (function($){

    // Coloque o conteúdo do plugin
    // Lembre-se que o conteúdo será executado assim que o arquivo for carregado

  })(jQuery);
}
```
**Pronto!**
Seu plugin foi adiciondo a lista de compilação.


##### Pasta **Modules**
Está pasta é específica para módulos de página do Magento, ex: página do produto, página de categoria, página de login, etc...
Os módulos serão executado no momento em que a página finalizar o carregamento por completo.

#### Como criar novo módulo?
Para criar um novo módulo basta seguir os passos abaixo:

* Criar o arquivo na pasta default/src/js/modules/**nome_do_modulo.js**
* Adicionar o código abaixo como skeleton para o modulo

```javascript
jn2.modules.nome_do_modulo = function(){
  (function($){

    // Coloque o conteúdo do módulo
    // Lembre-se que o conteúdo será executado quando a página finalizar o carregamento por completo

  })(jQuery);
}
```
**Pronto!**
Seu módulo foi adiciondo a lista de compilação.


#### Stylesheet
Os códigos de estilos estão dentra na pasta **SRC/SASS**, sendo o arquivo **style.scss** o arquivo principal e responsavel pela importação de todo o estilo do tema.

##### Estutura de pastas **Sass**
```
├─ .
├─ ..
├─ sass +
├─── framework
├─── modules
├─── _colors.scss
├─── _framework.scss
├─── _modules.scss
├─── _vars.scss
├─── style.scss
```

##### Pasta **Framework**
Está pasta é responsável por guardar todos os arquivo de scss utilizado na estrutura básica do tema como: sistema de grid, botões, etc...

Todos arquivo adiciondo nesta pasta deve ser incluido manualmente dentro do aquivo _framework.scss
```sass
// Exemplo de como importar o novo arquivo
@import "framework/nome_do_arquivo.scss"
```


##### Pasta **Modules**
Está pasta é responsável por guardar todos os arquivo de scss utilizado na estrutura básica das página como: categoria, produto, home, etc...

Todos arquivo adiciondo nesta pasta deve ser incluido manualmente dentro do aquivo _modules.scss
```sass
// Exemplo de como importar o novo arquivo
@import "modules/nome_do_arquivo.scss"
```

##### Arquivo **_Colors.scss**
Neste arquivo está as cores utilizadas nos arquivos scss a serem compilados

##### Arquivo **_Vars.scss**
Neste arquivo está as variáveis utilizadas nos arquivos scss a serem compilados


#### Imagens e fontes
As imagens utilizadas pelo tema deve estar dentro das pastas **default/src/images** e **default/src/fonts** respectivamente. Desta forma elas serão compiladas e copiadas para as pastas corretas na qual serão chamadas pelo XML no Frontend


***
## Compilação
Para compilar os arquivos o processo é muito simples.
Você deverá ter instalado previamente na máquina o **NPM**, **SASS** e o **GULP**.

**Compilando o Javascript, imagens e fonts**
Para escutar qualquer alteração nos arquivos e compilar automaticamente.
```shell
gulp watch
```

Para compilar.
```shell
gulp
```

**Compilando o CSS**
Para escutar qualquer alteração nos arquivos e compilar automaticamente.
```shell
compass watch
```

Para compilar.
```shell
compass compile
```
