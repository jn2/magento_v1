OnestepcheckoutShipment = Class.create();
OnestepcheckoutShipment.prototype = {
    initialize: function(config) {
        this.container = $$(config.containerSelector).first();
        this.switchMethodInputs = $$(config.switchMethodInputsSelector);
        this.saveShipmentUrl = config.saveShipmentUrl;

        var _current_method = document.querySelector(config.currentMethodSelector);
        this.currentMethod = _current_method ? _current_method.value : null;

        this.init();
        this.initObservers();
    },

    init: function() {
        var me = this;
        this.switchMethodInputs.each(function(element) {
            var methodCode = element.value;
            if (element.checked) {
                //me.currentMethod = methodCode;
                //Quando existe apenas uma opção, força o ajax do save acontecer
                me.switchToMethod(methodCode);
            }
        });
    },

    initObservers: function() {
        var me = this;
        this.switchMethodInputs.each(function(element) {
            element.observe('click', function(e) {
                me.switchToMethod(element.value);
            });
        })
    },

    switchToMethod: function(methodCode) {
        if (this.currentMethod !== methodCode) {
            OnestepcheckoutCore.updater.startRequest(this.saveShipmentUrl, {
                method: 'post',
                parameters: Form.serialize(this.container, true)
            });
            this.currentMethod = methodCode;
        }
    }
};
