/**
* JN2
* Jn2 Default Theme
* @package: design
* @author: Igor Gottschalg <igorgottsweb@gmail.com>
*
*/

// Carregamento dos plugins
var
gulp        = require('gulp'),
concat      = require('gulp-concat'),
uglify      = require('gulp-uglify'),
notify      = require('gulp-notify'),
minify      = require('gulp-minify'),
pump        = require('pump'),
strip       = require('gulp-strip-comments'),
compass     = require('gulp-compass'),
browserSync = require('browser-sync').create();

// Pasta com os códigos
var pathsSrc = {
  scripts: [
    '../../default/src/js/vars.js',
    '../../default/src/js/config/*.js',
    '../../default/src/js/framework/*.js',
    '../../default/src/js/modules/*.js',
    '../../default/src/js/application.js',
    './js/config/*.js',
    './js/modules/*.js'
  ],
  sass: ['./sass/**/*.scss'],
};

// Pasta destino com compilados
var pathDistro = {
  scripts: '../js',
  styles: '../css'
}

// JavaScript
gulp.task('js', function () {

  var stream = gulp
  .src(pathsSrc.scripts)
  .pipe(strip())
  .pipe(concat('script.js'))
  .on('error', swallowError)
  .pipe(uglify({
    mangle: false
  }))
  .on('error', swallowError)
  .pipe(gulp.dest(pathDistro.scripts));

  browserSync.reload();
  return stream
  .pipe(notify({message: 'Successfully compiled JavaScript'}));
});

// Sass
gulp.task('sass', function () {
  return gulp.src(pathsSrc.sass)
  .pipe(compass({
    config_file: './config.rb',
    css: pathDistro.styles,
    sass: 'sass'
  }).on('error', swallowError))
  .pipe(gulp.dest(pathDistro.styles));

  browserSync.reload();
  return stream
  .pipe(notify({message: 'Successfully compiled Css'}));
});

// browserSync Server proxy
gulp.task('server', ['default'], function () {
  browserSync.init({
    proxy: "jn2-magento-1-9.dev"
  });
  notify({message: 'Server was loaded'});
});

gulp.task('reload', function () {
  browserSync.reload();
  notify({message: 'Server was refreshed'});
});



// Tarefa padrão
gulp.task('default', function () {
  gulp.start('js', 'sass');
});

// Error ignore
function swallowError(error) {
  console.log(error.toString())
  this.emit('end')
}

// Watch
gulp.task('watch', ['server'], function () {

  // Watch All Phtml
  gulp.watch("../../../../../app/design/frontend/agile-x/**/*", ['reload']);

  // Watch .js files
  gulp.watch(pathsSrc.scripts, ['js']);

  // Watch Sass
  gulp.watch(pathsSrc.sass, ['sass']);

});
