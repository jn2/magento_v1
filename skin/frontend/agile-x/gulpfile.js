/**
 * JN2
 * Jn2 Default Theme
 * @package: design
 * @author: Igor Gottschalg <igorgottsweb@gmail.com>
 *
 */

const gulp = require('gulp');
const chug = require('gulp-chug');

const path = require('path');
const fs = require('fs');
const child_process = require('child_process');

function getFolders(dir) {
  return fs.readdirSync(dir)
    .filter(function(file) {
      return fs.statSync(path.join(dir, file)).isDirectory();
    });
}

function subfolders(folder) {
  return fs.readdirSync(folder)
    .filter(subfolder => fs.statSync(path.join(folder, subfolder)).isDirectory())
    .filter(subfolder => subfolder !== 'node_modules' && subfolder[0] !== '.')
    .map(subfolder => path.join(folder, subfolder))
}

gulp.task('install', function() {
  // Since this script is intended to be run as a "preinstall" command,
  // it will be `npm install` inside root in the end.
  console.log('===================================================================')
  console.log(`Performing "npm install" inside Theme's folder`)
  console.log('===================================================================')

  for (let theme of getFolders("./")) {
    var folder = "./" + theme + "/src/";
    if (!fs.existsSync(folder + "node_modules") && theme != "node_modules") {
      console.log("");
      console.log('===================================================================')
      console.log(`Performing install on theme ` + theme)
      console.log('===================================================================')
      child_process.execSync('npm install', { cwd: folder, env: process.env, stdio: 'inherit' })
    } else {
      console.log("");
      console.log('===================================================================')
      console.log(`Theme ` + theme + " it's OK!")
      console.log('===================================================================')

    }
  }
});

gulp.task('default', function() {
  gulp.src('./**/src/gulpfile.js')
    .pipe(chug())
});

gulp.task('compile-all', function() {
    gulp.src( './**/src/gulpfile.js' )
  // gulp.src([
  //     './assinatura-clean/src/gulpfile.js',
  //     './beauty-clean/src/gulpfile.js',
  //     './beauty-cross/src/gulpfile.js',
  //     './beauty-flat/src/gulpfile.js',
  //     './deco-clean/src/gulpfile.js',
  //     './deco-cross/src/gulpfile.js',
  //     './deco-flat/src/gulpfile.js',
  //     './default/src/gulpfile.js',
  //     './eletro-blue/src/gulpfile.js',
  //     './eletro-clean/src/gulpfile.js',
  //     './eletro-cross/src/gulpfile.js',
  //     './eletro-flat/src/gulpfile.js',
  //     './esporte-clean/src/gulpfile.js',
  //     './esporte-cross/src/gulpfile.js',
  //     './esporte-flat/src/gulpfile.js',
  //     './games-clean/src/gulpfile.js',
  //     './games-cross/src/gulpfile.js',
  //     './games-flat/src/gulpfile.js',
  //     './moda-clean/src/gulpfile.js',
  //     './moda-cross/src/gulpfile.js',
  //     './moda-flat/src/gulpfile.js',
  //     './moveedeco-clean/src/gulpfile.js',      
  //   ])
    .pipe(chug())
});