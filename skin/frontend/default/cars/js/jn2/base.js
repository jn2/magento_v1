jQuery(function ($) {
    jQuery.fn.imagesLoaded = function () {
        var $imgs = this.find('img[src!=""]');
        if (!$imgs.length) {
            return $.Deferred().resolve().promise();
        }
        var dfds = [];
        $imgs.each(function () {
            var dfd = $.Deferred();
            dfds.push(dfd);
            var img = new Image();
            img.onload = function () {
                dfd.resolve();
            }
            img.onerror = function () {
                dfd.resolve();
            }
            img.src = this.src;
        });
        return $.when.apply($, dfds);
    }

    if ($(window).width() < 460) {
        $("#ibanners-home_top_left-wrapper").remove()
    }

    if ($(window).width() > 767) {
        $(document).imagesLoaded().then(function () {

            $(".products-grid").each(function (index, group) {
                var heightMax = 0;
                $(group).find("li").each(function (index, element) {
                    if ($(element).height() > heightMax) {
                        heightMax = $(element).height();
                    }
                });
                $(group).find("li").height(heightMax);
            });
            $(".products-grid").find("li > .actions").css({
                position: 'absolute',
                width: '100%',
                bottom: '5px'
            });
            $(".products-grid").find("li > .actions button").css('margin', '0px');
        });

        $(".products-grid").each(function (indexGroup, group) {

            var heightMax = 0;
            $(group).find(".product-name").each(function (index, element) {
                if ($(element).height() > heightMax) {
                    heightMax = $(element).height();
                }
            });
            $(group).find(".product-name").height(heightMax);

            var ratingHeight = 0;
            var watchsHeight = 0;

            $(group).find("li").each(function (indexLi, element) {

                var productName = $(element).find(".product-name");
                var ratings = $(element).find(".ratings");
                var watch = $(element).find(".watchs");


                if (ratings.height() > 0) {
                    productName.addClass("with-rating");
                    if (ratings.height() > ratingHeight) {
                        ratingHeight = ratings.height();
                    }
                }

                if (watch.height() > 0) {
                    productName.addClass("with-watch");
                    if (watch.height() > watchsHeight) {
                        watchsHeight = watch.height();
                    }
                }

            });

            var productNameAll = $(group).find(".product-name");

            productNameAll.each(function (indexName, elementName) {
                var elementName = $(elementName);

                if (!elementName.hasClass("with-rating")) {
                    elementName.height(elementName.height() + ratingHeight + 15);
                }

                if (!elementName.hasClass("with-watch") && elementName.hasClass("with-rating")) {
                    elementName.height(elementName.height() + watchsHeight + 8);
                } else if (!elementName.hasClass("with-watch")) {
                    elementName.height(elementName.height() + watchsHeight);
                }

            });



//            var nameHeight = productNameAll.height() + ratingHeight + 15; //15 é a margin do rating
//            productNameAll.css('height',nameHeight);
//            
//            var productNameAll = $(group).find(".product-name").not(".with-watch");
//            var nameHeight = nameHeight + watchsHeight;
//            productNameAll.css('height',nameHeight);


            var heightMax = 0;
            $(group).find(".price-box").each(function (index, element) {
                if ($(element).height() > heightMax) {
                    heightMax = $(element).height() + 30;
                }
            });
            $(group).find(".price-box").height(heightMax);


        });
    }
    ;


});
