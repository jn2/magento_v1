$chageToolBar = '<div class="show-panel"><i class="fa fa-cog fa-spin"></i></div><div class="panel"> <h3>Cores</h3> <div class="panel-container"> <div class="options"> <ul class="color-switch"></ul> </div></div></div><style>.triangle{width: 0; height: 0; border-style: solid; border-width: 30px 30px 0 0; border-color: #003f82 transparent transparent transparent;}.triangle-bottom{width: 0; height: 0; border-style: solid; border-width: 0 0 30px 30px; border-color: #003f82 transparent transparent transparent; margin-top: -30px;}.show-panel{width: 50px; height: 49px; position: fixed; top: 100px; left: -1px; background: #666; z-index: 122; cursor: pointer;}.show-panel i{font-size: 24px; color: #9ACFB7; line-height: 49px;}.panel{width: 400px; padding: 10px 0; background: rgba(0, 0, 0, 0.6); display: none; position: fixed; top: 100px; left: 0; z-index: 121;}.panel h3{font-weight: 700; margin: 50px 0 10px; color: #fff;}.panel-container{float: left; width: 90%; margin: 10px 2.5% 0;}.color-switch{padding-left: 0px; margin-left: 10px; padding-bottom: 30px; margin-top: 6px; margin-bottom: 20px; width: 95%;}.color-switch li{display: inline-block; width: 30px; height: 30px; margin: 2px 0 0 2px;}.color-switch li.red{background: #F65857;}.color-switch li.red{background: #F65857;}.color-switch li a{float: left; display: block; width: 100%; height: 100%; position: relative; z-index: 10;}</style>';

jQuery(document).ready(function(){

	var colors = [
		{
			color1:        "408840",
			color2:        "80AD41",
			color3:        "80AD41",
			color4:        "80AD41",
			color5:        "408840",
			footerColor1:  "004600",
			footerColor2:  "FFFFFF",
			headerColor1:  "FFFFFF",
			headerColor2:  "1C3000",
			menuColor1:    "A1E73C",
			menuColor2:    "FFF0F0",
			buyButton:     "E74C3C",
			buyButtonText: "FFFFFF"
		},
		{
			color1:        "4C4A42",
			color2:        "4C4A42",
			color3:        "4C4A42",
			color4:        "4C4A42",
			color5:        "4C4A42",
			footerColor1:  "819444",
			footerColor2:  "FFFFFF",
			headerColor1:  "FFFFFF",
			headerColor2:  "534922",
			menuColor1:    "4C4A42",
			menuColor2:    "FFFFFF",
			buyButton:     "6BB300",
			buyButtonText: "FFFFFF"
		},
		{
			color1:        "013C61",
			color2:        "07BBA0",
			color3:        "026BAE",
			color4:        "FFFFFF",
			color5:        "000000",
			footerColor1:  "D1D1D1",
			footerColor2:  "000000",
			headerColor1:  "013C61",
			headerColor2:  "FFFFFF",
			menuColor1:    "07BBA0",
			menuColor2:    "FFFFFF",
			buyButton:     "07BBA0",
			buyButtonText: "FFFFFF"
		},
		{
			color1:        "990000",
			color2:        "990000",
			color3:        "990000",
			color4:        "990000",
			color5:        "990000",
			footerColor1:  "000000",
			footerColor2:  "FFFFFF",
			headerColor1:  "000000",
			headerColor2:  "FFFFFF",
			menuColor1:    "990000",
			menuColor2:    "FFFFFF",
			buyButton:     "218237",
			buyButtonText: "FFFFFF"
		},
		{
			color1:        "FDD900",
			color2:        "6633CC",
			color3:        "026BAE",
			color4:        "FFFFFF",
			color5:        "000000",
			footerColor1:  "666666",
			footerColor2:  "FFFFFF",
			headerColor1:  "FDD900",
			headerColor2:  "6633CC",
			menuColor1:    "6633CC",
			menuColor2:    "FFFFFF",
			buyButton:     "07BBA0",
			buyButtonText: "FFFFFF"
		},
		{
			color1:        "81593C",
			color2:        "085B3C",
			color3:        "085B3C",
			color4:        "085B3C",
			color5:        "085B3C",
			footerColor1:  "085B3C",
			footerColor2:  "C5D1D1",
			headerColor1:  "FFFFFF",
			headerColor2:  "81593C",
			menuColor1:    "085B3C",
			menuColor2:    "FFFFFF",
			buyButton:     "085B3C",
			buyButtonText: "FFFFFF"
		},
		{
			color1:        "FA8D29",
			color2:        "FF5000",
			color3:        "FF5000",
			color4:        "FF5000",
			color5:        "FF5000",
			footerColor1:  "54565B",
			footerColor2:  "ECE9E9",
			headerColor1:  "FFFFFF",
			headerColor2:  "FF5000",
			menuColor1:    "FF5000",
			menuColor2:    "FFFFFF",
			buyButton:     "085B3C",
			buyButtonText: "FFFFFF"
		},
		{
			color1:        "DB302A",
			color2:        "292A2E",
			color3:        "292A2E",
			color4:        "292A2E",
			color5:        "292A2E",
			footerColor1:  "292A2E",
			footerColor2:  "ECE9E9",
			headerColor1:  "292A2E",
			headerColor2:  "DB302A",
			menuColor1:    "DB302A",
			menuColor2:    "292A2E",
			buyButton:     "085B3C",
			buyButtonText: "FFFFFF"
		}



	];


	jQuery("body").append($chageToolBar);

	jQuery(document).on("click", ".show-panel", function(){
		jQuery(".panel").slideToggle();
	});

	jQuery.each(colors, function(index, element){
		jQuery(".color-switch").append(jQuery("<li/>",{
			html: jQuery("<a/>",{
				href: "#",
				html: '<div class="triangle" style="border-color: #' + element.color1 + ' transparent transparent transparent;"></div>'+
					  '<div class="triangle-bottom" style="border-color: transparent transparent #' + element.color2 + ' transparent;"></div>'
			})
		}));
	});

	jQuery(document).on("click", ".panel li a", function(){
		var index = jQuery(this).parent().index();
		jQuery("#themeColor").attr("href", BASE_URL+"skin/frontend/base/default/jn2/home/wireframes/color.php?"
			+ "color1=" +  colors[index].color1 
			+ "&color2=" +  colors[index].color2
			+ "&color3=" +  colors[index].color3 
			+ "&color4=" +  colors[index].color4 
			+ "&color5=" +  colors[index].color5 
			+ "&footerColor1=" +  colors[index].footerColor1 
			+ "&footerColor2=" +  colors[index].footerColor2 
			+ "&headerColor1=" +  colors[index].headerColor1 
			+ "&headerColor2=" +  colors[index].headerColor2 
			+ "&menuColor1=" +  colors[index].menuColor1 
			+ "&menuColor2=" +  colors[index].menuColor2
			+ "&buyButton=" +  colors[index].buyButton
			+ "&buyButtonText=" +  colors[index].buyButtonText
			);
	});
});