var J = jQuery.noConflict();

jQuery(function($){
  jQuery.fn.imagesLoaded = function () {
    var $imgs = this.find('img[src!=""]');
    if (!$imgs.length) {return $.Deferred().resolve().promise();}
    var dfds = [];
    $imgs.each(function(){
      var dfd = jQuery.Deferred();
      dfds.push(dfd);
      var img = new Image();
      img.onload = function(){dfd.resolve();}
      img.onerror = function(){dfd.resolve();}
      img.src = this.src;
    });
    return jQuery.when.apply($,dfds);
  }
});

function isPage(className){
  return J("body").hasClass(className);
}

function is_mobile(){
  return jQuery(window).width() <= 768;
}

jQuery(document).ready(function(J){
  var $body        = J("body");
  var $page        = J(".page");
  var $header      = J("header");
  var $main        = J(".main-container");
  var $window      = J(window);
  var $menu        = J(".mega-menu-jn2");
  var $wrapper     = J(".wrapper");
  var windowHeight = $window.height();

  //jQuery(".product-image-zoom").attr("style","width: 90% !important; padding: 10px");

});
