(function($){

  var controller = {
    productbar_relocate: function() {
      var anchor  = $('.short-description');
      var element = $('.box-buy');

      var window_top = $(window).scrollTop();
      var div_top = anchor.offset().top + anchor.height() + element.height();
      if (window_top > div_top) {
        element.addClass('realocate');
      } else {
        element.removeClass('realocate');
      }
    }

  };

  function initialize() {
    $(window).scroll(controller.productbar_relocate);
    controller.productbar_relocate();
  }

  $(initialize);
})(jQuery);
