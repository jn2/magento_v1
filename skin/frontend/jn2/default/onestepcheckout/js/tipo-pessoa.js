/////////////////////////////////////////////////////////////////////////////////
/*TIPO PESSSOA*/
/////////////////////////////////////////////////////////////////////////////////
function setTipopessoaArea() {
};

jQuery(document).ready(function($) {
  jQuery(document).on('change', '[data-tipo_pessoa_scope] [name*=radio_tipopessoa]', function(event) {
    var $wrapper = jQuery(this).parents('[data-tipo_pessoa_scope]').first();


    //Removendo todas as classes
    $wrapper.removeClass('pessoa-jurica')
            .removeClass('pessoa-fisica');

    target = jQuery(event.target);

    var val = target.val(),
    juridica = 10,
    fisica = 9;

    //Alterando labels
    if (val == juridica) {
      $wrapper.addClass('pessoa-jurica');
    } else {
      $wrapper.addClass('pessoa-fisica');
    }
    //Definindo valor do tipopessoa hidden
    jQuery('#tipopessoa').val(target.val());
  });
  jQuery('#fisica').change();
});
