jQuery(function($){
  jQuery.fn.imagesLoaded = function () {
    var $imgs = this.find('img[src!=""]');
    if (!$imgs.length) {return $.Deferred().resolve().promise();}
    var dfds = [];
    $imgs.each(function(){
      var dfd = $.Deferred();
      dfds.push(dfd);
      var img = new Image();
      img.onload = function(){dfd.resolve();}
      img.onerror = function(){dfd.resolve();}
      img.src = this.src;
    });
    return $.when.apply($,dfds);
  }
});

jQuery(document).ready(function($){



    var $actions = $(".products-grid .actions").css('display');
    var $actionsHeight = $(".products-grid .actions").height();


    function calculoAltura(){

setTimeout(function(){

      $(".products-grid").each(function(index,group){

        var heightMax = 0;
        $(group).find("li").css({'height':'auto'})
        $(group).find("li").each(function(index,element){
          if($(element).height() > heightMax){
            heightMax = $(element).height();
          }
        });
        if($actions != "none"){
          $(group).find("li").height(heightMax + $actionsHeight + 20);
        }else{
          $(group).find("li").height(heightMax);
        }
      });

      $(".products-grid").find("li > .actions").css({
        position: 'absolute',
        width: '100%',
        bottom: '5px'
      });
      $(".products-grid").find("li > .actions button").css('margin', '0px');
}, 200);

    }


    $(window).on('load',calculoAltura);

    $(window).on('resize',calculoAltura);

});
