require 'compass/import-once/activate'
require 'sass-globbing'

http_path = "/"
css_dir = "./css/"
sass_dir = "./src/"
sourcemap = true

output_style = :compressed
