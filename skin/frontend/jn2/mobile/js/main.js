/*
 *  jQuery OwlCarousel v1.3.2
 *
 *  Copyright (c) 2013 Bartosz Wojciechowski
 *  http://www.owlgraphic.com/owlcarousel/
 *
 *  Licensed under MIT
 *
 */

/*JS Lint helpers: */
/*global dragMove: false, dragEnd: false, $, jQuery, alert, window, document */
/*jslint nomen: true, continue:true */

if (typeof Object.create !== "function") {
    Object.create = function (obj) {
        function F() {}
        F.prototype = obj;
        return new F();
    };
}
(function ($, window, document) {

    var Carousel = {
        init : function (options, el) {
            var base = this;

            base.$elem = $(el);
            base.options = $.extend({}, $.fn.owlCarousel.options, base.$elem.data(), options);

            base.userOptions = options;
            base.loadContent();
        },

        loadContent : function () {
            var base = this, url;

            function getData(data) {
                var i, content = "";
                if (typeof base.options.jsonSuccess === "function") {
                    base.options.jsonSuccess.apply(this, [data]);
                } else {
                    for (i in data.owl) {
                        if (data.owl.hasOwnProperty(i)) {
                            content += data.owl[i].item;
                        }
                    }
                    base.$elem.html(content);
                }
                base.logIn();
            }

            if (typeof base.options.beforeInit === "function") {
                base.options.beforeInit.apply(this, [base.$elem]);
            }

            if (typeof base.options.jsonPath === "string") {
                url = base.options.jsonPath;
                $.getJSON(url, getData);
            } else {
                base.logIn();
            }
        },

        logIn : function () {
            var base = this;

            base.$elem.data("owl-originalStyles", base.$elem.attr("style"))
                      .data("owl-originalClasses", base.$elem.attr("class"));

            base.$elem.css({opacity: 0});
            base.orignalItems = base.options.items;
            base.checkBrowser();
            base.wrapperWidth = 0;
            base.checkVisible = null;
            base.setVars();
        },

        setVars : function () {
            var base = this;
            if (base.$elem.children().length === 0) {return false; }
            base.baseClass();
            base.eventTypes();
            base.$userItems = base.$elem.children();
            base.itemsAmount = base.$userItems.length;
            base.wrapItems();
            base.$owlItems = base.$elem.find(".owl-item");
            base.$owlWrapper = base.$elem.find(".owl-wrapper");
            base.playDirection = "next";
            base.prevItem = 0;
            base.prevArr = [0];
            base.currentItem = 0;
            base.customEvents();
            base.onStartup();
        },

        onStartup : function () {
            var base = this;
            base.updateItems();
            base.calculateAll();
            base.buildControls();
            base.updateControls();
            base.response();
            base.moveEvents();
            base.stopOnHover();
            base.owlStatus();

            if (base.options.transitionStyle !== false) {
                base.transitionTypes(base.options.transitionStyle);
            }
            if (base.options.autoPlay === true) {
                base.options.autoPlay = 5000;
            }
            base.play();

            base.$elem.find(".owl-wrapper").css("display", "block");

            if (!base.$elem.is(":visible")) {
                base.watchVisibility();
            } else {
                base.$elem.css("opacity", 1);
            }
            base.onstartup = false;
            base.eachMoveUpdate();
            if (typeof base.options.afterInit === "function") {
                base.options.afterInit.apply(this, [base.$elem]);
            }
        },

        eachMoveUpdate : function () {
            var base = this;

            if (base.options.lazyLoad === true) {
                base.lazyLoad();
            }
            if (base.options.autoHeight === true) {
                base.autoHeight();
            }
            base.onVisibleItems();

            if (typeof base.options.afterAction === "function") {
                base.options.afterAction.apply(this, [base.$elem]);
            }
        },

        updateVars : function () {
            var base = this;
            if (typeof base.options.beforeUpdate === "function") {
                base.options.beforeUpdate.apply(this, [base.$elem]);
            }
            base.watchVisibility();
            base.updateItems();
            base.calculateAll();
            base.updatePosition();
            base.updateControls();
            base.eachMoveUpdate();
            if (typeof base.options.afterUpdate === "function") {
                base.options.afterUpdate.apply(this, [base.$elem]);
            }
        },

        reload : function () {
            var base = this;
            window.setTimeout(function () {
                base.updateVars();
            }, 0);
        },

        watchVisibility : function () {
            var base = this;

            if (base.$elem.is(":visible") === false) {
                base.$elem.css({opacity: 0});
                window.clearInterval(base.autoPlayInterval);
                window.clearInterval(base.checkVisible);
            } else {
                return false;
            }
            base.checkVisible = window.setInterval(function () {
                if (base.$elem.is(":visible")) {
                    base.reload();
                    base.$elem.animate({opacity: 1}, 200);
                    window.clearInterval(base.checkVisible);
                }
            }, 500);
        },

        wrapItems : function () {
            var base = this;
            base.$userItems.wrapAll("<div class=\"owl-wrapper\">").wrap("<div class=\"owl-item\"></div>");
            base.$elem.find(".owl-wrapper").wrap("<div class=\"owl-wrapper-outer\">");
            base.wrapperOuter = base.$elem.find(".owl-wrapper-outer");
            base.$elem.css("display", "block");
        },

        baseClass : function () {
            var base = this,
                hasBaseClass = base.$elem.hasClass(base.options.baseClass),
                hasThemeClass = base.$elem.hasClass(base.options.theme);

            if (!hasBaseClass) {
                base.$elem.addClass(base.options.baseClass);
            }

            if (!hasThemeClass) {
                base.$elem.addClass(base.options.theme);
            }
        },

        updateItems : function () {
            var base = this, width, i;

            if (base.options.responsive === false) {
                return false;
            }
            if (base.options.singleItem === true) {
                base.options.items = base.orignalItems = 1;
                base.options.itemsCustom = false;
                base.options.itemsDesktop = false;
                base.options.itemsDesktopSmall = false;
                base.options.itemsTablet = false;
                base.options.itemsTabletSmall = false;
                base.options.itemsMobile = false;
                return false;
            }

            width = $(base.options.responsiveBaseWidth).width();

            if (width > (base.options.itemsDesktop[0] || base.orignalItems)) {
                base.options.items = base.orignalItems;
            }
            if (base.options.itemsCustom !== false) {
                //Reorder array by screen size
                base.options.itemsCustom.sort(function (a, b) {return a[0] - b[0]; });

                for (i = 0; i < base.options.itemsCustom.length; i += 1) {
                    if (base.options.itemsCustom[i][0] <= width) {
                        base.options.items = base.options.itemsCustom[i][1];
                    }
                }

            } else {

                if (width <= base.options.itemsDesktop[0] && base.options.itemsDesktop !== false) {
                    base.options.items = base.options.itemsDesktop[1];
                }

                if (width <= base.options.itemsDesktopSmall[0] && base.options.itemsDesktopSmall !== false) {
                    base.options.items = base.options.itemsDesktopSmall[1];
                }

                if (width <= base.options.itemsTablet[0] && base.options.itemsTablet !== false) {
                    base.options.items = base.options.itemsTablet[1];
                }

                if (width <= base.options.itemsTabletSmall[0] && base.options.itemsTabletSmall !== false) {
                    base.options.items = base.options.itemsTabletSmall[1];
                }

                if (width <= base.options.itemsMobile[0] && base.options.itemsMobile !== false) {
                    base.options.items = base.options.itemsMobile[1];
                }
            }

            //if number of items is less than declared
            if (base.options.items > base.itemsAmount && base.options.itemsScaleUp === true) {
                base.options.items = base.itemsAmount;
            }
        },

        response : function () {
            var base = this,
                smallDelay,
                lastWindowWidth;

            if (base.options.responsive !== true) {
                return false;
            }
            lastWindowWidth = $(window).width();

            base.resizer = function () {
                if ($(window).width() !== lastWindowWidth) {
                    if (base.options.autoPlay !== false) {
                        window.clearInterval(base.autoPlayInterval);
                    }
                    window.clearTimeout(smallDelay);
                    smallDelay = window.setTimeout(function () {
                        lastWindowWidth = $(window).width();
                        base.updateVars();
                    }, base.options.responsiveRefreshRate);
                }
            };
            $(window).resize(base.resizer);
        },

        updatePosition : function () {
            var base = this;
            base.jumpTo(base.currentItem);
            if (base.options.autoPlay !== false) {
                base.checkAp();
            }
        },

        appendItemsSizes : function () {
            var base = this,
                roundPages = 0,
                lastItem = base.itemsAmount - base.options.items;

            base.$owlItems.each(function (index) {
                var $this = $(this);
                $this
                    .css({"width": base.itemWidth})
                    .data("owl-item", Number(index));

                if (index % base.options.items === 0 || index === lastItem) {
                    if (!(index > lastItem)) {
                        roundPages += 1;
                    }
                }
                $this.data("owl-roundPages", roundPages);
            });
        },

        appendWrapperSizes : function () {
            var base = this,
                width = base.$owlItems.length * base.itemWidth;

            base.$owlWrapper.css({
                "width": width * 2,
                "left": 0
            });
            base.appendItemsSizes();
        },

        calculateAll : function () {
            var base = this;
            base.calculateWidth();
            base.appendWrapperSizes();
            base.loops();
            base.max();
        },

        calculateWidth : function () {
            var base = this;
            base.itemWidth = Math.round(base.$elem.width() / base.options.items);
        },

        max : function () {
            var base = this,
                maximum = ((base.itemsAmount * base.itemWidth) - base.options.items * base.itemWidth) * -1;
            if (base.options.items > base.itemsAmount) {
                base.maximumItem = 0;
                maximum = 0;
                base.maximumPixels = 0;
            } else {
                base.maximumItem = base.itemsAmount - base.options.items;
                base.maximumPixels = maximum;
            }
            return maximum;
        },

        min : function () {
            return 0;
        },

        loops : function () {
            var base = this,
                prev = 0,
                elWidth = 0,
                i,
                item,
                roundPageNum;

            base.positionsInArray = [0];
            base.pagesInArray = [];

            for (i = 0; i < base.itemsAmount; i += 1) {
                elWidth += base.itemWidth;
                base.positionsInArray.push(-elWidth);

                if (base.options.scrollPerPage === true) {
                    item = $(base.$owlItems[i]);
                    roundPageNum = item.data("owl-roundPages");
                    if (roundPageNum !== prev) {
                        base.pagesInArray[prev] = base.positionsInArray[i];
                        prev = roundPageNum;
                    }
                }
            }
        },

        buildControls : function () {
            var base = this;
            if (base.options.navigation === true || base.options.pagination === true) {
                base.owlControls = $("<div class=\"owl-controls\"/>").toggleClass("clickable", !base.browser.isTouch).appendTo(base.$elem);
            }
            if (base.options.pagination === true) {
                base.buildPagination();
            }
            if (base.options.navigation === true) {
                base.buildButtons();
            }
        },

        buildButtons : function () {
            var base = this,
                buttonsWrapper = $("<div class=\"owl-buttons\"/>");
            base.owlControls.append(buttonsWrapper);

            base.buttonPrev = $("<div/>", {
                "class" : "owl-prev",
                "html" : base.options.navigationText[0] || ""
            });

            base.buttonNext = $("<div/>", {
                "class" : "owl-next",
                "html" : base.options.navigationText[1] || ""
            });

            buttonsWrapper
                .append(base.buttonPrev)
                .append(base.buttonNext);

            buttonsWrapper.on("touchstart.owlControls mousedown.owlControls", "div[class^=\"owl\"]", function (event) {
                event.preventDefault();
            });

            buttonsWrapper.on("touchend.owlControls mouseup.owlControls", "div[class^=\"owl\"]", function (event) {
                event.preventDefault();
                if ($(this).hasClass("owl-next")) {
                    base.next();
                } else {
                    base.prev();
                }
            });
        },

        buildPagination : function () {
            var base = this;

            base.paginationWrapper = $("<div class=\"owl-pagination\"/>");
            base.owlControls.append(base.paginationWrapper);

            base.paginationWrapper.on("touchend.owlControls mouseup.owlControls", ".owl-page", function (event) {
                event.preventDefault();
                if (Number($(this).data("owl-page")) !== base.currentItem) {
                    base.goTo(Number($(this).data("owl-page")), true);
                }
            });
        },

        updatePagination : function () {
            var base = this,
                counter,
                lastPage,
                lastItem,
                i,
                paginationButton,
                paginationButtonInner;

            if (base.options.pagination === false) {
                return false;
            }

            base.paginationWrapper.html("");

            counter = 0;
            lastPage = base.itemsAmount - base.itemsAmount % base.options.items;

            for (i = 0; i < base.itemsAmount; i += 1) {
                if (i % base.options.items === 0) {
                    counter += 1;
                    if (lastPage === i) {
                        lastItem = base.itemsAmount - base.options.items;
                    }
                    paginationButton = $("<div/>", {
                        "class" : "owl-page"
                    });
                    paginationButtonInner = $("<span></span>", {
                        "text": base.options.paginationNumbers === true ? counter : "",
                        "class": base.options.paginationNumbers === true ? "owl-numbers" : ""
                    });
                    paginationButton.append(paginationButtonInner);

                    paginationButton.data("owl-page", lastPage === i ? lastItem : i);
                    paginationButton.data("owl-roundPages", counter);

                    base.paginationWrapper.append(paginationButton);
                }
            }
            base.checkPagination();
        },
        checkPagination : function () {
            var base = this;
            if (base.options.pagination === false) {
                return false;
            }
            base.paginationWrapper.find(".owl-page").each(function () {
                if ($(this).data("owl-roundPages") === $(base.$owlItems[base.currentItem]).data("owl-roundPages")) {
                    base.paginationWrapper
                        .find(".owl-page")
                        .removeClass("active");
                    $(this).addClass("active");
                }
            });
        },

        checkNavigation : function () {
            var base = this;

            if (base.options.navigation === false) {
                return false;
            }
            if (base.options.rewindNav === false) {
                if (base.currentItem === 0 && base.maximumItem === 0) {
                    base.buttonPrev.addClass("disabled");
                    base.buttonNext.addClass("disabled");
                } else if (base.currentItem === 0 && base.maximumItem !== 0) {
                    base.buttonPrev.addClass("disabled");
                    base.buttonNext.removeClass("disabled");
                } else if (base.currentItem === base.maximumItem) {
                    base.buttonPrev.removeClass("disabled");
                    base.buttonNext.addClass("disabled");
                } else if (base.currentItem !== 0 && base.currentItem !== base.maximumItem) {
                    base.buttonPrev.removeClass("disabled");
                    base.buttonNext.removeClass("disabled");
                }
            }
        },

        updateControls : function () {
            var base = this;
            base.updatePagination();
            base.checkNavigation();
            if (base.owlControls) {
                if (base.options.items >= base.itemsAmount) {
                    base.owlControls.hide();
                } else {
                    base.owlControls.show();
                }
            }
        },

        destroyControls : function () {
            var base = this;
            if (base.owlControls) {
                base.owlControls.remove();
            }
        },

        next : function (speed) {
            var base = this;

            if (base.isTransition) {
                return false;
            }

            base.currentItem += base.options.scrollPerPage === true ? base.options.items : 1;
            if (base.currentItem > base.maximumItem + (base.options.scrollPerPage === true ? (base.options.items - 1) : 0)) {
                if (base.options.rewindNav === true) {
                    base.currentItem = 0;
                    speed = "rewind";
                } else {
                    base.currentItem = base.maximumItem;
                    return false;
                }
            }
            base.goTo(base.currentItem, speed);
        },

        prev : function (speed) {
            var base = this;

            if (base.isTransition) {
                return false;
            }

            if (base.options.scrollPerPage === true && base.currentItem > 0 && base.currentItem < base.options.items) {
                base.currentItem = 0;
            } else {
                base.currentItem -= base.options.scrollPerPage === true ? base.options.items : 1;
            }
            if (base.currentItem < 0) {
                if (base.options.rewindNav === true) {
                    base.currentItem = base.maximumItem;
                    speed = "rewind";
                } else {
                    base.currentItem = 0;
                    return false;
                }
            }
            base.goTo(base.currentItem, speed);
        },

        goTo : function (position, speed, drag) {
            var base = this,
                goToPixel;

            if (base.isTransition) {
                return false;
            }
            if (typeof base.options.beforeMove === "function") {
                base.options.beforeMove.apply(this, [base.$elem]);
            }
            if (position >= base.maximumItem) {
                position = base.maximumItem;
            } else if (position <= 0) {
                position = 0;
            }

            base.currentItem = base.owl.currentItem = position;
            if (base.options.transitionStyle !== false && drag !== "drag" && base.options.items === 1 && base.browser.support3d === true) {
                base.swapSpeed(0);
                if (base.browser.support3d === true) {
                    base.transition3d(base.positionsInArray[position]);
                } else {
                    base.css2slide(base.positionsInArray[position], 1);
                }
                base.afterGo();
                base.singleItemTransition();
                return false;
            }
            goToPixel = base.positionsInArray[position];

            if (base.browser.support3d === true) {
                base.isCss3Finish = false;

                if (speed === true) {
                    base.swapSpeed("paginationSpeed");
                    window.setTimeout(function () {
                        base.isCss3Finish = true;
                    }, base.options.paginationSpeed);

                } else if (speed === "rewind") {
                    base.swapSpeed(base.options.rewindSpeed);
                    window.setTimeout(function () {
                        base.isCss3Finish = true;
                    }, base.options.rewindSpeed);

                } else {
                    base.swapSpeed("slideSpeed");
                    window.setTimeout(function () {
                        base.isCss3Finish = true;
                    }, base.options.slideSpeed);
                }
                base.transition3d(goToPixel);
            } else {
                if (speed === true) {
                    base.css2slide(goToPixel, base.options.paginationSpeed);
                } else if (speed === "rewind") {
                    base.css2slide(goToPixel, base.options.rewindSpeed);
                } else {
                    base.css2slide(goToPixel, base.options.slideSpeed);
                }
            }
            base.afterGo();
        },

        jumpTo : function (position) {
            var base = this;
            if (typeof base.options.beforeMove === "function") {
                base.options.beforeMove.apply(this, [base.$elem]);
            }
            if (position >= base.maximumItem || position === -1) {
                position = base.maximumItem;
            } else if (position <= 0) {
                position = 0;
            }
            base.swapSpeed(0);
            if (base.browser.support3d === true) {
                base.transition3d(base.positionsInArray[position]);
            } else {
                base.css2slide(base.positionsInArray[position], 1);
            }
            base.currentItem = base.owl.currentItem = position;
            base.afterGo();
        },

        afterGo : function () {
            var base = this;

            base.prevArr.push(base.currentItem);
            base.prevItem = base.owl.prevItem = base.prevArr[base.prevArr.length - 2];
            base.prevArr.shift(0);

            if (base.prevItem !== base.currentItem) {
                base.checkPagination();
                base.checkNavigation();
                base.eachMoveUpdate();

                if (base.options.autoPlay !== false) {
                    base.checkAp();
                }
            }
            if (typeof base.options.afterMove === "function" && base.prevItem !== base.currentItem) {
                base.options.afterMove.apply(this, [base.$elem]);
            }
        },

        stop : function () {
            var base = this;
            base.apStatus = "stop";
            window.clearInterval(base.autoPlayInterval);
        },

        checkAp : function () {
            var base = this;
            if (base.apStatus !== "stop") {
                base.play();
            }
        },

        play : function () {
            var base = this;
            base.apStatus = "play";
            if (base.options.autoPlay === false) {
                return false;
            }
            window.clearInterval(base.autoPlayInterval);
            base.autoPlayInterval = window.setInterval(function () {
                base.next(true);
            }, base.options.autoPlay);
        },

        swapSpeed : function (action) {
            var base = this;
            if (action === "slideSpeed") {
                base.$owlWrapper.css(base.addCssSpeed(base.options.slideSpeed));
            } else if (action === "paginationSpeed") {
                base.$owlWrapper.css(base.addCssSpeed(base.options.paginationSpeed));
            } else if (typeof action !== "string") {
                base.$owlWrapper.css(base.addCssSpeed(action));
            }
        },

        addCssSpeed : function (speed) {
            return {
                "-webkit-transition": "all " + speed + "ms ease",
                "-moz-transition": "all " + speed + "ms ease",
                "-o-transition": "all " + speed + "ms ease",
                "transition": "all " + speed + "ms ease"
            };
        },

        removeTransition : function () {
            return {
                "-webkit-transition": "",
                "-moz-transition": "",
                "-o-transition": "",
                "transition": ""
            };
        },

        doTranslate : function (pixels) {
            return {
                "-webkit-transform": "translate3d(" + pixels + "px, 0px, 0px)",
                "-moz-transform": "translate3d(" + pixels + "px, 0px, 0px)",
                "-o-transform": "translate3d(" + pixels + "px, 0px, 0px)",
                "-ms-transform": "translate3d(" + pixels + "px, 0px, 0px)",
                "transform": "translate3d(" + pixels + "px, 0px,0px)"
            };
        },

        transition3d : function (value) {
            var base = this;
            base.$owlWrapper.css(base.doTranslate(value));
        },

        css2move : function (value) {
            var base = this;
            base.$owlWrapper.css({"left" : value});
        },

        css2slide : function (value, speed) {
            var base = this;

            base.isCssFinish = false;
            base.$owlWrapper.stop(true, true).animate({
                "left" : value
            }, {
                duration : speed || base.options.slideSpeed,
                complete : function () {
                    base.isCssFinish = true;
                }
            });
        },

        checkBrowser : function () {
            var base = this,
                translate3D = "translate3d(0px, 0px, 0px)",
                tempElem = document.createElement("div"),
                regex,
                asSupport,
                support3d,
                isTouch;

            tempElem.style.cssText = "  -moz-transform:" + translate3D +
                                  "; -ms-transform:"     + translate3D +
                                  "; -o-transform:"      + translate3D +
                                  "; -webkit-transform:" + translate3D +
                                  "; transform:"         + translate3D;
            regex = /translate3d\(0px, 0px, 0px\)/g;
            asSupport = tempElem.style.cssText.match(regex);
            support3d = (asSupport !== null && asSupport.length === 1);

            isTouch = "ontouchstart" in window || window.navigator.msMaxTouchPoints;

            base.browser = {
                "support3d" : support3d,
                "isTouch" : isTouch
            };
        },

        moveEvents : function () {
            var base = this;
            if (base.options.mouseDrag !== false || base.options.touchDrag !== false) {
                base.gestures();
                base.disabledEvents();
            }
        },

        eventTypes : function () {
            var base = this,
                types = ["s", "e", "x"];

            base.ev_types = {};

            if (base.options.mouseDrag === true && base.options.touchDrag === true) {
                types = [
                    "touchstart.owl mousedown.owl",
                    "touchmove.owl mousemove.owl",
                    "touchend.owl touchcancel.owl mouseup.owl"
                ];
            } else if (base.options.mouseDrag === false && base.options.touchDrag === true) {
                types = [
                    "touchstart.owl",
                    "touchmove.owl",
                    "touchend.owl touchcancel.owl"
                ];
            } else if (base.options.mouseDrag === true && base.options.touchDrag === false) {
                types = [
                    "mousedown.owl",
                    "mousemove.owl",
                    "mouseup.owl"
                ];
            }

            base.ev_types.start = types[0];
            base.ev_types.move = types[1];
            base.ev_types.end = types[2];
        },

        disabledEvents :  function () {
            var base = this;
            base.$elem.on("dragstart.owl", function (event) { event.preventDefault(); });
            base.$elem.on("mousedown.disableTextSelect", function (e) {
                return $(e.target).is('input, textarea, select, option');
            });
        },

        gestures : function () {
            /*jslint unparam: true*/
            var base = this,
                locals = {
                    offsetX : 0,
                    offsetY : 0,
                    baseElWidth : 0,
                    relativePos : 0,
                    position: null,
                    minSwipe : null,
                    maxSwipe: null,
                    sliding : null,
                    dargging: null,
                    targetElement : null
                };

            base.isCssFinish = true;

            function getTouches(event) {
                if (event.touches !== undefined) {
                    return {
                        x : event.touches[0].pageX,
                        y : event.touches[0].pageY
                    };
                }

                if (event.touches === undefined) {
                    if (event.pageX !== undefined) {
                        return {
                            x : event.pageX,
                            y : event.pageY
                        };
                    }
                    if (event.pageX === undefined) {
                        return {
                            x : event.clientX,
                            y : event.clientY
                        };
                    }
                }
            }

            function swapEvents(type) {
                if (type === "on") {
                    $(document).on(base.ev_types.move, dragMove);
                    $(document).on(base.ev_types.end, dragEnd);
                } else if (type === "off") {
                    $(document).off(base.ev_types.move);
                    $(document).off(base.ev_types.end);
                }
            }

            function dragStart(event) {
                var ev = event.originalEvent || event || window.event,
                    position;

                if (ev.which === 3) {
                    return false;
                }
                if (base.itemsAmount <= base.options.items) {
                    return;
                }
                if (base.isCssFinish === false && !base.options.dragBeforeAnimFinish) {
                    return false;
                }
                if (base.isCss3Finish === false && !base.options.dragBeforeAnimFinish) {
                    return false;
                }

                if (base.options.autoPlay !== false) {
                    window.clearInterval(base.autoPlayInterval);
                }

                if (base.browser.isTouch !== true && !base.$owlWrapper.hasClass("grabbing")) {
                    base.$owlWrapper.addClass("grabbing");
                }

                base.newPosX = 0;
                base.newRelativeX = 0;

                $(this).css(base.removeTransition());

                position = $(this).position();
                locals.relativePos = position.left;

                locals.offsetX = getTouches(ev).x - position.left;
                locals.offsetY = getTouches(ev).y - position.top;

                swapEvents("on");

                locals.sliding = false;
                locals.targetElement = ev.target || ev.srcElement;
            }

            function dragMove(event) {
                var ev = event.originalEvent || event || window.event,
                    minSwipe,
                    maxSwipe;

                base.newPosX = getTouches(ev).x - locals.offsetX;
                base.newPosY = getTouches(ev).y - locals.offsetY;
                base.newRelativeX = base.newPosX - locals.relativePos;

                if (typeof base.options.startDragging === "function" && locals.dragging !== true && base.newRelativeX !== 0) {
                    locals.dragging = true;
                    base.options.startDragging.apply(base, [base.$elem]);
                }

                if ((base.newRelativeX > 8 || base.newRelativeX < -8) && (base.browser.isTouch === true)) {
                    if (ev.preventDefault !== undefined) {
                        ev.preventDefault();
                    } else {
                        ev.returnValue = false;
                    }
                    locals.sliding = true;
                }

                if ((base.newPosY > 10 || base.newPosY < -10) && locals.sliding === false) {
                    $(document).off("touchmove.owl");
                }

                minSwipe = function () {
                    return base.newRelativeX / 5;
                };

                maxSwipe = function () {
                    return base.maximumPixels + base.newRelativeX / 5;
                };

                base.newPosX = Math.max(Math.min(base.newPosX, minSwipe()), maxSwipe());
                if (base.browser.support3d === true) {
                    base.transition3d(base.newPosX);
                } else {
                    base.css2move(base.newPosX);
                }
            }

            function dragEnd(event) {
                var ev = event.originalEvent || event || window.event,
                    newPosition,
                    handlers,
                    owlStopEvent;

                ev.target = ev.target || ev.srcElement;

                locals.dragging = false;

                if (base.browser.isTouch !== true) {
                    base.$owlWrapper.removeClass("grabbing");
                }

                if (base.newRelativeX < 0) {
                    base.dragDirection = base.owl.dragDirection = "left";
                } else {
                    base.dragDirection = base.owl.dragDirection = "right";
                }

                if (base.newRelativeX !== 0) {
                    newPosition = base.getNewPosition();
                    base.goTo(newPosition, false, "drag");
                    if (locals.targetElement === ev.target && base.browser.isTouch !== true) {
                        $(ev.target).on("click.disable", function (ev) {
                            ev.stopImmediatePropagation();
                            ev.stopPropagation();
                            ev.preventDefault();
                            $(ev.target).off("click.disable");
                        });
                        handlers = $._data(ev.target, "events").click;
                        owlStopEvent = handlers.pop();
                        handlers.splice(0, 0, owlStopEvent);
                    }
                }
                swapEvents("off");
            }
            base.$elem.on(base.ev_types.start, ".owl-wrapper", dragStart);
        },

        getNewPosition : function () {
            var base = this,
                newPosition = base.closestItem();

            if (newPosition > base.maximumItem) {
                base.currentItem = base.maximumItem;
                newPosition  = base.maximumItem;
            } else if (base.newPosX >= 0) {
                newPosition = 0;
                base.currentItem = 0;
            }
            return newPosition;
        },
        closestItem : function () {
            var base = this,
                array = base.options.scrollPerPage === true ? base.pagesInArray : base.positionsInArray,
                goal = base.newPosX,
                closest = null;

            $.each(array, function (i, v) {
                if (goal - (base.itemWidth / 20) > array[i + 1] && goal - (base.itemWidth / 20) < v && base.moveDirection() === "left") {
                    closest = v;
                    if (base.options.scrollPerPage === true) {
                        base.currentItem = $.inArray(closest, base.positionsInArray);
                    } else {
                        base.currentItem = i;
                    }
                } else if (goal + (base.itemWidth / 20) < v && goal + (base.itemWidth / 20) > (array[i + 1] || array[i] - base.itemWidth) && base.moveDirection() === "right") {
                    if (base.options.scrollPerPage === true) {
                        closest = array[i + 1] || array[array.length - 1];
                        base.currentItem = $.inArray(closest, base.positionsInArray);
                    } else {
                        closest = array[i + 1];
                        base.currentItem = i + 1;
                    }
                }
            });
            return base.currentItem;
        },

        moveDirection : function () {
            var base = this,
                direction;
            if (base.newRelativeX < 0) {
                direction = "right";
                base.playDirection = "next";
            } else {
                direction = "left";
                base.playDirection = "prev";
            }
            return direction;
        },

        customEvents : function () {
            /*jslint unparam: true*/
            var base = this;
            base.$elem.on("owl.next", function () {
                base.next();
            });
            base.$elem.on("owl.prev", function () {
                base.prev();
            });
            base.$elem.on("owl.play", function (event, speed) {
                base.options.autoPlay = speed;
                base.play();
                base.hoverStatus = "play";
            });
            base.$elem.on("owl.stop", function () {
                base.stop();
                base.hoverStatus = "stop";
            });
            base.$elem.on("owl.goTo", function (event, item) {
                base.goTo(item);
            });
            base.$elem.on("owl.jumpTo", function (event, item) {
                base.jumpTo(item);
            });
        },

        stopOnHover : function () {
            var base = this;
            if (base.options.stopOnHover === true && base.browser.isTouch !== true && base.options.autoPlay !== false) {
                base.$elem.on("mouseover", function () {
                    base.stop();
                });
                base.$elem.on("mouseout", function () {
                    if (base.hoverStatus !== "stop") {
                        base.play();
                    }
                });
            }
        },

        lazyLoad : function () {
            var base = this,
                i,
                $item,
                itemNumber,
                $lazyImg,
                follow;

            if (base.options.lazyLoad === false) {
                return false;
            }
            for (i = 0; i < base.itemsAmount; i += 1) {
                $item = $(base.$owlItems[i]);

                if ($item.data("owl-loaded") === "loaded") {
                    continue;
                }

                itemNumber = $item.data("owl-item");
                $lazyImg = $item.find(".lazyOwl");

                if (typeof $lazyImg.data("src") !== "string") {
                    $item.data("owl-loaded", "loaded");
                    continue;
                }
                if ($item.data("owl-loaded") === undefined) {
                    $lazyImg.hide();
                    $item.addClass("loading").data("owl-loaded", "checked");
                }
                if (base.options.lazyFollow === true) {
                    follow = itemNumber >= base.currentItem;
                } else {
                    follow = true;
                }
                if (follow && itemNumber < base.currentItem + base.options.items && $lazyImg.length) {
                    base.lazyPreload($item, $lazyImg);
                }
            }
        },

        lazyPreload : function ($item, $lazyImg) {
            var base = this,
                iterations = 0,
                isBackgroundImg;

            if ($lazyImg.prop("tagName") === "DIV") {
                $lazyImg.css("background-image", "url(" + $lazyImg.data("src") + ")");
                isBackgroundImg = true;
            } else {
                $lazyImg[0].src = $lazyImg.data("src");
            }

            function showImage() {
                $item.data("owl-loaded", "loaded").removeClass("loading");
                $lazyImg.removeAttr("data-src");
                if (base.options.lazyEffect === "fade") {
                    $lazyImg.fadeIn(400);
                } else {
                    $lazyImg.show();
                }
                if (typeof base.options.afterLazyLoad === "function") {
                    base.options.afterLazyLoad.apply(this, [base.$elem]);
                }
            }

            function checkLazyImage() {
                iterations += 1;
                if (base.completeImg($lazyImg.get(0)) || isBackgroundImg === true) {
                    showImage();
                } else if (iterations <= 100) {//if image loads in less than 10 seconds
                    window.setTimeout(checkLazyImage, 100);
                } else {
                    showImage();
                }
            }

            checkLazyImage();
        },

        autoHeight : function () {
            var base = this,
                $currentimg = $(base.$owlItems[base.currentItem]).find("img"),
                iterations;

            function addHeight() {
                var $currentItem = $(base.$owlItems[base.currentItem]).height();
                base.wrapperOuter.css("height", $currentItem + "px");
                if (!base.wrapperOuter.hasClass("autoHeight")) {
                    window.setTimeout(function () {
                        base.wrapperOuter.addClass("autoHeight");
                    }, 0);
                }
            }

            function checkImage() {
                iterations += 1;
                if (base.completeImg($currentimg.get(0))) {
                    addHeight();
                } else if (iterations <= 100) { //if image loads in less than 10 seconds
                    window.setTimeout(checkImage, 100);
                } else {
                    base.wrapperOuter.css("height", ""); //Else remove height attribute
                }
            }

            if ($currentimg.get(0) !== undefined) {
                iterations = 0;
                checkImage();
            } else {
                addHeight();
            }
        },

        completeImg : function (img) {
            var naturalWidthType;

            if (!img.complete) {
                return false;
            }
            naturalWidthType = typeof img.naturalWidth;
            if (naturalWidthType !== "undefined" && img.naturalWidth === 0) {
                return false;
            }
            return true;
        },

        onVisibleItems : function () {
            var base = this,
                i;

            if (base.options.addClassActive === true) {
                base.$owlItems.removeClass("active");
            }
            base.visibleItems = [];
            for (i = base.currentItem; i < base.currentItem + base.options.items; i += 1) {
                base.visibleItems.push(i);

                if (base.options.addClassActive === true) {
                    $(base.$owlItems[i]).addClass("active");
                }
            }
            base.owl.visibleItems = base.visibleItems;
        },

        transitionTypes : function (className) {
            var base = this;
            //Currently available: "fade", "backSlide", "goDown", "fadeUp"
            base.outClass = "owl-" + className + "-out";
            base.inClass = "owl-" + className + "-in";
        },

        singleItemTransition : function () {
            var base = this,
                outClass = base.outClass,
                inClass = base.inClass,
                $currentItem = base.$owlItems.eq(base.currentItem),
                $prevItem = base.$owlItems.eq(base.prevItem),
                prevPos = Math.abs(base.positionsInArray[base.currentItem]) + base.positionsInArray[base.prevItem],
                origin = Math.abs(base.positionsInArray[base.currentItem]) + base.itemWidth / 2,
                animEnd = 'webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend';

            base.isTransition = true;

            base.$owlWrapper
                .addClass('owl-origin')
                .css({
                    "-webkit-transform-origin" : origin + "px",
                    "-moz-perspective-origin" : origin + "px",
                    "perspective-origin" : origin + "px"
                });
            function transStyles(prevPos) {
                return {
                    "position" : "relative",
                    "left" : prevPos + "px"
                };
            }

            $prevItem
                .css(transStyles(prevPos, 10))
                .addClass(outClass)
                .on(animEnd, function () {
                    base.endPrev = true;
                    $prevItem.off(animEnd);
                    base.clearTransStyle($prevItem, outClass);
                });

            $currentItem
                .addClass(inClass)
                .on(animEnd, function () {
                    base.endCurrent = true;
                    $currentItem.off(animEnd);
                    base.clearTransStyle($currentItem, inClass);
                });
        },

        clearTransStyle : function (item, classToRemove) {
            var base = this;
            item.css({
                "position" : "",
                "left" : ""
            }).removeClass(classToRemove);

            if (base.endPrev && base.endCurrent) {
                base.$owlWrapper.removeClass('owl-origin');
                base.endPrev = false;
                base.endCurrent = false;
                base.isTransition = false;
            }
        },

        owlStatus : function () {
            var base = this;
            base.owl = {
                "userOptions"   : base.userOptions,
                "baseElement"   : base.$elem,
                "userItems"     : base.$userItems,
                "owlItems"      : base.$owlItems,
                "currentItem"   : base.currentItem,
                "prevItem"      : base.prevItem,
                "visibleItems"  : base.visibleItems,
                "isTouch"       : base.browser.isTouch,
                "browser"       : base.browser,
                "dragDirection" : base.dragDirection
            };
        },

        clearEvents : function () {
            var base = this;
            base.$elem.off(".owl owl mousedown.disableTextSelect");
            $(document).off(".owl owl");
            $(window).off("resize", base.resizer);
        },

        unWrap : function () {
            var base = this;
            if (base.$elem.children().length !== 0) {
                base.$owlWrapper.unwrap();
                base.$userItems.unwrap().unwrap();
                if (base.owlControls) {
                    base.owlControls.remove();
                }
            }
            base.clearEvents();
            base.$elem
                .attr("style", base.$elem.data("owl-originalStyles") || "")
                .attr("class", base.$elem.data("owl-originalClasses"));
        },

        destroy : function () {
            var base = this;
            base.stop();
            window.clearInterval(base.checkVisible);
            base.unWrap();
            base.$elem.removeData();
        },

        reinit : function (newOptions) {
            var base = this,
                options = $.extend({}, base.userOptions, newOptions);
            base.unWrap();
            base.init(options, base.$elem);
        },

        addItem : function (htmlString, targetPosition) {
            var base = this,
                position;

            if (!htmlString) {return false; }

            if (base.$elem.children().length === 0) {
                base.$elem.append(htmlString);
                base.setVars();
                return false;
            }
            base.unWrap();
            if (targetPosition === undefined || targetPosition === -1) {
                position = -1;
            } else {
                position = targetPosition;
            }
            if (position >= base.$userItems.length || position === -1) {
                base.$userItems.eq(-1).after(htmlString);
            } else {
                base.$userItems.eq(position).before(htmlString);
            }

            base.setVars();
        },

        removeItem : function (targetPosition) {
            var base = this,
                position;

            if (base.$elem.children().length === 0) {
                return false;
            }
            if (targetPosition === undefined || targetPosition === -1) {
                position = -1;
            } else {
                position = targetPosition;
            }

            base.unWrap();
            base.$userItems.eq(position).remove();
            base.setVars();
        }

    };

    $.fn.owlCarousel = function (options) {
        return this.each(function () {
            if ($(this).data("owl-init") === true) {
                return false;
            }
            $(this).data("owl-init", true);
            var carousel = Object.create(Carousel);
            carousel.init(options, this);
            $.data(this, "owlCarousel", carousel);
        });
    };

    $.fn.owlCarousel.options = {

        items : 5,
        itemsCustom : false,
        itemsDesktop : [1199, 4],
        itemsDesktopSmall : [979, 3],
        itemsTablet : [768, 2],
        itemsTabletSmall : false,
        itemsMobile : [479, 1],
        singleItem : false,
        itemsScaleUp : false,

        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,

        autoPlay : false,
        stopOnHover : false,

        navigation : false,
        navigationText : ["prev", "next"],
        rewindNav : true,
        scrollPerPage : false,

        pagination : true,
        paginationNumbers : false,

        responsive : true,
        responsiveRefreshRate : 200,
        responsiveBaseWidth : window,

        baseClass : "owl-carousel",
        theme : "owl-theme",

        lazyLoad : false,
        lazyFollow : true,
        lazyEffect : "fade",

        autoHeight : false,

        jsonPath : false,
        jsonSuccess : false,

        dragBeforeAnimFinish : true,
        mouseDrag : true,
        touchDrag : true,

        addClassActive : false,
        transitionStyle : false,

        beforeUpdate : false,
        afterUpdate : false,
        beforeInit : false,
        afterInit : false,
        beforeMove : false,
        afterMove : false,
        afterAction : false,
        startDragging : false,
        afterLazyLoad: false
    };
}(jQuery, window, document));
/*
 AngularJS v1.3.20
 (c) 2010-2014 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(R,W,u){'use strict';function S(b){return function(){var a=arguments[0],c;c="["+(b?b+":":"")+a+"] http://errors.angularjs.org/1.3.20/"+(b?b+"/":"")+a;for(a=1;a<arguments.length;a++){c=c+(1==a?"?":"&")+"p"+(a-1)+"=";var d=encodeURIComponent,e;e=arguments[a];e="function"==typeof e?e.toString().replace(/ \{[\s\S]*$/,""):"undefined"==typeof e?"undefined":"string"!=typeof e?JSON.stringify(e):e;c+=d(e)}return Error(c)}}function Ta(b){if(null==b||Ua(b))return!1;var a="length"in Object(b)&&b.length;
return b.nodeType===qa&&a?!0:x(b)||H(b)||0===a||"number"===typeof a&&0<a&&a-1 in b}function r(b,a,c){var d,e;if(b)if(z(b))for(d in b)"prototype"==d||"length"==d||"name"==d||b.hasOwnProperty&&!b.hasOwnProperty(d)||a.call(c,b[d],d,b);else if(H(b)||Ta(b)){var f="object"!==typeof b;d=0;for(e=b.length;d<e;d++)(f||d in b)&&a.call(c,b[d],d,b)}else if(b.forEach&&b.forEach!==r)b.forEach(a,c,b);else for(d in b)b.hasOwnProperty(d)&&a.call(c,b[d],d,b);return b}function Ed(b,a,c){for(var d=Object.keys(b).sort(),
e=0;e<d.length;e++)a.call(c,b[d[e]],d[e]);return d}function lc(b){return function(a,c){b(c,a)}}function Fd(){return++rb}function mc(b,a){a?b.$$hashKey=a:delete b.$$hashKey}function w(b){for(var a=b.$$hashKey,c=1,d=arguments.length;c<d;c++){var e=arguments[c];if(e)for(var f=Object.keys(e),g=0,h=f.length;g<h;g++){var l=f[g];b[l]=e[l]}}mc(b,a);return b}function aa(b){return parseInt(b,10)}function Ob(b,a){return w(Object.create(b),a)}function A(){}function ra(b){return b}function ea(b){return function(){return b}}
function D(b){return"undefined"===typeof b}function y(b){return"undefined"!==typeof b}function L(b){return null!==b&&"object"===typeof b}function x(b){return"string"===typeof b}function Y(b){return"number"===typeof b}function ha(b){return"[object Date]"===Ca.call(b)}function z(b){return"function"===typeof b}function Va(b){return"[object RegExp]"===Ca.call(b)}function Ua(b){return b&&b.window===b}function Wa(b){return b&&b.$evalAsync&&b.$watch}function Xa(b){return"boolean"===typeof b}function nc(b){return!(!b||
!(b.nodeName||b.prop&&b.attr&&b.find))}function Gd(b){var a={};b=b.split(",");var c;for(c=0;c<b.length;c++)a[b[c]]=!0;return a}function wa(b){return K(b.nodeName||b[0]&&b[0].nodeName)}function Ya(b,a){var c=b.indexOf(a);0<=c&&b.splice(c,1);return a}function Da(b,a,c,d){if(Ua(b)||Wa(b))throw Ja("cpws");if(a){if(b===a)throw Ja("cpi");c=c||[];d=d||[];if(L(b)){var e=c.indexOf(b);if(-1!==e)return d[e];c.push(b);d.push(a)}if(H(b))for(var f=a.length=0;f<b.length;f++)e=Da(b[f],null,c,d),L(b[f])&&(c.push(b[f]),
d.push(e)),a.push(e);else{var g=a.$$hashKey;H(a)?a.length=0:r(a,function(b,c){delete a[c]});for(f in b)b.hasOwnProperty(f)&&(e=Da(b[f],null,c,d),L(b[f])&&(c.push(b[f]),d.push(e)),a[f]=e);mc(a,g)}}else if(a=b)H(b)?a=Da(b,[],c,d):ha(b)?a=new Date(b.getTime()):Va(b)?(a=new RegExp(b.source,b.toString().match(/[^\/]*$/)[0]),a.lastIndex=b.lastIndex):L(b)&&(e=Object.create(Object.getPrototypeOf(b)),a=Da(b,e,c,d));return a}function sa(b,a){if(H(b)){a=a||[];for(var c=0,d=b.length;c<d;c++)a[c]=b[c]}else if(L(b))for(c in a=
a||{},b)if("$"!==c.charAt(0)||"$"!==c.charAt(1))a[c]=b[c];return a||b}function ia(b,a){if(b===a)return!0;if(null===b||null===a)return!1;if(b!==b&&a!==a)return!0;var c=typeof b,d;if(c==typeof a&&"object"==c)if(H(b)){if(!H(a))return!1;if((c=b.length)==a.length){for(d=0;d<c;d++)if(!ia(b[d],a[d]))return!1;return!0}}else{if(ha(b))return ha(a)?ia(b.getTime(),a.getTime()):!1;if(Va(b))return Va(a)?b.toString()==a.toString():!1;if(Wa(b)||Wa(a)||Ua(b)||Ua(a)||H(a)||ha(a)||Va(a))return!1;c={};for(d in b)if("$"!==
d.charAt(0)&&!z(b[d])){if(!ia(b[d],a[d]))return!1;c[d]=!0}for(d in a)if(!c.hasOwnProperty(d)&&"$"!==d.charAt(0)&&a[d]!==u&&!z(a[d]))return!1;return!0}return!1}function Za(b,a,c){return b.concat($a.call(a,c))}function oc(b,a){var c=2<arguments.length?$a.call(arguments,2):[];return!z(a)||a instanceof RegExp?a:c.length?function(){return arguments.length?a.apply(b,Za(c,arguments,0)):a.apply(b,c)}:function(){return arguments.length?a.apply(b,arguments):a.call(b)}}function Hd(b,a){var c=a;"string"===typeof b&&
"$"===b.charAt(0)&&"$"===b.charAt(1)?c=u:Ua(a)?c="$WINDOW":a&&W===a?c="$DOCUMENT":Wa(a)&&(c="$SCOPE");return c}function ab(b,a){if("undefined"===typeof b)return u;Y(a)||(a=a?2:null);return JSON.stringify(b,Hd,a)}function pc(b){return x(b)?JSON.parse(b):b}function xa(b){b=B(b).clone();try{b.empty()}catch(a){}var c=B("<div>").append(b).html();try{return b[0].nodeType===bb?K(c):c.match(/^(<[^>]+>)/)[1].replace(/^<([\w\-]+)/,function(a,b){return"<"+K(b)})}catch(d){return K(c)}}function qc(b){try{return decodeURIComponent(b)}catch(a){}}
function rc(b){var a={},c,d;r((b||"").split("&"),function(b){b&&(c=b.replace(/\+/g,"%20").split("="),d=qc(c[0]),y(d)&&(b=y(c[1])?qc(c[1]):!0,sc.call(a,d)?H(a[d])?a[d].push(b):a[d]=[a[d],b]:a[d]=b))});return a}function Pb(b){var a=[];r(b,function(b,d){H(b)?r(b,function(b){a.push(Ea(d,!0)+(!0===b?"":"="+Ea(b,!0)))}):a.push(Ea(d,!0)+(!0===b?"":"="+Ea(b,!0)))});return a.length?a.join("&"):""}function sb(b){return Ea(b,!0).replace(/%26/gi,"&").replace(/%3D/gi,"=").replace(/%2B/gi,"+")}function Ea(b,a){return encodeURIComponent(b).replace(/%40/gi,
"@").replace(/%3A/gi,":").replace(/%24/g,"$").replace(/%2C/gi,",").replace(/%3B/gi,";").replace(/%20/g,a?"%20":"+")}function Id(b,a){var c,d,e=tb.length;b=B(b);for(d=0;d<e;++d)if(c=tb[d]+a,x(c=b.attr(c)))return c;return null}function Jd(b,a){var c,d,e={};r(tb,function(a){a+="app";!c&&b.hasAttribute&&b.hasAttribute(a)&&(c=b,d=b.getAttribute(a))});r(tb,function(a){a+="app";var e;!c&&(e=b.querySelector("["+a.replace(":","\\:")+"]"))&&(c=e,d=e.getAttribute(a))});c&&(e.strictDi=null!==Id(c,"strict-di"),
a(c,d?[d]:[],e))}function tc(b,a,c){L(c)||(c={});c=w({strictDi:!1},c);var d=function(){b=B(b);if(b.injector()){var d=b[0]===W?"document":xa(b);throw Ja("btstrpd",d.replace(/</,"&lt;").replace(/>/,"&gt;"));}a=a||[];a.unshift(["$provide",function(a){a.value("$rootElement",b)}]);c.debugInfoEnabled&&a.push(["$compileProvider",function(a){a.debugInfoEnabled(!0)}]);a.unshift("ng");d=cb(a,c.strictDi);d.invoke(["$rootScope","$rootElement","$compile","$injector",function(a,b,c,d){a.$apply(function(){b.data("$injector",
d);c(b)(a)})}]);return d},e=/^NG_ENABLE_DEBUG_INFO!/,f=/^NG_DEFER_BOOTSTRAP!/;R&&e.test(R.name)&&(c.debugInfoEnabled=!0,R.name=R.name.replace(e,""));if(R&&!f.test(R.name))return d();R.name=R.name.replace(f,"");ca.resumeBootstrap=function(b){r(b,function(b){a.push(b)});return d()};z(ca.resumeDeferredBootstrap)&&ca.resumeDeferredBootstrap()}function Kd(){R.name="NG_ENABLE_DEBUG_INFO!"+R.name;R.location.reload()}function Ld(b){b=ca.element(b).injector();if(!b)throw Ja("test");return b.get("$$testability")}
function uc(b,a){a=a||"_";return b.replace(Md,function(b,d){return(d?a:"")+b.toLowerCase()})}function Nd(){var b;vc||((ta=R.jQuery)&&ta.fn.on?(B=ta,w(ta.fn,{scope:Ka.scope,isolateScope:Ka.isolateScope,controller:Ka.controller,injector:Ka.injector,inheritedData:Ka.inheritedData}),b=ta.cleanData,ta.cleanData=function(a){var c;if(Qb)Qb=!1;else for(var d=0,e;null!=(e=a[d]);d++)(c=ta._data(e,"events"))&&c.$destroy&&ta(e).triggerHandler("$destroy");b(a)}):B=T,ca.element=B,vc=!0)}function Rb(b,a,c){if(!b)throw Ja("areq",
a||"?",c||"required");return b}function La(b,a,c){c&&H(b)&&(b=b[b.length-1]);Rb(z(b),a,"not a function, got "+(b&&"object"===typeof b?b.constructor.name||"Object":typeof b));return b}function Ma(b,a){if("hasOwnProperty"===b)throw Ja("badname",a);}function wc(b,a,c){if(!a)return b;a=a.split(".");for(var d,e=b,f=a.length,g=0;g<f;g++)d=a[g],b&&(b=(e=b)[d]);return!c&&z(b)?oc(e,b):b}function ub(b){var a=b[0];b=b[b.length-1];var c=[a];do{a=a.nextSibling;if(!a)break;c.push(a)}while(a!==b);return B(c)}function ja(){return Object.create(null)}
function Od(b){function a(a,b,c){return a[b]||(a[b]=c())}var c=S("$injector"),d=S("ng");b=a(b,"angular",Object);b.$$minErr=b.$$minErr||S;return a(b,"module",function(){var b={};return function(f,g,h){if("hasOwnProperty"===f)throw d("badname","module");g&&b.hasOwnProperty(f)&&(b[f]=null);return a(b,f,function(){function a(c,d,e,f){f||(f=b);return function(){f[e||"push"]([c,d,arguments]);return t}}if(!g)throw c("nomod",f);var b=[],d=[],e=[],q=a("$injector","invoke","push",d),t={_invokeQueue:b,_configBlocks:d,
_runBlocks:e,requires:g,name:f,provider:a("$provide","provider"),factory:a("$provide","factory"),service:a("$provide","service"),value:a("$provide","value"),constant:a("$provide","constant","unshift"),animation:a("$animateProvider","register"),filter:a("$filterProvider","register"),controller:a("$controllerProvider","register"),directive:a("$compileProvider","directive"),config:q,run:function(a){e.push(a);return this}};h&&q(h);return t})}})}function Pd(b){w(b,{bootstrap:tc,copy:Da,extend:w,equals:ia,
element:B,forEach:r,injector:cb,noop:A,bind:oc,toJson:ab,fromJson:pc,identity:ra,isUndefined:D,isDefined:y,isString:x,isFunction:z,isObject:L,isNumber:Y,isElement:nc,isArray:H,version:Qd,isDate:ha,lowercase:K,uppercase:vb,callbacks:{counter:0},getTestability:Ld,$$minErr:S,$$csp:db,reloadWithDebugInfo:Kd});eb=Od(R);try{eb("ngLocale")}catch(a){eb("ngLocale",[]).provider("$locale",Rd)}eb("ng",["ngLocale"],["$provide",function(a){a.provider({$$sanitizeUri:Sd});a.provider("$compile",xc).directive({a:Td,
input:yc,textarea:yc,form:Ud,script:Vd,select:Wd,style:Xd,option:Yd,ngBind:Zd,ngBindHtml:$d,ngBindTemplate:ae,ngClass:be,ngClassEven:ce,ngClassOdd:de,ngCloak:ee,ngController:fe,ngForm:ge,ngHide:he,ngIf:ie,ngInclude:je,ngInit:ke,ngNonBindable:le,ngPluralize:me,ngRepeat:ne,ngShow:oe,ngStyle:pe,ngSwitch:qe,ngSwitchWhen:re,ngSwitchDefault:se,ngOptions:te,ngTransclude:ue,ngModel:ve,ngList:we,ngChange:xe,pattern:zc,ngPattern:zc,required:Ac,ngRequired:Ac,minlength:Bc,ngMinlength:Bc,maxlength:Cc,ngMaxlength:Cc,
ngValue:ye,ngModelOptions:ze}).directive({ngInclude:Ae}).directive(wb).directive(Dc);a.provider({$anchorScroll:Be,$animate:Ce,$browser:De,$cacheFactory:Ee,$controller:Fe,$document:Ge,$exceptionHandler:He,$filter:Ec,$interpolate:Ie,$interval:Je,$http:Ke,$httpBackend:Le,$location:Me,$log:Ne,$parse:Oe,$rootScope:Pe,$q:Qe,$$q:Re,$sce:Se,$sceDelegate:Te,$sniffer:Ue,$templateCache:Ve,$templateRequest:We,$$testability:Xe,$timeout:Ye,$window:Ze,$$rAF:$e,$$asyncCallback:af,$$jqLite:bf})}])}function fb(b){return b.replace(cf,
function(a,b,d,e){return e?d.toUpperCase():d}).replace(df,"Moz$1")}function Fc(b){b=b.nodeType;return b===qa||!b||9===b}function Gc(b,a){var c,d,e=a.createDocumentFragment(),f=[];if(Sb.test(b)){c=c||e.appendChild(a.createElement("div"));d=(ef.exec(b)||["",""])[1].toLowerCase();d=ka[d]||ka._default;c.innerHTML=d[1]+b.replace(ff,"<$1></$2>")+d[2];for(d=d[0];d--;)c=c.lastChild;f=Za(f,c.childNodes);c=e.firstChild;c.textContent=""}else f.push(a.createTextNode(b));e.textContent="";e.innerHTML="";r(f,function(a){e.appendChild(a)});
return e}function T(b){if(b instanceof T)return b;var a;x(b)&&(b=N(b),a=!0);if(!(this instanceof T)){if(a&&"<"!=b.charAt(0))throw Tb("nosel");return new T(b)}if(a){a=W;var c;b=(c=gf.exec(b))?[a.createElement(c[1])]:(c=Gc(b,a))?c.childNodes:[]}Hc(this,b)}function Ub(b){return b.cloneNode(!0)}function xb(b,a){a||yb(b);if(b.querySelectorAll)for(var c=b.querySelectorAll("*"),d=0,e=c.length;d<e;d++)yb(c[d])}function Ic(b,a,c,d){if(y(d))throw Tb("offargs");var e=(d=zb(b))&&d.events,f=d&&d.handle;if(f)if(a)r(a.split(" "),
function(a){if(y(c)){var d=e[a];Ya(d||[],c);if(d&&0<d.length)return}b.removeEventListener(a,f,!1);delete e[a]});else for(a in e)"$destroy"!==a&&b.removeEventListener(a,f,!1),delete e[a]}function yb(b,a){var c=b.ng339,d=c&&Ab[c];d&&(a?delete d.data[a]:(d.handle&&(d.events.$destroy&&d.handle({},"$destroy"),Ic(b)),delete Ab[c],b.ng339=u))}function zb(b,a){var c=b.ng339,c=c&&Ab[c];a&&!c&&(b.ng339=c=++hf,c=Ab[c]={events:{},data:{},handle:u});return c}function Vb(b,a,c){if(Fc(b)){var d=y(c),e=!d&&a&&!L(a),
f=!a;b=(b=zb(b,!e))&&b.data;if(d)b[a]=c;else{if(f)return b;if(e)return b&&b[a];w(b,a)}}}function Bb(b,a){return b.getAttribute?-1<(" "+(b.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ").indexOf(" "+a+" "):!1}function Cb(b,a){a&&b.setAttribute&&r(a.split(" "),function(a){b.setAttribute("class",N((" "+(b.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ").replace(" "+N(a)+" "," ")))})}function Db(b,a){if(a&&b.setAttribute){var c=(" "+(b.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ");
r(a.split(" "),function(a){a=N(a);-1===c.indexOf(" "+a+" ")&&(c+=a+" ")});b.setAttribute("class",N(c))}}function Hc(b,a){if(a)if(a.nodeType)b[b.length++]=a;else{var c=a.length;if("number"===typeof c&&a.window!==a){if(c)for(var d=0;d<c;d++)b[b.length++]=a[d]}else b[b.length++]=a}}function Jc(b,a){return Eb(b,"$"+(a||"ngController")+"Controller")}function Eb(b,a,c){9==b.nodeType&&(b=b.documentElement);for(a=H(a)?a:[a];b;){for(var d=0,e=a.length;d<e;d++)if((c=B.data(b,a[d]))!==u)return c;b=b.parentNode||
11===b.nodeType&&b.host}}function Kc(b){for(xb(b,!0);b.firstChild;)b.removeChild(b.firstChild)}function Lc(b,a){a||xb(b);var c=b.parentNode;c&&c.removeChild(b)}function jf(b,a){a=a||R;if("complete"===a.document.readyState)a.setTimeout(b);else B(a).on("load",b)}function Mc(b,a){var c=Fb[a.toLowerCase()];return c&&Nc[wa(b)]&&c}function kf(b,a){var c=b.nodeName;return("INPUT"===c||"TEXTAREA"===c)&&Oc[a]}function lf(b,a){var c=function(c,e){c.isDefaultPrevented=function(){return c.defaultPrevented};var f=
a[e||c.type],g=f?f.length:0;if(g){if(D(c.immediatePropagationStopped)){var h=c.stopImmediatePropagation;c.stopImmediatePropagation=function(){c.immediatePropagationStopped=!0;c.stopPropagation&&c.stopPropagation();h&&h.call(c)}}c.isImmediatePropagationStopped=function(){return!0===c.immediatePropagationStopped};1<g&&(f=sa(f));for(var l=0;l<g;l++)c.isImmediatePropagationStopped()||f[l].call(b,c)}};c.elem=b;return c}function bf(){this.$get=function(){return w(T,{hasClass:function(b,a){b.attr&&(b=b[0]);
return Bb(b,a)},addClass:function(b,a){b.attr&&(b=b[0]);return Db(b,a)},removeClass:function(b,a){b.attr&&(b=b[0]);return Cb(b,a)}})}}function Na(b,a){var c=b&&b.$$hashKey;if(c)return"function"===typeof c&&(c=b.$$hashKey()),c;c=typeof b;return c="function"==c||"object"==c&&null!==b?b.$$hashKey=c+":"+(a||Fd)():c+":"+b}function gb(b,a){if(a){var c=0;this.nextUid=function(){return++c}}r(b,this.put,this)}function mf(b){return(b=b.toString().replace(Pc,"").match(Qc))?"function("+(b[1]||"").replace(/[\s\r\n]+/,
" ")+")":"fn"}function cb(b,a){function c(a){return function(b,c){if(L(b))r(b,lc(a));else return a(b,c)}}function d(a,b){Ma(a,"service");if(z(b)||H(b))b=q.instantiate(b);if(!b.$get)throw Fa("pget",a);return p[a+"Provider"]=b}function e(a,b){return function(){var c=s.invoke(b,this);if(D(c))throw Fa("undef",a);return c}}function f(a,b,c){return d(a,{$get:!1!==c?e(a,b):b})}function g(a){var b=[],c;r(a,function(a){function d(a){var b,c;b=0;for(c=a.length;b<c;b++){var e=a[b],f=q.get(e[0]);f[e[1]].apply(f,
e[2])}}if(!n.get(a)){n.put(a,!0);try{x(a)?(c=eb(a),b=b.concat(g(c.requires)).concat(c._runBlocks),d(c._invokeQueue),d(c._configBlocks)):z(a)?b.push(q.invoke(a)):H(a)?b.push(q.invoke(a)):La(a,"module")}catch(e){throw H(a)&&(a=a[a.length-1]),e.message&&e.stack&&-1==e.stack.indexOf(e.message)&&(e=e.message+"\n"+e.stack),Fa("modulerr",a,e.stack||e.message||e);}}});return b}function h(b,c){function d(a,e){if(b.hasOwnProperty(a)){if(b[a]===l)throw Fa("cdep",a+" <- "+k.join(" <- "));return b[a]}try{return k.unshift(a),
b[a]=l,b[a]=c(a,e)}catch(f){throw b[a]===l&&delete b[a],f;}finally{k.shift()}}function e(b,c,f,g){"string"===typeof f&&(g=f,f=null);var k=[],l=cb.$$annotate(b,a,g),h,q,p;q=0;for(h=l.length;q<h;q++){p=l[q];if("string"!==typeof p)throw Fa("itkn",p);k.push(f&&f.hasOwnProperty(p)?f[p]:d(p,g))}H(b)&&(b=b[h]);return b.apply(c,k)}return{invoke:e,instantiate:function(a,b,c){var d=Object.create((H(a)?a[a.length-1]:a).prototype||null);a=e(a,d,b,c);return L(a)||z(a)?a:d},get:d,annotate:cb.$$annotate,has:function(a){return p.hasOwnProperty(a+
"Provider")||b.hasOwnProperty(a)}}}a=!0===a;var l={},k=[],n=new gb([],!0),p={$provide:{provider:c(d),factory:c(f),service:c(function(a,b){return f(a,["$injector",function(a){return a.instantiate(b)}])}),value:c(function(a,b){return f(a,ea(b),!1)}),constant:c(function(a,b){Ma(a,"constant");p[a]=b;t[a]=b}),decorator:function(a,b){var c=q.get(a+"Provider"),d=c.$get;c.$get=function(){var a=s.invoke(d,c);return s.invoke(b,null,{$delegate:a})}}}},q=p.$injector=h(p,function(a,b){ca.isString(b)&&k.push(b);
throw Fa("unpr",k.join(" <- "));}),t={},s=t.$injector=h(t,function(a,b){var c=q.get(a+"Provider",b);return s.invoke(c.$get,c,u,a)});r(g(b),function(a){s.invoke(a||A)});return s}function Be(){var b=!0;this.disableAutoScrolling=function(){b=!1};this.$get=["$window","$location","$rootScope",function(a,c,d){function e(a){var b=null;Array.prototype.some.call(a,function(a){if("a"===wa(a))return b=a,!0});return b}function f(b){if(b){b.scrollIntoView();var c;c=g.yOffset;z(c)?c=c():nc(c)?(c=c[0],c="fixed"!==
a.getComputedStyle(c).position?0:c.getBoundingClientRect().bottom):Y(c)||(c=0);c&&(b=b.getBoundingClientRect().top,a.scrollBy(0,b-c))}else a.scrollTo(0,0)}function g(){var a=c.hash(),b;a?(b=h.getElementById(a))?f(b):(b=e(h.getElementsByName(a)))?f(b):"top"===a&&f(null):f(null)}var h=a.document;b&&d.$watch(function(){return c.hash()},function(a,b){a===b&&""===a||jf(function(){d.$evalAsync(g)})});return g}]}function af(){this.$get=["$$rAF","$timeout",function(b,a){return b.supported?function(a){return b(a)}:
function(b){return a(b,0,!1)}}]}function nf(b,a,c,d){function e(a){try{a.apply(null,$a.call(arguments,1))}finally{if(m--,0===m)for(;C.length;)try{C.pop()()}catch(b){c.error(b)}}}function f(a,b){(function da(){r($,function(a){a()});I=b(da,a)})()}function g(){h();l()}function h(){a:{try{M=t.state;break a}catch(a){}M=void 0}M=D(M)?null:M;ia(M,P)&&(M=P);P=M}function l(){if(G!==n.url()||E!==M)G=n.url(),E=M,r(X,function(a){a(n.url(),M)})}function k(a){try{return decodeURIComponent(a)}catch(b){return a}}
var n=this,p=a[0],q=b.location,t=b.history,s=b.setTimeout,F=b.clearTimeout,v={};n.isMock=!1;var m=0,C=[];n.$$completeOutstandingRequest=e;n.$$incOutstandingRequestCount=function(){m++};n.notifyWhenNoOutstandingRequests=function(a){r($,function(a){a()});0===m?a():C.push(a)};var $=[],I;n.addPollFn=function(a){D(I)&&f(100,s);$.push(a);return a};var M,E,G=q.href,O=a.find("base"),Q=null;h();E=M;n.url=function(a,c,e){D(e)&&(e=null);q!==b.location&&(q=b.location);t!==b.history&&(t=b.history);if(a){var f=
E===e;if(G===a&&(!d.history||f))return n;var g=G&&Ga(G)===Ga(a);G=a;E=e;if(!d.history||g&&f){if(!g||Q)Q=a;c?q.replace(a):g?(c=q,e=a.indexOf("#"),a=-1===e?"":a.substr(e),c.hash=a):q.href=a}else t[c?"replaceState":"pushState"](e,"",a),h(),E=M;return n}return Q||q.href.replace(/%27/g,"'")};n.state=function(){return M};var X=[],ba=!1,P=null;n.onUrlChange=function(a){if(!ba){if(d.history)B(b).on("popstate",g);B(b).on("hashchange",g);ba=!0}X.push(a);return a};n.$$checkUrlChange=l;n.baseHref=function(){var a=
O.attr("href");return a?a.replace(/^(https?\:)?\/\/[^\/]*/,""):""};var fa={},y="",la=n.baseHref();n.cookies=function(a,b){var d,e,f,g;if(a)b===u?p.cookie=encodeURIComponent(a)+"=;path="+la+";expires=Thu, 01 Jan 1970 00:00:00 GMT":x(b)&&(d=(p.cookie=encodeURIComponent(a)+"="+encodeURIComponent(b)+";path="+la).length+1,4096<d&&c.warn("Cookie '"+a+"' possibly not set or overflowed because it was too large ("+d+" > 4096 bytes)!"));else{if(p.cookie!==y)for(y=p.cookie,d=y.split("; "),fa={},f=0;f<d.length;f++)e=
d[f],g=e.indexOf("="),0<g&&(a=k(e.substring(0,g)),fa[a]===u&&(fa[a]=k(e.substring(g+1))));return fa}};n.defer=function(a,b){var c;m++;c=s(function(){delete v[c];e(a)},b||0);v[c]=!0;return c};n.defer.cancel=function(a){return v[a]?(delete v[a],F(a),e(A),!0):!1}}function De(){this.$get=["$window","$log","$sniffer","$document",function(b,a,c,d){return new nf(b,d,a,c)}]}function Ee(){this.$get=function(){function b(b,d){function e(a){a!=p&&(q?q==a&&(q=a.n):q=a,f(a.n,a.p),f(a,p),p=a,p.n=null)}function f(a,
b){a!=b&&(a&&(a.p=b),b&&(b.n=a))}if(b in a)throw S("$cacheFactory")("iid",b);var g=0,h=w({},d,{id:b}),l={},k=d&&d.capacity||Number.MAX_VALUE,n={},p=null,q=null;return a[b]={put:function(a,b){if(k<Number.MAX_VALUE){var c=n[a]||(n[a]={key:a});e(c)}if(!D(b))return a in l||g++,l[a]=b,g>k&&this.remove(q.key),b},get:function(a){if(k<Number.MAX_VALUE){var b=n[a];if(!b)return;e(b)}return l[a]},remove:function(a){if(k<Number.MAX_VALUE){var b=n[a];if(!b)return;b==p&&(p=b.p);b==q&&(q=b.n);f(b.n,b.p);delete n[a]}delete l[a];
g--},removeAll:function(){l={};g=0;n={};p=q=null},destroy:function(){n=h=l=null;delete a[b]},info:function(){return w({},h,{size:g})}}}var a={};b.info=function(){var b={};r(a,function(a,e){b[e]=a.info()});return b};b.get=function(b){return a[b]};return b}}function Ve(){this.$get=["$cacheFactory",function(b){return b("templates")}]}function xc(b,a){function c(a,b){var c=/^\s*([@&]|=(\*?))(\??)\s*(\w*)\s*$/,d={};r(a,function(a,e){var f=a.match(c);if(!f)throw ma("iscp",b,e,a);d[e]={mode:f[1][0],collection:"*"===
f[2],optional:"?"===f[3],attrName:f[4]||e}});return d}var d={},e=/^\s*directive\:\s*([\w\-]+)\s+(.*)$/,f=/(([\w\-]+)(?:\:([^;]+))?;?)/,g=Gd("ngSrc,ngSrcset,src,srcset"),h=/^(?:(\^\^?)?(\?)?(\^\^?)?)?/,l=/^(on[a-z]+|formaction)$/;this.directive=function p(a,e){Ma(a,"directive");x(a)?(Rb(e,"directiveFactory"),d.hasOwnProperty(a)||(d[a]=[],b.factory(a+"Directive",["$injector","$exceptionHandler",function(b,e){var f=[];r(d[a],function(d,g){try{var h=b.invoke(d);z(h)?h={compile:ea(h)}:!h.compile&&h.link&&
(h.compile=ea(h.link));h.priority=h.priority||0;h.index=g;h.name=h.name||a;h.require=h.require||h.controller&&h.name;h.restrict=h.restrict||"EA";L(h.scope)&&(h.$$isolateBindings=c(h.scope,h.name));f.push(h)}catch(l){e(l)}});return f}])),d[a].push(e)):r(a,lc(p));return this};this.aHrefSanitizationWhitelist=function(b){return y(b)?(a.aHrefSanitizationWhitelist(b),this):a.aHrefSanitizationWhitelist()};this.imgSrcSanitizationWhitelist=function(b){return y(b)?(a.imgSrcSanitizationWhitelist(b),this):a.imgSrcSanitizationWhitelist()};
var k=!0;this.debugInfoEnabled=function(a){return y(a)?(k=a,this):k};this.$get=["$injector","$interpolate","$exceptionHandler","$templateRequest","$parse","$controller","$rootScope","$document","$sce","$animate","$$sanitizeUri",function(a,b,c,s,F,v,m,C,$,I,M){function E(a,b){try{a.addClass(b)}catch(c){}}function G(a,b,c,d,e){a instanceof B||(a=B(a));r(a,function(b,c){b.nodeType==bb&&b.nodeValue.match(/\S+/)&&(a[c]=B(b).wrap("<span></span>").parent()[0])});var f=O(a,b,a,c,d,e);G.$$addScopeClass(a);
var g=null;return function(b,c,d){Rb(b,"scope");d=d||{};var e=d.parentBoundTranscludeFn,h=d.transcludeControllers;d=d.futureParentElement;e&&e.$$boundTransclude&&(e=e.$$boundTransclude);g||(g=(d=d&&d[0])?"foreignobject"!==wa(d)&&d.toString().match(/SVG/)?"svg":"html":"html");d="html"!==g?B(Xb(g,B("<div>").append(a).html())):c?Ka.clone.call(a):a;if(h)for(var l in h)d.data("$"+l+"Controller",h[l].instance);G.$$addScopeInfo(d,b);c&&c(d,b);f&&f(b,d,d,e);return d}}function O(a,b,c,d,e,f){function g(a,
c,d,e){var f,l,k,q,p,s,t;if(m)for(t=Array(c.length),q=0;q<h.length;q+=3)f=h[q],t[f]=c[f];else t=c;q=0;for(p=h.length;q<p;)l=t[h[q++]],c=h[q++],f=h[q++],c?(c.scope?(k=a.$new(),G.$$addScopeInfo(B(l),k)):k=a,s=c.transcludeOnThisElement?Q(a,c.transclude,e,c.elementTranscludeOnThisElement):!c.templateOnThisElement&&e?e:!e&&b?Q(a,b):null,c(f,k,l,d,s)):f&&f(a,l.childNodes,u,e)}for(var h=[],l,k,q,p,m,s=0;s<a.length;s++){l=new Yb;k=X(a[s],[],l,0===s?d:u,e);(f=k.length?fa(k,a[s],l,b,c,null,[],[],f):null)&&
f.scope&&G.$$addScopeClass(l.$$element);l=f&&f.terminal||!(q=a[s].childNodes)||!q.length?null:O(q,f?(f.transcludeOnThisElement||!f.templateOnThisElement)&&f.transclude:b);if(f||l)h.push(s,f,l),p=!0,m=m||f;f=null}return p?g:null}function Q(a,b,c,d){return function(d,e,f,g,h){d||(d=a.$new(!1,h),d.$$transcluded=!0);return b(d,e,{parentBoundTranscludeFn:c,transcludeControllers:f,futureParentElement:g})}}function X(a,b,c,d,g){var h=c.$attr,l;switch(a.nodeType){case qa:la(b,ya(wa(a)),"E",d,g);for(var k,
q,p,m=a.attributes,s=0,t=m&&m.length;s<t;s++){var M=!1,I=!1;k=m[s];l=k.name;q=N(k.value);k=ya(l);if(p=U.test(k))l=l.replace(Rc,"").substr(8).replace(/_(.)/g,function(a,b){return b.toUpperCase()});var F=k.replace(/(Start|End)$/,"");D(F)&&k===F+"Start"&&(M=l,I=l.substr(0,l.length-5)+"end",l=l.substr(0,l.length-6));k=ya(l.toLowerCase());h[k]=l;if(p||!c.hasOwnProperty(k))c[k]=q,Mc(a,k)&&(c[k]=!0);Pa(a,b,q,k,p);la(b,k,"A",d,g,M,I)}a=a.className;L(a)&&(a=a.animVal);if(x(a)&&""!==a)for(;l=f.exec(a);)k=ya(l[2]),
la(b,k,"C",d,g)&&(c[k]=N(l[3])),a=a.substr(l.index+l[0].length);break;case bb:za(b,a.nodeValue);break;case 8:try{if(l=e.exec(a.nodeValue))k=ya(l[1]),la(b,k,"M",d,g)&&(c[k]=N(l[2]))}catch(v){}}b.sort(da);return b}function ba(a,b,c){var d=[],e=0;if(b&&a.hasAttribute&&a.hasAttribute(b)){do{if(!a)throw ma("uterdir",b,c);a.nodeType==qa&&(a.hasAttribute(b)&&e++,a.hasAttribute(c)&&e--);d.push(a);a=a.nextSibling}while(0<e)}else d.push(a);return B(d)}function P(a,b,c){return function(d,e,f,g,h){e=ba(e[0],
b,c);return a(d,e,f,g,h)}}function fa(a,d,e,f,g,l,k,p,m){function s(a,b,c,d){if(a){c&&(a=P(a,c,d));a.require=J.require;a.directiveName=da;if(Q===J||J.$$isolateScope)a=Y(a,{isolateScope:!0});k.push(a)}if(b){c&&(b=P(b,c,d));b.require=J.require;b.directiveName=da;if(Q===J||J.$$isolateScope)b=Y(b,{isolateScope:!0});p.push(b)}}function M(a,b,c,d){var e,f="data",g=!1,l=c,k;if(x(b)){k=b.match(h);b=b.substring(k[0].length);k[3]&&(k[1]?k[3]=null:k[1]=k[3]);"^"===k[1]?f="inheritedData":"^^"===k[1]&&(f="inheritedData",
l=c.parent());"?"===k[2]&&(g=!0);e=null;d&&"data"===f&&(e=d[b])&&(e=e.instance);e=e||l[f]("$"+b+"Controller");if(!e&&!g)throw ma("ctreq",b,a);return e||null}H(b)&&(e=[],r(b,function(b){e.push(M(a,b,c,d))}));return e}function I(a,c,f,g,l){function h(a,b,c){var d;Wa(a)||(c=b,b=a,a=u);A&&(d=C);c||(c=A?X.parent():X);return l(a,b,d,c,Wb)}var m,s,t,E,C,ib,X,P;d===f?(P=e,X=e.$$element):(X=B(f),P=new Yb(X,e));Q&&(E=c.$new(!0));l&&(ib=h,ib.$$boundTransclude=l);O&&($={},C={},r(O,function(a){var b={$scope:a===
Q||a.$$isolateScope?E:c,$element:X,$attrs:P,$transclude:ib};t=a.controller;"@"==t&&(t=P[a.name]);b=v(t,b,!0,a.controllerAs);C[a.name]=b;A||X.data("$"+a.name+"Controller",b.instance);$[a.name]=b}));if(Q){G.$$addScopeInfo(X,E,!0,!(na&&(na===Q||na===Q.$$originalDirective)));G.$$addScopeClass(X,!0);g=$&&$[Q.name];var ba=E;g&&g.identifier&&!0===Q.bindToController&&(ba=g.instance);r(E.$$isolateBindings=Q.$$isolateBindings,function(a,d){var e=a.attrName,f=a.optional,g,l,h,k;switch(a.mode){case "@":P.$observe(e,
function(a){ba[d]=a});P.$$observers[e].$$scope=c;P[e]&&(ba[d]=b(P[e])(c));break;case "=":if(f&&!P[e])break;l=F(P[e]);k=l.literal?ia:function(a,b){return a===b||a!==a&&b!==b};h=l.assign||function(){g=ba[d]=l(c);throw ma("nonassign",P[e],Q.name);};g=ba[d]=l(c);f=function(a){k(a,ba[d])||(k(a,g)?h(c,a=ba[d]):ba[d]=a);return g=a};f.$stateful=!0;f=a.collection?c.$watchCollection(P[e],f):c.$watch(F(P[e],f),null,l.literal);E.$on("$destroy",f);break;case "&":l=F(P[e]),ba[d]=function(a){return l(c,a)}}})}$&&
(r($,function(a){a()}),$=null);g=0;for(m=k.length;g<m;g++)s=k[g],Z(s,s.isolateScope?E:c,X,P,s.require&&M(s.directiveName,s.require,X,C),ib);var Wb=c;Q&&(Q.template||null===Q.templateUrl)&&(Wb=E);a&&a(Wb,f.childNodes,u,l);for(g=p.length-1;0<=g;g--)s=p[g],Z(s,s.isolateScope?E:c,X,P,s.require&&M(s.directiveName,s.require,X,C),ib)}m=m||{};for(var E=-Number.MAX_VALUE,C,O=m.controllerDirectives,$,Q=m.newIsolateScopeDirective,na=m.templateDirective,fa=m.nonTlbTranscludeDirective,la=!1,D=!1,A=m.hasElementTranscludeDirective,
w=e.$$element=B(d),J,da,V,hb=f,za,K=0,R=a.length;K<R;K++){J=a[K];var Pa=J.$$start,U=J.$$end;Pa&&(w=ba(d,Pa,U));V=u;if(E>J.priority)break;if(V=J.scope)J.templateUrl||(L(V)?(Oa("new/isolated scope",Q||C,J,w),Q=J):Oa("new/isolated scope",Q,J,w)),C=C||J;da=J.name;!J.templateUrl&&J.controller&&(V=J.controller,O=O||{},Oa("'"+da+"' controller",O[da],J,w),O[da]=J);if(V=J.transclude)la=!0,J.$$tlb||(Oa("transclusion",fa,J,w),fa=J),"element"==V?(A=!0,E=J.priority,V=w,w=e.$$element=B(W.createComment(" "+da+": "+
e[da]+" ")),d=w[0],T(g,$a.call(V,0),d),hb=G(V,f,E,l&&l.name,{nonTlbTranscludeDirective:fa})):(V=B(Ub(d)).contents(),w.empty(),hb=G(V,f));if(J.template)if(D=!0,Oa("template",na,J,w),na=J,V=z(J.template)?J.template(w,e):J.template,V=Sc(V),J.replace){l=J;V=Sb.test(V)?Tc(Xb(J.templateNamespace,N(V))):[];d=V[0];if(1!=V.length||d.nodeType!==qa)throw ma("tplrt",da,"");T(g,w,d);R={$attr:{}};V=X(d,[],R);var aa=a.splice(K+1,a.length-(K+1));Q&&y(V);a=a.concat(V).concat(aa);S(e,R);R=a.length}else w.html(V);if(J.templateUrl)D=
!0,Oa("template",na,J,w),na=J,J.replace&&(l=J),I=of(a.splice(K,a.length-K),w,e,g,la&&hb,k,p,{controllerDirectives:O,newIsolateScopeDirective:Q,templateDirective:na,nonTlbTranscludeDirective:fa}),R=a.length;else if(J.compile)try{za=J.compile(w,e,hb),z(za)?s(null,za,Pa,U):za&&s(za.pre,za.post,Pa,U)}catch(pf){c(pf,xa(w))}J.terminal&&(I.terminal=!0,E=Math.max(E,J.priority))}I.scope=C&&!0===C.scope;I.transcludeOnThisElement=la;I.elementTranscludeOnThisElement=A;I.templateOnThisElement=D;I.transclude=hb;
m.hasElementTranscludeDirective=A;return I}function y(a){for(var b=0,c=a.length;b<c;b++)a[b]=Ob(a[b],{$$isolateScope:!0})}function la(b,e,f,g,l,h,k){if(e===l)return null;l=null;if(d.hasOwnProperty(e)){var q;e=a.get(e+"Directive");for(var m=0,s=e.length;m<s;m++)try{q=e[m],(g===u||g>q.priority)&&-1!=q.restrict.indexOf(f)&&(h&&(q=Ob(q,{$$start:h,$$end:k})),b.push(q),l=q)}catch(I){c(I)}}return l}function D(b){if(d.hasOwnProperty(b))for(var c=a.get(b+"Directive"),e=0,f=c.length;e<f;e++)if(b=c[e],b.multiElement)return!0;
return!1}function S(a,b){var c=b.$attr,d=a.$attr,e=a.$$element;r(a,function(d,e){"$"!=e.charAt(0)&&(b[e]&&b[e]!==d&&(d+=("style"===e?";":" ")+b[e]),a.$set(e,d,!0,c[e]))});r(b,function(b,f){"class"==f?(E(e,b),a["class"]=(a["class"]?a["class"]+" ":"")+b):"style"==f?(e.attr("style",e.attr("style")+";"+b),a.style=(a.style?a.style+";":"")+b):"$"==f.charAt(0)||a.hasOwnProperty(f)||(a[f]=b,d[f]=c[f])})}function of(a,b,c,d,e,f,g,l){var h=[],k,q,p=b[0],m=a.shift(),t=Ob(m,{templateUrl:null,transclude:null,
replace:null,$$originalDirective:m}),I=z(m.templateUrl)?m.templateUrl(b,c):m.templateUrl,M=m.templateNamespace;b.empty();s(I).then(function(s){var F,v;s=Sc(s);if(m.replace){s=Sb.test(s)?Tc(Xb(M,N(s))):[];F=s[0];if(1!=s.length||F.nodeType!==qa)throw ma("tplrt",m.name,I);s={$attr:{}};T(d,b,F);var C=X(F,[],s);L(m.scope)&&y(C);a=C.concat(a);S(c,s)}else F=p,b.html(s);a.unshift(t);k=fa(a,F,c,e,b,m,f,g,l);r(d,function(a,c){a==F&&(d[c]=b[0])});for(q=O(b[0].childNodes,e);h.length;){s=h.shift();v=h.shift();
var G=h.shift(),P=h.shift(),C=b[0];if(!s.$$destroyed){if(v!==p){var $=v.className;l.hasElementTranscludeDirective&&m.replace||(C=Ub(F));T(G,B(v),C);E(B(C),$)}v=k.transcludeOnThisElement?Q(s,k.transclude,P):P;k(q,s,C,d,v)}}h=null});return function(a,b,c,d,e){a=e;b.$$destroyed||(h?h.push(b,c,d,a):(k.transcludeOnThisElement&&(a=Q(b,k.transclude,e)),k(q,b,c,d,a)))}}function da(a,b){var c=b.priority-a.priority;return 0!==c?c:a.name!==b.name?a.name<b.name?-1:1:a.index-b.index}function Oa(a,b,c,d){if(b)throw ma("multidir",
b.name,c.name,a,xa(d));}function za(a,c){var d=b(c,!0);d&&a.push({priority:0,compile:function(a){a=a.parent();var b=!!a.length;b&&G.$$addBindingClass(a);return function(a,c){var e=c.parent();b||G.$$addBindingClass(e);G.$$addBindingInfo(e,d.expressions);a.$watch(d,function(a){c[0].nodeValue=a})}}})}function Xb(a,b){a=K(a||"html");switch(a){case "svg":case "math":var c=W.createElement("div");c.innerHTML="<"+a+">"+b+"</"+a+">";return c.childNodes[0].childNodes;default:return b}}function R(a,b){if("srcdoc"==
b)return $.HTML;var c=wa(a);if("xlinkHref"==b||"form"==c&&"action"==b||"img"!=c&&("src"==b||"ngSrc"==b))return $.RESOURCE_URL}function Pa(a,c,d,e,f){var h=R(a,e);f=g[e]||f;var k=b(d,!0,h,f);if(k){if("multiple"===e&&"select"===wa(a))throw ma("selmulti",xa(a));c.push({priority:100,compile:function(){return{pre:function(a,c,g){c=g.$$observers||(g.$$observers={});if(l.test(e))throw ma("nodomevents");var p=g[e];p!==d&&(k=p&&b(p,!0,h,f),d=p);k&&(g[e]=k(a),(c[e]||(c[e]=[])).$$inter=!0,(g.$$observers&&g.$$observers[e].$$scope||
a).$watch(k,function(a,b){"class"===e&&a!=b?g.$updateClass(a,b):g.$set(e,a)}))}}}})}}function T(a,b,c){var d=b[0],e=b.length,f=d.parentNode,g,l;if(a)for(g=0,l=a.length;g<l;g++)if(a[g]==d){a[g++]=c;l=g+e-1;for(var h=a.length;g<h;g++,l++)l<h?a[g]=a[l]:delete a[g];a.length-=e-1;a.context===d&&(a.context=c);break}f&&f.replaceChild(c,d);a=W.createDocumentFragment();a.appendChild(d);B(c).data(B(d).data());ta?(Qb=!0,ta.cleanData([d])):delete B.cache[d[B.expando]];d=1;for(e=b.length;d<e;d++)f=b[d],B(f).remove(),
a.appendChild(f),delete b[d];b[0]=c;b.length=1}function Y(a,b){return w(function(){return a.apply(null,arguments)},a,b)}function Z(a,b,d,e,f,g){try{a(b,d,e,f,g)}catch(l){c(l,xa(d))}}var Yb=function(a,b){if(b){var c=Object.keys(b),d,e,f;d=0;for(e=c.length;d<e;d++)f=c[d],this[f]=b[f]}else this.$attr={};this.$$element=a};Yb.prototype={$normalize:ya,$addClass:function(a){a&&0<a.length&&I.addClass(this.$$element,a)},$removeClass:function(a){a&&0<a.length&&I.removeClass(this.$$element,a)},$updateClass:function(a,
b){var c=Uc(a,b);c&&c.length&&I.addClass(this.$$element,c);(c=Uc(b,a))&&c.length&&I.removeClass(this.$$element,c)},$set:function(a,b,d,e){var f=this.$$element[0],g=Mc(f,a),l=kf(f,a),f=a;g?(this.$$element.prop(a,b),e=g):l&&(this[l]=b,f=l);this[a]=b;e?this.$attr[a]=e:(e=this.$attr[a])||(this.$attr[a]=e=uc(a,"-"));g=wa(this.$$element);if("a"===g&&"href"===a||"img"===g&&"src"===a)this[a]=b=M(b,"src"===a);else if("img"===g&&"srcset"===a){for(var g="",l=N(b),h=/(\s+\d+x\s*,|\s+\d+w\s*,|\s+,|,\s+)/,h=/\s/.test(l)?
h:/(,)/,l=l.split(h),h=Math.floor(l.length/2),k=0;k<h;k++)var q=2*k,g=g+M(N(l[q]),!0),g=g+(" "+N(l[q+1]));l=N(l[2*k]).split(/\s/);g+=M(N(l[0]),!0);2===l.length&&(g+=" "+N(l[1]));this[a]=b=g}!1!==d&&(null===b||b===u?this.$$element.removeAttr(e):this.$$element.attr(e,b));(a=this.$$observers)&&r(a[f],function(a){try{a(b)}catch(d){c(d)}})},$observe:function(a,b){var c=this,d=c.$$observers||(c.$$observers=ja()),e=d[a]||(d[a]=[]);e.push(b);m.$evalAsync(function(){!e.$$inter&&c.hasOwnProperty(a)&&b(c[a])});
return function(){Ya(e,b)}}};var V=b.startSymbol(),na=b.endSymbol(),Sc="{{"==V||"}}"==na?ra:function(a){return a.replace(/\{\{/g,V).replace(/}}/g,na)},U=/^ngAttr[A-Z]/;G.$$addBindingInfo=k?function(a,b){var c=a.data("$binding")||[];H(b)?c=c.concat(b):c.push(b);a.data("$binding",c)}:A;G.$$addBindingClass=k?function(a){E(a,"ng-binding")}:A;G.$$addScopeInfo=k?function(a,b,c,d){a.data(c?d?"$isolateScopeNoTemplate":"$isolateScope":"$scope",b)}:A;G.$$addScopeClass=k?function(a,b){E(a,b?"ng-isolate-scope":
"ng-scope")}:A;return G}]}function ya(b){return fb(b.replace(Rc,""))}function Uc(b,a){var c="",d=b.split(/\s+/),e=a.split(/\s+/),f=0;a:for(;f<d.length;f++){for(var g=d[f],h=0;h<e.length;h++)if(g==e[h])continue a;c+=(0<c.length?" ":"")+g}return c}function Tc(b){b=B(b);var a=b.length;if(1>=a)return b;for(;a--;)8===b[a].nodeType&&qf.call(b,a,1);return b}function Fe(){var b={},a=!1,c=/^(\S+)(\s+as\s+(\w+))?$/;this.register=function(a,c){Ma(a,"controller");L(a)?w(b,a):b[a]=c};this.allowGlobals=function(){a=
!0};this.$get=["$injector","$window",function(d,e){function f(a,b,c,d){if(!a||!L(a.$scope))throw S("$controller")("noscp",d,b);a.$scope[b]=c}return function(g,h,l,k){var n,p,q;l=!0===l;k&&x(k)&&(q=k);if(x(g)){k=g.match(c);if(!k)throw rf("ctrlfmt",g);p=k[1];q=q||k[3];g=b.hasOwnProperty(p)?b[p]:wc(h.$scope,p,!0)||(a?wc(e,p,!0):u);La(g,p,!0)}if(l)return l=(H(g)?g[g.length-1]:g).prototype,n=Object.create(l||null),q&&f(h,q,n,p||g.name),w(function(){d.invoke(g,n,h,p);return n},{instance:n,identifier:q});
n=d.instantiate(g,h,p);q&&f(h,q,n,p||g.name);return n}}]}function Ge(){this.$get=["$window",function(b){return B(b.document)}]}function He(){this.$get=["$log",function(b){return function(a,c){b.error.apply(b,arguments)}}]}function Zb(b,a){if(x(b)){var c=b.replace(sf,"").trim();if(c){var d=a("Content-Type");(d=d&&0===d.indexOf(Vc))||(d=(d=c.match(tf))&&uf[d[0]].test(c));d&&(b=pc(c))}}return b}function Wc(b){var a=ja(),c,d,e;if(!b)return a;r(b.split("\n"),function(b){e=b.indexOf(":");c=K(N(b.substr(0,
e)));d=N(b.substr(e+1));c&&(a[c]=a[c]?a[c]+", "+d:d)});return a}function Xc(b){var a=L(b)?b:u;return function(c){a||(a=Wc(b));return c?(c=a[K(c)],void 0===c&&(c=null),c):a}}function Yc(b,a,c,d){if(z(d))return d(b,a,c);r(d,function(d){b=d(b,a,c)});return b}function Ke(){var b=this.defaults={transformResponse:[Zb],transformRequest:[function(a){return L(a)&&"[object File]"!==Ca.call(a)&&"[object Blob]"!==Ca.call(a)&&"[object FormData]"!==Ca.call(a)?ab(a):a}],headers:{common:{Accept:"application/json, text/plain, */*"},
post:sa($b),put:sa($b),patch:sa($b)},xsrfCookieName:"XSRF-TOKEN",xsrfHeaderName:"X-XSRF-TOKEN"},a=!1;this.useApplyAsync=function(b){return y(b)?(a=!!b,this):a};var c=this.interceptors=[];this.$get=["$httpBackend","$browser","$cacheFactory","$rootScope","$q","$injector",function(d,e,f,g,h,l){function k(a){function c(a){var b=w({},a);b.data=a.data?Yc(a.data,a.headers,a.status,e.transformResponse):a.data;a=a.status;return 200<=a&&300>a?b:h.reject(b)}function d(a){var b,c={};r(a,function(a,d){z(a)?(b=
a(),null!=b&&(c[d]=b)):c[d]=a});return c}if(!ca.isObject(a))throw S("$http")("badreq",a);var e=w({method:"get",transformRequest:b.transformRequest,transformResponse:b.transformResponse},a);e.headers=function(a){var c=b.headers,e=w({},a.headers),f,g,c=w({},c.common,c[K(a.method)]);a:for(f in c){a=K(f);for(g in e)if(K(g)===a)continue a;e[f]=c[f]}return d(e)}(a);e.method=vb(e.method);var f=[function(a){var d=a.headers,e=Yc(a.data,Xc(d),u,a.transformRequest);D(e)&&r(d,function(a,b){"content-type"===K(b)&&
delete d[b]});D(a.withCredentials)&&!D(b.withCredentials)&&(a.withCredentials=b.withCredentials);return n(a,e).then(c,c)},u],g=h.when(e);for(r(t,function(a){(a.request||a.requestError)&&f.unshift(a.request,a.requestError);(a.response||a.responseError)&&f.push(a.response,a.responseError)});f.length;){a=f.shift();var l=f.shift(),g=g.then(a,l)}g.success=function(a){La(a,"fn");g.then(function(b){a(b.data,b.status,b.headers,e)});return g};g.error=function(a){La(a,"fn");g.then(null,function(b){a(b.data,
b.status,b.headers,e)});return g};return g}function n(c,f){function l(b,c,d,e){function f(){m(c,b,d,e)}E&&(200<=b&&300>b?E.put(Q,[b,c,Wc(d),e]):E.remove(Q));a?g.$applyAsync(f):(f(),g.$$phase||g.$apply())}function m(a,b,d,e){b=-1<=b?b:0;(200<=b&&300>b?I.resolve:I.reject)({data:a,status:b,headers:Xc(d),config:c,statusText:e})}function n(a){m(a.data,a.status,sa(a.headers()),a.statusText)}function t(){var a=k.pendingRequests.indexOf(c);-1!==a&&k.pendingRequests.splice(a,1)}var I=h.defer(),M=I.promise,
E,G,O=c.headers,Q=p(c.url,c.params);k.pendingRequests.push(c);M.then(t,t);!c.cache&&!b.cache||!1===c.cache||"GET"!==c.method&&"JSONP"!==c.method||(E=L(c.cache)?c.cache:L(b.cache)?b.cache:q);E&&(G=E.get(Q),y(G)?G&&z(G.then)?G.then(n,n):H(G)?m(G[1],G[0],sa(G[2]),G[3]):m(G,200,{},"OK"):E.put(Q,M));D(G)&&((G=Zc(c.url)?e.cookies()[c.xsrfCookieName||b.xsrfCookieName]:u)&&(O[c.xsrfHeaderName||b.xsrfHeaderName]=G),d(c.method,Q,f,l,O,c.timeout,c.withCredentials,c.responseType));return M}function p(a,b){if(!b)return a;
var c=[];Ed(b,function(a,b){null===a||D(a)||(H(a)||(a=[a]),r(a,function(a){L(a)&&(a=ha(a)?a.toISOString():ab(a));c.push(Ea(b)+"="+Ea(a))}))});0<c.length&&(a+=(-1==a.indexOf("?")?"?":"&")+c.join("&"));return a}var q=f("$http"),t=[];r(c,function(a){t.unshift(x(a)?l.get(a):l.invoke(a))});k.pendingRequests=[];(function(a){r(arguments,function(a){k[a]=function(b,c){return k(w(c||{},{method:a,url:b}))}})})("get","delete","head","jsonp");(function(a){r(arguments,function(a){k[a]=function(b,c,d){return k(w(d||
{},{method:a,url:b,data:c}))}})})("post","put","patch");k.defaults=b;return k}]}function vf(){return new R.XMLHttpRequest}function Le(){this.$get=["$browser","$window","$document",function(b,a,c){return wf(b,vf,b.defer,a.angular.callbacks,c[0])}]}function wf(b,a,c,d,e){function f(a,b,c){var f=e.createElement("script"),n=null;f.type="text/javascript";f.src=a;f.async=!0;n=function(a){f.removeEventListener("load",n,!1);f.removeEventListener("error",n,!1);e.body.removeChild(f);f=null;var g=-1,t="unknown";
a&&("load"!==a.type||d[b].called||(a={type:"error"}),t=a.type,g="error"===a.type?404:200);c&&c(g,t)};f.addEventListener("load",n,!1);f.addEventListener("error",n,!1);e.body.appendChild(f);return n}return function(e,h,l,k,n,p,q,t){function s(){m&&m();C&&C.abort()}function F(a,d,e,f,g){I!==u&&c.cancel(I);m=C=null;a(d,e,f,g);b.$$completeOutstandingRequest(A)}b.$$incOutstandingRequestCount();h=h||b.url();if("jsonp"==K(e)){var v="_"+(d.counter++).toString(36);d[v]=function(a){d[v].data=a;d[v].called=!0};
var m=f(h.replace("JSON_CALLBACK","angular.callbacks."+v),v,function(a,b){F(k,a,d[v].data,"",b);d[v]=A})}else{var C=a();C.open(e,h,!0);r(n,function(a,b){y(a)&&C.setRequestHeader(b,a)});C.onload=function(){var a=C.statusText||"",b="response"in C?C.response:C.responseText,c=1223===C.status?204:C.status;0===c&&(c=b?200:"file"==Aa(h).protocol?404:0);F(k,c,b,C.getAllResponseHeaders(),a)};e=function(){F(k,-1,null,null,"")};C.onerror=e;C.onabort=e;q&&(C.withCredentials=!0);if(t)try{C.responseType=t}catch($){if("json"!==
t)throw $;}C.send(l||null)}if(0<p)var I=c(s,p);else p&&z(p.then)&&p.then(s)}}function Ie(){var b="{{",a="}}";this.startSymbol=function(a){return a?(b=a,this):b};this.endSymbol=function(b){return b?(a=b,this):a};this.$get=["$parse","$exceptionHandler","$sce",function(c,d,e){function f(a){return"\\\\\\"+a}function g(f,g,t,s){function F(c){return c.replace(k,b).replace(n,a)}function v(a){try{var b=a;a=t?e.getTrusted(t,b):e.valueOf(b);var c;if(s&&!y(a))c=a;else if(null==a)c="";else{switch(typeof a){case "string":break;
case "number":a=""+a;break;default:a=ab(a)}c=a}return c}catch(g){c=ac("interr",f,g.toString()),d(c)}}s=!!s;for(var m,C,r=0,I=[],M=[],E=f.length,G=[],O=[];r<E;)if(-1!=(m=f.indexOf(b,r))&&-1!=(C=f.indexOf(a,m+h)))r!==m&&G.push(F(f.substring(r,m))),r=f.substring(m+h,C),I.push(r),M.push(c(r,v)),r=C+l,O.push(G.length),G.push("");else{r!==E&&G.push(F(f.substring(r)));break}if(t&&1<G.length)throw ac("noconcat",f);if(!g||I.length){var Q=function(a){for(var b=0,c=I.length;b<c;b++){if(s&&D(a[b]))return;G[O[b]]=
a[b]}return G.join("")};return w(function(a){var b=0,c=I.length,e=Array(c);try{for(;b<c;b++)e[b]=M[b](a);return Q(e)}catch(g){a=ac("interr",f,g.toString()),d(a)}},{exp:f,expressions:I,$$watchDelegate:function(a,b,c){var d;return a.$watchGroup(M,function(c,e){var f=Q(c);z(b)&&b.call(this,f,c!==e?d:f,a);d=f},c)}})}}var h=b.length,l=a.length,k=new RegExp(b.replace(/./g,f),"g"),n=new RegExp(a.replace(/./g,f),"g");g.startSymbol=function(){return b};g.endSymbol=function(){return a};return g}]}function Je(){this.$get=
["$rootScope","$window","$q","$$q",function(b,a,c,d){function e(e,h,l,k){var n=a.setInterval,p=a.clearInterval,q=0,t=y(k)&&!k,s=(t?d:c).defer(),F=s.promise;l=y(l)?l:0;F.then(null,null,e);F.$$intervalId=n(function(){s.notify(q++);0<l&&q>=l&&(s.resolve(q),p(F.$$intervalId),delete f[F.$$intervalId]);t||b.$apply()},h);f[F.$$intervalId]=s;return F}var f={};e.cancel=function(b){return b&&b.$$intervalId in f?(f[b.$$intervalId].reject("canceled"),a.clearInterval(b.$$intervalId),delete f[b.$$intervalId],!0):
!1};return e}]}function Rd(){this.$get=function(){return{id:"en-us",NUMBER_FORMATS:{DECIMAL_SEP:".",GROUP_SEP:",",PATTERNS:[{minInt:1,minFrac:0,maxFrac:3,posPre:"",posSuf:"",negPre:"-",negSuf:"",gSize:3,lgSize:3},{minInt:1,minFrac:2,maxFrac:2,posPre:"\u00a4",posSuf:"",negPre:"(\u00a4",negSuf:")",gSize:3,lgSize:3}],CURRENCY_SYM:"$"},DATETIME_FORMATS:{MONTH:"January February March April May June July August September October November December".split(" "),SHORTMONTH:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
DAY:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),SHORTDAY:"Sun Mon Tue Wed Thu Fri Sat".split(" "),AMPMS:["AM","PM"],medium:"MMM d, y h:mm:ss a","short":"M/d/yy h:mm a",fullDate:"EEEE, MMMM d, y",longDate:"MMMM d, y",mediumDate:"MMM d, y",shortDate:"M/d/yy",mediumTime:"h:mm:ss a",shortTime:"h:mm a",ERANAMES:["Before Christ","Anno Domini"],ERAS:["BC","AD"]},pluralCat:function(b){return 1===b?"one":"other"}}}}function bc(b){b=b.split("/");for(var a=b.length;a--;)b[a]=sb(b[a]);
return b.join("/")}function $c(b,a){var c=Aa(b);a.$$protocol=c.protocol;a.$$host=c.hostname;a.$$port=aa(c.port)||xf[c.protocol]||null}function ad(b,a){var c="/"!==b.charAt(0);c&&(b="/"+b);var d=Aa(b);a.$$path=decodeURIComponent(c&&"/"===d.pathname.charAt(0)?d.pathname.substring(1):d.pathname);a.$$search=rc(d.search);a.$$hash=decodeURIComponent(d.hash);a.$$path&&"/"!=a.$$path.charAt(0)&&(a.$$path="/"+a.$$path)}function ua(b,a){if(0===a.indexOf(b))return a.substr(b.length)}function Ga(b){var a=b.indexOf("#");
return-1==a?b:b.substr(0,a)}function Gb(b){return b.replace(/(#.+)|#$/,"$1")}function cc(b,a,c){this.$$html5=!0;c=c||"";$c(b,this);this.$$parse=function(b){var c=ua(a,b);if(!x(c))throw Hb("ipthprfx",b,a);ad(c,this);this.$$path||(this.$$path="/");this.$$compose()};this.$$compose=function(){var b=Pb(this.$$search),c=this.$$hash?"#"+sb(this.$$hash):"";this.$$url=bc(this.$$path)+(b?"?"+b:"")+c;this.$$absUrl=a+this.$$url.substr(1)};this.$$parseLinkUrl=function(d,e){if(e&&"#"===e[0])return this.hash(e.slice(1)),
!0;var f,g;(f=ua(b,d))!==u?(g=f,g=(f=ua(c,f))!==u?a+(ua("/",f)||f):b+g):(f=ua(a,d))!==u?g=a+f:a==d+"/"&&(g=a);g&&this.$$parse(g);return!!g}}function dc(b,a,c){$c(b,this);this.$$parse=function(d){var e=ua(b,d)||ua(a,d),f;D(e)||"#"!==e.charAt(0)?this.$$html5?f=e:(f="",D(e)&&(b=d,this.replace())):(f=ua(c,e),D(f)&&(f=e));ad(f,this);d=this.$$path;var e=b,g=/^\/[A-Z]:(\/.*)/;0===f.indexOf(e)&&(f=f.replace(e,""));g.exec(f)||(d=(f=g.exec(d))?f[1]:d);this.$$path=d;this.$$compose()};this.$$compose=function(){var a=
Pb(this.$$search),e=this.$$hash?"#"+sb(this.$$hash):"";this.$$url=bc(this.$$path)+(a?"?"+a:"")+e;this.$$absUrl=b+(this.$$url?c+this.$$url:"")};this.$$parseLinkUrl=function(a,c){return Ga(b)==Ga(a)?(this.$$parse(a),!0):!1}}function bd(b,a,c){this.$$html5=!0;dc.apply(this,arguments);this.$$parseLinkUrl=function(d,e){if(e&&"#"===e[0])return this.hash(e.slice(1)),!0;var f,g;b==Ga(d)?f=d:(g=ua(a,d))?f=b+c+g:a===d+"/"&&(f=a);f&&this.$$parse(f);return!!f};this.$$compose=function(){var a=Pb(this.$$search),
e=this.$$hash?"#"+sb(this.$$hash):"";this.$$url=bc(this.$$path)+(a?"?"+a:"")+e;this.$$absUrl=b+c+this.$$url}}function Ib(b){return function(){return this[b]}}function cd(b,a){return function(c){if(D(c))return this[b];this[b]=a(c);this.$$compose();return this}}function Me(){var b="",a={enabled:!1,requireBase:!0,rewriteLinks:!0};this.hashPrefix=function(a){return y(a)?(b=a,this):b};this.html5Mode=function(b){return Xa(b)?(a.enabled=b,this):L(b)?(Xa(b.enabled)&&(a.enabled=b.enabled),Xa(b.requireBase)&&
(a.requireBase=b.requireBase),Xa(b.rewriteLinks)&&(a.rewriteLinks=b.rewriteLinks),this):a};this.$get=["$rootScope","$browser","$sniffer","$rootElement","$window",function(c,d,e,f,g){function h(a,b,c){var e=k.url(),f=k.$$state;try{d.url(a,b,c),k.$$state=d.state()}catch(g){throw k.url(e),k.$$state=f,g;}}function l(a,b){c.$broadcast("$locationChangeSuccess",k.absUrl(),a,k.$$state,b)}var k,n;n=d.baseHref();var p=d.url(),q;if(a.enabled){if(!n&&a.requireBase)throw Hb("nobase");q=p.substring(0,p.indexOf("/",
p.indexOf("//")+2))+(n||"/");n=e.history?cc:bd}else q=Ga(p),n=dc;var t=q.substr(0,Ga(q).lastIndexOf("/")+1);k=new n(q,t,"#"+b);k.$$parseLinkUrl(p,p);k.$$state=d.state();var s=/^\s*(javascript|mailto):/i;f.on("click",function(b){if(a.rewriteLinks&&!b.ctrlKey&&!b.metaKey&&!b.shiftKey&&2!=b.which&&2!=b.button){for(var e=B(b.target);"a"!==wa(e[0]);)if(e[0]===f[0]||!(e=e.parent())[0])return;var l=e.prop("href"),h=e.attr("href")||e.attr("xlink:href");L(l)&&"[object SVGAnimatedString]"===l.toString()&&(l=
Aa(l.animVal).href);s.test(l)||!l||e.attr("target")||b.isDefaultPrevented()||!k.$$parseLinkUrl(l,h)||(b.preventDefault(),k.absUrl()!=d.url()&&(c.$apply(),g.angular["ff-684208-preventDefault"]=!0))}});Gb(k.absUrl())!=Gb(p)&&d.url(k.absUrl(),!0);var F=!0;d.onUrlChange(function(a,b){D(ua(t,a))?g.location.href=a:(c.$evalAsync(function(){var d=k.absUrl(),e=k.$$state,f;k.$$parse(a);k.$$state=b;f=c.$broadcast("$locationChangeStart",a,d,b,e).defaultPrevented;k.absUrl()===a&&(f?(k.$$parse(d),k.$$state=e,h(d,
!1,e)):(F=!1,l(d,e)))}),c.$$phase||c.$digest())});c.$watch(function(){var a=Gb(d.url()),b=Gb(k.absUrl()),f=d.state(),g=k.$$replace,q=a!==b||k.$$html5&&e.history&&f!==k.$$state;if(F||q)F=!1,c.$evalAsync(function(){var b=k.absUrl(),d=c.$broadcast("$locationChangeStart",b,a,k.$$state,f).defaultPrevented;k.absUrl()===b&&(d?(k.$$parse(a),k.$$state=f):(q&&h(b,g,f===k.$$state?null:k.$$state),l(a,f)))});k.$$replace=!1});return k}]}function Ne(){var b=!0,a=this;this.debugEnabled=function(a){return y(a)?(b=
a,this):b};this.$get=["$window",function(c){function d(a){a instanceof Error&&(a.stack?a=a.message&&-1===a.stack.indexOf(a.message)?"Error: "+a.message+"\n"+a.stack:a.stack:a.sourceURL&&(a=a.message+"\n"+a.sourceURL+":"+a.line));return a}function e(a){var b=c.console||{},e=b[a]||b.log||A;a=!1;try{a=!!e.apply}catch(l){}return a?function(){var a=[];r(arguments,function(b){a.push(d(b))});return e.apply(b,a)}:function(a,b){e(a,null==b?"":b)}}return{log:e("log"),info:e("info"),warn:e("warn"),error:e("error"),
debug:function(){var c=e("debug");return function(){b&&c.apply(a,arguments)}}()}}]}function va(b,a){if("__defineGetter__"===b||"__defineSetter__"===b||"__lookupGetter__"===b||"__lookupSetter__"===b||"__proto__"===b)throw ga("isecfld",a);return b}function dd(b,a){b+="";if(!x(b))throw ga("iseccst",a);return b}function oa(b,a){if(b){if(b.constructor===b)throw ga("isecfn",a);if(b.window===b)throw ga("isecwindow",a);if(b.children&&(b.nodeName||b.prop&&b.attr&&b.find))throw ga("isecdom",a);if(b===Object)throw ga("isecobj",
a);}return b}function ec(b){return b.constant}function jb(b,a,c,d,e){oa(b,e);oa(a,e);c=c.split(".");for(var f,g=0;1<c.length;g++){f=va(c.shift(),e);var h=0===g&&a&&a[f]||b[f];h||(h={},b[f]=h);b=oa(h,e)}f=va(c.shift(),e);oa(b[f],e);return b[f]=d}function Qa(b){return"constructor"==b}function ed(b,a,c,d,e,f,g){va(b,f);va(a,f);va(c,f);va(d,f);va(e,f);var h=function(a){return oa(a,f)},l=g||Qa(b)?h:ra,k=g||Qa(a)?h:ra,n=g||Qa(c)?h:ra,p=g||Qa(d)?h:ra,q=g||Qa(e)?h:ra;return function(f,g){var h=g&&g.hasOwnProperty(b)?
g:f;if(null==h)return h;h=l(h[b]);if(!a)return h;if(null==h)return u;h=k(h[a]);if(!c)return h;if(null==h)return u;h=n(h[c]);if(!d)return h;if(null==h)return u;h=p(h[d]);return e?null==h?u:h=q(h[e]):h}}function yf(b,a){return function(c,d){return b(c,d,oa,a)}}function zf(b,a,c){var d=a.expensiveChecks,e=d?Af:Bf,f=e[b];if(f)return f;var g=b.split("."),h=g.length;if(a.csp)f=6>h?ed(g[0],g[1],g[2],g[3],g[4],c,d):function(a,b){var e=0,f;do f=ed(g[e++],g[e++],g[e++],g[e++],g[e++],c,d)(a,b),b=u,a=f;while(e<
h);return f};else{var l="";d&&(l+="s = eso(s, fe);\nl = eso(l, fe);\n");var k=d;r(g,function(a,b){va(a,c);var e=(b?"s":'((l&&l.hasOwnProperty("'+a+'"))?l:s)')+"."+a;if(d||Qa(a))e="eso("+e+", fe)",k=!0;l+="if(s == null) return undefined;\ns="+e+";\n"});l+="return s;";a=new Function("s","l","eso","fe",l);a.toString=ea(l);k&&(a=yf(a,c));f=a}f.sharedGetter=!0;f.assign=function(a,c,d){return jb(a,d,b,c,b)};return e[b]=f}function fc(b){return z(b.valueOf)?b.valueOf():Cf.call(b)}function Oe(){var b=ja(),
a=ja();this.$get=["$filter","$sniffer",function(c,d){function e(a){var b=a;a.sharedGetter&&(b=function(b,c){return a(b,c)},b.literal=a.literal,b.constant=a.constant,b.assign=a.assign);return b}function f(a,b){for(var c=0,d=a.length;c<d;c++){var e=a[c];e.constant||(e.inputs?f(e.inputs,b):-1===b.indexOf(e)&&b.push(e))}return b}function g(a,b){return null==a||null==b?a===b:"object"===typeof a&&(a=fc(a),"object"===typeof a)?!1:a===b||a!==a&&b!==b}function h(a,b,c,d){var e=d.$$inputs||(d.$$inputs=f(d.inputs,
[])),l;if(1===e.length){var h=g,e=e[0];return a.$watch(function(a){var b=e(a);g(b,h)||(l=d(a),h=b&&fc(b));return l},b,c)}for(var k=[],q=0,p=e.length;q<p;q++)k[q]=g;return a.$watch(function(a){for(var b=!1,c=0,f=e.length;c<f;c++){var h=e[c](a);if(b||(b=!g(h,k[c])))k[c]=h&&fc(h)}b&&(l=d(a));return l},b,c)}function l(a,b,c,d){var e,f;return e=a.$watch(function(a){return d(a)},function(a,c,d){f=a;z(b)&&b.apply(this,arguments);y(a)&&d.$$postDigest(function(){y(f)&&e()})},c)}function k(a,b,c,d){function e(a){var b=
!0;r(a,function(a){y(a)||(b=!1)});return b}var f,g;return f=a.$watch(function(a){return d(a)},function(a,c,d){g=a;z(b)&&b.call(this,a,c,d);e(a)&&d.$$postDigest(function(){e(g)&&f()})},c)}function n(a,b,c,d){var e;return e=a.$watch(function(a){return d(a)},function(a,c,d){z(b)&&b.apply(this,arguments);e()},c)}function p(a,b){if(!b)return a;var c=a.$$watchDelegate,c=c!==k&&c!==l?function(c,d){var e=a(c,d);return b(e,c,d)}:function(c,d){var e=a(c,d),f=b(e,c,d);return y(e)?f:e};a.$$watchDelegate&&a.$$watchDelegate!==
h?c.$$watchDelegate=a.$$watchDelegate:b.$stateful||(c.$$watchDelegate=h,c.inputs=[a]);return c}var q={csp:d.csp,expensiveChecks:!1},t={csp:d.csp,expensiveChecks:!0};return function(d,f,g){var m,r,u;switch(typeof d){case "string":u=d=d.trim();var I=g?a:b;m=I[u];m||(":"===d.charAt(0)&&":"===d.charAt(1)&&(r=!0,d=d.substring(2)),g=g?t:q,m=new gc(g),m=(new kb(m,c,g)).parse(d),m.constant?m.$$watchDelegate=n:r?(m=e(m),m.$$watchDelegate=m.literal?k:l):m.inputs&&(m.$$watchDelegate=h),I[u]=m);return p(m,f);
case "function":return p(d,f);default:return p(A,f)}}}]}function Qe(){this.$get=["$rootScope","$exceptionHandler",function(b,a){return fd(function(a){b.$evalAsync(a)},a)}]}function Re(){this.$get=["$browser","$exceptionHandler",function(b,a){return fd(function(a){b.defer(a)},a)}]}function fd(b,a){function c(a,b,c){function d(b){return function(c){e||(e=!0,b.call(a,c))}}var e=!1;return[d(b),d(c)]}function d(){this.$$state={status:0}}function e(a,b){return function(c){b.call(a,c)}}function f(c){!c.processScheduled&&
c.pending&&(c.processScheduled=!0,b(function(){var b,d,e;e=c.pending;c.processScheduled=!1;c.pending=u;for(var f=0,g=e.length;f<g;++f){d=e[f][0];b=e[f][c.status];try{z(b)?d.resolve(b(c.value)):1===c.status?d.resolve(c.value):d.reject(c.value)}catch(l){d.reject(l),a(l)}}}))}function g(){this.promise=new d;this.resolve=e(this,this.resolve);this.reject=e(this,this.reject);this.notify=e(this,this.notify)}var h=S("$q",TypeError);d.prototype={then:function(a,b,c){var d=new g;this.$$state.pending=this.$$state.pending||
[];this.$$state.pending.push([d,a,b,c]);0<this.$$state.status&&f(this.$$state);return d.promise},"catch":function(a){return this.then(null,a)},"finally":function(a,b){return this.then(function(b){return k(b,!0,a)},function(b){return k(b,!1,a)},b)}};g.prototype={resolve:function(a){this.promise.$$state.status||(a===this.promise?this.$$reject(h("qcycle",a)):this.$$resolve(a))},$$resolve:function(b){var d,e;e=c(this,this.$$resolve,this.$$reject);try{if(L(b)||z(b))d=b&&b.then;z(d)?(this.promise.$$state.status=
-1,d.call(b,e[0],e[1],this.notify)):(this.promise.$$state.value=b,this.promise.$$state.status=1,f(this.promise.$$state))}catch(g){e[1](g),a(g)}},reject:function(a){this.promise.$$state.status||this.$$reject(a)},$$reject:function(a){this.promise.$$state.value=a;this.promise.$$state.status=2;f(this.promise.$$state)},notify:function(c){var d=this.promise.$$state.pending;0>=this.promise.$$state.status&&d&&d.length&&b(function(){for(var b,e,f=0,g=d.length;f<g;f++){e=d[f][0];b=d[f][3];try{e.notify(z(b)?
b(c):c)}catch(l){a(l)}}})}};var l=function(a,b){var c=new g;b?c.resolve(a):c.reject(a);return c.promise},k=function(a,b,c){var d=null;try{z(c)&&(d=c())}catch(e){return l(e,!1)}return d&&z(d.then)?d.then(function(){return l(a,b)},function(a){return l(a,!1)}):l(a,b)},n=function(a,b,c,d){var e=new g;e.resolve(a);return e.promise.then(b,c,d)},p=function t(a){if(!z(a))throw h("norslvr",a);if(!(this instanceof t))return new t(a);var b=new g;a(function(a){b.resolve(a)},function(a){b.reject(a)});return b.promise};
p.defer=function(){return new g};p.reject=function(a){var b=new g;b.reject(a);return b.promise};p.when=n;p.all=function(a){var b=new g,c=0,d=H(a)?[]:{};r(a,function(a,e){c++;n(a).then(function(a){d.hasOwnProperty(e)||(d[e]=a,--c||b.resolve(d))},function(a){d.hasOwnProperty(e)||b.reject(a)})});0===c&&b.resolve(d);return b.promise};return p}function $e(){this.$get=["$window","$timeout",function(b,a){function c(){for(var a=0;a<n.length;a++){var b=n[a];b&&(n[a]=null,b())}k=n.length=0}function d(a){var b=
n.length;k++;n.push(a);0===b&&(l=h(c));return function(){0<=b&&(b=n[b]=null,0===--k&&l&&(l(),l=null,n.length=0))}}var e=b.requestAnimationFrame||b.webkitRequestAnimationFrame,f=b.cancelAnimationFrame||b.webkitCancelAnimationFrame||b.webkitCancelRequestAnimationFrame,g=!!e,h=g?function(a){var b=e(a);return function(){f(b)}}:function(b){var c=a(b,16.66,!1);return function(){a.cancel(c)}};d.supported=g;var l,k=0,n=[];return d}]}function Pe(){function b(a){function b(){this.$$watchers=this.$$nextSibling=
this.$$childHead=this.$$childTail=null;this.$$listeners={};this.$$listenerCount={};this.$id=++rb;this.$$ChildScope=null}b.prototype=a;return b}var a=10,c=S("$rootScope"),d=null,e=null;this.digestTtl=function(b){arguments.length&&(a=b);return a};this.$get=["$injector","$exceptionHandler","$parse","$browser",function(f,g,h,l){function k(a){a.currentScope.$$destroyed=!0}function n(){this.$id=++rb;this.$$phase=this.$parent=this.$$watchers=this.$$nextSibling=this.$$prevSibling=this.$$childHead=this.$$childTail=
null;this.$root=this;this.$$destroyed=!1;this.$$listeners={};this.$$listenerCount={};this.$$isolateBindings=null}function p(a){if(v.$$phase)throw c("inprog",v.$$phase);v.$$phase=a}function q(a,b,c){do a.$$listenerCount[c]-=b,0===a.$$listenerCount[c]&&delete a.$$listenerCount[c];while(a=a.$parent)}function t(){}function s(){for(;u.length;)try{u.shift()()}catch(a){g(a)}e=null}function F(){null===e&&(e=l.defer(function(){v.$apply(s)}))}n.prototype={constructor:n,$new:function(a,c){var d;c=c||this;a?
(d=new n,d.$root=this.$root):(this.$$ChildScope||(this.$$ChildScope=b(this)),d=new this.$$ChildScope);d.$parent=c;d.$$prevSibling=c.$$childTail;c.$$childHead?(c.$$childTail.$$nextSibling=d,c.$$childTail=d):c.$$childHead=c.$$childTail=d;(a||c!=this)&&d.$on("$destroy",k);return d},$watch:function(a,b,c){var e=h(a);if(e.$$watchDelegate)return e.$$watchDelegate(this,b,c,e);var f=this.$$watchers,g={fn:b,last:t,get:e,exp:a,eq:!!c};d=null;z(b)||(g.fn=A);f||(f=this.$$watchers=[]);f.unshift(g);return function(){Ya(f,
g);d=null}},$watchGroup:function(a,b){function c(){l=!1;h?(h=!1,b(e,e,g)):b(e,d,g)}var d=Array(a.length),e=Array(a.length),f=[],g=this,l=!1,h=!0;if(!a.length){var k=!0;g.$evalAsync(function(){k&&b(e,e,g)});return function(){k=!1}}if(1===a.length)return this.$watch(a[0],function(a,c,f){e[0]=a;d[0]=c;b(e,a===c?e:d,f)});r(a,function(a,b){var h=g.$watch(a,function(a,f){e[b]=a;d[b]=f;l||(l=!0,g.$evalAsync(c))});f.push(h)});return function(){for(;f.length;)f.shift()()}},$watchCollection:function(a,b){function c(a){e=
a;var b,d,g,l;if(!D(e)){if(L(e))if(Ta(e))for(f!==p&&(f=p,t=f.length=0,k++),a=e.length,t!==a&&(k++,f.length=t=a),b=0;b<a;b++)l=f[b],g=e[b],d=l!==l&&g!==g,d||l===g||(k++,f[b]=g);else{f!==n&&(f=n={},t=0,k++);a=0;for(b in e)e.hasOwnProperty(b)&&(a++,g=e[b],l=f[b],b in f?(d=l!==l&&g!==g,d||l===g||(k++,f[b]=g)):(t++,f[b]=g,k++));if(t>a)for(b in k++,f)e.hasOwnProperty(b)||(t--,delete f[b])}else f!==e&&(f=e,k++);return k}}c.$stateful=!0;var d=this,e,f,g,l=1<b.length,k=0,q=h(a,c),p=[],n={},m=!0,t=0;return this.$watch(q,
function(){m?(m=!1,b(e,e,d)):b(e,g,d);if(l)if(L(e))if(Ta(e)){g=Array(e.length);for(var a=0;a<e.length;a++)g[a]=e[a]}else for(a in g={},e)sc.call(e,a)&&(g[a]=e[a]);else g=e})},$digest:function(){var b,f,h,k,q,n,r=a,F,P=[],u,y;p("$digest");l.$$checkUrlChange();this===v&&null!==e&&(l.defer.cancel(e),s());d=null;do{n=!1;for(F=this;m.length;){try{y=m.shift(),y.scope.$eval(y.expression,y.locals)}catch(w){g(w)}d=null}a:do{if(k=F.$$watchers)for(q=k.length;q--;)try{if(b=k[q])if((f=b.get(F))!==(h=b.last)&&
!(b.eq?ia(f,h):"number"===typeof f&&"number"===typeof h&&isNaN(f)&&isNaN(h)))n=!0,d=b,b.last=b.eq?Da(f,null):f,b.fn(f,h===t?f:h,F),5>r&&(u=4-r,P[u]||(P[u]=[]),P[u].push({msg:z(b.exp)?"fn: "+(b.exp.name||b.exp.toString()):b.exp,newVal:f,oldVal:h}));else if(b===d){n=!1;break a}}catch(D){g(D)}if(!(k=F.$$childHead||F!==this&&F.$$nextSibling))for(;F!==this&&!(k=F.$$nextSibling);)F=F.$parent}while(F=k);if((n||m.length)&&!r--)throw v.$$phase=null,c("infdig",a,P);}while(n||m.length);for(v.$$phase=null;C.length;)try{C.shift()()}catch(B){g(B)}},
$destroy:function(){if(!this.$$destroyed){var a=this.$parent;this.$broadcast("$destroy");this.$$destroyed=!0;if(this!==v){for(var b in this.$$listenerCount)q(this,this.$$listenerCount[b],b);a.$$childHead==this&&(a.$$childHead=this.$$nextSibling);a.$$childTail==this&&(a.$$childTail=this.$$prevSibling);this.$$prevSibling&&(this.$$prevSibling.$$nextSibling=this.$$nextSibling);this.$$nextSibling&&(this.$$nextSibling.$$prevSibling=this.$$prevSibling);this.$destroy=this.$digest=this.$apply=this.$evalAsync=
this.$applyAsync=A;this.$on=this.$watch=this.$watchGroup=function(){return A};this.$$listeners={};this.$parent=this.$$nextSibling=this.$$prevSibling=this.$$childHead=this.$$childTail=this.$root=this.$$watchers=null}}},$eval:function(a,b){return h(a)(this,b)},$evalAsync:function(a,b){v.$$phase||m.length||l.defer(function(){m.length&&v.$digest()});m.push({scope:this,expression:a,locals:b})},$$postDigest:function(a){C.push(a)},$apply:function(a){try{return p("$apply"),this.$eval(a)}catch(b){g(b)}finally{v.$$phase=
null;try{v.$digest()}catch(c){throw g(c),c;}}},$applyAsync:function(a){function b(){c.$eval(a)}var c=this;a&&u.push(b);F()},$on:function(a,b){var c=this.$$listeners[a];c||(this.$$listeners[a]=c=[]);c.push(b);var d=this;do d.$$listenerCount[a]||(d.$$listenerCount[a]=0),d.$$listenerCount[a]++;while(d=d.$parent);var e=this;return function(){var d=c.indexOf(b);-1!==d&&(c[d]=null,q(e,1,a))}},$emit:function(a,b){var c=[],d,e=this,f=!1,l={name:a,targetScope:e,stopPropagation:function(){f=!0},preventDefault:function(){l.defaultPrevented=
!0},defaultPrevented:!1},h=Za([l],arguments,1),k,q;do{d=e.$$listeners[a]||c;l.currentScope=e;k=0;for(q=d.length;k<q;k++)if(d[k])try{d[k].apply(null,h)}catch(p){g(p)}else d.splice(k,1),k--,q--;if(f)return l.currentScope=null,l;e=e.$parent}while(e);l.currentScope=null;return l},$broadcast:function(a,b){var c=this,d=this,e={name:a,targetScope:this,preventDefault:function(){e.defaultPrevented=!0},defaultPrevented:!1};if(!this.$$listenerCount[a])return e;for(var f=Za([e],arguments,1),l,h;c=d;){e.currentScope=
c;d=c.$$listeners[a]||[];l=0;for(h=d.length;l<h;l++)if(d[l])try{d[l].apply(null,f)}catch(k){g(k)}else d.splice(l,1),l--,h--;if(!(d=c.$$listenerCount[a]&&c.$$childHead||c!==this&&c.$$nextSibling))for(;c!==this&&!(d=c.$$nextSibling);)c=c.$parent}e.currentScope=null;return e}};var v=new n,m=v.$$asyncQueue=[],C=v.$$postDigestQueue=[],u=v.$$applyAsyncQueue=[];return v}]}function Sd(){var b=/^\s*(https?|ftp|mailto|tel|file):/,a=/^\s*((https?|ftp|file|blob):|data:image\/)/;this.aHrefSanitizationWhitelist=
function(a){return y(a)?(b=a,this):b};this.imgSrcSanitizationWhitelist=function(b){return y(b)?(a=b,this):a};this.$get=function(){return function(c,d){var e=d?a:b,f;f=Aa(c).href;return""===f||f.match(e)?c:"unsafe:"+f}}}function Df(b){if("self"===b)return b;if(x(b)){if(-1<b.indexOf("***"))throw Ba("iwcard",b);b=gd(b).replace("\\*\\*",".*").replace("\\*","[^:/.?&;]*");return new RegExp("^"+b+"$")}if(Va(b))return new RegExp("^"+b.source+"$");throw Ba("imatcher");}function hd(b){var a=[];y(b)&&r(b,function(b){a.push(Df(b))});
return a}function Te(){this.SCE_CONTEXTS=pa;var b=["self"],a=[];this.resourceUrlWhitelist=function(a){arguments.length&&(b=hd(a));return b};this.resourceUrlBlacklist=function(b){arguments.length&&(a=hd(b));return a};this.$get=["$injector",function(c){function d(a,b){return"self"===a?Zc(b):!!a.exec(b.href)}function e(a){var b=function(a){this.$$unwrapTrustedValue=function(){return a}};a&&(b.prototype=new a);b.prototype.valueOf=function(){return this.$$unwrapTrustedValue()};b.prototype.toString=function(){return this.$$unwrapTrustedValue().toString()};
return b}var f=function(a){throw Ba("unsafe");};c.has("$sanitize")&&(f=c.get("$sanitize"));var g=e(),h={};h[pa.HTML]=e(g);h[pa.CSS]=e(g);h[pa.URL]=e(g);h[pa.JS]=e(g);h[pa.RESOURCE_URL]=e(h[pa.URL]);return{trustAs:function(a,b){var c=h.hasOwnProperty(a)?h[a]:null;if(!c)throw Ba("icontext",a,b);if(null===b||b===u||""===b)return b;if("string"!==typeof b)throw Ba("itype",a);return new c(b)},getTrusted:function(c,e){if(null===e||e===u||""===e)return e;var g=h.hasOwnProperty(c)?h[c]:null;if(g&&e instanceof
g)return e.$$unwrapTrustedValue();if(c===pa.RESOURCE_URL){var g=Aa(e.toString()),p,q,t=!1;p=0;for(q=b.length;p<q;p++)if(d(b[p],g)){t=!0;break}if(t)for(p=0,q=a.length;p<q;p++)if(d(a[p],g)){t=!1;break}if(t)return e;throw Ba("insecurl",e.toString());}if(c===pa.HTML)return f(e);throw Ba("unsafe");},valueOf:function(a){return a instanceof g?a.$$unwrapTrustedValue():a}}}]}function Se(){var b=!0;this.enabled=function(a){arguments.length&&(b=!!a);return b};this.$get=["$parse","$sceDelegate",function(a,c){if(b&&
8>Ra)throw Ba("iequirks");var d=sa(pa);d.isEnabled=function(){return b};d.trustAs=c.trustAs;d.getTrusted=c.getTrusted;d.valueOf=c.valueOf;b||(d.trustAs=d.getTrusted=function(a,b){return b},d.valueOf=ra);d.parseAs=function(b,c){var e=a(c);return e.literal&&e.constant?e:a(c,function(a){return d.getTrusted(b,a)})};var e=d.parseAs,f=d.getTrusted,g=d.trustAs;r(pa,function(a,b){var c=K(b);d[fb("parse_as_"+c)]=function(b){return e(a,b)};d[fb("get_trusted_"+c)]=function(b){return f(a,b)};d[fb("trust_as_"+
c)]=function(b){return g(a,b)}});return d}]}function Ue(){this.$get=["$window","$document",function(b,a){var c={},d=aa((/android (\d+)/.exec(K((b.navigator||{}).userAgent))||[])[1]),e=/Boxee/i.test((b.navigator||{}).userAgent),f=a[0]||{},g,h=/^(Moz|webkit|ms)(?=[A-Z])/,l=f.body&&f.body.style,k=!1,n=!1;if(l){for(var p in l)if(k=h.exec(p)){g=k[0];g=g.substr(0,1).toUpperCase()+g.substr(1);break}g||(g="WebkitOpacity"in l&&"webkit");k=!!("transition"in l||g+"Transition"in l);n=!!("animation"in l||g+"Animation"in
l);!d||k&&n||(k=x(f.body.style.webkitTransition),n=x(f.body.style.webkitAnimation))}return{history:!(!b.history||!b.history.pushState||4>d||e),hasEvent:function(a){if("input"===a&&11>=Ra)return!1;if(D(c[a])){var b=f.createElement("div");c[a]="on"+a in b}return c[a]},csp:db(),vendorPrefix:g,transitions:k,animations:n,android:d}}]}function We(){this.$get=["$templateCache","$http","$q","$sce",function(b,a,c,d){function e(f,g){e.totalPendingRequests++;x(f)&&b.get(f)||(f=d.getTrustedResourceUrl(f));var h=
a.defaults&&a.defaults.transformResponse;H(h)?h=h.filter(function(a){return a!==Zb}):h===Zb&&(h=null);return a.get(f,{cache:b,transformResponse:h})["finally"](function(){e.totalPendingRequests--}).then(function(a){return a.data},function(a){if(!g)throw ma("tpload",f);return c.reject(a)})}e.totalPendingRequests=0;return e}]}function Xe(){this.$get=["$rootScope","$browser","$location",function(b,a,c){return{findBindings:function(a,b,c){a=a.getElementsByClassName("ng-binding");var g=[];r(a,function(a){var d=
ca.element(a).data("$binding");d&&r(d,function(d){c?(new RegExp("(^|\\s)"+gd(b)+"(\\s|\\||$)")).test(d)&&g.push(a):-1!=d.indexOf(b)&&g.push(a)})});return g},findModels:function(a,b,c){for(var g=["ng-","data-ng-","ng\\:"],h=0;h<g.length;++h){var l=a.querySelectorAll("["+g[h]+"model"+(c?"=":"*=")+'"'+b+'"]');if(l.length)return l}},getLocation:function(){return c.url()},setLocation:function(a){a!==c.url()&&(c.url(a),b.$digest())},whenStable:function(b){a.notifyWhenNoOutstandingRequests(b)}}}]}function Ye(){this.$get=
["$rootScope","$browser","$q","$$q","$exceptionHandler",function(b,a,c,d,e){function f(f,l,k){var n=y(k)&&!k,p=(n?d:c).defer(),q=p.promise;l=a.defer(function(){try{p.resolve(f())}catch(a){p.reject(a),e(a)}finally{delete g[q.$$timeoutId]}n||b.$apply()},l);q.$$timeoutId=l;g[l]=p;return q}var g={};f.cancel=function(b){return b&&b.$$timeoutId in g?(g[b.$$timeoutId].reject("canceled"),delete g[b.$$timeoutId],a.defer.cancel(b.$$timeoutId)):!1};return f}]}function Aa(b){Ra&&(Z.setAttribute("href",b),b=Z.href);
Z.setAttribute("href",b);return{href:Z.href,protocol:Z.protocol?Z.protocol.replace(/:$/,""):"",host:Z.host,search:Z.search?Z.search.replace(/^\?/,""):"",hash:Z.hash?Z.hash.replace(/^#/,""):"",hostname:Z.hostname,port:Z.port,pathname:"/"===Z.pathname.charAt(0)?Z.pathname:"/"+Z.pathname}}function Zc(b){b=x(b)?Aa(b):b;return b.protocol===id.protocol&&b.host===id.host}function Ze(){this.$get=ea(R)}function Ec(b){function a(c,d){if(L(c)){var e={};r(c,function(b,c){e[c]=a(c,b)});return e}return b.factory(c+
"Filter",d)}this.register=a;this.$get=["$injector",function(a){return function(b){return a.get(b+"Filter")}}];a("currency",jd);a("date",kd);a("filter",Ef);a("json",Ff);a("limitTo",Gf);a("lowercase",Hf);a("number",ld);a("orderBy",md);a("uppercase",If)}function Ef(){return function(b,a,c){if(!H(b))return b;var d;switch(null!==a?typeof a:"null"){case "function":break;case "boolean":case "null":case "number":case "string":d=!0;case "object":a=Jf(a,c,d);break;default:return b}return b.filter(a)}}function Jf(b,
a,c){var d=L(b)&&"$"in b;!0===a?a=ia:z(a)||(a=function(a,b){if(D(a))return!1;if(null===a||null===b)return a===b;if(L(a)||L(b))return!1;a=K(""+a);b=K(""+b);return-1!==a.indexOf(b)});return function(e){return d&&!L(e)?Ha(e,b.$,a,!1):Ha(e,b,a,c)}}function Ha(b,a,c,d,e){var f=null!==b?typeof b:"null",g=null!==a?typeof a:"null";if("string"===g&&"!"===a.charAt(0))return!Ha(b,a.substring(1),c,d);if(H(b))return b.some(function(b){return Ha(b,a,c,d)});switch(f){case "object":var h;if(d){for(h in b)if("$"!==
h.charAt(0)&&Ha(b[h],a,c,!0))return!0;return e?!1:Ha(b,a,c,!1)}if("object"===g){for(h in a)if(e=a[h],!z(e)&&!D(e)&&(f="$"===h,!Ha(f?b:b[h],e,c,f,f)))return!1;return!0}return c(b,a);case "function":return!1;default:return c(b,a)}}function jd(b){var a=b.NUMBER_FORMATS;return function(b,d,e){D(d)&&(d=a.CURRENCY_SYM);D(e)&&(e=a.PATTERNS[1].maxFrac);return null==b?b:nd(b,a.PATTERNS[1],a.GROUP_SEP,a.DECIMAL_SEP,e).replace(/\u00A4/g,d)}}function ld(b){var a=b.NUMBER_FORMATS;return function(b,d){return null==
b?b:nd(b,a.PATTERNS[0],a.GROUP_SEP,a.DECIMAL_SEP,d)}}function nd(b,a,c,d,e){if(!isFinite(b)||L(b))return"";var f=0>b;b=Math.abs(b);var g=b+"",h="",l=[],k=!1;if(-1!==g.indexOf("e")){var n=g.match(/([\d\.]+)e(-?)(\d+)/);n&&"-"==n[2]&&n[3]>e+1?b=0:(h=g,k=!0)}if(k)0<e&&1>b&&(h=b.toFixed(e),b=parseFloat(h));else{g=(g.split(od)[1]||"").length;D(e)&&(e=Math.min(Math.max(a.minFrac,g),a.maxFrac));b=+(Math.round(+(b.toString()+"e"+e)).toString()+"e"+-e);var g=(""+b).split(od),k=g[0],g=g[1]||"",p=0,q=a.lgSize,
t=a.gSize;if(k.length>=q+t)for(p=k.length-q,n=0;n<p;n++)0===(p-n)%t&&0!==n&&(h+=c),h+=k.charAt(n);for(n=p;n<k.length;n++)0===(k.length-n)%q&&0!==n&&(h+=c),h+=k.charAt(n);for(;g.length<e;)g+="0";e&&"0"!==e&&(h+=d+g.substr(0,e))}0===b&&(f=!1);l.push(f?a.negPre:a.posPre,h,f?a.negSuf:a.posSuf);return l.join("")}function Jb(b,a,c){var d="";0>b&&(d="-",b=-b);for(b=""+b;b.length<a;)b="0"+b;c&&(b=b.substr(b.length-a));return d+b}function U(b,a,c,d){c=c||0;return function(e){e=e["get"+b]();if(0<c||e>-c)e+=
c;0===e&&-12==c&&(e=12);return Jb(e,a,d)}}function Kb(b,a){return function(c,d){var e=c["get"+b](),f=vb(a?"SHORT"+b:b);return d[f][e]}}function pd(b){var a=(new Date(b,0,1)).getDay();return new Date(b,0,(4>=a?5:12)-a)}function qd(b){return function(a){var c=pd(a.getFullYear());a=+new Date(a.getFullYear(),a.getMonth(),a.getDate()+(4-a.getDay()))-+c;a=1+Math.round(a/6048E5);return Jb(a,b)}}function hc(b,a){return 0>=b.getFullYear()?a.ERAS[0]:a.ERAS[1]}function kd(b){function a(a){var b;if(b=a.match(c)){a=
new Date(0);var f=0,g=0,h=b[8]?a.setUTCFullYear:a.setFullYear,l=b[8]?a.setUTCHours:a.setHours;b[9]&&(f=aa(b[9]+b[10]),g=aa(b[9]+b[11]));h.call(a,aa(b[1]),aa(b[2])-1,aa(b[3]));f=aa(b[4]||0)-f;g=aa(b[5]||0)-g;h=aa(b[6]||0);b=Math.round(1E3*parseFloat("0."+(b[7]||0)));l.call(a,f,g,h,b)}return a}var c=/^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?::?(\d\d)(?::?(\d\d)(?:\.(\d+))?)?)?(Z|([+-])(\d\d):?(\d\d))?)?$/;return function(c,e,f){var g="",h=[],l,k;e=e||"mediumDate";e=b.DATETIME_FORMATS[e]||e;x(c)&&(c=Kf.test(c)?
aa(c):a(c));Y(c)&&(c=new Date(c));if(!ha(c))return c;for(;e;)(k=Lf.exec(e))?(h=Za(h,k,1),e=h.pop()):(h.push(e),e=null);f&&"UTC"===f&&(c=new Date(c.getTime()),c.setMinutes(c.getMinutes()+c.getTimezoneOffset()));r(h,function(a){l=Mf[a];g+=l?l(c,b.DATETIME_FORMATS):a.replace(/(^'|'$)/g,"").replace(/''/g,"'")});return g}}function Ff(){return function(b,a){D(a)&&(a=2);return ab(b,a)}}function Gf(){return function(b,a){Y(b)&&(b=b.toString());return H(b)||x(b)?(a=Infinity===Math.abs(Number(a))?Number(a):
aa(a))?0<a?b.slice(0,a):b.slice(a):x(b)?"":[]:b}}function md(b){return function(a,c,d){function e(a,b){return b?function(b,c){return a(c,b)}:a}function f(a){switch(typeof a){case "number":case "boolean":case "string":return!0;default:return!1}}function g(a){return null===a?"null":"function"===typeof a.valueOf&&(a=a.valueOf(),f(a))||"function"===typeof a.toString&&(a=a.toString(),f(a))?a:""}function h(a,b){var c=typeof a,d=typeof b;c===d&&"object"===c&&(a=g(a),b=g(b));return c===d?("string"===c&&(a=
a.toLowerCase(),b=b.toLowerCase()),a===b?0:a<b?-1:1):c<d?-1:1}if(!Ta(a))return a;c=H(c)?c:[c];0===c.length&&(c=["+"]);c=c.map(function(a){var c=!1,d=a||ra;if(x(a)){if("+"==a.charAt(0)||"-"==a.charAt(0))c="-"==a.charAt(0),a=a.substring(1);if(""===a)return e(h,c);d=b(a);if(d.constant){var f=d();return e(function(a,b){return h(a[f],b[f])},c)}}return e(function(a,b){return h(d(a),d(b))},c)});return $a.call(a).sort(e(function(a,b){for(var d=0;d<c.length;d++){var e=c[d](a,b);if(0!==e)return e}return 0},
d))}}function Ia(b){z(b)&&(b={link:b});b.restrict=b.restrict||"AC";return ea(b)}function rd(b,a,c,d,e){var f=this,g=[],h=f.$$parentForm=b.parent().controller("form")||Lb;f.$error={};f.$$success={};f.$pending=u;f.$name=e(a.name||a.ngForm||"")(c);f.$dirty=!1;f.$pristine=!0;f.$valid=!0;f.$invalid=!1;f.$submitted=!1;h.$addControl(f);f.$rollbackViewValue=function(){r(g,function(a){a.$rollbackViewValue()})};f.$commitViewValue=function(){r(g,function(a){a.$commitViewValue()})};f.$addControl=function(a){Ma(a.$name,
"input");g.push(a);a.$name&&(f[a.$name]=a)};f.$$renameControl=function(a,b){var c=a.$name;f[c]===a&&delete f[c];f[b]=a;a.$name=b};f.$removeControl=function(a){a.$name&&f[a.$name]===a&&delete f[a.$name];r(f.$pending,function(b,c){f.$setValidity(c,null,a)});r(f.$error,function(b,c){f.$setValidity(c,null,a)});r(f.$$success,function(b,c){f.$setValidity(c,null,a)});Ya(g,a)};sd({ctrl:this,$element:b,set:function(a,b,c){var d=a[b];d?-1===d.indexOf(c)&&d.push(c):a[b]=[c]},unset:function(a,b,c){var d=a[b];
d&&(Ya(d,c),0===d.length&&delete a[b])},parentForm:h,$animate:d});f.$setDirty=function(){d.removeClass(b,Sa);d.addClass(b,Mb);f.$dirty=!0;f.$pristine=!1;h.$setDirty()};f.$setPristine=function(){d.setClass(b,Sa,Mb+" ng-submitted");f.$dirty=!1;f.$pristine=!0;f.$submitted=!1;r(g,function(a){a.$setPristine()})};f.$setUntouched=function(){r(g,function(a){a.$setUntouched()})};f.$setSubmitted=function(){d.addClass(b,"ng-submitted");f.$submitted=!0;h.$setSubmitted()}}function ic(b){b.$formatters.push(function(a){return b.$isEmpty(a)?
a:a.toString()})}function lb(b,a,c,d,e,f){var g=K(a[0].type);if(!e.android){var h=!1;a.on("compositionstart",function(a){h=!0});a.on("compositionend",function(){h=!1;l()})}var l=function(b){k&&(f.defer.cancel(k),k=null);if(!h){var e=a.val();b=b&&b.type;"password"===g||c.ngTrim&&"false"===c.ngTrim||(e=N(e));(d.$viewValue!==e||""===e&&d.$$hasNativeValidators)&&d.$setViewValue(e,b)}};if(e.hasEvent("input"))a.on("input",l);else{var k,n=function(a,b,c){k||(k=f.defer(function(){k=null;b&&b.value===c||l(a)}))};
a.on("keydown",function(a){var b=a.keyCode;91===b||15<b&&19>b||37<=b&&40>=b||n(a,this,this.value)});if(e.hasEvent("paste"))a.on("paste cut",n)}a.on("change",l);d.$render=function(){a.val(d.$isEmpty(d.$viewValue)?"":d.$viewValue)}}function Nb(b,a){return function(c,d){var e,f;if(ha(c))return c;if(x(c)){'"'==c.charAt(0)&&'"'==c.charAt(c.length-1)&&(c=c.substring(1,c.length-1));if(Nf.test(c))return new Date(c);b.lastIndex=0;if(e=b.exec(c))return e.shift(),f=d?{yyyy:d.getFullYear(),MM:d.getMonth()+1,
dd:d.getDate(),HH:d.getHours(),mm:d.getMinutes(),ss:d.getSeconds(),sss:d.getMilliseconds()/1E3}:{yyyy:1970,MM:1,dd:1,HH:0,mm:0,ss:0,sss:0},r(e,function(b,c){c<a.length&&(f[a[c]]=+b)}),new Date(f.yyyy,f.MM-1,f.dd,f.HH,f.mm,f.ss||0,1E3*f.sss||0)}return NaN}}function mb(b,a,c,d){return function(e,f,g,h,l,k,n){function p(a){return a&&!(a.getTime&&a.getTime()!==a.getTime())}function q(a){return y(a)?ha(a)?a:c(a):u}td(e,f,g,h);lb(e,f,g,h,l,k);var t=h&&h.$options&&h.$options.timezone,s;h.$$parserName=b;
h.$parsers.push(function(b){return h.$isEmpty(b)?null:a.test(b)?(b=c(b,s),"UTC"===t&&b.setMinutes(b.getMinutes()-b.getTimezoneOffset()),b):u});h.$formatters.push(function(a){if(a&&!ha(a))throw nb("datefmt",a);if(p(a)){if((s=a)&&"UTC"===t){var b=6E4*s.getTimezoneOffset();s=new Date(s.getTime()+b)}return n("date")(a,d,t)}s=null;return""});if(y(g.min)||g.ngMin){var r;h.$validators.min=function(a){return!p(a)||D(r)||c(a)>=r};g.$observe("min",function(a){r=q(a);h.$validate()})}if(y(g.max)||g.ngMax){var v;
h.$validators.max=function(a){return!p(a)||D(v)||c(a)<=v};g.$observe("max",function(a){v=q(a);h.$validate()})}}}function td(b,a,c,d){(d.$$hasNativeValidators=L(a[0].validity))&&d.$parsers.push(function(b){var c=a.prop("validity")||{};return c.badInput&&!c.typeMismatch?u:b})}function ud(b,a,c,d,e){if(y(d)){b=b(d);if(!b.constant)throw nb("constexpr",c,d);return b(a)}return e}function jc(b,a){b="ngClass"+b;return["$animate",function(c){function d(a,b){var c=[],d=0;a:for(;d<a.length;d++){for(var e=a[d],
n=0;n<b.length;n++)if(e==b[n])continue a;c.push(e)}return c}function e(a){if(!H(a)){if(x(a))return a.split(" ");if(L(a)){var b=[];r(a,function(a,c){a&&(b=b.concat(c.split(" ")))});return b}}return a}return{restrict:"AC",link:function(f,g,h){function l(a,b){var c=g.data("$classCounts")||{},d=[];r(a,function(a){if(0<b||c[a])c[a]=(c[a]||0)+b,c[a]===+(0<b)&&d.push(a)});g.data("$classCounts",c);return d.join(" ")}function k(b){if(!0===a||f.$index%2===a){var k=e(b||[]);if(!n){var t=l(k,1);h.$addClass(t)}else if(!ia(b,
n)){var s=e(n),t=d(k,s),k=d(s,k),t=l(t,1),k=l(k,-1);t&&t.length&&c.addClass(g,t);k&&k.length&&c.removeClass(g,k)}}n=sa(b)}var n;f.$watch(h[b],k,!0);h.$observe("class",function(a){k(f.$eval(h[b]))});"ngClass"!==b&&f.$watch("$index",function(c,d){var g=c&1;if(g!==(d&1)){var k=e(f.$eval(h[b]));g===a?(g=l(k,1),h.$addClass(g)):(g=l(k,-1),h.$removeClass(g))}})}}}]}function sd(b){function a(a,b){b&&!f[a]?(k.addClass(e,a),f[a]=!0):!b&&f[a]&&(k.removeClass(e,a),f[a]=!1)}function c(b,c){b=b?"-"+uc(b,"-"):"";
a(ob+b,!0===c);a(vd+b,!1===c)}var d=b.ctrl,e=b.$element,f={},g=b.set,h=b.unset,l=b.parentForm,k=b.$animate;f[vd]=!(f[ob]=e.hasClass(ob));d.$setValidity=function(b,e,f){e===u?(d.$pending||(d.$pending={}),g(d.$pending,b,f)):(d.$pending&&h(d.$pending,b,f),wd(d.$pending)&&(d.$pending=u));Xa(e)?e?(h(d.$error,b,f),g(d.$$success,b,f)):(g(d.$error,b,f),h(d.$$success,b,f)):(h(d.$error,b,f),h(d.$$success,b,f));d.$pending?(a(xd,!0),d.$valid=d.$invalid=u,c("",null)):(a(xd,!1),d.$valid=wd(d.$error),d.$invalid=
!d.$valid,c("",d.$valid));e=d.$pending&&d.$pending[b]?u:d.$error[b]?!1:d.$$success[b]?!0:null;c(b,e);l.$setValidity(b,e,d)}}function wd(b){if(b)for(var a in b)return!1;return!0}var Of=/^\/(.+)\/([a-z]*)$/,K=function(b){return x(b)?b.toLowerCase():b},sc=Object.prototype.hasOwnProperty,vb=function(b){return x(b)?b.toUpperCase():b},Ra,B,ta,$a=[].slice,qf=[].splice,Pf=[].push,Ca=Object.prototype.toString,Ja=S("ng"),ca=R.angular||(R.angular={}),eb,rb=0;Ra=W.documentMode;A.$inject=[];ra.$inject=[];var H=
Array.isArray,N=function(b){return x(b)?b.trim():b},gd=function(b){return b.replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g,"\\$1").replace(/\x08/g,"\\x08")},db=function(){if(y(db.isActive_))return db.isActive_;var b=!(!W.querySelector("[ng-csp]")&&!W.querySelector("[data-ng-csp]"));if(!b)try{new Function("")}catch(a){b=!0}return db.isActive_=b},tb=["ng-","data-ng-","ng:","x-ng-"],Md=/[A-Z]/g,vc=!1,Qb,qa=1,bb=3,Qd={full:"1.3.20",major:1,minor:3,dot:20,codeName:"shallow-translucence"};T.expando="ng339";var Ab=
T.cache={},hf=1;T._data=function(b){return this.cache[b[this.expando]]||{}};var cf=/([\:\-\_]+(.))/g,df=/^moz([A-Z])/,Qf={mouseleave:"mouseout",mouseenter:"mouseover"},Tb=S("jqLite"),gf=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,Sb=/<|&#?\w+;/,ef=/<([\w:]+)/,ff=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,ka={option:[1,'<select multiple="multiple">',"</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],
td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};ka.optgroup=ka.option;ka.tbody=ka.tfoot=ka.colgroup=ka.caption=ka.thead;ka.th=ka.td;var Ka=T.prototype={ready:function(b){function a(){c||(c=!0,b())}var c=!1;"complete"===W.readyState?setTimeout(a):(this.on("DOMContentLoaded",a),T(R).on("load",a))},toString:function(){var b=[];r(this,function(a){b.push(""+a)});return"["+b.join(", ")+"]"},eq:function(b){return 0<=b?B(this[b]):B(this[this.length+b])},length:0,push:Pf,sort:[].sort,
splice:[].splice},Fb={};r("multiple selected checked disabled readOnly required open".split(" "),function(b){Fb[K(b)]=b});var Nc={};r("input select option textarea button form details".split(" "),function(b){Nc[b]=!0});var Oc={ngMinlength:"minlength",ngMaxlength:"maxlength",ngMin:"min",ngMax:"max",ngPattern:"pattern"};r({data:Vb,removeData:yb},function(b,a){T[a]=b});r({data:Vb,inheritedData:Eb,scope:function(b){return B.data(b,"$scope")||Eb(b.parentNode||b,["$isolateScope","$scope"])},isolateScope:function(b){return B.data(b,
"$isolateScope")||B.data(b,"$isolateScopeNoTemplate")},controller:Jc,injector:function(b){return Eb(b,"$injector")},removeAttr:function(b,a){b.removeAttribute(a)},hasClass:Bb,css:function(b,a,c){a=fb(a);if(y(c))b.style[a]=c;else return b.style[a]},attr:function(b,a,c){var d=b.nodeType;if(d!==bb&&2!==d&&8!==d)if(d=K(a),Fb[d])if(y(c))c?(b[a]=!0,b.setAttribute(a,d)):(b[a]=!1,b.removeAttribute(d));else return b[a]||(b.attributes.getNamedItem(a)||A).specified?d:u;else if(y(c))b.setAttribute(a,c);else if(b.getAttribute)return b=
b.getAttribute(a,2),null===b?u:b},prop:function(b,a,c){if(y(c))b[a]=c;else return b[a]},text:function(){function b(a,b){if(D(b)){var d=a.nodeType;return d===qa||d===bb?a.textContent:""}a.textContent=b}b.$dv="";return b}(),val:function(b,a){if(D(a)){if(b.multiple&&"select"===wa(b)){var c=[];r(b.options,function(a){a.selected&&c.push(a.value||a.text)});return 0===c.length?null:c}return b.value}b.value=a},html:function(b,a){if(D(a))return b.innerHTML;xb(b,!0);b.innerHTML=a},empty:Kc},function(b,a){T.prototype[a]=
function(a,d){var e,f,g=this.length;if(b!==Kc&&(2==b.length&&b!==Bb&&b!==Jc?a:d)===u){if(L(a)){for(e=0;e<g;e++)if(b===Vb)b(this[e],a);else for(f in a)b(this[e],f,a[f]);return this}e=b.$dv;g=e===u?Math.min(g,1):g;for(f=0;f<g;f++){var h=b(this[f],a,d);e=e?e+h:h}return e}for(e=0;e<g;e++)b(this[e],a,d);return this}});r({removeData:yb,on:function a(c,d,e,f){if(y(f))throw Tb("onargs");if(Fc(c)){var g=zb(c,!0);f=g.events;var h=g.handle;h||(h=g.handle=lf(c,f));for(var g=0<=d.indexOf(" ")?d.split(" "):[d],
l=g.length;l--;){d=g[l];var k=f[d];k||(f[d]=[],"mouseenter"===d||"mouseleave"===d?a(c,Qf[d],function(a){var c=a.relatedTarget;c&&(c===this||this.contains(c))||h(a,d)}):"$destroy"!==d&&c.addEventListener(d,h,!1),k=f[d]);k.push(e)}}},off:Ic,one:function(a,c,d){a=B(a);a.on(c,function f(){a.off(c,d);a.off(c,f)});a.on(c,d)},replaceWith:function(a,c){var d,e=a.parentNode;xb(a);r(new T(c),function(c){d?e.insertBefore(c,d.nextSibling):e.replaceChild(c,a);d=c})},children:function(a){var c=[];r(a.childNodes,
function(a){a.nodeType===qa&&c.push(a)});return c},contents:function(a){return a.contentDocument||a.childNodes||[]},append:function(a,c){var d=a.nodeType;if(d===qa||11===d){c=new T(c);for(var d=0,e=c.length;d<e;d++)a.appendChild(c[d])}},prepend:function(a,c){if(a.nodeType===qa){var d=a.firstChild;r(new T(c),function(c){a.insertBefore(c,d)})}},wrap:function(a,c){c=B(c).eq(0).clone()[0];var d=a.parentNode;d&&d.replaceChild(c,a);c.appendChild(a)},remove:Lc,detach:function(a){Lc(a,!0)},after:function(a,
c){var d=a,e=a.parentNode;c=new T(c);for(var f=0,g=c.length;f<g;f++){var h=c[f];e.insertBefore(h,d.nextSibling);d=h}},addClass:Db,removeClass:Cb,toggleClass:function(a,c,d){c&&r(c.split(" "),function(c){var f=d;D(f)&&(f=!Bb(a,c));(f?Db:Cb)(a,c)})},parent:function(a){return(a=a.parentNode)&&11!==a.nodeType?a:null},next:function(a){return a.nextElementSibling},find:function(a,c){return a.getElementsByTagName?a.getElementsByTagName(c):[]},clone:Ub,triggerHandler:function(a,c,d){var e,f,g=c.type||c,h=
zb(a);if(h=(h=h&&h.events)&&h[g])e={preventDefault:function(){this.defaultPrevented=!0},isDefaultPrevented:function(){return!0===this.defaultPrevented},stopImmediatePropagation:function(){this.immediatePropagationStopped=!0},isImmediatePropagationStopped:function(){return!0===this.immediatePropagationStopped},stopPropagation:A,type:g,target:a},c.type&&(e=w(e,c)),c=sa(h),f=d?[e].concat(d):[e],r(c,function(c){e.isImmediatePropagationStopped()||c.apply(a,f)})}},function(a,c){T.prototype[c]=function(c,
e,f){for(var g,h=0,l=this.length;h<l;h++)D(g)?(g=a(this[h],c,e,f),y(g)&&(g=B(g))):Hc(g,a(this[h],c,e,f));return y(g)?g:this};T.prototype.bind=T.prototype.on;T.prototype.unbind=T.prototype.off});gb.prototype={put:function(a,c){this[Na(a,this.nextUid)]=c},get:function(a){return this[Na(a,this.nextUid)]},remove:function(a){var c=this[a=Na(a,this.nextUid)];delete this[a];return c}};var Qc=/^function\s*[^\(]*\(\s*([^\)]*)\)/m,Rf=/,/,Sf=/^\s*(_?)(\S+?)\1\s*$/,Pc=/((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg,Fa=S("$injector");
cb.$$annotate=function(a,c,d){var e;if("function"===typeof a){if(!(e=a.$inject)){e=[];if(a.length){if(c)throw x(d)&&d||(d=a.name||mf(a)),Fa("strictdi",d);c=a.toString().replace(Pc,"");c=c.match(Qc);r(c[1].split(Rf),function(a){a.replace(Sf,function(a,c,d){e.push(d)})})}a.$inject=e}}else H(a)?(c=a.length-1,La(a[c],"fn"),e=a.slice(0,c)):La(a,"fn",!0);return e};var Tf=S("$animate"),Ce=["$provide",function(a){this.$$selectors={};this.register=function(c,d){var e=c+"-animation";if(c&&"."!=c.charAt(0))throw Tf("notcsel",
c);this.$$selectors[c.substr(1)]=e;a.factory(e,d)};this.classNameFilter=function(a){1===arguments.length&&(this.$$classNameFilter=a instanceof RegExp?a:null);return this.$$classNameFilter};this.$get=["$$q","$$asyncCallback","$rootScope",function(a,d,e){function f(d){var f,g=a.defer();g.promise.$$cancelFn=function(){f&&f()};e.$$postDigest(function(){f=d(function(){g.resolve()})});return g.promise}function g(a,c){var d=[],e=[],f=ja();r((a.attr("class")||"").split(/\s+/),function(a){f[a]=!0});r(c,function(a,
c){var g=f[c];!1===a&&g?e.push(c):!0!==a||g||d.push(c)});return 0<d.length+e.length&&[d.length?d:null,e.length?e:null]}function h(a,c,d){for(var e=0,f=c.length;e<f;++e)a[c[e]]=d}function l(){n||(n=a.defer(),d(function(){n.resolve();n=null}));return n.promise}function k(a,c){if(ca.isObject(c)){var d=w(c.from||{},c.to||{});a.css(d)}}var n;return{animate:function(a,c,d){k(a,{from:c,to:d});return l()},enter:function(a,c,d,e){k(a,e);d?d.after(a):c.prepend(a);return l()},leave:function(a,c){k(a,c);a.remove();
return l()},move:function(a,c,d,e){return this.enter(a,c,d,e)},addClass:function(a,c,d){return this.setClass(a,c,[],d)},$$addClassImmediately:function(a,c,d){a=B(a);c=x(c)?c:H(c)?c.join(" "):"";r(a,function(a){Db(a,c)});k(a,d);return l()},removeClass:function(a,c,d){return this.setClass(a,[],c,d)},$$removeClassImmediately:function(a,c,d){a=B(a);c=x(c)?c:H(c)?c.join(" "):"";r(a,function(a){Cb(a,c)});k(a,d);return l()},setClass:function(a,c,d,e){var k=this,l=!1;a=B(a);var m=a.data("$$animateClasses");
m?e&&m.options&&(m.options=ca.extend(m.options||{},e)):(m={classes:{},options:e},l=!0);e=m.classes;c=H(c)?c:c.split(" ");d=H(d)?d:d.split(" ");h(e,c,!0);h(e,d,!1);l&&(m.promise=f(function(c){var d=a.data("$$animateClasses");a.removeData("$$animateClasses");if(d){var e=g(a,d.classes);e&&k.$$setClassImmediately(a,e[0],e[1],d.options)}c()}),a.data("$$animateClasses",m));return m.promise},$$setClassImmediately:function(a,c,d,e){c&&this.$$addClassImmediately(a,c);d&&this.$$removeClassImmediately(a,d);
k(a,e);return l()},enabled:A,cancel:A}}]}],ma=S("$compile");xc.$inject=["$provide","$$sanitizeUriProvider"];var Rc=/^((?:x|data)[\:\-_])/i,rf=S("$controller"),Vc="application/json",$b={"Content-Type":Vc+";charset=utf-8"},tf=/^\[|^\{(?!\{)/,uf={"[":/]$/,"{":/}$/},sf=/^\)\]\}',?\n/,ac=S("$interpolate"),Uf=/^([^\?#]*)(\?([^#]*))?(#(.*))?$/,xf={http:80,https:443,ftp:21},Hb=S("$location"),Vf={$$html5:!1,$$replace:!1,absUrl:Ib("$$absUrl"),url:function(a){if(D(a))return this.$$url;var c=Uf.exec(a);(c[1]||
""===a)&&this.path(decodeURIComponent(c[1]));(c[2]||c[1]||""===a)&&this.search(c[3]||"");this.hash(c[5]||"");return this},protocol:Ib("$$protocol"),host:Ib("$$host"),port:Ib("$$port"),path:cd("$$path",function(a){a=null!==a?a.toString():"";return"/"==a.charAt(0)?a:"/"+a}),search:function(a,c){switch(arguments.length){case 0:return this.$$search;case 1:if(x(a)||Y(a))a=a.toString(),this.$$search=rc(a);else if(L(a))a=Da(a,{}),r(a,function(c,e){null==c&&delete a[e]}),this.$$search=a;else throw Hb("isrcharg");
break;default:D(c)||null===c?delete this.$$search[a]:this.$$search[a]=c}this.$$compose();return this},hash:cd("$$hash",function(a){return null!==a?a.toString():""}),replace:function(){this.$$replace=!0;return this}};r([bd,dc,cc],function(a){a.prototype=Object.create(Vf);a.prototype.state=function(c){if(!arguments.length)return this.$$state;if(a!==cc||!this.$$html5)throw Hb("nostate");this.$$state=D(c)?null:c;return this}});var ga=S("$parse"),Wf=Function.prototype.call,Xf=Function.prototype.apply,
Yf=Function.prototype.bind,pb=ja();r({"null":function(){return null},"true":function(){return!0},"false":function(){return!1},undefined:function(){}},function(a,c){a.constant=a.literal=a.sharedGetter=!0;pb[c]=a});pb["this"]=function(a){return a};pb["this"].sharedGetter=!0;var qb=w(ja(),{"+":function(a,c,d,e){d=d(a,c);e=e(a,c);return y(d)?y(e)?d+e:d:y(e)?e:u},"-":function(a,c,d,e){d=d(a,c);e=e(a,c);return(y(d)?d:0)-(y(e)?e:0)},"*":function(a,c,d,e){return d(a,c)*e(a,c)},"/":function(a,c,d,e){return d(a,
c)/e(a,c)},"%":function(a,c,d,e){return d(a,c)%e(a,c)},"===":function(a,c,d,e){return d(a,c)===e(a,c)},"!==":function(a,c,d,e){return d(a,c)!==e(a,c)},"==":function(a,c,d,e){return d(a,c)==e(a,c)},"!=":function(a,c,d,e){return d(a,c)!=e(a,c)},"<":function(a,c,d,e){return d(a,c)<e(a,c)},">":function(a,c,d,e){return d(a,c)>e(a,c)},"<=":function(a,c,d,e){return d(a,c)<=e(a,c)},">=":function(a,c,d,e){return d(a,c)>=e(a,c)},"&&":function(a,c,d,e){return d(a,c)&&e(a,c)},"||":function(a,c,d,e){return d(a,
c)||e(a,c)},"!":function(a,c,d){return!d(a,c)},"=":!0,"|":!0}),Zf={n:"\n",f:"\f",r:"\r",t:"\t",v:"\v","'":"'",'"':'"'},gc=function(a){this.options=a};gc.prototype={constructor:gc,lex:function(a){this.text=a;this.index=0;for(this.tokens=[];this.index<this.text.length;)if(a=this.text.charAt(this.index),'"'===a||"'"===a)this.readString(a);else if(this.isNumber(a)||"."===a&&this.isNumber(this.peek()))this.readNumber();else if(this.isIdent(a))this.readIdent();else if(this.is(a,"(){}[].,;:?"))this.tokens.push({index:this.index,
text:a}),this.index++;else if(this.isWhitespace(a))this.index++;else{var c=a+this.peek(),d=c+this.peek(2),e=qb[c],f=qb[d];qb[a]||e||f?(a=f?d:e?c:a,this.tokens.push({index:this.index,text:a,operator:!0}),this.index+=a.length):this.throwError("Unexpected next character ",this.index,this.index+1)}return this.tokens},is:function(a,c){return-1!==c.indexOf(a)},peek:function(a){a=a||1;return this.index+a<this.text.length?this.text.charAt(this.index+a):!1},isNumber:function(a){return"0"<=a&&"9">=a&&"string"===
typeof a},isWhitespace:function(a){return" "===a||"\r"===a||"\t"===a||"\n"===a||"\v"===a||"\u00a0"===a},isIdent:function(a){return"a"<=a&&"z">=a||"A"<=a&&"Z">=a||"_"===a||"$"===a},isExpOperator:function(a){return"-"===a||"+"===a||this.isNumber(a)},throwError:function(a,c,d){d=d||this.index;c=y(c)?"s "+c+"-"+this.index+" ["+this.text.substring(c,d)+"]":" "+d;throw ga("lexerr",a,c,this.text);},readNumber:function(){for(var a="",c=this.index;this.index<this.text.length;){var d=K(this.text.charAt(this.index));
if("."==d||this.isNumber(d))a+=d;else{var e=this.peek();if("e"==d&&this.isExpOperator(e))a+=d;else if(this.isExpOperator(d)&&e&&this.isNumber(e)&&"e"==a.charAt(a.length-1))a+=d;else if(!this.isExpOperator(d)||e&&this.isNumber(e)||"e"!=a.charAt(a.length-1))break;else this.throwError("Invalid exponent")}this.index++}this.tokens.push({index:c,text:a,constant:!0,value:Number(a)})},readIdent:function(){for(var a=this.index;this.index<this.text.length;){var c=this.text.charAt(this.index);if(!this.isIdent(c)&&
!this.isNumber(c))break;this.index++}this.tokens.push({index:a,text:this.text.slice(a,this.index),identifier:!0})},readString:function(a){var c=this.index;this.index++;for(var d="",e=a,f=!1;this.index<this.text.length;){var g=this.text.charAt(this.index),e=e+g;if(f)"u"===g?(f=this.text.substring(this.index+1,this.index+5),f.match(/[\da-f]{4}/i)||this.throwError("Invalid unicode escape [\\u"+f+"]"),this.index+=4,d+=String.fromCharCode(parseInt(f,16))):d+=Zf[g]||g,f=!1;else if("\\"===g)f=!0;else{if(g===
a){this.index++;this.tokens.push({index:c,text:e,constant:!0,value:d});return}d+=g}this.index++}this.throwError("Unterminated quote",c)}};var kb=function(a,c,d){this.lexer=a;this.$filter=c;this.options=d};kb.ZERO=w(function(){return 0},{sharedGetter:!0,constant:!0});kb.prototype={constructor:kb,parse:function(a){this.text=a;this.tokens=this.lexer.lex(a);a=this.statements();0!==this.tokens.length&&this.throwError("is an unexpected token",this.tokens[0]);a.literal=!!a.literal;a.constant=!!a.constant;
return a},primary:function(){var a;this.expect("(")?(a=this.filterChain(),this.consume(")")):this.expect("[")?a=this.arrayDeclaration():this.expect("{")?a=this.object():this.peek().identifier&&this.peek().text in pb?a=pb[this.consume().text]:this.peek().identifier?a=this.identifier():this.peek().constant?a=this.constant():this.throwError("not a primary expression",this.peek());for(var c,d;c=this.expect("(","[",".");)"("===c.text?(a=this.functionCall(a,d),d=null):"["===c.text?(d=a,a=this.objectIndex(a)):
"."===c.text?(d=a,a=this.fieldAccess(a)):this.throwError("IMPOSSIBLE");return a},throwError:function(a,c){throw ga("syntax",c.text,a,c.index+1,this.text,this.text.substring(c.index));},peekToken:function(){if(0===this.tokens.length)throw ga("ueoe",this.text);return this.tokens[0]},peek:function(a,c,d,e){return this.peekAhead(0,a,c,d,e)},peekAhead:function(a,c,d,e,f){if(this.tokens.length>a){a=this.tokens[a];var g=a.text;if(g===c||g===d||g===e||g===f||!(c||d||e||f))return a}return!1},expect:function(a,
c,d,e){return(a=this.peek(a,c,d,e))?(this.tokens.shift(),a):!1},consume:function(a){if(0===this.tokens.length)throw ga("ueoe",this.text);var c=this.expect(a);c||this.throwError("is unexpected, expecting ["+a+"]",this.peek());return c},unaryFn:function(a,c){var d=qb[a];return w(function(a,f){return d(a,f,c)},{constant:c.constant,inputs:[c]})},binaryFn:function(a,c,d,e){var f=qb[c];return w(function(c,e){return f(c,e,a,d)},{constant:a.constant&&d.constant,inputs:!e&&[a,d]})},identifier:function(){for(var a=
this.consume().text;this.peek(".")&&this.peekAhead(1).identifier&&!this.peekAhead(2,"(");)a+=this.consume().text+this.consume().text;return zf(a,this.options,this.text)},constant:function(){var a=this.consume().value;return w(function(){return a},{constant:!0,literal:!0})},statements:function(){for(var a=[];;)if(0<this.tokens.length&&!this.peek("}",")",";","]")&&a.push(this.filterChain()),!this.expect(";"))return 1===a.length?a[0]:function(c,d){for(var e,f=0,g=a.length;f<g;f++)e=a[f](c,d);return e}},
filterChain:function(){for(var a=this.expression();this.expect("|");)a=this.filter(a);return a},filter:function(a){var c=this.$filter(this.consume().text),d,e;if(this.peek(":"))for(d=[],e=[];this.expect(":");)d.push(this.expression());var f=[a].concat(d||[]);return w(function(f,h){var l=a(f,h);if(e){e[0]=l;for(l=d.length;l--;)e[l+1]=d[l](f,h);return c.apply(u,e)}return c(l)},{constant:!c.$stateful&&f.every(ec),inputs:!c.$stateful&&f})},expression:function(){return this.assignment()},assignment:function(){var a=
this.ternary(),c,d;return(d=this.expect("="))?(a.assign||this.throwError("implies assignment but ["+this.text.substring(0,d.index)+"] can not be assigned to",d),c=this.ternary(),w(function(d,f){return a.assign(d,c(d,f),f)},{inputs:[a,c]})):a},ternary:function(){var a=this.logicalOR(),c;if(this.expect("?")&&(c=this.assignment(),this.consume(":"))){var d=this.assignment();return w(function(e,f){return a(e,f)?c(e,f):d(e,f)},{constant:a.constant&&c.constant&&d.constant})}return a},logicalOR:function(){for(var a=
this.logicalAND(),c;c=this.expect("||");)a=this.binaryFn(a,c.text,this.logicalAND(),!0);return a},logicalAND:function(){for(var a=this.equality(),c;c=this.expect("&&");)a=this.binaryFn(a,c.text,this.equality(),!0);return a},equality:function(){for(var a=this.relational(),c;c=this.expect("==","!=","===","!==");)a=this.binaryFn(a,c.text,this.relational());return a},relational:function(){for(var a=this.additive(),c;c=this.expect("<",">","<=",">=");)a=this.binaryFn(a,c.text,this.additive());return a},
additive:function(){for(var a=this.multiplicative(),c;c=this.expect("+","-");)a=this.binaryFn(a,c.text,this.multiplicative());return a},multiplicative:function(){for(var a=this.unary(),c;c=this.expect("*","/","%");)a=this.binaryFn(a,c.text,this.unary());return a},unary:function(){var a;return this.expect("+")?this.primary():(a=this.expect("-"))?this.binaryFn(kb.ZERO,a.text,this.unary()):(a=this.expect("!"))?this.unaryFn(a.text,this.unary()):this.primary()},fieldAccess:function(a){var c=this.identifier();
return w(function(d,e,f){d=f||a(d,e);return null==d?u:c(d)},{assign:function(d,e,f){var g=a(d,f);g||a.assign(d,g={},f);return c.assign(g,e)}})},objectIndex:function(a){var c=this.text,d=this.expression();this.consume("]");return w(function(e,f){var g=a(e,f),h=dd(d(e,f),c);va(h,c);return g?oa(g[h],c):u},{assign:function(e,f,g){var h=va(dd(d(e,g),c),c),l=oa(a(e,g),c);l||a.assign(e,l={},g);return l[h]=f}})},functionCall:function(a,c){var d=[];if(")"!==this.peekToken().text){do d.push(this.expression());
while(this.expect(","))}this.consume(")");var e=this.text,f=d.length?[]:null;return function(g,h){var l=c?c(g,h):y(c)?u:g,k=a(g,h,l)||A;if(f)for(var n=d.length;n--;)f[n]=oa(d[n](g,h),e);oa(l,e);if(k){if(k.constructor===k)throw ga("isecfn",e);if(k===Wf||k===Xf||k===Yf)throw ga("isecff",e);}l=k.apply?k.apply(l,f):k(f[0],f[1],f[2],f[3],f[4]);f&&(f.length=0);return oa(l,e)}},arrayDeclaration:function(){var a=[];if("]"!==this.peekToken().text){do{if(this.peek("]"))break;a.push(this.expression())}while(this.expect(","))
}this.consume("]");return w(function(c,d){for(var e=[],f=0,g=a.length;f<g;f++)e.push(a[f](c,d));return e},{literal:!0,constant:a.every(ec),inputs:a})},object:function(){var a=[],c=[];if("}"!==this.peekToken().text){do{if(this.peek("}"))break;var d=this.consume();d.constant?a.push(d.value):d.identifier?a.push(d.text):this.throwError("invalid key",d);this.consume(":");c.push(this.expression())}while(this.expect(","))}this.consume("}");return w(function(d,f){for(var g={},h=0,l=c.length;h<l;h++)g[a[h]]=
c[h](d,f);return g},{literal:!0,constant:c.every(ec),inputs:c})}};var Bf=ja(),Af=ja(),Cf=Object.prototype.valueOf,Ba=S("$sce"),pa={HTML:"html",CSS:"css",URL:"url",RESOURCE_URL:"resourceUrl",JS:"js"},ma=S("$compile"),Z=W.createElement("a"),id=Aa(R.location.href);Ec.$inject=["$provide"];jd.$inject=["$locale"];ld.$inject=["$locale"];var od=".",Mf={yyyy:U("FullYear",4),yy:U("FullYear",2,0,!0),y:U("FullYear",1),MMMM:Kb("Month"),MMM:Kb("Month",!0),MM:U("Month",2,1),M:U("Month",1,1),dd:U("Date",2),d:U("Date",
1),HH:U("Hours",2),H:U("Hours",1),hh:U("Hours",2,-12),h:U("Hours",1,-12),mm:U("Minutes",2),m:U("Minutes",1),ss:U("Seconds",2),s:U("Seconds",1),sss:U("Milliseconds",3),EEEE:Kb("Day"),EEE:Kb("Day",!0),a:function(a,c){return 12>a.getHours()?c.AMPMS[0]:c.AMPMS[1]},Z:function(a){a=-1*a.getTimezoneOffset();return a=(0<=a?"+":"")+(Jb(Math[0<a?"floor":"ceil"](a/60),2)+Jb(Math.abs(a%60),2))},ww:qd(2),w:qd(1),G:hc,GG:hc,GGG:hc,GGGG:function(a,c){return 0>=a.getFullYear()?c.ERANAMES[0]:c.ERANAMES[1]}},Lf=/((?:[^yMdHhmsaZEwG']+)|(?:'(?:[^']|'')*')|(?:E+|y+|M+|d+|H+|h+|m+|s+|a|Z|G+|w+))(.*)/,
Kf=/^\-?\d+$/;kd.$inject=["$locale"];var Hf=ea(K),If=ea(vb);md.$inject=["$parse"];var Td=ea({restrict:"E",compile:function(a,c){if(!c.href&&!c.xlinkHref&&!c.name)return function(a,c){if("a"===c[0].nodeName.toLowerCase()){var f="[object SVGAnimatedString]"===Ca.call(c.prop("href"))?"xlink:href":"href";c.on("click",function(a){c.attr(f)||a.preventDefault()})}}}}),wb={};r(Fb,function(a,c){if("multiple"!=a){var d=ya("ng-"+c);wb[d]=function(){return{restrict:"A",priority:100,link:function(a,f,g){a.$watch(g[d],
function(a){g.$set(c,!!a)})}}}}});r(Oc,function(a,c){wb[c]=function(){return{priority:100,link:function(a,e,f){if("ngPattern"===c&&"/"==f.ngPattern.charAt(0)&&(e=f.ngPattern.match(Of))){f.$set("ngPattern",new RegExp(e[1],e[2]));return}a.$watch(f[c],function(a){f.$set(c,a)})}}}});r(["src","srcset","href"],function(a){var c=ya("ng-"+a);wb[c]=function(){return{priority:99,link:function(d,e,f){var g=a,h=a;"href"===a&&"[object SVGAnimatedString]"===Ca.call(e.prop("href"))&&(h="xlinkHref",f.$attr[h]="xlink:href",
g=null);f.$observe(c,function(c){c?(f.$set(h,c),Ra&&g&&e.prop(g,f[h])):"href"===a&&f.$set(h,null)})}}}});var Lb={$addControl:A,$$renameControl:function(a,c){a.$name=c},$removeControl:A,$setValidity:A,$setDirty:A,$setPristine:A,$setSubmitted:A};rd.$inject=["$element","$attrs","$scope","$animate","$interpolate"];var yd=function(a){return["$timeout",function(c){return{name:"form",restrict:a?"EAC":"E",controller:rd,compile:function(d,e){d.addClass(Sa).addClass(ob);var f=e.name?"name":a&&e.ngForm?"ngForm":
!1;return{pre:function(a,d,e,k){if(!("action"in e)){var n=function(c){a.$apply(function(){k.$commitViewValue();k.$setSubmitted()});c.preventDefault()};d[0].addEventListener("submit",n,!1);d.on("$destroy",function(){c(function(){d[0].removeEventListener("submit",n,!1)},0,!1)})}var p=k.$$parentForm;f&&(jb(a,null,k.$name,k,k.$name),e.$observe(f,function(c){k.$name!==c&&(jb(a,null,k.$name,u,k.$name),p.$$renameControl(k,c),jb(a,null,k.$name,k,k.$name))}));d.on("$destroy",function(){p.$removeControl(k);
f&&jb(a,null,e[f],u,k.$name);w(k,Lb)})}}}}}]},Ud=yd(),ge=yd(!0),Nf=/\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,$f=/^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,ag=/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i,bg=/^\s*(\-|\+)?(\d+|(\d*(\.\d*)))\s*$/,zd=/^(\d{4})-(\d{2})-(\d{2})$/,Ad=/^(\d{4})-(\d\d)-(\d\d)T(\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/,kc=/^(\d{4})-W(\d\d)$/,Bd=/^(\d{4})-(\d\d)$/,
Cd=/^(\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/,Dd={text:function(a,c,d,e,f,g){lb(a,c,d,e,f,g);ic(e)},date:mb("date",zd,Nb(zd,["yyyy","MM","dd"]),"yyyy-MM-dd"),"datetime-local":mb("datetimelocal",Ad,Nb(Ad,"yyyy MM dd HH mm ss sss".split(" ")),"yyyy-MM-ddTHH:mm:ss.sss"),time:mb("time",Cd,Nb(Cd,["HH","mm","ss","sss"]),"HH:mm:ss.sss"),week:mb("week",kc,function(a,c){if(ha(a))return a;if(x(a)){kc.lastIndex=0;var d=kc.exec(a);if(d){var e=+d[1],f=+d[2],g=d=0,h=0,l=0,k=pd(e),f=7*(f-1);c&&(d=c.getHours(),g=
c.getMinutes(),h=c.getSeconds(),l=c.getMilliseconds());return new Date(e,0,k.getDate()+f,d,g,h,l)}}return NaN},"yyyy-Www"),month:mb("month",Bd,Nb(Bd,["yyyy","MM"]),"yyyy-MM"),number:function(a,c,d,e,f,g){td(a,c,d,e);lb(a,c,d,e,f,g);e.$$parserName="number";e.$parsers.push(function(a){return e.$isEmpty(a)?null:bg.test(a)?parseFloat(a):u});e.$formatters.push(function(a){if(!e.$isEmpty(a)){if(!Y(a))throw nb("numfmt",a);a=a.toString()}return a});if(y(d.min)||d.ngMin){var h;e.$validators.min=function(a){return e.$isEmpty(a)||
D(h)||a>=h};d.$observe("min",function(a){y(a)&&!Y(a)&&(a=parseFloat(a,10));h=Y(a)&&!isNaN(a)?a:u;e.$validate()})}if(y(d.max)||d.ngMax){var l;e.$validators.max=function(a){return e.$isEmpty(a)||D(l)||a<=l};d.$observe("max",function(a){y(a)&&!Y(a)&&(a=parseFloat(a,10));l=Y(a)&&!isNaN(a)?a:u;e.$validate()})}},url:function(a,c,d,e,f,g){lb(a,c,d,e,f,g);ic(e);e.$$parserName="url";e.$validators.url=function(a,c){var d=a||c;return e.$isEmpty(d)||$f.test(d)}},email:function(a,c,d,e,f,g){lb(a,c,d,e,f,g);ic(e);
e.$$parserName="email";e.$validators.email=function(a,c){var d=a||c;return e.$isEmpty(d)||ag.test(d)}},radio:function(a,c,d,e){D(d.name)&&c.attr("name",++rb);c.on("click",function(a){c[0].checked&&e.$setViewValue(d.value,a&&a.type)});e.$render=function(){c[0].checked=d.value==e.$viewValue};d.$observe("value",e.$render)},checkbox:function(a,c,d,e,f,g,h,l){var k=ud(l,a,"ngTrueValue",d.ngTrueValue,!0),n=ud(l,a,"ngFalseValue",d.ngFalseValue,!1);c.on("click",function(a){e.$setViewValue(c[0].checked,a&&
a.type)});e.$render=function(){c[0].checked=e.$viewValue};e.$isEmpty=function(a){return!1===a};e.$formatters.push(function(a){return ia(a,k)});e.$parsers.push(function(a){return a?k:n})},hidden:A,button:A,submit:A,reset:A,file:A},yc=["$browser","$sniffer","$filter","$parse",function(a,c,d,e){return{restrict:"E",require:["?ngModel"],link:{pre:function(f,g,h,l){l[0]&&(Dd[K(h.type)]||Dd.text)(f,g,h,l[0],c,a,d,e)}}}}],cg=/^(true|false|\d+)$/,ye=function(){return{restrict:"A",priority:100,compile:function(a,
c){return cg.test(c.ngValue)?function(a,c,f){f.$set("value",a.$eval(f.ngValue))}:function(a,c,f){a.$watch(f.ngValue,function(a){f.$set("value",a)})}}}},Zd=["$compile",function(a){return{restrict:"AC",compile:function(c){a.$$addBindingClass(c);return function(c,e,f){a.$$addBindingInfo(e,f.ngBind);e=e[0];c.$watch(f.ngBind,function(a){e.textContent=a===u?"":a})}}}}],ae=["$interpolate","$compile",function(a,c){return{compile:function(d){c.$$addBindingClass(d);return function(d,f,g){d=a(f.attr(g.$attr.ngBindTemplate));
c.$$addBindingInfo(f,d.expressions);f=f[0];g.$observe("ngBindTemplate",function(a){f.textContent=a===u?"":a})}}}}],$d=["$sce","$parse","$compile",function(a,c,d){return{restrict:"A",compile:function(e,f){var g=c(f.ngBindHtml),h=c(f.ngBindHtml,function(a){return(a||"").toString()});d.$$addBindingClass(e);return function(c,e,f){d.$$addBindingInfo(e,f.ngBindHtml);c.$watch(h,function(){e.html(a.getTrustedHtml(g(c))||"")})}}}}],xe=ea({restrict:"A",require:"ngModel",link:function(a,c,d,e){e.$viewChangeListeners.push(function(){a.$eval(d.ngChange)})}}),
be=jc("",!0),de=jc("Odd",0),ce=jc("Even",1),ee=Ia({compile:function(a,c){c.$set("ngCloak",u);a.removeClass("ng-cloak")}}),fe=[function(){return{restrict:"A",scope:!0,controller:"@",priority:500}}],Dc={},dg={blur:!0,focus:!0};r("click dblclick mousedown mouseup mouseover mouseout mousemove mouseenter mouseleave keydown keyup keypress submit focus blur copy cut paste".split(" "),function(a){var c=ya("ng-"+a);Dc[c]=["$parse","$rootScope",function(d,e){return{restrict:"A",compile:function(f,g){var h=
d(g[c],null,!0);return function(c,d){d.on(a,function(d){var f=function(){h(c,{$event:d})};dg[a]&&e.$$phase?c.$evalAsync(f):c.$apply(f)})}}}}]});var ie=["$animate",function(a){return{multiElement:!0,transclude:"element",priority:600,terminal:!0,restrict:"A",$$tlb:!0,link:function(c,d,e,f,g){var h,l,k;c.$watch(e.ngIf,function(c){c?l||g(function(c,f){l=f;c[c.length++]=W.createComment(" end ngIf: "+e.ngIf+" ");h={clone:c};a.enter(c,d.parent(),d)}):(k&&(k.remove(),k=null),l&&(l.$destroy(),l=null),h&&(k=
ub(h.clone),a.leave(k).then(function(){k=null}),h=null))})}}}],je=["$templateRequest","$anchorScroll","$animate",function(a,c,d){return{restrict:"ECA",priority:400,terminal:!0,transclude:"element",controller:ca.noop,compile:function(e,f){var g=f.ngInclude||f.src,h=f.onload||"",l=f.autoscroll;return function(e,f,p,q,r){var s=0,u,v,m,C=function(){v&&(v.remove(),v=null);u&&(u.$destroy(),u=null);m&&(d.leave(m).then(function(){v=null}),v=m,m=null)};e.$watch(g,function(g){var p=function(){!y(l)||l&&!e.$eval(l)||
c()},M=++s;g?(a(g,!0).then(function(a){if(M===s){var c=e.$new();q.template=a;a=r(c,function(a){C();d.enter(a,null,f).then(p)});u=c;m=a;u.$emit("$includeContentLoaded",g);e.$eval(h)}},function(){M===s&&(C(),e.$emit("$includeContentError",g))}),e.$emit("$includeContentRequested",g)):(C(),q.template=null)})}}}}],Ae=["$compile",function(a){return{restrict:"ECA",priority:-400,require:"ngInclude",link:function(c,d,e,f){/SVG/.test(d[0].toString())?(d.empty(),a(Gc(f.template,W).childNodes)(c,function(a){d.append(a)},
{futureParentElement:d})):(d.html(f.template),a(d.contents())(c))}}}],ke=Ia({priority:450,compile:function(){return{pre:function(a,c,d){a.$eval(d.ngInit)}}}}),we=function(){return{restrict:"A",priority:100,require:"ngModel",link:function(a,c,d,e){var f=c.attr(d.$attr.ngList)||", ",g="false"!==d.ngTrim,h=g?N(f):f;e.$parsers.push(function(a){if(!D(a)){var c=[];a&&r(a.split(h),function(a){a&&c.push(g?N(a):a)});return c}});e.$formatters.push(function(a){return H(a)?a.join(f):u});e.$isEmpty=function(a){return!a||
!a.length}}}},ob="ng-valid",vd="ng-invalid",Sa="ng-pristine",Mb="ng-dirty",xd="ng-pending",nb=S("ngModel"),eg=["$scope","$exceptionHandler","$attrs","$element","$parse","$animate","$timeout","$rootScope","$q","$interpolate",function(a,c,d,e,f,g,h,l,k,n){this.$modelValue=this.$viewValue=Number.NaN;this.$$rawModelValue=u;this.$validators={};this.$asyncValidators={};this.$parsers=[];this.$formatters=[];this.$viewChangeListeners=[];this.$untouched=!0;this.$touched=!1;this.$pristine=!0;this.$dirty=!1;
this.$valid=!0;this.$invalid=!1;this.$error={};this.$$success={};this.$pending=u;this.$name=n(d.name||"",!1)(a);var p=f(d.ngModel),q=p.assign,t=p,s=q,F=null,v,m=this;this.$$setOptions=function(a){if((m.$options=a)&&a.getterSetter){var c=f(d.ngModel+"()"),g=f(d.ngModel+"($$$p)");t=function(a){var d=p(a);z(d)&&(d=c(a));return d};s=function(a,c){z(p(a))?g(a,{$$$p:m.$modelValue}):q(a,m.$modelValue)}}else if(!p.assign)throw nb("nonassign",d.ngModel,xa(e));};this.$render=A;this.$isEmpty=function(a){return D(a)||
""===a||null===a||a!==a};var C=e.inheritedData("$formController")||Lb,w=0;sd({ctrl:this,$element:e,set:function(a,c){a[c]=!0},unset:function(a,c){delete a[c]},parentForm:C,$animate:g});this.$setPristine=function(){m.$dirty=!1;m.$pristine=!0;g.removeClass(e,Mb);g.addClass(e,Sa)};this.$setDirty=function(){m.$dirty=!0;m.$pristine=!1;g.removeClass(e,Sa);g.addClass(e,Mb);C.$setDirty()};this.$setUntouched=function(){m.$touched=!1;m.$untouched=!0;g.setClass(e,"ng-untouched","ng-touched")};this.$setTouched=
function(){m.$touched=!0;m.$untouched=!1;g.setClass(e,"ng-touched","ng-untouched")};this.$rollbackViewValue=function(){h.cancel(F);m.$viewValue=m.$$lastCommittedViewValue;m.$render()};this.$validate=function(){if(!Y(m.$modelValue)||!isNaN(m.$modelValue)){var a=m.$$rawModelValue,c=m.$valid,d=m.$modelValue,e=m.$options&&m.$options.allowInvalid;m.$$runValidators(a,m.$$lastCommittedViewValue,function(f){e||c===f||(m.$modelValue=f?a:u,m.$modelValue!==d&&m.$$writeModelToScope())})}};this.$$runValidators=
function(a,c,d){function e(){var d=!0;r(m.$validators,function(e,f){var h=e(a,c);d=d&&h;g(f,h)});return d?!0:(r(m.$asyncValidators,function(a,c){g(c,null)}),!1)}function f(){var d=[],e=!0;r(m.$asyncValidators,function(f,h){var l=f(a,c);if(!l||!z(l.then))throw nb("$asyncValidators",l);g(h,u);d.push(l.then(function(){g(h,!0)},function(a){e=!1;g(h,!1)}))});d.length?k.all(d).then(function(){h(e)},A):h(!0)}function g(a,c){l===w&&m.$setValidity(a,c)}function h(a){l===w&&d(a)}w++;var l=w;(function(){var a=
m.$$parserName||"parse";if(v===u)g(a,null);else return v||(r(m.$validators,function(a,c){g(c,null)}),r(m.$asyncValidators,function(a,c){g(c,null)})),g(a,v),v;return!0})()?e()?f():h(!1):h(!1)};this.$commitViewValue=function(){var a=m.$viewValue;h.cancel(F);if(m.$$lastCommittedViewValue!==a||""===a&&m.$$hasNativeValidators)m.$$lastCommittedViewValue=a,m.$pristine&&this.$setDirty(),this.$$parseAndValidate()};this.$$parseAndValidate=function(){var c=m.$$lastCommittedViewValue;if(v=D(c)?u:!0)for(var d=
0;d<m.$parsers.length;d++)if(c=m.$parsers[d](c),D(c)){v=!1;break}Y(m.$modelValue)&&isNaN(m.$modelValue)&&(m.$modelValue=t(a));var e=m.$modelValue,f=m.$options&&m.$options.allowInvalid;m.$$rawModelValue=c;f&&(m.$modelValue=c,m.$modelValue!==e&&m.$$writeModelToScope());m.$$runValidators(c,m.$$lastCommittedViewValue,function(a){f||(m.$modelValue=a?c:u,m.$modelValue!==e&&m.$$writeModelToScope())})};this.$$writeModelToScope=function(){s(a,m.$modelValue);r(m.$viewChangeListeners,function(a){try{a()}catch(d){c(d)}})};
this.$setViewValue=function(a,c){m.$viewValue=a;m.$options&&!m.$options.updateOnDefault||m.$$debounceViewValueCommit(c)};this.$$debounceViewValueCommit=function(c){var d=0,e=m.$options;e&&y(e.debounce)&&(e=e.debounce,Y(e)?d=e:Y(e[c])?d=e[c]:Y(e["default"])&&(d=e["default"]));h.cancel(F);d?F=h(function(){m.$commitViewValue()},d):l.$$phase?m.$commitViewValue():a.$apply(function(){m.$commitViewValue()})};a.$watch(function(){var c=t(a);if(c!==m.$modelValue&&(m.$modelValue===m.$modelValue||c===c)){m.$modelValue=
m.$$rawModelValue=c;v=u;for(var d=m.$formatters,e=d.length,f=c;e--;)f=d[e](f);m.$viewValue!==f&&(m.$viewValue=m.$$lastCommittedViewValue=f,m.$render(),m.$$runValidators(c,f,A))}return c})}],ve=["$rootScope",function(a){return{restrict:"A",require:["ngModel","^?form","^?ngModelOptions"],controller:eg,priority:1,compile:function(c){c.addClass(Sa).addClass("ng-untouched").addClass(ob);return{pre:function(a,c,f,g){var h=g[0],l=g[1]||Lb;h.$$setOptions(g[2]&&g[2].$options);l.$addControl(h);f.$observe("name",
function(a){h.$name!==a&&l.$$renameControl(h,a)});a.$on("$destroy",function(){l.$removeControl(h)})},post:function(c,e,f,g){var h=g[0];if(h.$options&&h.$options.updateOn)e.on(h.$options.updateOn,function(a){h.$$debounceViewValueCommit(a&&a.type)});e.on("blur",function(e){h.$touched||(a.$$phase?c.$evalAsync(h.$setTouched):c.$apply(h.$setTouched))})}}}}}],fg=/(\s+|^)default(\s+|$)/,ze=function(){return{restrict:"A",controller:["$scope","$attrs",function(a,c){var d=this;this.$options=a.$eval(c.ngModelOptions);
this.$options.updateOn!==u?(this.$options.updateOnDefault=!1,this.$options.updateOn=N(this.$options.updateOn.replace(fg,function(){d.$options.updateOnDefault=!0;return" "}))):this.$options.updateOnDefault=!0}]}},le=Ia({terminal:!0,priority:1E3}),me=["$locale","$interpolate",function(a,c){var d=/{}/g,e=/^when(Minus)?(.+)$/;return{restrict:"EA",link:function(f,g,h){function l(a){g.text(a||"")}var k=h.count,n=h.$attr.when&&g.attr(h.$attr.when),p=h.offset||0,q=f.$eval(n)||{},t={},n=c.startSymbol(),s=
c.endSymbol(),u=n+k+"-"+p+s,v=ca.noop,m;r(h,function(a,c){var d=e.exec(c);d&&(d=(d[1]?"-":"")+K(d[2]),q[d]=g.attr(h.$attr[c]))});r(q,function(a,e){t[e]=c(a.replace(d,u))});f.$watch(k,function(c){c=parseFloat(c);var d=isNaN(c);d||c in q||(c=a.pluralCat(c-p));c===m||d&&isNaN(m)||(v(),v=f.$watch(t[c],l),m=c)})}}}],ne=["$parse","$animate",function(a,c){var d=S("ngRepeat"),e=function(a,c,d,e,k,n,p){a[d]=e;k&&(a[k]=n);a.$index=c;a.$first=0===c;a.$last=c===p-1;a.$middle=!(a.$first||a.$last);a.$odd=!(a.$even=
0===(c&1))};return{restrict:"A",multiElement:!0,transclude:"element",priority:1E3,terminal:!0,$$tlb:!0,compile:function(f,g){var h=g.ngRepeat,l=W.createComment(" end ngRepeat: "+h+" "),k=h.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);if(!k)throw d("iexp",h);var n=k[1],p=k[2],q=k[3],t=k[4],k=n.match(/^(?:(\s*[\$\w]+)|\(\s*([\$\w]+)\s*,\s*([\$\w]+)\s*\))$/);if(!k)throw d("iidexp",n);var s=k[3]||k[1],F=k[2];if(q&&(!/^[$a-zA-Z_][$a-zA-Z0-9_]*$/.test(q)||
/^(null|undefined|this|\$index|\$first|\$middle|\$last|\$even|\$odd|\$parent|\$root|\$id)$/.test(q)))throw d("badident",q);var v,m,C,y,w={$id:Na};t?v=a(t):(C=function(a,c){return Na(c)},y=function(a){return a});return function(a,f,g,k,n){v&&(m=function(c,d,e){F&&(w[F]=c);w[s]=d;w.$index=e;return v(a,w)});var t=ja();a.$watchCollection(p,function(g){var k,p,v=f[0],G,w=ja(),D,I,A,z,H,O,x;q&&(a[q]=g);if(Ta(g))H=g,p=m||C;else{p=m||y;H=[];for(x in g)g.hasOwnProperty(x)&&"$"!=x.charAt(0)&&H.push(x);H.sort()}D=
H.length;x=Array(D);for(k=0;k<D;k++)if(I=g===H?k:H[k],A=g[I],z=p(I,A,k),t[z])O=t[z],delete t[z],w[z]=O,x[k]=O;else{if(w[z])throw r(x,function(a){a&&a.scope&&(t[a.id]=a)}),d("dupes",h,z,A);x[k]={id:z,scope:u,clone:u};w[z]=!0}for(G in t){O=t[G];z=ub(O.clone);c.leave(z);if(z[0].parentNode)for(k=0,p=z.length;k<p;k++)z[k].$$NG_REMOVED=!0;O.scope.$destroy()}for(k=0;k<D;k++)if(I=g===H?k:H[k],A=g[I],O=x[k],O.scope){G=v;do G=G.nextSibling;while(G&&G.$$NG_REMOVED);O.clone[0]!=G&&c.move(ub(O.clone),null,B(v));
v=O.clone[O.clone.length-1];e(O.scope,k,s,A,F,I,D)}else n(function(a,d){O.scope=d;var f=l.cloneNode(!1);a[a.length++]=f;c.enter(a,null,B(v));v=f;O.clone=a;w[O.id]=O;e(O.scope,k,s,A,F,I,D)});t=w})}}}}],oe=["$animate",function(a){return{restrict:"A",multiElement:!0,link:function(c,d,e){c.$watch(e.ngShow,function(c){a[c?"removeClass":"addClass"](d,"ng-hide",{tempClasses:"ng-hide-animate"})})}}}],he=["$animate",function(a){return{restrict:"A",multiElement:!0,link:function(c,d,e){c.$watch(e.ngHide,function(c){a[c?
"addClass":"removeClass"](d,"ng-hide",{tempClasses:"ng-hide-animate"})})}}}],pe=Ia(function(a,c,d){a.$watch(d.ngStyle,function(a,d){d&&a!==d&&r(d,function(a,d){c.css(d,"")});a&&c.css(a)},!0)}),qe=["$animate",function(a){return{restrict:"EA",require:"ngSwitch",controller:["$scope",function(){this.cases={}}],link:function(c,d,e,f){var g=[],h=[],l=[],k=[],n=function(a,c){return function(){a.splice(c,1)}};c.$watch(e.ngSwitch||e.on,function(c){var d,e;d=0;for(e=l.length;d<e;++d)a.cancel(l[d]);d=l.length=
0;for(e=k.length;d<e;++d){var s=ub(h[d].clone);k[d].$destroy();(l[d]=a.leave(s)).then(n(l,d))}h.length=0;k.length=0;(g=f.cases["!"+c]||f.cases["?"])&&r(g,function(c){c.transclude(function(d,e){k.push(e);var f=c.element;d[d.length++]=W.createComment(" end ngSwitchWhen: ");h.push({clone:d});a.enter(d,f.parent(),f)})})})}}}],re=Ia({transclude:"element",priority:1200,require:"^ngSwitch",multiElement:!0,link:function(a,c,d,e,f){e.cases["!"+d.ngSwitchWhen]=e.cases["!"+d.ngSwitchWhen]||[];e.cases["!"+d.ngSwitchWhen].push({transclude:f,
element:c})}}),se=Ia({transclude:"element",priority:1200,require:"^ngSwitch",multiElement:!0,link:function(a,c,d,e,f){e.cases["?"]=e.cases["?"]||[];e.cases["?"].push({transclude:f,element:c})}}),ue=Ia({restrict:"EAC",link:function(a,c,d,e,f){if(!f)throw S("ngTransclude")("orphan",xa(c));f(function(a){c.empty();c.append(a)})}}),Vd=["$templateCache",function(a){return{restrict:"E",terminal:!0,compile:function(c,d){"text/ng-template"==d.type&&a.put(d.id,c[0].text)}}}],gg=S("ngOptions"),te=ea({restrict:"A",
terminal:!0}),Wd=["$compile","$parse",function(a,c){var d=/^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/,e={$setViewValue:A};return{restrict:"E",require:["select","?ngModel"],controller:["$element","$scope","$attrs",function(a,c,d){var l=this,k={},n=e,p;l.databound=d.ngModel;l.init=function(a,c,d){n=a;p=d};l.addOption=function(c,d){Ma(c,'"option value"');
k[c]=!0;n.$viewValue==c&&(a.val(c),p.parent()&&p.remove());d&&d[0].hasAttribute("selected")&&(d[0].selected=!0)};l.removeOption=function(a){this.hasOption(a)&&(delete k[a],n.$viewValue===a&&this.renderUnknownOption(a))};l.renderUnknownOption=function(c){c="? "+Na(c)+" ?";p.val(c);a.prepend(p);a.val(c);p.prop("selected",!0)};l.hasOption=function(a){return k.hasOwnProperty(a)};c.$on("$destroy",function(){l.renderUnknownOption=A})}],link:function(e,g,h,l){function k(a,c,d,e){d.$render=function(){var a=
d.$viewValue;e.hasOption(a)?(z.parent()&&z.remove(),c.val(a),""===a&&v.prop("selected",!0)):null==a&&v?c.val(""):e.renderUnknownOption(a)};c.on("change",function(){a.$apply(function(){z.parent()&&z.remove();d.$setViewValue(c.val())})})}function n(a,c,d){var e;d.$render=function(){var a=new gb(d.$viewValue);r(c.find("option"),function(c){c.selected=y(a.get(c.value))})};a.$watch(function(){ia(e,d.$viewValue)||(e=sa(d.$viewValue),d.$render())});c.on("change",function(){a.$apply(function(){var a=[];r(c.find("option"),
function(c){c.selected&&a.push(c.value)});d.$setViewValue(a)})})}function p(e,f,g){function h(a,c,d){T[A]=d;I&&(T[I]=c);return a(e,T)}function l(a){var c;if(t)if(K&&H(a)){c=new gb([]);for(var d=0;d<a.length;d++)c.put(h(K,null,a[d]),!0)}else c=new gb(a);else K&&(a=h(K,null,a));return function(d,e){var f;f=K?K:x?x:E;return t?y(c.remove(h(f,d,e))):a===h(f,d,e)}}function k(){m||(e.$$postDigest(p),m=!0)}function n(a,c,d){a[c]=a[c]||0;a[c]+=d?1:-1}function p(){m=!1;var a={"":[]},c=[""],d,k,s,u,v;s=g.$viewValue;
u=L(e)||[];var A=I?Object.keys(u).sort():u,x,B,H,E,P={};v=l(s);var N=!1,U,W;R={};for(E=0;H=A.length,E<H;E++){x=E;if(I&&(x=A[E],"$"===x.charAt(0)))continue;B=u[x];d=h(M,x,B)||"";(k=a[d])||(k=a[d]=[],c.push(d));d=v(x,B);N=N||d;B=h(z,x,B);B=y(B)?B:"";W=K?K(e,T):I?A[E]:E;K&&(R[W]=x);k.push({id:W,label:B,selected:d})}t||(w||null===s?a[""].unshift({id:"",label:"",selected:!N}):N||a[""].unshift({id:"?",label:"",selected:!0}));x=0;for(A=c.length;x<A;x++){d=c[x];k=a[d];S.length<=x?(s={element:D.clone().attr("label",
d),label:k.label},u=[s],S.push(u),f.append(s.element)):(u=S[x],s=u[0],s.label!=d&&s.element.attr("label",s.label=d));N=null;E=0;for(H=k.length;E<H;E++)d=k[E],(v=u[E+1])?(N=v.element,v.label!==d.label&&(n(P,v.label,!1),n(P,d.label,!0),N.text(v.label=d.label),N.prop("label",v.label)),v.id!==d.id&&N.val(v.id=d.id),N[0].selected!==d.selected&&(N.prop("selected",v.selected=d.selected),Ra&&N.prop("selected",v.selected))):(""===d.id&&w?U=w:(U=C.clone()).val(d.id).prop("selected",d.selected).attr("selected",
d.selected).prop("label",d.label).text(d.label),u.push(v={element:U,label:d.label,id:d.id,selected:d.selected}),n(P,d.label,!0),N?N.after(U):s.element.append(U),N=U);for(E++;u.length>E;)d=u.pop(),n(P,d.label,!1),d.element.remove()}for(;S.length>x;){k=S.pop();for(E=1;E<k.length;++E)n(P,k[E].label,!1);k[0].element.remove()}r(P,function(a,c){0<a?q.addOption(c):0>a&&q.removeOption(c)})}var v;if(!(v=s.match(d)))throw gg("iexp",s,xa(f));var z=c(v[2]||v[1]),A=v[4]||v[6],B=/ as /.test(v[0])&&v[1],x=B?c(B):
null,I=v[5],M=c(v[3]||""),E=c(v[2]?v[1]:A),L=c(v[7]),K=v[8]?c(v[8]):null,R={},S=[[{element:f,label:""}]],T={};w&&(a(w)(e),w.removeClass("ng-scope"),w.remove());f.empty();f.on("change",function(){e.$apply(function(){var a=L(e)||[],c;if(t)c=[],r(f.val(),function(d){d=K?R[d]:d;c.push("?"===d?u:""===d?null:h(x?x:E,d,a[d]))});else{var d=K?R[f.val()]:f.val();c="?"===d?u:""===d?null:h(x?x:E,d,a[d])}g.$setViewValue(c);p()})});g.$render=p;e.$watchCollection(L,k);e.$watchCollection(function(){var a=L(e),c;
if(a&&H(a)){c=Array(a.length);for(var d=0,f=a.length;d<f;d++)c[d]=h(z,d,a[d])}else if(a)for(d in c={},a)a.hasOwnProperty(d)&&(c[d]=h(z,d,a[d]));return c},k);t&&e.$watchCollection(function(){return g.$modelValue},k)}if(l[1]){var q=l[0];l=l[1];var t=h.multiple,s=h.ngOptions,w=!1,v,m=!1,C=B(W.createElement("option")),D=B(W.createElement("optgroup")),z=C.clone();h=0;for(var A=g.children(),x=A.length;h<x;h++)if(""===A[h].value){v=w=A.eq(h);break}q.init(l,w,z);t&&(l.$isEmpty=function(a){return!a||0===a.length});
s?p(e,g,l):t?n(e,g,l):k(e,g,l,q)}}}}],Yd=["$interpolate",function(a){var c={addOption:A,removeOption:A};return{restrict:"E",priority:100,compile:function(d,e){if(D(e.value)){var f=a(d.text(),!0);f||e.$set("value",d.text())}return function(a,d,e){var k=d.parent(),n=k.data("$selectController")||k.parent().data("$selectController");n&&n.databound||(n=c);f?a.$watch(f,function(a,c){e.$set("value",a);c!==a&&n.removeOption(c);n.addOption(a,d)}):n.addOption(e.value,d);d.on("$destroy",function(){n.removeOption(e.value)})}}}}],
Xd=ea({restrict:"E",terminal:!1}),Ac=function(){return{restrict:"A",require:"?ngModel",link:function(a,c,d,e){e&&(d.required=!0,e.$validators.required=function(a,c){return!d.required||!e.$isEmpty(c)},d.$observe("required",function(){e.$validate()}))}}},zc=function(){return{restrict:"A",require:"?ngModel",link:function(a,c,d,e){if(e){var f,g=d.ngPattern||d.pattern;d.$observe("pattern",function(a){x(a)&&0<a.length&&(a=new RegExp("^"+a+"$"));if(a&&!a.test)throw S("ngPattern")("noregexp",g,a,xa(c));f=
a||u;e.$validate()});e.$validators.pattern=function(a,c){return e.$isEmpty(c)||D(f)||f.test(c)}}}}},Cc=function(){return{restrict:"A",require:"?ngModel",link:function(a,c,d,e){if(e){var f=-1;d.$observe("maxlength",function(a){a=aa(a);f=isNaN(a)?-1:a;e.$validate()});e.$validators.maxlength=function(a,c){return 0>f||e.$isEmpty(c)||c.length<=f}}}}},Bc=function(){return{restrict:"A",require:"?ngModel",link:function(a,c,d,e){if(e){var f=0;d.$observe("minlength",function(a){f=aa(a)||0;e.$validate()});e.$validators.minlength=
function(a,c){return e.$isEmpty(c)||c.length>=f}}}}};R.angular.bootstrap?console.log("WARNING: Tried to load angular more than once."):(Nd(),Pd(ca),B(W).ready(function(){Jd(W,tc)}))})(window,document);!window.angular.$$csp()&&window.angular.element(document.head).prepend('<style type="text/css">@charset "UTF-8";[ng\\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\\:form{display:block;}</style>');
//# sourceMappingURL=angular.min.js.map

/*!
 * Bootstrap v3.3.6 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under the MIT license
 */

if (typeof jQuery === 'undefined') {
  throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

+function ($) {
  'use strict';
  var version = $.fn.jquery.split(' ')[0].split('.')
  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1) || (version[0] > 2)) {
    throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3')
  }
}(jQuery);

/* ========================================================================
 * Bootstrap: transition.js v3.3.6
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: alert.js v3.3.6
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="alert"]'
  var Alert   = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.VERSION = '3.3.6'

  Alert.TRANSITION_DURATION = 150

  Alert.prototype.close = function (e) {
    var $this    = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
      $parent = $this.closest('.alert')
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      // detach from parent, fire event then clean up data
      $parent.detach().trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one('bsTransitionEnd', removeElement)
        .emulateTransitionEnd(Alert.TRANSITION_DURATION) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.alert

  $.fn.alert             = Plugin
  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);

/* ========================================================================
 * Bootstrap: button.js v3.3.6
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.VERSION  = '3.3.6'

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d    = 'disabled'
    var $el  = this.$element
    var val  = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state += 'Text'

    if (data.resetText == null) $el.data('resetText', $el[val]())

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      $el[val](data[state] == null ? this.options[state] : data[state])

      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked')) changed = false
        $parent.find('.active').removeClass('active')
        this.$element.addClass('active')
      } else if ($input.prop('type') == 'checkbox') {
        if (($input.prop('checked')) !== this.$element.hasClass('active')) changed = false
        this.$element.toggleClass('active')
      }
      $input.prop('checked', this.$element.hasClass('active'))
      if (changed) $input.trigger('change')
    } else {
      this.$element.attr('aria-pressed', !this.$element.hasClass('active'))
      this.$element.toggleClass('active')
    }
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  var old = $.fn.button

  $.fn.button             = Plugin
  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document)
    .on('click.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      var $btn = $(e.target)
      if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
      Plugin.call($btn, 'toggle')
      if (!($(e.target).is('input[type="radio"]') || $(e.target).is('input[type="checkbox"]'))) e.preventDefault()
    })
    .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      $(e.target).closest('.btn').toggleClass('focus', /^focus(in)?$/.test(e.type))
    })

}(jQuery);

/* ========================================================================
 * Bootstrap: carousel.js v3.3.6
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CAROUSEL CLASS DEFINITION
  // =========================

  var Carousel = function (element, options) {
    this.$element    = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options     = options
    this.paused      = null
    this.sliding     = null
    this.interval    = null
    this.$active     = null
    this.$items      = null

    this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

    this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
      .on('mouseenter.bs.carousel', $.proxy(this.pause, this))
      .on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
  }

  Carousel.VERSION  = '3.3.6'

  Carousel.TRANSITION_DURATION = 600

  Carousel.DEFAULTS = {
    interval: 5000,
    pause: 'hover',
    wrap: true,
    keyboard: true
  }

  Carousel.prototype.keydown = function (e) {
    if (/input|textarea/i.test(e.target.tagName)) return
    switch (e.which) {
      case 37: this.prev(); break
      case 39: this.next(); break
      default: return
    }

    e.preventDefault()
  }

  Carousel.prototype.cycle = function (e) {
    e || (this.paused = false)

    this.interval && clearInterval(this.interval)

    this.options.interval
      && !this.paused
      && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

    return this
  }

  Carousel.prototype.getItemIndex = function (item) {
    this.$items = item.parent().children('.item')
    return this.$items.index(item || this.$active)
  }

  Carousel.prototype.getItemForDirection = function (direction, active) {
    var activeIndex = this.getItemIndex(active)
    var willWrap = (direction == 'prev' && activeIndex === 0)
                || (direction == 'next' && activeIndex == (this.$items.length - 1))
    if (willWrap && !this.options.wrap) return active
    var delta = direction == 'prev' ? -1 : 1
    var itemIndex = (activeIndex + delta) % this.$items.length
    return this.$items.eq(itemIndex)
  }

  Carousel.prototype.to = function (pos) {
    var that        = this
    var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

    if (pos > (this.$items.length - 1) || pos < 0) return

    if (this.sliding)       return this.$element.one('slid.bs.carousel', function () { that.to(pos) }) // yes, "slid"
    if (activeIndex == pos) return this.pause().cycle()

    return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
  }

  Carousel.prototype.pause = function (e) {
    e || (this.paused = true)

    if (this.$element.find('.next, .prev').length && $.support.transition) {
      this.$element.trigger($.support.transition.end)
      this.cycle(true)
    }

    this.interval = clearInterval(this.interval)

    return this
  }

  Carousel.prototype.next = function () {
    if (this.sliding) return
    return this.slide('next')
  }

  Carousel.prototype.prev = function () {
    if (this.sliding) return
    return this.slide('prev')
  }

  Carousel.prototype.slide = function (type, next) {
    var $active   = this.$element.find('.item.active')
    var $next     = next || this.getItemForDirection(type, $active)
    var isCycling = this.interval
    var direction = type == 'next' ? 'left' : 'right'
    var that      = this

    if ($next.hasClass('active')) return (this.sliding = false)

    var relatedTarget = $next[0]
    var slideEvent = $.Event('slide.bs.carousel', {
      relatedTarget: relatedTarget,
      direction: direction
    })
    this.$element.trigger(slideEvent)
    if (slideEvent.isDefaultPrevented()) return

    this.sliding = true

    isCycling && this.pause()

    if (this.$indicators.length) {
      this.$indicators.find('.active').removeClass('active')
      var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
      $nextIndicator && $nextIndicator.addClass('active')
    }

    var slidEvent = $.Event('slid.bs.carousel', { relatedTarget: relatedTarget, direction: direction }) // yes, "slid"
    if ($.support.transition && this.$element.hasClass('slide')) {
      $next.addClass(type)
      $next[0].offsetWidth // force reflow
      $active.addClass(direction)
      $next.addClass(direction)
      $active
        .one('bsTransitionEnd', function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () {
            that.$element.trigger(slidEvent)
          }, 0)
        })
        .emulateTransitionEnd(Carousel.TRANSITION_DURATION)
    } else {
      $active.removeClass('active')
      $next.addClass('active')
      this.sliding = false
      this.$element.trigger(slidEvent)
    }

    isCycling && this.cycle()

    return this
  }


  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.carousel')
      var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
      var action  = typeof option == 'string' ? option : options.slide

      if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  var old = $.fn.carousel

  $.fn.carousel             = Plugin
  $.fn.carousel.Constructor = Carousel


  // CAROUSEL NO CONFLICT
  // ====================

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }


  // CAROUSEL DATA-API
  // =================

  var clickHandler = function (e) {
    var href
    var $this   = $(this)
    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
    if (!$target.hasClass('carousel')) return
    var options = $.extend({}, $target.data(), $this.data())
    var slideIndex = $this.attr('data-slide-to')
    if (slideIndex) options.interval = false

    Plugin.call($target, options)

    if (slideIndex) {
      $target.data('bs.carousel').to(slideIndex)
    }

    e.preventDefault()
  }

  $(document)
    .on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
    .on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

  $(window).on('load', function () {
    $('[data-ride="carousel"]').each(function () {
      var $carousel = $(this)
      Plugin.call($carousel, $carousel.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: collapse.js v3.3.6
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger      = $('[data-toggle="collapse"][href="#' + element.id + '"],' +
                           '[data-toggle="collapse"][data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION  = '3.3.6'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.children('.panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && /show|hide/.test(option)) options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse             = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()

    Plugin.call($target, option)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.6
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.6'

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget))
    })
  }

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $(document.createElement('div'))
          .addClass('dropdown-backdrop')
          .insertAfter($(this))
          .on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger($.Event('shown.bs.dropdown', relatedTarget))
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if (!isActive && e.which != 27 || isActive && e.which == 27) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.disabled):visible a'
    var $items = $parent.find('.dropdown-menu' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0)                 index--         // up
    if (e.which == 40 && index < $items.length - 1) index++         // down
    if (!~index)                                    index = 0

    $items.eq(index).trigger('focus')
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown             = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '.dropdown-menu', Dropdown.prototype.keydown)

}(jQuery);

/* ========================================================================
 * Bootstrap: modal.js v3.3.6
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options             = options
    this.$body               = $(document.body)
    this.$element            = $(element)
    this.$dialog             = this.$element.find('.modal-dialog')
    this.$backdrop           = null
    this.isShown             = null
    this.originalBodyPad     = null
    this.scrollbarWidth      = 0
    this.ignoreBackdropClick = false

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.3.6'

  Modal.TRANSITION_DURATION = 300
  Modal.BACKDROP_TRANSITION_DURATION = 150

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.setScrollbar()
    this.$body.addClass('modal-open')

    this.escape()
    this.resize()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
      })
    })

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      that.adjustDialog()

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element.addClass('in')

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$dialog // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.escape()
    this.resize()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .off('click.dismiss.bs.modal')
      .off('mouseup.dismiss.bs.modal')

    this.$dialog.off('mousedown.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal')
    }
  }

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
    } else {
      $(window).off('resize.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$body.removeClass('modal-open')
      that.resetAdjustments()
      that.resetScrollbar()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $(document.createElement('div'))
        .addClass('modal-backdrop ' + animate)
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false
          return
        }
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus()
          : this.hide()
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog()
  }

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

    this.$element.css({
      paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    })
  }

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    })
  }

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect()
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
    this.scrollbarWidth = this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    this.originalBodyPad = document.body.style.paddingRight || ''
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad)
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tooltip.js v3.3.6
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       = null
    this.options    = null
    this.enabled    = null
    this.timeout    = null
    this.hoverState = null
    this.$element   = null
    this.inState    = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION  = '3.3.6'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport))
    this.inState   = { click: false, hover: false, focus: false }

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true
    }

    if (self.tip().hasClass('in') || self.hoverState == 'in') {
      self.hoverState = 'in'
      return
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.isInStateTrue = function () {
    for (var key in this.inState) {
      if (this.inState[key]) return true
    }

    return false
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false
    }

    if (self.isInStateTrue()) return

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
      this.$element.trigger('inserted.bs.' + this.type)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < viewportDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > viewportDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < viewportDim.left   ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  += marginTop
    offset.left += marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical          = /top|bottom/.test(placement)
    var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      that.$element
        .removeAttr('aria-describedby')
        .trigger('hidden.bs.' + that.type)
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element

    var el     = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect    = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var elOffset  = isBody ? { top: 0, left: 0 } : $element.offset()
    var scroll    = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.right) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    if (!this.$tip) {
      this.$tip = $(this.options.template)
      if (this.$tip.length != 1) {
        throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!')
      }
    }
    return this.$tip
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    if (e) {
      self.inState.click = !self.inState.click
      if (self.isInStateTrue()) self.enter(self)
      else self.leave(self)
    } else {
      self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
      if (that.$tip) {
        that.$tip.detach()
      }
      that.$tip = null
      that.$arrow = null
      that.$viewport = null
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip             = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: popover.js v3.3.6
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION  = '3.3.6'

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o  = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($e[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.arrow'))
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.popover

  $.fn.popover             = Plugin
  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: scrollspy.js v3.3.6
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    this.$body          = $(document.body)
    this.$scrollElement = $(element).is(document.body) ? $(window) : $(element)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target || '') + ' .nav li > a'
    this.offsets        = []
    this.targets        = []
    this.activeTarget   = null
    this.scrollHeight   = 0

    this.$scrollElement.on('scroll.bs.scrollspy', $.proxy(this.process, this))
    this.refresh()
    this.process()
  }

  ScrollSpy.VERSION  = '3.3.6'

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
  }

  ScrollSpy.prototype.refresh = function () {
    var that          = this
    var offsetMethod  = 'offset'
    var offsetBase    = 0

    this.offsets      = []
    this.targets      = []
    this.scrollHeight = this.getScrollHeight()

    if (!$.isWindow(this.$scrollElement[0])) {
      offsetMethod = 'position'
      offsetBase   = this.$scrollElement.scrollTop()
    }

    this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[$href[offsetMethod]().top + offsetBase, href]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        that.offsets.push(this[0])
        that.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.getScrollHeight()
    var maxScroll    = this.options.offset + scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (this.scrollHeight != scrollHeight) {
      this.refresh()
    }

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets[targets.length - 1]) && this.activate(i)
    }

    if (activeTarget && scrollTop < offsets[0]) {
      this.activeTarget = null
      return this.clear()
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (offsets[i + 1] === undefined || scrollTop < offsets[i + 1])
        && this.activate(targets[i])
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    this.clear()

    var selector = this.selector +
      '[data-target="' + target + '"],' +
      this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }

  ScrollSpy.prototype.clear = function () {
    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.scrollspy

  $.fn.scrollspy             = Plugin
  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      Plugin.call($spy, $spy.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tab.js v3.3.6
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    // jscs:disable requireDollarBeforejQueryAssignment
    this.element = $(element)
    // jscs:enable requireDollarBeforejQueryAssignment
  }

  Tab.VERSION = '3.3.6'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && ($active.length && $active.hasClass('fade') || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
          .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu').length) {
        element
          .closest('li.dropdown')
            .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
            .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);

/* ========================================================================
 * Bootstrap: affix.js v3.3.6
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)

    this.$target = $(this.options.target)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))

    this.$element     = $(element)
    this.affixed      = null
    this.unpin        = null
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.VERSION  = '3.3.6'

  Affix.RESET    = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0,
    target: window
  }

  Affix.prototype.getState = function (scrollHeight, height, offsetTop, offsetBottom) {
    var scrollTop    = this.$target.scrollTop()
    var position     = this.$element.offset()
    var targetHeight = this.$target.height()

    if (offsetTop != null && this.affixed == 'top') return scrollTop < offsetTop ? 'top' : false

    if (this.affixed == 'bottom') {
      if (offsetTop != null) return (scrollTop + this.unpin <= position.top) ? false : 'bottom'
      return (scrollTop + targetHeight <= scrollHeight - offsetBottom) ? false : 'bottom'
    }

    var initializing   = this.affixed == null
    var colliderTop    = initializing ? scrollTop : position.top
    var colliderHeight = initializing ? targetHeight : height

    if (offsetTop != null && scrollTop <= offsetTop) return 'top'
    if (offsetBottom != null && (colliderTop + colliderHeight >= scrollHeight - offsetBottom)) return 'bottom'

    return false
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$target.scrollTop()
    var position  = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var height       = this.$element.height()
    var offset       = this.options.offset
    var offsetTop    = offset.top
    var offsetBottom = offset.bottom
    var scrollHeight = Math.max($(document).height(), $(document.body).height())

    if (typeof offset != 'object')         offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.getState(scrollHeight, height, offsetTop, offsetBottom)

    if (this.affixed != affix) {
      if (this.unpin != null) this.$element.css('top', '')

      var affixType = 'affix' + (affix ? '-' + affix : '')
      var e         = $.Event(affixType + '.bs.affix')

      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return

      this.affixed = affix
      this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

      this.$element
        .removeClass(Affix.RESET)
        .addClass(affixType)
        .trigger(affixType.replace('affix', 'affixed') + '.bs.affix')
    }

    if (affix == 'bottom') {
      this.$element.offset({
        top: scrollHeight - height - offsetBottom
      })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.affix

  $.fn.affix             = Plugin
  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom != null) data.offset.bottom = data.offsetBottom
      if (data.offsetTop    != null) data.offset.top    = data.offsetTop

      Plugin.call($spy, data)
    })
  })

}(jQuery);

/*
 *  Angular RangeSlider Directive
 *
 *  Version: 0.0.13
 *
 *  Author: Daniel Crisp, danielcrisp.com
 *
 *  The rangeSlider has been styled to match the default styling
 *  of form elements styled using Twitter's Bootstrap
 *
 *  Originally forked from https://github.com/leongersen/noUiSlider
 *

    This code is released under the MIT Licence - http://opensource.org/licenses/MIT

    Copyright (c) 2013 Daniel Crisp

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

*/

(function() {
    'use strict';

    // check if we need to support legacy angular
    var legacySupport = (angular.version.major === 1 && angular.version.minor === 0);

    /**
     * RangeSlider, allows user to define a range of values using a slider
     * Touch friendly.
     * @directive
     */
    angular.module('ui-rangeSlider', [])
        .directive('rangeSlider', ['$document', '$filter', '$log', function($document, $filter, $log) {

            // test for mouse, pointer or touch
            var eventNamespace = '.rangeSlider',

                defaults = {
                    disabled: false,
                    orientation: 'horizontal',
                    step: 0,
                    decimalPlaces: 0,
                    showValues: true,
                    preventEqualMinMax: false,
                    attachHandleValues: false
                },

                // Determine the events to bind. IE11 implements pointerEvents without
                // a prefix, which breaks compatibility with the IE10 implementation.
                /** @const */
                actions = window.navigator.pointerEnabled ? {
                    start: 'pointerdown',
                    move: 'pointermove',
                    end: 'pointerup',
                    over: 'pointerdown',
                    out: 'mouseout'
                } : window.navigator.msPointerEnabled ? {
                    start: 'MSPointerDown',
                    move: 'MSPointerMove',
                    end: 'MSPointerUp',
                    over: 'MSPointerDown',
                    out: 'mouseout'
                } : {
                    start: 'mousedown touchstart',
                    move: 'mousemove touchmove',
                    end: 'mouseup touchend',
                    over: 'mouseover touchstart',
                    out: 'mouseout'
                },

                onEvent = actions.start + eventNamespace,
                moveEvent = actions.move + eventNamespace,
                offEvent = actions.end + eventNamespace,
                overEvent = actions.over + eventNamespace,
                outEvent = actions.out + eventNamespace,

                // get standarised clientX and clientY
                client = function(f) {
                    try {
                        return [(f.clientX || f.originalEvent.clientX || f.originalEvent.touches[0].clientX), (f.clientY || f.originalEvent.clientY || f.originalEvent.touches[0].clientY)];
                    } catch (e) {
                        return ['x', 'y'];
                    }
                },

                restrict = function(value) {

                    // normalize so it can't move out of bounds
                    return (value < 0 ? 0 : (value > 100 ? 100 : value));

                },

                isNumber = function(n) {
                    // console.log(n);
                    return !isNaN(parseFloat(n)) && isFinite(n);
                },

                scopeOptions = {
                    disabled: '=?',
                    min: '=',
                    max: '=',
                    modelMin: '=?',
                    modelMax: '=?',
                    onHandleDown: '&', // calls optional function when handle is grabbed
                    onHandleUp: '&', // calls optional function when handle is released
                    orientation: '@', // options: horizontal | vertical | vertical left | vertical right
                    step: '@',
                    decimalPlaces: '@',
                    filter: '@',
                    filterOptions: '@',
                    showValues: '@',
                    pinHandle: '@',
                    preventEqualMinMax: '@',
                    attachHandleValues: '@',
                    getterSetter: '@' // Allow the use of getterSetters for model values
                };

            if (legacySupport) {
                // make optional properties required
                scopeOptions.disabled = '=';
                scopeOptions.modelMin = '=';
                scopeOptions.modelMax = '=';
            }

            // if (EVENT < 4) {
            //     // some sort of touch has been detected
            //     angular.element('html').addClass('ngrs-touch');
            // } else {
            //     angular.element('html').addClass('ngrs-no-touch');
            // }


            return {
                restrict: 'A',
                replace: true,
                template: ['<div class="ngrs-range-slider">',
                    '<div class="ngrs-runner">',
                    '<div class="ngrs-handle ngrs-handle-min"><i></i></div>',
                    '<div class="ngrs-handle ngrs-handle-max"><i></i></div>',
                    '<div class="ngrs-join"></div>',
                    '</div>',
                    '<div class="ngrs-value-runner">',
                    '<div class="ngrs-value ngrs-value-min" ng-show="showValues"><div>{{filteredModelMin}}</div></div>',
                    '<div class="ngrs-value ngrs-value-max" ng-show="showValues"><div>{{filteredModelMax}}</div></div>',
                    '</div>',
                    '</div>'
                ].join(''),
                scope: scopeOptions,
                link: function(scope, element, attrs, controller) {

                    /**
                     *  FIND ELEMENTS
                     */

                    var $slider = angular.element(element),
                        handles = [element.find('.ngrs-handle-min'), element.find('.ngrs-handle-max')],
                        values = [element.find('.ngrs-value-min'), element.find('.ngrs-value-max')],
                        join = element.find('.ngrs-join'),
                        pos = 'left',
                        posOpp = 'right',
                        orientation = 0,
                        allowedRange = [0, 0],
                        range = 0,
                        down = false;

                    // filtered
                    scope.filteredModelMin = modelMin();
                    scope.filteredModelMax = modelMax();

                    /**
                     *  FALL BACK TO DEFAULTS FOR SOME ATTRIBUTES
                     */

                    attrs.$observe('disabled', function(val) {
                        if (!angular.isDefined(val)) {
                            scope.disabled = defaults.disabled;
                        }

                        scope.$watch('disabled', setDisabledStatus);
                    });

                    attrs.$observe('orientation', function(val) {
                        if (!angular.isDefined(val)) {
                            scope.orientation = defaults.orientation;
                        }

                        var classNames = scope.orientation.split(' '),
                            useClass;

                        for (var i = 0, l = classNames.length; i < l; i++) {
                            classNames[i] = 'ngrs-' + classNames[i];
                        }

                        useClass = classNames.join(' ');

                        // add class to element
                        $slider.addClass(useClass);

                        // update pos
                        if (scope.orientation === 'vertical' || scope.orientation === 'vertical left' || scope.orientation === 'vertical right') {
                            pos = 'top';
                            posOpp = 'bottom';
                            orientation = 1;
                        }
                    });

                    attrs.$observe('step', function(val) {
                        if (!angular.isDefined(val)) {
                            scope.step = defaults.step;
                        }
                    });

                    attrs.$observe('decimalPlaces', function(val) {
                        if (!angular.isDefined(val)) {
                            scope.decimalPlaces = defaults.decimalPlaces;
                        }
                    });

                    attrs.$observe('showValues', function(val) {
                        if (!angular.isDefined(val)) {
                            scope.showValues = defaults.showValues;
                        } else {
                            if (val === 'false') {
                                scope.showValues = false;
                            } else {
                                scope.showValues = true;
                            }
                        }
                    });

                    attrs.$observe('pinHandle', function(val) {
                        if (!angular.isDefined(val)) {
                            scope.pinHandle = null;
                        } else {
                            if (val === 'min' || val === 'max') {
                                scope.pinHandle = val;
                            } else {
                                scope.pinHandle = null;
                            }
                        }

                        scope.$watch('pinHandle', setPinHandle);
                    });

                    attrs.$observe('preventEqualMinMax', function(val) {
                        if (!angular.isDefined(val)) {
                            scope.preventEqualMinMax = defaults.preventEqualMinMax;
                        } else {
                            if (val === 'false') {
                                scope.preventEqualMinMax = false;
                            } else {
                                scope.preventEqualMinMax = true;
                            }
                        }
                    });

                    attrs.$observe('attachHandleValues', function(val) {
                        if (!angular.isDefined(val)) {
                            scope.attachHandleValues = defaults.attachHandleValues;
                        } else {
                            if (val === 'true' || val === '') {
                                // flag as true
                                scope.attachHandleValues = true;
                                // add class to runner
                                element.find('.ngrs-value-runner').addClass('ngrs-attached-handles');
                            } else {
                                scope.attachHandleValues = false;
                            }
                        }
                    });

                    // GetterSetters for model values

                    function modelMin(newValue) {
                        if(scope.getterSetter) {
                            return arguments.length ? scope.modelMin(newValue) : scope.modelMin();
                        } else {
                            return arguments.length ? (scope.modelMin = newValue) : scope.modelMin;
                        }
                    }

                    function modelMax(newValue) {
                        if(scope.getterSetter) {
                            return arguments.length ? scope.modelMax(newValue) : scope.modelMax();
                        } else {
                            return arguments.length ? (scope.modelMax = newValue) : scope.modelMax;
                        }
                    }

                    // listen for changes to values
                    scope.$watch('min', setMinMax);
                    scope.$watch('max', setMinMax);

                    scope.$watch(function () {
                        return modelMin();
                    }, setModelMinMax);
                    scope.$watch(function () {
                        return modelMax();
                    }, setModelMinMax);

                    /**
                     * HANDLE CHANGES
                     */

                    function setPinHandle(status) {
                        if (status === "min") {
                            angular.element(handles[0]).css('display', 'none');
                            angular.element(handles[1]).css('display', 'block');
                        } else if (status === "max") {
                            angular.element(handles[0]).css('display', 'block');
                            angular.element(handles[1]).css('display', 'none');
                        } else {
                            angular.element(handles[0]).css('display', 'block');
                            angular.element(handles[1]).css('display', 'block');
                        }
                    }

                    function setDisabledStatus(status) {
                        if (status) {
                            $slider.addClass('ngrs-disabled');
                        } else {
                            $slider.removeClass('ngrs-disabled');
                        }
                    }

                    function setMinMax() {

                        if (scope.min > scope.max) {
                            throwError('min must be less than or equal to max');
                        }

                        // only do stuff when both values are ready
                        if (angular.isDefined(scope.min) && angular.isDefined(scope.max)) {

                            // make sure they are numbers
                            if (!isNumber(scope.min)) {
                                throwError('min must be a number');
                            }

                            if (!isNumber(scope.max)) {
                                throwError('max must be a number');
                            }

                            range = scope.max - scope.min;
                            allowedRange = [scope.min, scope.max];

                            // update models too
                            setModelMinMax();

                        }
                    }

                    function setModelMinMax() {

                        if (modelMin() > modelMax()) {
                            throwWarning('modelMin must be less than or equal to modelMax');
                            // reset values to correct
                            modelMin(modelMax());
                        }

                        // only do stuff when both values are ready
                        if (
                            (angular.isDefined(modelMin()) || scope.pinHandle === 'min') &&
                            (angular.isDefined(modelMax()) || scope.pinHandle === 'max')
                        ) {

                            // make sure they are numbers
                            if (!isNumber(modelMin())) {
                                if (scope.pinHandle !== 'min') {
                                    throwWarning('modelMin must be a number');
                                }
                                modelMin(scope.min);
                            }

                            if (!isNumber(modelMax())) {
                                if (scope.pinHandle !== 'max') {
                                    throwWarning('modelMax must be a number');
                                }
                                modelMax(scope.max);
                            }

                            var handle1pos = restrict(((modelMin() - scope.min) / range) * 100),
                                handle2pos = restrict(((modelMax() - scope.min) / range) * 100),
                                value1pos,
                                value2pos;

                            if (scope.attachHandleValues) {
                                value1pos = handle1pos;
                                value2pos = handle2pos;
                            }

                            // make sure the model values are within the allowed range
                            modelMin(Math.max(scope.min, modelMin()));
                            modelMax(Math.min(scope.max, modelMax()));

                            if (scope.filter && scope.filterOptions) {
                                scope.filteredModelMin = $filter(scope.filter)(modelMin(), scope.filterOptions);
                                scope.filteredModelMax = $filter(scope.filter)(modelMax(), scope.filterOptions);
                            } else if (scope.filter) {

                                var filterTokens = scope.filter.split(':'),
                                    filterName = scope.filter.split(':')[0],
                                    filterOptions = filterTokens.slice().slice(1),
                                    modelMinOptions,
                                    modelMaxOptions;

                                // properly parse string and number args
                                filterOptions = filterOptions.map(function (arg) {
                                    if (isNumber(arg)) {
                                        return +arg;
                                    } else if ((arg[0] == "\"" && arg[arg.length-1] == "\"") || (arg[0] == "\'" && arg[arg.length-1] == "\'")) {
                                        return arg.slice(1, -1);
                                    }
                                });

                                modelMinOptions = filterOptions.slice();
                                modelMaxOptions = filterOptions.slice();
                                modelMinOptions.unshift(modelMin());
                                modelMaxOptions.unshift(modelMax());

                                scope.filteredModelMin = $filter(filterName).apply(null, modelMinOptions);
                                scope.filteredModelMax = $filter(filterName).apply(null, modelMaxOptions);
                            } else {
                                scope.filteredModelMin = modelMin();
                                scope.filteredModelMax = modelMax();
                            }

                            // check for no range
                            if (scope.min === scope.max && modelMin() == modelMax()) {

                                // reposition handles
                                angular.element(handles[0]).css(pos, '0%');
                                angular.element(handles[1]).css(pos, '100%');

                                if (scope.attachHandleValues) {
                                    // reposition values
                                    angular.element(values[0]).css(pos, '0%');
                                    angular.element(values[1]).css(pos, '100%');
                                }

                                // reposition join
                                angular.element(join).css(pos, '0%').css(posOpp, '0%');

                            } else {

                                // reposition handles
                                angular.element(handles[0]).css(pos, handle1pos + '%');
                                angular.element(handles[1]).css(pos, handle2pos + '%');

                                if (scope.attachHandleValues) {
                                    // reposition values
                                    angular.element(values[0]).css(pos, value1pos + '%');
                                    angular.element(values[1]).css(pos, value2pos + '%');
                                    angular.element(values[1]).css(posOpp, 'auto');
                                }

                                // reposition join
                                angular.element(join).css(pos, handle1pos + '%').css(posOpp, (100 - handle2pos) + '%');

                                // ensure min handle can't be hidden behind max handle
                                if (handle1pos > 95) {
                                    angular.element(handles[0]).css('z-index', 3);
                                }
                            }

                        }

                    }

                    function handleMove(index) {

                        var $handle = handles[index];

                        // on mousedown / touchstart
                        $handle.bind(onEvent + 'X', function(event) {

                            var handleDownClass = (index === 0 ? 'ngrs-handle-min' : 'ngrs-handle-max') + '-down',
                                //unbind = $handle.add($document).add('body'),
                                modelValue = (index === 0 ? modelMin() : modelMax()) - scope.min,
                                originalPosition = (modelValue / range) * 100,
                                originalClick = client(event),
                                previousClick = originalClick,
                                previousProposal = false;

                            if (angular.isFunction(scope.onHandleDown)) {
                                scope.onHandleDown();
                            }

                            // stop user accidentally selecting stuff
                            angular.element('body').bind('selectstart' + eventNamespace, function() {
                                return false;
                            });

                            // only do stuff if we are disabled
                            if (!scope.disabled) {

                                // flag as down
                                down = true;

                                // add down class
                                $handle.addClass('ngrs-down');

                                $slider.addClass('ngrs-focus ' + handleDownClass);

                                // add touch class for MS styling
                                angular.element('body').addClass('ngrs-touching');

                                // listen for mousemove / touchmove document events
                                $document.bind(moveEvent, function(e) {
                                    // prevent default
                                    e.preventDefault();

                                    var currentClick = client(e),
                                        movement,
                                        proposal,
                                        other,
                                        per = (scope.step / range) * 100,
                                        otherModelPosition = (((index === 0 ? modelMax() : modelMin()) - scope.min) / range) * 100;

                                    if (currentClick[0] === "x") {
                                        return;
                                    }

                                    // calculate deltas
                                    currentClick[0] -= originalClick[0];
                                    currentClick[1] -= originalClick[1];

                                    // has movement occurred on either axis?
                                    movement = [
                                        (previousClick[0] !== currentClick[0]), (previousClick[1] !== currentClick[1])
                                    ];

                                    // propose a movement
                                    proposal = originalPosition + ((currentClick[orientation] * 100) / (orientation ? $slider.height() : $slider.width()));

                                    // normalize so it can't move out of bounds
                                    proposal = restrict(proposal);

                                    if (scope.preventEqualMinMax) {

                                        if (per === 0) {
                                            per = (1 / range) * 100; // restrict to 1
                                        }

                                        if (index === 0) {
                                            otherModelPosition = otherModelPosition - per;
                                        } else if (index === 1) {
                                            otherModelPosition = otherModelPosition + per;
                                        }
                                    }

                                    // check which handle is being moved and add / remove margin
                                    if (index === 0) {
                                        proposal = proposal > otherModelPosition ? otherModelPosition : proposal;
                                    } else if (index === 1) {
                                        proposal = proposal < otherModelPosition ? otherModelPosition : proposal;
                                    }

                                    if (scope.step > 0) {
                                        // only change if we are within the extremes, otherwise we get strange rounding
                                        if (proposal < 100 && proposal > 0) {
                                            proposal = Math.round(proposal / per) * per;
                                        }
                                    }

                                    if (proposal > 95 && index === 0) {
                                        $handle.css('z-index', 3);
                                    } else {
                                        $handle.css('z-index', '');
                                    }

                                    if (movement[orientation] && proposal != previousProposal) {

                                        if (index === 0) {

                                            // update model as we slide
                                            modelMin(parseFloat(parseFloat((((proposal * range) / 100) + scope.min)).toFixed(scope.decimalPlaces)));

                                        } else if (index === 1) {

                                            modelMax(parseFloat(parseFloat((((proposal * range) / 100) + scope.min)).toFixed(scope.decimalPlaces)));
                                        }

                                        // update angular
                                        scope.$apply();

                                        previousProposal = proposal;

                                    }

                                    previousClick = currentClick;

                                }).bind(offEvent, function() {

                                    if (angular.isFunction(scope.onHandleUp)) {
                                        scope.onHandleUp();
                                    }

                                    // unbind listeners
                                    $document.off(moveEvent);
                                    $document.off(offEvent);

                                    angular.element('body').removeClass('ngrs-touching');

                                    // cancel down flag
                                    down = false;

                                    // remove down and over class
                                    $handle.removeClass('ngrs-down');
                                    $handle.removeClass('ngrs-over');

                                    // remove active class
                                    $slider.removeClass('ngrs-focus ' + handleDownClass);

                                });
                            }

                        }).on(overEvent, function () {
                            $handle.addClass('ngrs-over');
                        }).on(outEvent, function () {
                            if (!down) {
                                $handle.removeClass('ngrs-over');
                            }
                        });
                    }

                    function throwError(message) {
                        scope.disabled = true;
                        throw new Error('RangeSlider: ' + message);
                    }

                    function throwWarning(message) {
                        $log.warn(message);
                    }

                    /**
                     * DESTROY
                     */

                    scope.$on('$destroy', function() {

                        // unbind event from slider
                        $slider.off(eventNamespace);

                        // unbind from body
                        angular.element('body').off(eventNamespace);

                        // unbind from document
                        $document.off(eventNamespace);

                        // unbind from handles
                        for (var i = 0, l = handles.length; i < l; i++) {
                            handles[i].off(eventNamespace);
                            handles[i].off(eventNamespace + 'X');
                        }

                    });

                    /**
                     * INIT
                     */

                    $slider
                    // disable selection
                        .bind('selectstart' + eventNamespace, function(event) {
                            return false;
                        })
                        // stop propagation
                        .bind('click', function(event) {
                            event.stopPropagation();
                        });

                    // bind events to each handle
                    handleMove(0);
                    handleMove(1);

                }
            };
        }]);

    // requestAnimationFramePolyFill
    // http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
    // shim layer with setTimeout fallback
    window.requestAnimFrame = (function() {
        return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            function(callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();
}());

/*!
 * Masonry PACKAGED v4.0.0
 * Cascading grid layout library
 * http://masonry.desandro.com
 * MIT License
 * by David DeSandro
 */

!function(t,e){"use strict";"function"==typeof define&&define.amd?define("jquery-bridget/jquery-bridget",["jquery"],function(i){e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("jquery")):t.jQueryBridget=e(t,t.jQuery)}(window,function(t,e){"use strict";function i(i,r,a){function h(t,e,n){var o,r="$()."+i+'("'+e+'")';return t.each(function(t,h){var u=a.data(h,i);if(!u)return void s(i+" not initialized. Cannot call methods, i.e. "+r);var d=u[e];if(!d||"_"==e.charAt(0))return void s(r+" is not a valid method");var c=d.apply(u,n);o=void 0===o?c:o}),void 0!==o?o:t}function u(t,e){t.each(function(t,n){var o=a.data(n,i);o?(o.option(e),o._init()):(o=new r(n,e),a.data(n,i,o))})}a=a||e||t.jQuery,a&&(r.prototype.option||(r.prototype.option=function(t){a.isPlainObject(t)&&(this.options=a.extend(!0,this.options,t))}),a.fn[i]=function(t){if("string"==typeof t){var e=o.call(arguments,1);return h(this,t,e)}return u(this,t),this},n(a))}function n(t){!t||t&&t.bridget||(t.bridget=i)}var o=Array.prototype.slice,r=t.console,s="undefined"==typeof r?function(){}:function(t){r.error(t)};return n(e||t.jQuery),i}),function(t,e){"function"==typeof define&&define.amd?define("ev-emitter/ev-emitter",e):"object"==typeof module&&module.exports?module.exports=e():t.EvEmitter=e()}(this,function(){function t(){}var e=t.prototype;return e.on=function(t,e){if(t&&e){var i=this._events=this._events||{},n=i[t]=i[t]||[];return-1==n.indexOf(e)&&n.push(e),this}},e.once=function(t,e){if(t&&e){this.on(t,e);var i=this._onceEvents=this._onceEvents||{},n=i[t]=i[t]||[];return n[e]=!0,this}},e.off=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var n=i.indexOf(e);return-1!=n&&i.splice(n,1),this}},e.emitEvent=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var n=0,o=i[n];e=e||[];for(var r=this._onceEvents&&this._onceEvents[t];o;){var s=r&&r[o];s&&(this.off(t,o),delete r[o]),o.apply(this,e),n+=s?0:1,o=i[n]}return this}},t}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("get-size/get-size",[],function(){return e()}):"object"==typeof module&&module.exports?module.exports=e():t.getSize=e()}(window,function(){"use strict";function t(t){var e=parseFloat(t),i=-1==t.indexOf("%")&&!isNaN(e);return i&&e}function e(){}function i(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0;u>e;e++){var i=h[e];t[i]=0}return t}function n(t){var e=getComputedStyle(t);return e||a("Style returned "+e+". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"),e}function o(){if(!d){d=!0;var e=document.createElement("div");e.style.width="200px",e.style.padding="1px 2px 3px 4px",e.style.borderStyle="solid",e.style.borderWidth="1px 2px 3px 4px",e.style.boxSizing="border-box";var i=document.body||document.documentElement;i.appendChild(e);var o=n(e);r.isBoxSizeOuter=s=200==t(o.width),i.removeChild(e)}}function r(e){if(o(),"string"==typeof e&&(e=document.querySelector(e)),e&&"object"==typeof e&&e.nodeType){var r=n(e);if("none"==r.display)return i();var a={};a.width=e.offsetWidth,a.height=e.offsetHeight;for(var d=a.isBorderBox="border-box"==r.boxSizing,c=0;u>c;c++){var l=h[c],f=r[l],m=parseFloat(f);a[l]=isNaN(m)?0:m}var p=a.paddingLeft+a.paddingRight,g=a.paddingTop+a.paddingBottom,y=a.marginLeft+a.marginRight,v=a.marginTop+a.marginBottom,_=a.borderLeftWidth+a.borderRightWidth,E=a.borderTopWidth+a.borderBottomWidth,z=d&&s,b=t(r.width);b!==!1&&(a.width=b+(z?0:p+_));var x=t(r.height);return x!==!1&&(a.height=x+(z?0:g+E)),a.innerWidth=a.width-(p+_),a.innerHeight=a.height-(g+E),a.outerWidth=a.width+y,a.outerHeight=a.height+v,a}}var s,a="undefined"==typeof console?e:function(t){console.error(t)},h=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"],u=h.length,d=!1;return r}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("matches-selector/matches-selector",e):"object"==typeof module&&module.exports?module.exports=e():t.matchesSelector=e()}(window,function(){"use strict";var t=function(){var t=Element.prototype;if(t.matches)return"matches";if(t.matchesSelector)return"matchesSelector";for(var e=["webkit","moz","ms","o"],i=0;i<e.length;i++){var n=e[i],o=n+"MatchesSelector";if(t[o])return o}}();return function(e,i){return e[t](i)}}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("fizzy-ui-utils/utils",["matches-selector/matches-selector"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("desandro-matches-selector")):t.fizzyUIUtils=e(t,t.matchesSelector)}(window,function(t,e){var i={};i.extend=function(t,e){for(var i in e)t[i]=e[i];return t},i.modulo=function(t,e){return(t%e+e)%e},i.makeArray=function(t){var e=[];if(Array.isArray(t))e=t;else if(t&&"number"==typeof t.length)for(var i=0;i<t.length;i++)e.push(t[i]);else e.push(t);return e},i.removeFrom=function(t,e){var i=t.indexOf(e);-1!=i&&t.splice(i,1)},i.getParent=function(t,i){for(;t!=document.body;)if(t=t.parentNode,e(t,i))return t},i.getQueryElement=function(t){return"string"==typeof t?document.querySelector(t):t},i.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},i.filterFindElements=function(t,n){t=i.makeArray(t);var o=[];return t.forEach(function(t){if(t instanceof HTMLElement){if(!n)return void o.push(t);e(t,n)&&o.push(t);for(var i=t.querySelectorAll(n),r=0;r<i.length;r++)o.push(i[r])}}),o},i.debounceMethod=function(t,e,i){var n=t.prototype[e],o=e+"Timeout";t.prototype[e]=function(){var t=this[o];t&&clearTimeout(t);var e=arguments,r=this;this[o]=setTimeout(function(){n.apply(r,e),delete r[o]},i||100)}},i.docReady=function(t){"complete"==document.readyState?t():document.addEventListener("DOMContentLoaded",t)},i.toDashed=function(t){return t.replace(/(.)([A-Z])/g,function(t,e,i){return e+"-"+i}).toLowerCase()};var n=t.console;return i.htmlInit=function(e,o){i.docReady(function(){var r=i.toDashed(o),s="data-"+r,a=document.querySelectorAll("["+s+"]"),h=document.querySelectorAll(".js-"+r),u=i.makeArray(a).concat(i.makeArray(h)),d=s+"-options",c=t.jQuery;u.forEach(function(t){var i,r=t.getAttribute(s)||t.getAttribute(d);try{i=r&&JSON.parse(r)}catch(a){return void(n&&n.error("Error parsing "+s+" on "+t.className+": "+a))}var h=new e(t,i);c&&c.data(t,o,h)})})},i}),function(t,e){"function"==typeof define&&define.amd?define("outlayer/item",["ev-emitter/ev-emitter","get-size/get-size"],function(i,n){return e(t,i,n)}):"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter"),require("get-size")):(t.Outlayer={},t.Outlayer.Item=e(t,t.EvEmitter,t.getSize))}(window,function(t,e,i){"use strict";function n(t){for(var e in t)return!1;return e=null,!0}function o(t,e){t&&(this.element=t,this.layout=e,this.position={x:0,y:0},this._create())}function r(t){return t.replace(/([A-Z])/g,function(t){return"-"+t.toLowerCase()})}var s=document.documentElement.style,a="string"==typeof s.transition?"transition":"WebkitTransition",h="string"==typeof s.transform?"transform":"WebkitTransform",u={WebkitTransition:"webkitTransitionEnd",transition:"transitionend"}[a],d=[h,a,a+"Duration",a+"Property"],c=o.prototype=Object.create(e.prototype);c.constructor=o,c._create=function(){this._transn={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},c.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},c.getSize=function(){this.size=i(this.element)},c.css=function(t){var e=this.element.style;for(var i in t){var n=d[i]||i;e[n]=t[i]}},c.getPosition=function(){var t=getComputedStyle(this.element),e=this.layout._getOption("originLeft"),i=this.layout._getOption("originTop"),n=t[e?"left":"right"],o=t[i?"top":"bottom"],r=this.layout.size,s=-1!=n.indexOf("%")?parseFloat(n)/100*r.width:parseInt(n,10),a=-1!=o.indexOf("%")?parseFloat(o)/100*r.height:parseInt(o,10);s=isNaN(s)?0:s,a=isNaN(a)?0:a,s-=e?r.paddingLeft:r.paddingRight,a-=i?r.paddingTop:r.paddingBottom,this.position.x=s,this.position.y=a},c.layoutPosition=function(){var t=this.layout.size,e={},i=this.layout._getOption("originLeft"),n=this.layout._getOption("originTop"),o=i?"paddingLeft":"paddingRight",r=i?"left":"right",s=i?"right":"left",a=this.position.x+t[o];e[r]=this.getXValue(a),e[s]="";var h=n?"paddingTop":"paddingBottom",u=n?"top":"bottom",d=n?"bottom":"top",c=this.position.y+t[h];e[u]=this.getYValue(c),e[d]="",this.css(e),this.emitEvent("layout",[this])},c.getXValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&!e?t/this.layout.size.width*100+"%":t+"px"},c.getYValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&e?t/this.layout.size.height*100+"%":t+"px"},c._transitionTo=function(t,e){this.getPosition();var i=this.position.x,n=this.position.y,o=parseInt(t,10),r=parseInt(e,10),s=o===this.position.x&&r===this.position.y;if(this.setPosition(t,e),s&&!this.isTransitioning)return void this.layoutPosition();var a=t-i,h=e-n,u={};u.transform=this.getTranslate(a,h),this.transition({to:u,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},c.getTranslate=function(t,e){var i=this.layout._getOption("originLeft"),n=this.layout._getOption("originTop");return t=i?t:-t,e=n?e:-e,"translate3d("+t+"px, "+e+"px, 0)"},c.goTo=function(t,e){this.setPosition(t,e),this.layoutPosition()},c.moveTo=c._transitionTo,c.setPosition=function(t,e){this.position.x=parseInt(t,10),this.position.y=parseInt(e,10)},c._nonTransition=function(t){this.css(t.to),t.isCleaning&&this._removeStyles(t.to);for(var e in t.onTransitionEnd)t.onTransitionEnd[e].call(this)},c._transition=function(t){if(!parseFloat(this.layout.options.transitionDuration))return void this._nonTransition(t);var e=this._transn;for(var i in t.onTransitionEnd)e.onEnd[i]=t.onTransitionEnd[i];for(i in t.to)e.ingProperties[i]=!0,t.isCleaning&&(e.clean[i]=!0);if(t.from){this.css(t.from);var n=this.element.offsetHeight;n=null}this.enableTransition(t.to),this.css(t.to),this.isTransitioning=!0};var l="opacity,"+r(d.transform||"transform");c.enableTransition=function(){this.isTransitioning||(this.css({transitionProperty:l,transitionDuration:this.layout.options.transitionDuration}),this.element.addEventListener(u,this,!1))},c.transition=o.prototype[a?"_transition":"_nonTransition"],c.onwebkitTransitionEnd=function(t){this.ontransitionend(t)},c.onotransitionend=function(t){this.ontransitionend(t)};var f={"-webkit-transform":"transform"};c.ontransitionend=function(t){if(t.target===this.element){var e=this._transn,i=f[t.propertyName]||t.propertyName;if(delete e.ingProperties[i],n(e.ingProperties)&&this.disableTransition(),i in e.clean&&(this.element.style[t.propertyName]="",delete e.clean[i]),i in e.onEnd){var o=e.onEnd[i];o.call(this),delete e.onEnd[i]}this.emitEvent("transitionEnd",[this])}},c.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(u,this,!1),this.isTransitioning=!1},c._removeStyles=function(t){var e={};for(var i in t)e[i]="";this.css(e)};var m={transitionProperty:"",transitionDuration:""};return c.removeTransitionStyles=function(){this.css(m)},c.removeElem=function(){this.element.parentNode.removeChild(this.element),this.css({display:""}),this.emitEvent("remove",[this])},c.remove=function(){return a&&parseFloat(this.layout.options.transitionDuration)?(this.once("transitionEnd",function(){this.removeElem()}),void this.hide()):void this.removeElem()},c.reveal=function(){delete this.isHidden,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("visibleStyle");e[i]=this.onRevealTransitionEnd,this.transition({from:t.hiddenStyle,to:t.visibleStyle,isCleaning:!0,onTransitionEnd:e})},c.onRevealTransitionEnd=function(){this.isHidden||this.emitEvent("reveal")},c.getHideRevealTransitionEndProperty=function(t){var e=this.layout.options[t];if(e.opacity)return"opacity";for(var i in e)return i},c.hide=function(){this.isHidden=!0,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("hiddenStyle");e[i]=this.onHideTransitionEnd,this.transition({from:t.visibleStyle,to:t.hiddenStyle,isCleaning:!0,onTransitionEnd:e})},c.onHideTransitionEnd=function(){this.isHidden&&(this.css({display:"none"}),this.emitEvent("hide"))},c.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},o}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("outlayer/outlayer",["ev-emitter/ev-emitter","get-size/get-size","fizzy-ui-utils/utils","./item"],function(i,n,o,r){return e(t,i,n,o,r)}):"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter"),require("get-size"),require("fizzy-ui-utils"),require("./item")):t.Outlayer=e(t,t.EvEmitter,t.getSize,t.fizzyUIUtils,t.Outlayer.Item)}(window,function(t,e,i,n,o){"use strict";function r(t,e){var i=n.getQueryElement(t);if(!i)return void(a&&a.error("Bad element for "+this.constructor.namespace+": "+(i||t)));this.element=i,h&&(this.$element=h(this.element)),this.options=n.extend({},this.constructor.defaults),this.option(e);var o=++d;this.element.outlayerGUID=o,c[o]=this,this._create();var r=this._getOption("initLayout");r&&this.layout()}function s(t){function e(){t.apply(this,arguments)}return e.prototype=Object.create(t.prototype),e.prototype.constructor=e,e}var a=t.console,h=t.jQuery,u=function(){},d=0,c={};r.namespace="outlayer",r.Item=o,r.defaults={containerStyle:{position:"relative"},initLayout:!0,originLeft:!0,originTop:!0,resize:!0,resizeContainer:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}};var l=r.prototype;return n.extend(l,e.prototype),l.option=function(t){n.extend(this.options,t)},l._getOption=function(t){var e=this.constructor.compatOptions[t];return e&&void 0!==this.options[e]?this.options[e]:this.options[t]},r.compatOptions={initLayout:"isInitLayout",horizontal:"isHorizontal",layoutInstant:"isLayoutInstant",originLeft:"isOriginLeft",originTop:"isOriginTop",resize:"isResizeBound",resizeContainer:"isResizingContainer"},l._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),n.extend(this.element.style,this.options.containerStyle);var t=this._getOption("resize");t&&this.bindResize()},l.reloadItems=function(){this.items=this._itemize(this.element.children)},l._itemize=function(t){for(var e=this._filterFindItemElements(t),i=this.constructor.Item,n=[],o=0;o<e.length;o++){var r=e[o],s=new i(r,this);n.push(s)}return n},l._filterFindItemElements=function(t){return n.filterFindElements(t,this.options.itemSelector)},l.getItemElements=function(){return this.items.map(function(t){return t.element})},l.layout=function(){this._resetLayout(),this._manageStamps();var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;this.layoutItems(this.items,e),this._isLayoutInited=!0},l._init=l.layout,l._resetLayout=function(){this.getSize()},l.getSize=function(){this.size=i(this.element)},l._getMeasurement=function(t,e){var n,o=this.options[t];o?("string"==typeof o?n=this.element.querySelector(o):o instanceof HTMLElement&&(n=o),this[t]=n?i(n)[e]:o):this[t]=0},l.layoutItems=function(t,e){t=this._getItemsForLayout(t),this._layoutItems(t,e),this._postLayout()},l._getItemsForLayout=function(t){return t.filter(function(t){return!t.isIgnored})},l._layoutItems=function(t,e){if(this._emitCompleteOnItems("layout",t),t&&t.length){var i=[];t.forEach(function(t){var n=this._getItemLayoutPosition(t);n.item=t,n.isInstant=e||t.isLayoutInstant,i.push(n)},this),this._processLayoutQueue(i)}},l._getItemLayoutPosition=function(){return{x:0,y:0}},l._processLayoutQueue=function(t){t.forEach(function(t){this._positionItem(t.item,t.x,t.y,t.isInstant)},this)},l._positionItem=function(t,e,i,n){n?t.goTo(e,i):t.moveTo(e,i)},l._postLayout=function(){this.resizeContainer()},l.resizeContainer=function(){var t=this._getOption("resizeContainer");if(t){var e=this._getContainerSize();e&&(this._setContainerMeasure(e.width,!0),this._setContainerMeasure(e.height,!1))}},l._getContainerSize=u,l._setContainerMeasure=function(t,e){if(void 0!==t){var i=this.size;i.isBorderBox&&(t+=e?i.paddingLeft+i.paddingRight+i.borderLeftWidth+i.borderRightWidth:i.paddingBottom+i.paddingTop+i.borderTopWidth+i.borderBottomWidth),t=Math.max(t,0),this.element.style[e?"width":"height"]=t+"px"}},l._emitCompleteOnItems=function(t,e){function i(){o.dispatchEvent(t+"Complete",null,[e])}function n(){s++,s==r&&i()}var o=this,r=e.length;if(!e||!r)return void i();var s=0;e.forEach(function(e){e.once(t,n)})},l.dispatchEvent=function(t,e,i){var n=e?[e].concat(i):i;if(this.emitEvent(t,n),h)if(this.$element=this.$element||h(this.element),e){var o=h.Event(e);o.type=t,this.$element.trigger(o,i)}else this.$element.trigger(t,i)},l.ignore=function(t){var e=this.getItem(t);e&&(e.isIgnored=!0)},l.unignore=function(t){var e=this.getItem(t);e&&delete e.isIgnored},l.stamp=function(t){t=this._find(t),t&&(this.stamps=this.stamps.concat(t),t.forEach(this.ignore,this))},l.unstamp=function(t){t=this._find(t),t&&t.forEach(function(t){n.removeFrom(this.stamps,t),this.unignore(t)},this)},l._find=function(t){return t?("string"==typeof t&&(t=this.element.querySelectorAll(t)),t=n.makeArray(t)):void 0},l._manageStamps=function(){this.stamps&&this.stamps.length&&(this._getBoundingRect(),this.stamps.forEach(this._manageStamp,this))},l._getBoundingRect=function(){var t=this.element.getBoundingClientRect(),e=this.size;this._boundingRect={left:t.left+e.paddingLeft+e.borderLeftWidth,top:t.top+e.paddingTop+e.borderTopWidth,right:t.right-(e.paddingRight+e.borderRightWidth),bottom:t.bottom-(e.paddingBottom+e.borderBottomWidth)}},l._manageStamp=u,l._getElementOffset=function(t){var e=t.getBoundingClientRect(),n=this._boundingRect,o=i(t),r={left:e.left-n.left-o.marginLeft,top:e.top-n.top-o.marginTop,right:n.right-e.right-o.marginRight,bottom:n.bottom-e.bottom-o.marginBottom};return r},l.handleEvent=n.handleEvent,l.bindResize=function(){t.addEventListener("resize",this),this.isResizeBound=!0},l.unbindResize=function(){t.removeEventListener("resize",this),this.isResizeBound=!1},l.onresize=function(){this.resize()},n.debounceMethod(r,"onresize",100),l.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&this.layout()},l.needsResizeLayout=function(){var t=i(this.element),e=this.size&&t;return e&&t.innerWidth!==this.size.innerWidth},l.addItems=function(t){var e=this._itemize(t);return e.length&&(this.items=this.items.concat(e)),e},l.appended=function(t){var e=this.addItems(t);e.length&&(this.layoutItems(e,!0),this.reveal(e))},l.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps(),this.layoutItems(e,!0),this.reveal(e),this.layoutItems(i)}},l.reveal=function(t){this._emitCompleteOnItems("reveal",t),t&&t.length&&t.forEach(function(t){t.reveal()})},l.hide=function(t){this._emitCompleteOnItems("hide",t),t&&t.length&&t.forEach(function(t){t.hide()})},l.revealItemElements=function(t){var e=this.getItems(t);this.reveal(e)},l.hideItemElements=function(t){var e=this.getItems(t);this.hide(e)},l.getItem=function(t){for(var e=0;e<this.items.length;e++){var i=this.items[e];if(i.element==t)return i}},l.getItems=function(t){t=n.makeArray(t);var e=[];return t.forEach(function(t){var i=this.getItem(t);i&&e.push(i)},this),e},l.remove=function(t){var e=this.getItems(t);this._emitCompleteOnItems("remove",e),e&&e.length&&e.forEach(function(t){t.remove(),n.removeFrom(this.items,t)},this)},l.destroy=function(){var t=this.element.style;t.height="",t.position="",t.width="",this.items.forEach(function(t){t.destroy()}),this.unbindResize();var e=this.element.outlayerGUID;delete c[e],delete this.element.outlayerGUID,h&&h.removeData(this.element,this.constructor.namespace)},r.data=function(t){t=n.getQueryElement(t);var e=t&&t.outlayerGUID;return e&&c[e]},r.create=function(t,e){var i=s(r);return i.defaults=n.extend({},r.defaults),n.extend(i.defaults,e),i.compatOptions=n.extend({},r.compatOptions),i.namespace=t,i.data=r.data,i.Item=s(o),n.htmlInit(i,t),h&&h.bridget&&h.bridget(t,i),i},r.Item=o,r}),function(t,e){"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("outlayer"),require("get-size")):t.Masonry=e(t.Outlayer,t.getSize)}(window,function(t,e){var i=t.create("masonry");return i.compatOptions.fitWidth="isFitWidth",i.prototype._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns(),this.colYs=[];for(var t=0;t<this.cols;t++)this.colYs.push(0);this.maxY=0},i.prototype.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var t=this.items[0],i=t&&t.element;this.columnWidth=i&&e(i).outerWidth||this.containerWidth}var n=this.columnWidth+=this.gutter,o=this.containerWidth+this.gutter,r=o/n,s=n-o%n,a=s&&1>s?"round":"floor";r=Math[a](r),this.cols=Math.max(r,1)},i.prototype.getContainerWidth=function(){var t=this._getOption("fitWidth"),i=t?this.element.parentNode:this.element,n=e(i);this.containerWidth=n&&n.innerWidth},i.prototype._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth%this.columnWidth,i=e&&1>e?"round":"ceil",n=Math[i](t.size.outerWidth/this.columnWidth);n=Math.min(n,this.cols);for(var o=this._getColGroup(n),r=Math.min.apply(Math,o),s=o.indexOf(r),a={x:this.columnWidth*s,y:r},h=r+t.size.outerHeight,u=this.cols+1-o.length,d=0;u>d;d++)this.colYs[s+d]=h;return a},i.prototype._getColGroup=function(t){if(2>t)return this.colYs;for(var e=[],i=this.cols+1-t,n=0;i>n;n++){var o=this.colYs.slice(n,n+t);e[n]=Math.max.apply(Math,o)}return e},i.prototype._manageStamp=function(t){var i=e(t),n=this._getElementOffset(t),o=this._getOption("originLeft"),r=o?n.left:n.right,s=r+i.outerWidth,a=Math.floor(r/this.columnWidth);a=Math.max(0,a);var h=Math.floor(s/this.columnWidth);h-=s%this.columnWidth?0:1,h=Math.min(this.cols-1,h);for(var u=this._getOption("originTop"),d=(u?n.top:n.bottom)+i.outerHeight,c=a;h>=c;c++)this.colYs[c]=Math.max(d,this.colYs[c])},i.prototype._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var t={height:this.maxY};return this._getOption("fitWidth")&&(t.width=this._getContainerFitWidth()),t},i.prototype._getContainerFitWidth=function(){for(var t=0,e=this.cols;--e&&0===this.colYs[e];)t++;return(this.cols-t)*this.columnWidth-this.gutter},i.prototype.needsResizeLayout=function(){var t=this.containerWidth;return this.getContainerWidth(),t!=this.containerWidth},i});

/*!
 * imagesLoaded v4.1.0
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */

( function( window, factory ) { 'use strict';
  // universal module definition

  /*global define: false, module: false, require: false */

  if ( typeof define == 'function' && define.amd ) {
    // AMD
    define( [
      'ev-emitter/ev-emitter'
    ], function( EvEmitter ) {
      return factory( window, EvEmitter );
    });
  } else if ( typeof module == 'object' && module.exports ) {
    // CommonJS
    module.exports = factory(
      window,
      require('ev-emitter')
    );
  } else {
    // browser global
    window.imagesLoaded = factory(
      window,
      window.EvEmitter
    );
  }

})( window,

// --------------------------  factory -------------------------- //

function factory( window, EvEmitter ) {

'use strict';

var $ = window.jQuery;
var console = window.console;

// -------------------------- helpers -------------------------- //

// extend objects
function extend( a, b ) {
  for ( var prop in b ) {
    a[ prop ] = b[ prop ];
  }
  return a;
}

// turn element or nodeList into an array
function makeArray( obj ) {
  var ary = [];
  if ( Array.isArray( obj ) ) {
    // use object if already an array
    ary = obj;
  } else if ( typeof obj.length == 'number' ) {
    // convert nodeList to array
    for ( var i=0; i < obj.length; i++ ) {
      ary.push( obj[i] );
    }
  } else {
    // array of single index
    ary.push( obj );
  }
  return ary;
}

// -------------------------- imagesLoaded -------------------------- //

/**
 * @param {Array, Element, NodeList, String} elem
 * @param {Object or Function} options - if function, use as callback
 * @param {Function} onAlways - callback function
 */
function ImagesLoaded( elem, options, onAlways ) {
  // coerce ImagesLoaded() without new, to be new ImagesLoaded()
  if ( !( this instanceof ImagesLoaded ) ) {
    return new ImagesLoaded( elem, options, onAlways );
  }
  // use elem as selector string
  if ( typeof elem == 'string' ) {
    elem = document.querySelectorAll( elem );
  }

  this.elements = makeArray( elem );
  this.options = extend( {}, this.options );

  if ( typeof options == 'function' ) {
    onAlways = options;
  } else {
    extend( this.options, options );
  }

  if ( onAlways ) {
    this.on( 'always', onAlways );
  }

  this.getImages();

  if ( $ ) {
    // add jQuery Deferred object
    this.jqDeferred = new $.Deferred();
  }

  // HACK check async to allow time to bind listeners
  setTimeout( function() {
    this.check();
  }.bind( this ));
}

ImagesLoaded.prototype = Object.create( EvEmitter.prototype );

ImagesLoaded.prototype.options = {};

ImagesLoaded.prototype.getImages = function() {
  this.images = [];

  // filter & find items if we have an item selector
  this.elements.forEach( this.addElementImages, this );
};

/**
 * @param {Node} element
 */
ImagesLoaded.prototype.addElementImages = function( elem ) {
  // filter siblings
  if ( elem.nodeName == 'IMG' ) {
    this.addImage( elem );
  }
  // get background image on element
  if ( this.options.background === true ) {
    this.addElementBackgroundImages( elem );
  }

  // find children
  // no non-element nodes, #143
  var nodeType = elem.nodeType;
  if ( !nodeType || !elementNodeTypes[ nodeType ] ) {
    return;
  }
  var childImgs = elem.querySelectorAll('img');
  // concat childElems to filterFound array
  for ( var i=0; i < childImgs.length; i++ ) {
    var img = childImgs[i];
    this.addImage( img );
  }

  // get child background images
  if ( typeof this.options.background == 'string' ) {
    var children = elem.querySelectorAll( this.options.background );
    for ( i=0; i < children.length; i++ ) {
      var child = children[i];
      this.addElementBackgroundImages( child );
    }
  }
};

var elementNodeTypes = {
  1: true,
  9: true,
  11: true
};

ImagesLoaded.prototype.addElementBackgroundImages = function( elem ) {
  var style = getComputedStyle( elem );
  if ( !style ) {
    // Firefox returns null if in a hidden iframe https://bugzil.la/548397
    return;
  }
  // get url inside url("...")
  var reURL = /url\((['"])?(.*?)\1\)/gi;
  var matches = reURL.exec( style.backgroundImage );
  while ( matches !== null ) {
    var url = matches && matches[2];
    if ( url ) {
      this.addBackground( url, elem );
    }
    matches = reURL.exec( style.backgroundImage );
  }
};

/**
 * @param {Image} img
 */
ImagesLoaded.prototype.addImage = function( img ) {
  var loadingImage = new LoadingImage( img );
  this.images.push( loadingImage );
};

ImagesLoaded.prototype.addBackground = function( url, elem ) {
  var background = new Background( url, elem );
  this.images.push( background );
};

ImagesLoaded.prototype.check = function() {
  var _this = this;
  this.progressedCount = 0;
  this.hasAnyBroken = false;
  // complete if no images
  if ( !this.images.length ) {
    this.complete();
    return;
  }

  function onProgress( image, elem, message ) {
    // HACK - Chrome triggers event before object properties have changed. #83
    setTimeout( function() {
      _this.progress( image, elem, message );
    });
  }

  this.images.forEach( function( loadingImage ) {
    loadingImage.once( 'progress', onProgress );
    loadingImage.check();
  });
};

ImagesLoaded.prototype.progress = function( image, elem, message ) {
  this.progressedCount++;
  this.hasAnyBroken = this.hasAnyBroken || !image.isLoaded;
  // progress event
  this.emitEvent( 'progress', [ this, image, elem ] );
  if ( this.jqDeferred && this.jqDeferred.notify ) {
    this.jqDeferred.notify( this, image );
  }
  // check if completed
  if ( this.progressedCount == this.images.length ) {
    this.complete();
  }

  if ( this.options.debug && console ) {
    console.log( 'progress: ' + message, image, elem );
  }
};

ImagesLoaded.prototype.complete = function() {
  var eventName = this.hasAnyBroken ? 'fail' : 'done';
  this.isComplete = true;
  this.emitEvent( eventName, [ this ] );
  this.emitEvent( 'always', [ this ] );
  if ( this.jqDeferred ) {
    var jqMethod = this.hasAnyBroken ? 'reject' : 'resolve';
    this.jqDeferred[ jqMethod ]( this );
  }
};

// --------------------------  -------------------------- //

function LoadingImage( img ) {
  this.img = img;
}

LoadingImage.prototype = Object.create( EvEmitter.prototype );

LoadingImage.prototype.check = function() {
  // If complete is true and browser supports natural sizes,
  // try to check for image status manually.
  var isComplete = this.getIsImageComplete();
  if ( isComplete ) {
    // report based on naturalWidth
    this.confirm( this.img.naturalWidth !== 0, 'naturalWidth' );
    return;
  }

  // If none of the checks above matched, simulate loading on detached element.
  this.proxyImage = new Image();
  this.proxyImage.addEventListener( 'load', this );
  this.proxyImage.addEventListener( 'error', this );
  // bind to image as well for Firefox. #191
  this.img.addEventListener( 'load', this );
  this.img.addEventListener( 'error', this );
  this.proxyImage.src = this.img.src;
};

LoadingImage.prototype.getIsImageComplete = function() {
  return this.img.complete && this.img.naturalWidth !== undefined;
};

LoadingImage.prototype.confirm = function( isLoaded, message ) {
  this.isLoaded = isLoaded;
  this.emitEvent( 'progress', [ this, this.img, message ] );
};

// ----- events ----- //

// trigger specified handler for event type
LoadingImage.prototype.handleEvent = function( event ) {
  var method = 'on' + event.type;
  if ( this[ method ] ) {
    this[ method ]( event );
  }
};

LoadingImage.prototype.onload = function() {
  this.confirm( true, 'onload' );
  this.unbindEvents();
};

LoadingImage.prototype.onerror = function() {
  this.confirm( false, 'onerror' );
  this.unbindEvents();
};

LoadingImage.prototype.unbindEvents = function() {
  this.proxyImage.removeEventListener( 'load', this );
  this.proxyImage.removeEventListener( 'error', this );
  this.img.removeEventListener( 'load', this );
  this.img.removeEventListener( 'error', this );
};

// -------------------------- Background -------------------------- //

function Background( url, element ) {
  this.url = url;
  this.element = element;
  this.img = new Image();
}

// inherit LoadingImage prototype
Background.prototype = Object.create( LoadingImage.prototype );

Background.prototype.check = function() {
  this.img.addEventListener( 'load', this );
  this.img.addEventListener( 'error', this );
  this.img.src = this.url;
  // check if image is already complete
  var isComplete = this.getIsImageComplete();
  if ( isComplete ) {
    this.confirm( this.img.naturalWidth !== 0, 'naturalWidth' );
    this.unbindEvents();
  }
};

Background.prototype.unbindEvents = function() {
  this.img.removeEventListener( 'load', this );
  this.img.removeEventListener( 'error', this );
};

Background.prototype.confirm = function( isLoaded, message ) {
  this.isLoaded = isLoaded;
  this.emitEvent( 'progress', [ this, this.element, message ] );
};

// -------------------------- jQuery -------------------------- //

ImagesLoaded.makeJQueryPlugin = function( jQuery ) {
  jQuery = jQuery || window.jQuery;
  if ( !jQuery ) {
    return;
  }
  // set local variable
  $ = jQuery;
  // $().imagesLoaded()
  $.fn.imagesLoaded = function( options, callback ) {
    var instance = new ImagesLoaded( this, options, callback );
    return instance.jqDeferred.promise( $(this) );
  };
};
// try making plugin
ImagesLoaded.makeJQueryPlugin();

// --------------------------  -------------------------- //

return ImagesLoaded;

});
angular.module("angular-preload-image",[]);angular.module("angular-preload-image").factory("preLoader",function(){return function(e,t,n){angular.element(new Image).bind("load",function(){t()}).bind("error",function(){n()}).attr("src",e)}});angular.module("angular-preload-image").directive("preloadImage",["preLoader",function(e){return{restrict:"A",terminal:true,priority:100,link:function(t,n,r){var i=r.ngSrc;t.default=r.defaultImage||"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wEWEygNWiLqlwAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAMSURBVAjXY/j//z8ABf4C/tzMWecAAAAASUVORK5CYII=";r.$set("src",t.default);e(i,function(){r.$set("src",i)},function(){if(r.fallbackImage!=undefined){r.$set("src",r.fallbackImage)}})}}}]);angular.module("angular-preload-image").directive("preloadBgImage",["preLoader",function(e){return{restrict:"A",link:function(t,n,r){if(r.preloadBgImage!=undefined){t.default=r.defaultImage||"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wEWEygNWiLqlwAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAMSURBVAjXY/j//z8ABf4C/tzMWecAAAAASUVORK5CYII=";n.css({"background-image":'url("'+t.default+'")'});e(r.preloadBgImage,function(){n.css({"background-image":'url("'+r.preloadBgImage+'")'})},function(){if(r.fallbackImage!=undefined){n.css({"background-image":'url("'+r.fallbackImage+'")'})}})}}}}])
'use strict'

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/

/**
* App Init
*/
var app = angular.module('Jn2MobileApp', ['angular-preload-image','ui-rangeSlider']);

var FORM_KEY=null;
(function($){
  var initAngular = function(){
      angular.bootstrap(document, ['Jn2MobileApp']);
  }

 $(function(){
    // if(window.Jn2 && window.Jn2.CacheBypass !== undefined){
      $(document).on('cache:finish',function(evt,content){
        var data     = content.data;
        FORM_KEY     = data.form_key;
        initAngular();
      });
      $(document).on('cache:error',initAngular);
    // }else{
      // initAngular();
    // }
  });
})(jQuery);
//

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/

app.controller('cartController',function($scope){

  $scope.plusProduct = function(id){
    var value = parseInt(jQuery(id).val());
    if(value < jQuery(id).data("max")){
      jQuery(id).val(value+1);
    }

    if(value < jQuery(id).data("max")){
      jQuery(id).parent().find(".plus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".plus").addClass("disable");
    }

    if(value > 1){
      jQuery(id).parent().find(".minus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".minus").addClass("disable");
    }

    jQuery("#update-cart").submit();
  }

  $scope.blurQtyItem = function(id){
    var value = parseInt(jQuery(id).val());
    if(value > jQuery(id).data("max")) jQuery(id).val(jQuery(id).data("max"));

    if(value < jQuery(id).data("max")){
      jQuery(id).parent().find(".plus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".plus").addClass("disable");
    }

    if(value > 1){
      jQuery(id).parent().find(".minus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".minus").addClass("disable");
    }

    jQuery("#update-cart").submit();
  }

  $scope.minusProduct = function(id){
    var value = parseInt(jQuery(id).val());
    if(value > 1){
      jQuery(id).val(value-1);
    }

    if(value < jQuery(id).data("max")){
      jQuery(id).parent().find(".plus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".plus").addClass("disable");
    }

    if(value > 1){
      jQuery(id).parent().find(".minus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".minus").addClass("disable");
    }

    jQuery("#update-cart").submit();
  }

  jQuery(document).ready(function($){

    $(".frame-card").find(".item").each(function(index,element){
      var $height = $(element).height();
      $(element).find(".qty").height($height);
      $(element).find(".image").height($height);
      $(element).find(".info").height($height);
    });

    $("#update-cart").submit(function(e){
      e.preventDefault();
      jQuery(".loading").show();
      $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success: function (data) {
          location.reload();
        }
      });
      return false;
    });

  });

});

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/


app.controller('categoryController',function($scope, $interval){
  $scope.price = {
    min: null,
    max: null
  }

  str = jQuery("div.filter").html().trim();
  if(str.length === 0){
    jQuery("span.filter").hide();
  }

  $scope.apllyFilter = function(){
    var url = window.location.href.split("?")[0];
    setLocation(url+"?price="+$scope.price.min+"-"+$scope.price.max);
  }

  imagesLoaded( '.products-grid', function(){
    var msnry = new Masonry('.products-grid', {
      itemselector: '.grid-item',
      columnwidth: '.grid-item',
    });
  });

  $scope.filterStatus = 0;

  $scope.filterToggle = function(){
    $scope.filterStatus = !$scope.filterStatus;
  }

});

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/


app.controller('checkoutController',function($scope){

  $scope.openTermos = function(){
    jQuery('#termos-condicoes').modal('show');
  }

});

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/


app.controller('loginController',function($scope){

});

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/

app.controller('mainController',function($scope){
  jQuery(".pitbar-mobile .jicon").text("");
  jQuery(".view").addClass("load")

  jQuery(window).unload(function(){
    jQuery(".view").removeClass("load");
  });

  var pinHeight = jQuery(".pitbar-mobile").find(".col-xs-6").first().height();
  jQuery(".pitbar-mobile").find(".col-xs-6").each(function(index,element){
    if(jQuery(element).height() > pinHeight){
      pinHeight = jQuery(element).height();
    }
  });

  jQuery(".pitbar-mobile").find(".col-xs-6").height(pinHeight);

  jQuery('.btn').addClass("waves-effect");
  jQuery('.item').addClass("waves-effect");
  jQuery('button').addClass("waves-effect");
  jQuery('a[href]').addClass("waves-effect");
  jQuery('.button').addClass("waves-effect");
  jQuery('.frame-card').addClass("waves-effect");

});

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/

app.controller('productController',function($scope){

  $scope.description = 0;
  $scope.options = 1;
  $scope.shipping = 0;
  $scope.comments = 0;

  $scope.buy_action = function(){

  }

  $scope.plusProduct = function(id){
    var value = parseInt(jQuery(id).val());
    if(value < jQuery(id).data("max")){
      jQuery(id).val(value+1);
    }

    if(value < jQuery(id).data("max")){
      jQuery(id).parent().find(".plus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".plus").addClass("disable");
    }

    if(value > 1){
      jQuery(id).parent().find(".minus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".minus").addClass("disable");
    }
  }

  $scope.minusProduct = function(id){
    var value = parseInt(jQuery(id).val());
    if(value > 1){
      jQuery(id).val(value-1);
    }

    if(value < jQuery(id).data("max")){
      jQuery(id).parent().find(".plus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".plus").addClass("disable");
    }

    if(value > 1){
      jQuery(id).parent().find(".minus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".minus").addClass("disable");
    }
  }

  jQuery(function($){
    var $media = $('#media-slider').owlCarousel({
      navigation: false,
      pagination: true,
      singleItem: true,
      items: 1,
      autoPlay: false
    });

    jQuery(".swatchContainer a").addClass("no-loading");

    jQuery(document).on("click", ".swatchContainer a", function(e){
      e.preventDefault();
      var url = $(this).attr("href").split('-zoom.')[0];
      url = url.split("/");
      url = url[url.length-1];
      var image = $(".more-views").find("img[src*='" + url + "']").parent();
      $media.trigger('owl.jumpTo', image.index());
      return false;
    });

    jQuery(".options").on("change",".swatchSelect", function(){
      var link = $(".swatchesContainer").find(".swatchSelected").parent();
      var url = $(link).attr("href").split('-zoom.')[0];
      url = url.split("/");
      url = url[url.length-1];
      var image = $(".more-views").find("img[src*='" + url + "']").parent();
      $media.trigger('owl.jumpTo', image.index());
    });

    function getEmptyOptions(){
      return jQuery('.product-options .required-entry').filter(function(){
        return !this.value;
      });
    }

    jQuery(".add-to-cart .btn-cart, .block-shipping-estimate button").click(function(){
      if(getEmptyOptions().length > 0){
        alert("É necessário escolher uma opção para o produto");
        window.scrollTo(getEmptyOptions().first().offset().top,0);
      }
    });
  });

});

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/


app.controller('registerController',function($scope){
	$scope.pessoa = 0;
	$scope.zipcode = 0;
	$scope.firstname = "";
	$scope.lastname  = "";
	$scope.cnpj  = "";
	$scope.taxvat  = "";

	$scope.updateCNPJ = function(){
		$scope.taxvat  = $scope.cnpj;
	}

	$scope.updateName = function(){
		var name = $scope.firstname.split(" ");
		if(name.length<2){
			if(jQuery("#firstname").parent().find(".form-control-feedback").length == 0 ){
				jQuery("#firstname").parent().addClass("has-error has-feedback");
				jQuery("#firstname").attr("aria-describedby","inputError2Status");
				jQuery("#firstname").after(jQuery('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span><span id="inputError2Status" class="sr-only">(error)</span>'))
			}
		}else{
			jQuery(".form-control-feedback").remove();
			jQuery("#firstname").after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span><span id="inputSuccess2Status" class="sr-only">(success)</span>');
			jQuery("#firstname").parent().removeClass("has-error").addClass(" has-success");
			$scope.lastname = name[name.length-1];
		}
	}

	jQuery("#form-validate").on("blur", "input", function(){
		if(jQuery(this).attr("id")!= "firstname"){
			jQuery(this).parent().find(".form-control-feedback").remove();
			var val = (jQuery(this).attr("id")=="taxvat") ? jQuery(this).val().replace(/\D/g, "") : jQuery(this).val();
			if(val.length == 0){
				jQuery(this).parent().addClass("has-error has-feedback");
				jQuery(this).attr("aria-describedby","inputError2Status");
				jQuery(this).after(jQuery('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'))
			}else{
				jQuery(this).after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
				jQuery(this).parent().removeClass("has-error").addClass(" has-success");
			}
		}
	});

});

'use strict'

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/
app.controller('topBarController',function($scope){

//  jQuery(".loading").hide();

//  jQuery(document).on('click', "a[href!='#']", function(){
//    if(jQuery(this).attr("target")!='_blank' && !jQuery(this).hasClass('no-loading')){
//      jQuery(".loading").show();
//    }
//  });



  jQuery(document).on("click",".logo-store", function(){
    window.location.href = BASE_URL;
  });

  $scope.menuState = 0;
  $scope.searchState = 0;
  $scope.q = "";

  // jQuery(document).on("click","[data-cache-bypass='1'] [ng-click]", function(){
  //   var fn = jQuery(this).attr("ng-click").split('(')[0].replace(/\s+/g,'');
  //   $scope[fn]();
  //   //eval("$scope." + jQuery(this).attr("ng-click"));
  // });

  $scope.menuToggle = function(){
    $scope.menuState = $scope.menuState ? 0 : 1;
  }

  $scope.searchToggle = function(){
    $scope.searchState = !$scope.searchState;
    if($scope.searchState){
      jQuery("header.headerFixed div.search .form-control").trigger("focus");
    }
  };

  $scope.search = function(){
    jQuery(".loading").show();
    // if(FORM_KEY){
      setLocation(BASE_URL+"catalogsearch/result/?q=" + encodeURIComponent($scope.q)+"&form_key="+FORM_KEY);
    // }else{
    //   jQuery(document).on('cache:finish',function(evt,content){
    //     var data = content.data;
    //     setLocation(BASE_URL+"catalogsearch/result/?q=" + encodeURIComponent($scope.q)+"&form_key="+data.form_key);
    //   });
    // }
  }

  $scope.goBag = function(){
    jQuery(".loading").show();
    window.location.href = BASE_URL + "checkout/cart/";
  };

  jQuery(function(){

    jQuery(".menu-offcanvas .categories ul ul").slideUp();

    jQuery(document).on("click",".menu-offcanvas .categories a", function(){
      $scope.menuToggle();
    });

    jQuery(document).on("click", ".menu-offcanvas .categories .expand-menu", function(){
      jQuery(this).parent().toggleClass("open").find("ul").first().slideToggle();
    });

    jQuery(document).on("click",".menu-offcanvas .categories a span", function(){
      window.location.href = jQuery(this).parent().attr("href");
    });

  });


});

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/

app.directive('ngEqualer',function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      setTimeout(function(){
        var attrs = eval('(' + attr.ngEqualer + ')');

        if(attrs.type === "max"){
          var height = 0;
          var elements = element.children();
          elements.each(function(index, elementEqualer){
            if(attrs.element.length > 0){
              var elementary = jQuery(elementEqualer).find(attrs.element);
              if(elementary.height() > height){
                height = elementary.height();
              }
            }else{
              if(jQuery(elementEqualer).height() > height){
                height = jQuery(elementEqualer).height();
              }
            }
          });
          if(attrs.element.length > 0){
            elements.each(function(index, elementEqualer){
              jQuery(elementEqualer).find(attrs.element).css({"height": "calc("+height+"px + 3rem)"});
            });
          }else{
            elements.css({"height": "calc("+height+"px + 2rem)"});
          }
        }else{
          var height = element.children().first().height();
          var elements = element.children();
          elements.each(function(index, elementEqualer){
            if(jQuery(elementEqualer).height() < height){
              height = jQuery(elementEqualer).height();
            }
          });
          elements.height(height);
        }
      },500);
    }
  }
});

/*!
* Waves v0.6.4
* http://fian.my.id/Waves
*
* Copyright 2014 Alfiana E. Sibuea and other contributors
* Released under the MIT license
* https://github.com/fians/Waves/blob/master/LICENSE
*/

;(function(window) {
  'use strict';

  var Waves = Waves || {};
  var $$ = document.querySelectorAll.bind(document);

  // Find exact position of element
  function isWindow(obj) {
    return obj !== null && obj === obj.window;
  }

  function getWindow(elem) {
    return isWindow(elem) ? elem : elem.nodeType === 9 && elem.defaultView;
  }

  function offset(elem) {
    var docElem, win,
    box = {top: 0, left: 0},
    doc = elem && elem.ownerDocument;

    docElem = doc.documentElement;

    if (typeof elem.getBoundingClientRect !== typeof undefined) {
      box = elem.getBoundingClientRect();
    }
    win = getWindow(doc);
    return {
      top: box.top + win.pageYOffset - docElem.clientTop,
      left: box.left + win.pageXOffset - docElem.clientLeft
    };
  }

  function convertStyle(obj) {
    var style = '';

    for (var a in obj) {
      if (obj.hasOwnProperty(a)) {
        style += (a + ':' + obj[a] + ';');
      }
    }

    return style;
  }

  var Effect = {

    // Effect delay
    duration: 750,

    show: function(e, element) {

      // Disable right click
      if (e.button === 2) {
        return false;
      }

      var el = element || this;

      // Create ripple
      var ripple = document.createElement('div');
      ripple.className = 'waves-ripple';
      el.appendChild(ripple);

      // Get click coordinate and element witdh
      var pos         = offset(el);
      var relativeY   = (e.pageY - pos.top);
      var relativeX   = (e.pageX - pos.left);
      var scale       = 'scale('+((el.clientWidth / 100) * 10)+')';

      // Support for touch devices
      if ('touches' in e) {
        relativeY   = (e.touches[0].pageY - pos.top);
        relativeX   = (e.touches[0].pageX - pos.left);
      }

      // Attach data to element
      ripple.setAttribute('data-hold', Date.now());
      ripple.setAttribute('data-scale', scale);
      ripple.setAttribute('data-x', relativeX);
      ripple.setAttribute('data-y', relativeY);

      // Set ripple position
      var rippleStyle = {
        'top': relativeY+'px',
        'left': relativeX+'px'
      };

      ripple.className = ripple.className + ' waves-notransition';
      ripple.setAttribute('style', convertStyle(rippleStyle));
      ripple.className = ripple.className.replace('waves-notransition', '');

      // Scale the ripple
      rippleStyle['-webkit-transform'] = scale;
      rippleStyle['-moz-transform'] = scale;
      rippleStyle['-ms-transform'] = scale;
      rippleStyle['-o-transform'] = scale;
      rippleStyle.transform = scale;
      rippleStyle.opacity   = '1';

      rippleStyle['-webkit-transition-duration'] = Effect.duration + 'ms';
      rippleStyle['-moz-transition-duration']    = Effect.duration + 'ms';
      rippleStyle['-o-transition-duration']      = Effect.duration + 'ms';
      rippleStyle['transition-duration']         = Effect.duration + 'ms';

      rippleStyle['-webkit-transition-timing-function'] = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
      rippleStyle['-moz-transition-timing-function']    = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
      rippleStyle['-o-transition-timing-function']      = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
      rippleStyle['transition-timing-function']         = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';

      ripple.setAttribute('style', convertStyle(rippleStyle));
    },

    hide: function(e) {
      TouchHandler.touchup(e);

      var el = this;
      var width = el.clientWidth * 1.4;

      // Get first ripple
      var ripple = null;
      var ripples = el.getElementsByClassName('waves-ripple');
      if (ripples.length > 0) {
        ripple = ripples[ripples.length - 1];
      } else {
        return false;
      }

      var relativeX   = ripple.getAttribute('data-x');
      var relativeY   = ripple.getAttribute('data-y');
      var scale       = ripple.getAttribute('data-scale');

      // Get delay beetween mousedown and mouse leave
      var diff = Date.now() - Number(ripple.getAttribute('data-hold'));
      var delay = 350 - diff;

      if (delay < 0) {
        delay = 0;
      }

      // Fade out ripple after delay
      setTimeout(function() {
        var style = {
          'top': relativeY+'px',
          'left': relativeX+'px',
          'opacity': '0',

          // Duration
          '-webkit-transition-duration': Effect.duration + 'ms',
          '-moz-transition-duration': Effect.duration + 'ms',
          '-o-transition-duration': Effect.duration + 'ms',
          'transition-duration': Effect.duration + 'ms',
          '-webkit-transform': scale,
          '-moz-transform': scale,
          '-ms-transform': scale,
          '-o-transform': scale,
          'transform': scale,
        };

        ripple.setAttribute('style', convertStyle(style));

        setTimeout(function() {
          try {
            el.removeChild(ripple);
          } catch(e) {
            return false;
          }
        }, Effect.duration);
      }, delay);
    },

    // Little hack to make <input> can perform waves effect
    wrapInput: function(elements) {
      for (var a = 0; a < elements.length; a++) {
        var el = elements[a];

        if (el.tagName.toLowerCase() === 'input') {
          var parent = el.parentNode;

          // If input already have parent just pass through
          if (parent.tagName.toLowerCase() === 'i' && parent.className.indexOf('waves-effect') !== -1) {
            continue;
          }

          // Put element class and style to the specified parent
          var wrapper = document.createElement('i');
          wrapper.className = el.className + ' waves-input-wrapper';

          var elementStyle = el.getAttribute('style');

          if (!elementStyle) {
            elementStyle = '';
          }

          wrapper.setAttribute('style', elementStyle);

          el.className = 'waves-button-input';
          el.removeAttribute('style');

          // Put element as child
          parent.replaceChild(wrapper, el);
          wrapper.appendChild(el);
        }
      }
    }
  };


  /**
  * Disable mousedown event for 500ms during and after touch
  */
  var TouchHandler = {
    /* uses an integer rather than bool so there's no issues with
    * needing to clear timeouts if another touch event occurred
    * within the 500ms. Cannot mouseup between touchstart and
    * touchend, nor in the 500ms after touchend. */
    touches: 0,
    allowEvent: function(e) {
      var allow = true;

      if (e.type === 'touchstart') {
        TouchHandler.touches += 1; //push
      } else if (e.type === 'touchend' || e.type === 'touchcancel') {
        setTimeout(function() {
          if (TouchHandler.touches > 0) {
            TouchHandler.touches -= 1; //pop after 500ms
          }
        }, 500);
      } else if (e.type === 'mousedown' && TouchHandler.touches > 0) {
        allow = false;
      }

      return allow;
    },
    touchup: function(e) {
      TouchHandler.allowEvent(e);
    }
  };


  /**
  * Delegated click handler for .waves-effect element.
  * returns null when .waves-effect element not in "click tree"
  */
  function getWavesEffectElement(e) {
    if (TouchHandler.allowEvent(e) === false) {
      return null;
    }

    var element = null;
    var target = e.target || e.srcElement;

    while (target.parentElement !== null) {
      if (!(target instanceof SVGElement) && target.className.indexOf('waves-effect') !== -1) {
        element = target;
        break;
      } else if (target.classList.contains('waves-effect')) {
        element = target;
        break;
      }
      target = target.parentElement;
    }

    return element;
  }

  /**
  * Bubble the click and show effect if .waves-effect elem was found
  */
  function showEffect(e) {
    var element = getWavesEffectElement(e);

    if (element !== null) {
      Effect.show(e, element);

      if ('ontouchstart' in window) {
        element.addEventListener('touchend', Effect.hide, false);
        element.addEventListener('touchcancel', Effect.hide, false);
      }

      element.addEventListener('mouseup', Effect.hide, false);
      element.addEventListener('mouseleave', Effect.hide, false);
    }
  }

  Waves.displayEffect = function(options) {
    options = options || {};

    if ('duration' in options) {
      Effect.duration = options.duration;
    }

    //Wrap input inside <i> tag
    Effect.wrapInput($$('.waves-effect'));

    if ('ontouchstart' in window) {
      document.body.addEventListener('touchstart', showEffect, false);
    }

    document.body.addEventListener('mousedown', showEffect, false);
  };

  /**
  * Attach Waves to an input element (or any element which doesn't
  * bubble mouseup/mousedown events).
  *   Intended to be used with dynamically loaded forms/inputs, or
  * where the user doesn't want a delegated click handler.
  */
  Waves.attach = function(element) {
    //FUTURE: automatically add waves classes and allow users
    // to specify them with an options param? Eg. light/classic/button
    if (element.tagName.toLowerCase() === 'input') {
      Effect.wrapInput([element]);
      element = element.parentElement;
    }

    if ('ontouchstart' in window) {
      element.addEventListener('touchstart', showEffect, false);
    }

    element.addEventListener('mousedown', showEffect, false);
  };

  window.Waves = Waves;

  document.addEventListener('DOMContentLoaded', function() {
    Waves.displayEffect();
  }, false);

})(window);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm93bC5jYXJvdXNlbC5qcyIsImFuZ3VsYXIubWluLmpzIiwiYm9vdHN0cmFwLmpzIiwiYW5ndWxhci5yYW5nZVNsaWRlci5qcyIsIm1hc29ucnkuanMiLCJhbmd1bGFyLXByZWxvYWQtaW1hZ2UubWluLmpzIiwiYXBwbGljYXRpb24uanMiLCJjb250cm9sbGVycy9jYXJ0Q29udHJvbGxlci5qcyIsImNvbnRyb2xsZXJzL2NhdGVnb3J5Q29udHJvbGxlci5qcyIsImNvbnRyb2xsZXJzL2NoZWNrb3V0Q29udHJvbGxlci5qcyIsImNvbnRyb2xsZXJzL2xvZ2luQ29udHJvbGxlci5qcyIsImNvbnRyb2xsZXJzL21haW5Db250cm9sbGVyLmpzIiwiY29udHJvbGxlcnMvcHJvZHVjdENvbnRyb2xsZXIuanMiLCJjb250cm9sbGVycy9yZWdpc3RlckNvbnRyb2xsZXIuanMiLCJjb250cm9sbGVycy90b3BCYXJDb250cm9sbGVyLmpzIiwiZGlyZWN0aXZlcy9uZ0VxdWFsZXIuanMiLCJkaXJlY3RpdmVzL3dhdmVzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3YrQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzdQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzekVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNyc0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzWEE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaEdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3RDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2hDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNyR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3BEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2xEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiAgalF1ZXJ5IE93bENhcm91c2VsIHYxLjMuMlxuICpcbiAqICBDb3B5cmlnaHQgKGMpIDIwMTMgQmFydG9zeiBXb2pjaWVjaG93c2tpXG4gKiAgaHR0cDovL3d3dy5vd2xncmFwaGljLmNvbS9vd2xjYXJvdXNlbC9cbiAqXG4gKiAgTGljZW5zZWQgdW5kZXIgTUlUXG4gKlxuICovXG5cbi8qSlMgTGludCBoZWxwZXJzOiAqL1xuLypnbG9iYWwgZHJhZ01vdmU6IGZhbHNlLCBkcmFnRW5kOiBmYWxzZSwgJCwgalF1ZXJ5LCBhbGVydCwgd2luZG93LCBkb2N1bWVudCAqL1xuLypqc2xpbnQgbm9tZW46IHRydWUsIGNvbnRpbnVlOnRydWUgKi9cblxuaWYgKHR5cGVvZiBPYmplY3QuY3JlYXRlICE9PSBcImZ1bmN0aW9uXCIpIHtcbiAgICBPYmplY3QuY3JlYXRlID0gZnVuY3Rpb24gKG9iaikge1xuICAgICAgICBmdW5jdGlvbiBGKCkge31cbiAgICAgICAgRi5wcm90b3R5cGUgPSBvYmo7XG4gICAgICAgIHJldHVybiBuZXcgRigpO1xuICAgIH07XG59XG4oZnVuY3Rpb24gKCQsIHdpbmRvdywgZG9jdW1lbnQpIHtcblxuICAgIHZhciBDYXJvdXNlbCA9IHtcbiAgICAgICAgaW5pdCA6IGZ1bmN0aW9uIChvcHRpb25zLCBlbCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuXG4gICAgICAgICAgICBiYXNlLiRlbGVtID0gJChlbCk7XG4gICAgICAgICAgICBiYXNlLm9wdGlvbnMgPSAkLmV4dGVuZCh7fSwgJC5mbi5vd2xDYXJvdXNlbC5vcHRpb25zLCBiYXNlLiRlbGVtLmRhdGEoKSwgb3B0aW9ucyk7XG5cbiAgICAgICAgICAgIGJhc2UudXNlck9wdGlvbnMgPSBvcHRpb25zO1xuICAgICAgICAgICAgYmFzZS5sb2FkQ29udGVudCgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGxvYWRDb250ZW50IDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzLCB1cmw7XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGdldERhdGEoZGF0YSkge1xuICAgICAgICAgICAgICAgIHZhciBpLCBjb250ZW50ID0gXCJcIjtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGJhc2Uub3B0aW9ucy5qc29uU3VjY2VzcyA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5qc29uU3VjY2Vzcy5hcHBseSh0aGlzLCBbZGF0YV0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGZvciAoaSBpbiBkYXRhLm93bCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEub3dsLmhhc093blByb3BlcnR5KGkpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudCArPSBkYXRhLm93bFtpXS5pdGVtO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGJhc2UuJGVsZW0uaHRtbChjb250ZW50KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYmFzZS5sb2dJbigpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodHlwZW9mIGJhc2Uub3B0aW9ucy5iZWZvcmVJbml0ID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm9wdGlvbnMuYmVmb3JlSW5pdC5hcHBseSh0aGlzLCBbYmFzZS4kZWxlbV0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodHlwZW9mIGJhc2Uub3B0aW9ucy5qc29uUGF0aCA9PT0gXCJzdHJpbmdcIikge1xuICAgICAgICAgICAgICAgIHVybCA9IGJhc2Uub3B0aW9ucy5qc29uUGF0aDtcbiAgICAgICAgICAgICAgICAkLmdldEpTT04odXJsLCBnZXREYXRhKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgYmFzZS5sb2dJbigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGxvZ0luIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuXG4gICAgICAgICAgICBiYXNlLiRlbGVtLmRhdGEoXCJvd2wtb3JpZ2luYWxTdHlsZXNcIiwgYmFzZS4kZWxlbS5hdHRyKFwic3R5bGVcIikpXG4gICAgICAgICAgICAgICAgICAgICAgLmRhdGEoXCJvd2wtb3JpZ2luYWxDbGFzc2VzXCIsIGJhc2UuJGVsZW0uYXR0cihcImNsYXNzXCIpKTtcblxuICAgICAgICAgICAgYmFzZS4kZWxlbS5jc3Moe29wYWNpdHk6IDB9KTtcbiAgICAgICAgICAgIGJhc2Uub3JpZ25hbEl0ZW1zID0gYmFzZS5vcHRpb25zLml0ZW1zO1xuICAgICAgICAgICAgYmFzZS5jaGVja0Jyb3dzZXIoKTtcbiAgICAgICAgICAgIGJhc2Uud3JhcHBlcldpZHRoID0gMDtcbiAgICAgICAgICAgIGJhc2UuY2hlY2tWaXNpYmxlID0gbnVsbDtcbiAgICAgICAgICAgIGJhc2Uuc2V0VmFycygpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNldFZhcnMgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG4gICAgICAgICAgICBpZiAoYmFzZS4kZWxlbS5jaGlsZHJlbigpLmxlbmd0aCA9PT0gMCkge3JldHVybiBmYWxzZTsgfVxuICAgICAgICAgICAgYmFzZS5iYXNlQ2xhc3MoKTtcbiAgICAgICAgICAgIGJhc2UuZXZlbnRUeXBlcygpO1xuICAgICAgICAgICAgYmFzZS4kdXNlckl0ZW1zID0gYmFzZS4kZWxlbS5jaGlsZHJlbigpO1xuICAgICAgICAgICAgYmFzZS5pdGVtc0Ftb3VudCA9IGJhc2UuJHVzZXJJdGVtcy5sZW5ndGg7XG4gICAgICAgICAgICBiYXNlLndyYXBJdGVtcygpO1xuICAgICAgICAgICAgYmFzZS4kb3dsSXRlbXMgPSBiYXNlLiRlbGVtLmZpbmQoXCIub3dsLWl0ZW1cIik7XG4gICAgICAgICAgICBiYXNlLiRvd2xXcmFwcGVyID0gYmFzZS4kZWxlbS5maW5kKFwiLm93bC13cmFwcGVyXCIpO1xuICAgICAgICAgICAgYmFzZS5wbGF5RGlyZWN0aW9uID0gXCJuZXh0XCI7XG4gICAgICAgICAgICBiYXNlLnByZXZJdGVtID0gMDtcbiAgICAgICAgICAgIGJhc2UucHJldkFyciA9IFswXTtcbiAgICAgICAgICAgIGJhc2UuY3VycmVudEl0ZW0gPSAwO1xuICAgICAgICAgICAgYmFzZS5jdXN0b21FdmVudHMoKTtcbiAgICAgICAgICAgIGJhc2Uub25TdGFydHVwKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgb25TdGFydHVwIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgYmFzZS51cGRhdGVJdGVtcygpO1xuICAgICAgICAgICAgYmFzZS5jYWxjdWxhdGVBbGwoKTtcbiAgICAgICAgICAgIGJhc2UuYnVpbGRDb250cm9scygpO1xuICAgICAgICAgICAgYmFzZS51cGRhdGVDb250cm9scygpO1xuICAgICAgICAgICAgYmFzZS5yZXNwb25zZSgpO1xuICAgICAgICAgICAgYmFzZS5tb3ZlRXZlbnRzKCk7XG4gICAgICAgICAgICBiYXNlLnN0b3BPbkhvdmVyKCk7XG4gICAgICAgICAgICBiYXNlLm93bFN0YXR1cygpO1xuXG4gICAgICAgICAgICBpZiAoYmFzZS5vcHRpb25zLnRyYW5zaXRpb25TdHlsZSAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICBiYXNlLnRyYW5zaXRpb25UeXBlcyhiYXNlLm9wdGlvbnMudHJhbnNpdGlvblN0eWxlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMuYXV0b1BsYXkgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm9wdGlvbnMuYXV0b1BsYXkgPSA1MDAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYmFzZS5wbGF5KCk7XG5cbiAgICAgICAgICAgIGJhc2UuJGVsZW0uZmluZChcIi5vd2wtd3JhcHBlclwiKS5jc3MoXCJkaXNwbGF5XCIsIFwiYmxvY2tcIik7XG5cbiAgICAgICAgICAgIGlmICghYmFzZS4kZWxlbS5pcyhcIjp2aXNpYmxlXCIpKSB7XG4gICAgICAgICAgICAgICAgYmFzZS53YXRjaFZpc2liaWxpdHkoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgYmFzZS4kZWxlbS5jc3MoXCJvcGFjaXR5XCIsIDEpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYmFzZS5vbnN0YXJ0dXAgPSBmYWxzZTtcbiAgICAgICAgICAgIGJhc2UuZWFjaE1vdmVVcGRhdGUoKTtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgYmFzZS5vcHRpb25zLmFmdGVySW5pdCA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5vcHRpb25zLmFmdGVySW5pdC5hcHBseSh0aGlzLCBbYmFzZS4kZWxlbV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGVhY2hNb3ZlVXBkYXRlIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuXG4gICAgICAgICAgICBpZiAoYmFzZS5vcHRpb25zLmxhenlMb2FkID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5sYXp5TG9hZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5hdXRvSGVpZ2h0ID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5hdXRvSGVpZ2h0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBiYXNlLm9uVmlzaWJsZUl0ZW1zKCk7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgYmFzZS5vcHRpb25zLmFmdGVyQWN0aW9uID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm9wdGlvbnMuYWZ0ZXJBY3Rpb24uYXBwbHkodGhpcywgW2Jhc2UuJGVsZW1dKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB1cGRhdGVWYXJzIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBiYXNlLm9wdGlvbnMuYmVmb3JlVXBkYXRlID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm9wdGlvbnMuYmVmb3JlVXBkYXRlLmFwcGx5KHRoaXMsIFtiYXNlLiRlbGVtXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBiYXNlLndhdGNoVmlzaWJpbGl0eSgpO1xuICAgICAgICAgICAgYmFzZS51cGRhdGVJdGVtcygpO1xuICAgICAgICAgICAgYmFzZS5jYWxjdWxhdGVBbGwoKTtcbiAgICAgICAgICAgIGJhc2UudXBkYXRlUG9zaXRpb24oKTtcbiAgICAgICAgICAgIGJhc2UudXBkYXRlQ29udHJvbHMoKTtcbiAgICAgICAgICAgIGJhc2UuZWFjaE1vdmVVcGRhdGUoKTtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgYmFzZS5vcHRpb25zLmFmdGVyVXBkYXRlID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm9wdGlvbnMuYWZ0ZXJVcGRhdGUuYXBwbHkodGhpcywgW2Jhc2UuJGVsZW1dKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZWxvYWQgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG4gICAgICAgICAgICB3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgYmFzZS51cGRhdGVWYXJzKCk7XG4gICAgICAgICAgICB9LCAwKTtcbiAgICAgICAgfSxcblxuICAgICAgICB3YXRjaFZpc2liaWxpdHkgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG5cbiAgICAgICAgICAgIGlmIChiYXNlLiRlbGVtLmlzKFwiOnZpc2libGVcIikgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgYmFzZS4kZWxlbS5jc3Moe29wYWNpdHk6IDB9KTtcbiAgICAgICAgICAgICAgICB3aW5kb3cuY2xlYXJJbnRlcnZhbChiYXNlLmF1dG9QbGF5SW50ZXJ2YWwpO1xuICAgICAgICAgICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKGJhc2UuY2hlY2tWaXNpYmxlKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYmFzZS5jaGVja1Zpc2libGUgPSB3aW5kb3cuc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmIChiYXNlLiRlbGVtLmlzKFwiOnZpc2libGVcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5yZWxvYWQoKTtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS4kZWxlbS5hbmltYXRlKHtvcGFjaXR5OiAxfSwgMjAwKTtcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LmNsZWFySW50ZXJ2YWwoYmFzZS5jaGVja1Zpc2libGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIDUwMCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgd3JhcEl0ZW1zIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgYmFzZS4kdXNlckl0ZW1zLndyYXBBbGwoXCI8ZGl2IGNsYXNzPVxcXCJvd2wtd3JhcHBlclxcXCI+XCIpLndyYXAoXCI8ZGl2IGNsYXNzPVxcXCJvd2wtaXRlbVxcXCI+PC9kaXY+XCIpO1xuICAgICAgICAgICAgYmFzZS4kZWxlbS5maW5kKFwiLm93bC13cmFwcGVyXCIpLndyYXAoXCI8ZGl2IGNsYXNzPVxcXCJvd2wtd3JhcHBlci1vdXRlclxcXCI+XCIpO1xuICAgICAgICAgICAgYmFzZS53cmFwcGVyT3V0ZXIgPSBiYXNlLiRlbGVtLmZpbmQoXCIub3dsLXdyYXBwZXItb3V0ZXJcIik7XG4gICAgICAgICAgICBiYXNlLiRlbGVtLmNzcyhcImRpc3BsYXlcIiwgXCJibG9ja1wiKTtcbiAgICAgICAgfSxcblxuICAgICAgICBiYXNlQ2xhc3MgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXMsXG4gICAgICAgICAgICAgICAgaGFzQmFzZUNsYXNzID0gYmFzZS4kZWxlbS5oYXNDbGFzcyhiYXNlLm9wdGlvbnMuYmFzZUNsYXNzKSxcbiAgICAgICAgICAgICAgICBoYXNUaGVtZUNsYXNzID0gYmFzZS4kZWxlbS5oYXNDbGFzcyhiYXNlLm9wdGlvbnMudGhlbWUpO1xuXG4gICAgICAgICAgICBpZiAoIWhhc0Jhc2VDbGFzcykge1xuICAgICAgICAgICAgICAgIGJhc2UuJGVsZW0uYWRkQ2xhc3MoYmFzZS5vcHRpb25zLmJhc2VDbGFzcyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICghaGFzVGhlbWVDbGFzcykge1xuICAgICAgICAgICAgICAgIGJhc2UuJGVsZW0uYWRkQ2xhc3MoYmFzZS5vcHRpb25zLnRoZW1lKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB1cGRhdGVJdGVtcyA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcywgd2lkdGgsIGk7XG5cbiAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMucmVzcG9uc2l2ZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoYmFzZS5vcHRpb25zLnNpbmdsZUl0ZW0gPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm9wdGlvbnMuaXRlbXMgPSBiYXNlLm9yaWduYWxJdGVtcyA9IDE7XG4gICAgICAgICAgICAgICAgYmFzZS5vcHRpb25zLml0ZW1zQ3VzdG9tID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgYmFzZS5vcHRpb25zLml0ZW1zRGVza3RvcCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5pdGVtc0Rlc2t0b3BTbWFsbCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5pdGVtc1RhYmxldCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5pdGVtc1RhYmxldFNtYWxsID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgYmFzZS5vcHRpb25zLml0ZW1zTW9iaWxlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB3aWR0aCA9ICQoYmFzZS5vcHRpb25zLnJlc3BvbnNpdmVCYXNlV2lkdGgpLndpZHRoKCk7XG5cbiAgICAgICAgICAgIGlmICh3aWR0aCA+IChiYXNlLm9wdGlvbnMuaXRlbXNEZXNrdG9wWzBdIHx8IGJhc2Uub3JpZ25hbEl0ZW1zKSkge1xuICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5pdGVtcyA9IGJhc2Uub3JpZ25hbEl0ZW1zO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5pdGVtc0N1c3RvbSAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAvL1Jlb3JkZXIgYXJyYXkgYnkgc2NyZWVuIHNpemVcbiAgICAgICAgICAgICAgICBiYXNlLm9wdGlvbnMuaXRlbXNDdXN0b20uc29ydChmdW5jdGlvbiAoYSwgYikge3JldHVybiBhWzBdIC0gYlswXTsgfSk7XG5cbiAgICAgICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgYmFzZS5vcHRpb25zLml0ZW1zQ3VzdG9tLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMuaXRlbXNDdXN0b21baV1bMF0gPD0gd2lkdGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5pdGVtcyA9IGJhc2Uub3B0aW9ucy5pdGVtc0N1c3RvbVtpXVsxXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIGlmICh3aWR0aCA8PSBiYXNlLm9wdGlvbnMuaXRlbXNEZXNrdG9wWzBdICYmIGJhc2Uub3B0aW9ucy5pdGVtc0Rlc2t0b3AgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5pdGVtcyA9IGJhc2Uub3B0aW9ucy5pdGVtc0Rlc2t0b3BbMV07XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHdpZHRoIDw9IGJhc2Uub3B0aW9ucy5pdGVtc0Rlc2t0b3BTbWFsbFswXSAmJiBiYXNlLm9wdGlvbnMuaXRlbXNEZXNrdG9wU21hbGwgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5pdGVtcyA9IGJhc2Uub3B0aW9ucy5pdGVtc0Rlc2t0b3BTbWFsbFsxXTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAod2lkdGggPD0gYmFzZS5vcHRpb25zLml0ZW1zVGFibGV0WzBdICYmIGJhc2Uub3B0aW9ucy5pdGVtc1RhYmxldCAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5vcHRpb25zLml0ZW1zID0gYmFzZS5vcHRpb25zLml0ZW1zVGFibGV0WzFdO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICh3aWR0aCA8PSBiYXNlLm9wdGlvbnMuaXRlbXNUYWJsZXRTbWFsbFswXSAmJiBiYXNlLm9wdGlvbnMuaXRlbXNUYWJsZXRTbWFsbCAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5vcHRpb25zLml0ZW1zID0gYmFzZS5vcHRpb25zLml0ZW1zVGFibGV0U21hbGxbMV07XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHdpZHRoIDw9IGJhc2Uub3B0aW9ucy5pdGVtc01vYmlsZVswXSAmJiBiYXNlLm9wdGlvbnMuaXRlbXNNb2JpbGUgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5pdGVtcyA9IGJhc2Uub3B0aW9ucy5pdGVtc01vYmlsZVsxXTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vaWYgbnVtYmVyIG9mIGl0ZW1zIGlzIGxlc3MgdGhhbiBkZWNsYXJlZFxuICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5pdGVtcyA+IGJhc2UuaXRlbXNBbW91bnQgJiYgYmFzZS5vcHRpb25zLml0ZW1zU2NhbGVVcCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5pdGVtcyA9IGJhc2UuaXRlbXNBbW91bnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVzcG9uc2UgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXMsXG4gICAgICAgICAgICAgICAgc21hbGxEZWxheSxcbiAgICAgICAgICAgICAgICBsYXN0V2luZG93V2lkdGg7XG5cbiAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMucmVzcG9uc2l2ZSAhPT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxhc3RXaW5kb3dXaWR0aCA9ICQod2luZG93KS53aWR0aCgpO1xuXG4gICAgICAgICAgICBiYXNlLnJlc2l6ZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaWYgKCQod2luZG93KS53aWR0aCgpICE9PSBsYXN0V2luZG93V2lkdGgpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5hdXRvUGxheSAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKGJhc2UuYXV0b1BsYXlJbnRlcnZhbCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgd2luZG93LmNsZWFyVGltZW91dChzbWFsbERlbGF5KTtcbiAgICAgICAgICAgICAgICAgICAgc21hbGxEZWxheSA9IHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhc3RXaW5kb3dXaWR0aCA9ICQod2luZG93KS53aWR0aCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgYmFzZS51cGRhdGVWYXJzKCk7XG4gICAgICAgICAgICAgICAgICAgIH0sIGJhc2Uub3B0aW9ucy5yZXNwb25zaXZlUmVmcmVzaFJhdGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICAkKHdpbmRvdykucmVzaXplKGJhc2UucmVzaXplcik7XG4gICAgICAgIH0sXG5cbiAgICAgICAgdXBkYXRlUG9zaXRpb24gOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG4gICAgICAgICAgICBiYXNlLmp1bXBUbyhiYXNlLmN1cnJlbnRJdGVtKTtcbiAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMuYXV0b1BsYXkgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5jaGVja0FwKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgYXBwZW5kSXRlbXNTaXplcyA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcyxcbiAgICAgICAgICAgICAgICByb3VuZFBhZ2VzID0gMCxcbiAgICAgICAgICAgICAgICBsYXN0SXRlbSA9IGJhc2UuaXRlbXNBbW91bnQgLSBiYXNlLm9wdGlvbnMuaXRlbXM7XG5cbiAgICAgICAgICAgIGJhc2UuJG93bEl0ZW1zLmVhY2goZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcbiAgICAgICAgICAgICAgICAkdGhpc1xuICAgICAgICAgICAgICAgICAgICAuY3NzKHtcIndpZHRoXCI6IGJhc2UuaXRlbVdpZHRofSlcbiAgICAgICAgICAgICAgICAgICAgLmRhdGEoXCJvd2wtaXRlbVwiLCBOdW1iZXIoaW5kZXgpKTtcblxuICAgICAgICAgICAgICAgIGlmIChpbmRleCAlIGJhc2Uub3B0aW9ucy5pdGVtcyA9PT0gMCB8fCBpbmRleCA9PT0gbGFzdEl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEoaW5kZXggPiBsYXN0SXRlbSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdW5kUGFnZXMgKz0gMTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAkdGhpcy5kYXRhKFwib3dsLXJvdW5kUGFnZXNcIiwgcm91bmRQYWdlcyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBhcHBlbmRXcmFwcGVyU2l6ZXMgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXMsXG4gICAgICAgICAgICAgICAgd2lkdGggPSBiYXNlLiRvd2xJdGVtcy5sZW5ndGggKiBiYXNlLml0ZW1XaWR0aDtcblxuICAgICAgICAgICAgYmFzZS4kb3dsV3JhcHBlci5jc3Moe1xuICAgICAgICAgICAgICAgIFwid2lkdGhcIjogd2lkdGggKiAyLFxuICAgICAgICAgICAgICAgIFwibGVmdFwiOiAwXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGJhc2UuYXBwZW5kSXRlbXNTaXplcygpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNhbGN1bGF0ZUFsbCA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcbiAgICAgICAgICAgIGJhc2UuY2FsY3VsYXRlV2lkdGgoKTtcbiAgICAgICAgICAgIGJhc2UuYXBwZW5kV3JhcHBlclNpemVzKCk7XG4gICAgICAgICAgICBiYXNlLmxvb3BzKCk7XG4gICAgICAgICAgICBiYXNlLm1heCgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNhbGN1bGF0ZVdpZHRoIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgYmFzZS5pdGVtV2lkdGggPSBNYXRoLnJvdW5kKGJhc2UuJGVsZW0ud2lkdGgoKSAvIGJhc2Uub3B0aW9ucy5pdGVtcyk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgbWF4IDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzLFxuICAgICAgICAgICAgICAgIG1heGltdW0gPSAoKGJhc2UuaXRlbXNBbW91bnQgKiBiYXNlLml0ZW1XaWR0aCkgLSBiYXNlLm9wdGlvbnMuaXRlbXMgKiBiYXNlLml0ZW1XaWR0aCkgKiAtMTtcbiAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMuaXRlbXMgPiBiYXNlLml0ZW1zQW1vdW50KSB7XG4gICAgICAgICAgICAgICAgYmFzZS5tYXhpbXVtSXRlbSA9IDA7XG4gICAgICAgICAgICAgICAgbWF4aW11bSA9IDA7XG4gICAgICAgICAgICAgICAgYmFzZS5tYXhpbXVtUGl4ZWxzID0gMDtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgYmFzZS5tYXhpbXVtSXRlbSA9IGJhc2UuaXRlbXNBbW91bnQgLSBiYXNlLm9wdGlvbnMuaXRlbXM7XG4gICAgICAgICAgICAgICAgYmFzZS5tYXhpbXVtUGl4ZWxzID0gbWF4aW11bTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBtYXhpbXVtO1xuICAgICAgICB9LFxuXG4gICAgICAgIG1pbiA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiAwO1xuICAgICAgICB9LFxuXG4gICAgICAgIGxvb3BzIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzLFxuICAgICAgICAgICAgICAgIHByZXYgPSAwLFxuICAgICAgICAgICAgICAgIGVsV2lkdGggPSAwLFxuICAgICAgICAgICAgICAgIGksXG4gICAgICAgICAgICAgICAgaXRlbSxcbiAgICAgICAgICAgICAgICByb3VuZFBhZ2VOdW07XG5cbiAgICAgICAgICAgIGJhc2UucG9zaXRpb25zSW5BcnJheSA9IFswXTtcbiAgICAgICAgICAgIGJhc2UucGFnZXNJbkFycmF5ID0gW107XG5cbiAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBiYXNlLml0ZW1zQW1vdW50OyBpICs9IDEpIHtcbiAgICAgICAgICAgICAgICBlbFdpZHRoICs9IGJhc2UuaXRlbVdpZHRoO1xuICAgICAgICAgICAgICAgIGJhc2UucG9zaXRpb25zSW5BcnJheS5wdXNoKC1lbFdpZHRoKTtcblxuICAgICAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMuc2Nyb2xsUGVyUGFnZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBpdGVtID0gJChiYXNlLiRvd2xJdGVtc1tpXSk7XG4gICAgICAgICAgICAgICAgICAgIHJvdW5kUGFnZU51bSA9IGl0ZW0uZGF0YShcIm93bC1yb3VuZFBhZ2VzXCIpO1xuICAgICAgICAgICAgICAgICAgICBpZiAocm91bmRQYWdlTnVtICE9PSBwcmV2KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBiYXNlLnBhZ2VzSW5BcnJheVtwcmV2XSA9IGJhc2UucG9zaXRpb25zSW5BcnJheVtpXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHByZXYgPSByb3VuZFBhZ2VOdW07XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgYnVpbGRDb250cm9scyA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcbiAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMubmF2aWdhdGlvbiA9PT0gdHJ1ZSB8fCBiYXNlLm9wdGlvbnMucGFnaW5hdGlvbiA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIGJhc2Uub3dsQ29udHJvbHMgPSAkKFwiPGRpdiBjbGFzcz1cXFwib3dsLWNvbnRyb2xzXFxcIi8+XCIpLnRvZ2dsZUNsYXNzKFwiY2xpY2thYmxlXCIsICFiYXNlLmJyb3dzZXIuaXNUb3VjaCkuYXBwZW5kVG8oYmFzZS4kZWxlbSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoYmFzZS5vcHRpb25zLnBhZ2luYXRpb24gPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICBiYXNlLmJ1aWxkUGFnaW5hdGlvbigpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5uYXZpZ2F0aW9uID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5idWlsZEJ1dHRvbnMoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBidWlsZEJ1dHRvbnMgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXMsXG4gICAgICAgICAgICAgICAgYnV0dG9uc1dyYXBwZXIgPSAkKFwiPGRpdiBjbGFzcz1cXFwib3dsLWJ1dHRvbnNcXFwiLz5cIik7XG4gICAgICAgICAgICBiYXNlLm93bENvbnRyb2xzLmFwcGVuZChidXR0b25zV3JhcHBlcik7XG5cbiAgICAgICAgICAgIGJhc2UuYnV0dG9uUHJldiA9ICQoXCI8ZGl2Lz5cIiwge1xuICAgICAgICAgICAgICAgIFwiY2xhc3NcIiA6IFwib3dsLXByZXZcIixcbiAgICAgICAgICAgICAgICBcImh0bWxcIiA6IGJhc2Uub3B0aW9ucy5uYXZpZ2F0aW9uVGV4dFswXSB8fCBcIlwiXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgYmFzZS5idXR0b25OZXh0ID0gJChcIjxkaXYvPlwiLCB7XG4gICAgICAgICAgICAgICAgXCJjbGFzc1wiIDogXCJvd2wtbmV4dFwiLFxuICAgICAgICAgICAgICAgIFwiaHRtbFwiIDogYmFzZS5vcHRpb25zLm5hdmlnYXRpb25UZXh0WzFdIHx8IFwiXCJcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBidXR0b25zV3JhcHBlclxuICAgICAgICAgICAgICAgIC5hcHBlbmQoYmFzZS5idXR0b25QcmV2KVxuICAgICAgICAgICAgICAgIC5hcHBlbmQoYmFzZS5idXR0b25OZXh0KTtcblxuICAgICAgICAgICAgYnV0dG9uc1dyYXBwZXIub24oXCJ0b3VjaHN0YXJ0Lm93bENvbnRyb2xzIG1vdXNlZG93bi5vd2xDb250cm9sc1wiLCBcImRpdltjbGFzc149XFxcIm93bFxcXCJdXCIsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgYnV0dG9uc1dyYXBwZXIub24oXCJ0b3VjaGVuZC5vd2xDb250cm9scyBtb3VzZXVwLm93bENvbnRyb2xzXCIsIFwiZGl2W2NsYXNzXj1cXFwib3dsXFxcIl1cIiwgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS5oYXNDbGFzcyhcIm93bC1uZXh0XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UubmV4dCgpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UucHJldigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGJ1aWxkUGFnaW5hdGlvbiA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcblxuICAgICAgICAgICAgYmFzZS5wYWdpbmF0aW9uV3JhcHBlciA9ICQoXCI8ZGl2IGNsYXNzPVxcXCJvd2wtcGFnaW5hdGlvblxcXCIvPlwiKTtcbiAgICAgICAgICAgIGJhc2Uub3dsQ29udHJvbHMuYXBwZW5kKGJhc2UucGFnaW5hdGlvbldyYXBwZXIpO1xuXG4gICAgICAgICAgICBiYXNlLnBhZ2luYXRpb25XcmFwcGVyLm9uKFwidG91Y2hlbmQub3dsQ29udHJvbHMgbW91c2V1cC5vd2xDb250cm9sc1wiLCBcIi5vd2wtcGFnZVwiLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIGlmIChOdW1iZXIoJCh0aGlzKS5kYXRhKFwib3dsLXBhZ2VcIikpICE9PSBiYXNlLmN1cnJlbnRJdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UuZ29UbyhOdW1iZXIoJCh0aGlzKS5kYXRhKFwib3dsLXBhZ2VcIikpLCB0cnVlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICB1cGRhdGVQYWdpbmF0aW9uIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzLFxuICAgICAgICAgICAgICAgIGNvdW50ZXIsXG4gICAgICAgICAgICAgICAgbGFzdFBhZ2UsXG4gICAgICAgICAgICAgICAgbGFzdEl0ZW0sXG4gICAgICAgICAgICAgICAgaSxcbiAgICAgICAgICAgICAgICBwYWdpbmF0aW9uQnV0dG9uLFxuICAgICAgICAgICAgICAgIHBhZ2luYXRpb25CdXR0b25Jbm5lcjtcblxuICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5wYWdpbmF0aW9uID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgYmFzZS5wYWdpbmF0aW9uV3JhcHBlci5odG1sKFwiXCIpO1xuXG4gICAgICAgICAgICBjb3VudGVyID0gMDtcbiAgICAgICAgICAgIGxhc3RQYWdlID0gYmFzZS5pdGVtc0Ftb3VudCAtIGJhc2UuaXRlbXNBbW91bnQgJSBiYXNlLm9wdGlvbnMuaXRlbXM7XG5cbiAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBiYXNlLml0ZW1zQW1vdW50OyBpICs9IDEpIHtcbiAgICAgICAgICAgICAgICBpZiAoaSAlIGJhc2Uub3B0aW9ucy5pdGVtcyA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICBjb3VudGVyICs9IDE7XG4gICAgICAgICAgICAgICAgICAgIGlmIChsYXN0UGFnZSA9PT0gaSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGFzdEl0ZW0gPSBiYXNlLml0ZW1zQW1vdW50IC0gYmFzZS5vcHRpb25zLml0ZW1zO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHBhZ2luYXRpb25CdXR0b24gPSAkKFwiPGRpdi8+XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiY2xhc3NcIiA6IFwib3dsLXBhZ2VcIlxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcGFnaW5hdGlvbkJ1dHRvbklubmVyID0gJChcIjxzcGFuPjwvc3Bhbj5cIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0ZXh0XCI6IGJhc2Uub3B0aW9ucy5wYWdpbmF0aW9uTnVtYmVycyA9PT0gdHJ1ZSA/IGNvdW50ZXIgOiBcIlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJjbGFzc1wiOiBiYXNlLm9wdGlvbnMucGFnaW5hdGlvbk51bWJlcnMgPT09IHRydWUgPyBcIm93bC1udW1iZXJzXCIgOiBcIlwiXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBwYWdpbmF0aW9uQnV0dG9uLmFwcGVuZChwYWdpbmF0aW9uQnV0dG9uSW5uZXIpO1xuXG4gICAgICAgICAgICAgICAgICAgIHBhZ2luYXRpb25CdXR0b24uZGF0YShcIm93bC1wYWdlXCIsIGxhc3RQYWdlID09PSBpID8gbGFzdEl0ZW0gOiBpKTtcbiAgICAgICAgICAgICAgICAgICAgcGFnaW5hdGlvbkJ1dHRvbi5kYXRhKFwib3dsLXJvdW5kUGFnZXNcIiwgY291bnRlcik7XG5cbiAgICAgICAgICAgICAgICAgICAgYmFzZS5wYWdpbmF0aW9uV3JhcHBlci5hcHBlbmQocGFnaW5hdGlvbkJ1dHRvbik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYmFzZS5jaGVja1BhZ2luYXRpb24oKTtcbiAgICAgICAgfSxcbiAgICAgICAgY2hlY2tQYWdpbmF0aW9uIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5wYWdpbmF0aW9uID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGJhc2UucGFnaW5hdGlvbldyYXBwZXIuZmluZChcIi5vd2wtcGFnZVwiKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS5kYXRhKFwib3dsLXJvdW5kUGFnZXNcIikgPT09ICQoYmFzZS4kb3dsSXRlbXNbYmFzZS5jdXJyZW50SXRlbV0pLmRhdGEoXCJvd2wtcm91bmRQYWdlc1wiKSkge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLnBhZ2luYXRpb25XcmFwcGVyXG4gICAgICAgICAgICAgICAgICAgICAgICAuZmluZChcIi5vd2wtcGFnZVwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNoZWNrTmF2aWdhdGlvbiA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcblxuICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5uYXZpZ2F0aW9uID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMucmV3aW5kTmF2ID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIGlmIChiYXNlLmN1cnJlbnRJdGVtID09PSAwICYmIGJhc2UubWF4aW11bUl0ZW0gPT09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5idXR0b25QcmV2LmFkZENsYXNzKFwiZGlzYWJsZWRcIik7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UuYnV0dG9uTmV4dC5hZGRDbGFzcyhcImRpc2FibGVkXCIpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoYmFzZS5jdXJyZW50SXRlbSA9PT0gMCAmJiBiYXNlLm1heGltdW1JdGVtICE9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UuYnV0dG9uUHJldi5hZGRDbGFzcyhcImRpc2FibGVkXCIpO1xuICAgICAgICAgICAgICAgICAgICBiYXNlLmJ1dHRvbk5leHQucmVtb3ZlQ2xhc3MoXCJkaXNhYmxlZFwiKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGJhc2UuY3VycmVudEl0ZW0gPT09IGJhc2UubWF4aW11bUl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5idXR0b25QcmV2LnJlbW92ZUNsYXNzKFwiZGlzYWJsZWRcIik7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UuYnV0dG9uTmV4dC5hZGRDbGFzcyhcImRpc2FibGVkXCIpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoYmFzZS5jdXJyZW50SXRlbSAhPT0gMCAmJiBiYXNlLmN1cnJlbnRJdGVtICE9PSBiYXNlLm1heGltdW1JdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UuYnV0dG9uUHJldi5yZW1vdmVDbGFzcyhcImRpc2FibGVkXCIpO1xuICAgICAgICAgICAgICAgICAgICBiYXNlLmJ1dHRvbk5leHQucmVtb3ZlQ2xhc3MoXCJkaXNhYmxlZFwiKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgdXBkYXRlQ29udHJvbHMgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG4gICAgICAgICAgICBiYXNlLnVwZGF0ZVBhZ2luYXRpb24oKTtcbiAgICAgICAgICAgIGJhc2UuY2hlY2tOYXZpZ2F0aW9uKCk7XG4gICAgICAgICAgICBpZiAoYmFzZS5vd2xDb250cm9scykge1xuICAgICAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMuaXRlbXMgPj0gYmFzZS5pdGVtc0Ftb3VudCkge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLm93bENvbnRyb2xzLmhpZGUoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLm93bENvbnRyb2xzLnNob3coKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVzdHJveUNvbnRyb2xzIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgaWYgKGJhc2Uub3dsQ29udHJvbHMpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm93bENvbnRyb2xzLnJlbW92ZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIG5leHQgOiBmdW5jdGlvbiAoc3BlZWQpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcblxuICAgICAgICAgICAgaWYgKGJhc2UuaXNUcmFuc2l0aW9uKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBiYXNlLmN1cnJlbnRJdGVtICs9IGJhc2Uub3B0aW9ucy5zY3JvbGxQZXJQYWdlID09PSB0cnVlID8gYmFzZS5vcHRpb25zLml0ZW1zIDogMTtcbiAgICAgICAgICAgIGlmIChiYXNlLmN1cnJlbnRJdGVtID4gYmFzZS5tYXhpbXVtSXRlbSArIChiYXNlLm9wdGlvbnMuc2Nyb2xsUGVyUGFnZSA9PT0gdHJ1ZSA/IChiYXNlLm9wdGlvbnMuaXRlbXMgLSAxKSA6IDApKSB7XG4gICAgICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5yZXdpbmROYXYgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5jdXJyZW50SXRlbSA9IDA7XG4gICAgICAgICAgICAgICAgICAgIHNwZWVkID0gXCJyZXdpbmRcIjtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLmN1cnJlbnRJdGVtID0gYmFzZS5tYXhpbXVtSXRlbTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGJhc2UuZ29UbyhiYXNlLmN1cnJlbnRJdGVtLCBzcGVlZCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcHJldiA6IGZ1bmN0aW9uIChzcGVlZCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuXG4gICAgICAgICAgICBpZiAoYmFzZS5pc1RyYW5zaXRpb24pIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMuc2Nyb2xsUGVyUGFnZSA9PT0gdHJ1ZSAmJiBiYXNlLmN1cnJlbnRJdGVtID4gMCAmJiBiYXNlLmN1cnJlbnRJdGVtIDwgYmFzZS5vcHRpb25zLml0ZW1zKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5jdXJyZW50SXRlbSA9IDA7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGJhc2UuY3VycmVudEl0ZW0gLT0gYmFzZS5vcHRpb25zLnNjcm9sbFBlclBhZ2UgPT09IHRydWUgPyBiYXNlLm9wdGlvbnMuaXRlbXMgOiAxO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGJhc2UuY3VycmVudEl0ZW0gPCAwKSB7XG4gICAgICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5yZXdpbmROYXYgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5jdXJyZW50SXRlbSA9IGJhc2UubWF4aW11bUl0ZW07XG4gICAgICAgICAgICAgICAgICAgIHNwZWVkID0gXCJyZXdpbmRcIjtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLmN1cnJlbnRJdGVtID0gMDtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGJhc2UuZ29UbyhiYXNlLmN1cnJlbnRJdGVtLCBzcGVlZCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ29UbyA6IGZ1bmN0aW9uIChwb3NpdGlvbiwgc3BlZWQsIGRyYWcpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcyxcbiAgICAgICAgICAgICAgICBnb1RvUGl4ZWw7XG5cbiAgICAgICAgICAgIGlmIChiYXNlLmlzVHJhbnNpdGlvbikge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh0eXBlb2YgYmFzZS5vcHRpb25zLmJlZm9yZU1vdmUgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5iZWZvcmVNb3ZlLmFwcGx5KHRoaXMsIFtiYXNlLiRlbGVtXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAocG9zaXRpb24gPj0gYmFzZS5tYXhpbXVtSXRlbSkge1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uID0gYmFzZS5tYXhpbXVtSXRlbTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocG9zaXRpb24gPD0gMCkge1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uID0gMDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgYmFzZS5jdXJyZW50SXRlbSA9IGJhc2Uub3dsLmN1cnJlbnRJdGVtID0gcG9zaXRpb247XG4gICAgICAgICAgICBpZiAoYmFzZS5vcHRpb25zLnRyYW5zaXRpb25TdHlsZSAhPT0gZmFsc2UgJiYgZHJhZyAhPT0gXCJkcmFnXCIgJiYgYmFzZS5vcHRpb25zLml0ZW1zID09PSAxICYmIGJhc2UuYnJvd3Nlci5zdXBwb3J0M2QgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICBiYXNlLnN3YXBTcGVlZCgwKTtcbiAgICAgICAgICAgICAgICBpZiAoYmFzZS5icm93c2VyLnN1cHBvcnQzZCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLnRyYW5zaXRpb24zZChiYXNlLnBvc2l0aW9uc0luQXJyYXlbcG9zaXRpb25dKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLmNzczJzbGlkZShiYXNlLnBvc2l0aW9uc0luQXJyYXlbcG9zaXRpb25dLCAxKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYmFzZS5hZnRlckdvKCk7XG4gICAgICAgICAgICAgICAgYmFzZS5zaW5nbGVJdGVtVHJhbnNpdGlvbigpO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGdvVG9QaXhlbCA9IGJhc2UucG9zaXRpb25zSW5BcnJheVtwb3NpdGlvbl07XG5cbiAgICAgICAgICAgIGlmIChiYXNlLmJyb3dzZXIuc3VwcG9ydDNkID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5pc0NzczNGaW5pc2ggPSBmYWxzZTtcblxuICAgICAgICAgICAgICAgIGlmIChzcGVlZCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLnN3YXBTcGVlZChcInBhZ2luYXRpb25TcGVlZFwiKTtcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYmFzZS5pc0NzczNGaW5pc2ggPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9LCBiYXNlLm9wdGlvbnMucGFnaW5hdGlvblNwZWVkKTtcblxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3BlZWQgPT09IFwicmV3aW5kXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5zd2FwU3BlZWQoYmFzZS5vcHRpb25zLnJld2luZFNwZWVkKTtcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYmFzZS5pc0NzczNGaW5pc2ggPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9LCBiYXNlLm9wdGlvbnMucmV3aW5kU3BlZWQpO1xuXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5zd2FwU3BlZWQoXCJzbGlkZVNwZWVkXCIpO1xuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBiYXNlLmlzQ3NzM0ZpbmlzaCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH0sIGJhc2Uub3B0aW9ucy5zbGlkZVNwZWVkKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYmFzZS50cmFuc2l0aW9uM2QoZ29Ub1BpeGVsKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaWYgKHNwZWVkID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UuY3NzMnNsaWRlKGdvVG9QaXhlbCwgYmFzZS5vcHRpb25zLnBhZ2luYXRpb25TcGVlZCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzcGVlZCA9PT0gXCJyZXdpbmRcIikge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLmNzczJzbGlkZShnb1RvUGl4ZWwsIGJhc2Uub3B0aW9ucy5yZXdpbmRTcGVlZCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5jc3Myc2xpZGUoZ29Ub1BpeGVsLCBiYXNlLm9wdGlvbnMuc2xpZGVTcGVlZCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYmFzZS5hZnRlckdvKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAganVtcFRvIDogZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG4gICAgICAgICAgICBpZiAodHlwZW9mIGJhc2Uub3B0aW9ucy5iZWZvcmVNb3ZlID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm9wdGlvbnMuYmVmb3JlTW92ZS5hcHBseSh0aGlzLCBbYmFzZS4kZWxlbV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHBvc2l0aW9uID49IGJhc2UubWF4aW11bUl0ZW0gfHwgcG9zaXRpb24gPT09IC0xKSB7XG4gICAgICAgICAgICAgICAgcG9zaXRpb24gPSBiYXNlLm1heGltdW1JdGVtO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChwb3NpdGlvbiA8PSAwKSB7XG4gICAgICAgICAgICAgICAgcG9zaXRpb24gPSAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYmFzZS5zd2FwU3BlZWQoMCk7XG4gICAgICAgICAgICBpZiAoYmFzZS5icm93c2VyLnN1cHBvcnQzZCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIGJhc2UudHJhbnNpdGlvbjNkKGJhc2UucG9zaXRpb25zSW5BcnJheVtwb3NpdGlvbl0pO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBiYXNlLmNzczJzbGlkZShiYXNlLnBvc2l0aW9uc0luQXJyYXlbcG9zaXRpb25dLCAxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGJhc2UuY3VycmVudEl0ZW0gPSBiYXNlLm93bC5jdXJyZW50SXRlbSA9IHBvc2l0aW9uO1xuICAgICAgICAgICAgYmFzZS5hZnRlckdvKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgYWZ0ZXJHbyA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcblxuICAgICAgICAgICAgYmFzZS5wcmV2QXJyLnB1c2goYmFzZS5jdXJyZW50SXRlbSk7XG4gICAgICAgICAgICBiYXNlLnByZXZJdGVtID0gYmFzZS5vd2wucHJldkl0ZW0gPSBiYXNlLnByZXZBcnJbYmFzZS5wcmV2QXJyLmxlbmd0aCAtIDJdO1xuICAgICAgICAgICAgYmFzZS5wcmV2QXJyLnNoaWZ0KDApO1xuXG4gICAgICAgICAgICBpZiAoYmFzZS5wcmV2SXRlbSAhPT0gYmFzZS5jdXJyZW50SXRlbSkge1xuICAgICAgICAgICAgICAgIGJhc2UuY2hlY2tQYWdpbmF0aW9uKCk7XG4gICAgICAgICAgICAgICAgYmFzZS5jaGVja05hdmlnYXRpb24oKTtcbiAgICAgICAgICAgICAgICBiYXNlLmVhY2hNb3ZlVXBkYXRlKCk7XG5cbiAgICAgICAgICAgICAgICBpZiAoYmFzZS5vcHRpb25zLmF1dG9QbGF5ICE9PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLmNoZWNrQXAoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodHlwZW9mIGJhc2Uub3B0aW9ucy5hZnRlck1vdmUgPT09IFwiZnVuY3Rpb25cIiAmJiBiYXNlLnByZXZJdGVtICE9PSBiYXNlLmN1cnJlbnRJdGVtKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5vcHRpb25zLmFmdGVyTW92ZS5hcHBseSh0aGlzLCBbYmFzZS4kZWxlbV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHN0b3AgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG4gICAgICAgICAgICBiYXNlLmFwU3RhdHVzID0gXCJzdG9wXCI7XG4gICAgICAgICAgICB3aW5kb3cuY2xlYXJJbnRlcnZhbChiYXNlLmF1dG9QbGF5SW50ZXJ2YWwpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNoZWNrQXAgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG4gICAgICAgICAgICBpZiAoYmFzZS5hcFN0YXR1cyAhPT0gXCJzdG9wXCIpIHtcbiAgICAgICAgICAgICAgICBiYXNlLnBsYXkoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBwbGF5IDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgYmFzZS5hcFN0YXR1cyA9IFwicGxheVwiO1xuICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5hdXRvUGxheSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB3aW5kb3cuY2xlYXJJbnRlcnZhbChiYXNlLmF1dG9QbGF5SW50ZXJ2YWwpO1xuICAgICAgICAgICAgYmFzZS5hdXRvUGxheUludGVydmFsID0gd2luZG93LnNldEludGVydmFsKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm5leHQodHJ1ZSk7XG4gICAgICAgICAgICB9LCBiYXNlLm9wdGlvbnMuYXV0b1BsYXkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHN3YXBTcGVlZCA6IGZ1bmN0aW9uIChhY3Rpb24pIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcbiAgICAgICAgICAgIGlmIChhY3Rpb24gPT09IFwic2xpZGVTcGVlZFwiKSB7XG4gICAgICAgICAgICAgICAgYmFzZS4kb3dsV3JhcHBlci5jc3MoYmFzZS5hZGRDc3NTcGVlZChiYXNlLm9wdGlvbnMuc2xpZGVTcGVlZCkpO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChhY3Rpb24gPT09IFwicGFnaW5hdGlvblNwZWVkXCIpIHtcbiAgICAgICAgICAgICAgICBiYXNlLiRvd2xXcmFwcGVyLmNzcyhiYXNlLmFkZENzc1NwZWVkKGJhc2Uub3B0aW9ucy5wYWdpbmF0aW9uU3BlZWQpKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGFjdGlvbiAhPT0gXCJzdHJpbmdcIikge1xuICAgICAgICAgICAgICAgIGJhc2UuJG93bFdyYXBwZXIuY3NzKGJhc2UuYWRkQ3NzU3BlZWQoYWN0aW9uKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgYWRkQ3NzU3BlZWQgOiBmdW5jdGlvbiAoc3BlZWQpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgXCItd2Via2l0LXRyYW5zaXRpb25cIjogXCJhbGwgXCIgKyBzcGVlZCArIFwibXMgZWFzZVwiLFxuICAgICAgICAgICAgICAgIFwiLW1vei10cmFuc2l0aW9uXCI6IFwiYWxsIFwiICsgc3BlZWQgKyBcIm1zIGVhc2VcIixcbiAgICAgICAgICAgICAgICBcIi1vLXRyYW5zaXRpb25cIjogXCJhbGwgXCIgKyBzcGVlZCArIFwibXMgZWFzZVwiLFxuICAgICAgICAgICAgICAgIFwidHJhbnNpdGlvblwiOiBcImFsbCBcIiArIHNwZWVkICsgXCJtcyBlYXNlXCJcbiAgICAgICAgICAgIH07XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVtb3ZlVHJhbnNpdGlvbiA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgXCItd2Via2l0LXRyYW5zaXRpb25cIjogXCJcIixcbiAgICAgICAgICAgICAgICBcIi1tb3otdHJhbnNpdGlvblwiOiBcIlwiLFxuICAgICAgICAgICAgICAgIFwiLW8tdHJhbnNpdGlvblwiOiBcIlwiLFxuICAgICAgICAgICAgICAgIFwidHJhbnNpdGlvblwiOiBcIlwiXG4gICAgICAgICAgICB9O1xuICAgICAgICB9LFxuXG4gICAgICAgIGRvVHJhbnNsYXRlIDogZnVuY3Rpb24gKHBpeGVscykge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICBcIi13ZWJraXQtdHJhbnNmb3JtXCI6IFwidHJhbnNsYXRlM2QoXCIgKyBwaXhlbHMgKyBcInB4LCAwcHgsIDBweClcIixcbiAgICAgICAgICAgICAgICBcIi1tb3otdHJhbnNmb3JtXCI6IFwidHJhbnNsYXRlM2QoXCIgKyBwaXhlbHMgKyBcInB4LCAwcHgsIDBweClcIixcbiAgICAgICAgICAgICAgICBcIi1vLXRyYW5zZm9ybVwiOiBcInRyYW5zbGF0ZTNkKFwiICsgcGl4ZWxzICsgXCJweCwgMHB4LCAwcHgpXCIsXG4gICAgICAgICAgICAgICAgXCItbXMtdHJhbnNmb3JtXCI6IFwidHJhbnNsYXRlM2QoXCIgKyBwaXhlbHMgKyBcInB4LCAwcHgsIDBweClcIixcbiAgICAgICAgICAgICAgICBcInRyYW5zZm9ybVwiOiBcInRyYW5zbGF0ZTNkKFwiICsgcGl4ZWxzICsgXCJweCwgMHB4LDBweClcIlxuICAgICAgICAgICAgfTtcbiAgICAgICAgfSxcblxuICAgICAgICB0cmFuc2l0aW9uM2QgOiBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcbiAgICAgICAgICAgIGJhc2UuJG93bFdyYXBwZXIuY3NzKGJhc2UuZG9UcmFuc2xhdGUodmFsdWUpKTtcbiAgICAgICAgfSxcblxuICAgICAgICBjc3MybW92ZSA6IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgYmFzZS4kb3dsV3JhcHBlci5jc3Moe1wibGVmdFwiIDogdmFsdWV9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBjc3Myc2xpZGUgOiBmdW5jdGlvbiAodmFsdWUsIHNwZWVkKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG5cbiAgICAgICAgICAgIGJhc2UuaXNDc3NGaW5pc2ggPSBmYWxzZTtcbiAgICAgICAgICAgIGJhc2UuJG93bFdyYXBwZXIuc3RvcCh0cnVlLCB0cnVlKS5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICBcImxlZnRcIiA6IHZhbHVlXG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgZHVyYXRpb24gOiBzcGVlZCB8fCBiYXNlLm9wdGlvbnMuc2xpZGVTcGVlZCxcbiAgICAgICAgICAgICAgICBjb21wbGV0ZSA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5pc0Nzc0ZpbmlzaCA9IHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2hlY2tCcm93c2VyIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzLFxuICAgICAgICAgICAgICAgIHRyYW5zbGF0ZTNEID0gXCJ0cmFuc2xhdGUzZCgwcHgsIDBweCwgMHB4KVwiLFxuICAgICAgICAgICAgICAgIHRlbXBFbGVtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKSxcbiAgICAgICAgICAgICAgICByZWdleCxcbiAgICAgICAgICAgICAgICBhc1N1cHBvcnQsXG4gICAgICAgICAgICAgICAgc3VwcG9ydDNkLFxuICAgICAgICAgICAgICAgIGlzVG91Y2g7XG5cbiAgICAgICAgICAgIHRlbXBFbGVtLnN0eWxlLmNzc1RleHQgPSBcIiAgLW1vei10cmFuc2Zvcm06XCIgKyB0cmFuc2xhdGUzRCArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCI7IC1tcy10cmFuc2Zvcm06XCIgICAgICsgdHJhbnNsYXRlM0QgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiOyAtby10cmFuc2Zvcm06XCIgICAgICArIHRyYW5zbGF0ZTNEICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIjsgLXdlYmtpdC10cmFuc2Zvcm06XCIgKyB0cmFuc2xhdGUzRCArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCI7IHRyYW5zZm9ybTpcIiAgICAgICAgICsgdHJhbnNsYXRlM0Q7XG4gICAgICAgICAgICByZWdleCA9IC90cmFuc2xhdGUzZFxcKDBweCwgMHB4LCAwcHhcXCkvZztcbiAgICAgICAgICAgIGFzU3VwcG9ydCA9IHRlbXBFbGVtLnN0eWxlLmNzc1RleHQubWF0Y2gocmVnZXgpO1xuICAgICAgICAgICAgc3VwcG9ydDNkID0gKGFzU3VwcG9ydCAhPT0gbnVsbCAmJiBhc1N1cHBvcnQubGVuZ3RoID09PSAxKTtcblxuICAgICAgICAgICAgaXNUb3VjaCA9IFwib250b3VjaHN0YXJ0XCIgaW4gd2luZG93IHx8IHdpbmRvdy5uYXZpZ2F0b3IubXNNYXhUb3VjaFBvaW50cztcblxuICAgICAgICAgICAgYmFzZS5icm93c2VyID0ge1xuICAgICAgICAgICAgICAgIFwic3VwcG9ydDNkXCIgOiBzdXBwb3J0M2QsXG4gICAgICAgICAgICAgICAgXCJpc1RvdWNoXCIgOiBpc1RvdWNoXG4gICAgICAgICAgICB9O1xuICAgICAgICB9LFxuXG4gICAgICAgIG1vdmVFdmVudHMgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG4gICAgICAgICAgICBpZiAoYmFzZS5vcHRpb25zLm1vdXNlRHJhZyAhPT0gZmFsc2UgfHwgYmFzZS5vcHRpb25zLnRvdWNoRHJhZyAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICBiYXNlLmdlc3R1cmVzKCk7XG4gICAgICAgICAgICAgICAgYmFzZS5kaXNhYmxlZEV2ZW50cygpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGV2ZW50VHlwZXMgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXMsXG4gICAgICAgICAgICAgICAgdHlwZXMgPSBbXCJzXCIsIFwiZVwiLCBcInhcIl07XG5cbiAgICAgICAgICAgIGJhc2UuZXZfdHlwZXMgPSB7fTtcblxuICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5tb3VzZURyYWcgPT09IHRydWUgJiYgYmFzZS5vcHRpb25zLnRvdWNoRHJhZyA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIHR5cGVzID0gW1xuICAgICAgICAgICAgICAgICAgICBcInRvdWNoc3RhcnQub3dsIG1vdXNlZG93bi5vd2xcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0b3VjaG1vdmUub3dsIG1vdXNlbW92ZS5vd2xcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0b3VjaGVuZC5vd2wgdG91Y2hjYW5jZWwub3dsIG1vdXNldXAub3dsXCJcbiAgICAgICAgICAgICAgICBdO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChiYXNlLm9wdGlvbnMubW91c2VEcmFnID09PSBmYWxzZSAmJiBiYXNlLm9wdGlvbnMudG91Y2hEcmFnID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgdHlwZXMgPSBbXG4gICAgICAgICAgICAgICAgICAgIFwidG91Y2hzdGFydC5vd2xcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0b3VjaG1vdmUub3dsXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidG91Y2hlbmQub3dsIHRvdWNoY2FuY2VsLm93bFwiXG4gICAgICAgICAgICAgICAgXTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYmFzZS5vcHRpb25zLm1vdXNlRHJhZyA9PT0gdHJ1ZSAmJiBiYXNlLm9wdGlvbnMudG91Y2hEcmFnID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHR5cGVzID0gW1xuICAgICAgICAgICAgICAgICAgICBcIm1vdXNlZG93bi5vd2xcIixcbiAgICAgICAgICAgICAgICAgICAgXCJtb3VzZW1vdmUub3dsXCIsXG4gICAgICAgICAgICAgICAgICAgIFwibW91c2V1cC5vd2xcIlxuICAgICAgICAgICAgICAgIF07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGJhc2UuZXZfdHlwZXMuc3RhcnQgPSB0eXBlc1swXTtcbiAgICAgICAgICAgIGJhc2UuZXZfdHlwZXMubW92ZSA9IHR5cGVzWzFdO1xuICAgICAgICAgICAgYmFzZS5ldl90eXBlcy5lbmQgPSB0eXBlc1syXTtcbiAgICAgICAgfSxcblxuICAgICAgICBkaXNhYmxlZEV2ZW50cyA6ICBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG4gICAgICAgICAgICBiYXNlLiRlbGVtLm9uKFwiZHJhZ3N0YXJ0Lm93bFwiLCBmdW5jdGlvbiAoZXZlbnQpIHsgZXZlbnQucHJldmVudERlZmF1bHQoKTsgfSk7XG4gICAgICAgICAgICBiYXNlLiRlbGVtLm9uKFwibW91c2Vkb3duLmRpc2FibGVUZXh0U2VsZWN0XCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICQoZS50YXJnZXQpLmlzKCdpbnB1dCwgdGV4dGFyZWEsIHNlbGVjdCwgb3B0aW9uJyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBnZXN0dXJlcyA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIC8qanNsaW50IHVucGFyYW06IHRydWUqL1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzLFxuICAgICAgICAgICAgICAgIGxvY2FscyA9IHtcbiAgICAgICAgICAgICAgICAgICAgb2Zmc2V0WCA6IDAsXG4gICAgICAgICAgICAgICAgICAgIG9mZnNldFkgOiAwLFxuICAgICAgICAgICAgICAgICAgICBiYXNlRWxXaWR0aCA6IDAsXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aXZlUG9zIDogMCxcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IG51bGwsXG4gICAgICAgICAgICAgICAgICAgIG1pblN3aXBlIDogbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgbWF4U3dpcGU6IG51bGwsXG4gICAgICAgICAgICAgICAgICAgIHNsaWRpbmcgOiBudWxsLFxuICAgICAgICAgICAgICAgICAgICBkYXJnZ2luZzogbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0RWxlbWVudCA6IG51bGxcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBiYXNlLmlzQ3NzRmluaXNoID0gdHJ1ZTtcblxuICAgICAgICAgICAgZnVuY3Rpb24gZ2V0VG91Y2hlcyhldmVudCkge1xuICAgICAgICAgICAgICAgIGlmIChldmVudC50b3VjaGVzICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHggOiBldmVudC50b3VjaGVzWzBdLnBhZ2VYLFxuICAgICAgICAgICAgICAgICAgICAgICAgeSA6IGV2ZW50LnRvdWNoZXNbMF0ucGFnZVlcbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoZXZlbnQudG91Y2hlcyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChldmVudC5wYWdlWCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHggOiBldmVudC5wYWdlWCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB5IDogZXZlbnQucGFnZVlcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50LnBhZ2VYID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeCA6IGV2ZW50LmNsaWVudFgsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeSA6IGV2ZW50LmNsaWVudFlcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIHN3YXBFdmVudHModHlwZSkge1xuICAgICAgICAgICAgICAgIGlmICh0eXBlID09PSBcIm9uXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgJChkb2N1bWVudCkub24oYmFzZS5ldl90eXBlcy5tb3ZlLCBkcmFnTW92ZSk7XG4gICAgICAgICAgICAgICAgICAgICQoZG9jdW1lbnQpLm9uKGJhc2UuZXZfdHlwZXMuZW5kLCBkcmFnRW5kKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGUgPT09IFwib2ZmXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgJChkb2N1bWVudCkub2ZmKGJhc2UuZXZfdHlwZXMubW92ZSk7XG4gICAgICAgICAgICAgICAgICAgICQoZG9jdW1lbnQpLm9mZihiYXNlLmV2X3R5cGVzLmVuZCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBkcmFnU3RhcnQoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICB2YXIgZXYgPSBldmVudC5vcmlnaW5hbEV2ZW50IHx8IGV2ZW50IHx8IHdpbmRvdy5ldmVudCxcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb247XG5cbiAgICAgICAgICAgICAgICBpZiAoZXYud2hpY2ggPT09IDMpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoYmFzZS5pdGVtc0Ftb3VudCA8PSBiYXNlLm9wdGlvbnMuaXRlbXMpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoYmFzZS5pc0Nzc0ZpbmlzaCA9PT0gZmFsc2UgJiYgIWJhc2Uub3B0aW9ucy5kcmFnQmVmb3JlQW5pbUZpbmlzaCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChiYXNlLmlzQ3NzM0ZpbmlzaCA9PT0gZmFsc2UgJiYgIWJhc2Uub3B0aW9ucy5kcmFnQmVmb3JlQW5pbUZpbmlzaCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5hdXRvUGxheSAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LmNsZWFySW50ZXJ2YWwoYmFzZS5hdXRvUGxheUludGVydmFsKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoYmFzZS5icm93c2VyLmlzVG91Y2ggIT09IHRydWUgJiYgIWJhc2UuJG93bFdyYXBwZXIuaGFzQ2xhc3MoXCJncmFiYmluZ1wiKSkge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLiRvd2xXcmFwcGVyLmFkZENsYXNzKFwiZ3JhYmJpbmdcIik7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgYmFzZS5uZXdQb3NYID0gMDtcbiAgICAgICAgICAgICAgICBiYXNlLm5ld1JlbGF0aXZlWCA9IDA7XG5cbiAgICAgICAgICAgICAgICAkKHRoaXMpLmNzcyhiYXNlLnJlbW92ZVRyYW5zaXRpb24oKSk7XG5cbiAgICAgICAgICAgICAgICBwb3NpdGlvbiA9ICQodGhpcykucG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICBsb2NhbHMucmVsYXRpdmVQb3MgPSBwb3NpdGlvbi5sZWZ0O1xuXG4gICAgICAgICAgICAgICAgbG9jYWxzLm9mZnNldFggPSBnZXRUb3VjaGVzKGV2KS54IC0gcG9zaXRpb24ubGVmdDtcbiAgICAgICAgICAgICAgICBsb2NhbHMub2Zmc2V0WSA9IGdldFRvdWNoZXMoZXYpLnkgLSBwb3NpdGlvbi50b3A7XG5cbiAgICAgICAgICAgICAgICBzd2FwRXZlbnRzKFwib25cIik7XG5cbiAgICAgICAgICAgICAgICBsb2NhbHMuc2xpZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIGxvY2Fscy50YXJnZXRFbGVtZW50ID0gZXYudGFyZ2V0IHx8IGV2LnNyY0VsZW1lbnQ7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGRyYWdNb3ZlKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgdmFyIGV2ID0gZXZlbnQub3JpZ2luYWxFdmVudCB8fCBldmVudCB8fCB3aW5kb3cuZXZlbnQsXG4gICAgICAgICAgICAgICAgICAgIG1pblN3aXBlLFxuICAgICAgICAgICAgICAgICAgICBtYXhTd2lwZTtcblxuICAgICAgICAgICAgICAgIGJhc2UubmV3UG9zWCA9IGdldFRvdWNoZXMoZXYpLnggLSBsb2NhbHMub2Zmc2V0WDtcbiAgICAgICAgICAgICAgICBiYXNlLm5ld1Bvc1kgPSBnZXRUb3VjaGVzKGV2KS55IC0gbG9jYWxzLm9mZnNldFk7XG4gICAgICAgICAgICAgICAgYmFzZS5uZXdSZWxhdGl2ZVggPSBiYXNlLm5ld1Bvc1ggLSBsb2NhbHMucmVsYXRpdmVQb3M7XG5cbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGJhc2Uub3B0aW9ucy5zdGFydERyYWdnaW5nID09PSBcImZ1bmN0aW9uXCIgJiYgbG9jYWxzLmRyYWdnaW5nICE9PSB0cnVlICYmIGJhc2UubmV3UmVsYXRpdmVYICE9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIGxvY2Fscy5kcmFnZ2luZyA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIGJhc2Uub3B0aW9ucy5zdGFydERyYWdnaW5nLmFwcGx5KGJhc2UsIFtiYXNlLiRlbGVtXSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKChiYXNlLm5ld1JlbGF0aXZlWCA+IDggfHwgYmFzZS5uZXdSZWxhdGl2ZVggPCAtOCkgJiYgKGJhc2UuYnJvd3Nlci5pc1RvdWNoID09PSB0cnVlKSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoZXYucHJldmVudERlZmF1bHQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGV2LnJldHVyblZhbHVlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgbG9jYWxzLnNsaWRpbmcgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICgoYmFzZS5uZXdQb3NZID4gMTAgfHwgYmFzZS5uZXdQb3NZIDwgLTEwKSAmJiBsb2NhbHMuc2xpZGluZyA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgJChkb2N1bWVudCkub2ZmKFwidG91Y2htb3ZlLm93bFwiKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBtaW5Td2lwZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGJhc2UubmV3UmVsYXRpdmVYIC8gNTtcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgbWF4U3dpcGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBiYXNlLm1heGltdW1QaXhlbHMgKyBiYXNlLm5ld1JlbGF0aXZlWCAvIDU7XG4gICAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICAgIGJhc2UubmV3UG9zWCA9IE1hdGgubWF4KE1hdGgubWluKGJhc2UubmV3UG9zWCwgbWluU3dpcGUoKSksIG1heFN3aXBlKCkpO1xuICAgICAgICAgICAgICAgIGlmIChiYXNlLmJyb3dzZXIuc3VwcG9ydDNkID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UudHJhbnNpdGlvbjNkKGJhc2UubmV3UG9zWCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5jc3MybW92ZShiYXNlLm5ld1Bvc1gpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gZHJhZ0VuZChldmVudCkge1xuICAgICAgICAgICAgICAgIHZhciBldiA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQgfHwgZXZlbnQgfHwgd2luZG93LmV2ZW50LFxuICAgICAgICAgICAgICAgICAgICBuZXdQb3NpdGlvbixcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlcnMsXG4gICAgICAgICAgICAgICAgICAgIG93bFN0b3BFdmVudDtcblxuICAgICAgICAgICAgICAgIGV2LnRhcmdldCA9IGV2LnRhcmdldCB8fCBldi5zcmNFbGVtZW50O1xuXG4gICAgICAgICAgICAgICAgbG9jYWxzLmRyYWdnaW5nID0gZmFsc2U7XG5cbiAgICAgICAgICAgICAgICBpZiAoYmFzZS5icm93c2VyLmlzVG91Y2ggIT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS4kb3dsV3JhcHBlci5yZW1vdmVDbGFzcyhcImdyYWJiaW5nXCIpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChiYXNlLm5ld1JlbGF0aXZlWCA8IDApIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5kcmFnRGlyZWN0aW9uID0gYmFzZS5vd2wuZHJhZ0RpcmVjdGlvbiA9IFwibGVmdFwiO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UuZHJhZ0RpcmVjdGlvbiA9IGJhc2Uub3dsLmRyYWdEaXJlY3Rpb24gPSBcInJpZ2h0XCI7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKGJhc2UubmV3UmVsYXRpdmVYICE9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIG5ld1Bvc2l0aW9uID0gYmFzZS5nZXROZXdQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICBiYXNlLmdvVG8obmV3UG9zaXRpb24sIGZhbHNlLCBcImRyYWdcIik7XG4gICAgICAgICAgICAgICAgICAgIGlmIChsb2NhbHMudGFyZ2V0RWxlbWVudCA9PT0gZXYudGFyZ2V0ICYmIGJhc2UuYnJvd3Nlci5pc1RvdWNoICE9PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKGV2LnRhcmdldCkub24oXCJjbGljay5kaXNhYmxlXCIsIGZ1bmN0aW9uIChldikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChldi50YXJnZXQpLm9mZihcImNsaWNrLmRpc2FibGVcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZXJzID0gJC5fZGF0YShldi50YXJnZXQsIFwiZXZlbnRzXCIpLmNsaWNrO1xuICAgICAgICAgICAgICAgICAgICAgICAgb3dsU3RvcEV2ZW50ID0gaGFuZGxlcnMucG9wKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVycy5zcGxpY2UoMCwgMCwgb3dsU3RvcEV2ZW50KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBzd2FwRXZlbnRzKFwib2ZmXCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYmFzZS4kZWxlbS5vbihiYXNlLmV2X3R5cGVzLnN0YXJ0LCBcIi5vd2wtd3JhcHBlclwiLCBkcmFnU3RhcnQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldE5ld1Bvc2l0aW9uIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzLFxuICAgICAgICAgICAgICAgIG5ld1Bvc2l0aW9uID0gYmFzZS5jbG9zZXN0SXRlbSgpO1xuXG4gICAgICAgICAgICBpZiAobmV3UG9zaXRpb24gPiBiYXNlLm1heGltdW1JdGVtKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5jdXJyZW50SXRlbSA9IGJhc2UubWF4aW11bUl0ZW07XG4gICAgICAgICAgICAgICAgbmV3UG9zaXRpb24gID0gYmFzZS5tYXhpbXVtSXRlbTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYmFzZS5uZXdQb3NYID49IDApIHtcbiAgICAgICAgICAgICAgICBuZXdQb3NpdGlvbiA9IDA7XG4gICAgICAgICAgICAgICAgYmFzZS5jdXJyZW50SXRlbSA9IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbmV3UG9zaXRpb247XG4gICAgICAgIH0sXG4gICAgICAgIGNsb3Nlc3RJdGVtIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzLFxuICAgICAgICAgICAgICAgIGFycmF5ID0gYmFzZS5vcHRpb25zLnNjcm9sbFBlclBhZ2UgPT09IHRydWUgPyBiYXNlLnBhZ2VzSW5BcnJheSA6IGJhc2UucG9zaXRpb25zSW5BcnJheSxcbiAgICAgICAgICAgICAgICBnb2FsID0gYmFzZS5uZXdQb3NYLFxuICAgICAgICAgICAgICAgIGNsb3Nlc3QgPSBudWxsO1xuXG4gICAgICAgICAgICAkLmVhY2goYXJyYXksIGZ1bmN0aW9uIChpLCB2KSB7XG4gICAgICAgICAgICAgICAgaWYgKGdvYWwgLSAoYmFzZS5pdGVtV2lkdGggLyAyMCkgPiBhcnJheVtpICsgMV0gJiYgZ29hbCAtIChiYXNlLml0ZW1XaWR0aCAvIDIwKSA8IHYgJiYgYmFzZS5tb3ZlRGlyZWN0aW9uKCkgPT09IFwibGVmdFwiKSB7XG4gICAgICAgICAgICAgICAgICAgIGNsb3Nlc3QgPSB2O1xuICAgICAgICAgICAgICAgICAgICBpZiAoYmFzZS5vcHRpb25zLnNjcm9sbFBlclBhZ2UgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhc2UuY3VycmVudEl0ZW0gPSAkLmluQXJyYXkoY2xvc2VzdCwgYmFzZS5wb3NpdGlvbnNJbkFycmF5KTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhc2UuY3VycmVudEl0ZW0gPSBpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChnb2FsICsgKGJhc2UuaXRlbVdpZHRoIC8gMjApIDwgdiAmJiBnb2FsICsgKGJhc2UuaXRlbVdpZHRoIC8gMjApID4gKGFycmF5W2kgKyAxXSB8fCBhcnJheVtpXSAtIGJhc2UuaXRlbVdpZHRoKSAmJiBiYXNlLm1vdmVEaXJlY3Rpb24oKSA9PT0gXCJyaWdodFwiKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMuc2Nyb2xsUGVyUGFnZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2xvc2VzdCA9IGFycmF5W2kgKyAxXSB8fCBhcnJheVthcnJheS5sZW5ndGggLSAxXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhc2UuY3VycmVudEl0ZW0gPSAkLmluQXJyYXkoY2xvc2VzdCwgYmFzZS5wb3NpdGlvbnNJbkFycmF5KTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsb3Nlc3QgPSBhcnJheVtpICsgMV07XG4gICAgICAgICAgICAgICAgICAgICAgICBiYXNlLmN1cnJlbnRJdGVtID0gaSArIDE7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiBiYXNlLmN1cnJlbnRJdGVtO1xuICAgICAgICB9LFxuXG4gICAgICAgIG1vdmVEaXJlY3Rpb24gOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXMsXG4gICAgICAgICAgICAgICAgZGlyZWN0aW9uO1xuICAgICAgICAgICAgaWYgKGJhc2UubmV3UmVsYXRpdmVYIDwgMCkge1xuICAgICAgICAgICAgICAgIGRpcmVjdGlvbiA9IFwicmlnaHRcIjtcbiAgICAgICAgICAgICAgICBiYXNlLnBsYXlEaXJlY3Rpb24gPSBcIm5leHRcIjtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgZGlyZWN0aW9uID0gXCJsZWZ0XCI7XG4gICAgICAgICAgICAgICAgYmFzZS5wbGF5RGlyZWN0aW9uID0gXCJwcmV2XCI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZGlyZWN0aW9uO1xuICAgICAgICB9LFxuXG4gICAgICAgIGN1c3RvbUV2ZW50cyA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIC8qanNsaW50IHVucGFyYW06IHRydWUqL1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgYmFzZS4kZWxlbS5vbihcIm93bC5uZXh0XCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm5leHQoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYmFzZS4kZWxlbS5vbihcIm93bC5wcmV2XCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBiYXNlLnByZXYoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYmFzZS4kZWxlbS5vbihcIm93bC5wbGF5XCIsIGZ1bmN0aW9uIChldmVudCwgc3BlZWQpIHtcbiAgICAgICAgICAgICAgICBiYXNlLm9wdGlvbnMuYXV0b1BsYXkgPSBzcGVlZDtcbiAgICAgICAgICAgICAgICBiYXNlLnBsYXkoKTtcbiAgICAgICAgICAgICAgICBiYXNlLmhvdmVyU3RhdHVzID0gXCJwbGF5XCI7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGJhc2UuJGVsZW0ub24oXCJvd2wuc3RvcFwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5zdG9wKCk7XG4gICAgICAgICAgICAgICAgYmFzZS5ob3ZlclN0YXR1cyA9IFwic3RvcFwiO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBiYXNlLiRlbGVtLm9uKFwib3dsLmdvVG9cIiwgZnVuY3Rpb24gKGV2ZW50LCBpdGVtKSB7XG4gICAgICAgICAgICAgICAgYmFzZS5nb1RvKGl0ZW0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBiYXNlLiRlbGVtLm9uKFwib3dsLmp1bXBUb1wiLCBmdW5jdGlvbiAoZXZlbnQsIGl0ZW0pIHtcbiAgICAgICAgICAgICAgICBiYXNlLmp1bXBUbyhpdGVtKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIHN0b3BPbkhvdmVyIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgaWYgKGJhc2Uub3B0aW9ucy5zdG9wT25Ib3ZlciA9PT0gdHJ1ZSAmJiBiYXNlLmJyb3dzZXIuaXNUb3VjaCAhPT0gdHJ1ZSAmJiBiYXNlLm9wdGlvbnMuYXV0b1BsYXkgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgYmFzZS4kZWxlbS5vbihcIm1vdXNlb3ZlclwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2Uuc3RvcCgpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGJhc2UuJGVsZW0ub24oXCJtb3VzZW91dFwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChiYXNlLmhvdmVyU3RhdHVzICE9PSBcInN0b3BcIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgYmFzZS5wbGF5KCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBsYXp5TG9hZCA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcyxcbiAgICAgICAgICAgICAgICBpLFxuICAgICAgICAgICAgICAgICRpdGVtLFxuICAgICAgICAgICAgICAgIGl0ZW1OdW1iZXIsXG4gICAgICAgICAgICAgICAgJGxhenlJbWcsXG4gICAgICAgICAgICAgICAgZm9sbG93O1xuXG4gICAgICAgICAgICBpZiAoYmFzZS5vcHRpb25zLmxhenlMb2FkID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBiYXNlLml0ZW1zQW1vdW50OyBpICs9IDEpIHtcbiAgICAgICAgICAgICAgICAkaXRlbSA9ICQoYmFzZS4kb3dsSXRlbXNbaV0pO1xuXG4gICAgICAgICAgICAgICAgaWYgKCRpdGVtLmRhdGEoXCJvd2wtbG9hZGVkXCIpID09PSBcImxvYWRlZFwiKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGl0ZW1OdW1iZXIgPSAkaXRlbS5kYXRhKFwib3dsLWl0ZW1cIik7XG4gICAgICAgICAgICAgICAgJGxhenlJbWcgPSAkaXRlbS5maW5kKFwiLmxhenlPd2xcIik7XG5cbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mICRsYXp5SW1nLmRhdGEoXCJzcmNcIikgIT09IFwic3RyaW5nXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgJGl0ZW0uZGF0YShcIm93bC1sb2FkZWRcIiwgXCJsb2FkZWRcIik7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoJGl0ZW0uZGF0YShcIm93bC1sb2FkZWRcIikgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAkbGF6eUltZy5oaWRlKCk7XG4gICAgICAgICAgICAgICAgICAgICRpdGVtLmFkZENsYXNzKFwibG9hZGluZ1wiKS5kYXRhKFwib3dsLWxvYWRlZFwiLCBcImNoZWNrZWRcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMubGF6eUZvbGxvdyA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBmb2xsb3cgPSBpdGVtTnVtYmVyID49IGJhc2UuY3VycmVudEl0ZW07XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgZm9sbG93ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKGZvbGxvdyAmJiBpdGVtTnVtYmVyIDwgYmFzZS5jdXJyZW50SXRlbSArIGJhc2Uub3B0aW9ucy5pdGVtcyAmJiAkbGF6eUltZy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5sYXp5UHJlbG9hZCgkaXRlbSwgJGxhenlJbWcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBsYXp5UHJlbG9hZCA6IGZ1bmN0aW9uICgkaXRlbSwgJGxhenlJbWcpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcyxcbiAgICAgICAgICAgICAgICBpdGVyYXRpb25zID0gMCxcbiAgICAgICAgICAgICAgICBpc0JhY2tncm91bmRJbWc7XG5cbiAgICAgICAgICAgIGlmICgkbGF6eUltZy5wcm9wKFwidGFnTmFtZVwiKSA9PT0gXCJESVZcIikge1xuICAgICAgICAgICAgICAgICRsYXp5SW1nLmNzcyhcImJhY2tncm91bmQtaW1hZ2VcIiwgXCJ1cmwoXCIgKyAkbGF6eUltZy5kYXRhKFwic3JjXCIpICsgXCIpXCIpO1xuICAgICAgICAgICAgICAgIGlzQmFja2dyb3VuZEltZyA9IHRydWU7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICRsYXp5SW1nWzBdLnNyYyA9ICRsYXp5SW1nLmRhdGEoXCJzcmNcIik7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIHNob3dJbWFnZSgpIHtcbiAgICAgICAgICAgICAgICAkaXRlbS5kYXRhKFwib3dsLWxvYWRlZFwiLCBcImxvYWRlZFwiKS5yZW1vdmVDbGFzcyhcImxvYWRpbmdcIik7XG4gICAgICAgICAgICAgICAgJGxhenlJbWcucmVtb3ZlQXR0cihcImRhdGEtc3JjXCIpO1xuICAgICAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMubGF6eUVmZmVjdCA9PT0gXCJmYWRlXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgJGxhenlJbWcuZmFkZUluKDQwMCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgJGxhenlJbWcuc2hvdygpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGJhc2Uub3B0aW9ucy5hZnRlckxhenlMb2FkID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgYmFzZS5vcHRpb25zLmFmdGVyTGF6eUxvYWQuYXBwbHkodGhpcywgW2Jhc2UuJGVsZW1dKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGNoZWNrTGF6eUltYWdlKCkge1xuICAgICAgICAgICAgICAgIGl0ZXJhdGlvbnMgKz0gMTtcbiAgICAgICAgICAgICAgICBpZiAoYmFzZS5jb21wbGV0ZUltZygkbGF6eUltZy5nZXQoMCkpIHx8IGlzQmFja2dyb3VuZEltZyA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBzaG93SW1hZ2UoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGl0ZXJhdGlvbnMgPD0gMTAwKSB7Ly9pZiBpbWFnZSBsb2FkcyBpbiBsZXNzIHRoYW4gMTAgc2Vjb25kcyBcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LnNldFRpbWVvdXQoY2hlY2tMYXp5SW1hZ2UsIDEwMCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgc2hvd0ltYWdlKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBjaGVja0xhenlJbWFnZSgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGF1dG9IZWlnaHQgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXMsXG4gICAgICAgICAgICAgICAgJGN1cnJlbnRpbWcgPSAkKGJhc2UuJG93bEl0ZW1zW2Jhc2UuY3VycmVudEl0ZW1dKS5maW5kKFwiaW1nXCIpLFxuICAgICAgICAgICAgICAgIGl0ZXJhdGlvbnM7XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGFkZEhlaWdodCgpIHtcbiAgICAgICAgICAgICAgICB2YXIgJGN1cnJlbnRJdGVtID0gJChiYXNlLiRvd2xJdGVtc1tiYXNlLmN1cnJlbnRJdGVtXSkuaGVpZ2h0KCk7XG4gICAgICAgICAgICAgICAgYmFzZS53cmFwcGVyT3V0ZXIuY3NzKFwiaGVpZ2h0XCIsICRjdXJyZW50SXRlbSArIFwicHhcIik7XG4gICAgICAgICAgICAgICAgaWYgKCFiYXNlLndyYXBwZXJPdXRlci5oYXNDbGFzcyhcImF1dG9IZWlnaHRcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYmFzZS53cmFwcGVyT3V0ZXIuYWRkQ2xhc3MoXCJhdXRvSGVpZ2h0XCIpO1xuICAgICAgICAgICAgICAgICAgICB9LCAwKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGNoZWNrSW1hZ2UoKSB7XG4gICAgICAgICAgICAgICAgaXRlcmF0aW9ucyArPSAxO1xuICAgICAgICAgICAgICAgIGlmIChiYXNlLmNvbXBsZXRlSW1nKCRjdXJyZW50aW1nLmdldCgwKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgYWRkSGVpZ2h0KCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChpdGVyYXRpb25zIDw9IDEwMCkgeyAvL2lmIGltYWdlIGxvYWRzIGluIGxlc3MgdGhhbiAxMCBzZWNvbmRzIFxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2V0VGltZW91dChjaGVja0ltYWdlLCAxMDApO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2Uud3JhcHBlck91dGVyLmNzcyhcImhlaWdodFwiLCBcIlwiKTsgLy9FbHNlIHJlbW92ZSBoZWlnaHQgYXR0cmlidXRlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoJGN1cnJlbnRpbWcuZ2V0KDApICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBpdGVyYXRpb25zID0gMDtcbiAgICAgICAgICAgICAgICBjaGVja0ltYWdlKCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGFkZEhlaWdodCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGNvbXBsZXRlSW1nIDogZnVuY3Rpb24gKGltZykge1xuICAgICAgICAgICAgdmFyIG5hdHVyYWxXaWR0aFR5cGU7XG5cbiAgICAgICAgICAgIGlmICghaW1nLmNvbXBsZXRlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbmF0dXJhbFdpZHRoVHlwZSA9IHR5cGVvZiBpbWcubmF0dXJhbFdpZHRoO1xuICAgICAgICAgICAgaWYgKG5hdHVyYWxXaWR0aFR5cGUgIT09IFwidW5kZWZpbmVkXCIgJiYgaW1nLm5hdHVyYWxXaWR0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9LFxuXG4gICAgICAgIG9uVmlzaWJsZUl0ZW1zIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzLFxuICAgICAgICAgICAgICAgIGk7XG5cbiAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMuYWRkQ2xhc3NBY3RpdmUgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICBiYXNlLiRvd2xJdGVtcy5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGJhc2UudmlzaWJsZUl0ZW1zID0gW107XG4gICAgICAgICAgICBmb3IgKGkgPSBiYXNlLmN1cnJlbnRJdGVtOyBpIDwgYmFzZS5jdXJyZW50SXRlbSArIGJhc2Uub3B0aW9ucy5pdGVtczsgaSArPSAxKSB7XG4gICAgICAgICAgICAgICAgYmFzZS52aXNpYmxlSXRlbXMucHVzaChpKTtcblxuICAgICAgICAgICAgICAgIGlmIChiYXNlLm9wdGlvbnMuYWRkQ2xhc3NBY3RpdmUgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgJChiYXNlLiRvd2xJdGVtc1tpXSkuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYmFzZS5vd2wudmlzaWJsZUl0ZW1zID0gYmFzZS52aXNpYmxlSXRlbXM7XG4gICAgICAgIH0sXG5cbiAgICAgICAgdHJhbnNpdGlvblR5cGVzIDogZnVuY3Rpb24gKGNsYXNzTmFtZSkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgLy9DdXJyZW50bHkgYXZhaWxhYmxlOiBcImZhZGVcIiwgXCJiYWNrU2xpZGVcIiwgXCJnb0Rvd25cIiwgXCJmYWRlVXBcIlxuICAgICAgICAgICAgYmFzZS5vdXRDbGFzcyA9IFwib3dsLVwiICsgY2xhc3NOYW1lICsgXCItb3V0XCI7XG4gICAgICAgICAgICBiYXNlLmluQ2xhc3MgPSBcIm93bC1cIiArIGNsYXNzTmFtZSArIFwiLWluXCI7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2luZ2xlSXRlbVRyYW5zaXRpb24gOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXMsXG4gICAgICAgICAgICAgICAgb3V0Q2xhc3MgPSBiYXNlLm91dENsYXNzLFxuICAgICAgICAgICAgICAgIGluQ2xhc3MgPSBiYXNlLmluQ2xhc3MsXG4gICAgICAgICAgICAgICAgJGN1cnJlbnRJdGVtID0gYmFzZS4kb3dsSXRlbXMuZXEoYmFzZS5jdXJyZW50SXRlbSksXG4gICAgICAgICAgICAgICAgJHByZXZJdGVtID0gYmFzZS4kb3dsSXRlbXMuZXEoYmFzZS5wcmV2SXRlbSksXG4gICAgICAgICAgICAgICAgcHJldlBvcyA9IE1hdGguYWJzKGJhc2UucG9zaXRpb25zSW5BcnJheVtiYXNlLmN1cnJlbnRJdGVtXSkgKyBiYXNlLnBvc2l0aW9uc0luQXJyYXlbYmFzZS5wcmV2SXRlbV0sXG4gICAgICAgICAgICAgICAgb3JpZ2luID0gTWF0aC5hYnMoYmFzZS5wb3NpdGlvbnNJbkFycmF5W2Jhc2UuY3VycmVudEl0ZW1dKSArIGJhc2UuaXRlbVdpZHRoIC8gMixcbiAgICAgICAgICAgICAgICBhbmltRW5kID0gJ3dlYmtpdEFuaW1hdGlvbkVuZCBvQW5pbWF0aW9uRW5kIE1TQW5pbWF0aW9uRW5kIGFuaW1hdGlvbmVuZCc7XG5cbiAgICAgICAgICAgIGJhc2UuaXNUcmFuc2l0aW9uID0gdHJ1ZTtcblxuICAgICAgICAgICAgYmFzZS4kb3dsV3JhcHBlclxuICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnb3dsLW9yaWdpbicpXG4gICAgICAgICAgICAgICAgLmNzcyh7XG4gICAgICAgICAgICAgICAgICAgIFwiLXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luXCIgOiBvcmlnaW4gKyBcInB4XCIsXG4gICAgICAgICAgICAgICAgICAgIFwiLW1vei1wZXJzcGVjdGl2ZS1vcmlnaW5cIiA6IG9yaWdpbiArIFwicHhcIixcbiAgICAgICAgICAgICAgICAgICAgXCJwZXJzcGVjdGl2ZS1vcmlnaW5cIiA6IG9yaWdpbiArIFwicHhcIlxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgZnVuY3Rpb24gdHJhbnNTdHlsZXMocHJldlBvcykge1xuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIFwicG9zaXRpb25cIiA6IFwicmVsYXRpdmVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJsZWZ0XCIgOiBwcmV2UG9zICsgXCJweFwiXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHByZXZJdGVtXG4gICAgICAgICAgICAgICAgLmNzcyh0cmFuc1N0eWxlcyhwcmV2UG9zLCAxMCkpXG4gICAgICAgICAgICAgICAgLmFkZENsYXNzKG91dENsYXNzKVxuICAgICAgICAgICAgICAgIC5vbihhbmltRW5kLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UuZW5kUHJldiA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICRwcmV2SXRlbS5vZmYoYW5pbUVuZCk7XG4gICAgICAgICAgICAgICAgICAgIGJhc2UuY2xlYXJUcmFuc1N0eWxlKCRwcmV2SXRlbSwgb3V0Q2xhc3MpO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkY3VycmVudEl0ZW1cbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoaW5DbGFzcylcbiAgICAgICAgICAgICAgICAub24oYW5pbUVuZCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLmVuZEN1cnJlbnQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAkY3VycmVudEl0ZW0ub2ZmKGFuaW1FbmQpO1xuICAgICAgICAgICAgICAgICAgICBiYXNlLmNsZWFyVHJhbnNTdHlsZSgkY3VycmVudEl0ZW0sIGluQ2xhc3MpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNsZWFyVHJhbnNTdHlsZSA6IGZ1bmN0aW9uIChpdGVtLCBjbGFzc1RvUmVtb3ZlKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXM7XG4gICAgICAgICAgICBpdGVtLmNzcyh7XG4gICAgICAgICAgICAgICAgXCJwb3NpdGlvblwiIDogXCJcIixcbiAgICAgICAgICAgICAgICBcImxlZnRcIiA6IFwiXCJcbiAgICAgICAgICAgIH0pLnJlbW92ZUNsYXNzKGNsYXNzVG9SZW1vdmUpO1xuXG4gICAgICAgICAgICBpZiAoYmFzZS5lbmRQcmV2ICYmIGJhc2UuZW5kQ3VycmVudCkge1xuICAgICAgICAgICAgICAgIGJhc2UuJG93bFdyYXBwZXIucmVtb3ZlQ2xhc3MoJ293bC1vcmlnaW4nKTtcbiAgICAgICAgICAgICAgICBiYXNlLmVuZFByZXYgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBiYXNlLmVuZEN1cnJlbnQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBiYXNlLmlzVHJhbnNpdGlvbiA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIG93bFN0YXR1cyA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcbiAgICAgICAgICAgIGJhc2Uub3dsID0ge1xuICAgICAgICAgICAgICAgIFwidXNlck9wdGlvbnNcIiAgIDogYmFzZS51c2VyT3B0aW9ucyxcbiAgICAgICAgICAgICAgICBcImJhc2VFbGVtZW50XCIgICA6IGJhc2UuJGVsZW0sXG4gICAgICAgICAgICAgICAgXCJ1c2VySXRlbXNcIiAgICAgOiBiYXNlLiR1c2VySXRlbXMsXG4gICAgICAgICAgICAgICAgXCJvd2xJdGVtc1wiICAgICAgOiBiYXNlLiRvd2xJdGVtcyxcbiAgICAgICAgICAgICAgICBcImN1cnJlbnRJdGVtXCIgICA6IGJhc2UuY3VycmVudEl0ZW0sXG4gICAgICAgICAgICAgICAgXCJwcmV2SXRlbVwiICAgICAgOiBiYXNlLnByZXZJdGVtLFxuICAgICAgICAgICAgICAgIFwidmlzaWJsZUl0ZW1zXCIgIDogYmFzZS52aXNpYmxlSXRlbXMsXG4gICAgICAgICAgICAgICAgXCJpc1RvdWNoXCIgICAgICAgOiBiYXNlLmJyb3dzZXIuaXNUb3VjaCxcbiAgICAgICAgICAgICAgICBcImJyb3dzZXJcIiAgICAgICA6IGJhc2UuYnJvd3NlcixcbiAgICAgICAgICAgICAgICBcImRyYWdEaXJlY3Rpb25cIiA6IGJhc2UuZHJhZ0RpcmVjdGlvblxuICAgICAgICAgICAgfTtcbiAgICAgICAgfSxcblxuICAgICAgICBjbGVhckV2ZW50cyA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcbiAgICAgICAgICAgIGJhc2UuJGVsZW0ub2ZmKFwiLm93bCBvd2wgbW91c2Vkb3duLmRpc2FibGVUZXh0U2VsZWN0XCIpO1xuICAgICAgICAgICAgJChkb2N1bWVudCkub2ZmKFwiLm93bCBvd2xcIik7XG4gICAgICAgICAgICAkKHdpbmRvdykub2ZmKFwicmVzaXplXCIsIGJhc2UucmVzaXplcik7XG4gICAgICAgIH0sXG5cbiAgICAgICAgdW5XcmFwIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzO1xuICAgICAgICAgICAgaWYgKGJhc2UuJGVsZW0uY2hpbGRyZW4oKS5sZW5ndGggIT09IDApIHtcbiAgICAgICAgICAgICAgICBiYXNlLiRvd2xXcmFwcGVyLnVud3JhcCgpO1xuICAgICAgICAgICAgICAgIGJhc2UuJHVzZXJJdGVtcy51bndyYXAoKS51bndyYXAoKTtcbiAgICAgICAgICAgICAgICBpZiAoYmFzZS5vd2xDb250cm9scykge1xuICAgICAgICAgICAgICAgICAgICBiYXNlLm93bENvbnRyb2xzLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGJhc2UuY2xlYXJFdmVudHMoKTtcbiAgICAgICAgICAgIGJhc2UuJGVsZW1cbiAgICAgICAgICAgICAgICAuYXR0cihcInN0eWxlXCIsIGJhc2UuJGVsZW0uZGF0YShcIm93bC1vcmlnaW5hbFN0eWxlc1wiKSB8fCBcIlwiKVxuICAgICAgICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgYmFzZS4kZWxlbS5kYXRhKFwib3dsLW9yaWdpbmFsQ2xhc3Nlc1wiKSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVzdHJveSA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBiYXNlID0gdGhpcztcbiAgICAgICAgICAgIGJhc2Uuc3RvcCgpO1xuICAgICAgICAgICAgd2luZG93LmNsZWFySW50ZXJ2YWwoYmFzZS5jaGVja1Zpc2libGUpO1xuICAgICAgICAgICAgYmFzZS51bldyYXAoKTtcbiAgICAgICAgICAgIGJhc2UuJGVsZW0ucmVtb3ZlRGF0YSgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlaW5pdCA6IGZ1bmN0aW9uIChuZXdPcHRpb25zKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXMsXG4gICAgICAgICAgICAgICAgb3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCBiYXNlLnVzZXJPcHRpb25zLCBuZXdPcHRpb25zKTtcbiAgICAgICAgICAgIGJhc2UudW5XcmFwKCk7XG4gICAgICAgICAgICBiYXNlLmluaXQob3B0aW9ucywgYmFzZS4kZWxlbSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgYWRkSXRlbSA6IGZ1bmN0aW9uIChodG1sU3RyaW5nLCB0YXJnZXRQb3NpdGlvbikge1xuICAgICAgICAgICAgdmFyIGJhc2UgPSB0aGlzLFxuICAgICAgICAgICAgICAgIHBvc2l0aW9uO1xuXG4gICAgICAgICAgICBpZiAoIWh0bWxTdHJpbmcpIHtyZXR1cm4gZmFsc2U7IH1cblxuICAgICAgICAgICAgaWYgKGJhc2UuJGVsZW0uY2hpbGRyZW4oKS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgICAgICBiYXNlLiRlbGVtLmFwcGVuZChodG1sU3RyaW5nKTtcbiAgICAgICAgICAgICAgICBiYXNlLnNldFZhcnMoKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBiYXNlLnVuV3JhcCgpO1xuICAgICAgICAgICAgaWYgKHRhcmdldFBvc2l0aW9uID09PSB1bmRlZmluZWQgfHwgdGFyZ2V0UG9zaXRpb24gPT09IC0xKSB7XG4gICAgICAgICAgICAgICAgcG9zaXRpb24gPSAtMTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcG9zaXRpb24gPSB0YXJnZXRQb3NpdGlvbjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChwb3NpdGlvbiA+PSBiYXNlLiR1c2VySXRlbXMubGVuZ3RoIHx8IHBvc2l0aW9uID09PSAtMSkge1xuICAgICAgICAgICAgICAgIGJhc2UuJHVzZXJJdGVtcy5lcSgtMSkuYWZ0ZXIoaHRtbFN0cmluZyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGJhc2UuJHVzZXJJdGVtcy5lcShwb3NpdGlvbikuYmVmb3JlKGh0bWxTdHJpbmcpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBiYXNlLnNldFZhcnMoKTtcbiAgICAgICAgfSxcblxuICAgICAgICByZW1vdmVJdGVtIDogZnVuY3Rpb24gKHRhcmdldFBvc2l0aW9uKSB7XG4gICAgICAgICAgICB2YXIgYmFzZSA9IHRoaXMsXG4gICAgICAgICAgICAgICAgcG9zaXRpb247XG5cbiAgICAgICAgICAgIGlmIChiYXNlLiRlbGVtLmNoaWxkcmVuKCkubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRhcmdldFBvc2l0aW9uID09PSB1bmRlZmluZWQgfHwgdGFyZ2V0UG9zaXRpb24gPT09IC0xKSB7XG4gICAgICAgICAgICAgICAgcG9zaXRpb24gPSAtMTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcG9zaXRpb24gPSB0YXJnZXRQb3NpdGlvbjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgYmFzZS51bldyYXAoKTtcbiAgICAgICAgICAgIGJhc2UuJHVzZXJJdGVtcy5lcShwb3NpdGlvbikucmVtb3ZlKCk7XG4gICAgICAgICAgICBiYXNlLnNldFZhcnMoKTtcbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgICQuZm4ub3dsQ2Fyb3VzZWwgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICgkKHRoaXMpLmRhdGEoXCJvd2wtaW5pdFwiKSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICQodGhpcykuZGF0YShcIm93bC1pbml0XCIsIHRydWUpO1xuICAgICAgICAgICAgdmFyIGNhcm91c2VsID0gT2JqZWN0LmNyZWF0ZShDYXJvdXNlbCk7XG4gICAgICAgICAgICBjYXJvdXNlbC5pbml0KG9wdGlvbnMsIHRoaXMpO1xuICAgICAgICAgICAgJC5kYXRhKHRoaXMsIFwib3dsQ2Fyb3VzZWxcIiwgY2Fyb3VzZWwpO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgJC5mbi5vd2xDYXJvdXNlbC5vcHRpb25zID0ge1xuXG4gICAgICAgIGl0ZW1zIDogNSxcbiAgICAgICAgaXRlbXNDdXN0b20gOiBmYWxzZSxcbiAgICAgICAgaXRlbXNEZXNrdG9wIDogWzExOTksIDRdLFxuICAgICAgICBpdGVtc0Rlc2t0b3BTbWFsbCA6IFs5NzksIDNdLFxuICAgICAgICBpdGVtc1RhYmxldCA6IFs3NjgsIDJdLFxuICAgICAgICBpdGVtc1RhYmxldFNtYWxsIDogZmFsc2UsXG4gICAgICAgIGl0ZW1zTW9iaWxlIDogWzQ3OSwgMV0sXG4gICAgICAgIHNpbmdsZUl0ZW0gOiBmYWxzZSxcbiAgICAgICAgaXRlbXNTY2FsZVVwIDogZmFsc2UsXG5cbiAgICAgICAgc2xpZGVTcGVlZCA6IDIwMCxcbiAgICAgICAgcGFnaW5hdGlvblNwZWVkIDogODAwLFxuICAgICAgICByZXdpbmRTcGVlZCA6IDEwMDAsXG5cbiAgICAgICAgYXV0b1BsYXkgOiBmYWxzZSxcbiAgICAgICAgc3RvcE9uSG92ZXIgOiBmYWxzZSxcblxuICAgICAgICBuYXZpZ2F0aW9uIDogZmFsc2UsXG4gICAgICAgIG5hdmlnYXRpb25UZXh0IDogW1wicHJldlwiLCBcIm5leHRcIl0sXG4gICAgICAgIHJld2luZE5hdiA6IHRydWUsXG4gICAgICAgIHNjcm9sbFBlclBhZ2UgOiBmYWxzZSxcblxuICAgICAgICBwYWdpbmF0aW9uIDogdHJ1ZSxcbiAgICAgICAgcGFnaW5hdGlvbk51bWJlcnMgOiBmYWxzZSxcblxuICAgICAgICByZXNwb25zaXZlIDogdHJ1ZSxcbiAgICAgICAgcmVzcG9uc2l2ZVJlZnJlc2hSYXRlIDogMjAwLFxuICAgICAgICByZXNwb25zaXZlQmFzZVdpZHRoIDogd2luZG93LFxuXG4gICAgICAgIGJhc2VDbGFzcyA6IFwib3dsLWNhcm91c2VsXCIsXG4gICAgICAgIHRoZW1lIDogXCJvd2wtdGhlbWVcIixcblxuICAgICAgICBsYXp5TG9hZCA6IGZhbHNlLFxuICAgICAgICBsYXp5Rm9sbG93IDogdHJ1ZSxcbiAgICAgICAgbGF6eUVmZmVjdCA6IFwiZmFkZVwiLFxuXG4gICAgICAgIGF1dG9IZWlnaHQgOiBmYWxzZSxcblxuICAgICAgICBqc29uUGF0aCA6IGZhbHNlLFxuICAgICAgICBqc29uU3VjY2VzcyA6IGZhbHNlLFxuXG4gICAgICAgIGRyYWdCZWZvcmVBbmltRmluaXNoIDogdHJ1ZSxcbiAgICAgICAgbW91c2VEcmFnIDogdHJ1ZSxcbiAgICAgICAgdG91Y2hEcmFnIDogdHJ1ZSxcblxuICAgICAgICBhZGRDbGFzc0FjdGl2ZSA6IGZhbHNlLFxuICAgICAgICB0cmFuc2l0aW9uU3R5bGUgOiBmYWxzZSxcblxuICAgICAgICBiZWZvcmVVcGRhdGUgOiBmYWxzZSxcbiAgICAgICAgYWZ0ZXJVcGRhdGUgOiBmYWxzZSxcbiAgICAgICAgYmVmb3JlSW5pdCA6IGZhbHNlLFxuICAgICAgICBhZnRlckluaXQgOiBmYWxzZSxcbiAgICAgICAgYmVmb3JlTW92ZSA6IGZhbHNlLFxuICAgICAgICBhZnRlck1vdmUgOiBmYWxzZSxcbiAgICAgICAgYWZ0ZXJBY3Rpb24gOiBmYWxzZSxcbiAgICAgICAgc3RhcnREcmFnZ2luZyA6IGZhbHNlLFxuICAgICAgICBhZnRlckxhenlMb2FkOiBmYWxzZVxuICAgIH07XG59KGpRdWVyeSwgd2luZG93LCBkb2N1bWVudCkpOyIsIi8qXG4gQW5ndWxhckpTIHYxLjMuMjBcbiAoYykgMjAxMC0yMDE0IEdvb2dsZSwgSW5jLiBodHRwOi8vYW5ndWxhcmpzLm9yZ1xuIExpY2Vuc2U6IE1JVFxuKi9cbihmdW5jdGlvbihSLFcsdSl7J3VzZSBzdHJpY3QnO2Z1bmN0aW9uIFMoYil7cmV0dXJuIGZ1bmN0aW9uKCl7dmFyIGE9YXJndW1lbnRzWzBdLGM7Yz1cIltcIisoYj9iK1wiOlwiOlwiXCIpK2ErXCJdIGh0dHA6Ly9lcnJvcnMuYW5ndWxhcmpzLm9yZy8xLjMuMjAvXCIrKGI/YitcIi9cIjpcIlwiKSthO2ZvcihhPTE7YTxhcmd1bWVudHMubGVuZ3RoO2ErKyl7Yz1jKygxPT1hP1wiP1wiOlwiJlwiKStcInBcIisoYS0xKStcIj1cIjt2YXIgZD1lbmNvZGVVUklDb21wb25lbnQsZTtlPWFyZ3VtZW50c1thXTtlPVwiZnVuY3Rpb25cIj09dHlwZW9mIGU/ZS50b1N0cmluZygpLnJlcGxhY2UoLyBcXHtbXFxzXFxTXSokLyxcIlwiKTpcInVuZGVmaW5lZFwiPT10eXBlb2YgZT9cInVuZGVmaW5lZFwiOlwic3RyaW5nXCIhPXR5cGVvZiBlP0pTT04uc3RyaW5naWZ5KGUpOmU7Yys9ZChlKX1yZXR1cm4gRXJyb3IoYyl9fWZ1bmN0aW9uIFRhKGIpe2lmKG51bGw9PWJ8fFVhKGIpKXJldHVybiExO3ZhciBhPVwibGVuZ3RoXCJpbiBPYmplY3QoYikmJmIubGVuZ3RoO1xucmV0dXJuIGIubm9kZVR5cGU9PT1xYSYmYT8hMDp4KGIpfHxIKGIpfHwwPT09YXx8XCJudW1iZXJcIj09PXR5cGVvZiBhJiYwPGEmJmEtMSBpbiBifWZ1bmN0aW9uIHIoYixhLGMpe3ZhciBkLGU7aWYoYilpZih6KGIpKWZvcihkIGluIGIpXCJwcm90b3R5cGVcIj09ZHx8XCJsZW5ndGhcIj09ZHx8XCJuYW1lXCI9PWR8fGIuaGFzT3duUHJvcGVydHkmJiFiLmhhc093blByb3BlcnR5KGQpfHxhLmNhbGwoYyxiW2RdLGQsYik7ZWxzZSBpZihIKGIpfHxUYShiKSl7dmFyIGY9XCJvYmplY3RcIiE9PXR5cGVvZiBiO2Q9MDtmb3IoZT1iLmxlbmd0aDtkPGU7ZCsrKShmfHxkIGluIGIpJiZhLmNhbGwoYyxiW2RdLGQsYil9ZWxzZSBpZihiLmZvckVhY2gmJmIuZm9yRWFjaCE9PXIpYi5mb3JFYWNoKGEsYyxiKTtlbHNlIGZvcihkIGluIGIpYi5oYXNPd25Qcm9wZXJ0eShkKSYmYS5jYWxsKGMsYltkXSxkLGIpO3JldHVybiBifWZ1bmN0aW9uIEVkKGIsYSxjKXtmb3IodmFyIGQ9T2JqZWN0LmtleXMoYikuc29ydCgpLFxuZT0wO2U8ZC5sZW5ndGg7ZSsrKWEuY2FsbChjLGJbZFtlXV0sZFtlXSk7cmV0dXJuIGR9ZnVuY3Rpb24gbGMoYil7cmV0dXJuIGZ1bmN0aW9uKGEsYyl7YihjLGEpfX1mdW5jdGlvbiBGZCgpe3JldHVybisrcmJ9ZnVuY3Rpb24gbWMoYixhKXthP2IuJCRoYXNoS2V5PWE6ZGVsZXRlIGIuJCRoYXNoS2V5fWZ1bmN0aW9uIHcoYil7Zm9yKHZhciBhPWIuJCRoYXNoS2V5LGM9MSxkPWFyZ3VtZW50cy5sZW5ndGg7YzxkO2MrKyl7dmFyIGU9YXJndW1lbnRzW2NdO2lmKGUpZm9yKHZhciBmPU9iamVjdC5rZXlzKGUpLGc9MCxoPWYubGVuZ3RoO2c8aDtnKyspe3ZhciBsPWZbZ107YltsXT1lW2xdfX1tYyhiLGEpO3JldHVybiBifWZ1bmN0aW9uIGFhKGIpe3JldHVybiBwYXJzZUludChiLDEwKX1mdW5jdGlvbiBPYihiLGEpe3JldHVybiB3KE9iamVjdC5jcmVhdGUoYiksYSl9ZnVuY3Rpb24gQSgpe31mdW5jdGlvbiByYShiKXtyZXR1cm4gYn1mdW5jdGlvbiBlYShiKXtyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm4gYn19XG5mdW5jdGlvbiBEKGIpe3JldHVyblwidW5kZWZpbmVkXCI9PT10eXBlb2YgYn1mdW5jdGlvbiB5KGIpe3JldHVyblwidW5kZWZpbmVkXCIhPT10eXBlb2YgYn1mdW5jdGlvbiBMKGIpe3JldHVybiBudWxsIT09YiYmXCJvYmplY3RcIj09PXR5cGVvZiBifWZ1bmN0aW9uIHgoYil7cmV0dXJuXCJzdHJpbmdcIj09PXR5cGVvZiBifWZ1bmN0aW9uIFkoYil7cmV0dXJuXCJudW1iZXJcIj09PXR5cGVvZiBifWZ1bmN0aW9uIGhhKGIpe3JldHVyblwiW29iamVjdCBEYXRlXVwiPT09Q2EuY2FsbChiKX1mdW5jdGlvbiB6KGIpe3JldHVyblwiZnVuY3Rpb25cIj09PXR5cGVvZiBifWZ1bmN0aW9uIFZhKGIpe3JldHVyblwiW29iamVjdCBSZWdFeHBdXCI9PT1DYS5jYWxsKGIpfWZ1bmN0aW9uIFVhKGIpe3JldHVybiBiJiZiLndpbmRvdz09PWJ9ZnVuY3Rpb24gV2EoYil7cmV0dXJuIGImJmIuJGV2YWxBc3luYyYmYi4kd2F0Y2h9ZnVuY3Rpb24gWGEoYil7cmV0dXJuXCJib29sZWFuXCI9PT10eXBlb2YgYn1mdW5jdGlvbiBuYyhiKXtyZXR1cm4hKCFifHxcbiEoYi5ub2RlTmFtZXx8Yi5wcm9wJiZiLmF0dHImJmIuZmluZCkpfWZ1bmN0aW9uIEdkKGIpe3ZhciBhPXt9O2I9Yi5zcGxpdChcIixcIik7dmFyIGM7Zm9yKGM9MDtjPGIubGVuZ3RoO2MrKylhW2JbY11dPSEwO3JldHVybiBhfWZ1bmN0aW9uIHdhKGIpe3JldHVybiBLKGIubm9kZU5hbWV8fGJbMF0mJmJbMF0ubm9kZU5hbWUpfWZ1bmN0aW9uIFlhKGIsYSl7dmFyIGM9Yi5pbmRleE9mKGEpOzA8PWMmJmIuc3BsaWNlKGMsMSk7cmV0dXJuIGF9ZnVuY3Rpb24gRGEoYixhLGMsZCl7aWYoVWEoYil8fFdhKGIpKXRocm93IEphKFwiY3B3c1wiKTtpZihhKXtpZihiPT09YSl0aHJvdyBKYShcImNwaVwiKTtjPWN8fFtdO2Q9ZHx8W107aWYoTChiKSl7dmFyIGU9Yy5pbmRleE9mKGIpO2lmKC0xIT09ZSlyZXR1cm4gZFtlXTtjLnB1c2goYik7ZC5wdXNoKGEpfWlmKEgoYikpZm9yKHZhciBmPWEubGVuZ3RoPTA7ZjxiLmxlbmd0aDtmKyspZT1EYShiW2ZdLG51bGwsYyxkKSxMKGJbZl0pJiYoYy5wdXNoKGJbZl0pLFxuZC5wdXNoKGUpKSxhLnB1c2goZSk7ZWxzZXt2YXIgZz1hLiQkaGFzaEtleTtIKGEpP2EubGVuZ3RoPTA6cihhLGZ1bmN0aW9uKGIsYyl7ZGVsZXRlIGFbY119KTtmb3IoZiBpbiBiKWIuaGFzT3duUHJvcGVydHkoZikmJihlPURhKGJbZl0sbnVsbCxjLGQpLEwoYltmXSkmJihjLnB1c2goYltmXSksZC5wdXNoKGUpKSxhW2ZdPWUpO21jKGEsZyl9fWVsc2UgaWYoYT1iKUgoYik/YT1EYShiLFtdLGMsZCk6aGEoYik/YT1uZXcgRGF0ZShiLmdldFRpbWUoKSk6VmEoYik/KGE9bmV3IFJlZ0V4cChiLnNvdXJjZSxiLnRvU3RyaW5nKCkubWF0Y2goL1teXFwvXSokLylbMF0pLGEubGFzdEluZGV4PWIubGFzdEluZGV4KTpMKGIpJiYoZT1PYmplY3QuY3JlYXRlKE9iamVjdC5nZXRQcm90b3R5cGVPZihiKSksYT1EYShiLGUsYyxkKSk7cmV0dXJuIGF9ZnVuY3Rpb24gc2EoYixhKXtpZihIKGIpKXthPWF8fFtdO2Zvcih2YXIgYz0wLGQ9Yi5sZW5ndGg7YzxkO2MrKylhW2NdPWJbY119ZWxzZSBpZihMKGIpKWZvcihjIGluIGE9XG5hfHx7fSxiKWlmKFwiJFwiIT09Yy5jaGFyQXQoMCl8fFwiJFwiIT09Yy5jaGFyQXQoMSkpYVtjXT1iW2NdO3JldHVybiBhfHxifWZ1bmN0aW9uIGlhKGIsYSl7aWYoYj09PWEpcmV0dXJuITA7aWYobnVsbD09PWJ8fG51bGw9PT1hKXJldHVybiExO2lmKGIhPT1iJiZhIT09YSlyZXR1cm4hMDt2YXIgYz10eXBlb2YgYixkO2lmKGM9PXR5cGVvZiBhJiZcIm9iamVjdFwiPT1jKWlmKEgoYikpe2lmKCFIKGEpKXJldHVybiExO2lmKChjPWIubGVuZ3RoKT09YS5sZW5ndGgpe2ZvcihkPTA7ZDxjO2QrKylpZighaWEoYltkXSxhW2RdKSlyZXR1cm4hMTtyZXR1cm4hMH19ZWxzZXtpZihoYShiKSlyZXR1cm4gaGEoYSk/aWEoYi5nZXRUaW1lKCksYS5nZXRUaW1lKCkpOiExO2lmKFZhKGIpKXJldHVybiBWYShhKT9iLnRvU3RyaW5nKCk9PWEudG9TdHJpbmcoKTohMTtpZihXYShiKXx8V2EoYSl8fFVhKGIpfHxVYShhKXx8SChhKXx8aGEoYSl8fFZhKGEpKXJldHVybiExO2M9e307Zm9yKGQgaW4gYilpZihcIiRcIiE9PVxuZC5jaGFyQXQoMCkmJiF6KGJbZF0pKXtpZighaWEoYltkXSxhW2RdKSlyZXR1cm4hMTtjW2RdPSEwfWZvcihkIGluIGEpaWYoIWMuaGFzT3duUHJvcGVydHkoZCkmJlwiJFwiIT09ZC5jaGFyQXQoMCkmJmFbZF0hPT11JiYheihhW2RdKSlyZXR1cm4hMTtyZXR1cm4hMH1yZXR1cm4hMX1mdW5jdGlvbiBaYShiLGEsYyl7cmV0dXJuIGIuY29uY2F0KCRhLmNhbGwoYSxjKSl9ZnVuY3Rpb24gb2MoYixhKXt2YXIgYz0yPGFyZ3VtZW50cy5sZW5ndGg/JGEuY2FsbChhcmd1bWVudHMsMik6W107cmV0dXJuIXooYSl8fGEgaW5zdGFuY2VvZiBSZWdFeHA/YTpjLmxlbmd0aD9mdW5jdGlvbigpe3JldHVybiBhcmd1bWVudHMubGVuZ3RoP2EuYXBwbHkoYixaYShjLGFyZ3VtZW50cywwKSk6YS5hcHBseShiLGMpfTpmdW5jdGlvbigpe3JldHVybiBhcmd1bWVudHMubGVuZ3RoP2EuYXBwbHkoYixhcmd1bWVudHMpOmEuY2FsbChiKX19ZnVuY3Rpb24gSGQoYixhKXt2YXIgYz1hO1wic3RyaW5nXCI9PT10eXBlb2YgYiYmXG5cIiRcIj09PWIuY2hhckF0KDApJiZcIiRcIj09PWIuY2hhckF0KDEpP2M9dTpVYShhKT9jPVwiJFdJTkRPV1wiOmEmJlc9PT1hP2M9XCIkRE9DVU1FTlRcIjpXYShhKSYmKGM9XCIkU0NPUEVcIik7cmV0dXJuIGN9ZnVuY3Rpb24gYWIoYixhKXtpZihcInVuZGVmaW5lZFwiPT09dHlwZW9mIGIpcmV0dXJuIHU7WShhKXx8KGE9YT8yOm51bGwpO3JldHVybiBKU09OLnN0cmluZ2lmeShiLEhkLGEpfWZ1bmN0aW9uIHBjKGIpe3JldHVybiB4KGIpP0pTT04ucGFyc2UoYik6Yn1mdW5jdGlvbiB4YShiKXtiPUIoYikuY2xvbmUoKTt0cnl7Yi5lbXB0eSgpfWNhdGNoKGEpe312YXIgYz1CKFwiPGRpdj5cIikuYXBwZW5kKGIpLmh0bWwoKTt0cnl7cmV0dXJuIGJbMF0ubm9kZVR5cGU9PT1iYj9LKGMpOmMubWF0Y2goL14oPFtePl0rPikvKVsxXS5yZXBsYWNlKC9ePChbXFx3XFwtXSspLyxmdW5jdGlvbihhLGIpe3JldHVyblwiPFwiK0soYil9KX1jYXRjaChkKXtyZXR1cm4gSyhjKX19ZnVuY3Rpb24gcWMoYil7dHJ5e3JldHVybiBkZWNvZGVVUklDb21wb25lbnQoYil9Y2F0Y2goYSl7fX1cbmZ1bmN0aW9uIHJjKGIpe3ZhciBhPXt9LGMsZDtyKChifHxcIlwiKS5zcGxpdChcIiZcIiksZnVuY3Rpb24oYil7YiYmKGM9Yi5yZXBsYWNlKC9cXCsvZyxcIiUyMFwiKS5zcGxpdChcIj1cIiksZD1xYyhjWzBdKSx5KGQpJiYoYj15KGNbMV0pP3FjKGNbMV0pOiEwLHNjLmNhbGwoYSxkKT9IKGFbZF0pP2FbZF0ucHVzaChiKTphW2RdPVthW2RdLGJdOmFbZF09YikpfSk7cmV0dXJuIGF9ZnVuY3Rpb24gUGIoYil7dmFyIGE9W107cihiLGZ1bmN0aW9uKGIsZCl7SChiKT9yKGIsZnVuY3Rpb24oYil7YS5wdXNoKEVhKGQsITApKyghMD09PWI/XCJcIjpcIj1cIitFYShiLCEwKSkpfSk6YS5wdXNoKEVhKGQsITApKyghMD09PWI/XCJcIjpcIj1cIitFYShiLCEwKSkpfSk7cmV0dXJuIGEubGVuZ3RoP2Euam9pbihcIiZcIik6XCJcIn1mdW5jdGlvbiBzYihiKXtyZXR1cm4gRWEoYiwhMCkucmVwbGFjZSgvJTI2L2dpLFwiJlwiKS5yZXBsYWNlKC8lM0QvZ2ksXCI9XCIpLnJlcGxhY2UoLyUyQi9naSxcIitcIil9ZnVuY3Rpb24gRWEoYixhKXtyZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGIpLnJlcGxhY2UoLyU0MC9naSxcblwiQFwiKS5yZXBsYWNlKC8lM0EvZ2ksXCI6XCIpLnJlcGxhY2UoLyUyNC9nLFwiJFwiKS5yZXBsYWNlKC8lMkMvZ2ksXCIsXCIpLnJlcGxhY2UoLyUzQi9naSxcIjtcIikucmVwbGFjZSgvJTIwL2csYT9cIiUyMFwiOlwiK1wiKX1mdW5jdGlvbiBJZChiLGEpe3ZhciBjLGQsZT10Yi5sZW5ndGg7Yj1CKGIpO2ZvcihkPTA7ZDxlOysrZClpZihjPXRiW2RdK2EseChjPWIuYXR0cihjKSkpcmV0dXJuIGM7cmV0dXJuIG51bGx9ZnVuY3Rpb24gSmQoYixhKXt2YXIgYyxkLGU9e307cih0YixmdW5jdGlvbihhKXthKz1cImFwcFwiOyFjJiZiLmhhc0F0dHJpYnV0ZSYmYi5oYXNBdHRyaWJ1dGUoYSkmJihjPWIsZD1iLmdldEF0dHJpYnV0ZShhKSl9KTtyKHRiLGZ1bmN0aW9uKGEpe2ErPVwiYXBwXCI7dmFyIGU7IWMmJihlPWIucXVlcnlTZWxlY3RvcihcIltcIithLnJlcGxhY2UoXCI6XCIsXCJcXFxcOlwiKStcIl1cIikpJiYoYz1lLGQ9ZS5nZXRBdHRyaWJ1dGUoYSkpfSk7YyYmKGUuc3RyaWN0RGk9bnVsbCE9PUlkKGMsXCJzdHJpY3QtZGlcIiksXG5hKGMsZD9bZF06W10sZSkpfWZ1bmN0aW9uIHRjKGIsYSxjKXtMKGMpfHwoYz17fSk7Yz13KHtzdHJpY3REaTohMX0sYyk7dmFyIGQ9ZnVuY3Rpb24oKXtiPUIoYik7aWYoYi5pbmplY3RvcigpKXt2YXIgZD1iWzBdPT09Vz9cImRvY3VtZW50XCI6eGEoYik7dGhyb3cgSmEoXCJidHN0cnBkXCIsZC5yZXBsYWNlKC88LyxcIiZsdDtcIikucmVwbGFjZSgvPi8sXCImZ3Q7XCIpKTt9YT1hfHxbXTthLnVuc2hpZnQoW1wiJHByb3ZpZGVcIixmdW5jdGlvbihhKXthLnZhbHVlKFwiJHJvb3RFbGVtZW50XCIsYil9XSk7Yy5kZWJ1Z0luZm9FbmFibGVkJiZhLnB1c2goW1wiJGNvbXBpbGVQcm92aWRlclwiLGZ1bmN0aW9uKGEpe2EuZGVidWdJbmZvRW5hYmxlZCghMCl9XSk7YS51bnNoaWZ0KFwibmdcIik7ZD1jYihhLGMuc3RyaWN0RGkpO2QuaW52b2tlKFtcIiRyb290U2NvcGVcIixcIiRyb290RWxlbWVudFwiLFwiJGNvbXBpbGVcIixcIiRpbmplY3RvclwiLGZ1bmN0aW9uKGEsYixjLGQpe2EuJGFwcGx5KGZ1bmN0aW9uKCl7Yi5kYXRhKFwiJGluamVjdG9yXCIsXG5kKTtjKGIpKGEpfSl9XSk7cmV0dXJuIGR9LGU9L15OR19FTkFCTEVfREVCVUdfSU5GTyEvLGY9L15OR19ERUZFUl9CT09UU1RSQVAhLztSJiZlLnRlc3QoUi5uYW1lKSYmKGMuZGVidWdJbmZvRW5hYmxlZD0hMCxSLm5hbWU9Ui5uYW1lLnJlcGxhY2UoZSxcIlwiKSk7aWYoUiYmIWYudGVzdChSLm5hbWUpKXJldHVybiBkKCk7Ui5uYW1lPVIubmFtZS5yZXBsYWNlKGYsXCJcIik7Y2EucmVzdW1lQm9vdHN0cmFwPWZ1bmN0aW9uKGIpe3IoYixmdW5jdGlvbihiKXthLnB1c2goYil9KTtyZXR1cm4gZCgpfTt6KGNhLnJlc3VtZURlZmVycmVkQm9vdHN0cmFwKSYmY2EucmVzdW1lRGVmZXJyZWRCb290c3RyYXAoKX1mdW5jdGlvbiBLZCgpe1IubmFtZT1cIk5HX0VOQUJMRV9ERUJVR19JTkZPIVwiK1IubmFtZTtSLmxvY2F0aW9uLnJlbG9hZCgpfWZ1bmN0aW9uIExkKGIpe2I9Y2EuZWxlbWVudChiKS5pbmplY3RvcigpO2lmKCFiKXRocm93IEphKFwidGVzdFwiKTtyZXR1cm4gYi5nZXQoXCIkJHRlc3RhYmlsaXR5XCIpfVxuZnVuY3Rpb24gdWMoYixhKXthPWF8fFwiX1wiO3JldHVybiBiLnJlcGxhY2UoTWQsZnVuY3Rpb24oYixkKXtyZXR1cm4oZD9hOlwiXCIpK2IudG9Mb3dlckNhc2UoKX0pfWZ1bmN0aW9uIE5kKCl7dmFyIGI7dmN8fCgodGE9Ui5qUXVlcnkpJiZ0YS5mbi5vbj8oQj10YSx3KHRhLmZuLHtzY29wZTpLYS5zY29wZSxpc29sYXRlU2NvcGU6S2EuaXNvbGF0ZVNjb3BlLGNvbnRyb2xsZXI6S2EuY29udHJvbGxlcixpbmplY3RvcjpLYS5pbmplY3Rvcixpbmhlcml0ZWREYXRhOkthLmluaGVyaXRlZERhdGF9KSxiPXRhLmNsZWFuRGF0YSx0YS5jbGVhbkRhdGE9ZnVuY3Rpb24oYSl7dmFyIGM7aWYoUWIpUWI9ITE7ZWxzZSBmb3IodmFyIGQ9MCxlO251bGwhPShlPWFbZF0pO2QrKykoYz10YS5fZGF0YShlLFwiZXZlbnRzXCIpKSYmYy4kZGVzdHJveSYmdGEoZSkudHJpZ2dlckhhbmRsZXIoXCIkZGVzdHJveVwiKTtiKGEpfSk6Qj1ULGNhLmVsZW1lbnQ9Qix2Yz0hMCl9ZnVuY3Rpb24gUmIoYixhLGMpe2lmKCFiKXRocm93IEphKFwiYXJlcVwiLFxuYXx8XCI/XCIsY3x8XCJyZXF1aXJlZFwiKTtyZXR1cm4gYn1mdW5jdGlvbiBMYShiLGEsYyl7YyYmSChiKSYmKGI9YltiLmxlbmd0aC0xXSk7UmIoeihiKSxhLFwibm90IGEgZnVuY3Rpb24sIGdvdCBcIisoYiYmXCJvYmplY3RcIj09PXR5cGVvZiBiP2IuY29uc3RydWN0b3IubmFtZXx8XCJPYmplY3RcIjp0eXBlb2YgYikpO3JldHVybiBifWZ1bmN0aW9uIE1hKGIsYSl7aWYoXCJoYXNPd25Qcm9wZXJ0eVwiPT09Yil0aHJvdyBKYShcImJhZG5hbWVcIixhKTt9ZnVuY3Rpb24gd2MoYixhLGMpe2lmKCFhKXJldHVybiBiO2E9YS5zcGxpdChcIi5cIik7Zm9yKHZhciBkLGU9YixmPWEubGVuZ3RoLGc9MDtnPGY7ZysrKWQ9YVtnXSxiJiYoYj0oZT1iKVtkXSk7cmV0dXJuIWMmJnooYik/b2MoZSxiKTpifWZ1bmN0aW9uIHViKGIpe3ZhciBhPWJbMF07Yj1iW2IubGVuZ3RoLTFdO3ZhciBjPVthXTtkb3thPWEubmV4dFNpYmxpbmc7aWYoIWEpYnJlYWs7Yy5wdXNoKGEpfXdoaWxlKGEhPT1iKTtyZXR1cm4gQihjKX1mdW5jdGlvbiBqYSgpe3JldHVybiBPYmplY3QuY3JlYXRlKG51bGwpfVxuZnVuY3Rpb24gT2QoYil7ZnVuY3Rpb24gYShhLGIsYyl7cmV0dXJuIGFbYl18fChhW2JdPWMoKSl9dmFyIGM9UyhcIiRpbmplY3RvclwiKSxkPVMoXCJuZ1wiKTtiPWEoYixcImFuZ3VsYXJcIixPYmplY3QpO2IuJCRtaW5FcnI9Yi4kJG1pbkVycnx8UztyZXR1cm4gYShiLFwibW9kdWxlXCIsZnVuY3Rpb24oKXt2YXIgYj17fTtyZXR1cm4gZnVuY3Rpb24oZixnLGgpe2lmKFwiaGFzT3duUHJvcGVydHlcIj09PWYpdGhyb3cgZChcImJhZG5hbWVcIixcIm1vZHVsZVwiKTtnJiZiLmhhc093blByb3BlcnR5KGYpJiYoYltmXT1udWxsKTtyZXR1cm4gYShiLGYsZnVuY3Rpb24oKXtmdW5jdGlvbiBhKGMsZCxlLGYpe2Z8fChmPWIpO3JldHVybiBmdW5jdGlvbigpe2ZbZXx8XCJwdXNoXCJdKFtjLGQsYXJndW1lbnRzXSk7cmV0dXJuIHR9fWlmKCFnKXRocm93IGMoXCJub21vZFwiLGYpO3ZhciBiPVtdLGQ9W10sZT1bXSxxPWEoXCIkaW5qZWN0b3JcIixcImludm9rZVwiLFwicHVzaFwiLGQpLHQ9e19pbnZva2VRdWV1ZTpiLF9jb25maWdCbG9ja3M6ZCxcbl9ydW5CbG9ja3M6ZSxyZXF1aXJlczpnLG5hbWU6Zixwcm92aWRlcjphKFwiJHByb3ZpZGVcIixcInByb3ZpZGVyXCIpLGZhY3Rvcnk6YShcIiRwcm92aWRlXCIsXCJmYWN0b3J5XCIpLHNlcnZpY2U6YShcIiRwcm92aWRlXCIsXCJzZXJ2aWNlXCIpLHZhbHVlOmEoXCIkcHJvdmlkZVwiLFwidmFsdWVcIiksY29uc3RhbnQ6YShcIiRwcm92aWRlXCIsXCJjb25zdGFudFwiLFwidW5zaGlmdFwiKSxhbmltYXRpb246YShcIiRhbmltYXRlUHJvdmlkZXJcIixcInJlZ2lzdGVyXCIpLGZpbHRlcjphKFwiJGZpbHRlclByb3ZpZGVyXCIsXCJyZWdpc3RlclwiKSxjb250cm9sbGVyOmEoXCIkY29udHJvbGxlclByb3ZpZGVyXCIsXCJyZWdpc3RlclwiKSxkaXJlY3RpdmU6YShcIiRjb21waWxlUHJvdmlkZXJcIixcImRpcmVjdGl2ZVwiKSxjb25maWc6cSxydW46ZnVuY3Rpb24oYSl7ZS5wdXNoKGEpO3JldHVybiB0aGlzfX07aCYmcShoKTtyZXR1cm4gdH0pfX0pfWZ1bmN0aW9uIFBkKGIpe3coYix7Ym9vdHN0cmFwOnRjLGNvcHk6RGEsZXh0ZW5kOncsZXF1YWxzOmlhLFxuZWxlbWVudDpCLGZvckVhY2g6cixpbmplY3RvcjpjYixub29wOkEsYmluZDpvYyx0b0pzb246YWIsZnJvbUpzb246cGMsaWRlbnRpdHk6cmEsaXNVbmRlZmluZWQ6RCxpc0RlZmluZWQ6eSxpc1N0cmluZzp4LGlzRnVuY3Rpb246eixpc09iamVjdDpMLGlzTnVtYmVyOlksaXNFbGVtZW50Om5jLGlzQXJyYXk6SCx2ZXJzaW9uOlFkLGlzRGF0ZTpoYSxsb3dlcmNhc2U6Syx1cHBlcmNhc2U6dmIsY2FsbGJhY2tzOntjb3VudGVyOjB9LGdldFRlc3RhYmlsaXR5OkxkLCQkbWluRXJyOlMsJCRjc3A6ZGIscmVsb2FkV2l0aERlYnVnSW5mbzpLZH0pO2ViPU9kKFIpO3RyeXtlYihcIm5nTG9jYWxlXCIpfWNhdGNoKGEpe2ViKFwibmdMb2NhbGVcIixbXSkucHJvdmlkZXIoXCIkbG9jYWxlXCIsUmQpfWViKFwibmdcIixbXCJuZ0xvY2FsZVwiXSxbXCIkcHJvdmlkZVwiLGZ1bmN0aW9uKGEpe2EucHJvdmlkZXIoeyQkc2FuaXRpemVVcmk6U2R9KTthLnByb3ZpZGVyKFwiJGNvbXBpbGVcIix4YykuZGlyZWN0aXZlKHthOlRkLFxuaW5wdXQ6eWMsdGV4dGFyZWE6eWMsZm9ybTpVZCxzY3JpcHQ6VmQsc2VsZWN0OldkLHN0eWxlOlhkLG9wdGlvbjpZZCxuZ0JpbmQ6WmQsbmdCaW5kSHRtbDokZCxuZ0JpbmRUZW1wbGF0ZTphZSxuZ0NsYXNzOmJlLG5nQ2xhc3NFdmVuOmNlLG5nQ2xhc3NPZGQ6ZGUsbmdDbG9hazplZSxuZ0NvbnRyb2xsZXI6ZmUsbmdGb3JtOmdlLG5nSGlkZTpoZSxuZ0lmOmllLG5nSW5jbHVkZTpqZSxuZ0luaXQ6a2UsbmdOb25CaW5kYWJsZTpsZSxuZ1BsdXJhbGl6ZTptZSxuZ1JlcGVhdDpuZSxuZ1Nob3c6b2UsbmdTdHlsZTpwZSxuZ1N3aXRjaDpxZSxuZ1N3aXRjaFdoZW46cmUsbmdTd2l0Y2hEZWZhdWx0OnNlLG5nT3B0aW9uczp0ZSxuZ1RyYW5zY2x1ZGU6dWUsbmdNb2RlbDp2ZSxuZ0xpc3Q6d2UsbmdDaGFuZ2U6eGUscGF0dGVybjp6YyxuZ1BhdHRlcm46emMscmVxdWlyZWQ6QWMsbmdSZXF1aXJlZDpBYyxtaW5sZW5ndGg6QmMsbmdNaW5sZW5ndGg6QmMsbWF4bGVuZ3RoOkNjLG5nTWF4bGVuZ3RoOkNjLFxubmdWYWx1ZTp5ZSxuZ01vZGVsT3B0aW9uczp6ZX0pLmRpcmVjdGl2ZSh7bmdJbmNsdWRlOkFlfSkuZGlyZWN0aXZlKHdiKS5kaXJlY3RpdmUoRGMpO2EucHJvdmlkZXIoeyRhbmNob3JTY3JvbGw6QmUsJGFuaW1hdGU6Q2UsJGJyb3dzZXI6RGUsJGNhY2hlRmFjdG9yeTpFZSwkY29udHJvbGxlcjpGZSwkZG9jdW1lbnQ6R2UsJGV4Y2VwdGlvbkhhbmRsZXI6SGUsJGZpbHRlcjpFYywkaW50ZXJwb2xhdGU6SWUsJGludGVydmFsOkplLCRodHRwOktlLCRodHRwQmFja2VuZDpMZSwkbG9jYXRpb246TWUsJGxvZzpOZSwkcGFyc2U6T2UsJHJvb3RTY29wZTpQZSwkcTpRZSwkJHE6UmUsJHNjZTpTZSwkc2NlRGVsZWdhdGU6VGUsJHNuaWZmZXI6VWUsJHRlbXBsYXRlQ2FjaGU6VmUsJHRlbXBsYXRlUmVxdWVzdDpXZSwkJHRlc3RhYmlsaXR5OlhlLCR0aW1lb3V0OlllLCR3aW5kb3c6WmUsJCRyQUY6JGUsJCRhc3luY0NhbGxiYWNrOmFmLCQkanFMaXRlOmJmfSl9XSl9ZnVuY3Rpb24gZmIoYil7cmV0dXJuIGIucmVwbGFjZShjZixcbmZ1bmN0aW9uKGEsYixkLGUpe3JldHVybiBlP2QudG9VcHBlckNhc2UoKTpkfSkucmVwbGFjZShkZixcIk1veiQxXCIpfWZ1bmN0aW9uIEZjKGIpe2I9Yi5ub2RlVHlwZTtyZXR1cm4gYj09PXFhfHwhYnx8OT09PWJ9ZnVuY3Rpb24gR2MoYixhKXt2YXIgYyxkLGU9YS5jcmVhdGVEb2N1bWVudEZyYWdtZW50KCksZj1bXTtpZihTYi50ZXN0KGIpKXtjPWN8fGUuYXBwZW5kQ2hpbGQoYS5jcmVhdGVFbGVtZW50KFwiZGl2XCIpKTtkPShlZi5leGVjKGIpfHxbXCJcIixcIlwiXSlbMV0udG9Mb3dlckNhc2UoKTtkPWthW2RdfHxrYS5fZGVmYXVsdDtjLmlubmVySFRNTD1kWzFdK2IucmVwbGFjZShmZixcIjwkMT48LyQyPlwiKStkWzJdO2ZvcihkPWRbMF07ZC0tOyljPWMubGFzdENoaWxkO2Y9WmEoZixjLmNoaWxkTm9kZXMpO2M9ZS5maXJzdENoaWxkO2MudGV4dENvbnRlbnQ9XCJcIn1lbHNlIGYucHVzaChhLmNyZWF0ZVRleHROb2RlKGIpKTtlLnRleHRDb250ZW50PVwiXCI7ZS5pbm5lckhUTUw9XCJcIjtyKGYsZnVuY3Rpb24oYSl7ZS5hcHBlbmRDaGlsZChhKX0pO1xucmV0dXJuIGV9ZnVuY3Rpb24gVChiKXtpZihiIGluc3RhbmNlb2YgVClyZXR1cm4gYjt2YXIgYTt4KGIpJiYoYj1OKGIpLGE9ITApO2lmKCEodGhpcyBpbnN0YW5jZW9mIFQpKXtpZihhJiZcIjxcIiE9Yi5jaGFyQXQoMCkpdGhyb3cgVGIoXCJub3NlbFwiKTtyZXR1cm4gbmV3IFQoYil9aWYoYSl7YT1XO3ZhciBjO2I9KGM9Z2YuZXhlYyhiKSk/W2EuY3JlYXRlRWxlbWVudChjWzFdKV06KGM9R2MoYixhKSk/Yy5jaGlsZE5vZGVzOltdfUhjKHRoaXMsYil9ZnVuY3Rpb24gVWIoYil7cmV0dXJuIGIuY2xvbmVOb2RlKCEwKX1mdW5jdGlvbiB4YihiLGEpe2F8fHliKGIpO2lmKGIucXVlcnlTZWxlY3RvckFsbClmb3IodmFyIGM9Yi5xdWVyeVNlbGVjdG9yQWxsKFwiKlwiKSxkPTAsZT1jLmxlbmd0aDtkPGU7ZCsrKXliKGNbZF0pfWZ1bmN0aW9uIEljKGIsYSxjLGQpe2lmKHkoZCkpdGhyb3cgVGIoXCJvZmZhcmdzXCIpO3ZhciBlPShkPXpiKGIpKSYmZC5ldmVudHMsZj1kJiZkLmhhbmRsZTtpZihmKWlmKGEpcihhLnNwbGl0KFwiIFwiKSxcbmZ1bmN0aW9uKGEpe2lmKHkoYykpe3ZhciBkPWVbYV07WWEoZHx8W10sYyk7aWYoZCYmMDxkLmxlbmd0aClyZXR1cm59Yi5yZW1vdmVFdmVudExpc3RlbmVyKGEsZiwhMSk7ZGVsZXRlIGVbYV19KTtlbHNlIGZvcihhIGluIGUpXCIkZGVzdHJveVwiIT09YSYmYi5yZW1vdmVFdmVudExpc3RlbmVyKGEsZiwhMSksZGVsZXRlIGVbYV19ZnVuY3Rpb24geWIoYixhKXt2YXIgYz1iLm5nMzM5LGQ9YyYmQWJbY107ZCYmKGE/ZGVsZXRlIGQuZGF0YVthXTooZC5oYW5kbGUmJihkLmV2ZW50cy4kZGVzdHJveSYmZC5oYW5kbGUoe30sXCIkZGVzdHJveVwiKSxJYyhiKSksZGVsZXRlIEFiW2NdLGIubmczMzk9dSkpfWZ1bmN0aW9uIHpiKGIsYSl7dmFyIGM9Yi5uZzMzOSxjPWMmJkFiW2NdO2EmJiFjJiYoYi5uZzMzOT1jPSsraGYsYz1BYltjXT17ZXZlbnRzOnt9LGRhdGE6e30saGFuZGxlOnV9KTtyZXR1cm4gY31mdW5jdGlvbiBWYihiLGEsYyl7aWYoRmMoYikpe3ZhciBkPXkoYyksZT0hZCYmYSYmIUwoYSksXG5mPSFhO2I9KGI9emIoYiwhZSkpJiZiLmRhdGE7aWYoZCliW2FdPWM7ZWxzZXtpZihmKXJldHVybiBiO2lmKGUpcmV0dXJuIGImJmJbYV07dyhiLGEpfX19ZnVuY3Rpb24gQmIoYixhKXtyZXR1cm4gYi5nZXRBdHRyaWJ1dGU/LTE8KFwiIFwiKyhiLmdldEF0dHJpYnV0ZShcImNsYXNzXCIpfHxcIlwiKStcIiBcIikucmVwbGFjZSgvW1xcblxcdF0vZyxcIiBcIikuaW5kZXhPZihcIiBcIithK1wiIFwiKTohMX1mdW5jdGlvbiBDYihiLGEpe2EmJmIuc2V0QXR0cmlidXRlJiZyKGEuc3BsaXQoXCIgXCIpLGZ1bmN0aW9uKGEpe2Iuc2V0QXR0cmlidXRlKFwiY2xhc3NcIixOKChcIiBcIisoYi5nZXRBdHRyaWJ1dGUoXCJjbGFzc1wiKXx8XCJcIikrXCIgXCIpLnJlcGxhY2UoL1tcXG5cXHRdL2csXCIgXCIpLnJlcGxhY2UoXCIgXCIrTihhKStcIiBcIixcIiBcIikpKX0pfWZ1bmN0aW9uIERiKGIsYSl7aWYoYSYmYi5zZXRBdHRyaWJ1dGUpe3ZhciBjPShcIiBcIisoYi5nZXRBdHRyaWJ1dGUoXCJjbGFzc1wiKXx8XCJcIikrXCIgXCIpLnJlcGxhY2UoL1tcXG5cXHRdL2csXCIgXCIpO1xucihhLnNwbGl0KFwiIFwiKSxmdW5jdGlvbihhKXthPU4oYSk7LTE9PT1jLmluZGV4T2YoXCIgXCIrYStcIiBcIikmJihjKz1hK1wiIFwiKX0pO2Iuc2V0QXR0cmlidXRlKFwiY2xhc3NcIixOKGMpKX19ZnVuY3Rpb24gSGMoYixhKXtpZihhKWlmKGEubm9kZVR5cGUpYltiLmxlbmd0aCsrXT1hO2Vsc2V7dmFyIGM9YS5sZW5ndGg7aWYoXCJudW1iZXJcIj09PXR5cGVvZiBjJiZhLndpbmRvdyE9PWEpe2lmKGMpZm9yKHZhciBkPTA7ZDxjO2QrKyliW2IubGVuZ3RoKytdPWFbZF19ZWxzZSBiW2IubGVuZ3RoKytdPWF9fWZ1bmN0aW9uIEpjKGIsYSl7cmV0dXJuIEViKGIsXCIkXCIrKGF8fFwibmdDb250cm9sbGVyXCIpK1wiQ29udHJvbGxlclwiKX1mdW5jdGlvbiBFYihiLGEsYyl7OT09Yi5ub2RlVHlwZSYmKGI9Yi5kb2N1bWVudEVsZW1lbnQpO2ZvcihhPUgoYSk/YTpbYV07Yjspe2Zvcih2YXIgZD0wLGU9YS5sZW5ndGg7ZDxlO2QrKylpZigoYz1CLmRhdGEoYixhW2RdKSkhPT11KXJldHVybiBjO2I9Yi5wYXJlbnROb2RlfHxcbjExPT09Yi5ub2RlVHlwZSYmYi5ob3N0fX1mdW5jdGlvbiBLYyhiKXtmb3IoeGIoYiwhMCk7Yi5maXJzdENoaWxkOyliLnJlbW92ZUNoaWxkKGIuZmlyc3RDaGlsZCl9ZnVuY3Rpb24gTGMoYixhKXthfHx4YihiKTt2YXIgYz1iLnBhcmVudE5vZGU7YyYmYy5yZW1vdmVDaGlsZChiKX1mdW5jdGlvbiBqZihiLGEpe2E9YXx8UjtpZihcImNvbXBsZXRlXCI9PT1hLmRvY3VtZW50LnJlYWR5U3RhdGUpYS5zZXRUaW1lb3V0KGIpO2Vsc2UgQihhKS5vbihcImxvYWRcIixiKX1mdW5jdGlvbiBNYyhiLGEpe3ZhciBjPUZiW2EudG9Mb3dlckNhc2UoKV07cmV0dXJuIGMmJk5jW3dhKGIpXSYmY31mdW5jdGlvbiBrZihiLGEpe3ZhciBjPWIubm9kZU5hbWU7cmV0dXJuKFwiSU5QVVRcIj09PWN8fFwiVEVYVEFSRUFcIj09PWMpJiZPY1thXX1mdW5jdGlvbiBsZihiLGEpe3ZhciBjPWZ1bmN0aW9uKGMsZSl7Yy5pc0RlZmF1bHRQcmV2ZW50ZWQ9ZnVuY3Rpb24oKXtyZXR1cm4gYy5kZWZhdWx0UHJldmVudGVkfTt2YXIgZj1cbmFbZXx8Yy50eXBlXSxnPWY/Zi5sZW5ndGg6MDtpZihnKXtpZihEKGMuaW1tZWRpYXRlUHJvcGFnYXRpb25TdG9wcGVkKSl7dmFyIGg9Yy5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb247Yy5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb249ZnVuY3Rpb24oKXtjLmltbWVkaWF0ZVByb3BhZ2F0aW9uU3RvcHBlZD0hMDtjLnN0b3BQcm9wYWdhdGlvbiYmYy5zdG9wUHJvcGFnYXRpb24oKTtoJiZoLmNhbGwoYyl9fWMuaXNJbW1lZGlhdGVQcm9wYWdhdGlvblN0b3BwZWQ9ZnVuY3Rpb24oKXtyZXR1cm4hMD09PWMuaW1tZWRpYXRlUHJvcGFnYXRpb25TdG9wcGVkfTsxPGcmJihmPXNhKGYpKTtmb3IodmFyIGw9MDtsPGc7bCsrKWMuaXNJbW1lZGlhdGVQcm9wYWdhdGlvblN0b3BwZWQoKXx8ZltsXS5jYWxsKGIsYyl9fTtjLmVsZW09YjtyZXR1cm4gY31mdW5jdGlvbiBiZigpe3RoaXMuJGdldD1mdW5jdGlvbigpe3JldHVybiB3KFQse2hhc0NsYXNzOmZ1bmN0aW9uKGIsYSl7Yi5hdHRyJiYoYj1iWzBdKTtcbnJldHVybiBCYihiLGEpfSxhZGRDbGFzczpmdW5jdGlvbihiLGEpe2IuYXR0ciYmKGI9YlswXSk7cmV0dXJuIERiKGIsYSl9LHJlbW92ZUNsYXNzOmZ1bmN0aW9uKGIsYSl7Yi5hdHRyJiYoYj1iWzBdKTtyZXR1cm4gQ2IoYixhKX19KX19ZnVuY3Rpb24gTmEoYixhKXt2YXIgYz1iJiZiLiQkaGFzaEtleTtpZihjKXJldHVyblwiZnVuY3Rpb25cIj09PXR5cGVvZiBjJiYoYz1iLiQkaGFzaEtleSgpKSxjO2M9dHlwZW9mIGI7cmV0dXJuIGM9XCJmdW5jdGlvblwiPT1jfHxcIm9iamVjdFwiPT1jJiZudWxsIT09Yj9iLiQkaGFzaEtleT1jK1wiOlwiKyhhfHxGZCkoKTpjK1wiOlwiK2J9ZnVuY3Rpb24gZ2IoYixhKXtpZihhKXt2YXIgYz0wO3RoaXMubmV4dFVpZD1mdW5jdGlvbigpe3JldHVybisrY319cihiLHRoaXMucHV0LHRoaXMpfWZ1bmN0aW9uIG1mKGIpe3JldHVybihiPWIudG9TdHJpbmcoKS5yZXBsYWNlKFBjLFwiXCIpLm1hdGNoKFFjKSk/XCJmdW5jdGlvbihcIisoYlsxXXx8XCJcIikucmVwbGFjZSgvW1xcc1xcclxcbl0rLyxcblwiIFwiKStcIilcIjpcImZuXCJ9ZnVuY3Rpb24gY2IoYixhKXtmdW5jdGlvbiBjKGEpe3JldHVybiBmdW5jdGlvbihiLGMpe2lmKEwoYikpcihiLGxjKGEpKTtlbHNlIHJldHVybiBhKGIsYyl9fWZ1bmN0aW9uIGQoYSxiKXtNYShhLFwic2VydmljZVwiKTtpZih6KGIpfHxIKGIpKWI9cS5pbnN0YW50aWF0ZShiKTtpZighYi4kZ2V0KXRocm93IEZhKFwicGdldFwiLGEpO3JldHVybiBwW2ErXCJQcm92aWRlclwiXT1ifWZ1bmN0aW9uIGUoYSxiKXtyZXR1cm4gZnVuY3Rpb24oKXt2YXIgYz1zLmludm9rZShiLHRoaXMpO2lmKEQoYykpdGhyb3cgRmEoXCJ1bmRlZlwiLGEpO3JldHVybiBjfX1mdW5jdGlvbiBmKGEsYixjKXtyZXR1cm4gZChhLHskZ2V0OiExIT09Yz9lKGEsYik6Yn0pfWZ1bmN0aW9uIGcoYSl7dmFyIGI9W10sYztyKGEsZnVuY3Rpb24oYSl7ZnVuY3Rpb24gZChhKXt2YXIgYixjO2I9MDtmb3IoYz1hLmxlbmd0aDtiPGM7YisrKXt2YXIgZT1hW2JdLGY9cS5nZXQoZVswXSk7ZltlWzFdXS5hcHBseShmLFxuZVsyXSl9fWlmKCFuLmdldChhKSl7bi5wdXQoYSwhMCk7dHJ5e3goYSk/KGM9ZWIoYSksYj1iLmNvbmNhdChnKGMucmVxdWlyZXMpKS5jb25jYXQoYy5fcnVuQmxvY2tzKSxkKGMuX2ludm9rZVF1ZXVlKSxkKGMuX2NvbmZpZ0Jsb2NrcykpOnooYSk/Yi5wdXNoKHEuaW52b2tlKGEpKTpIKGEpP2IucHVzaChxLmludm9rZShhKSk6TGEoYSxcIm1vZHVsZVwiKX1jYXRjaChlKXt0aHJvdyBIKGEpJiYoYT1hW2EubGVuZ3RoLTFdKSxlLm1lc3NhZ2UmJmUuc3RhY2smJi0xPT1lLnN0YWNrLmluZGV4T2YoZS5tZXNzYWdlKSYmKGU9ZS5tZXNzYWdlK1wiXFxuXCIrZS5zdGFjayksRmEoXCJtb2R1bGVyclwiLGEsZS5zdGFja3x8ZS5tZXNzYWdlfHxlKTt9fX0pO3JldHVybiBifWZ1bmN0aW9uIGgoYixjKXtmdW5jdGlvbiBkKGEsZSl7aWYoYi5oYXNPd25Qcm9wZXJ0eShhKSl7aWYoYlthXT09PWwpdGhyb3cgRmEoXCJjZGVwXCIsYStcIiA8LSBcIitrLmpvaW4oXCIgPC0gXCIpKTtyZXR1cm4gYlthXX10cnl7cmV0dXJuIGsudW5zaGlmdChhKSxcbmJbYV09bCxiW2FdPWMoYSxlKX1jYXRjaChmKXt0aHJvdyBiW2FdPT09bCYmZGVsZXRlIGJbYV0sZjt9ZmluYWxseXtrLnNoaWZ0KCl9fWZ1bmN0aW9uIGUoYixjLGYsZyl7XCJzdHJpbmdcIj09PXR5cGVvZiBmJiYoZz1mLGY9bnVsbCk7dmFyIGs9W10sbD1jYi4kJGFubm90YXRlKGIsYSxnKSxoLHEscDtxPTA7Zm9yKGg9bC5sZW5ndGg7cTxoO3ErKyl7cD1sW3FdO2lmKFwic3RyaW5nXCIhPT10eXBlb2YgcCl0aHJvdyBGYShcIml0a25cIixwKTtrLnB1c2goZiYmZi5oYXNPd25Qcm9wZXJ0eShwKT9mW3BdOmQocCxnKSl9SChiKSYmKGI9YltoXSk7cmV0dXJuIGIuYXBwbHkoYyxrKX1yZXR1cm57aW52b2tlOmUsaW5zdGFudGlhdGU6ZnVuY3Rpb24oYSxiLGMpe3ZhciBkPU9iamVjdC5jcmVhdGUoKEgoYSk/YVthLmxlbmd0aC0xXTphKS5wcm90b3R5cGV8fG51bGwpO2E9ZShhLGQsYixjKTtyZXR1cm4gTChhKXx8eihhKT9hOmR9LGdldDpkLGFubm90YXRlOmNiLiQkYW5ub3RhdGUsaGFzOmZ1bmN0aW9uKGEpe3JldHVybiBwLmhhc093blByb3BlcnR5KGErXG5cIlByb3ZpZGVyXCIpfHxiLmhhc093blByb3BlcnR5KGEpfX19YT0hMD09PWE7dmFyIGw9e30saz1bXSxuPW5ldyBnYihbXSwhMCkscD17JHByb3ZpZGU6e3Byb3ZpZGVyOmMoZCksZmFjdG9yeTpjKGYpLHNlcnZpY2U6YyhmdW5jdGlvbihhLGIpe3JldHVybiBmKGEsW1wiJGluamVjdG9yXCIsZnVuY3Rpb24oYSl7cmV0dXJuIGEuaW5zdGFudGlhdGUoYil9XSl9KSx2YWx1ZTpjKGZ1bmN0aW9uKGEsYil7cmV0dXJuIGYoYSxlYShiKSwhMSl9KSxjb25zdGFudDpjKGZ1bmN0aW9uKGEsYil7TWEoYSxcImNvbnN0YW50XCIpO3BbYV09Yjt0W2FdPWJ9KSxkZWNvcmF0b3I6ZnVuY3Rpb24oYSxiKXt2YXIgYz1xLmdldChhK1wiUHJvdmlkZXJcIiksZD1jLiRnZXQ7Yy4kZ2V0PWZ1bmN0aW9uKCl7dmFyIGE9cy5pbnZva2UoZCxjKTtyZXR1cm4gcy5pbnZva2UoYixudWxsLHskZGVsZWdhdGU6YX0pfX19fSxxPXAuJGluamVjdG9yPWgocCxmdW5jdGlvbihhLGIpe2NhLmlzU3RyaW5nKGIpJiZrLnB1c2goYik7XG50aHJvdyBGYShcInVucHJcIixrLmpvaW4oXCIgPC0gXCIpKTt9KSx0PXt9LHM9dC4kaW5qZWN0b3I9aCh0LGZ1bmN0aW9uKGEsYil7dmFyIGM9cS5nZXQoYStcIlByb3ZpZGVyXCIsYik7cmV0dXJuIHMuaW52b2tlKGMuJGdldCxjLHUsYSl9KTtyKGcoYiksZnVuY3Rpb24oYSl7cy5pbnZva2UoYXx8QSl9KTtyZXR1cm4gc31mdW5jdGlvbiBCZSgpe3ZhciBiPSEwO3RoaXMuZGlzYWJsZUF1dG9TY3JvbGxpbmc9ZnVuY3Rpb24oKXtiPSExfTt0aGlzLiRnZXQ9W1wiJHdpbmRvd1wiLFwiJGxvY2F0aW9uXCIsXCIkcm9vdFNjb3BlXCIsZnVuY3Rpb24oYSxjLGQpe2Z1bmN0aW9uIGUoYSl7dmFyIGI9bnVsbDtBcnJheS5wcm90b3R5cGUuc29tZS5jYWxsKGEsZnVuY3Rpb24oYSl7aWYoXCJhXCI9PT13YShhKSlyZXR1cm4gYj1hLCEwfSk7cmV0dXJuIGJ9ZnVuY3Rpb24gZihiKXtpZihiKXtiLnNjcm9sbEludG9WaWV3KCk7dmFyIGM7Yz1nLnlPZmZzZXQ7eihjKT9jPWMoKTpuYyhjKT8oYz1jWzBdLGM9XCJmaXhlZFwiIT09XG5hLmdldENvbXB1dGVkU3R5bGUoYykucG9zaXRpb24/MDpjLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmJvdHRvbSk6WShjKXx8KGM9MCk7YyYmKGI9Yi5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS50b3AsYS5zY3JvbGxCeSgwLGItYykpfWVsc2UgYS5zY3JvbGxUbygwLDApfWZ1bmN0aW9uIGcoKXt2YXIgYT1jLmhhc2goKSxiO2E/KGI9aC5nZXRFbGVtZW50QnlJZChhKSk/ZihiKTooYj1lKGguZ2V0RWxlbWVudHNCeU5hbWUoYSkpKT9mKGIpOlwidG9wXCI9PT1hJiZmKG51bGwpOmYobnVsbCl9dmFyIGg9YS5kb2N1bWVudDtiJiZkLiR3YXRjaChmdW5jdGlvbigpe3JldHVybiBjLmhhc2goKX0sZnVuY3Rpb24oYSxiKXthPT09YiYmXCJcIj09PWF8fGpmKGZ1bmN0aW9uKCl7ZC4kZXZhbEFzeW5jKGcpfSl9KTtyZXR1cm4gZ31dfWZ1bmN0aW9uIGFmKCl7dGhpcy4kZ2V0PVtcIiQkckFGXCIsXCIkdGltZW91dFwiLGZ1bmN0aW9uKGIsYSl7cmV0dXJuIGIuc3VwcG9ydGVkP2Z1bmN0aW9uKGEpe3JldHVybiBiKGEpfTpcbmZ1bmN0aW9uKGIpe3JldHVybiBhKGIsMCwhMSl9fV19ZnVuY3Rpb24gbmYoYixhLGMsZCl7ZnVuY3Rpb24gZShhKXt0cnl7YS5hcHBseShudWxsLCRhLmNhbGwoYXJndW1lbnRzLDEpKX1maW5hbGx5e2lmKG0tLSwwPT09bSlmb3IoO0MubGVuZ3RoOyl0cnl7Qy5wb3AoKSgpfWNhdGNoKGIpe2MuZXJyb3IoYil9fX1mdW5jdGlvbiBmKGEsYil7KGZ1bmN0aW9uIGRhKCl7cigkLGZ1bmN0aW9uKGEpe2EoKX0pO0k9YihkYSxhKX0pKCl9ZnVuY3Rpb24gZygpe2goKTtsKCl9ZnVuY3Rpb24gaCgpe2E6e3RyeXtNPXQuc3RhdGU7YnJlYWsgYX1jYXRjaChhKXt9TT12b2lkIDB9TT1EKE0pP251bGw6TTtpYShNLFApJiYoTT1QKTtQPU19ZnVuY3Rpb24gbCgpe2lmKEchPT1uLnVybCgpfHxFIT09TSlHPW4udXJsKCksRT1NLHIoWCxmdW5jdGlvbihhKXthKG4udXJsKCksTSl9KX1mdW5jdGlvbiBrKGEpe3RyeXtyZXR1cm4gZGVjb2RlVVJJQ29tcG9uZW50KGEpfWNhdGNoKGIpe3JldHVybiBhfX1cbnZhciBuPXRoaXMscD1hWzBdLHE9Yi5sb2NhdGlvbix0PWIuaGlzdG9yeSxzPWIuc2V0VGltZW91dCxGPWIuY2xlYXJUaW1lb3V0LHY9e307bi5pc01vY2s9ITE7dmFyIG09MCxDPVtdO24uJCRjb21wbGV0ZU91dHN0YW5kaW5nUmVxdWVzdD1lO24uJCRpbmNPdXRzdGFuZGluZ1JlcXVlc3RDb3VudD1mdW5jdGlvbigpe20rK307bi5ub3RpZnlXaGVuTm9PdXRzdGFuZGluZ1JlcXVlc3RzPWZ1bmN0aW9uKGEpe3IoJCxmdW5jdGlvbihhKXthKCl9KTswPT09bT9hKCk6Qy5wdXNoKGEpfTt2YXIgJD1bXSxJO24uYWRkUG9sbEZuPWZ1bmN0aW9uKGEpe0QoSSkmJmYoMTAwLHMpOyQucHVzaChhKTtyZXR1cm4gYX07dmFyIE0sRSxHPXEuaHJlZixPPWEuZmluZChcImJhc2VcIiksUT1udWxsO2goKTtFPU07bi51cmw9ZnVuY3Rpb24oYSxjLGUpe0QoZSkmJihlPW51bGwpO3EhPT1iLmxvY2F0aW9uJiYocT1iLmxvY2F0aW9uKTt0IT09Yi5oaXN0b3J5JiYodD1iLmhpc3RvcnkpO2lmKGEpe3ZhciBmPVxuRT09PWU7aWYoRz09PWEmJighZC5oaXN0b3J5fHxmKSlyZXR1cm4gbjt2YXIgZz1HJiZHYShHKT09PUdhKGEpO0c9YTtFPWU7aWYoIWQuaGlzdG9yeXx8ZyYmZil7aWYoIWd8fFEpUT1hO2M/cS5yZXBsYWNlKGEpOmc/KGM9cSxlPWEuaW5kZXhPZihcIiNcIiksYT0tMT09PWU/XCJcIjphLnN1YnN0cihlKSxjLmhhc2g9YSk6cS5ocmVmPWF9ZWxzZSB0W2M/XCJyZXBsYWNlU3RhdGVcIjpcInB1c2hTdGF0ZVwiXShlLFwiXCIsYSksaCgpLEU9TTtyZXR1cm4gbn1yZXR1cm4gUXx8cS5ocmVmLnJlcGxhY2UoLyUyNy9nLFwiJ1wiKX07bi5zdGF0ZT1mdW5jdGlvbigpe3JldHVybiBNfTt2YXIgWD1bXSxiYT0hMSxQPW51bGw7bi5vblVybENoYW5nZT1mdW5jdGlvbihhKXtpZighYmEpe2lmKGQuaGlzdG9yeSlCKGIpLm9uKFwicG9wc3RhdGVcIixnKTtCKGIpLm9uKFwiaGFzaGNoYW5nZVwiLGcpO2JhPSEwfVgucHVzaChhKTtyZXR1cm4gYX07bi4kJGNoZWNrVXJsQ2hhbmdlPWw7bi5iYXNlSHJlZj1mdW5jdGlvbigpe3ZhciBhPVxuTy5hdHRyKFwiaHJlZlwiKTtyZXR1cm4gYT9hLnJlcGxhY2UoL14oaHR0cHM/XFw6KT9cXC9cXC9bXlxcL10qLyxcIlwiKTpcIlwifTt2YXIgZmE9e30seT1cIlwiLGxhPW4uYmFzZUhyZWYoKTtuLmNvb2tpZXM9ZnVuY3Rpb24oYSxiKXt2YXIgZCxlLGYsZztpZihhKWI9PT11P3AuY29va2llPWVuY29kZVVSSUNvbXBvbmVudChhKStcIj07cGF0aD1cIitsYStcIjtleHBpcmVzPVRodSwgMDEgSmFuIDE5NzAgMDA6MDA6MDAgR01UXCI6eChiKSYmKGQ9KHAuY29va2llPWVuY29kZVVSSUNvbXBvbmVudChhKStcIj1cIitlbmNvZGVVUklDb21wb25lbnQoYikrXCI7cGF0aD1cIitsYSkubGVuZ3RoKzEsNDA5NjxkJiZjLndhcm4oXCJDb29raWUgJ1wiK2ErXCInIHBvc3NpYmx5IG5vdCBzZXQgb3Igb3ZlcmZsb3dlZCBiZWNhdXNlIGl0IHdhcyB0b28gbGFyZ2UgKFwiK2QrXCIgPiA0MDk2IGJ5dGVzKSFcIikpO2Vsc2V7aWYocC5jb29raWUhPT15KWZvcih5PXAuY29va2llLGQ9eS5zcGxpdChcIjsgXCIpLGZhPXt9LGY9MDtmPGQubGVuZ3RoO2YrKyllPVxuZFtmXSxnPWUuaW5kZXhPZihcIj1cIiksMDxnJiYoYT1rKGUuc3Vic3RyaW5nKDAsZykpLGZhW2FdPT09dSYmKGZhW2FdPWsoZS5zdWJzdHJpbmcoZysxKSkpKTtyZXR1cm4gZmF9fTtuLmRlZmVyPWZ1bmN0aW9uKGEsYil7dmFyIGM7bSsrO2M9cyhmdW5jdGlvbigpe2RlbGV0ZSB2W2NdO2UoYSl9LGJ8fDApO3ZbY109ITA7cmV0dXJuIGN9O24uZGVmZXIuY2FuY2VsPWZ1bmN0aW9uKGEpe3JldHVybiB2W2FdPyhkZWxldGUgdlthXSxGKGEpLGUoQSksITApOiExfX1mdW5jdGlvbiBEZSgpe3RoaXMuJGdldD1bXCIkd2luZG93XCIsXCIkbG9nXCIsXCIkc25pZmZlclwiLFwiJGRvY3VtZW50XCIsZnVuY3Rpb24oYixhLGMsZCl7cmV0dXJuIG5ldyBuZihiLGQsYSxjKX1dfWZ1bmN0aW9uIEVlKCl7dGhpcy4kZ2V0PWZ1bmN0aW9uKCl7ZnVuY3Rpb24gYihiLGQpe2Z1bmN0aW9uIGUoYSl7YSE9cCYmKHE/cT09YSYmKHE9YS5uKTpxPWEsZihhLm4sYS5wKSxmKGEscCkscD1hLHAubj1udWxsKX1mdW5jdGlvbiBmKGEsXG5iKXthIT1iJiYoYSYmKGEucD1iKSxiJiYoYi5uPWEpKX1pZihiIGluIGEpdGhyb3cgUyhcIiRjYWNoZUZhY3RvcnlcIikoXCJpaWRcIixiKTt2YXIgZz0wLGg9dyh7fSxkLHtpZDpifSksbD17fSxrPWQmJmQuY2FwYWNpdHl8fE51bWJlci5NQVhfVkFMVUUsbj17fSxwPW51bGwscT1udWxsO3JldHVybiBhW2JdPXtwdXQ6ZnVuY3Rpb24oYSxiKXtpZihrPE51bWJlci5NQVhfVkFMVUUpe3ZhciBjPW5bYV18fChuW2FdPXtrZXk6YX0pO2UoYyl9aWYoIUQoYikpcmV0dXJuIGEgaW4gbHx8ZysrLGxbYV09YixnPmsmJnRoaXMucmVtb3ZlKHEua2V5KSxifSxnZXQ6ZnVuY3Rpb24oYSl7aWYoazxOdW1iZXIuTUFYX1ZBTFVFKXt2YXIgYj1uW2FdO2lmKCFiKXJldHVybjtlKGIpfXJldHVybiBsW2FdfSxyZW1vdmU6ZnVuY3Rpb24oYSl7aWYoazxOdW1iZXIuTUFYX1ZBTFVFKXt2YXIgYj1uW2FdO2lmKCFiKXJldHVybjtiPT1wJiYocD1iLnApO2I9PXEmJihxPWIubik7ZihiLm4sYi5wKTtkZWxldGUgblthXX1kZWxldGUgbFthXTtcbmctLX0scmVtb3ZlQWxsOmZ1bmN0aW9uKCl7bD17fTtnPTA7bj17fTtwPXE9bnVsbH0sZGVzdHJveTpmdW5jdGlvbigpe249aD1sPW51bGw7ZGVsZXRlIGFbYl19LGluZm86ZnVuY3Rpb24oKXtyZXR1cm4gdyh7fSxoLHtzaXplOmd9KX19fXZhciBhPXt9O2IuaW5mbz1mdW5jdGlvbigpe3ZhciBiPXt9O3IoYSxmdW5jdGlvbihhLGUpe2JbZV09YS5pbmZvKCl9KTtyZXR1cm4gYn07Yi5nZXQ9ZnVuY3Rpb24oYil7cmV0dXJuIGFbYl19O3JldHVybiBifX1mdW5jdGlvbiBWZSgpe3RoaXMuJGdldD1bXCIkY2FjaGVGYWN0b3J5XCIsZnVuY3Rpb24oYil7cmV0dXJuIGIoXCJ0ZW1wbGF0ZXNcIil9XX1mdW5jdGlvbiB4YyhiLGEpe2Z1bmN0aW9uIGMoYSxiKXt2YXIgYz0vXlxccyooW0AmXXw9KFxcKj8pKShcXD8/KVxccyooXFx3KilcXHMqJC8sZD17fTtyKGEsZnVuY3Rpb24oYSxlKXt2YXIgZj1hLm1hdGNoKGMpO2lmKCFmKXRocm93IG1hKFwiaXNjcFwiLGIsZSxhKTtkW2VdPXttb2RlOmZbMV1bMF0sY29sbGVjdGlvbjpcIipcIj09PVxuZlsyXSxvcHRpb25hbDpcIj9cIj09PWZbM10sYXR0ck5hbWU6Zls0XXx8ZX19KTtyZXR1cm4gZH12YXIgZD17fSxlPS9eXFxzKmRpcmVjdGl2ZVxcOlxccyooW1xcd1xcLV0rKVxccysoLiopJC8sZj0vKChbXFx3XFwtXSspKD86XFw6KFteO10rKSk/Oz8pLyxnPUdkKFwibmdTcmMsbmdTcmNzZXQsc3JjLHNyY3NldFwiKSxoPS9eKD86KFxcXlxcXj8pPyhcXD8pPyhcXF5cXF4/KT8pPy8sbD0vXihvblthLXpdK3xmb3JtYWN0aW9uKSQvO3RoaXMuZGlyZWN0aXZlPWZ1bmN0aW9uIHAoYSxlKXtNYShhLFwiZGlyZWN0aXZlXCIpO3goYSk/KFJiKGUsXCJkaXJlY3RpdmVGYWN0b3J5XCIpLGQuaGFzT3duUHJvcGVydHkoYSl8fChkW2FdPVtdLGIuZmFjdG9yeShhK1wiRGlyZWN0aXZlXCIsW1wiJGluamVjdG9yXCIsXCIkZXhjZXB0aW9uSGFuZGxlclwiLGZ1bmN0aW9uKGIsZSl7dmFyIGY9W107cihkW2FdLGZ1bmN0aW9uKGQsZyl7dHJ5e3ZhciBoPWIuaW52b2tlKGQpO3ooaCk/aD17Y29tcGlsZTplYShoKX06IWguY29tcGlsZSYmaC5saW5rJiZcbihoLmNvbXBpbGU9ZWEoaC5saW5rKSk7aC5wcmlvcml0eT1oLnByaW9yaXR5fHwwO2guaW5kZXg9ZztoLm5hbWU9aC5uYW1lfHxhO2gucmVxdWlyZT1oLnJlcXVpcmV8fGguY29udHJvbGxlciYmaC5uYW1lO2gucmVzdHJpY3Q9aC5yZXN0cmljdHx8XCJFQVwiO0woaC5zY29wZSkmJihoLiQkaXNvbGF0ZUJpbmRpbmdzPWMoaC5zY29wZSxoLm5hbWUpKTtmLnB1c2goaCl9Y2F0Y2gobCl7ZShsKX19KTtyZXR1cm4gZn1dKSksZFthXS5wdXNoKGUpKTpyKGEsbGMocCkpO3JldHVybiB0aGlzfTt0aGlzLmFIcmVmU2FuaXRpemF0aW9uV2hpdGVsaXN0PWZ1bmN0aW9uKGIpe3JldHVybiB5KGIpPyhhLmFIcmVmU2FuaXRpemF0aW9uV2hpdGVsaXN0KGIpLHRoaXMpOmEuYUhyZWZTYW5pdGl6YXRpb25XaGl0ZWxpc3QoKX07dGhpcy5pbWdTcmNTYW5pdGl6YXRpb25XaGl0ZWxpc3Q9ZnVuY3Rpb24oYil7cmV0dXJuIHkoYik/KGEuaW1nU3JjU2FuaXRpemF0aW9uV2hpdGVsaXN0KGIpLHRoaXMpOmEuaW1nU3JjU2FuaXRpemF0aW9uV2hpdGVsaXN0KCl9O1xudmFyIGs9ITA7dGhpcy5kZWJ1Z0luZm9FbmFibGVkPWZ1bmN0aW9uKGEpe3JldHVybiB5KGEpPyhrPWEsdGhpcyk6a307dGhpcy4kZ2V0PVtcIiRpbmplY3RvclwiLFwiJGludGVycG9sYXRlXCIsXCIkZXhjZXB0aW9uSGFuZGxlclwiLFwiJHRlbXBsYXRlUmVxdWVzdFwiLFwiJHBhcnNlXCIsXCIkY29udHJvbGxlclwiLFwiJHJvb3RTY29wZVwiLFwiJGRvY3VtZW50XCIsXCIkc2NlXCIsXCIkYW5pbWF0ZVwiLFwiJCRzYW5pdGl6ZVVyaVwiLGZ1bmN0aW9uKGEsYixjLHMsRix2LG0sQywkLEksTSl7ZnVuY3Rpb24gRShhLGIpe3RyeXthLmFkZENsYXNzKGIpfWNhdGNoKGMpe319ZnVuY3Rpb24gRyhhLGIsYyxkLGUpe2EgaW5zdGFuY2VvZiBCfHwoYT1CKGEpKTtyKGEsZnVuY3Rpb24oYixjKXtiLm5vZGVUeXBlPT1iYiYmYi5ub2RlVmFsdWUubWF0Y2goL1xcUysvKSYmKGFbY109QihiKS53cmFwKFwiPHNwYW4+PC9zcGFuPlwiKS5wYXJlbnQoKVswXSl9KTt2YXIgZj1PKGEsYixhLGMsZCxlKTtHLiQkYWRkU2NvcGVDbGFzcyhhKTtcbnZhciBnPW51bGw7cmV0dXJuIGZ1bmN0aW9uKGIsYyxkKXtSYihiLFwic2NvcGVcIik7ZD1kfHx7fTt2YXIgZT1kLnBhcmVudEJvdW5kVHJhbnNjbHVkZUZuLGg9ZC50cmFuc2NsdWRlQ29udHJvbGxlcnM7ZD1kLmZ1dHVyZVBhcmVudEVsZW1lbnQ7ZSYmZS4kJGJvdW5kVHJhbnNjbHVkZSYmKGU9ZS4kJGJvdW5kVHJhbnNjbHVkZSk7Z3x8KGc9KGQ9ZCYmZFswXSk/XCJmb3JlaWdub2JqZWN0XCIhPT13YShkKSYmZC50b1N0cmluZygpLm1hdGNoKC9TVkcvKT9cInN2Z1wiOlwiaHRtbFwiOlwiaHRtbFwiKTtkPVwiaHRtbFwiIT09Zz9CKFhiKGcsQihcIjxkaXY+XCIpLmFwcGVuZChhKS5odG1sKCkpKTpjP0thLmNsb25lLmNhbGwoYSk6YTtpZihoKWZvcih2YXIgbCBpbiBoKWQuZGF0YShcIiRcIitsK1wiQ29udHJvbGxlclwiLGhbbF0uaW5zdGFuY2UpO0cuJCRhZGRTY29wZUluZm8oZCxiKTtjJiZjKGQsYik7ZiYmZihiLGQsZCxlKTtyZXR1cm4gZH19ZnVuY3Rpb24gTyhhLGIsYyxkLGUsZil7ZnVuY3Rpb24gZyhhLFxuYyxkLGUpe3ZhciBmLGwsayxxLHAscyx0O2lmKG0pZm9yKHQ9QXJyYXkoYy5sZW5ndGgpLHE9MDtxPGgubGVuZ3RoO3ErPTMpZj1oW3FdLHRbZl09Y1tmXTtlbHNlIHQ9YztxPTA7Zm9yKHA9aC5sZW5ndGg7cTxwOylsPXRbaFtxKytdXSxjPWhbcSsrXSxmPWhbcSsrXSxjPyhjLnNjb3BlPyhrPWEuJG5ldygpLEcuJCRhZGRTY29wZUluZm8oQihsKSxrKSk6az1hLHM9Yy50cmFuc2NsdWRlT25UaGlzRWxlbWVudD9RKGEsYy50cmFuc2NsdWRlLGUsYy5lbGVtZW50VHJhbnNjbHVkZU9uVGhpc0VsZW1lbnQpOiFjLnRlbXBsYXRlT25UaGlzRWxlbWVudCYmZT9lOiFlJiZiP1EoYSxiKTpudWxsLGMoZixrLGwsZCxzKSk6ZiYmZihhLGwuY2hpbGROb2Rlcyx1LGUpfWZvcih2YXIgaD1bXSxsLGsscSxwLG0scz0wO3M8YS5sZW5ndGg7cysrKXtsPW5ldyBZYjtrPVgoYVtzXSxbXSxsLDA9PT1zP2Q6dSxlKTsoZj1rLmxlbmd0aD9mYShrLGFbc10sbCxiLGMsbnVsbCxbXSxbXSxmKTpudWxsKSYmXG5mLnNjb3BlJiZHLiQkYWRkU2NvcGVDbGFzcyhsLiQkZWxlbWVudCk7bD1mJiZmLnRlcm1pbmFsfHwhKHE9YVtzXS5jaGlsZE5vZGVzKXx8IXEubGVuZ3RoP251bGw6TyhxLGY/KGYudHJhbnNjbHVkZU9uVGhpc0VsZW1lbnR8fCFmLnRlbXBsYXRlT25UaGlzRWxlbWVudCkmJmYudHJhbnNjbHVkZTpiKTtpZihmfHxsKWgucHVzaChzLGYsbCkscD0hMCxtPW18fGY7Zj1udWxsfXJldHVybiBwP2c6bnVsbH1mdW5jdGlvbiBRKGEsYixjLGQpe3JldHVybiBmdW5jdGlvbihkLGUsZixnLGgpe2R8fChkPWEuJG5ldyghMSxoKSxkLiQkdHJhbnNjbHVkZWQ9ITApO3JldHVybiBiKGQsZSx7cGFyZW50Qm91bmRUcmFuc2NsdWRlRm46Yyx0cmFuc2NsdWRlQ29udHJvbGxlcnM6ZixmdXR1cmVQYXJlbnRFbGVtZW50Omd9KX19ZnVuY3Rpb24gWChhLGIsYyxkLGcpe3ZhciBoPWMuJGF0dHIsbDtzd2l0Y2goYS5ub2RlVHlwZSl7Y2FzZSBxYTpsYShiLHlhKHdhKGEpKSxcIkVcIixkLGcpO2Zvcih2YXIgayxcbnEscCxtPWEuYXR0cmlidXRlcyxzPTAsdD1tJiZtLmxlbmd0aDtzPHQ7cysrKXt2YXIgTT0hMSxJPSExO2s9bVtzXTtsPWsubmFtZTtxPU4oay52YWx1ZSk7az15YShsKTtpZihwPVUudGVzdChrKSlsPWwucmVwbGFjZShSYyxcIlwiKS5zdWJzdHIoOCkucmVwbGFjZSgvXyguKS9nLGZ1bmN0aW9uKGEsYil7cmV0dXJuIGIudG9VcHBlckNhc2UoKX0pO3ZhciBGPWsucmVwbGFjZSgvKFN0YXJ0fEVuZCkkLyxcIlwiKTtEKEYpJiZrPT09RitcIlN0YXJ0XCImJihNPWwsST1sLnN1YnN0cigwLGwubGVuZ3RoLTUpK1wiZW5kXCIsbD1sLnN1YnN0cigwLGwubGVuZ3RoLTYpKTtrPXlhKGwudG9Mb3dlckNhc2UoKSk7aFtrXT1sO2lmKHB8fCFjLmhhc093blByb3BlcnR5KGspKWNba109cSxNYyhhLGspJiYoY1trXT0hMCk7UGEoYSxiLHEsayxwKTtsYShiLGssXCJBXCIsZCxnLE0sSSl9YT1hLmNsYXNzTmFtZTtMKGEpJiYoYT1hLmFuaW1WYWwpO2lmKHgoYSkmJlwiXCIhPT1hKWZvcig7bD1mLmV4ZWMoYSk7KWs9eWEobFsyXSksXG5sYShiLGssXCJDXCIsZCxnKSYmKGNba109TihsWzNdKSksYT1hLnN1YnN0cihsLmluZGV4K2xbMF0ubGVuZ3RoKTticmVhaztjYXNlIGJiOnphKGIsYS5ub2RlVmFsdWUpO2JyZWFrO2Nhc2UgODp0cnl7aWYobD1lLmV4ZWMoYS5ub2RlVmFsdWUpKWs9eWEobFsxXSksbGEoYixrLFwiTVwiLGQsZykmJihjW2tdPU4obFsyXSkpfWNhdGNoKHYpe319Yi5zb3J0KGRhKTtyZXR1cm4gYn1mdW5jdGlvbiBiYShhLGIsYyl7dmFyIGQ9W10sZT0wO2lmKGImJmEuaGFzQXR0cmlidXRlJiZhLmhhc0F0dHJpYnV0ZShiKSl7ZG97aWYoIWEpdGhyb3cgbWEoXCJ1dGVyZGlyXCIsYixjKTthLm5vZGVUeXBlPT1xYSYmKGEuaGFzQXR0cmlidXRlKGIpJiZlKyssYS5oYXNBdHRyaWJ1dGUoYykmJmUtLSk7ZC5wdXNoKGEpO2E9YS5uZXh0U2libGluZ313aGlsZSgwPGUpfWVsc2UgZC5wdXNoKGEpO3JldHVybiBCKGQpfWZ1bmN0aW9uIFAoYSxiLGMpe3JldHVybiBmdW5jdGlvbihkLGUsZixnLGgpe2U9YmEoZVswXSxcbmIsYyk7cmV0dXJuIGEoZCxlLGYsZyxoKX19ZnVuY3Rpb24gZmEoYSxkLGUsZixnLGwsayxwLG0pe2Z1bmN0aW9uIHMoYSxiLGMsZCl7aWYoYSl7YyYmKGE9UChhLGMsZCkpO2EucmVxdWlyZT1KLnJlcXVpcmU7YS5kaXJlY3RpdmVOYW1lPWRhO2lmKFE9PT1KfHxKLiQkaXNvbGF0ZVNjb3BlKWE9WShhLHtpc29sYXRlU2NvcGU6ITB9KTtrLnB1c2goYSl9aWYoYil7YyYmKGI9UChiLGMsZCkpO2IucmVxdWlyZT1KLnJlcXVpcmU7Yi5kaXJlY3RpdmVOYW1lPWRhO2lmKFE9PT1KfHxKLiQkaXNvbGF0ZVNjb3BlKWI9WShiLHtpc29sYXRlU2NvcGU6ITB9KTtwLnB1c2goYil9fWZ1bmN0aW9uIE0oYSxiLGMsZCl7dmFyIGUsZj1cImRhdGFcIixnPSExLGw9YyxrO2lmKHgoYikpe2s9Yi5tYXRjaChoKTtiPWIuc3Vic3RyaW5nKGtbMF0ubGVuZ3RoKTtrWzNdJiYoa1sxXT9rWzNdPW51bGw6a1sxXT1rWzNdKTtcIl5cIj09PWtbMV0/Zj1cImluaGVyaXRlZERhdGFcIjpcIl5eXCI9PT1rWzFdJiYoZj1cImluaGVyaXRlZERhdGFcIixcbmw9Yy5wYXJlbnQoKSk7XCI/XCI9PT1rWzJdJiYoZz0hMCk7ZT1udWxsO2QmJlwiZGF0YVwiPT09ZiYmKGU9ZFtiXSkmJihlPWUuaW5zdGFuY2UpO2U9ZXx8bFtmXShcIiRcIitiK1wiQ29udHJvbGxlclwiKTtpZighZSYmIWcpdGhyb3cgbWEoXCJjdHJlcVwiLGIsYSk7cmV0dXJuIGV8fG51bGx9SChiKSYmKGU9W10scihiLGZ1bmN0aW9uKGIpe2UucHVzaChNKGEsYixjLGQpKX0pKTtyZXR1cm4gZX1mdW5jdGlvbiBJKGEsYyxmLGcsbCl7ZnVuY3Rpb24gaChhLGIsYyl7dmFyIGQ7V2EoYSl8fChjPWIsYj1hLGE9dSk7QSYmKGQ9Qyk7Y3x8KGM9QT9YLnBhcmVudCgpOlgpO3JldHVybiBsKGEsYixkLGMsV2IpfXZhciBtLHMsdCxFLEMsaWIsWCxQO2Q9PT1mPyhQPWUsWD1lLiQkZWxlbWVudCk6KFg9QihmKSxQPW5ldyBZYihYLGUpKTtRJiYoRT1jLiRuZXcoITApKTtsJiYoaWI9aCxpYi4kJGJvdW5kVHJhbnNjbHVkZT1sKTtPJiYoJD17fSxDPXt9LHIoTyxmdW5jdGlvbihhKXt2YXIgYj17JHNjb3BlOmE9PT1cblF8fGEuJCRpc29sYXRlU2NvcGU/RTpjLCRlbGVtZW50OlgsJGF0dHJzOlAsJHRyYW5zY2x1ZGU6aWJ9O3Q9YS5jb250cm9sbGVyO1wiQFwiPT10JiYodD1QW2EubmFtZV0pO2I9dih0LGIsITAsYS5jb250cm9sbGVyQXMpO0NbYS5uYW1lXT1iO0F8fFguZGF0YShcIiRcIithLm5hbWUrXCJDb250cm9sbGVyXCIsYi5pbnN0YW5jZSk7JFthLm5hbWVdPWJ9KSk7aWYoUSl7Ry4kJGFkZFNjb3BlSW5mbyhYLEUsITAsIShuYSYmKG5hPT09UXx8bmE9PT1RLiQkb3JpZ2luYWxEaXJlY3RpdmUpKSk7Ry4kJGFkZFNjb3BlQ2xhc3MoWCwhMCk7Zz0kJiYkW1EubmFtZV07dmFyIGJhPUU7ZyYmZy5pZGVudGlmaWVyJiYhMD09PVEuYmluZFRvQ29udHJvbGxlciYmKGJhPWcuaW5zdGFuY2UpO3IoRS4kJGlzb2xhdGVCaW5kaW5ncz1RLiQkaXNvbGF0ZUJpbmRpbmdzLGZ1bmN0aW9uKGEsZCl7dmFyIGU9YS5hdHRyTmFtZSxmPWEub3B0aW9uYWwsZyxsLGgsaztzd2l0Y2goYS5tb2RlKXtjYXNlIFwiQFwiOlAuJG9ic2VydmUoZSxcbmZ1bmN0aW9uKGEpe2JhW2RdPWF9KTtQLiQkb2JzZXJ2ZXJzW2VdLiQkc2NvcGU9YztQW2VdJiYoYmFbZF09YihQW2VdKShjKSk7YnJlYWs7Y2FzZSBcIj1cIjppZihmJiYhUFtlXSlicmVhaztsPUYoUFtlXSk7az1sLmxpdGVyYWw/aWE6ZnVuY3Rpb24oYSxiKXtyZXR1cm4gYT09PWJ8fGEhPT1hJiZiIT09Yn07aD1sLmFzc2lnbnx8ZnVuY3Rpb24oKXtnPWJhW2RdPWwoYyk7dGhyb3cgbWEoXCJub25hc3NpZ25cIixQW2VdLFEubmFtZSk7fTtnPWJhW2RdPWwoYyk7Zj1mdW5jdGlvbihhKXtrKGEsYmFbZF0pfHwoayhhLGcpP2goYyxhPWJhW2RdKTpiYVtkXT1hKTtyZXR1cm4gZz1hfTtmLiRzdGF0ZWZ1bD0hMDtmPWEuY29sbGVjdGlvbj9jLiR3YXRjaENvbGxlY3Rpb24oUFtlXSxmKTpjLiR3YXRjaChGKFBbZV0sZiksbnVsbCxsLmxpdGVyYWwpO0UuJG9uKFwiJGRlc3Ryb3lcIixmKTticmVhaztjYXNlIFwiJlwiOmw9RihQW2VdKSxiYVtkXT1mdW5jdGlvbihhKXtyZXR1cm4gbChjLGEpfX19KX0kJiZcbihyKCQsZnVuY3Rpb24oYSl7YSgpfSksJD1udWxsKTtnPTA7Zm9yKG09ay5sZW5ndGg7ZzxtO2crKylzPWtbZ10sWihzLHMuaXNvbGF0ZVNjb3BlP0U6YyxYLFAscy5yZXF1aXJlJiZNKHMuZGlyZWN0aXZlTmFtZSxzLnJlcXVpcmUsWCxDKSxpYik7dmFyIFdiPWM7USYmKFEudGVtcGxhdGV8fG51bGw9PT1RLnRlbXBsYXRlVXJsKSYmKFdiPUUpO2EmJmEoV2IsZi5jaGlsZE5vZGVzLHUsbCk7Zm9yKGc9cC5sZW5ndGgtMTswPD1nO2ctLSlzPXBbZ10sWihzLHMuaXNvbGF0ZVNjb3BlP0U6YyxYLFAscy5yZXF1aXJlJiZNKHMuZGlyZWN0aXZlTmFtZSxzLnJlcXVpcmUsWCxDKSxpYil9bT1tfHx7fTtmb3IodmFyIEU9LU51bWJlci5NQVhfVkFMVUUsQyxPPW0uY29udHJvbGxlckRpcmVjdGl2ZXMsJCxRPW0ubmV3SXNvbGF0ZVNjb3BlRGlyZWN0aXZlLG5hPW0udGVtcGxhdGVEaXJlY3RpdmUsZmE9bS5ub25UbGJUcmFuc2NsdWRlRGlyZWN0aXZlLGxhPSExLEQ9ITEsQT1tLmhhc0VsZW1lbnRUcmFuc2NsdWRlRGlyZWN0aXZlLFxudz1lLiQkZWxlbWVudD1CKGQpLEosZGEsVixoYj1mLHphLEs9MCxSPWEubGVuZ3RoO0s8UjtLKyspe0o9YVtLXTt2YXIgUGE9Si4kJHN0YXJ0LFU9Si4kJGVuZDtQYSYmKHc9YmEoZCxQYSxVKSk7Vj11O2lmKEU+Si5wcmlvcml0eSlicmVhaztpZihWPUouc2NvcGUpSi50ZW1wbGF0ZVVybHx8KEwoVik/KE9hKFwibmV3L2lzb2xhdGVkIHNjb3BlXCIsUXx8QyxKLHcpLFE9Sik6T2EoXCJuZXcvaXNvbGF0ZWQgc2NvcGVcIixRLEosdykpLEM9Q3x8SjtkYT1KLm5hbWU7IUoudGVtcGxhdGVVcmwmJkouY29udHJvbGxlciYmKFY9Si5jb250cm9sbGVyLE89T3x8e30sT2EoXCInXCIrZGErXCInIGNvbnRyb2xsZXJcIixPW2RhXSxKLHcpLE9bZGFdPUopO2lmKFY9Si50cmFuc2NsdWRlKWxhPSEwLEouJCR0bGJ8fChPYShcInRyYW5zY2x1c2lvblwiLGZhLEosdyksZmE9SiksXCJlbGVtZW50XCI9PVY/KEE9ITAsRT1KLnByaW9yaXR5LFY9dyx3PWUuJCRlbGVtZW50PUIoVy5jcmVhdGVDb21tZW50KFwiIFwiK2RhK1wiOiBcIitcbmVbZGFdK1wiIFwiKSksZD13WzBdLFQoZywkYS5jYWxsKFYsMCksZCksaGI9RyhWLGYsRSxsJiZsLm5hbWUse25vblRsYlRyYW5zY2x1ZGVEaXJlY3RpdmU6ZmF9KSk6KFY9QihVYihkKSkuY29udGVudHMoKSx3LmVtcHR5KCksaGI9RyhWLGYpKTtpZihKLnRlbXBsYXRlKWlmKEQ9ITAsT2EoXCJ0ZW1wbGF0ZVwiLG5hLEosdyksbmE9SixWPXooSi50ZW1wbGF0ZSk/Si50ZW1wbGF0ZSh3LGUpOkoudGVtcGxhdGUsVj1TYyhWKSxKLnJlcGxhY2Upe2w9SjtWPVNiLnRlc3QoVik/VGMoWGIoSi50ZW1wbGF0ZU5hbWVzcGFjZSxOKFYpKSk6W107ZD1WWzBdO2lmKDEhPVYubGVuZ3RofHxkLm5vZGVUeXBlIT09cWEpdGhyb3cgbWEoXCJ0cGxydFwiLGRhLFwiXCIpO1QoZyx3LGQpO1I9eyRhdHRyOnt9fTtWPVgoZCxbXSxSKTt2YXIgYWE9YS5zcGxpY2UoSysxLGEubGVuZ3RoLShLKzEpKTtRJiZ5KFYpO2E9YS5jb25jYXQoVikuY29uY2F0KGFhKTtTKGUsUik7Uj1hLmxlbmd0aH1lbHNlIHcuaHRtbChWKTtpZihKLnRlbXBsYXRlVXJsKUQ9XG4hMCxPYShcInRlbXBsYXRlXCIsbmEsSix3KSxuYT1KLEoucmVwbGFjZSYmKGw9SiksST1vZihhLnNwbGljZShLLGEubGVuZ3RoLUspLHcsZSxnLGxhJiZoYixrLHAse2NvbnRyb2xsZXJEaXJlY3RpdmVzOk8sbmV3SXNvbGF0ZVNjb3BlRGlyZWN0aXZlOlEsdGVtcGxhdGVEaXJlY3RpdmU6bmEsbm9uVGxiVHJhbnNjbHVkZURpcmVjdGl2ZTpmYX0pLFI9YS5sZW5ndGg7ZWxzZSBpZihKLmNvbXBpbGUpdHJ5e3phPUouY29tcGlsZSh3LGUsaGIpLHooemEpP3MobnVsbCx6YSxQYSxVKTp6YSYmcyh6YS5wcmUsemEucG9zdCxQYSxVKX1jYXRjaChwZil7YyhwZix4YSh3KSl9Si50ZXJtaW5hbCYmKEkudGVybWluYWw9ITAsRT1NYXRoLm1heChFLEoucHJpb3JpdHkpKX1JLnNjb3BlPUMmJiEwPT09Qy5zY29wZTtJLnRyYW5zY2x1ZGVPblRoaXNFbGVtZW50PWxhO0kuZWxlbWVudFRyYW5zY2x1ZGVPblRoaXNFbGVtZW50PUE7SS50ZW1wbGF0ZU9uVGhpc0VsZW1lbnQ9RDtJLnRyYW5zY2x1ZGU9aGI7XG5tLmhhc0VsZW1lbnRUcmFuc2NsdWRlRGlyZWN0aXZlPUE7cmV0dXJuIEl9ZnVuY3Rpb24geShhKXtmb3IodmFyIGI9MCxjPWEubGVuZ3RoO2I8YztiKyspYVtiXT1PYihhW2JdLHskJGlzb2xhdGVTY29wZTohMH0pfWZ1bmN0aW9uIGxhKGIsZSxmLGcsbCxoLGspe2lmKGU9PT1sKXJldHVybiBudWxsO2w9bnVsbDtpZihkLmhhc093blByb3BlcnR5KGUpKXt2YXIgcTtlPWEuZ2V0KGUrXCJEaXJlY3RpdmVcIik7Zm9yKHZhciBtPTAscz1lLmxlbmd0aDttPHM7bSsrKXRyeXtxPWVbbV0sKGc9PT11fHxnPnEucHJpb3JpdHkpJiYtMSE9cS5yZXN0cmljdC5pbmRleE9mKGYpJiYoaCYmKHE9T2IocSx7JCRzdGFydDpoLCQkZW5kOmt9KSksYi5wdXNoKHEpLGw9cSl9Y2F0Y2goSSl7YyhJKX19cmV0dXJuIGx9ZnVuY3Rpb24gRChiKXtpZihkLmhhc093blByb3BlcnR5KGIpKWZvcih2YXIgYz1hLmdldChiK1wiRGlyZWN0aXZlXCIpLGU9MCxmPWMubGVuZ3RoO2U8ZjtlKyspaWYoYj1jW2VdLGIubXVsdGlFbGVtZW50KXJldHVybiEwO1xucmV0dXJuITF9ZnVuY3Rpb24gUyhhLGIpe3ZhciBjPWIuJGF0dHIsZD1hLiRhdHRyLGU9YS4kJGVsZW1lbnQ7cihhLGZ1bmN0aW9uKGQsZSl7XCIkXCIhPWUuY2hhckF0KDApJiYoYltlXSYmYltlXSE9PWQmJihkKz0oXCJzdHlsZVwiPT09ZT9cIjtcIjpcIiBcIikrYltlXSksYS4kc2V0KGUsZCwhMCxjW2VdKSl9KTtyKGIsZnVuY3Rpb24oYixmKXtcImNsYXNzXCI9PWY/KEUoZSxiKSxhW1wiY2xhc3NcIl09KGFbXCJjbGFzc1wiXT9hW1wiY2xhc3NcIl0rXCIgXCI6XCJcIikrYik6XCJzdHlsZVwiPT1mPyhlLmF0dHIoXCJzdHlsZVwiLGUuYXR0cihcInN0eWxlXCIpK1wiO1wiK2IpLGEuc3R5bGU9KGEuc3R5bGU/YS5zdHlsZStcIjtcIjpcIlwiKStiKTpcIiRcIj09Zi5jaGFyQXQoMCl8fGEuaGFzT3duUHJvcGVydHkoZil8fChhW2ZdPWIsZFtmXT1jW2ZdKX0pfWZ1bmN0aW9uIG9mKGEsYixjLGQsZSxmLGcsbCl7dmFyIGg9W10sayxxLHA9YlswXSxtPWEuc2hpZnQoKSx0PU9iKG0se3RlbXBsYXRlVXJsOm51bGwsdHJhbnNjbHVkZTpudWxsLFxucmVwbGFjZTpudWxsLCQkb3JpZ2luYWxEaXJlY3RpdmU6bX0pLEk9eihtLnRlbXBsYXRlVXJsKT9tLnRlbXBsYXRlVXJsKGIsYyk6bS50ZW1wbGF0ZVVybCxNPW0udGVtcGxhdGVOYW1lc3BhY2U7Yi5lbXB0eSgpO3MoSSkudGhlbihmdW5jdGlvbihzKXt2YXIgRix2O3M9U2Mocyk7aWYobS5yZXBsYWNlKXtzPVNiLnRlc3Qocyk/VGMoWGIoTSxOKHMpKSk6W107Rj1zWzBdO2lmKDEhPXMubGVuZ3RofHxGLm5vZGVUeXBlIT09cWEpdGhyb3cgbWEoXCJ0cGxydFwiLG0ubmFtZSxJKTtzPXskYXR0cjp7fX07VChkLGIsRik7dmFyIEM9WChGLFtdLHMpO0wobS5zY29wZSkmJnkoQyk7YT1DLmNvbmNhdChhKTtTKGMscyl9ZWxzZSBGPXAsYi5odG1sKHMpO2EudW5zaGlmdCh0KTtrPWZhKGEsRixjLGUsYixtLGYsZyxsKTtyKGQsZnVuY3Rpb24oYSxjKXthPT1GJiYoZFtjXT1iWzBdKX0pO2ZvcihxPU8oYlswXS5jaGlsZE5vZGVzLGUpO2gubGVuZ3RoOyl7cz1oLnNoaWZ0KCk7dj1oLnNoaWZ0KCk7XG52YXIgRz1oLnNoaWZ0KCksUD1oLnNoaWZ0KCksQz1iWzBdO2lmKCFzLiQkZGVzdHJveWVkKXtpZih2IT09cCl7dmFyICQ9di5jbGFzc05hbWU7bC5oYXNFbGVtZW50VHJhbnNjbHVkZURpcmVjdGl2ZSYmbS5yZXBsYWNlfHwoQz1VYihGKSk7VChHLEIodiksQyk7RShCKEMpLCQpfXY9ay50cmFuc2NsdWRlT25UaGlzRWxlbWVudD9RKHMsay50cmFuc2NsdWRlLFApOlA7ayhxLHMsQyxkLHYpfX1oPW51bGx9KTtyZXR1cm4gZnVuY3Rpb24oYSxiLGMsZCxlKXthPWU7Yi4kJGRlc3Ryb3llZHx8KGg/aC5wdXNoKGIsYyxkLGEpOihrLnRyYW5zY2x1ZGVPblRoaXNFbGVtZW50JiYoYT1RKGIsay50cmFuc2NsdWRlLGUpKSxrKHEsYixjLGQsYSkpKX19ZnVuY3Rpb24gZGEoYSxiKXt2YXIgYz1iLnByaW9yaXR5LWEucHJpb3JpdHk7cmV0dXJuIDAhPT1jP2M6YS5uYW1lIT09Yi5uYW1lP2EubmFtZTxiLm5hbWU/LTE6MTphLmluZGV4LWIuaW5kZXh9ZnVuY3Rpb24gT2EoYSxiLGMsZCl7aWYoYil0aHJvdyBtYShcIm11bHRpZGlyXCIsXG5iLm5hbWUsYy5uYW1lLGEseGEoZCkpO31mdW5jdGlvbiB6YShhLGMpe3ZhciBkPWIoYywhMCk7ZCYmYS5wdXNoKHtwcmlvcml0eTowLGNvbXBpbGU6ZnVuY3Rpb24oYSl7YT1hLnBhcmVudCgpO3ZhciBiPSEhYS5sZW5ndGg7YiYmRy4kJGFkZEJpbmRpbmdDbGFzcyhhKTtyZXR1cm4gZnVuY3Rpb24oYSxjKXt2YXIgZT1jLnBhcmVudCgpO2J8fEcuJCRhZGRCaW5kaW5nQ2xhc3MoZSk7Ry4kJGFkZEJpbmRpbmdJbmZvKGUsZC5leHByZXNzaW9ucyk7YS4kd2F0Y2goZCxmdW5jdGlvbihhKXtjWzBdLm5vZGVWYWx1ZT1hfSl9fX0pfWZ1bmN0aW9uIFhiKGEsYil7YT1LKGF8fFwiaHRtbFwiKTtzd2l0Y2goYSl7Y2FzZSBcInN2Z1wiOmNhc2UgXCJtYXRoXCI6dmFyIGM9Vy5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO2MuaW5uZXJIVE1MPVwiPFwiK2ErXCI+XCIrYitcIjwvXCIrYStcIj5cIjtyZXR1cm4gYy5jaGlsZE5vZGVzWzBdLmNoaWxkTm9kZXM7ZGVmYXVsdDpyZXR1cm4gYn19ZnVuY3Rpb24gUihhLGIpe2lmKFwic3JjZG9jXCI9PVxuYilyZXR1cm4gJC5IVE1MO3ZhciBjPXdhKGEpO2lmKFwieGxpbmtIcmVmXCI9PWJ8fFwiZm9ybVwiPT1jJiZcImFjdGlvblwiPT1ifHxcImltZ1wiIT1jJiYoXCJzcmNcIj09Ynx8XCJuZ1NyY1wiPT1iKSlyZXR1cm4gJC5SRVNPVVJDRV9VUkx9ZnVuY3Rpb24gUGEoYSxjLGQsZSxmKXt2YXIgaD1SKGEsZSk7Zj1nW2VdfHxmO3ZhciBrPWIoZCwhMCxoLGYpO2lmKGspe2lmKFwibXVsdGlwbGVcIj09PWUmJlwic2VsZWN0XCI9PT13YShhKSl0aHJvdyBtYShcInNlbG11bHRpXCIseGEoYSkpO2MucHVzaCh7cHJpb3JpdHk6MTAwLGNvbXBpbGU6ZnVuY3Rpb24oKXtyZXR1cm57cHJlOmZ1bmN0aW9uKGEsYyxnKXtjPWcuJCRvYnNlcnZlcnN8fChnLiQkb2JzZXJ2ZXJzPXt9KTtpZihsLnRlc3QoZSkpdGhyb3cgbWEoXCJub2RvbWV2ZW50c1wiKTt2YXIgcD1nW2VdO3AhPT1kJiYoaz1wJiZiKHAsITAsaCxmKSxkPXApO2smJihnW2VdPWsoYSksKGNbZV18fChjW2VdPVtdKSkuJCRpbnRlcj0hMCwoZy4kJG9ic2VydmVycyYmZy4kJG9ic2VydmVyc1tlXS4kJHNjb3BlfHxcbmEpLiR3YXRjaChrLGZ1bmN0aW9uKGEsYil7XCJjbGFzc1wiPT09ZSYmYSE9Yj9nLiR1cGRhdGVDbGFzcyhhLGIpOmcuJHNldChlLGEpfSkpfX19fSl9fWZ1bmN0aW9uIFQoYSxiLGMpe3ZhciBkPWJbMF0sZT1iLmxlbmd0aCxmPWQucGFyZW50Tm9kZSxnLGw7aWYoYSlmb3IoZz0wLGw9YS5sZW5ndGg7ZzxsO2crKylpZihhW2ddPT1kKXthW2crK109YztsPWcrZS0xO2Zvcih2YXIgaD1hLmxlbmd0aDtnPGg7ZysrLGwrKylsPGg/YVtnXT1hW2xdOmRlbGV0ZSBhW2ddO2EubGVuZ3RoLT1lLTE7YS5jb250ZXh0PT09ZCYmKGEuY29udGV4dD1jKTticmVha31mJiZmLnJlcGxhY2VDaGlsZChjLGQpO2E9Vy5jcmVhdGVEb2N1bWVudEZyYWdtZW50KCk7YS5hcHBlbmRDaGlsZChkKTtCKGMpLmRhdGEoQihkKS5kYXRhKCkpO3RhPyhRYj0hMCx0YS5jbGVhbkRhdGEoW2RdKSk6ZGVsZXRlIEIuY2FjaGVbZFtCLmV4cGFuZG9dXTtkPTE7Zm9yKGU9Yi5sZW5ndGg7ZDxlO2QrKylmPWJbZF0sQihmKS5yZW1vdmUoKSxcbmEuYXBwZW5kQ2hpbGQoZiksZGVsZXRlIGJbZF07YlswXT1jO2IubGVuZ3RoPTF9ZnVuY3Rpb24gWShhLGIpe3JldHVybiB3KGZ1bmN0aW9uKCl7cmV0dXJuIGEuYXBwbHkobnVsbCxhcmd1bWVudHMpfSxhLGIpfWZ1bmN0aW9uIFooYSxiLGQsZSxmLGcpe3RyeXthKGIsZCxlLGYsZyl9Y2F0Y2gobCl7YyhsLHhhKGQpKX19dmFyIFliPWZ1bmN0aW9uKGEsYil7aWYoYil7dmFyIGM9T2JqZWN0LmtleXMoYiksZCxlLGY7ZD0wO2ZvcihlPWMubGVuZ3RoO2Q8ZTtkKyspZj1jW2RdLHRoaXNbZl09YltmXX1lbHNlIHRoaXMuJGF0dHI9e307dGhpcy4kJGVsZW1lbnQ9YX07WWIucHJvdG90eXBlPXskbm9ybWFsaXplOnlhLCRhZGRDbGFzczpmdW5jdGlvbihhKXthJiYwPGEubGVuZ3RoJiZJLmFkZENsYXNzKHRoaXMuJCRlbGVtZW50LGEpfSwkcmVtb3ZlQ2xhc3M6ZnVuY3Rpb24oYSl7YSYmMDxhLmxlbmd0aCYmSS5yZW1vdmVDbGFzcyh0aGlzLiQkZWxlbWVudCxhKX0sJHVwZGF0ZUNsYXNzOmZ1bmN0aW9uKGEsXG5iKXt2YXIgYz1VYyhhLGIpO2MmJmMubGVuZ3RoJiZJLmFkZENsYXNzKHRoaXMuJCRlbGVtZW50LGMpOyhjPVVjKGIsYSkpJiZjLmxlbmd0aCYmSS5yZW1vdmVDbGFzcyh0aGlzLiQkZWxlbWVudCxjKX0sJHNldDpmdW5jdGlvbihhLGIsZCxlKXt2YXIgZj10aGlzLiQkZWxlbWVudFswXSxnPU1jKGYsYSksbD1rZihmLGEpLGY9YTtnPyh0aGlzLiQkZWxlbWVudC5wcm9wKGEsYiksZT1nKTpsJiYodGhpc1tsXT1iLGY9bCk7dGhpc1thXT1iO2U/dGhpcy4kYXR0clthXT1lOihlPXRoaXMuJGF0dHJbYV0pfHwodGhpcy4kYXR0clthXT1lPXVjKGEsXCItXCIpKTtnPXdhKHRoaXMuJCRlbGVtZW50KTtpZihcImFcIj09PWcmJlwiaHJlZlwiPT09YXx8XCJpbWdcIj09PWcmJlwic3JjXCI9PT1hKXRoaXNbYV09Yj1NKGIsXCJzcmNcIj09PWEpO2Vsc2UgaWYoXCJpbWdcIj09PWcmJlwic3Jjc2V0XCI9PT1hKXtmb3IodmFyIGc9XCJcIixsPU4oYiksaD0vKFxccytcXGQreFxccyosfFxccytcXGQrd1xccyosfFxccyssfCxcXHMrKS8saD0vXFxzLy50ZXN0KGwpP1xuaDovKCwpLyxsPWwuc3BsaXQoaCksaD1NYXRoLmZsb29yKGwubGVuZ3RoLzIpLGs9MDtrPGg7aysrKXZhciBxPTIqayxnPWcrTShOKGxbcV0pLCEwKSxnPWcrKFwiIFwiK04obFtxKzFdKSk7bD1OKGxbMiprXSkuc3BsaXQoL1xccy8pO2crPU0oTihsWzBdKSwhMCk7Mj09PWwubGVuZ3RoJiYoZys9XCIgXCIrTihsWzFdKSk7dGhpc1thXT1iPWd9ITEhPT1kJiYobnVsbD09PWJ8fGI9PT11P3RoaXMuJCRlbGVtZW50LnJlbW92ZUF0dHIoZSk6dGhpcy4kJGVsZW1lbnQuYXR0cihlLGIpKTsoYT10aGlzLiQkb2JzZXJ2ZXJzKSYmcihhW2ZdLGZ1bmN0aW9uKGEpe3RyeXthKGIpfWNhdGNoKGQpe2MoZCl9fSl9LCRvYnNlcnZlOmZ1bmN0aW9uKGEsYil7dmFyIGM9dGhpcyxkPWMuJCRvYnNlcnZlcnN8fChjLiQkb2JzZXJ2ZXJzPWphKCkpLGU9ZFthXXx8KGRbYV09W10pO2UucHVzaChiKTttLiRldmFsQXN5bmMoZnVuY3Rpb24oKXshZS4kJGludGVyJiZjLmhhc093blByb3BlcnR5KGEpJiZiKGNbYV0pfSk7XG5yZXR1cm4gZnVuY3Rpb24oKXtZYShlLGIpfX19O3ZhciBWPWIuc3RhcnRTeW1ib2woKSxuYT1iLmVuZFN5bWJvbCgpLFNjPVwie3tcIj09Vnx8XCJ9fVwiPT1uYT9yYTpmdW5jdGlvbihhKXtyZXR1cm4gYS5yZXBsYWNlKC9cXHtcXHsvZyxWKS5yZXBsYWNlKC99fS9nLG5hKX0sVT0vXm5nQXR0cltBLVpdLztHLiQkYWRkQmluZGluZ0luZm89az9mdW5jdGlvbihhLGIpe3ZhciBjPWEuZGF0YShcIiRiaW5kaW5nXCIpfHxbXTtIKGIpP2M9Yy5jb25jYXQoYik6Yy5wdXNoKGIpO2EuZGF0YShcIiRiaW5kaW5nXCIsYyl9OkE7Ry4kJGFkZEJpbmRpbmdDbGFzcz1rP2Z1bmN0aW9uKGEpe0UoYSxcIm5nLWJpbmRpbmdcIil9OkE7Ry4kJGFkZFNjb3BlSW5mbz1rP2Z1bmN0aW9uKGEsYixjLGQpe2EuZGF0YShjP2Q/XCIkaXNvbGF0ZVNjb3BlTm9UZW1wbGF0ZVwiOlwiJGlzb2xhdGVTY29wZVwiOlwiJHNjb3BlXCIsYil9OkE7Ry4kJGFkZFNjb3BlQ2xhc3M9az9mdW5jdGlvbihhLGIpe0UoYSxiP1wibmctaXNvbGF0ZS1zY29wZVwiOlxuXCJuZy1zY29wZVwiKX06QTtyZXR1cm4gR31dfWZ1bmN0aW9uIHlhKGIpe3JldHVybiBmYihiLnJlcGxhY2UoUmMsXCJcIikpfWZ1bmN0aW9uIFVjKGIsYSl7dmFyIGM9XCJcIixkPWIuc3BsaXQoL1xccysvKSxlPWEuc3BsaXQoL1xccysvKSxmPTA7YTpmb3IoO2Y8ZC5sZW5ndGg7ZisrKXtmb3IodmFyIGc9ZFtmXSxoPTA7aDxlLmxlbmd0aDtoKyspaWYoZz09ZVtoXSljb250aW51ZSBhO2MrPSgwPGMubGVuZ3RoP1wiIFwiOlwiXCIpK2d9cmV0dXJuIGN9ZnVuY3Rpb24gVGMoYil7Yj1CKGIpO3ZhciBhPWIubGVuZ3RoO2lmKDE+PWEpcmV0dXJuIGI7Zm9yKDthLS07KTg9PT1iW2FdLm5vZGVUeXBlJiZxZi5jYWxsKGIsYSwxKTtyZXR1cm4gYn1mdW5jdGlvbiBGZSgpe3ZhciBiPXt9LGE9ITEsYz0vXihcXFMrKShcXHMrYXNcXHMrKFxcdyspKT8kLzt0aGlzLnJlZ2lzdGVyPWZ1bmN0aW9uKGEsYyl7TWEoYSxcImNvbnRyb2xsZXJcIik7TChhKT93KGIsYSk6YlthXT1jfTt0aGlzLmFsbG93R2xvYmFscz1mdW5jdGlvbigpe2E9XG4hMH07dGhpcy4kZ2V0PVtcIiRpbmplY3RvclwiLFwiJHdpbmRvd1wiLGZ1bmN0aW9uKGQsZSl7ZnVuY3Rpb24gZihhLGIsYyxkKXtpZighYXx8IUwoYS4kc2NvcGUpKXRocm93IFMoXCIkY29udHJvbGxlclwiKShcIm5vc2NwXCIsZCxiKTthLiRzY29wZVtiXT1jfXJldHVybiBmdW5jdGlvbihnLGgsbCxrKXt2YXIgbixwLHE7bD0hMD09PWw7ayYmeChrKSYmKHE9ayk7aWYoeChnKSl7az1nLm1hdGNoKGMpO2lmKCFrKXRocm93IHJmKFwiY3RybGZtdFwiLGcpO3A9a1sxXTtxPXF8fGtbM107Zz1iLmhhc093blByb3BlcnR5KHApP2JbcF06d2MoaC4kc2NvcGUscCwhMCl8fChhP3djKGUscCwhMCk6dSk7TGEoZyxwLCEwKX1pZihsKXJldHVybiBsPShIKGcpP2dbZy5sZW5ndGgtMV06ZykucHJvdG90eXBlLG49T2JqZWN0LmNyZWF0ZShsfHxudWxsKSxxJiZmKGgscSxuLHB8fGcubmFtZSksdyhmdW5jdGlvbigpe2QuaW52b2tlKGcsbixoLHApO3JldHVybiBufSx7aW5zdGFuY2U6bixpZGVudGlmaWVyOnF9KTtcbm49ZC5pbnN0YW50aWF0ZShnLGgscCk7cSYmZihoLHEsbixwfHxnLm5hbWUpO3JldHVybiBufX1dfWZ1bmN0aW9uIEdlKCl7dGhpcy4kZ2V0PVtcIiR3aW5kb3dcIixmdW5jdGlvbihiKXtyZXR1cm4gQihiLmRvY3VtZW50KX1dfWZ1bmN0aW9uIEhlKCl7dGhpcy4kZ2V0PVtcIiRsb2dcIixmdW5jdGlvbihiKXtyZXR1cm4gZnVuY3Rpb24oYSxjKXtiLmVycm9yLmFwcGx5KGIsYXJndW1lbnRzKX19XX1mdW5jdGlvbiBaYihiLGEpe2lmKHgoYikpe3ZhciBjPWIucmVwbGFjZShzZixcIlwiKS50cmltKCk7aWYoYyl7dmFyIGQ9YShcIkNvbnRlbnQtVHlwZVwiKTsoZD1kJiYwPT09ZC5pbmRleE9mKFZjKSl8fChkPShkPWMubWF0Y2godGYpKSYmdWZbZFswXV0udGVzdChjKSk7ZCYmKGI9cGMoYykpfX1yZXR1cm4gYn1mdW5jdGlvbiBXYyhiKXt2YXIgYT1qYSgpLGMsZCxlO2lmKCFiKXJldHVybiBhO3IoYi5zcGxpdChcIlxcblwiKSxmdW5jdGlvbihiKXtlPWIuaW5kZXhPZihcIjpcIik7Yz1LKE4oYi5zdWJzdHIoMCxcbmUpKSk7ZD1OKGIuc3Vic3RyKGUrMSkpO2MmJihhW2NdPWFbY10/YVtjXStcIiwgXCIrZDpkKX0pO3JldHVybiBhfWZ1bmN0aW9uIFhjKGIpe3ZhciBhPUwoYik/Yjp1O3JldHVybiBmdW5jdGlvbihjKXthfHwoYT1XYyhiKSk7cmV0dXJuIGM/KGM9YVtLKGMpXSx2b2lkIDA9PT1jJiYoYz1udWxsKSxjKTphfX1mdW5jdGlvbiBZYyhiLGEsYyxkKXtpZih6KGQpKXJldHVybiBkKGIsYSxjKTtyKGQsZnVuY3Rpb24oZCl7Yj1kKGIsYSxjKX0pO3JldHVybiBifWZ1bmN0aW9uIEtlKCl7dmFyIGI9dGhpcy5kZWZhdWx0cz17dHJhbnNmb3JtUmVzcG9uc2U6W1piXSx0cmFuc2Zvcm1SZXF1ZXN0OltmdW5jdGlvbihhKXtyZXR1cm4gTChhKSYmXCJbb2JqZWN0IEZpbGVdXCIhPT1DYS5jYWxsKGEpJiZcIltvYmplY3QgQmxvYl1cIiE9PUNhLmNhbGwoYSkmJlwiW29iamVjdCBGb3JtRGF0YV1cIiE9PUNhLmNhbGwoYSk/YWIoYSk6YX1dLGhlYWRlcnM6e2NvbW1vbjp7QWNjZXB0OlwiYXBwbGljYXRpb24vanNvbiwgdGV4dC9wbGFpbiwgKi8qXCJ9LFxucG9zdDpzYSgkYikscHV0OnNhKCRiKSxwYXRjaDpzYSgkYil9LHhzcmZDb29raWVOYW1lOlwiWFNSRi1UT0tFTlwiLHhzcmZIZWFkZXJOYW1lOlwiWC1YU1JGLVRPS0VOXCJ9LGE9ITE7dGhpcy51c2VBcHBseUFzeW5jPWZ1bmN0aW9uKGIpe3JldHVybiB5KGIpPyhhPSEhYix0aGlzKTphfTt2YXIgYz10aGlzLmludGVyY2VwdG9ycz1bXTt0aGlzLiRnZXQ9W1wiJGh0dHBCYWNrZW5kXCIsXCIkYnJvd3NlclwiLFwiJGNhY2hlRmFjdG9yeVwiLFwiJHJvb3RTY29wZVwiLFwiJHFcIixcIiRpbmplY3RvclwiLGZ1bmN0aW9uKGQsZSxmLGcsaCxsKXtmdW5jdGlvbiBrKGEpe2Z1bmN0aW9uIGMoYSl7dmFyIGI9dyh7fSxhKTtiLmRhdGE9YS5kYXRhP1ljKGEuZGF0YSxhLmhlYWRlcnMsYS5zdGF0dXMsZS50cmFuc2Zvcm1SZXNwb25zZSk6YS5kYXRhO2E9YS5zdGF0dXM7cmV0dXJuIDIwMDw9YSYmMzAwPmE/YjpoLnJlamVjdChiKX1mdW5jdGlvbiBkKGEpe3ZhciBiLGM9e307cihhLGZ1bmN0aW9uKGEsZCl7eihhKT8oYj1cbmEoKSxudWxsIT1iJiYoY1tkXT1iKSk6Y1tkXT1hfSk7cmV0dXJuIGN9aWYoIWNhLmlzT2JqZWN0KGEpKXRocm93IFMoXCIkaHR0cFwiKShcImJhZHJlcVwiLGEpO3ZhciBlPXcoe21ldGhvZDpcImdldFwiLHRyYW5zZm9ybVJlcXVlc3Q6Yi50cmFuc2Zvcm1SZXF1ZXN0LHRyYW5zZm9ybVJlc3BvbnNlOmIudHJhbnNmb3JtUmVzcG9uc2V9LGEpO2UuaGVhZGVycz1mdW5jdGlvbihhKXt2YXIgYz1iLmhlYWRlcnMsZT13KHt9LGEuaGVhZGVycyksZixnLGM9dyh7fSxjLmNvbW1vbixjW0soYS5tZXRob2QpXSk7YTpmb3IoZiBpbiBjKXthPUsoZik7Zm9yKGcgaW4gZSlpZihLKGcpPT09YSljb250aW51ZSBhO2VbZl09Y1tmXX1yZXR1cm4gZChlKX0oYSk7ZS5tZXRob2Q9dmIoZS5tZXRob2QpO3ZhciBmPVtmdW5jdGlvbihhKXt2YXIgZD1hLmhlYWRlcnMsZT1ZYyhhLmRhdGEsWGMoZCksdSxhLnRyYW5zZm9ybVJlcXVlc3QpO0QoZSkmJnIoZCxmdW5jdGlvbihhLGIpe1wiY29udGVudC10eXBlXCI9PT1LKGIpJiZcbmRlbGV0ZSBkW2JdfSk7RChhLndpdGhDcmVkZW50aWFscykmJiFEKGIud2l0aENyZWRlbnRpYWxzKSYmKGEud2l0aENyZWRlbnRpYWxzPWIud2l0aENyZWRlbnRpYWxzKTtyZXR1cm4gbihhLGUpLnRoZW4oYyxjKX0sdV0sZz1oLndoZW4oZSk7Zm9yKHIodCxmdW5jdGlvbihhKXsoYS5yZXF1ZXN0fHxhLnJlcXVlc3RFcnJvcikmJmYudW5zaGlmdChhLnJlcXVlc3QsYS5yZXF1ZXN0RXJyb3IpOyhhLnJlc3BvbnNlfHxhLnJlc3BvbnNlRXJyb3IpJiZmLnB1c2goYS5yZXNwb25zZSxhLnJlc3BvbnNlRXJyb3IpfSk7Zi5sZW5ndGg7KXthPWYuc2hpZnQoKTt2YXIgbD1mLnNoaWZ0KCksZz1nLnRoZW4oYSxsKX1nLnN1Y2Nlc3M9ZnVuY3Rpb24oYSl7TGEoYSxcImZuXCIpO2cudGhlbihmdW5jdGlvbihiKXthKGIuZGF0YSxiLnN0YXR1cyxiLmhlYWRlcnMsZSl9KTtyZXR1cm4gZ307Zy5lcnJvcj1mdW5jdGlvbihhKXtMYShhLFwiZm5cIik7Zy50aGVuKG51bGwsZnVuY3Rpb24oYil7YShiLmRhdGEsXG5iLnN0YXR1cyxiLmhlYWRlcnMsZSl9KTtyZXR1cm4gZ307cmV0dXJuIGd9ZnVuY3Rpb24gbihjLGYpe2Z1bmN0aW9uIGwoYixjLGQsZSl7ZnVuY3Rpb24gZigpe20oYyxiLGQsZSl9RSYmKDIwMDw9YiYmMzAwPmI/RS5wdXQoUSxbYixjLFdjKGQpLGVdKTpFLnJlbW92ZShRKSk7YT9nLiRhcHBseUFzeW5jKGYpOihmKCksZy4kJHBoYXNlfHxnLiRhcHBseSgpKX1mdW5jdGlvbiBtKGEsYixkLGUpe2I9LTE8PWI/YjowOygyMDA8PWImJjMwMD5iP0kucmVzb2x2ZTpJLnJlamVjdCkoe2RhdGE6YSxzdGF0dXM6YixoZWFkZXJzOlhjKGQpLGNvbmZpZzpjLHN0YXR1c1RleHQ6ZX0pfWZ1bmN0aW9uIG4oYSl7bShhLmRhdGEsYS5zdGF0dXMsc2EoYS5oZWFkZXJzKCkpLGEuc3RhdHVzVGV4dCl9ZnVuY3Rpb24gdCgpe3ZhciBhPWsucGVuZGluZ1JlcXVlc3RzLmluZGV4T2YoYyk7LTEhPT1hJiZrLnBlbmRpbmdSZXF1ZXN0cy5zcGxpY2UoYSwxKX12YXIgST1oLmRlZmVyKCksTT1JLnByb21pc2UsXG5FLEcsTz1jLmhlYWRlcnMsUT1wKGMudXJsLGMucGFyYW1zKTtrLnBlbmRpbmdSZXF1ZXN0cy5wdXNoKGMpO00udGhlbih0LHQpOyFjLmNhY2hlJiYhYi5jYWNoZXx8ITE9PT1jLmNhY2hlfHxcIkdFVFwiIT09Yy5tZXRob2QmJlwiSlNPTlBcIiE9PWMubWV0aG9kfHwoRT1MKGMuY2FjaGUpP2MuY2FjaGU6TChiLmNhY2hlKT9iLmNhY2hlOnEpO0UmJihHPUUuZ2V0KFEpLHkoRyk/RyYmeihHLnRoZW4pP0cudGhlbihuLG4pOkgoRyk/bShHWzFdLEdbMF0sc2EoR1syXSksR1szXSk6bShHLDIwMCx7fSxcIk9LXCIpOkUucHV0KFEsTSkpO0QoRykmJigoRz1aYyhjLnVybCk/ZS5jb29raWVzKClbYy54c3JmQ29va2llTmFtZXx8Yi54c3JmQ29va2llTmFtZV06dSkmJihPW2MueHNyZkhlYWRlck5hbWV8fGIueHNyZkhlYWRlck5hbWVdPUcpLGQoYy5tZXRob2QsUSxmLGwsTyxjLnRpbWVvdXQsYy53aXRoQ3JlZGVudGlhbHMsYy5yZXNwb25zZVR5cGUpKTtyZXR1cm4gTX1mdW5jdGlvbiBwKGEsYil7aWYoIWIpcmV0dXJuIGE7XG52YXIgYz1bXTtFZChiLGZ1bmN0aW9uKGEsYil7bnVsbD09PWF8fEQoYSl8fChIKGEpfHwoYT1bYV0pLHIoYSxmdW5jdGlvbihhKXtMKGEpJiYoYT1oYShhKT9hLnRvSVNPU3RyaW5nKCk6YWIoYSkpO2MucHVzaChFYShiKStcIj1cIitFYShhKSl9KSl9KTswPGMubGVuZ3RoJiYoYSs9KC0xPT1hLmluZGV4T2YoXCI/XCIpP1wiP1wiOlwiJlwiKStjLmpvaW4oXCImXCIpKTtyZXR1cm4gYX12YXIgcT1mKFwiJGh0dHBcIiksdD1bXTtyKGMsZnVuY3Rpb24oYSl7dC51bnNoaWZ0KHgoYSk/bC5nZXQoYSk6bC5pbnZva2UoYSkpfSk7ay5wZW5kaW5nUmVxdWVzdHM9W107KGZ1bmN0aW9uKGEpe3IoYXJndW1lbnRzLGZ1bmN0aW9uKGEpe2tbYV09ZnVuY3Rpb24oYixjKXtyZXR1cm4gayh3KGN8fHt9LHttZXRob2Q6YSx1cmw6Yn0pKX19KX0pKFwiZ2V0XCIsXCJkZWxldGVcIixcImhlYWRcIixcImpzb25wXCIpOyhmdW5jdGlvbihhKXtyKGFyZ3VtZW50cyxmdW5jdGlvbihhKXtrW2FdPWZ1bmN0aW9uKGIsYyxkKXtyZXR1cm4gayh3KGR8fFxue30se21ldGhvZDphLHVybDpiLGRhdGE6Y30pKX19KX0pKFwicG9zdFwiLFwicHV0XCIsXCJwYXRjaFwiKTtrLmRlZmF1bHRzPWI7cmV0dXJuIGt9XX1mdW5jdGlvbiB2Zigpe3JldHVybiBuZXcgUi5YTUxIdHRwUmVxdWVzdH1mdW5jdGlvbiBMZSgpe3RoaXMuJGdldD1bXCIkYnJvd3NlclwiLFwiJHdpbmRvd1wiLFwiJGRvY3VtZW50XCIsZnVuY3Rpb24oYixhLGMpe3JldHVybiB3ZihiLHZmLGIuZGVmZXIsYS5hbmd1bGFyLmNhbGxiYWNrcyxjWzBdKX1dfWZ1bmN0aW9uIHdmKGIsYSxjLGQsZSl7ZnVuY3Rpb24gZihhLGIsYyl7dmFyIGY9ZS5jcmVhdGVFbGVtZW50KFwic2NyaXB0XCIpLG49bnVsbDtmLnR5cGU9XCJ0ZXh0L2phdmFzY3JpcHRcIjtmLnNyYz1hO2YuYXN5bmM9ITA7bj1mdW5jdGlvbihhKXtmLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJsb2FkXCIsbiwhMSk7Zi5yZW1vdmVFdmVudExpc3RlbmVyKFwiZXJyb3JcIixuLCExKTtlLmJvZHkucmVtb3ZlQ2hpbGQoZik7Zj1udWxsO3ZhciBnPS0xLHQ9XCJ1bmtub3duXCI7XG5hJiYoXCJsb2FkXCIhPT1hLnR5cGV8fGRbYl0uY2FsbGVkfHwoYT17dHlwZTpcImVycm9yXCJ9KSx0PWEudHlwZSxnPVwiZXJyb3JcIj09PWEudHlwZT80MDQ6MjAwKTtjJiZjKGcsdCl9O2YuYWRkRXZlbnRMaXN0ZW5lcihcImxvYWRcIixuLCExKTtmLmFkZEV2ZW50TGlzdGVuZXIoXCJlcnJvclwiLG4sITEpO2UuYm9keS5hcHBlbmRDaGlsZChmKTtyZXR1cm4gbn1yZXR1cm4gZnVuY3Rpb24oZSxoLGwsayxuLHAscSx0KXtmdW5jdGlvbiBzKCl7bSYmbSgpO0MmJkMuYWJvcnQoKX1mdW5jdGlvbiBGKGEsZCxlLGYsZyl7SSE9PXUmJmMuY2FuY2VsKEkpO209Qz1udWxsO2EoZCxlLGYsZyk7Yi4kJGNvbXBsZXRlT3V0c3RhbmRpbmdSZXF1ZXN0KEEpfWIuJCRpbmNPdXRzdGFuZGluZ1JlcXVlc3RDb3VudCgpO2g9aHx8Yi51cmwoKTtpZihcImpzb25wXCI9PUsoZSkpe3ZhciB2PVwiX1wiKyhkLmNvdW50ZXIrKykudG9TdHJpbmcoMzYpO2Rbdl09ZnVuY3Rpb24oYSl7ZFt2XS5kYXRhPWE7ZFt2XS5jYWxsZWQ9ITB9O1xudmFyIG09ZihoLnJlcGxhY2UoXCJKU09OX0NBTExCQUNLXCIsXCJhbmd1bGFyLmNhbGxiYWNrcy5cIit2KSx2LGZ1bmN0aW9uKGEsYil7RihrLGEsZFt2XS5kYXRhLFwiXCIsYik7ZFt2XT1BfSl9ZWxzZXt2YXIgQz1hKCk7Qy5vcGVuKGUsaCwhMCk7cihuLGZ1bmN0aW9uKGEsYil7eShhKSYmQy5zZXRSZXF1ZXN0SGVhZGVyKGIsYSl9KTtDLm9ubG9hZD1mdW5jdGlvbigpe3ZhciBhPUMuc3RhdHVzVGV4dHx8XCJcIixiPVwicmVzcG9uc2VcImluIEM/Qy5yZXNwb25zZTpDLnJlc3BvbnNlVGV4dCxjPTEyMjM9PT1DLnN0YXR1cz8yMDQ6Qy5zdGF0dXM7MD09PWMmJihjPWI/MjAwOlwiZmlsZVwiPT1BYShoKS5wcm90b2NvbD80MDQ6MCk7RihrLGMsYixDLmdldEFsbFJlc3BvbnNlSGVhZGVycygpLGEpfTtlPWZ1bmN0aW9uKCl7RihrLC0xLG51bGwsbnVsbCxcIlwiKX07Qy5vbmVycm9yPWU7Qy5vbmFib3J0PWU7cSYmKEMud2l0aENyZWRlbnRpYWxzPSEwKTtpZih0KXRyeXtDLnJlc3BvbnNlVHlwZT10fWNhdGNoKCQpe2lmKFwianNvblwiIT09XG50KXRocm93ICQ7fUMuc2VuZChsfHxudWxsKX1pZigwPHApdmFyIEk9YyhzLHApO2Vsc2UgcCYmeihwLnRoZW4pJiZwLnRoZW4ocyl9fWZ1bmN0aW9uIEllKCl7dmFyIGI9XCJ7e1wiLGE9XCJ9fVwiO3RoaXMuc3RhcnRTeW1ib2w9ZnVuY3Rpb24oYSl7cmV0dXJuIGE/KGI9YSx0aGlzKTpifTt0aGlzLmVuZFN5bWJvbD1mdW5jdGlvbihiKXtyZXR1cm4gYj8oYT1iLHRoaXMpOmF9O3RoaXMuJGdldD1bXCIkcGFyc2VcIixcIiRleGNlcHRpb25IYW5kbGVyXCIsXCIkc2NlXCIsZnVuY3Rpb24oYyxkLGUpe2Z1bmN0aW9uIGYoYSl7cmV0dXJuXCJcXFxcXFxcXFxcXFxcIithfWZ1bmN0aW9uIGcoZixnLHQscyl7ZnVuY3Rpb24gRihjKXtyZXR1cm4gYy5yZXBsYWNlKGssYikucmVwbGFjZShuLGEpfWZ1bmN0aW9uIHYoYSl7dHJ5e3ZhciBiPWE7YT10P2UuZ2V0VHJ1c3RlZCh0LGIpOmUudmFsdWVPZihiKTt2YXIgYztpZihzJiYheShhKSljPWE7ZWxzZSBpZihudWxsPT1hKWM9XCJcIjtlbHNle3N3aXRjaCh0eXBlb2YgYSl7Y2FzZSBcInN0cmluZ1wiOmJyZWFrO1xuY2FzZSBcIm51bWJlclwiOmE9XCJcIithO2JyZWFrO2RlZmF1bHQ6YT1hYihhKX1jPWF9cmV0dXJuIGN9Y2F0Y2goZyl7Yz1hYyhcImludGVyclwiLGYsZy50b1N0cmluZygpKSxkKGMpfX1zPSEhcztmb3IodmFyIG0sQyxyPTAsST1bXSxNPVtdLEU9Zi5sZW5ndGgsRz1bXSxPPVtdO3I8RTspaWYoLTEhPShtPWYuaW5kZXhPZihiLHIpKSYmLTEhPShDPWYuaW5kZXhPZihhLG0raCkpKXIhPT1tJiZHLnB1c2goRihmLnN1YnN0cmluZyhyLG0pKSkscj1mLnN1YnN0cmluZyhtK2gsQyksSS5wdXNoKHIpLE0ucHVzaChjKHIsdikpLHI9QytsLE8ucHVzaChHLmxlbmd0aCksRy5wdXNoKFwiXCIpO2Vsc2V7ciE9PUUmJkcucHVzaChGKGYuc3Vic3RyaW5nKHIpKSk7YnJlYWt9aWYodCYmMTxHLmxlbmd0aCl0aHJvdyBhYyhcIm5vY29uY2F0XCIsZik7aWYoIWd8fEkubGVuZ3RoKXt2YXIgUT1mdW5jdGlvbihhKXtmb3IodmFyIGI9MCxjPUkubGVuZ3RoO2I8YztiKyspe2lmKHMmJkQoYVtiXSkpcmV0dXJuO0dbT1tiXV09XG5hW2JdfXJldHVybiBHLmpvaW4oXCJcIil9O3JldHVybiB3KGZ1bmN0aW9uKGEpe3ZhciBiPTAsYz1JLmxlbmd0aCxlPUFycmF5KGMpO3RyeXtmb3IoO2I8YztiKyspZVtiXT1NW2JdKGEpO3JldHVybiBRKGUpfWNhdGNoKGcpe2E9YWMoXCJpbnRlcnJcIixmLGcudG9TdHJpbmcoKSksZChhKX19LHtleHA6ZixleHByZXNzaW9uczpJLCQkd2F0Y2hEZWxlZ2F0ZTpmdW5jdGlvbihhLGIsYyl7dmFyIGQ7cmV0dXJuIGEuJHdhdGNoR3JvdXAoTSxmdW5jdGlvbihjLGUpe3ZhciBmPVEoYyk7eihiKSYmYi5jYWxsKHRoaXMsZixjIT09ZT9kOmYsYSk7ZD1mfSxjKX19KX19dmFyIGg9Yi5sZW5ndGgsbD1hLmxlbmd0aCxrPW5ldyBSZWdFeHAoYi5yZXBsYWNlKC8uL2csZiksXCJnXCIpLG49bmV3IFJlZ0V4cChhLnJlcGxhY2UoLy4vZyxmKSxcImdcIik7Zy5zdGFydFN5bWJvbD1mdW5jdGlvbigpe3JldHVybiBifTtnLmVuZFN5bWJvbD1mdW5jdGlvbigpe3JldHVybiBhfTtyZXR1cm4gZ31dfWZ1bmN0aW9uIEplKCl7dGhpcy4kZ2V0PVxuW1wiJHJvb3RTY29wZVwiLFwiJHdpbmRvd1wiLFwiJHFcIixcIiQkcVwiLGZ1bmN0aW9uKGIsYSxjLGQpe2Z1bmN0aW9uIGUoZSxoLGwsayl7dmFyIG49YS5zZXRJbnRlcnZhbCxwPWEuY2xlYXJJbnRlcnZhbCxxPTAsdD15KGspJiYhayxzPSh0P2Q6YykuZGVmZXIoKSxGPXMucHJvbWlzZTtsPXkobCk/bDowO0YudGhlbihudWxsLG51bGwsZSk7Ri4kJGludGVydmFsSWQ9bihmdW5jdGlvbigpe3Mubm90aWZ5KHErKyk7MDxsJiZxPj1sJiYocy5yZXNvbHZlKHEpLHAoRi4kJGludGVydmFsSWQpLGRlbGV0ZSBmW0YuJCRpbnRlcnZhbElkXSk7dHx8Yi4kYXBwbHkoKX0saCk7ZltGLiQkaW50ZXJ2YWxJZF09cztyZXR1cm4gRn12YXIgZj17fTtlLmNhbmNlbD1mdW5jdGlvbihiKXtyZXR1cm4gYiYmYi4kJGludGVydmFsSWQgaW4gZj8oZltiLiQkaW50ZXJ2YWxJZF0ucmVqZWN0KFwiY2FuY2VsZWRcIiksYS5jbGVhckludGVydmFsKGIuJCRpbnRlcnZhbElkKSxkZWxldGUgZltiLiQkaW50ZXJ2YWxJZF0sITApOlxuITF9O3JldHVybiBlfV19ZnVuY3Rpb24gUmQoKXt0aGlzLiRnZXQ9ZnVuY3Rpb24oKXtyZXR1cm57aWQ6XCJlbi11c1wiLE5VTUJFUl9GT1JNQVRTOntERUNJTUFMX1NFUDpcIi5cIixHUk9VUF9TRVA6XCIsXCIsUEFUVEVSTlM6W3ttaW5JbnQ6MSxtaW5GcmFjOjAsbWF4RnJhYzozLHBvc1ByZTpcIlwiLHBvc1N1ZjpcIlwiLG5lZ1ByZTpcIi1cIixuZWdTdWY6XCJcIixnU2l6ZTozLGxnU2l6ZTozfSx7bWluSW50OjEsbWluRnJhYzoyLG1heEZyYWM6Mixwb3NQcmU6XCJcXHUwMGE0XCIscG9zU3VmOlwiXCIsbmVnUHJlOlwiKFxcdTAwYTRcIixuZWdTdWY6XCIpXCIsZ1NpemU6MyxsZ1NpemU6M31dLENVUlJFTkNZX1NZTTpcIiRcIn0sREFURVRJTUVfRk9STUFUUzp7TU9OVEg6XCJKYW51YXJ5IEZlYnJ1YXJ5IE1hcmNoIEFwcmlsIE1heSBKdW5lIEp1bHkgQXVndXN0IFNlcHRlbWJlciBPY3RvYmVyIE5vdmVtYmVyIERlY2VtYmVyXCIuc3BsaXQoXCIgXCIpLFNIT1JUTU9OVEg6XCJKYW4gRmViIE1hciBBcHIgTWF5IEp1biBKdWwgQXVnIFNlcCBPY3QgTm92IERlY1wiLnNwbGl0KFwiIFwiKSxcbkRBWTpcIlN1bmRheSBNb25kYXkgVHVlc2RheSBXZWRuZXNkYXkgVGh1cnNkYXkgRnJpZGF5IFNhdHVyZGF5XCIuc3BsaXQoXCIgXCIpLFNIT1JUREFZOlwiU3VuIE1vbiBUdWUgV2VkIFRodSBGcmkgU2F0XCIuc3BsaXQoXCIgXCIpLEFNUE1TOltcIkFNXCIsXCJQTVwiXSxtZWRpdW06XCJNTU0gZCwgeSBoOm1tOnNzIGFcIixcInNob3J0XCI6XCJNL2QveXkgaDptbSBhXCIsZnVsbERhdGU6XCJFRUVFLCBNTU1NIGQsIHlcIixsb25nRGF0ZTpcIk1NTU0gZCwgeVwiLG1lZGl1bURhdGU6XCJNTU0gZCwgeVwiLHNob3J0RGF0ZTpcIk0vZC95eVwiLG1lZGl1bVRpbWU6XCJoOm1tOnNzIGFcIixzaG9ydFRpbWU6XCJoOm1tIGFcIixFUkFOQU1FUzpbXCJCZWZvcmUgQ2hyaXN0XCIsXCJBbm5vIERvbWluaVwiXSxFUkFTOltcIkJDXCIsXCJBRFwiXX0scGx1cmFsQ2F0OmZ1bmN0aW9uKGIpe3JldHVybiAxPT09Yj9cIm9uZVwiOlwib3RoZXJcIn19fX1mdW5jdGlvbiBiYyhiKXtiPWIuc3BsaXQoXCIvXCIpO2Zvcih2YXIgYT1iLmxlbmd0aDthLS07KWJbYV09c2IoYlthXSk7XG5yZXR1cm4gYi5qb2luKFwiL1wiKX1mdW5jdGlvbiAkYyhiLGEpe3ZhciBjPUFhKGIpO2EuJCRwcm90b2NvbD1jLnByb3RvY29sO2EuJCRob3N0PWMuaG9zdG5hbWU7YS4kJHBvcnQ9YWEoYy5wb3J0KXx8eGZbYy5wcm90b2NvbF18fG51bGx9ZnVuY3Rpb24gYWQoYixhKXt2YXIgYz1cIi9cIiE9PWIuY2hhckF0KDApO2MmJihiPVwiL1wiK2IpO3ZhciBkPUFhKGIpO2EuJCRwYXRoPWRlY29kZVVSSUNvbXBvbmVudChjJiZcIi9cIj09PWQucGF0aG5hbWUuY2hhckF0KDApP2QucGF0aG5hbWUuc3Vic3RyaW5nKDEpOmQucGF0aG5hbWUpO2EuJCRzZWFyY2g9cmMoZC5zZWFyY2gpO2EuJCRoYXNoPWRlY29kZVVSSUNvbXBvbmVudChkLmhhc2gpO2EuJCRwYXRoJiZcIi9cIiE9YS4kJHBhdGguY2hhckF0KDApJiYoYS4kJHBhdGg9XCIvXCIrYS4kJHBhdGgpfWZ1bmN0aW9uIHVhKGIsYSl7aWYoMD09PWEuaW5kZXhPZihiKSlyZXR1cm4gYS5zdWJzdHIoYi5sZW5ndGgpfWZ1bmN0aW9uIEdhKGIpe3ZhciBhPWIuaW5kZXhPZihcIiNcIik7XG5yZXR1cm4tMT09YT9iOmIuc3Vic3RyKDAsYSl9ZnVuY3Rpb24gR2IoYil7cmV0dXJuIGIucmVwbGFjZSgvKCMuKyl8IyQvLFwiJDFcIil9ZnVuY3Rpb24gY2MoYixhLGMpe3RoaXMuJCRodG1sNT0hMDtjPWN8fFwiXCI7JGMoYix0aGlzKTt0aGlzLiQkcGFyc2U9ZnVuY3Rpb24oYil7dmFyIGM9dWEoYSxiKTtpZigheChjKSl0aHJvdyBIYihcImlwdGhwcmZ4XCIsYixhKTthZChjLHRoaXMpO3RoaXMuJCRwYXRofHwodGhpcy4kJHBhdGg9XCIvXCIpO3RoaXMuJCRjb21wb3NlKCl9O3RoaXMuJCRjb21wb3NlPWZ1bmN0aW9uKCl7dmFyIGI9UGIodGhpcy4kJHNlYXJjaCksYz10aGlzLiQkaGFzaD9cIiNcIitzYih0aGlzLiQkaGFzaCk6XCJcIjt0aGlzLiQkdXJsPWJjKHRoaXMuJCRwYXRoKSsoYj9cIj9cIitiOlwiXCIpK2M7dGhpcy4kJGFic1VybD1hK3RoaXMuJCR1cmwuc3Vic3RyKDEpfTt0aGlzLiQkcGFyc2VMaW5rVXJsPWZ1bmN0aW9uKGQsZSl7aWYoZSYmXCIjXCI9PT1lWzBdKXJldHVybiB0aGlzLmhhc2goZS5zbGljZSgxKSksXG4hMDt2YXIgZixnOyhmPXVhKGIsZCkpIT09dT8oZz1mLGc9KGY9dWEoYyxmKSkhPT11P2ErKHVhKFwiL1wiLGYpfHxmKTpiK2cpOihmPXVhKGEsZCkpIT09dT9nPWErZjphPT1kK1wiL1wiJiYoZz1hKTtnJiZ0aGlzLiQkcGFyc2UoZyk7cmV0dXJuISFnfX1mdW5jdGlvbiBkYyhiLGEsYyl7JGMoYix0aGlzKTt0aGlzLiQkcGFyc2U9ZnVuY3Rpb24oZCl7dmFyIGU9dWEoYixkKXx8dWEoYSxkKSxmO0QoZSl8fFwiI1wiIT09ZS5jaGFyQXQoMCk/dGhpcy4kJGh0bWw1P2Y9ZTooZj1cIlwiLEQoZSkmJihiPWQsdGhpcy5yZXBsYWNlKCkpKTooZj11YShjLGUpLEQoZikmJihmPWUpKTthZChmLHRoaXMpO2Q9dGhpcy4kJHBhdGg7dmFyIGU9YixnPS9eXFwvW0EtWl06KFxcLy4qKS87MD09PWYuaW5kZXhPZihlKSYmKGY9Zi5yZXBsYWNlKGUsXCJcIikpO2cuZXhlYyhmKXx8KGQ9KGY9Zy5leGVjKGQpKT9mWzFdOmQpO3RoaXMuJCRwYXRoPWQ7dGhpcy4kJGNvbXBvc2UoKX07dGhpcy4kJGNvbXBvc2U9ZnVuY3Rpb24oKXt2YXIgYT1cblBiKHRoaXMuJCRzZWFyY2gpLGU9dGhpcy4kJGhhc2g/XCIjXCIrc2IodGhpcy4kJGhhc2gpOlwiXCI7dGhpcy4kJHVybD1iYyh0aGlzLiQkcGF0aCkrKGE/XCI/XCIrYTpcIlwiKStlO3RoaXMuJCRhYnNVcmw9YisodGhpcy4kJHVybD9jK3RoaXMuJCR1cmw6XCJcIil9O3RoaXMuJCRwYXJzZUxpbmtVcmw9ZnVuY3Rpb24oYSxjKXtyZXR1cm4gR2EoYik9PUdhKGEpPyh0aGlzLiQkcGFyc2UoYSksITApOiExfX1mdW5jdGlvbiBiZChiLGEsYyl7dGhpcy4kJGh0bWw1PSEwO2RjLmFwcGx5KHRoaXMsYXJndW1lbnRzKTt0aGlzLiQkcGFyc2VMaW5rVXJsPWZ1bmN0aW9uKGQsZSl7aWYoZSYmXCIjXCI9PT1lWzBdKXJldHVybiB0aGlzLmhhc2goZS5zbGljZSgxKSksITA7dmFyIGYsZztiPT1HYShkKT9mPWQ6KGc9dWEoYSxkKSk/Zj1iK2MrZzphPT09ZCtcIi9cIiYmKGY9YSk7ZiYmdGhpcy4kJHBhcnNlKGYpO3JldHVybiEhZn07dGhpcy4kJGNvbXBvc2U9ZnVuY3Rpb24oKXt2YXIgYT1QYih0aGlzLiQkc2VhcmNoKSxcbmU9dGhpcy4kJGhhc2g/XCIjXCIrc2IodGhpcy4kJGhhc2gpOlwiXCI7dGhpcy4kJHVybD1iYyh0aGlzLiQkcGF0aCkrKGE/XCI/XCIrYTpcIlwiKStlO3RoaXMuJCRhYnNVcmw9YitjK3RoaXMuJCR1cmx9fWZ1bmN0aW9uIEliKGIpe3JldHVybiBmdW5jdGlvbigpe3JldHVybiB0aGlzW2JdfX1mdW5jdGlvbiBjZChiLGEpe3JldHVybiBmdW5jdGlvbihjKXtpZihEKGMpKXJldHVybiB0aGlzW2JdO3RoaXNbYl09YShjKTt0aGlzLiQkY29tcG9zZSgpO3JldHVybiB0aGlzfX1mdW5jdGlvbiBNZSgpe3ZhciBiPVwiXCIsYT17ZW5hYmxlZDohMSxyZXF1aXJlQmFzZTohMCxyZXdyaXRlTGlua3M6ITB9O3RoaXMuaGFzaFByZWZpeD1mdW5jdGlvbihhKXtyZXR1cm4geShhKT8oYj1hLHRoaXMpOmJ9O3RoaXMuaHRtbDVNb2RlPWZ1bmN0aW9uKGIpe3JldHVybiBYYShiKT8oYS5lbmFibGVkPWIsdGhpcyk6TChiKT8oWGEoYi5lbmFibGVkKSYmKGEuZW5hYmxlZD1iLmVuYWJsZWQpLFhhKGIucmVxdWlyZUJhc2UpJiZcbihhLnJlcXVpcmVCYXNlPWIucmVxdWlyZUJhc2UpLFhhKGIucmV3cml0ZUxpbmtzKSYmKGEucmV3cml0ZUxpbmtzPWIucmV3cml0ZUxpbmtzKSx0aGlzKTphfTt0aGlzLiRnZXQ9W1wiJHJvb3RTY29wZVwiLFwiJGJyb3dzZXJcIixcIiRzbmlmZmVyXCIsXCIkcm9vdEVsZW1lbnRcIixcIiR3aW5kb3dcIixmdW5jdGlvbihjLGQsZSxmLGcpe2Z1bmN0aW9uIGgoYSxiLGMpe3ZhciBlPWsudXJsKCksZj1rLiQkc3RhdGU7dHJ5e2QudXJsKGEsYixjKSxrLiQkc3RhdGU9ZC5zdGF0ZSgpfWNhdGNoKGcpe3Rocm93IGsudXJsKGUpLGsuJCRzdGF0ZT1mLGc7fX1mdW5jdGlvbiBsKGEsYil7Yy4kYnJvYWRjYXN0KFwiJGxvY2F0aW9uQ2hhbmdlU3VjY2Vzc1wiLGsuYWJzVXJsKCksYSxrLiQkc3RhdGUsYil9dmFyIGssbjtuPWQuYmFzZUhyZWYoKTt2YXIgcD1kLnVybCgpLHE7aWYoYS5lbmFibGVkKXtpZighbiYmYS5yZXF1aXJlQmFzZSl0aHJvdyBIYihcIm5vYmFzZVwiKTtxPXAuc3Vic3RyaW5nKDAscC5pbmRleE9mKFwiL1wiLFxucC5pbmRleE9mKFwiLy9cIikrMikpKyhufHxcIi9cIik7bj1lLmhpc3Rvcnk/Y2M6YmR9ZWxzZSBxPUdhKHApLG49ZGM7dmFyIHQ9cS5zdWJzdHIoMCxHYShxKS5sYXN0SW5kZXhPZihcIi9cIikrMSk7az1uZXcgbihxLHQsXCIjXCIrYik7ay4kJHBhcnNlTGlua1VybChwLHApO2suJCRzdGF0ZT1kLnN0YXRlKCk7dmFyIHM9L15cXHMqKGphdmFzY3JpcHR8bWFpbHRvKTovaTtmLm9uKFwiY2xpY2tcIixmdW5jdGlvbihiKXtpZihhLnJld3JpdGVMaW5rcyYmIWIuY3RybEtleSYmIWIubWV0YUtleSYmIWIuc2hpZnRLZXkmJjIhPWIud2hpY2gmJjIhPWIuYnV0dG9uKXtmb3IodmFyIGU9QihiLnRhcmdldCk7XCJhXCIhPT13YShlWzBdKTspaWYoZVswXT09PWZbMF18fCEoZT1lLnBhcmVudCgpKVswXSlyZXR1cm47dmFyIGw9ZS5wcm9wKFwiaHJlZlwiKSxoPWUuYXR0cihcImhyZWZcIil8fGUuYXR0cihcInhsaW5rOmhyZWZcIik7TChsKSYmXCJbb2JqZWN0IFNWR0FuaW1hdGVkU3RyaW5nXVwiPT09bC50b1N0cmluZygpJiYobD1cbkFhKGwuYW5pbVZhbCkuaHJlZik7cy50ZXN0KGwpfHwhbHx8ZS5hdHRyKFwidGFyZ2V0XCIpfHxiLmlzRGVmYXVsdFByZXZlbnRlZCgpfHwhay4kJHBhcnNlTGlua1VybChsLGgpfHwoYi5wcmV2ZW50RGVmYXVsdCgpLGsuYWJzVXJsKCkhPWQudXJsKCkmJihjLiRhcHBseSgpLGcuYW5ndWxhcltcImZmLTY4NDIwOC1wcmV2ZW50RGVmYXVsdFwiXT0hMCkpfX0pO0diKGsuYWJzVXJsKCkpIT1HYihwKSYmZC51cmwoay5hYnNVcmwoKSwhMCk7dmFyIEY9ITA7ZC5vblVybENoYW5nZShmdW5jdGlvbihhLGIpe0QodWEodCxhKSk/Zy5sb2NhdGlvbi5ocmVmPWE6KGMuJGV2YWxBc3luYyhmdW5jdGlvbigpe3ZhciBkPWsuYWJzVXJsKCksZT1rLiQkc3RhdGUsZjtrLiQkcGFyc2UoYSk7ay4kJHN0YXRlPWI7Zj1jLiRicm9hZGNhc3QoXCIkbG9jYXRpb25DaGFuZ2VTdGFydFwiLGEsZCxiLGUpLmRlZmF1bHRQcmV2ZW50ZWQ7ay5hYnNVcmwoKT09PWEmJihmPyhrLiQkcGFyc2UoZCksay4kJHN0YXRlPWUsaChkLFxuITEsZSkpOihGPSExLGwoZCxlKSkpfSksYy4kJHBoYXNlfHxjLiRkaWdlc3QoKSl9KTtjLiR3YXRjaChmdW5jdGlvbigpe3ZhciBhPUdiKGQudXJsKCkpLGI9R2Ioay5hYnNVcmwoKSksZj1kLnN0YXRlKCksZz1rLiQkcmVwbGFjZSxxPWEhPT1ifHxrLiQkaHRtbDUmJmUuaGlzdG9yeSYmZiE9PWsuJCRzdGF0ZTtpZihGfHxxKUY9ITEsYy4kZXZhbEFzeW5jKGZ1bmN0aW9uKCl7dmFyIGI9ay5hYnNVcmwoKSxkPWMuJGJyb2FkY2FzdChcIiRsb2NhdGlvbkNoYW5nZVN0YXJ0XCIsYixhLGsuJCRzdGF0ZSxmKS5kZWZhdWx0UHJldmVudGVkO2suYWJzVXJsKCk9PT1iJiYoZD8oay4kJHBhcnNlKGEpLGsuJCRzdGF0ZT1mKToocSYmaChiLGcsZj09PWsuJCRzdGF0ZT9udWxsOmsuJCRzdGF0ZSksbChhLGYpKSl9KTtrLiQkcmVwbGFjZT0hMX0pO3JldHVybiBrfV19ZnVuY3Rpb24gTmUoKXt2YXIgYj0hMCxhPXRoaXM7dGhpcy5kZWJ1Z0VuYWJsZWQ9ZnVuY3Rpb24oYSl7cmV0dXJuIHkoYSk/KGI9XG5hLHRoaXMpOmJ9O3RoaXMuJGdldD1bXCIkd2luZG93XCIsZnVuY3Rpb24oYyl7ZnVuY3Rpb24gZChhKXthIGluc3RhbmNlb2YgRXJyb3ImJihhLnN0YWNrP2E9YS5tZXNzYWdlJiYtMT09PWEuc3RhY2suaW5kZXhPZihhLm1lc3NhZ2UpP1wiRXJyb3I6IFwiK2EubWVzc2FnZStcIlxcblwiK2Euc3RhY2s6YS5zdGFjazphLnNvdXJjZVVSTCYmKGE9YS5tZXNzYWdlK1wiXFxuXCIrYS5zb3VyY2VVUkwrXCI6XCIrYS5saW5lKSk7cmV0dXJuIGF9ZnVuY3Rpb24gZShhKXt2YXIgYj1jLmNvbnNvbGV8fHt9LGU9YlthXXx8Yi5sb2d8fEE7YT0hMTt0cnl7YT0hIWUuYXBwbHl9Y2F0Y2gobCl7fXJldHVybiBhP2Z1bmN0aW9uKCl7dmFyIGE9W107cihhcmd1bWVudHMsZnVuY3Rpb24oYil7YS5wdXNoKGQoYikpfSk7cmV0dXJuIGUuYXBwbHkoYixhKX06ZnVuY3Rpb24oYSxiKXtlKGEsbnVsbD09Yj9cIlwiOmIpfX1yZXR1cm57bG9nOmUoXCJsb2dcIiksaW5mbzplKFwiaW5mb1wiKSx3YXJuOmUoXCJ3YXJuXCIpLGVycm9yOmUoXCJlcnJvclwiKSxcbmRlYnVnOmZ1bmN0aW9uKCl7dmFyIGM9ZShcImRlYnVnXCIpO3JldHVybiBmdW5jdGlvbigpe2ImJmMuYXBwbHkoYSxhcmd1bWVudHMpfX0oKX19XX1mdW5jdGlvbiB2YShiLGEpe2lmKFwiX19kZWZpbmVHZXR0ZXJfX1wiPT09Ynx8XCJfX2RlZmluZVNldHRlcl9fXCI9PT1ifHxcIl9fbG9va3VwR2V0dGVyX19cIj09PWJ8fFwiX19sb29rdXBTZXR0ZXJfX1wiPT09Ynx8XCJfX3Byb3RvX19cIj09PWIpdGhyb3cgZ2EoXCJpc2VjZmxkXCIsYSk7cmV0dXJuIGJ9ZnVuY3Rpb24gZGQoYixhKXtiKz1cIlwiO2lmKCF4KGIpKXRocm93IGdhKFwiaXNlY2NzdFwiLGEpO3JldHVybiBifWZ1bmN0aW9uIG9hKGIsYSl7aWYoYil7aWYoYi5jb25zdHJ1Y3Rvcj09PWIpdGhyb3cgZ2EoXCJpc2VjZm5cIixhKTtpZihiLndpbmRvdz09PWIpdGhyb3cgZ2EoXCJpc2Vjd2luZG93XCIsYSk7aWYoYi5jaGlsZHJlbiYmKGIubm9kZU5hbWV8fGIucHJvcCYmYi5hdHRyJiZiLmZpbmQpKXRocm93IGdhKFwiaXNlY2RvbVwiLGEpO2lmKGI9PT1PYmplY3QpdGhyb3cgZ2EoXCJpc2Vjb2JqXCIsXG5hKTt9cmV0dXJuIGJ9ZnVuY3Rpb24gZWMoYil7cmV0dXJuIGIuY29uc3RhbnR9ZnVuY3Rpb24gamIoYixhLGMsZCxlKXtvYShiLGUpO29hKGEsZSk7Yz1jLnNwbGl0KFwiLlwiKTtmb3IodmFyIGYsZz0wOzE8Yy5sZW5ndGg7ZysrKXtmPXZhKGMuc2hpZnQoKSxlKTt2YXIgaD0wPT09ZyYmYSYmYVtmXXx8YltmXTtofHwoaD17fSxiW2ZdPWgpO2I9b2EoaCxlKX1mPXZhKGMuc2hpZnQoKSxlKTtvYShiW2ZdLGUpO3JldHVybiBiW2ZdPWR9ZnVuY3Rpb24gUWEoYil7cmV0dXJuXCJjb25zdHJ1Y3RvclwiPT1ifWZ1bmN0aW9uIGVkKGIsYSxjLGQsZSxmLGcpe3ZhKGIsZik7dmEoYSxmKTt2YShjLGYpO3ZhKGQsZik7dmEoZSxmKTt2YXIgaD1mdW5jdGlvbihhKXtyZXR1cm4gb2EoYSxmKX0sbD1nfHxRYShiKT9oOnJhLGs9Z3x8UWEoYSk/aDpyYSxuPWd8fFFhKGMpP2g6cmEscD1nfHxRYShkKT9oOnJhLHE9Z3x8UWEoZSk/aDpyYTtyZXR1cm4gZnVuY3Rpb24oZixnKXt2YXIgaD1nJiZnLmhhc093blByb3BlcnR5KGIpP1xuZzpmO2lmKG51bGw9PWgpcmV0dXJuIGg7aD1sKGhbYl0pO2lmKCFhKXJldHVybiBoO2lmKG51bGw9PWgpcmV0dXJuIHU7aD1rKGhbYV0pO2lmKCFjKXJldHVybiBoO2lmKG51bGw9PWgpcmV0dXJuIHU7aD1uKGhbY10pO2lmKCFkKXJldHVybiBoO2lmKG51bGw9PWgpcmV0dXJuIHU7aD1wKGhbZF0pO3JldHVybiBlP251bGw9PWg/dTpoPXEoaFtlXSk6aH19ZnVuY3Rpb24geWYoYixhKXtyZXR1cm4gZnVuY3Rpb24oYyxkKXtyZXR1cm4gYihjLGQsb2EsYSl9fWZ1bmN0aW9uIHpmKGIsYSxjKXt2YXIgZD1hLmV4cGVuc2l2ZUNoZWNrcyxlPWQ/QWY6QmYsZj1lW2JdO2lmKGYpcmV0dXJuIGY7dmFyIGc9Yi5zcGxpdChcIi5cIiksaD1nLmxlbmd0aDtpZihhLmNzcClmPTY+aD9lZChnWzBdLGdbMV0sZ1syXSxnWzNdLGdbNF0sYyxkKTpmdW5jdGlvbihhLGIpe3ZhciBlPTAsZjtkbyBmPWVkKGdbZSsrXSxnW2UrK10sZ1tlKytdLGdbZSsrXSxnW2UrK10sYyxkKShhLGIpLGI9dSxhPWY7d2hpbGUoZTxcbmgpO3JldHVybiBmfTtlbHNle3ZhciBsPVwiXCI7ZCYmKGwrPVwicyA9IGVzbyhzLCBmZSk7XFxubCA9IGVzbyhsLCBmZSk7XFxuXCIpO3ZhciBrPWQ7cihnLGZ1bmN0aW9uKGEsYil7dmEoYSxjKTt2YXIgZT0oYj9cInNcIjonKChsJiZsLmhhc093blByb3BlcnR5KFwiJythKydcIikpP2w6cyknKStcIi5cIithO2lmKGR8fFFhKGEpKWU9XCJlc28oXCIrZStcIiwgZmUpXCIsaz0hMDtsKz1cImlmKHMgPT0gbnVsbCkgcmV0dXJuIHVuZGVmaW5lZDtcXG5zPVwiK2UrXCI7XFxuXCJ9KTtsKz1cInJldHVybiBzO1wiO2E9bmV3IEZ1bmN0aW9uKFwic1wiLFwibFwiLFwiZXNvXCIsXCJmZVwiLGwpO2EudG9TdHJpbmc9ZWEobCk7ayYmKGE9eWYoYSxjKSk7Zj1hfWYuc2hhcmVkR2V0dGVyPSEwO2YuYXNzaWduPWZ1bmN0aW9uKGEsYyxkKXtyZXR1cm4gamIoYSxkLGIsYyxiKX07cmV0dXJuIGVbYl09Zn1mdW5jdGlvbiBmYyhiKXtyZXR1cm4geihiLnZhbHVlT2YpP2IudmFsdWVPZigpOkNmLmNhbGwoYil9ZnVuY3Rpb24gT2UoKXt2YXIgYj1qYSgpLFxuYT1qYSgpO3RoaXMuJGdldD1bXCIkZmlsdGVyXCIsXCIkc25pZmZlclwiLGZ1bmN0aW9uKGMsZCl7ZnVuY3Rpb24gZShhKXt2YXIgYj1hO2Euc2hhcmVkR2V0dGVyJiYoYj1mdW5jdGlvbihiLGMpe3JldHVybiBhKGIsYyl9LGIubGl0ZXJhbD1hLmxpdGVyYWwsYi5jb25zdGFudD1hLmNvbnN0YW50LGIuYXNzaWduPWEuYXNzaWduKTtyZXR1cm4gYn1mdW5jdGlvbiBmKGEsYil7Zm9yKHZhciBjPTAsZD1hLmxlbmd0aDtjPGQ7YysrKXt2YXIgZT1hW2NdO2UuY29uc3RhbnR8fChlLmlucHV0cz9mKGUuaW5wdXRzLGIpOi0xPT09Yi5pbmRleE9mKGUpJiZiLnB1c2goZSkpfXJldHVybiBifWZ1bmN0aW9uIGcoYSxiKXtyZXR1cm4gbnVsbD09YXx8bnVsbD09Yj9hPT09YjpcIm9iamVjdFwiPT09dHlwZW9mIGEmJihhPWZjKGEpLFwib2JqZWN0XCI9PT10eXBlb2YgYSk/ITE6YT09PWJ8fGEhPT1hJiZiIT09Yn1mdW5jdGlvbiBoKGEsYixjLGQpe3ZhciBlPWQuJCRpbnB1dHN8fChkLiQkaW5wdXRzPWYoZC5pbnB1dHMsXG5bXSkpLGw7aWYoMT09PWUubGVuZ3RoKXt2YXIgaD1nLGU9ZVswXTtyZXR1cm4gYS4kd2F0Y2goZnVuY3Rpb24oYSl7dmFyIGI9ZShhKTtnKGIsaCl8fChsPWQoYSksaD1iJiZmYyhiKSk7cmV0dXJuIGx9LGIsYyl9Zm9yKHZhciBrPVtdLHE9MCxwPWUubGVuZ3RoO3E8cDtxKyspa1txXT1nO3JldHVybiBhLiR3YXRjaChmdW5jdGlvbihhKXtmb3IodmFyIGI9ITEsYz0wLGY9ZS5sZW5ndGg7YzxmO2MrKyl7dmFyIGg9ZVtjXShhKTtpZihifHwoYj0hZyhoLGtbY10pKSlrW2NdPWgmJmZjKGgpfWImJihsPWQoYSkpO3JldHVybiBsfSxiLGMpfWZ1bmN0aW9uIGwoYSxiLGMsZCl7dmFyIGUsZjtyZXR1cm4gZT1hLiR3YXRjaChmdW5jdGlvbihhKXtyZXR1cm4gZChhKX0sZnVuY3Rpb24oYSxjLGQpe2Y9YTt6KGIpJiZiLmFwcGx5KHRoaXMsYXJndW1lbnRzKTt5KGEpJiZkLiQkcG9zdERpZ2VzdChmdW5jdGlvbigpe3koZikmJmUoKX0pfSxjKX1mdW5jdGlvbiBrKGEsYixjLGQpe2Z1bmN0aW9uIGUoYSl7dmFyIGI9XG4hMDtyKGEsZnVuY3Rpb24oYSl7eShhKXx8KGI9ITEpfSk7cmV0dXJuIGJ9dmFyIGYsZztyZXR1cm4gZj1hLiR3YXRjaChmdW5jdGlvbihhKXtyZXR1cm4gZChhKX0sZnVuY3Rpb24oYSxjLGQpe2c9YTt6KGIpJiZiLmNhbGwodGhpcyxhLGMsZCk7ZShhKSYmZC4kJHBvc3REaWdlc3QoZnVuY3Rpb24oKXtlKGcpJiZmKCl9KX0sYyl9ZnVuY3Rpb24gbihhLGIsYyxkKXt2YXIgZTtyZXR1cm4gZT1hLiR3YXRjaChmdW5jdGlvbihhKXtyZXR1cm4gZChhKX0sZnVuY3Rpb24oYSxjLGQpe3ooYikmJmIuYXBwbHkodGhpcyxhcmd1bWVudHMpO2UoKX0sYyl9ZnVuY3Rpb24gcChhLGIpe2lmKCFiKXJldHVybiBhO3ZhciBjPWEuJCR3YXRjaERlbGVnYXRlLGM9YyE9PWsmJmMhPT1sP2Z1bmN0aW9uKGMsZCl7dmFyIGU9YShjLGQpO3JldHVybiBiKGUsYyxkKX06ZnVuY3Rpb24oYyxkKXt2YXIgZT1hKGMsZCksZj1iKGUsYyxkKTtyZXR1cm4geShlKT9mOmV9O2EuJCR3YXRjaERlbGVnYXRlJiZhLiQkd2F0Y2hEZWxlZ2F0ZSE9PVxuaD9jLiQkd2F0Y2hEZWxlZ2F0ZT1hLiQkd2F0Y2hEZWxlZ2F0ZTpiLiRzdGF0ZWZ1bHx8KGMuJCR3YXRjaERlbGVnYXRlPWgsYy5pbnB1dHM9W2FdKTtyZXR1cm4gY312YXIgcT17Y3NwOmQuY3NwLGV4cGVuc2l2ZUNoZWNrczohMX0sdD17Y3NwOmQuY3NwLGV4cGVuc2l2ZUNoZWNrczohMH07cmV0dXJuIGZ1bmN0aW9uKGQsZixnKXt2YXIgbSxyLHU7c3dpdGNoKHR5cGVvZiBkKXtjYXNlIFwic3RyaW5nXCI6dT1kPWQudHJpbSgpO3ZhciBJPWc/YTpiO209SVt1XTttfHwoXCI6XCI9PT1kLmNoYXJBdCgwKSYmXCI6XCI9PT1kLmNoYXJBdCgxKSYmKHI9ITAsZD1kLnN1YnN0cmluZygyKSksZz1nP3Q6cSxtPW5ldyBnYyhnKSxtPShuZXcga2IobSxjLGcpKS5wYXJzZShkKSxtLmNvbnN0YW50P20uJCR3YXRjaERlbGVnYXRlPW46cj8obT1lKG0pLG0uJCR3YXRjaERlbGVnYXRlPW0ubGl0ZXJhbD9rOmwpOm0uaW5wdXRzJiYobS4kJHdhdGNoRGVsZWdhdGU9aCksSVt1XT1tKTtyZXR1cm4gcChtLGYpO1xuY2FzZSBcImZ1bmN0aW9uXCI6cmV0dXJuIHAoZCxmKTtkZWZhdWx0OnJldHVybiBwKEEsZil9fX1dfWZ1bmN0aW9uIFFlKCl7dGhpcy4kZ2V0PVtcIiRyb290U2NvcGVcIixcIiRleGNlcHRpb25IYW5kbGVyXCIsZnVuY3Rpb24oYixhKXtyZXR1cm4gZmQoZnVuY3Rpb24oYSl7Yi4kZXZhbEFzeW5jKGEpfSxhKX1dfWZ1bmN0aW9uIFJlKCl7dGhpcy4kZ2V0PVtcIiRicm93c2VyXCIsXCIkZXhjZXB0aW9uSGFuZGxlclwiLGZ1bmN0aW9uKGIsYSl7cmV0dXJuIGZkKGZ1bmN0aW9uKGEpe2IuZGVmZXIoYSl9LGEpfV19ZnVuY3Rpb24gZmQoYixhKXtmdW5jdGlvbiBjKGEsYixjKXtmdW5jdGlvbiBkKGIpe3JldHVybiBmdW5jdGlvbihjKXtlfHwoZT0hMCxiLmNhbGwoYSxjKSl9fXZhciBlPSExO3JldHVybltkKGIpLGQoYyldfWZ1bmN0aW9uIGQoKXt0aGlzLiQkc3RhdGU9e3N0YXR1czowfX1mdW5jdGlvbiBlKGEsYil7cmV0dXJuIGZ1bmN0aW9uKGMpe2IuY2FsbChhLGMpfX1mdW5jdGlvbiBmKGMpeyFjLnByb2Nlc3NTY2hlZHVsZWQmJlxuYy5wZW5kaW5nJiYoYy5wcm9jZXNzU2NoZWR1bGVkPSEwLGIoZnVuY3Rpb24oKXt2YXIgYixkLGU7ZT1jLnBlbmRpbmc7Yy5wcm9jZXNzU2NoZWR1bGVkPSExO2MucGVuZGluZz11O2Zvcih2YXIgZj0wLGc9ZS5sZW5ndGg7ZjxnOysrZil7ZD1lW2ZdWzBdO2I9ZVtmXVtjLnN0YXR1c107dHJ5e3ooYik/ZC5yZXNvbHZlKGIoYy52YWx1ZSkpOjE9PT1jLnN0YXR1cz9kLnJlc29sdmUoYy52YWx1ZSk6ZC5yZWplY3QoYy52YWx1ZSl9Y2F0Y2gobCl7ZC5yZWplY3QobCksYShsKX19fSkpfWZ1bmN0aW9uIGcoKXt0aGlzLnByb21pc2U9bmV3IGQ7dGhpcy5yZXNvbHZlPWUodGhpcyx0aGlzLnJlc29sdmUpO3RoaXMucmVqZWN0PWUodGhpcyx0aGlzLnJlamVjdCk7dGhpcy5ub3RpZnk9ZSh0aGlzLHRoaXMubm90aWZ5KX12YXIgaD1TKFwiJHFcIixUeXBlRXJyb3IpO2QucHJvdG90eXBlPXt0aGVuOmZ1bmN0aW9uKGEsYixjKXt2YXIgZD1uZXcgZzt0aGlzLiQkc3RhdGUucGVuZGluZz10aGlzLiQkc3RhdGUucGVuZGluZ3x8XG5bXTt0aGlzLiQkc3RhdGUucGVuZGluZy5wdXNoKFtkLGEsYixjXSk7MDx0aGlzLiQkc3RhdGUuc3RhdHVzJiZmKHRoaXMuJCRzdGF0ZSk7cmV0dXJuIGQucHJvbWlzZX0sXCJjYXRjaFwiOmZ1bmN0aW9uKGEpe3JldHVybiB0aGlzLnRoZW4obnVsbCxhKX0sXCJmaW5hbGx5XCI6ZnVuY3Rpb24oYSxiKXtyZXR1cm4gdGhpcy50aGVuKGZ1bmN0aW9uKGIpe3JldHVybiBrKGIsITAsYSl9LGZ1bmN0aW9uKGIpe3JldHVybiBrKGIsITEsYSl9LGIpfX07Zy5wcm90b3R5cGU9e3Jlc29sdmU6ZnVuY3Rpb24oYSl7dGhpcy5wcm9taXNlLiQkc3RhdGUuc3RhdHVzfHwoYT09PXRoaXMucHJvbWlzZT90aGlzLiQkcmVqZWN0KGgoXCJxY3ljbGVcIixhKSk6dGhpcy4kJHJlc29sdmUoYSkpfSwkJHJlc29sdmU6ZnVuY3Rpb24oYil7dmFyIGQsZTtlPWModGhpcyx0aGlzLiQkcmVzb2x2ZSx0aGlzLiQkcmVqZWN0KTt0cnl7aWYoTChiKXx8eihiKSlkPWImJmIudGhlbjt6KGQpPyh0aGlzLnByb21pc2UuJCRzdGF0ZS5zdGF0dXM9XG4tMSxkLmNhbGwoYixlWzBdLGVbMV0sdGhpcy5ub3RpZnkpKToodGhpcy5wcm9taXNlLiQkc3RhdGUudmFsdWU9Yix0aGlzLnByb21pc2UuJCRzdGF0ZS5zdGF0dXM9MSxmKHRoaXMucHJvbWlzZS4kJHN0YXRlKSl9Y2F0Y2goZyl7ZVsxXShnKSxhKGcpfX0scmVqZWN0OmZ1bmN0aW9uKGEpe3RoaXMucHJvbWlzZS4kJHN0YXRlLnN0YXR1c3x8dGhpcy4kJHJlamVjdChhKX0sJCRyZWplY3Q6ZnVuY3Rpb24oYSl7dGhpcy5wcm9taXNlLiQkc3RhdGUudmFsdWU9YTt0aGlzLnByb21pc2UuJCRzdGF0ZS5zdGF0dXM9MjtmKHRoaXMucHJvbWlzZS4kJHN0YXRlKX0sbm90aWZ5OmZ1bmN0aW9uKGMpe3ZhciBkPXRoaXMucHJvbWlzZS4kJHN0YXRlLnBlbmRpbmc7MD49dGhpcy5wcm9taXNlLiQkc3RhdGUuc3RhdHVzJiZkJiZkLmxlbmd0aCYmYihmdW5jdGlvbigpe2Zvcih2YXIgYixlLGY9MCxnPWQubGVuZ3RoO2Y8ZztmKyspe2U9ZFtmXVswXTtiPWRbZl1bM107dHJ5e2Uubm90aWZ5KHooYik/XG5iKGMpOmMpfWNhdGNoKGwpe2EobCl9fX0pfX07dmFyIGw9ZnVuY3Rpb24oYSxiKXt2YXIgYz1uZXcgZztiP2MucmVzb2x2ZShhKTpjLnJlamVjdChhKTtyZXR1cm4gYy5wcm9taXNlfSxrPWZ1bmN0aW9uKGEsYixjKXt2YXIgZD1udWxsO3RyeXt6KGMpJiYoZD1jKCkpfWNhdGNoKGUpe3JldHVybiBsKGUsITEpfXJldHVybiBkJiZ6KGQudGhlbik/ZC50aGVuKGZ1bmN0aW9uKCl7cmV0dXJuIGwoYSxiKX0sZnVuY3Rpb24oYSl7cmV0dXJuIGwoYSwhMSl9KTpsKGEsYil9LG49ZnVuY3Rpb24oYSxiLGMsZCl7dmFyIGU9bmV3IGc7ZS5yZXNvbHZlKGEpO3JldHVybiBlLnByb21pc2UudGhlbihiLGMsZCl9LHA9ZnVuY3Rpb24gdChhKXtpZigheihhKSl0aHJvdyBoKFwibm9yc2x2clwiLGEpO2lmKCEodGhpcyBpbnN0YW5jZW9mIHQpKXJldHVybiBuZXcgdChhKTt2YXIgYj1uZXcgZzthKGZ1bmN0aW9uKGEpe2IucmVzb2x2ZShhKX0sZnVuY3Rpb24oYSl7Yi5yZWplY3QoYSl9KTtyZXR1cm4gYi5wcm9taXNlfTtcbnAuZGVmZXI9ZnVuY3Rpb24oKXtyZXR1cm4gbmV3IGd9O3AucmVqZWN0PWZ1bmN0aW9uKGEpe3ZhciBiPW5ldyBnO2IucmVqZWN0KGEpO3JldHVybiBiLnByb21pc2V9O3Aud2hlbj1uO3AuYWxsPWZ1bmN0aW9uKGEpe3ZhciBiPW5ldyBnLGM9MCxkPUgoYSk/W106e307cihhLGZ1bmN0aW9uKGEsZSl7YysrO24oYSkudGhlbihmdW5jdGlvbihhKXtkLmhhc093blByb3BlcnR5KGUpfHwoZFtlXT1hLC0tY3x8Yi5yZXNvbHZlKGQpKX0sZnVuY3Rpb24oYSl7ZC5oYXNPd25Qcm9wZXJ0eShlKXx8Yi5yZWplY3QoYSl9KX0pOzA9PT1jJiZiLnJlc29sdmUoZCk7cmV0dXJuIGIucHJvbWlzZX07cmV0dXJuIHB9ZnVuY3Rpb24gJGUoKXt0aGlzLiRnZXQ9W1wiJHdpbmRvd1wiLFwiJHRpbWVvdXRcIixmdW5jdGlvbihiLGEpe2Z1bmN0aW9uIGMoKXtmb3IodmFyIGE9MDthPG4ubGVuZ3RoO2ErKyl7dmFyIGI9blthXTtiJiYoblthXT1udWxsLGIoKSl9az1uLmxlbmd0aD0wfWZ1bmN0aW9uIGQoYSl7dmFyIGI9XG5uLmxlbmd0aDtrKys7bi5wdXNoKGEpOzA9PT1iJiYobD1oKGMpKTtyZXR1cm4gZnVuY3Rpb24oKXswPD1iJiYoYj1uW2JdPW51bGwsMD09PS0tayYmbCYmKGwoKSxsPW51bGwsbi5sZW5ndGg9MCkpfX12YXIgZT1iLnJlcXVlc3RBbmltYXRpb25GcmFtZXx8Yi53ZWJraXRSZXF1ZXN0QW5pbWF0aW9uRnJhbWUsZj1iLmNhbmNlbEFuaW1hdGlvbkZyYW1lfHxiLndlYmtpdENhbmNlbEFuaW1hdGlvbkZyYW1lfHxiLndlYmtpdENhbmNlbFJlcXVlc3RBbmltYXRpb25GcmFtZSxnPSEhZSxoPWc/ZnVuY3Rpb24oYSl7dmFyIGI9ZShhKTtyZXR1cm4gZnVuY3Rpb24oKXtmKGIpfX06ZnVuY3Rpb24oYil7dmFyIGM9YShiLDE2LjY2LCExKTtyZXR1cm4gZnVuY3Rpb24oKXthLmNhbmNlbChjKX19O2Quc3VwcG9ydGVkPWc7dmFyIGwsaz0wLG49W107cmV0dXJuIGR9XX1mdW5jdGlvbiBQZSgpe2Z1bmN0aW9uIGIoYSl7ZnVuY3Rpb24gYigpe3RoaXMuJCR3YXRjaGVycz10aGlzLiQkbmV4dFNpYmxpbmc9XG50aGlzLiQkY2hpbGRIZWFkPXRoaXMuJCRjaGlsZFRhaWw9bnVsbDt0aGlzLiQkbGlzdGVuZXJzPXt9O3RoaXMuJCRsaXN0ZW5lckNvdW50PXt9O3RoaXMuJGlkPSsrcmI7dGhpcy4kJENoaWxkU2NvcGU9bnVsbH1iLnByb3RvdHlwZT1hO3JldHVybiBifXZhciBhPTEwLGM9UyhcIiRyb290U2NvcGVcIiksZD1udWxsLGU9bnVsbDt0aGlzLmRpZ2VzdFR0bD1mdW5jdGlvbihiKXthcmd1bWVudHMubGVuZ3RoJiYoYT1iKTtyZXR1cm4gYX07dGhpcy4kZ2V0PVtcIiRpbmplY3RvclwiLFwiJGV4Y2VwdGlvbkhhbmRsZXJcIixcIiRwYXJzZVwiLFwiJGJyb3dzZXJcIixmdW5jdGlvbihmLGcsaCxsKXtmdW5jdGlvbiBrKGEpe2EuY3VycmVudFNjb3BlLiQkZGVzdHJveWVkPSEwfWZ1bmN0aW9uIG4oKXt0aGlzLiRpZD0rK3JiO3RoaXMuJCRwaGFzZT10aGlzLiRwYXJlbnQ9dGhpcy4kJHdhdGNoZXJzPXRoaXMuJCRuZXh0U2libGluZz10aGlzLiQkcHJldlNpYmxpbmc9dGhpcy4kJGNoaWxkSGVhZD10aGlzLiQkY2hpbGRUYWlsPVxubnVsbDt0aGlzLiRyb290PXRoaXM7dGhpcy4kJGRlc3Ryb3llZD0hMTt0aGlzLiQkbGlzdGVuZXJzPXt9O3RoaXMuJCRsaXN0ZW5lckNvdW50PXt9O3RoaXMuJCRpc29sYXRlQmluZGluZ3M9bnVsbH1mdW5jdGlvbiBwKGEpe2lmKHYuJCRwaGFzZSl0aHJvdyBjKFwiaW5wcm9nXCIsdi4kJHBoYXNlKTt2LiQkcGhhc2U9YX1mdW5jdGlvbiBxKGEsYixjKXtkbyBhLiQkbGlzdGVuZXJDb3VudFtjXS09YiwwPT09YS4kJGxpc3RlbmVyQ291bnRbY10mJmRlbGV0ZSBhLiQkbGlzdGVuZXJDb3VudFtjXTt3aGlsZShhPWEuJHBhcmVudCl9ZnVuY3Rpb24gdCgpe31mdW5jdGlvbiBzKCl7Zm9yKDt1Lmxlbmd0aDspdHJ5e3Uuc2hpZnQoKSgpfWNhdGNoKGEpe2coYSl9ZT1udWxsfWZ1bmN0aW9uIEYoKXtudWxsPT09ZSYmKGU9bC5kZWZlcihmdW5jdGlvbigpe3YuJGFwcGx5KHMpfSkpfW4ucHJvdG90eXBlPXtjb25zdHJ1Y3RvcjpuLCRuZXc6ZnVuY3Rpb24oYSxjKXt2YXIgZDtjPWN8fHRoaXM7YT9cbihkPW5ldyBuLGQuJHJvb3Q9dGhpcy4kcm9vdCk6KHRoaXMuJCRDaGlsZFNjb3BlfHwodGhpcy4kJENoaWxkU2NvcGU9Yih0aGlzKSksZD1uZXcgdGhpcy4kJENoaWxkU2NvcGUpO2QuJHBhcmVudD1jO2QuJCRwcmV2U2libGluZz1jLiQkY2hpbGRUYWlsO2MuJCRjaGlsZEhlYWQ/KGMuJCRjaGlsZFRhaWwuJCRuZXh0U2libGluZz1kLGMuJCRjaGlsZFRhaWw9ZCk6Yy4kJGNoaWxkSGVhZD1jLiQkY2hpbGRUYWlsPWQ7KGF8fGMhPXRoaXMpJiZkLiRvbihcIiRkZXN0cm95XCIsayk7cmV0dXJuIGR9LCR3YXRjaDpmdW5jdGlvbihhLGIsYyl7dmFyIGU9aChhKTtpZihlLiQkd2F0Y2hEZWxlZ2F0ZSlyZXR1cm4gZS4kJHdhdGNoRGVsZWdhdGUodGhpcyxiLGMsZSk7dmFyIGY9dGhpcy4kJHdhdGNoZXJzLGc9e2ZuOmIsbGFzdDp0LGdldDplLGV4cDphLGVxOiEhY307ZD1udWxsO3ooYil8fChnLmZuPUEpO2Z8fChmPXRoaXMuJCR3YXRjaGVycz1bXSk7Zi51bnNoaWZ0KGcpO3JldHVybiBmdW5jdGlvbigpe1lhKGYsXG5nKTtkPW51bGx9fSwkd2F0Y2hHcm91cDpmdW5jdGlvbihhLGIpe2Z1bmN0aW9uIGMoKXtsPSExO2g/KGg9ITEsYihlLGUsZykpOmIoZSxkLGcpfXZhciBkPUFycmF5KGEubGVuZ3RoKSxlPUFycmF5KGEubGVuZ3RoKSxmPVtdLGc9dGhpcyxsPSExLGg9ITA7aWYoIWEubGVuZ3RoKXt2YXIgaz0hMDtnLiRldmFsQXN5bmMoZnVuY3Rpb24oKXtrJiZiKGUsZSxnKX0pO3JldHVybiBmdW5jdGlvbigpe2s9ITF9fWlmKDE9PT1hLmxlbmd0aClyZXR1cm4gdGhpcy4kd2F0Y2goYVswXSxmdW5jdGlvbihhLGMsZil7ZVswXT1hO2RbMF09YztiKGUsYT09PWM/ZTpkLGYpfSk7cihhLGZ1bmN0aW9uKGEsYil7dmFyIGg9Zy4kd2F0Y2goYSxmdW5jdGlvbihhLGYpe2VbYl09YTtkW2JdPWY7bHx8KGw9ITAsZy4kZXZhbEFzeW5jKGMpKX0pO2YucHVzaChoKX0pO3JldHVybiBmdW5jdGlvbigpe2Zvcig7Zi5sZW5ndGg7KWYuc2hpZnQoKSgpfX0sJHdhdGNoQ29sbGVjdGlvbjpmdW5jdGlvbihhLGIpe2Z1bmN0aW9uIGMoYSl7ZT1cbmE7dmFyIGIsZCxnLGw7aWYoIUQoZSkpe2lmKEwoZSkpaWYoVGEoZSkpZm9yKGYhPT1wJiYoZj1wLHQ9Zi5sZW5ndGg9MCxrKyspLGE9ZS5sZW5ndGgsdCE9PWEmJihrKyssZi5sZW5ndGg9dD1hKSxiPTA7YjxhO2IrKylsPWZbYl0sZz1lW2JdLGQ9bCE9PWwmJmchPT1nLGR8fGw9PT1nfHwoaysrLGZbYl09Zyk7ZWxzZXtmIT09biYmKGY9bj17fSx0PTAsaysrKTthPTA7Zm9yKGIgaW4gZSllLmhhc093blByb3BlcnR5KGIpJiYoYSsrLGc9ZVtiXSxsPWZbYl0sYiBpbiBmPyhkPWwhPT1sJiZnIT09ZyxkfHxsPT09Z3x8KGsrKyxmW2JdPWcpKToodCsrLGZbYl09ZyxrKyspKTtpZih0PmEpZm9yKGIgaW4gaysrLGYpZS5oYXNPd25Qcm9wZXJ0eShiKXx8KHQtLSxkZWxldGUgZltiXSl9ZWxzZSBmIT09ZSYmKGY9ZSxrKyspO3JldHVybiBrfX1jLiRzdGF0ZWZ1bD0hMDt2YXIgZD10aGlzLGUsZixnLGw9MTxiLmxlbmd0aCxrPTAscT1oKGEsYykscD1bXSxuPXt9LG09ITAsdD0wO3JldHVybiB0aGlzLiR3YXRjaChxLFxuZnVuY3Rpb24oKXttPyhtPSExLGIoZSxlLGQpKTpiKGUsZyxkKTtpZihsKWlmKEwoZSkpaWYoVGEoZSkpe2c9QXJyYXkoZS5sZW5ndGgpO2Zvcih2YXIgYT0wO2E8ZS5sZW5ndGg7YSsrKWdbYV09ZVthXX1lbHNlIGZvcihhIGluIGc9e30sZSlzYy5jYWxsKGUsYSkmJihnW2FdPWVbYV0pO2Vsc2UgZz1lfSl9LCRkaWdlc3Q6ZnVuY3Rpb24oKXt2YXIgYixmLGgsayxxLG4scj1hLEYsUD1bXSx1LHk7cChcIiRkaWdlc3RcIik7bC4kJGNoZWNrVXJsQ2hhbmdlKCk7dGhpcz09PXYmJm51bGwhPT1lJiYobC5kZWZlci5jYW5jZWwoZSkscygpKTtkPW51bGw7ZG97bj0hMTtmb3IoRj10aGlzO20ubGVuZ3RoOyl7dHJ5e3k9bS5zaGlmdCgpLHkuc2NvcGUuJGV2YWwoeS5leHByZXNzaW9uLHkubG9jYWxzKX1jYXRjaCh3KXtnKHcpfWQ9bnVsbH1hOmRve2lmKGs9Ri4kJHdhdGNoZXJzKWZvcihxPWsubGVuZ3RoO3EtLTspdHJ5e2lmKGI9a1txXSlpZigoZj1iLmdldChGKSkhPT0oaD1iLmxhc3QpJiZcbiEoYi5lcT9pYShmLGgpOlwibnVtYmVyXCI9PT10eXBlb2YgZiYmXCJudW1iZXJcIj09PXR5cGVvZiBoJiZpc05hTihmKSYmaXNOYU4oaCkpKW49ITAsZD1iLGIubGFzdD1iLmVxP0RhKGYsbnVsbCk6ZixiLmZuKGYsaD09PXQ/ZjpoLEYpLDU+ciYmKHU9NC1yLFBbdV18fChQW3VdPVtdKSxQW3VdLnB1c2goe21zZzp6KGIuZXhwKT9cImZuOiBcIisoYi5leHAubmFtZXx8Yi5leHAudG9TdHJpbmcoKSk6Yi5leHAsbmV3VmFsOmYsb2xkVmFsOmh9KSk7ZWxzZSBpZihiPT09ZCl7bj0hMTticmVhayBhfX1jYXRjaChEKXtnKEQpfWlmKCEoaz1GLiQkY2hpbGRIZWFkfHxGIT09dGhpcyYmRi4kJG5leHRTaWJsaW5nKSlmb3IoO0YhPT10aGlzJiYhKGs9Ri4kJG5leHRTaWJsaW5nKTspRj1GLiRwYXJlbnR9d2hpbGUoRj1rKTtpZigobnx8bS5sZW5ndGgpJiYhci0tKXRocm93IHYuJCRwaGFzZT1udWxsLGMoXCJpbmZkaWdcIixhLFApO313aGlsZShufHxtLmxlbmd0aCk7Zm9yKHYuJCRwaGFzZT1udWxsO0MubGVuZ3RoOyl0cnl7Qy5zaGlmdCgpKCl9Y2F0Y2goQil7ZyhCKX19LFxuJGRlc3Ryb3k6ZnVuY3Rpb24oKXtpZighdGhpcy4kJGRlc3Ryb3llZCl7dmFyIGE9dGhpcy4kcGFyZW50O3RoaXMuJGJyb2FkY2FzdChcIiRkZXN0cm95XCIpO3RoaXMuJCRkZXN0cm95ZWQ9ITA7aWYodGhpcyE9PXYpe2Zvcih2YXIgYiBpbiB0aGlzLiQkbGlzdGVuZXJDb3VudClxKHRoaXMsdGhpcy4kJGxpc3RlbmVyQ291bnRbYl0sYik7YS4kJGNoaWxkSGVhZD09dGhpcyYmKGEuJCRjaGlsZEhlYWQ9dGhpcy4kJG5leHRTaWJsaW5nKTthLiQkY2hpbGRUYWlsPT10aGlzJiYoYS4kJGNoaWxkVGFpbD10aGlzLiQkcHJldlNpYmxpbmcpO3RoaXMuJCRwcmV2U2libGluZyYmKHRoaXMuJCRwcmV2U2libGluZy4kJG5leHRTaWJsaW5nPXRoaXMuJCRuZXh0U2libGluZyk7dGhpcy4kJG5leHRTaWJsaW5nJiYodGhpcy4kJG5leHRTaWJsaW5nLiQkcHJldlNpYmxpbmc9dGhpcy4kJHByZXZTaWJsaW5nKTt0aGlzLiRkZXN0cm95PXRoaXMuJGRpZ2VzdD10aGlzLiRhcHBseT10aGlzLiRldmFsQXN5bmM9XG50aGlzLiRhcHBseUFzeW5jPUE7dGhpcy4kb249dGhpcy4kd2F0Y2g9dGhpcy4kd2F0Y2hHcm91cD1mdW5jdGlvbigpe3JldHVybiBBfTt0aGlzLiQkbGlzdGVuZXJzPXt9O3RoaXMuJHBhcmVudD10aGlzLiQkbmV4dFNpYmxpbmc9dGhpcy4kJHByZXZTaWJsaW5nPXRoaXMuJCRjaGlsZEhlYWQ9dGhpcy4kJGNoaWxkVGFpbD10aGlzLiRyb290PXRoaXMuJCR3YXRjaGVycz1udWxsfX19LCRldmFsOmZ1bmN0aW9uKGEsYil7cmV0dXJuIGgoYSkodGhpcyxiKX0sJGV2YWxBc3luYzpmdW5jdGlvbihhLGIpe3YuJCRwaGFzZXx8bS5sZW5ndGh8fGwuZGVmZXIoZnVuY3Rpb24oKXttLmxlbmd0aCYmdi4kZGlnZXN0KCl9KTttLnB1c2goe3Njb3BlOnRoaXMsZXhwcmVzc2lvbjphLGxvY2FsczpifSl9LCQkcG9zdERpZ2VzdDpmdW5jdGlvbihhKXtDLnB1c2goYSl9LCRhcHBseTpmdW5jdGlvbihhKXt0cnl7cmV0dXJuIHAoXCIkYXBwbHlcIiksdGhpcy4kZXZhbChhKX1jYXRjaChiKXtnKGIpfWZpbmFsbHl7di4kJHBoYXNlPVxubnVsbDt0cnl7di4kZGlnZXN0KCl9Y2F0Y2goYyl7dGhyb3cgZyhjKSxjO319fSwkYXBwbHlBc3luYzpmdW5jdGlvbihhKXtmdW5jdGlvbiBiKCl7Yy4kZXZhbChhKX12YXIgYz10aGlzO2EmJnUucHVzaChiKTtGKCl9LCRvbjpmdW5jdGlvbihhLGIpe3ZhciBjPXRoaXMuJCRsaXN0ZW5lcnNbYV07Y3x8KHRoaXMuJCRsaXN0ZW5lcnNbYV09Yz1bXSk7Yy5wdXNoKGIpO3ZhciBkPXRoaXM7ZG8gZC4kJGxpc3RlbmVyQ291bnRbYV18fChkLiQkbGlzdGVuZXJDb3VudFthXT0wKSxkLiQkbGlzdGVuZXJDb3VudFthXSsrO3doaWxlKGQ9ZC4kcGFyZW50KTt2YXIgZT10aGlzO3JldHVybiBmdW5jdGlvbigpe3ZhciBkPWMuaW5kZXhPZihiKTstMSE9PWQmJihjW2RdPW51bGwscShlLDEsYSkpfX0sJGVtaXQ6ZnVuY3Rpb24oYSxiKXt2YXIgYz1bXSxkLGU9dGhpcyxmPSExLGw9e25hbWU6YSx0YXJnZXRTY29wZTplLHN0b3BQcm9wYWdhdGlvbjpmdW5jdGlvbigpe2Y9ITB9LHByZXZlbnREZWZhdWx0OmZ1bmN0aW9uKCl7bC5kZWZhdWx0UHJldmVudGVkPVxuITB9LGRlZmF1bHRQcmV2ZW50ZWQ6ITF9LGg9WmEoW2xdLGFyZ3VtZW50cywxKSxrLHE7ZG97ZD1lLiQkbGlzdGVuZXJzW2FdfHxjO2wuY3VycmVudFNjb3BlPWU7az0wO2ZvcihxPWQubGVuZ3RoO2s8cTtrKyspaWYoZFtrXSl0cnl7ZFtrXS5hcHBseShudWxsLGgpfWNhdGNoKHApe2cocCl9ZWxzZSBkLnNwbGljZShrLDEpLGstLSxxLS07aWYoZilyZXR1cm4gbC5jdXJyZW50U2NvcGU9bnVsbCxsO2U9ZS4kcGFyZW50fXdoaWxlKGUpO2wuY3VycmVudFNjb3BlPW51bGw7cmV0dXJuIGx9LCRicm9hZGNhc3Q6ZnVuY3Rpb24oYSxiKXt2YXIgYz10aGlzLGQ9dGhpcyxlPXtuYW1lOmEsdGFyZ2V0U2NvcGU6dGhpcyxwcmV2ZW50RGVmYXVsdDpmdW5jdGlvbigpe2UuZGVmYXVsdFByZXZlbnRlZD0hMH0sZGVmYXVsdFByZXZlbnRlZDohMX07aWYoIXRoaXMuJCRsaXN0ZW5lckNvdW50W2FdKXJldHVybiBlO2Zvcih2YXIgZj1aYShbZV0sYXJndW1lbnRzLDEpLGwsaDtjPWQ7KXtlLmN1cnJlbnRTY29wZT1cbmM7ZD1jLiQkbGlzdGVuZXJzW2FdfHxbXTtsPTA7Zm9yKGg9ZC5sZW5ndGg7bDxoO2wrKylpZihkW2xdKXRyeXtkW2xdLmFwcGx5KG51bGwsZil9Y2F0Y2goayl7ZyhrKX1lbHNlIGQuc3BsaWNlKGwsMSksbC0tLGgtLTtpZighKGQ9Yy4kJGxpc3RlbmVyQ291bnRbYV0mJmMuJCRjaGlsZEhlYWR8fGMhPT10aGlzJiZjLiQkbmV4dFNpYmxpbmcpKWZvcig7YyE9PXRoaXMmJiEoZD1jLiQkbmV4dFNpYmxpbmcpOyljPWMuJHBhcmVudH1lLmN1cnJlbnRTY29wZT1udWxsO3JldHVybiBlfX07dmFyIHY9bmV3IG4sbT12LiQkYXN5bmNRdWV1ZT1bXSxDPXYuJCRwb3N0RGlnZXN0UXVldWU9W10sdT12LiQkYXBwbHlBc3luY1F1ZXVlPVtdO3JldHVybiB2fV19ZnVuY3Rpb24gU2QoKXt2YXIgYj0vXlxccyooaHR0cHM/fGZ0cHxtYWlsdG98dGVsfGZpbGUpOi8sYT0vXlxccyooKGh0dHBzP3xmdHB8ZmlsZXxibG9iKTp8ZGF0YTppbWFnZVxcLykvO3RoaXMuYUhyZWZTYW5pdGl6YXRpb25XaGl0ZWxpc3Q9XG5mdW5jdGlvbihhKXtyZXR1cm4geShhKT8oYj1hLHRoaXMpOmJ9O3RoaXMuaW1nU3JjU2FuaXRpemF0aW9uV2hpdGVsaXN0PWZ1bmN0aW9uKGIpe3JldHVybiB5KGIpPyhhPWIsdGhpcyk6YX07dGhpcy4kZ2V0PWZ1bmN0aW9uKCl7cmV0dXJuIGZ1bmN0aW9uKGMsZCl7dmFyIGU9ZD9hOmIsZjtmPUFhKGMpLmhyZWY7cmV0dXJuXCJcIj09PWZ8fGYubWF0Y2goZSk/YzpcInVuc2FmZTpcIitmfX19ZnVuY3Rpb24gRGYoYil7aWYoXCJzZWxmXCI9PT1iKXJldHVybiBiO2lmKHgoYikpe2lmKC0xPGIuaW5kZXhPZihcIioqKlwiKSl0aHJvdyBCYShcIml3Y2FyZFwiLGIpO2I9Z2QoYikucmVwbGFjZShcIlxcXFwqXFxcXCpcIixcIi4qXCIpLnJlcGxhY2UoXCJcXFxcKlwiLFwiW146Ly4/JjtdKlwiKTtyZXR1cm4gbmV3IFJlZ0V4cChcIl5cIitiK1wiJFwiKX1pZihWYShiKSlyZXR1cm4gbmV3IFJlZ0V4cChcIl5cIitiLnNvdXJjZStcIiRcIik7dGhyb3cgQmEoXCJpbWF0Y2hlclwiKTt9ZnVuY3Rpb24gaGQoYil7dmFyIGE9W107eShiKSYmcihiLGZ1bmN0aW9uKGIpe2EucHVzaChEZihiKSl9KTtcbnJldHVybiBhfWZ1bmN0aW9uIFRlKCl7dGhpcy5TQ0VfQ09OVEVYVFM9cGE7dmFyIGI9W1wic2VsZlwiXSxhPVtdO3RoaXMucmVzb3VyY2VVcmxXaGl0ZWxpc3Q9ZnVuY3Rpb24oYSl7YXJndW1lbnRzLmxlbmd0aCYmKGI9aGQoYSkpO3JldHVybiBifTt0aGlzLnJlc291cmNlVXJsQmxhY2tsaXN0PWZ1bmN0aW9uKGIpe2FyZ3VtZW50cy5sZW5ndGgmJihhPWhkKGIpKTtyZXR1cm4gYX07dGhpcy4kZ2V0PVtcIiRpbmplY3RvclwiLGZ1bmN0aW9uKGMpe2Z1bmN0aW9uIGQoYSxiKXtyZXR1cm5cInNlbGZcIj09PWE/WmMoYik6ISFhLmV4ZWMoYi5ocmVmKX1mdW5jdGlvbiBlKGEpe3ZhciBiPWZ1bmN0aW9uKGEpe3RoaXMuJCR1bndyYXBUcnVzdGVkVmFsdWU9ZnVuY3Rpb24oKXtyZXR1cm4gYX19O2EmJihiLnByb3RvdHlwZT1uZXcgYSk7Yi5wcm90b3R5cGUudmFsdWVPZj1mdW5jdGlvbigpe3JldHVybiB0aGlzLiQkdW53cmFwVHJ1c3RlZFZhbHVlKCl9O2IucHJvdG90eXBlLnRvU3RyaW5nPWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuJCR1bndyYXBUcnVzdGVkVmFsdWUoKS50b1N0cmluZygpfTtcbnJldHVybiBifXZhciBmPWZ1bmN0aW9uKGEpe3Rocm93IEJhKFwidW5zYWZlXCIpO307Yy5oYXMoXCIkc2FuaXRpemVcIikmJihmPWMuZ2V0KFwiJHNhbml0aXplXCIpKTt2YXIgZz1lKCksaD17fTtoW3BhLkhUTUxdPWUoZyk7aFtwYS5DU1NdPWUoZyk7aFtwYS5VUkxdPWUoZyk7aFtwYS5KU109ZShnKTtoW3BhLlJFU09VUkNFX1VSTF09ZShoW3BhLlVSTF0pO3JldHVybnt0cnVzdEFzOmZ1bmN0aW9uKGEsYil7dmFyIGM9aC5oYXNPd25Qcm9wZXJ0eShhKT9oW2FdOm51bGw7aWYoIWMpdGhyb3cgQmEoXCJpY29udGV4dFwiLGEsYik7aWYobnVsbD09PWJ8fGI9PT11fHxcIlwiPT09YilyZXR1cm4gYjtpZihcInN0cmluZ1wiIT09dHlwZW9mIGIpdGhyb3cgQmEoXCJpdHlwZVwiLGEpO3JldHVybiBuZXcgYyhiKX0sZ2V0VHJ1c3RlZDpmdW5jdGlvbihjLGUpe2lmKG51bGw9PT1lfHxlPT09dXx8XCJcIj09PWUpcmV0dXJuIGU7dmFyIGc9aC5oYXNPd25Qcm9wZXJ0eShjKT9oW2NdOm51bGw7aWYoZyYmZSBpbnN0YW5jZW9mXG5nKXJldHVybiBlLiQkdW53cmFwVHJ1c3RlZFZhbHVlKCk7aWYoYz09PXBhLlJFU09VUkNFX1VSTCl7dmFyIGc9QWEoZS50b1N0cmluZygpKSxwLHEsdD0hMTtwPTA7Zm9yKHE9Yi5sZW5ndGg7cDxxO3ArKylpZihkKGJbcF0sZykpe3Q9ITA7YnJlYWt9aWYodClmb3IocD0wLHE9YS5sZW5ndGg7cDxxO3ArKylpZihkKGFbcF0sZykpe3Q9ITE7YnJlYWt9aWYodClyZXR1cm4gZTt0aHJvdyBCYShcImluc2VjdXJsXCIsZS50b1N0cmluZygpKTt9aWYoYz09PXBhLkhUTUwpcmV0dXJuIGYoZSk7dGhyb3cgQmEoXCJ1bnNhZmVcIik7fSx2YWx1ZU9mOmZ1bmN0aW9uKGEpe3JldHVybiBhIGluc3RhbmNlb2YgZz9hLiQkdW53cmFwVHJ1c3RlZFZhbHVlKCk6YX19fV19ZnVuY3Rpb24gU2UoKXt2YXIgYj0hMDt0aGlzLmVuYWJsZWQ9ZnVuY3Rpb24oYSl7YXJndW1lbnRzLmxlbmd0aCYmKGI9ISFhKTtyZXR1cm4gYn07dGhpcy4kZ2V0PVtcIiRwYXJzZVwiLFwiJHNjZURlbGVnYXRlXCIsZnVuY3Rpb24oYSxjKXtpZihiJiZcbjg+UmEpdGhyb3cgQmEoXCJpZXF1aXJrc1wiKTt2YXIgZD1zYShwYSk7ZC5pc0VuYWJsZWQ9ZnVuY3Rpb24oKXtyZXR1cm4gYn07ZC50cnVzdEFzPWMudHJ1c3RBcztkLmdldFRydXN0ZWQ9Yy5nZXRUcnVzdGVkO2QudmFsdWVPZj1jLnZhbHVlT2Y7Ynx8KGQudHJ1c3RBcz1kLmdldFRydXN0ZWQ9ZnVuY3Rpb24oYSxiKXtyZXR1cm4gYn0sZC52YWx1ZU9mPXJhKTtkLnBhcnNlQXM9ZnVuY3Rpb24oYixjKXt2YXIgZT1hKGMpO3JldHVybiBlLmxpdGVyYWwmJmUuY29uc3RhbnQ/ZTphKGMsZnVuY3Rpb24oYSl7cmV0dXJuIGQuZ2V0VHJ1c3RlZChiLGEpfSl9O3ZhciBlPWQucGFyc2VBcyxmPWQuZ2V0VHJ1c3RlZCxnPWQudHJ1c3RBcztyKHBhLGZ1bmN0aW9uKGEsYil7dmFyIGM9SyhiKTtkW2ZiKFwicGFyc2VfYXNfXCIrYyldPWZ1bmN0aW9uKGIpe3JldHVybiBlKGEsYil9O2RbZmIoXCJnZXRfdHJ1c3RlZF9cIitjKV09ZnVuY3Rpb24oYil7cmV0dXJuIGYoYSxiKX07ZFtmYihcInRydXN0X2FzX1wiK1xuYyldPWZ1bmN0aW9uKGIpe3JldHVybiBnKGEsYil9fSk7cmV0dXJuIGR9XX1mdW5jdGlvbiBVZSgpe3RoaXMuJGdldD1bXCIkd2luZG93XCIsXCIkZG9jdW1lbnRcIixmdW5jdGlvbihiLGEpe3ZhciBjPXt9LGQ9YWEoKC9hbmRyb2lkIChcXGQrKS8uZXhlYyhLKChiLm5hdmlnYXRvcnx8e30pLnVzZXJBZ2VudCkpfHxbXSlbMV0pLGU9L0JveGVlL2kudGVzdCgoYi5uYXZpZ2F0b3J8fHt9KS51c2VyQWdlbnQpLGY9YVswXXx8e30sZyxoPS9eKE1venx3ZWJraXR8bXMpKD89W0EtWl0pLyxsPWYuYm9keSYmZi5ib2R5LnN0eWxlLGs9ITEsbj0hMTtpZihsKXtmb3IodmFyIHAgaW4gbClpZihrPWguZXhlYyhwKSl7Zz1rWzBdO2c9Zy5zdWJzdHIoMCwxKS50b1VwcGVyQ2FzZSgpK2cuc3Vic3RyKDEpO2JyZWFrfWd8fChnPVwiV2Via2l0T3BhY2l0eVwiaW4gbCYmXCJ3ZWJraXRcIik7az0hIShcInRyYW5zaXRpb25cImluIGx8fGcrXCJUcmFuc2l0aW9uXCJpbiBsKTtuPSEhKFwiYW5pbWF0aW9uXCJpbiBsfHxnK1wiQW5pbWF0aW9uXCJpblxubCk7IWR8fGsmJm58fChrPXgoZi5ib2R5LnN0eWxlLndlYmtpdFRyYW5zaXRpb24pLG49eChmLmJvZHkuc3R5bGUud2Via2l0QW5pbWF0aW9uKSl9cmV0dXJue2hpc3Rvcnk6ISghYi5oaXN0b3J5fHwhYi5oaXN0b3J5LnB1c2hTdGF0ZXx8ND5kfHxlKSxoYXNFdmVudDpmdW5jdGlvbihhKXtpZihcImlucHV0XCI9PT1hJiYxMT49UmEpcmV0dXJuITE7aWYoRChjW2FdKSl7dmFyIGI9Zi5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO2NbYV09XCJvblwiK2EgaW4gYn1yZXR1cm4gY1thXX0sY3NwOmRiKCksdmVuZG9yUHJlZml4OmcsdHJhbnNpdGlvbnM6ayxhbmltYXRpb25zOm4sYW5kcm9pZDpkfX1dfWZ1bmN0aW9uIFdlKCl7dGhpcy4kZ2V0PVtcIiR0ZW1wbGF0ZUNhY2hlXCIsXCIkaHR0cFwiLFwiJHFcIixcIiRzY2VcIixmdW5jdGlvbihiLGEsYyxkKXtmdW5jdGlvbiBlKGYsZyl7ZS50b3RhbFBlbmRpbmdSZXF1ZXN0cysrO3goZikmJmIuZ2V0KGYpfHwoZj1kLmdldFRydXN0ZWRSZXNvdXJjZVVybChmKSk7dmFyIGg9XG5hLmRlZmF1bHRzJiZhLmRlZmF1bHRzLnRyYW5zZm9ybVJlc3BvbnNlO0goaCk/aD1oLmZpbHRlcihmdW5jdGlvbihhKXtyZXR1cm4gYSE9PVpifSk6aD09PVpiJiYoaD1udWxsKTtyZXR1cm4gYS5nZXQoZix7Y2FjaGU6Yix0cmFuc2Zvcm1SZXNwb25zZTpofSlbXCJmaW5hbGx5XCJdKGZ1bmN0aW9uKCl7ZS50b3RhbFBlbmRpbmdSZXF1ZXN0cy0tfSkudGhlbihmdW5jdGlvbihhKXtyZXR1cm4gYS5kYXRhfSxmdW5jdGlvbihhKXtpZighZyl0aHJvdyBtYShcInRwbG9hZFwiLGYpO3JldHVybiBjLnJlamVjdChhKX0pfWUudG90YWxQZW5kaW5nUmVxdWVzdHM9MDtyZXR1cm4gZX1dfWZ1bmN0aW9uIFhlKCl7dGhpcy4kZ2V0PVtcIiRyb290U2NvcGVcIixcIiRicm93c2VyXCIsXCIkbG9jYXRpb25cIixmdW5jdGlvbihiLGEsYyl7cmV0dXJue2ZpbmRCaW5kaW5nczpmdW5jdGlvbihhLGIsYyl7YT1hLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJuZy1iaW5kaW5nXCIpO3ZhciBnPVtdO3IoYSxmdW5jdGlvbihhKXt2YXIgZD1cbmNhLmVsZW1lbnQoYSkuZGF0YShcIiRiaW5kaW5nXCIpO2QmJnIoZCxmdW5jdGlvbihkKXtjPyhuZXcgUmVnRXhwKFwiKF58XFxcXHMpXCIrZ2QoYikrXCIoXFxcXHN8XFxcXHx8JClcIikpLnRlc3QoZCkmJmcucHVzaChhKTotMSE9ZC5pbmRleE9mKGIpJiZnLnB1c2goYSl9KX0pO3JldHVybiBnfSxmaW5kTW9kZWxzOmZ1bmN0aW9uKGEsYixjKXtmb3IodmFyIGc9W1wibmctXCIsXCJkYXRhLW5nLVwiLFwibmdcXFxcOlwiXSxoPTA7aDxnLmxlbmd0aDsrK2gpe3ZhciBsPWEucXVlcnlTZWxlY3RvckFsbChcIltcIitnW2hdK1wibW9kZWxcIisoYz9cIj1cIjpcIio9XCIpKydcIicrYisnXCJdJyk7aWYobC5sZW5ndGgpcmV0dXJuIGx9fSxnZXRMb2NhdGlvbjpmdW5jdGlvbigpe3JldHVybiBjLnVybCgpfSxzZXRMb2NhdGlvbjpmdW5jdGlvbihhKXthIT09Yy51cmwoKSYmKGMudXJsKGEpLGIuJGRpZ2VzdCgpKX0sd2hlblN0YWJsZTpmdW5jdGlvbihiKXthLm5vdGlmeVdoZW5Ob091dHN0YW5kaW5nUmVxdWVzdHMoYil9fX1dfWZ1bmN0aW9uIFllKCl7dGhpcy4kZ2V0PVxuW1wiJHJvb3RTY29wZVwiLFwiJGJyb3dzZXJcIixcIiRxXCIsXCIkJHFcIixcIiRleGNlcHRpb25IYW5kbGVyXCIsZnVuY3Rpb24oYixhLGMsZCxlKXtmdW5jdGlvbiBmKGYsbCxrKXt2YXIgbj15KGspJiYhayxwPShuP2Q6YykuZGVmZXIoKSxxPXAucHJvbWlzZTtsPWEuZGVmZXIoZnVuY3Rpb24oKXt0cnl7cC5yZXNvbHZlKGYoKSl9Y2F0Y2goYSl7cC5yZWplY3QoYSksZShhKX1maW5hbGx5e2RlbGV0ZSBnW3EuJCR0aW1lb3V0SWRdfW58fGIuJGFwcGx5KCl9LGwpO3EuJCR0aW1lb3V0SWQ9bDtnW2xdPXA7cmV0dXJuIHF9dmFyIGc9e307Zi5jYW5jZWw9ZnVuY3Rpb24oYil7cmV0dXJuIGImJmIuJCR0aW1lb3V0SWQgaW4gZz8oZ1tiLiQkdGltZW91dElkXS5yZWplY3QoXCJjYW5jZWxlZFwiKSxkZWxldGUgZ1tiLiQkdGltZW91dElkXSxhLmRlZmVyLmNhbmNlbChiLiQkdGltZW91dElkKSk6ITF9O3JldHVybiBmfV19ZnVuY3Rpb24gQWEoYil7UmEmJihaLnNldEF0dHJpYnV0ZShcImhyZWZcIixiKSxiPVouaHJlZik7XG5aLnNldEF0dHJpYnV0ZShcImhyZWZcIixiKTtyZXR1cm57aHJlZjpaLmhyZWYscHJvdG9jb2w6Wi5wcm90b2NvbD9aLnByb3RvY29sLnJlcGxhY2UoLzokLyxcIlwiKTpcIlwiLGhvc3Q6Wi5ob3N0LHNlYXJjaDpaLnNlYXJjaD9aLnNlYXJjaC5yZXBsYWNlKC9eXFw/LyxcIlwiKTpcIlwiLGhhc2g6Wi5oYXNoP1ouaGFzaC5yZXBsYWNlKC9eIy8sXCJcIik6XCJcIixob3N0bmFtZTpaLmhvc3RuYW1lLHBvcnQ6Wi5wb3J0LHBhdGhuYW1lOlwiL1wiPT09Wi5wYXRobmFtZS5jaGFyQXQoMCk/Wi5wYXRobmFtZTpcIi9cIitaLnBhdGhuYW1lfX1mdW5jdGlvbiBaYyhiKXtiPXgoYik/QWEoYik6YjtyZXR1cm4gYi5wcm90b2NvbD09PWlkLnByb3RvY29sJiZiLmhvc3Q9PT1pZC5ob3N0fWZ1bmN0aW9uIFplKCl7dGhpcy4kZ2V0PWVhKFIpfWZ1bmN0aW9uIEVjKGIpe2Z1bmN0aW9uIGEoYyxkKXtpZihMKGMpKXt2YXIgZT17fTtyKGMsZnVuY3Rpb24oYixjKXtlW2NdPWEoYyxiKX0pO3JldHVybiBlfXJldHVybiBiLmZhY3RvcnkoYytcblwiRmlsdGVyXCIsZCl9dGhpcy5yZWdpc3Rlcj1hO3RoaXMuJGdldD1bXCIkaW5qZWN0b3JcIixmdW5jdGlvbihhKXtyZXR1cm4gZnVuY3Rpb24oYil7cmV0dXJuIGEuZ2V0KGIrXCJGaWx0ZXJcIil9fV07YShcImN1cnJlbmN5XCIsamQpO2EoXCJkYXRlXCIsa2QpO2EoXCJmaWx0ZXJcIixFZik7YShcImpzb25cIixGZik7YShcImxpbWl0VG9cIixHZik7YShcImxvd2VyY2FzZVwiLEhmKTthKFwibnVtYmVyXCIsbGQpO2EoXCJvcmRlckJ5XCIsbWQpO2EoXCJ1cHBlcmNhc2VcIixJZil9ZnVuY3Rpb24gRWYoKXtyZXR1cm4gZnVuY3Rpb24oYixhLGMpe2lmKCFIKGIpKXJldHVybiBiO3ZhciBkO3N3aXRjaChudWxsIT09YT90eXBlb2YgYTpcIm51bGxcIil7Y2FzZSBcImZ1bmN0aW9uXCI6YnJlYWs7Y2FzZSBcImJvb2xlYW5cIjpjYXNlIFwibnVsbFwiOmNhc2UgXCJudW1iZXJcIjpjYXNlIFwic3RyaW5nXCI6ZD0hMDtjYXNlIFwib2JqZWN0XCI6YT1KZihhLGMsZCk7YnJlYWs7ZGVmYXVsdDpyZXR1cm4gYn1yZXR1cm4gYi5maWx0ZXIoYSl9fWZ1bmN0aW9uIEpmKGIsXG5hLGMpe3ZhciBkPUwoYikmJlwiJFwiaW4gYjshMD09PWE/YT1pYTp6KGEpfHwoYT1mdW5jdGlvbihhLGIpe2lmKEQoYSkpcmV0dXJuITE7aWYobnVsbD09PWF8fG51bGw9PT1iKXJldHVybiBhPT09YjtpZihMKGEpfHxMKGIpKXJldHVybiExO2E9SyhcIlwiK2EpO2I9SyhcIlwiK2IpO3JldHVybi0xIT09YS5pbmRleE9mKGIpfSk7cmV0dXJuIGZ1bmN0aW9uKGUpe3JldHVybiBkJiYhTChlKT9IYShlLGIuJCxhLCExKTpIYShlLGIsYSxjKX19ZnVuY3Rpb24gSGEoYixhLGMsZCxlKXt2YXIgZj1udWxsIT09Yj90eXBlb2YgYjpcIm51bGxcIixnPW51bGwhPT1hP3R5cGVvZiBhOlwibnVsbFwiO2lmKFwic3RyaW5nXCI9PT1nJiZcIiFcIj09PWEuY2hhckF0KDApKXJldHVybiFIYShiLGEuc3Vic3RyaW5nKDEpLGMsZCk7aWYoSChiKSlyZXR1cm4gYi5zb21lKGZ1bmN0aW9uKGIpe3JldHVybiBIYShiLGEsYyxkKX0pO3N3aXRjaChmKXtjYXNlIFwib2JqZWN0XCI6dmFyIGg7aWYoZCl7Zm9yKGggaW4gYilpZihcIiRcIiE9PVxuaC5jaGFyQXQoMCkmJkhhKGJbaF0sYSxjLCEwKSlyZXR1cm4hMDtyZXR1cm4gZT8hMTpIYShiLGEsYywhMSl9aWYoXCJvYmplY3RcIj09PWcpe2ZvcihoIGluIGEpaWYoZT1hW2hdLCF6KGUpJiYhRChlKSYmKGY9XCIkXCI9PT1oLCFIYShmP2I6YltoXSxlLGMsZixmKSkpcmV0dXJuITE7cmV0dXJuITB9cmV0dXJuIGMoYixhKTtjYXNlIFwiZnVuY3Rpb25cIjpyZXR1cm4hMTtkZWZhdWx0OnJldHVybiBjKGIsYSl9fWZ1bmN0aW9uIGpkKGIpe3ZhciBhPWIuTlVNQkVSX0ZPUk1BVFM7cmV0dXJuIGZ1bmN0aW9uKGIsZCxlKXtEKGQpJiYoZD1hLkNVUlJFTkNZX1NZTSk7RChlKSYmKGU9YS5QQVRURVJOU1sxXS5tYXhGcmFjKTtyZXR1cm4gbnVsbD09Yj9iOm5kKGIsYS5QQVRURVJOU1sxXSxhLkdST1VQX1NFUCxhLkRFQ0lNQUxfU0VQLGUpLnJlcGxhY2UoL1xcdTAwQTQvZyxkKX19ZnVuY3Rpb24gbGQoYil7dmFyIGE9Yi5OVU1CRVJfRk9STUFUUztyZXR1cm4gZnVuY3Rpb24oYixkKXtyZXR1cm4gbnVsbD09XG5iP2I6bmQoYixhLlBBVFRFUk5TWzBdLGEuR1JPVVBfU0VQLGEuREVDSU1BTF9TRVAsZCl9fWZ1bmN0aW9uIG5kKGIsYSxjLGQsZSl7aWYoIWlzRmluaXRlKGIpfHxMKGIpKXJldHVyblwiXCI7dmFyIGY9MD5iO2I9TWF0aC5hYnMoYik7dmFyIGc9YitcIlwiLGg9XCJcIixsPVtdLGs9ITE7aWYoLTEhPT1nLmluZGV4T2YoXCJlXCIpKXt2YXIgbj1nLm1hdGNoKC8oW1xcZFxcLl0rKWUoLT8pKFxcZCspLyk7biYmXCItXCI9PW5bMl0mJm5bM10+ZSsxP2I9MDooaD1nLGs9ITApfWlmKGspMDxlJiYxPmImJihoPWIudG9GaXhlZChlKSxiPXBhcnNlRmxvYXQoaCkpO2Vsc2V7Zz0oZy5zcGxpdChvZClbMV18fFwiXCIpLmxlbmd0aDtEKGUpJiYoZT1NYXRoLm1pbihNYXRoLm1heChhLm1pbkZyYWMsZyksYS5tYXhGcmFjKSk7Yj0rKE1hdGgucm91bmQoKyhiLnRvU3RyaW5nKCkrXCJlXCIrZSkpLnRvU3RyaW5nKCkrXCJlXCIrLWUpO3ZhciBnPShcIlwiK2IpLnNwbGl0KG9kKSxrPWdbMF0sZz1nWzFdfHxcIlwiLHA9MCxxPWEubGdTaXplLFxudD1hLmdTaXplO2lmKGsubGVuZ3RoPj1xK3QpZm9yKHA9ay5sZW5ndGgtcSxuPTA7bjxwO24rKykwPT09KHAtbikldCYmMCE9PW4mJihoKz1jKSxoKz1rLmNoYXJBdChuKTtmb3Iobj1wO248ay5sZW5ndGg7bisrKTA9PT0oay5sZW5ndGgtbiklcSYmMCE9PW4mJihoKz1jKSxoKz1rLmNoYXJBdChuKTtmb3IoO2cubGVuZ3RoPGU7KWcrPVwiMFwiO2UmJlwiMFwiIT09ZSYmKGgrPWQrZy5zdWJzdHIoMCxlKSl9MD09PWImJihmPSExKTtsLnB1c2goZj9hLm5lZ1ByZTphLnBvc1ByZSxoLGY/YS5uZWdTdWY6YS5wb3NTdWYpO3JldHVybiBsLmpvaW4oXCJcIil9ZnVuY3Rpb24gSmIoYixhLGMpe3ZhciBkPVwiXCI7MD5iJiYoZD1cIi1cIixiPS1iKTtmb3IoYj1cIlwiK2I7Yi5sZW5ndGg8YTspYj1cIjBcIitiO2MmJihiPWIuc3Vic3RyKGIubGVuZ3RoLWEpKTtyZXR1cm4gZCtifWZ1bmN0aW9uIFUoYixhLGMsZCl7Yz1jfHwwO3JldHVybiBmdW5jdGlvbihlKXtlPWVbXCJnZXRcIitiXSgpO2lmKDA8Y3x8ZT4tYyllKz1cbmM7MD09PWUmJi0xMj09YyYmKGU9MTIpO3JldHVybiBKYihlLGEsZCl9fWZ1bmN0aW9uIEtiKGIsYSl7cmV0dXJuIGZ1bmN0aW9uKGMsZCl7dmFyIGU9Y1tcImdldFwiK2JdKCksZj12YihhP1wiU0hPUlRcIitiOmIpO3JldHVybiBkW2ZdW2VdfX1mdW5jdGlvbiBwZChiKXt2YXIgYT0obmV3IERhdGUoYiwwLDEpKS5nZXREYXkoKTtyZXR1cm4gbmV3IERhdGUoYiwwLCg0Pj1hPzU6MTIpLWEpfWZ1bmN0aW9uIHFkKGIpe3JldHVybiBmdW5jdGlvbihhKXt2YXIgYz1wZChhLmdldEZ1bGxZZWFyKCkpO2E9K25ldyBEYXRlKGEuZ2V0RnVsbFllYXIoKSxhLmdldE1vbnRoKCksYS5nZXREYXRlKCkrKDQtYS5nZXREYXkoKSkpLStjO2E9MStNYXRoLnJvdW5kKGEvNjA0OEU1KTtyZXR1cm4gSmIoYSxiKX19ZnVuY3Rpb24gaGMoYixhKXtyZXR1cm4gMD49Yi5nZXRGdWxsWWVhcigpP2EuRVJBU1swXTphLkVSQVNbMV19ZnVuY3Rpb24ga2QoYil7ZnVuY3Rpb24gYShhKXt2YXIgYjtpZihiPWEubWF0Y2goYykpe2E9XG5uZXcgRGF0ZSgwKTt2YXIgZj0wLGc9MCxoPWJbOF0/YS5zZXRVVENGdWxsWWVhcjphLnNldEZ1bGxZZWFyLGw9Yls4XT9hLnNldFVUQ0hvdXJzOmEuc2V0SG91cnM7Yls5XSYmKGY9YWEoYls5XStiWzEwXSksZz1hYShiWzldK2JbMTFdKSk7aC5jYWxsKGEsYWEoYlsxXSksYWEoYlsyXSktMSxhYShiWzNdKSk7Zj1hYShiWzRdfHwwKS1mO2c9YWEoYls1XXx8MCktZztoPWFhKGJbNl18fDApO2I9TWF0aC5yb3VuZCgxRTMqcGFyc2VGbG9hdChcIjAuXCIrKGJbN118fDApKSk7bC5jYWxsKGEsZixnLGgsYil9cmV0dXJuIGF9dmFyIGM9L14oXFxkezR9KS0/KFxcZFxcZCktPyhcXGRcXGQpKD86VChcXGRcXGQpKD86Oj8oXFxkXFxkKSg/Ojo/KFxcZFxcZCkoPzpcXC4oXFxkKykpPyk/KT8oWnwoWystXSkoXFxkXFxkKTo/KFxcZFxcZCkpPyk/JC87cmV0dXJuIGZ1bmN0aW9uKGMsZSxmKXt2YXIgZz1cIlwiLGg9W10sbCxrO2U9ZXx8XCJtZWRpdW1EYXRlXCI7ZT1iLkRBVEVUSU1FX0ZPUk1BVFNbZV18fGU7eChjKSYmKGM9S2YudGVzdChjKT9cbmFhKGMpOmEoYykpO1koYykmJihjPW5ldyBEYXRlKGMpKTtpZighaGEoYykpcmV0dXJuIGM7Zm9yKDtlOykoaz1MZi5leGVjKGUpKT8oaD1aYShoLGssMSksZT1oLnBvcCgpKTooaC5wdXNoKGUpLGU9bnVsbCk7ZiYmXCJVVENcIj09PWYmJihjPW5ldyBEYXRlKGMuZ2V0VGltZSgpKSxjLnNldE1pbnV0ZXMoYy5nZXRNaW51dGVzKCkrYy5nZXRUaW1lem9uZU9mZnNldCgpKSk7cihoLGZ1bmN0aW9uKGEpe2w9TWZbYV07Zys9bD9sKGMsYi5EQVRFVElNRV9GT1JNQVRTKTphLnJlcGxhY2UoLyheJ3wnJCkvZyxcIlwiKS5yZXBsYWNlKC8nJy9nLFwiJ1wiKX0pO3JldHVybiBnfX1mdW5jdGlvbiBGZigpe3JldHVybiBmdW5jdGlvbihiLGEpe0QoYSkmJihhPTIpO3JldHVybiBhYihiLGEpfX1mdW5jdGlvbiBHZigpe3JldHVybiBmdW5jdGlvbihiLGEpe1koYikmJihiPWIudG9TdHJpbmcoKSk7cmV0dXJuIEgoYil8fHgoYik/KGE9SW5maW5pdHk9PT1NYXRoLmFicyhOdW1iZXIoYSkpP051bWJlcihhKTpcbmFhKGEpKT8wPGE/Yi5zbGljZSgwLGEpOmIuc2xpY2UoYSk6eChiKT9cIlwiOltdOmJ9fWZ1bmN0aW9uIG1kKGIpe3JldHVybiBmdW5jdGlvbihhLGMsZCl7ZnVuY3Rpb24gZShhLGIpe3JldHVybiBiP2Z1bmN0aW9uKGIsYyl7cmV0dXJuIGEoYyxiKX06YX1mdW5jdGlvbiBmKGEpe3N3aXRjaCh0eXBlb2YgYSl7Y2FzZSBcIm51bWJlclwiOmNhc2UgXCJib29sZWFuXCI6Y2FzZSBcInN0cmluZ1wiOnJldHVybiEwO2RlZmF1bHQ6cmV0dXJuITF9fWZ1bmN0aW9uIGcoYSl7cmV0dXJuIG51bGw9PT1hP1wibnVsbFwiOlwiZnVuY3Rpb25cIj09PXR5cGVvZiBhLnZhbHVlT2YmJihhPWEudmFsdWVPZigpLGYoYSkpfHxcImZ1bmN0aW9uXCI9PT10eXBlb2YgYS50b1N0cmluZyYmKGE9YS50b1N0cmluZygpLGYoYSkpP2E6XCJcIn1mdW5jdGlvbiBoKGEsYil7dmFyIGM9dHlwZW9mIGEsZD10eXBlb2YgYjtjPT09ZCYmXCJvYmplY3RcIj09PWMmJihhPWcoYSksYj1nKGIpKTtyZXR1cm4gYz09PWQ/KFwic3RyaW5nXCI9PT1jJiYoYT1cbmEudG9Mb3dlckNhc2UoKSxiPWIudG9Mb3dlckNhc2UoKSksYT09PWI/MDphPGI/LTE6MSk6YzxkPy0xOjF9aWYoIVRhKGEpKXJldHVybiBhO2M9SChjKT9jOltjXTswPT09Yy5sZW5ndGgmJihjPVtcIitcIl0pO2M9Yy5tYXAoZnVuY3Rpb24oYSl7dmFyIGM9ITEsZD1hfHxyYTtpZih4KGEpKXtpZihcIitcIj09YS5jaGFyQXQoMCl8fFwiLVwiPT1hLmNoYXJBdCgwKSljPVwiLVwiPT1hLmNoYXJBdCgwKSxhPWEuc3Vic3RyaW5nKDEpO2lmKFwiXCI9PT1hKXJldHVybiBlKGgsYyk7ZD1iKGEpO2lmKGQuY29uc3RhbnQpe3ZhciBmPWQoKTtyZXR1cm4gZShmdW5jdGlvbihhLGIpe3JldHVybiBoKGFbZl0sYltmXSl9LGMpfX1yZXR1cm4gZShmdW5jdGlvbihhLGIpe3JldHVybiBoKGQoYSksZChiKSl9LGMpfSk7cmV0dXJuICRhLmNhbGwoYSkuc29ydChlKGZ1bmN0aW9uKGEsYil7Zm9yKHZhciBkPTA7ZDxjLmxlbmd0aDtkKyspe3ZhciBlPWNbZF0oYSxiKTtpZigwIT09ZSlyZXR1cm4gZX1yZXR1cm4gMH0sXG5kKSl9fWZ1bmN0aW9uIElhKGIpe3ooYikmJihiPXtsaW5rOmJ9KTtiLnJlc3RyaWN0PWIucmVzdHJpY3R8fFwiQUNcIjtyZXR1cm4gZWEoYil9ZnVuY3Rpb24gcmQoYixhLGMsZCxlKXt2YXIgZj10aGlzLGc9W10saD1mLiQkcGFyZW50Rm9ybT1iLnBhcmVudCgpLmNvbnRyb2xsZXIoXCJmb3JtXCIpfHxMYjtmLiRlcnJvcj17fTtmLiQkc3VjY2Vzcz17fTtmLiRwZW5kaW5nPXU7Zi4kbmFtZT1lKGEubmFtZXx8YS5uZ0Zvcm18fFwiXCIpKGMpO2YuJGRpcnR5PSExO2YuJHByaXN0aW5lPSEwO2YuJHZhbGlkPSEwO2YuJGludmFsaWQ9ITE7Zi4kc3VibWl0dGVkPSExO2guJGFkZENvbnRyb2woZik7Zi4kcm9sbGJhY2tWaWV3VmFsdWU9ZnVuY3Rpb24oKXtyKGcsZnVuY3Rpb24oYSl7YS4kcm9sbGJhY2tWaWV3VmFsdWUoKX0pfTtmLiRjb21taXRWaWV3VmFsdWU9ZnVuY3Rpb24oKXtyKGcsZnVuY3Rpb24oYSl7YS4kY29tbWl0Vmlld1ZhbHVlKCl9KX07Zi4kYWRkQ29udHJvbD1mdW5jdGlvbihhKXtNYShhLiRuYW1lLFxuXCJpbnB1dFwiKTtnLnB1c2goYSk7YS4kbmFtZSYmKGZbYS4kbmFtZV09YSl9O2YuJCRyZW5hbWVDb250cm9sPWZ1bmN0aW9uKGEsYil7dmFyIGM9YS4kbmFtZTtmW2NdPT09YSYmZGVsZXRlIGZbY107ZltiXT1hO2EuJG5hbWU9Yn07Zi4kcmVtb3ZlQ29udHJvbD1mdW5jdGlvbihhKXthLiRuYW1lJiZmW2EuJG5hbWVdPT09YSYmZGVsZXRlIGZbYS4kbmFtZV07cihmLiRwZW5kaW5nLGZ1bmN0aW9uKGIsYyl7Zi4kc2V0VmFsaWRpdHkoYyxudWxsLGEpfSk7cihmLiRlcnJvcixmdW5jdGlvbihiLGMpe2YuJHNldFZhbGlkaXR5KGMsbnVsbCxhKX0pO3IoZi4kJHN1Y2Nlc3MsZnVuY3Rpb24oYixjKXtmLiRzZXRWYWxpZGl0eShjLG51bGwsYSl9KTtZYShnLGEpfTtzZCh7Y3RybDp0aGlzLCRlbGVtZW50OmIsc2V0OmZ1bmN0aW9uKGEsYixjKXt2YXIgZD1hW2JdO2Q/LTE9PT1kLmluZGV4T2YoYykmJmQucHVzaChjKTphW2JdPVtjXX0sdW5zZXQ6ZnVuY3Rpb24oYSxiLGMpe3ZhciBkPWFbYl07XG5kJiYoWWEoZCxjKSwwPT09ZC5sZW5ndGgmJmRlbGV0ZSBhW2JdKX0scGFyZW50Rm9ybTpoLCRhbmltYXRlOmR9KTtmLiRzZXREaXJ0eT1mdW5jdGlvbigpe2QucmVtb3ZlQ2xhc3MoYixTYSk7ZC5hZGRDbGFzcyhiLE1iKTtmLiRkaXJ0eT0hMDtmLiRwcmlzdGluZT0hMTtoLiRzZXREaXJ0eSgpfTtmLiRzZXRQcmlzdGluZT1mdW5jdGlvbigpe2Quc2V0Q2xhc3MoYixTYSxNYitcIiBuZy1zdWJtaXR0ZWRcIik7Zi4kZGlydHk9ITE7Zi4kcHJpc3RpbmU9ITA7Zi4kc3VibWl0dGVkPSExO3IoZyxmdW5jdGlvbihhKXthLiRzZXRQcmlzdGluZSgpfSl9O2YuJHNldFVudG91Y2hlZD1mdW5jdGlvbigpe3IoZyxmdW5jdGlvbihhKXthLiRzZXRVbnRvdWNoZWQoKX0pfTtmLiRzZXRTdWJtaXR0ZWQ9ZnVuY3Rpb24oKXtkLmFkZENsYXNzKGIsXCJuZy1zdWJtaXR0ZWRcIik7Zi4kc3VibWl0dGVkPSEwO2guJHNldFN1Ym1pdHRlZCgpfX1mdW5jdGlvbiBpYyhiKXtiLiRmb3JtYXR0ZXJzLnB1c2goZnVuY3Rpb24oYSl7cmV0dXJuIGIuJGlzRW1wdHkoYSk/XG5hOmEudG9TdHJpbmcoKX0pfWZ1bmN0aW9uIGxiKGIsYSxjLGQsZSxmKXt2YXIgZz1LKGFbMF0udHlwZSk7aWYoIWUuYW5kcm9pZCl7dmFyIGg9ITE7YS5vbihcImNvbXBvc2l0aW9uc3RhcnRcIixmdW5jdGlvbihhKXtoPSEwfSk7YS5vbihcImNvbXBvc2l0aW9uZW5kXCIsZnVuY3Rpb24oKXtoPSExO2woKX0pfXZhciBsPWZ1bmN0aW9uKGIpe2smJihmLmRlZmVyLmNhbmNlbChrKSxrPW51bGwpO2lmKCFoKXt2YXIgZT1hLnZhbCgpO2I9YiYmYi50eXBlO1wicGFzc3dvcmRcIj09PWd8fGMubmdUcmltJiZcImZhbHNlXCI9PT1jLm5nVHJpbXx8KGU9TihlKSk7KGQuJHZpZXdWYWx1ZSE9PWV8fFwiXCI9PT1lJiZkLiQkaGFzTmF0aXZlVmFsaWRhdG9ycykmJmQuJHNldFZpZXdWYWx1ZShlLGIpfX07aWYoZS5oYXNFdmVudChcImlucHV0XCIpKWEub24oXCJpbnB1dFwiLGwpO2Vsc2V7dmFyIGssbj1mdW5jdGlvbihhLGIsYyl7a3x8KGs9Zi5kZWZlcihmdW5jdGlvbigpe2s9bnVsbDtiJiZiLnZhbHVlPT09Y3x8bChhKX0pKX07XG5hLm9uKFwia2V5ZG93blwiLGZ1bmN0aW9uKGEpe3ZhciBiPWEua2V5Q29kZTs5MT09PWJ8fDE1PGImJjE5PmJ8fDM3PD1iJiY0MD49Ynx8bihhLHRoaXMsdGhpcy52YWx1ZSl9KTtpZihlLmhhc0V2ZW50KFwicGFzdGVcIikpYS5vbihcInBhc3RlIGN1dFwiLG4pfWEub24oXCJjaGFuZ2VcIixsKTtkLiRyZW5kZXI9ZnVuY3Rpb24oKXthLnZhbChkLiRpc0VtcHR5KGQuJHZpZXdWYWx1ZSk/XCJcIjpkLiR2aWV3VmFsdWUpfX1mdW5jdGlvbiBOYihiLGEpe3JldHVybiBmdW5jdGlvbihjLGQpe3ZhciBlLGY7aWYoaGEoYykpcmV0dXJuIGM7aWYoeChjKSl7J1wiJz09Yy5jaGFyQXQoMCkmJidcIic9PWMuY2hhckF0KGMubGVuZ3RoLTEpJiYoYz1jLnN1YnN0cmluZygxLGMubGVuZ3RoLTEpKTtpZihOZi50ZXN0KGMpKXJldHVybiBuZXcgRGF0ZShjKTtiLmxhc3RJbmRleD0wO2lmKGU9Yi5leGVjKGMpKXJldHVybiBlLnNoaWZ0KCksZj1kP3t5eXl5OmQuZ2V0RnVsbFllYXIoKSxNTTpkLmdldE1vbnRoKCkrMSxcbmRkOmQuZ2V0RGF0ZSgpLEhIOmQuZ2V0SG91cnMoKSxtbTpkLmdldE1pbnV0ZXMoKSxzczpkLmdldFNlY29uZHMoKSxzc3M6ZC5nZXRNaWxsaXNlY29uZHMoKS8xRTN9Ont5eXl5OjE5NzAsTU06MSxkZDoxLEhIOjAsbW06MCxzczowLHNzczowfSxyKGUsZnVuY3Rpb24oYixjKXtjPGEubGVuZ3RoJiYoZlthW2NdXT0rYil9KSxuZXcgRGF0ZShmLnl5eXksZi5NTS0xLGYuZGQsZi5ISCxmLm1tLGYuc3N8fDAsMUUzKmYuc3NzfHwwKX1yZXR1cm4gTmFOfX1mdW5jdGlvbiBtYihiLGEsYyxkKXtyZXR1cm4gZnVuY3Rpb24oZSxmLGcsaCxsLGssbil7ZnVuY3Rpb24gcChhKXtyZXR1cm4gYSYmIShhLmdldFRpbWUmJmEuZ2V0VGltZSgpIT09YS5nZXRUaW1lKCkpfWZ1bmN0aW9uIHEoYSl7cmV0dXJuIHkoYSk/aGEoYSk/YTpjKGEpOnV9dGQoZSxmLGcsaCk7bGIoZSxmLGcsaCxsLGspO3ZhciB0PWgmJmguJG9wdGlvbnMmJmguJG9wdGlvbnMudGltZXpvbmUscztoLiQkcGFyc2VyTmFtZT1iO1xuaC4kcGFyc2Vycy5wdXNoKGZ1bmN0aW9uKGIpe3JldHVybiBoLiRpc0VtcHR5KGIpP251bGw6YS50ZXN0KGIpPyhiPWMoYixzKSxcIlVUQ1wiPT09dCYmYi5zZXRNaW51dGVzKGIuZ2V0TWludXRlcygpLWIuZ2V0VGltZXpvbmVPZmZzZXQoKSksYik6dX0pO2guJGZvcm1hdHRlcnMucHVzaChmdW5jdGlvbihhKXtpZihhJiYhaGEoYSkpdGhyb3cgbmIoXCJkYXRlZm10XCIsYSk7aWYocChhKSl7aWYoKHM9YSkmJlwiVVRDXCI9PT10KXt2YXIgYj02RTQqcy5nZXRUaW1lem9uZU9mZnNldCgpO3M9bmV3IERhdGUocy5nZXRUaW1lKCkrYil9cmV0dXJuIG4oXCJkYXRlXCIpKGEsZCx0KX1zPW51bGw7cmV0dXJuXCJcIn0pO2lmKHkoZy5taW4pfHxnLm5nTWluKXt2YXIgcjtoLiR2YWxpZGF0b3JzLm1pbj1mdW5jdGlvbihhKXtyZXR1cm4hcChhKXx8RChyKXx8YyhhKT49cn07Zy4kb2JzZXJ2ZShcIm1pblwiLGZ1bmN0aW9uKGEpe3I9cShhKTtoLiR2YWxpZGF0ZSgpfSl9aWYoeShnLm1heCl8fGcubmdNYXgpe3ZhciB2O1xuaC4kdmFsaWRhdG9ycy5tYXg9ZnVuY3Rpb24oYSl7cmV0dXJuIXAoYSl8fEQodil8fGMoYSk8PXZ9O2cuJG9ic2VydmUoXCJtYXhcIixmdW5jdGlvbihhKXt2PXEoYSk7aC4kdmFsaWRhdGUoKX0pfX19ZnVuY3Rpb24gdGQoYixhLGMsZCl7KGQuJCRoYXNOYXRpdmVWYWxpZGF0b3JzPUwoYVswXS52YWxpZGl0eSkpJiZkLiRwYXJzZXJzLnB1c2goZnVuY3Rpb24oYil7dmFyIGM9YS5wcm9wKFwidmFsaWRpdHlcIil8fHt9O3JldHVybiBjLmJhZElucHV0JiYhYy50eXBlTWlzbWF0Y2g/dTpifSl9ZnVuY3Rpb24gdWQoYixhLGMsZCxlKXtpZih5KGQpKXtiPWIoZCk7aWYoIWIuY29uc3RhbnQpdGhyb3cgbmIoXCJjb25zdGV4cHJcIixjLGQpO3JldHVybiBiKGEpfXJldHVybiBlfWZ1bmN0aW9uIGpjKGIsYSl7Yj1cIm5nQ2xhc3NcIitiO3JldHVybltcIiRhbmltYXRlXCIsZnVuY3Rpb24oYyl7ZnVuY3Rpb24gZChhLGIpe3ZhciBjPVtdLGQ9MDthOmZvcig7ZDxhLmxlbmd0aDtkKyspe2Zvcih2YXIgZT1hW2RdLFxubj0wO248Yi5sZW5ndGg7bisrKWlmKGU9PWJbbl0pY29udGludWUgYTtjLnB1c2goZSl9cmV0dXJuIGN9ZnVuY3Rpb24gZShhKXtpZighSChhKSl7aWYoeChhKSlyZXR1cm4gYS5zcGxpdChcIiBcIik7aWYoTChhKSl7dmFyIGI9W107cihhLGZ1bmN0aW9uKGEsYyl7YSYmKGI9Yi5jb25jYXQoYy5zcGxpdChcIiBcIikpKX0pO3JldHVybiBifX1yZXR1cm4gYX1yZXR1cm57cmVzdHJpY3Q6XCJBQ1wiLGxpbms6ZnVuY3Rpb24oZixnLGgpe2Z1bmN0aW9uIGwoYSxiKXt2YXIgYz1nLmRhdGEoXCIkY2xhc3NDb3VudHNcIil8fHt9LGQ9W107cihhLGZ1bmN0aW9uKGEpe2lmKDA8Ynx8Y1thXSljW2FdPShjW2FdfHwwKStiLGNbYV09PT0rKDA8YikmJmQucHVzaChhKX0pO2cuZGF0YShcIiRjbGFzc0NvdW50c1wiLGMpO3JldHVybiBkLmpvaW4oXCIgXCIpfWZ1bmN0aW9uIGsoYil7aWYoITA9PT1hfHxmLiRpbmRleCUyPT09YSl7dmFyIGs9ZShifHxbXSk7aWYoIW4pe3ZhciB0PWwoaywxKTtoLiRhZGRDbGFzcyh0KX1lbHNlIGlmKCFpYShiLFxubikpe3ZhciBzPWUobiksdD1kKGsscyksaz1kKHMsayksdD1sKHQsMSksaz1sKGssLTEpO3QmJnQubGVuZ3RoJiZjLmFkZENsYXNzKGcsdCk7ayYmay5sZW5ndGgmJmMucmVtb3ZlQ2xhc3MoZyxrKX19bj1zYShiKX12YXIgbjtmLiR3YXRjaChoW2JdLGssITApO2guJG9ic2VydmUoXCJjbGFzc1wiLGZ1bmN0aW9uKGEpe2soZi4kZXZhbChoW2JdKSl9KTtcIm5nQ2xhc3NcIiE9PWImJmYuJHdhdGNoKFwiJGluZGV4XCIsZnVuY3Rpb24oYyxkKXt2YXIgZz1jJjE7aWYoZyE9PShkJjEpKXt2YXIgaz1lKGYuJGV2YWwoaFtiXSkpO2c9PT1hPyhnPWwoaywxKSxoLiRhZGRDbGFzcyhnKSk6KGc9bChrLC0xKSxoLiRyZW1vdmVDbGFzcyhnKSl9fSl9fX1dfWZ1bmN0aW9uIHNkKGIpe2Z1bmN0aW9uIGEoYSxiKXtiJiYhZlthXT8oay5hZGRDbGFzcyhlLGEpLGZbYV09ITApOiFiJiZmW2FdJiYoay5yZW1vdmVDbGFzcyhlLGEpLGZbYV09ITEpfWZ1bmN0aW9uIGMoYixjKXtiPWI/XCItXCIrdWMoYixcIi1cIik6XCJcIjtcbmEob2IrYiwhMD09PWMpO2EodmQrYiwhMT09PWMpfXZhciBkPWIuY3RybCxlPWIuJGVsZW1lbnQsZj17fSxnPWIuc2V0LGg9Yi51bnNldCxsPWIucGFyZW50Rm9ybSxrPWIuJGFuaW1hdGU7Zlt2ZF09IShmW29iXT1lLmhhc0NsYXNzKG9iKSk7ZC4kc2V0VmFsaWRpdHk9ZnVuY3Rpb24oYixlLGYpe2U9PT11PyhkLiRwZW5kaW5nfHwoZC4kcGVuZGluZz17fSksZyhkLiRwZW5kaW5nLGIsZikpOihkLiRwZW5kaW5nJiZoKGQuJHBlbmRpbmcsYixmKSx3ZChkLiRwZW5kaW5nKSYmKGQuJHBlbmRpbmc9dSkpO1hhKGUpP2U/KGgoZC4kZXJyb3IsYixmKSxnKGQuJCRzdWNjZXNzLGIsZikpOihnKGQuJGVycm9yLGIsZiksaChkLiQkc3VjY2VzcyxiLGYpKTooaChkLiRlcnJvcixiLGYpLGgoZC4kJHN1Y2Nlc3MsYixmKSk7ZC4kcGVuZGluZz8oYSh4ZCwhMCksZC4kdmFsaWQ9ZC4kaW52YWxpZD11LGMoXCJcIixudWxsKSk6KGEoeGQsITEpLGQuJHZhbGlkPXdkKGQuJGVycm9yKSxkLiRpbnZhbGlkPVxuIWQuJHZhbGlkLGMoXCJcIixkLiR2YWxpZCkpO2U9ZC4kcGVuZGluZyYmZC4kcGVuZGluZ1tiXT91OmQuJGVycm9yW2JdPyExOmQuJCRzdWNjZXNzW2JdPyEwOm51bGw7YyhiLGUpO2wuJHNldFZhbGlkaXR5KGIsZSxkKX19ZnVuY3Rpb24gd2QoYil7aWYoYilmb3IodmFyIGEgaW4gYilyZXR1cm4hMTtyZXR1cm4hMH12YXIgT2Y9L15cXC8oLispXFwvKFthLXpdKikkLyxLPWZ1bmN0aW9uKGIpe3JldHVybiB4KGIpP2IudG9Mb3dlckNhc2UoKTpifSxzYz1PYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LHZiPWZ1bmN0aW9uKGIpe3JldHVybiB4KGIpP2IudG9VcHBlckNhc2UoKTpifSxSYSxCLHRhLCRhPVtdLnNsaWNlLHFmPVtdLnNwbGljZSxQZj1bXS5wdXNoLENhPU9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcsSmE9UyhcIm5nXCIpLGNhPVIuYW5ndWxhcnx8KFIuYW5ndWxhcj17fSksZWIscmI9MDtSYT1XLmRvY3VtZW50TW9kZTtBLiRpbmplY3Q9W107cmEuJGluamVjdD1bXTt2YXIgSD1cbkFycmF5LmlzQXJyYXksTj1mdW5jdGlvbihiKXtyZXR1cm4geChiKT9iLnRyaW0oKTpifSxnZD1mdW5jdGlvbihiKXtyZXR1cm4gYi5yZXBsYWNlKC8oWy0oKVxcW1xcXXt9Kz8qLiRcXF58LDojPCFcXFxcXSkvZyxcIlxcXFwkMVwiKS5yZXBsYWNlKC9cXHgwOC9nLFwiXFxcXHgwOFwiKX0sZGI9ZnVuY3Rpb24oKXtpZih5KGRiLmlzQWN0aXZlXykpcmV0dXJuIGRiLmlzQWN0aXZlXzt2YXIgYj0hKCFXLnF1ZXJ5U2VsZWN0b3IoXCJbbmctY3NwXVwiKSYmIVcucXVlcnlTZWxlY3RvcihcIltkYXRhLW5nLWNzcF1cIikpO2lmKCFiKXRyeXtuZXcgRnVuY3Rpb24oXCJcIil9Y2F0Y2goYSl7Yj0hMH1yZXR1cm4gZGIuaXNBY3RpdmVfPWJ9LHRiPVtcIm5nLVwiLFwiZGF0YS1uZy1cIixcIm5nOlwiLFwieC1uZy1cIl0sTWQ9L1tBLVpdL2csdmM9ITEsUWIscWE9MSxiYj0zLFFkPXtmdWxsOlwiMS4zLjIwXCIsbWFqb3I6MSxtaW5vcjozLGRvdDoyMCxjb2RlTmFtZTpcInNoYWxsb3ctdHJhbnNsdWNlbmNlXCJ9O1QuZXhwYW5kbz1cIm5nMzM5XCI7dmFyIEFiPVxuVC5jYWNoZT17fSxoZj0xO1QuX2RhdGE9ZnVuY3Rpb24oYil7cmV0dXJuIHRoaXMuY2FjaGVbYlt0aGlzLmV4cGFuZG9dXXx8e319O3ZhciBjZj0vKFtcXDpcXC1cXF9dKyguKSkvZyxkZj0vXm1veihbQS1aXSkvLFFmPXttb3VzZWxlYXZlOlwibW91c2VvdXRcIixtb3VzZWVudGVyOlwibW91c2VvdmVyXCJ9LFRiPVMoXCJqcUxpdGVcIiksZ2Y9L148KFxcdyspXFxzKlxcLz8+KD86PFxcL1xcMT58KSQvLFNiPS88fCYjP1xcdys7LyxlZj0vPChbXFx3Ol0rKS8sZmY9LzwoPyFhcmVhfGJyfGNvbHxlbWJlZHxocnxpbWd8aW5wdXR8bGlua3xtZXRhfHBhcmFtKSgoW1xcdzpdKylbXj5dKilcXC8+L2dpLGthPXtvcHRpb246WzEsJzxzZWxlY3QgbXVsdGlwbGU9XCJtdWx0aXBsZVwiPicsXCI8L3NlbGVjdD5cIl0sdGhlYWQ6WzEsXCI8dGFibGU+XCIsXCI8L3RhYmxlPlwiXSxjb2w6WzIsXCI8dGFibGU+PGNvbGdyb3VwPlwiLFwiPC9jb2xncm91cD48L3RhYmxlPlwiXSx0cjpbMixcIjx0YWJsZT48dGJvZHk+XCIsXCI8L3Rib2R5PjwvdGFibGU+XCJdLFxudGQ6WzMsXCI8dGFibGU+PHRib2R5Pjx0cj5cIixcIjwvdHI+PC90Ym9keT48L3RhYmxlPlwiXSxfZGVmYXVsdDpbMCxcIlwiLFwiXCJdfTtrYS5vcHRncm91cD1rYS5vcHRpb247a2EudGJvZHk9a2EudGZvb3Q9a2EuY29sZ3JvdXA9a2EuY2FwdGlvbj1rYS50aGVhZDtrYS50aD1rYS50ZDt2YXIgS2E9VC5wcm90b3R5cGU9e3JlYWR5OmZ1bmN0aW9uKGIpe2Z1bmN0aW9uIGEoKXtjfHwoYz0hMCxiKCkpfXZhciBjPSExO1wiY29tcGxldGVcIj09PVcucmVhZHlTdGF0ZT9zZXRUaW1lb3V0KGEpOih0aGlzLm9uKFwiRE9NQ29udGVudExvYWRlZFwiLGEpLFQoUikub24oXCJsb2FkXCIsYSkpfSx0b1N0cmluZzpmdW5jdGlvbigpe3ZhciBiPVtdO3IodGhpcyxmdW5jdGlvbihhKXtiLnB1c2goXCJcIithKX0pO3JldHVyblwiW1wiK2Iuam9pbihcIiwgXCIpK1wiXVwifSxlcTpmdW5jdGlvbihiKXtyZXR1cm4gMDw9Yj9CKHRoaXNbYl0pOkIodGhpc1t0aGlzLmxlbmd0aCtiXSl9LGxlbmd0aDowLHB1c2g6UGYsc29ydDpbXS5zb3J0LFxuc3BsaWNlOltdLnNwbGljZX0sRmI9e307cihcIm11bHRpcGxlIHNlbGVjdGVkIGNoZWNrZWQgZGlzYWJsZWQgcmVhZE9ubHkgcmVxdWlyZWQgb3BlblwiLnNwbGl0KFwiIFwiKSxmdW5jdGlvbihiKXtGYltLKGIpXT1ifSk7dmFyIE5jPXt9O3IoXCJpbnB1dCBzZWxlY3Qgb3B0aW9uIHRleHRhcmVhIGJ1dHRvbiBmb3JtIGRldGFpbHNcIi5zcGxpdChcIiBcIiksZnVuY3Rpb24oYil7TmNbYl09ITB9KTt2YXIgT2M9e25nTWlubGVuZ3RoOlwibWlubGVuZ3RoXCIsbmdNYXhsZW5ndGg6XCJtYXhsZW5ndGhcIixuZ01pbjpcIm1pblwiLG5nTWF4OlwibWF4XCIsbmdQYXR0ZXJuOlwicGF0dGVyblwifTtyKHtkYXRhOlZiLHJlbW92ZURhdGE6eWJ9LGZ1bmN0aW9uKGIsYSl7VFthXT1ifSk7cih7ZGF0YTpWYixpbmhlcml0ZWREYXRhOkViLHNjb3BlOmZ1bmN0aW9uKGIpe3JldHVybiBCLmRhdGEoYixcIiRzY29wZVwiKXx8RWIoYi5wYXJlbnROb2RlfHxiLFtcIiRpc29sYXRlU2NvcGVcIixcIiRzY29wZVwiXSl9LGlzb2xhdGVTY29wZTpmdW5jdGlvbihiKXtyZXR1cm4gQi5kYXRhKGIsXG5cIiRpc29sYXRlU2NvcGVcIil8fEIuZGF0YShiLFwiJGlzb2xhdGVTY29wZU5vVGVtcGxhdGVcIil9LGNvbnRyb2xsZXI6SmMsaW5qZWN0b3I6ZnVuY3Rpb24oYil7cmV0dXJuIEViKGIsXCIkaW5qZWN0b3JcIil9LHJlbW92ZUF0dHI6ZnVuY3Rpb24oYixhKXtiLnJlbW92ZUF0dHJpYnV0ZShhKX0saGFzQ2xhc3M6QmIsY3NzOmZ1bmN0aW9uKGIsYSxjKXthPWZiKGEpO2lmKHkoYykpYi5zdHlsZVthXT1jO2Vsc2UgcmV0dXJuIGIuc3R5bGVbYV19LGF0dHI6ZnVuY3Rpb24oYixhLGMpe3ZhciBkPWIubm9kZVR5cGU7aWYoZCE9PWJiJiYyIT09ZCYmOCE9PWQpaWYoZD1LKGEpLEZiW2RdKWlmKHkoYykpYz8oYlthXT0hMCxiLnNldEF0dHJpYnV0ZShhLGQpKTooYlthXT0hMSxiLnJlbW92ZUF0dHJpYnV0ZShkKSk7ZWxzZSByZXR1cm4gYlthXXx8KGIuYXR0cmlidXRlcy5nZXROYW1lZEl0ZW0oYSl8fEEpLnNwZWNpZmllZD9kOnU7ZWxzZSBpZih5KGMpKWIuc2V0QXR0cmlidXRlKGEsYyk7ZWxzZSBpZihiLmdldEF0dHJpYnV0ZSlyZXR1cm4gYj1cbmIuZ2V0QXR0cmlidXRlKGEsMiksbnVsbD09PWI/dTpifSxwcm9wOmZ1bmN0aW9uKGIsYSxjKXtpZih5KGMpKWJbYV09YztlbHNlIHJldHVybiBiW2FdfSx0ZXh0OmZ1bmN0aW9uKCl7ZnVuY3Rpb24gYihhLGIpe2lmKEQoYikpe3ZhciBkPWEubm9kZVR5cGU7cmV0dXJuIGQ9PT1xYXx8ZD09PWJiP2EudGV4dENvbnRlbnQ6XCJcIn1hLnRleHRDb250ZW50PWJ9Yi4kZHY9XCJcIjtyZXR1cm4gYn0oKSx2YWw6ZnVuY3Rpb24oYixhKXtpZihEKGEpKXtpZihiLm11bHRpcGxlJiZcInNlbGVjdFwiPT09d2EoYikpe3ZhciBjPVtdO3IoYi5vcHRpb25zLGZ1bmN0aW9uKGEpe2Euc2VsZWN0ZWQmJmMucHVzaChhLnZhbHVlfHxhLnRleHQpfSk7cmV0dXJuIDA9PT1jLmxlbmd0aD9udWxsOmN9cmV0dXJuIGIudmFsdWV9Yi52YWx1ZT1hfSxodG1sOmZ1bmN0aW9uKGIsYSl7aWYoRChhKSlyZXR1cm4gYi5pbm5lckhUTUw7eGIoYiwhMCk7Yi5pbm5lckhUTUw9YX0sZW1wdHk6S2N9LGZ1bmN0aW9uKGIsYSl7VC5wcm90b3R5cGVbYV09XG5mdW5jdGlvbihhLGQpe3ZhciBlLGYsZz10aGlzLmxlbmd0aDtpZihiIT09S2MmJigyPT1iLmxlbmd0aCYmYiE9PUJiJiZiIT09SmM/YTpkKT09PXUpe2lmKEwoYSkpe2ZvcihlPTA7ZTxnO2UrKylpZihiPT09VmIpYih0aGlzW2VdLGEpO2Vsc2UgZm9yKGYgaW4gYSliKHRoaXNbZV0sZixhW2ZdKTtyZXR1cm4gdGhpc31lPWIuJGR2O2c9ZT09PXU/TWF0aC5taW4oZywxKTpnO2ZvcihmPTA7ZjxnO2YrKyl7dmFyIGg9Yih0aGlzW2ZdLGEsZCk7ZT1lP2UraDpofXJldHVybiBlfWZvcihlPTA7ZTxnO2UrKyliKHRoaXNbZV0sYSxkKTtyZXR1cm4gdGhpc319KTtyKHtyZW1vdmVEYXRhOnliLG9uOmZ1bmN0aW9uIGEoYyxkLGUsZil7aWYoeShmKSl0aHJvdyBUYihcIm9uYXJnc1wiKTtpZihGYyhjKSl7dmFyIGc9emIoYywhMCk7Zj1nLmV2ZW50czt2YXIgaD1nLmhhbmRsZTtofHwoaD1nLmhhbmRsZT1sZihjLGYpKTtmb3IodmFyIGc9MDw9ZC5pbmRleE9mKFwiIFwiKT9kLnNwbGl0KFwiIFwiKTpbZF0sXG5sPWcubGVuZ3RoO2wtLTspe2Q9Z1tsXTt2YXIgaz1mW2RdO2t8fChmW2RdPVtdLFwibW91c2VlbnRlclwiPT09ZHx8XCJtb3VzZWxlYXZlXCI9PT1kP2EoYyxRZltkXSxmdW5jdGlvbihhKXt2YXIgYz1hLnJlbGF0ZWRUYXJnZXQ7YyYmKGM9PT10aGlzfHx0aGlzLmNvbnRhaW5zKGMpKXx8aChhLGQpfSk6XCIkZGVzdHJveVwiIT09ZCYmYy5hZGRFdmVudExpc3RlbmVyKGQsaCwhMSksaz1mW2RdKTtrLnB1c2goZSl9fX0sb2ZmOkljLG9uZTpmdW5jdGlvbihhLGMsZCl7YT1CKGEpO2Eub24oYyxmdW5jdGlvbiBmKCl7YS5vZmYoYyxkKTthLm9mZihjLGYpfSk7YS5vbihjLGQpfSxyZXBsYWNlV2l0aDpmdW5jdGlvbihhLGMpe3ZhciBkLGU9YS5wYXJlbnROb2RlO3hiKGEpO3IobmV3IFQoYyksZnVuY3Rpb24oYyl7ZD9lLmluc2VydEJlZm9yZShjLGQubmV4dFNpYmxpbmcpOmUucmVwbGFjZUNoaWxkKGMsYSk7ZD1jfSl9LGNoaWxkcmVuOmZ1bmN0aW9uKGEpe3ZhciBjPVtdO3IoYS5jaGlsZE5vZGVzLFxuZnVuY3Rpb24oYSl7YS5ub2RlVHlwZT09PXFhJiZjLnB1c2goYSl9KTtyZXR1cm4gY30sY29udGVudHM6ZnVuY3Rpb24oYSl7cmV0dXJuIGEuY29udGVudERvY3VtZW50fHxhLmNoaWxkTm9kZXN8fFtdfSxhcHBlbmQ6ZnVuY3Rpb24oYSxjKXt2YXIgZD1hLm5vZGVUeXBlO2lmKGQ9PT1xYXx8MTE9PT1kKXtjPW5ldyBUKGMpO2Zvcih2YXIgZD0wLGU9Yy5sZW5ndGg7ZDxlO2QrKylhLmFwcGVuZENoaWxkKGNbZF0pfX0scHJlcGVuZDpmdW5jdGlvbihhLGMpe2lmKGEubm9kZVR5cGU9PT1xYSl7dmFyIGQ9YS5maXJzdENoaWxkO3IobmV3IFQoYyksZnVuY3Rpb24oYyl7YS5pbnNlcnRCZWZvcmUoYyxkKX0pfX0sd3JhcDpmdW5jdGlvbihhLGMpe2M9QihjKS5lcSgwKS5jbG9uZSgpWzBdO3ZhciBkPWEucGFyZW50Tm9kZTtkJiZkLnJlcGxhY2VDaGlsZChjLGEpO2MuYXBwZW5kQ2hpbGQoYSl9LHJlbW92ZTpMYyxkZXRhY2g6ZnVuY3Rpb24oYSl7TGMoYSwhMCl9LGFmdGVyOmZ1bmN0aW9uKGEsXG5jKXt2YXIgZD1hLGU9YS5wYXJlbnROb2RlO2M9bmV3IFQoYyk7Zm9yKHZhciBmPTAsZz1jLmxlbmd0aDtmPGc7ZisrKXt2YXIgaD1jW2ZdO2UuaW5zZXJ0QmVmb3JlKGgsZC5uZXh0U2libGluZyk7ZD1ofX0sYWRkQ2xhc3M6RGIscmVtb3ZlQ2xhc3M6Q2IsdG9nZ2xlQ2xhc3M6ZnVuY3Rpb24oYSxjLGQpe2MmJnIoYy5zcGxpdChcIiBcIiksZnVuY3Rpb24oYyl7dmFyIGY9ZDtEKGYpJiYoZj0hQmIoYSxjKSk7KGY/RGI6Q2IpKGEsYyl9KX0scGFyZW50OmZ1bmN0aW9uKGEpe3JldHVybihhPWEucGFyZW50Tm9kZSkmJjExIT09YS5ub2RlVHlwZT9hOm51bGx9LG5leHQ6ZnVuY3Rpb24oYSl7cmV0dXJuIGEubmV4dEVsZW1lbnRTaWJsaW5nfSxmaW5kOmZ1bmN0aW9uKGEsYyl7cmV0dXJuIGEuZ2V0RWxlbWVudHNCeVRhZ05hbWU/YS5nZXRFbGVtZW50c0J5VGFnTmFtZShjKTpbXX0sY2xvbmU6VWIsdHJpZ2dlckhhbmRsZXI6ZnVuY3Rpb24oYSxjLGQpe3ZhciBlLGYsZz1jLnR5cGV8fGMsaD1cbnpiKGEpO2lmKGg9KGg9aCYmaC5ldmVudHMpJiZoW2ddKWU9e3ByZXZlbnREZWZhdWx0OmZ1bmN0aW9uKCl7dGhpcy5kZWZhdWx0UHJldmVudGVkPSEwfSxpc0RlZmF1bHRQcmV2ZW50ZWQ6ZnVuY3Rpb24oKXtyZXR1cm4hMD09PXRoaXMuZGVmYXVsdFByZXZlbnRlZH0sc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uOmZ1bmN0aW9uKCl7dGhpcy5pbW1lZGlhdGVQcm9wYWdhdGlvblN0b3BwZWQ9ITB9LGlzSW1tZWRpYXRlUHJvcGFnYXRpb25TdG9wcGVkOmZ1bmN0aW9uKCl7cmV0dXJuITA9PT10aGlzLmltbWVkaWF0ZVByb3BhZ2F0aW9uU3RvcHBlZH0sc3RvcFByb3BhZ2F0aW9uOkEsdHlwZTpnLHRhcmdldDphfSxjLnR5cGUmJihlPXcoZSxjKSksYz1zYShoKSxmPWQ/W2VdLmNvbmNhdChkKTpbZV0scihjLGZ1bmN0aW9uKGMpe2UuaXNJbW1lZGlhdGVQcm9wYWdhdGlvblN0b3BwZWQoKXx8Yy5hcHBseShhLGYpfSl9fSxmdW5jdGlvbihhLGMpe1QucHJvdG90eXBlW2NdPWZ1bmN0aW9uKGMsXG5lLGYpe2Zvcih2YXIgZyxoPTAsbD10aGlzLmxlbmd0aDtoPGw7aCsrKUQoZyk/KGc9YSh0aGlzW2hdLGMsZSxmKSx5KGcpJiYoZz1CKGcpKSk6SGMoZyxhKHRoaXNbaF0sYyxlLGYpKTtyZXR1cm4geShnKT9nOnRoaXN9O1QucHJvdG90eXBlLmJpbmQ9VC5wcm90b3R5cGUub247VC5wcm90b3R5cGUudW5iaW5kPVQucHJvdG90eXBlLm9mZn0pO2diLnByb3RvdHlwZT17cHV0OmZ1bmN0aW9uKGEsYyl7dGhpc1tOYShhLHRoaXMubmV4dFVpZCldPWN9LGdldDpmdW5jdGlvbihhKXtyZXR1cm4gdGhpc1tOYShhLHRoaXMubmV4dFVpZCldfSxyZW1vdmU6ZnVuY3Rpb24oYSl7dmFyIGM9dGhpc1thPU5hKGEsdGhpcy5uZXh0VWlkKV07ZGVsZXRlIHRoaXNbYV07cmV0dXJuIGN9fTt2YXIgUWM9L15mdW5jdGlvblxccypbXlxcKF0qXFwoXFxzKihbXlxcKV0qKVxcKS9tLFJmPS8sLyxTZj0vXlxccyooXz8pKFxcUys/KVxcMVxccyokLyxQYz0vKChcXC9cXC8uKiQpfChcXC9cXCpbXFxzXFxTXSo/XFwqXFwvKSkvbWcsRmE9UyhcIiRpbmplY3RvclwiKTtcbmNiLiQkYW5ub3RhdGU9ZnVuY3Rpb24oYSxjLGQpe3ZhciBlO2lmKFwiZnVuY3Rpb25cIj09PXR5cGVvZiBhKXtpZighKGU9YS4kaW5qZWN0KSl7ZT1bXTtpZihhLmxlbmd0aCl7aWYoYyl0aHJvdyB4KGQpJiZkfHwoZD1hLm5hbWV8fG1mKGEpKSxGYShcInN0cmljdGRpXCIsZCk7Yz1hLnRvU3RyaW5nKCkucmVwbGFjZShQYyxcIlwiKTtjPWMubWF0Y2goUWMpO3IoY1sxXS5zcGxpdChSZiksZnVuY3Rpb24oYSl7YS5yZXBsYWNlKFNmLGZ1bmN0aW9uKGEsYyxkKXtlLnB1c2goZCl9KX0pfWEuJGluamVjdD1lfX1lbHNlIEgoYSk/KGM9YS5sZW5ndGgtMSxMYShhW2NdLFwiZm5cIiksZT1hLnNsaWNlKDAsYykpOkxhKGEsXCJmblwiLCEwKTtyZXR1cm4gZX07dmFyIFRmPVMoXCIkYW5pbWF0ZVwiKSxDZT1bXCIkcHJvdmlkZVwiLGZ1bmN0aW9uKGEpe3RoaXMuJCRzZWxlY3RvcnM9e307dGhpcy5yZWdpc3Rlcj1mdW5jdGlvbihjLGQpe3ZhciBlPWMrXCItYW5pbWF0aW9uXCI7aWYoYyYmXCIuXCIhPWMuY2hhckF0KDApKXRocm93IFRmKFwibm90Y3NlbFwiLFxuYyk7dGhpcy4kJHNlbGVjdG9yc1tjLnN1YnN0cigxKV09ZTthLmZhY3RvcnkoZSxkKX07dGhpcy5jbGFzc05hbWVGaWx0ZXI9ZnVuY3Rpb24oYSl7MT09PWFyZ3VtZW50cy5sZW5ndGgmJih0aGlzLiQkY2xhc3NOYW1lRmlsdGVyPWEgaW5zdGFuY2VvZiBSZWdFeHA/YTpudWxsKTtyZXR1cm4gdGhpcy4kJGNsYXNzTmFtZUZpbHRlcn07dGhpcy4kZ2V0PVtcIiQkcVwiLFwiJCRhc3luY0NhbGxiYWNrXCIsXCIkcm9vdFNjb3BlXCIsZnVuY3Rpb24oYSxkLGUpe2Z1bmN0aW9uIGYoZCl7dmFyIGYsZz1hLmRlZmVyKCk7Zy5wcm9taXNlLiQkY2FuY2VsRm49ZnVuY3Rpb24oKXtmJiZmKCl9O2UuJCRwb3N0RGlnZXN0KGZ1bmN0aW9uKCl7Zj1kKGZ1bmN0aW9uKCl7Zy5yZXNvbHZlKCl9KX0pO3JldHVybiBnLnByb21pc2V9ZnVuY3Rpb24gZyhhLGMpe3ZhciBkPVtdLGU9W10sZj1qYSgpO3IoKGEuYXR0cihcImNsYXNzXCIpfHxcIlwiKS5zcGxpdCgvXFxzKy8pLGZ1bmN0aW9uKGEpe2ZbYV09ITB9KTtyKGMsZnVuY3Rpb24oYSxcbmMpe3ZhciBnPWZbY107ITE9PT1hJiZnP2UucHVzaChjKTohMCE9PWF8fGd8fGQucHVzaChjKX0pO3JldHVybiAwPGQubGVuZ3RoK2UubGVuZ3RoJiZbZC5sZW5ndGg/ZDpudWxsLGUubGVuZ3RoP2U6bnVsbF19ZnVuY3Rpb24gaChhLGMsZCl7Zm9yKHZhciBlPTAsZj1jLmxlbmd0aDtlPGY7KytlKWFbY1tlXV09ZH1mdW5jdGlvbiBsKCl7bnx8KG49YS5kZWZlcigpLGQoZnVuY3Rpb24oKXtuLnJlc29sdmUoKTtuPW51bGx9KSk7cmV0dXJuIG4ucHJvbWlzZX1mdW5jdGlvbiBrKGEsYyl7aWYoY2EuaXNPYmplY3QoYykpe3ZhciBkPXcoYy5mcm9tfHx7fSxjLnRvfHx7fSk7YS5jc3MoZCl9fXZhciBuO3JldHVybnthbmltYXRlOmZ1bmN0aW9uKGEsYyxkKXtrKGEse2Zyb206Yyx0bzpkfSk7cmV0dXJuIGwoKX0sZW50ZXI6ZnVuY3Rpb24oYSxjLGQsZSl7ayhhLGUpO2Q/ZC5hZnRlcihhKTpjLnByZXBlbmQoYSk7cmV0dXJuIGwoKX0sbGVhdmU6ZnVuY3Rpb24oYSxjKXtrKGEsYyk7YS5yZW1vdmUoKTtcbnJldHVybiBsKCl9LG1vdmU6ZnVuY3Rpb24oYSxjLGQsZSl7cmV0dXJuIHRoaXMuZW50ZXIoYSxjLGQsZSl9LGFkZENsYXNzOmZ1bmN0aW9uKGEsYyxkKXtyZXR1cm4gdGhpcy5zZXRDbGFzcyhhLGMsW10sZCl9LCQkYWRkQ2xhc3NJbW1lZGlhdGVseTpmdW5jdGlvbihhLGMsZCl7YT1CKGEpO2M9eChjKT9jOkgoYyk/Yy5qb2luKFwiIFwiKTpcIlwiO3IoYSxmdW5jdGlvbihhKXtEYihhLGMpfSk7ayhhLGQpO3JldHVybiBsKCl9LHJlbW92ZUNsYXNzOmZ1bmN0aW9uKGEsYyxkKXtyZXR1cm4gdGhpcy5zZXRDbGFzcyhhLFtdLGMsZCl9LCQkcmVtb3ZlQ2xhc3NJbW1lZGlhdGVseTpmdW5jdGlvbihhLGMsZCl7YT1CKGEpO2M9eChjKT9jOkgoYyk/Yy5qb2luKFwiIFwiKTpcIlwiO3IoYSxmdW5jdGlvbihhKXtDYihhLGMpfSk7ayhhLGQpO3JldHVybiBsKCl9LHNldENsYXNzOmZ1bmN0aW9uKGEsYyxkLGUpe3ZhciBrPXRoaXMsbD0hMTthPUIoYSk7dmFyIG09YS5kYXRhKFwiJCRhbmltYXRlQ2xhc3Nlc1wiKTtcbm0/ZSYmbS5vcHRpb25zJiYobS5vcHRpb25zPWNhLmV4dGVuZChtLm9wdGlvbnN8fHt9LGUpKToobT17Y2xhc3Nlczp7fSxvcHRpb25zOmV9LGw9ITApO2U9bS5jbGFzc2VzO2M9SChjKT9jOmMuc3BsaXQoXCIgXCIpO2Q9SChkKT9kOmQuc3BsaXQoXCIgXCIpO2goZSxjLCEwKTtoKGUsZCwhMSk7bCYmKG0ucHJvbWlzZT1mKGZ1bmN0aW9uKGMpe3ZhciBkPWEuZGF0YShcIiQkYW5pbWF0ZUNsYXNzZXNcIik7YS5yZW1vdmVEYXRhKFwiJCRhbmltYXRlQ2xhc3Nlc1wiKTtpZihkKXt2YXIgZT1nKGEsZC5jbGFzc2VzKTtlJiZrLiQkc2V0Q2xhc3NJbW1lZGlhdGVseShhLGVbMF0sZVsxXSxkLm9wdGlvbnMpfWMoKX0pLGEuZGF0YShcIiQkYW5pbWF0ZUNsYXNzZXNcIixtKSk7cmV0dXJuIG0ucHJvbWlzZX0sJCRzZXRDbGFzc0ltbWVkaWF0ZWx5OmZ1bmN0aW9uKGEsYyxkLGUpe2MmJnRoaXMuJCRhZGRDbGFzc0ltbWVkaWF0ZWx5KGEsYyk7ZCYmdGhpcy4kJHJlbW92ZUNsYXNzSW1tZWRpYXRlbHkoYSxkKTtcbmsoYSxlKTtyZXR1cm4gbCgpfSxlbmFibGVkOkEsY2FuY2VsOkF9fV19XSxtYT1TKFwiJGNvbXBpbGVcIik7eGMuJGluamVjdD1bXCIkcHJvdmlkZVwiLFwiJCRzYW5pdGl6ZVVyaVByb3ZpZGVyXCJdO3ZhciBSYz0vXigoPzp4fGRhdGEpW1xcOlxcLV9dKS9pLHJmPVMoXCIkY29udHJvbGxlclwiKSxWYz1cImFwcGxpY2F0aW9uL2pzb25cIiwkYj17XCJDb250ZW50LVR5cGVcIjpWYytcIjtjaGFyc2V0PXV0Zi04XCJ9LHRmPS9eXFxbfF5cXHsoPyFcXHspLyx1Zj17XCJbXCI6L10kLyxcIntcIjovfSQvfSxzZj0vXlxcKVxcXVxcfScsP1xcbi8sYWM9UyhcIiRpbnRlcnBvbGF0ZVwiKSxVZj0vXihbXlxcPyNdKikoXFw/KFteI10qKSk/KCMoLiopKT8kLyx4Zj17aHR0cDo4MCxodHRwczo0NDMsZnRwOjIxfSxIYj1TKFwiJGxvY2F0aW9uXCIpLFZmPXskJGh0bWw1OiExLCQkcmVwbGFjZTohMSxhYnNVcmw6SWIoXCIkJGFic1VybFwiKSx1cmw6ZnVuY3Rpb24oYSl7aWYoRChhKSlyZXR1cm4gdGhpcy4kJHVybDt2YXIgYz1VZi5leGVjKGEpOyhjWzFdfHxcblwiXCI9PT1hKSYmdGhpcy5wYXRoKGRlY29kZVVSSUNvbXBvbmVudChjWzFdKSk7KGNbMl18fGNbMV18fFwiXCI9PT1hKSYmdGhpcy5zZWFyY2goY1szXXx8XCJcIik7dGhpcy5oYXNoKGNbNV18fFwiXCIpO3JldHVybiB0aGlzfSxwcm90b2NvbDpJYihcIiQkcHJvdG9jb2xcIiksaG9zdDpJYihcIiQkaG9zdFwiKSxwb3J0OkliKFwiJCRwb3J0XCIpLHBhdGg6Y2QoXCIkJHBhdGhcIixmdW5jdGlvbihhKXthPW51bGwhPT1hP2EudG9TdHJpbmcoKTpcIlwiO3JldHVyblwiL1wiPT1hLmNoYXJBdCgwKT9hOlwiL1wiK2F9KSxzZWFyY2g6ZnVuY3Rpb24oYSxjKXtzd2l0Y2goYXJndW1lbnRzLmxlbmd0aCl7Y2FzZSAwOnJldHVybiB0aGlzLiQkc2VhcmNoO2Nhc2UgMTppZih4KGEpfHxZKGEpKWE9YS50b1N0cmluZygpLHRoaXMuJCRzZWFyY2g9cmMoYSk7ZWxzZSBpZihMKGEpKWE9RGEoYSx7fSkscihhLGZ1bmN0aW9uKGMsZSl7bnVsbD09YyYmZGVsZXRlIGFbZV19KSx0aGlzLiQkc2VhcmNoPWE7ZWxzZSB0aHJvdyBIYihcImlzcmNoYXJnXCIpO1xuYnJlYWs7ZGVmYXVsdDpEKGMpfHxudWxsPT09Yz9kZWxldGUgdGhpcy4kJHNlYXJjaFthXTp0aGlzLiQkc2VhcmNoW2FdPWN9dGhpcy4kJGNvbXBvc2UoKTtyZXR1cm4gdGhpc30saGFzaDpjZChcIiQkaGFzaFwiLGZ1bmN0aW9uKGEpe3JldHVybiBudWxsIT09YT9hLnRvU3RyaW5nKCk6XCJcIn0pLHJlcGxhY2U6ZnVuY3Rpb24oKXt0aGlzLiQkcmVwbGFjZT0hMDtyZXR1cm4gdGhpc319O3IoW2JkLGRjLGNjXSxmdW5jdGlvbihhKXthLnByb3RvdHlwZT1PYmplY3QuY3JlYXRlKFZmKTthLnByb3RvdHlwZS5zdGF0ZT1mdW5jdGlvbihjKXtpZighYXJndW1lbnRzLmxlbmd0aClyZXR1cm4gdGhpcy4kJHN0YXRlO2lmKGEhPT1jY3x8IXRoaXMuJCRodG1sNSl0aHJvdyBIYihcIm5vc3RhdGVcIik7dGhpcy4kJHN0YXRlPUQoYyk/bnVsbDpjO3JldHVybiB0aGlzfX0pO3ZhciBnYT1TKFwiJHBhcnNlXCIpLFdmPUZ1bmN0aW9uLnByb3RvdHlwZS5jYWxsLFhmPUZ1bmN0aW9uLnByb3RvdHlwZS5hcHBseSxcbllmPUZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLHBiPWphKCk7cih7XCJudWxsXCI6ZnVuY3Rpb24oKXtyZXR1cm4gbnVsbH0sXCJ0cnVlXCI6ZnVuY3Rpb24oKXtyZXR1cm4hMH0sXCJmYWxzZVwiOmZ1bmN0aW9uKCl7cmV0dXJuITF9LHVuZGVmaW5lZDpmdW5jdGlvbigpe319LGZ1bmN0aW9uKGEsYyl7YS5jb25zdGFudD1hLmxpdGVyYWw9YS5zaGFyZWRHZXR0ZXI9ITA7cGJbY109YX0pO3BiW1widGhpc1wiXT1mdW5jdGlvbihhKXtyZXR1cm4gYX07cGJbXCJ0aGlzXCJdLnNoYXJlZEdldHRlcj0hMDt2YXIgcWI9dyhqYSgpLHtcIitcIjpmdW5jdGlvbihhLGMsZCxlKXtkPWQoYSxjKTtlPWUoYSxjKTtyZXR1cm4geShkKT95KGUpP2QrZTpkOnkoZSk/ZTp1fSxcIi1cIjpmdW5jdGlvbihhLGMsZCxlKXtkPWQoYSxjKTtlPWUoYSxjKTtyZXR1cm4oeShkKT9kOjApLSh5KGUpP2U6MCl9LFwiKlwiOmZ1bmN0aW9uKGEsYyxkLGUpe3JldHVybiBkKGEsYykqZShhLGMpfSxcIi9cIjpmdW5jdGlvbihhLGMsZCxlKXtyZXR1cm4gZChhLFxuYykvZShhLGMpfSxcIiVcIjpmdW5jdGlvbihhLGMsZCxlKXtyZXR1cm4gZChhLGMpJWUoYSxjKX0sXCI9PT1cIjpmdW5jdGlvbihhLGMsZCxlKXtyZXR1cm4gZChhLGMpPT09ZShhLGMpfSxcIiE9PVwiOmZ1bmN0aW9uKGEsYyxkLGUpe3JldHVybiBkKGEsYykhPT1lKGEsYyl9LFwiPT1cIjpmdW5jdGlvbihhLGMsZCxlKXtyZXR1cm4gZChhLGMpPT1lKGEsYyl9LFwiIT1cIjpmdW5jdGlvbihhLGMsZCxlKXtyZXR1cm4gZChhLGMpIT1lKGEsYyl9LFwiPFwiOmZ1bmN0aW9uKGEsYyxkLGUpe3JldHVybiBkKGEsYyk8ZShhLGMpfSxcIj5cIjpmdW5jdGlvbihhLGMsZCxlKXtyZXR1cm4gZChhLGMpPmUoYSxjKX0sXCI8PVwiOmZ1bmN0aW9uKGEsYyxkLGUpe3JldHVybiBkKGEsYyk8PWUoYSxjKX0sXCI+PVwiOmZ1bmN0aW9uKGEsYyxkLGUpe3JldHVybiBkKGEsYyk+PWUoYSxjKX0sXCImJlwiOmZ1bmN0aW9uKGEsYyxkLGUpe3JldHVybiBkKGEsYykmJmUoYSxjKX0sXCJ8fFwiOmZ1bmN0aW9uKGEsYyxkLGUpe3JldHVybiBkKGEsXG5jKXx8ZShhLGMpfSxcIiFcIjpmdW5jdGlvbihhLGMsZCl7cmV0dXJuIWQoYSxjKX0sXCI9XCI6ITAsXCJ8XCI6ITB9KSxaZj17bjpcIlxcblwiLGY6XCJcXGZcIixyOlwiXFxyXCIsdDpcIlxcdFwiLHY6XCJcXHZcIixcIidcIjpcIidcIiwnXCInOidcIid9LGdjPWZ1bmN0aW9uKGEpe3RoaXMub3B0aW9ucz1hfTtnYy5wcm90b3R5cGU9e2NvbnN0cnVjdG9yOmdjLGxleDpmdW5jdGlvbihhKXt0aGlzLnRleHQ9YTt0aGlzLmluZGV4PTA7Zm9yKHRoaXMudG9rZW5zPVtdO3RoaXMuaW5kZXg8dGhpcy50ZXh0Lmxlbmd0aDspaWYoYT10aGlzLnRleHQuY2hhckF0KHRoaXMuaW5kZXgpLCdcIic9PT1hfHxcIidcIj09PWEpdGhpcy5yZWFkU3RyaW5nKGEpO2Vsc2UgaWYodGhpcy5pc051bWJlcihhKXx8XCIuXCI9PT1hJiZ0aGlzLmlzTnVtYmVyKHRoaXMucGVlaygpKSl0aGlzLnJlYWROdW1iZXIoKTtlbHNlIGlmKHRoaXMuaXNJZGVudChhKSl0aGlzLnJlYWRJZGVudCgpO2Vsc2UgaWYodGhpcy5pcyhhLFwiKCl7fVtdLiw7Oj9cIikpdGhpcy50b2tlbnMucHVzaCh7aW5kZXg6dGhpcy5pbmRleCxcbnRleHQ6YX0pLHRoaXMuaW5kZXgrKztlbHNlIGlmKHRoaXMuaXNXaGl0ZXNwYWNlKGEpKXRoaXMuaW5kZXgrKztlbHNle3ZhciBjPWErdGhpcy5wZWVrKCksZD1jK3RoaXMucGVlaygyKSxlPXFiW2NdLGY9cWJbZF07cWJbYV18fGV8fGY/KGE9Zj9kOmU/YzphLHRoaXMudG9rZW5zLnB1c2goe2luZGV4OnRoaXMuaW5kZXgsdGV4dDphLG9wZXJhdG9yOiEwfSksdGhpcy5pbmRleCs9YS5sZW5ndGgpOnRoaXMudGhyb3dFcnJvcihcIlVuZXhwZWN0ZWQgbmV4dCBjaGFyYWN0ZXIgXCIsdGhpcy5pbmRleCx0aGlzLmluZGV4KzEpfXJldHVybiB0aGlzLnRva2Vuc30saXM6ZnVuY3Rpb24oYSxjKXtyZXR1cm4tMSE9PWMuaW5kZXhPZihhKX0scGVlazpmdW5jdGlvbihhKXthPWF8fDE7cmV0dXJuIHRoaXMuaW5kZXgrYTx0aGlzLnRleHQubGVuZ3RoP3RoaXMudGV4dC5jaGFyQXQodGhpcy5pbmRleCthKTohMX0saXNOdW1iZXI6ZnVuY3Rpb24oYSl7cmV0dXJuXCIwXCI8PWEmJlwiOVwiPj1hJiZcInN0cmluZ1wiPT09XG50eXBlb2YgYX0saXNXaGl0ZXNwYWNlOmZ1bmN0aW9uKGEpe3JldHVyblwiIFwiPT09YXx8XCJcXHJcIj09PWF8fFwiXFx0XCI9PT1hfHxcIlxcblwiPT09YXx8XCJcXHZcIj09PWF8fFwiXFx1MDBhMFwiPT09YX0saXNJZGVudDpmdW5jdGlvbihhKXtyZXR1cm5cImFcIjw9YSYmXCJ6XCI+PWF8fFwiQVwiPD1hJiZcIlpcIj49YXx8XCJfXCI9PT1hfHxcIiRcIj09PWF9LGlzRXhwT3BlcmF0b3I6ZnVuY3Rpb24oYSl7cmV0dXJuXCItXCI9PT1hfHxcIitcIj09PWF8fHRoaXMuaXNOdW1iZXIoYSl9LHRocm93RXJyb3I6ZnVuY3Rpb24oYSxjLGQpe2Q9ZHx8dGhpcy5pbmRleDtjPXkoYyk/XCJzIFwiK2MrXCItXCIrdGhpcy5pbmRleCtcIiBbXCIrdGhpcy50ZXh0LnN1YnN0cmluZyhjLGQpK1wiXVwiOlwiIFwiK2Q7dGhyb3cgZ2EoXCJsZXhlcnJcIixhLGMsdGhpcy50ZXh0KTt9LHJlYWROdW1iZXI6ZnVuY3Rpb24oKXtmb3IodmFyIGE9XCJcIixjPXRoaXMuaW5kZXg7dGhpcy5pbmRleDx0aGlzLnRleHQubGVuZ3RoOyl7dmFyIGQ9Syh0aGlzLnRleHQuY2hhckF0KHRoaXMuaW5kZXgpKTtcbmlmKFwiLlwiPT1kfHx0aGlzLmlzTnVtYmVyKGQpKWErPWQ7ZWxzZXt2YXIgZT10aGlzLnBlZWsoKTtpZihcImVcIj09ZCYmdGhpcy5pc0V4cE9wZXJhdG9yKGUpKWErPWQ7ZWxzZSBpZih0aGlzLmlzRXhwT3BlcmF0b3IoZCkmJmUmJnRoaXMuaXNOdW1iZXIoZSkmJlwiZVwiPT1hLmNoYXJBdChhLmxlbmd0aC0xKSlhKz1kO2Vsc2UgaWYoIXRoaXMuaXNFeHBPcGVyYXRvcihkKXx8ZSYmdGhpcy5pc051bWJlcihlKXx8XCJlXCIhPWEuY2hhckF0KGEubGVuZ3RoLTEpKWJyZWFrO2Vsc2UgdGhpcy50aHJvd0Vycm9yKFwiSW52YWxpZCBleHBvbmVudFwiKX10aGlzLmluZGV4Kyt9dGhpcy50b2tlbnMucHVzaCh7aW5kZXg6Yyx0ZXh0OmEsY29uc3RhbnQ6ITAsdmFsdWU6TnVtYmVyKGEpfSl9LHJlYWRJZGVudDpmdW5jdGlvbigpe2Zvcih2YXIgYT10aGlzLmluZGV4O3RoaXMuaW5kZXg8dGhpcy50ZXh0Lmxlbmd0aDspe3ZhciBjPXRoaXMudGV4dC5jaGFyQXQodGhpcy5pbmRleCk7aWYoIXRoaXMuaXNJZGVudChjKSYmXG4hdGhpcy5pc051bWJlcihjKSlicmVhazt0aGlzLmluZGV4Kyt9dGhpcy50b2tlbnMucHVzaCh7aW5kZXg6YSx0ZXh0OnRoaXMudGV4dC5zbGljZShhLHRoaXMuaW5kZXgpLGlkZW50aWZpZXI6ITB9KX0scmVhZFN0cmluZzpmdW5jdGlvbihhKXt2YXIgYz10aGlzLmluZGV4O3RoaXMuaW5kZXgrKztmb3IodmFyIGQ9XCJcIixlPWEsZj0hMTt0aGlzLmluZGV4PHRoaXMudGV4dC5sZW5ndGg7KXt2YXIgZz10aGlzLnRleHQuY2hhckF0KHRoaXMuaW5kZXgpLGU9ZStnO2lmKGYpXCJ1XCI9PT1nPyhmPXRoaXMudGV4dC5zdWJzdHJpbmcodGhpcy5pbmRleCsxLHRoaXMuaW5kZXgrNSksZi5tYXRjaCgvW1xcZGEtZl17NH0vaSl8fHRoaXMudGhyb3dFcnJvcihcIkludmFsaWQgdW5pY29kZSBlc2NhcGUgW1xcXFx1XCIrZitcIl1cIiksdGhpcy5pbmRleCs9NCxkKz1TdHJpbmcuZnJvbUNoYXJDb2RlKHBhcnNlSW50KGYsMTYpKSk6ZCs9WmZbZ118fGcsZj0hMTtlbHNlIGlmKFwiXFxcXFwiPT09ZylmPSEwO2Vsc2V7aWYoZz09PVxuYSl7dGhpcy5pbmRleCsrO3RoaXMudG9rZW5zLnB1c2goe2luZGV4OmMsdGV4dDplLGNvbnN0YW50OiEwLHZhbHVlOmR9KTtyZXR1cm59ZCs9Z310aGlzLmluZGV4Kyt9dGhpcy50aHJvd0Vycm9yKFwiVW50ZXJtaW5hdGVkIHF1b3RlXCIsYyl9fTt2YXIga2I9ZnVuY3Rpb24oYSxjLGQpe3RoaXMubGV4ZXI9YTt0aGlzLiRmaWx0ZXI9Yzt0aGlzLm9wdGlvbnM9ZH07a2IuWkVSTz13KGZ1bmN0aW9uKCl7cmV0dXJuIDB9LHtzaGFyZWRHZXR0ZXI6ITAsY29uc3RhbnQ6ITB9KTtrYi5wcm90b3R5cGU9e2NvbnN0cnVjdG9yOmtiLHBhcnNlOmZ1bmN0aW9uKGEpe3RoaXMudGV4dD1hO3RoaXMudG9rZW5zPXRoaXMubGV4ZXIubGV4KGEpO2E9dGhpcy5zdGF0ZW1lbnRzKCk7MCE9PXRoaXMudG9rZW5zLmxlbmd0aCYmdGhpcy50aHJvd0Vycm9yKFwiaXMgYW4gdW5leHBlY3RlZCB0b2tlblwiLHRoaXMudG9rZW5zWzBdKTthLmxpdGVyYWw9ISFhLmxpdGVyYWw7YS5jb25zdGFudD0hIWEuY29uc3RhbnQ7XG5yZXR1cm4gYX0scHJpbWFyeTpmdW5jdGlvbigpe3ZhciBhO3RoaXMuZXhwZWN0KFwiKFwiKT8oYT10aGlzLmZpbHRlckNoYWluKCksdGhpcy5jb25zdW1lKFwiKVwiKSk6dGhpcy5leHBlY3QoXCJbXCIpP2E9dGhpcy5hcnJheURlY2xhcmF0aW9uKCk6dGhpcy5leHBlY3QoXCJ7XCIpP2E9dGhpcy5vYmplY3QoKTp0aGlzLnBlZWsoKS5pZGVudGlmaWVyJiZ0aGlzLnBlZWsoKS50ZXh0IGluIHBiP2E9cGJbdGhpcy5jb25zdW1lKCkudGV4dF06dGhpcy5wZWVrKCkuaWRlbnRpZmllcj9hPXRoaXMuaWRlbnRpZmllcigpOnRoaXMucGVlaygpLmNvbnN0YW50P2E9dGhpcy5jb25zdGFudCgpOnRoaXMudGhyb3dFcnJvcihcIm5vdCBhIHByaW1hcnkgZXhwcmVzc2lvblwiLHRoaXMucGVlaygpKTtmb3IodmFyIGMsZDtjPXRoaXMuZXhwZWN0KFwiKFwiLFwiW1wiLFwiLlwiKTspXCIoXCI9PT1jLnRleHQ/KGE9dGhpcy5mdW5jdGlvbkNhbGwoYSxkKSxkPW51bGwpOlwiW1wiPT09Yy50ZXh0PyhkPWEsYT10aGlzLm9iamVjdEluZGV4KGEpKTpcblwiLlwiPT09Yy50ZXh0PyhkPWEsYT10aGlzLmZpZWxkQWNjZXNzKGEpKTp0aGlzLnRocm93RXJyb3IoXCJJTVBPU1NJQkxFXCIpO3JldHVybiBhfSx0aHJvd0Vycm9yOmZ1bmN0aW9uKGEsYyl7dGhyb3cgZ2EoXCJzeW50YXhcIixjLnRleHQsYSxjLmluZGV4KzEsdGhpcy50ZXh0LHRoaXMudGV4dC5zdWJzdHJpbmcoYy5pbmRleCkpO30scGVla1Rva2VuOmZ1bmN0aW9uKCl7aWYoMD09PXRoaXMudG9rZW5zLmxlbmd0aCl0aHJvdyBnYShcInVlb2VcIix0aGlzLnRleHQpO3JldHVybiB0aGlzLnRva2Vuc1swXX0scGVlazpmdW5jdGlvbihhLGMsZCxlKXtyZXR1cm4gdGhpcy5wZWVrQWhlYWQoMCxhLGMsZCxlKX0scGVla0FoZWFkOmZ1bmN0aW9uKGEsYyxkLGUsZil7aWYodGhpcy50b2tlbnMubGVuZ3RoPmEpe2E9dGhpcy50b2tlbnNbYV07dmFyIGc9YS50ZXh0O2lmKGc9PT1jfHxnPT09ZHx8Zz09PWV8fGc9PT1mfHwhKGN8fGR8fGV8fGYpKXJldHVybiBhfXJldHVybiExfSxleHBlY3Q6ZnVuY3Rpb24oYSxcbmMsZCxlKXtyZXR1cm4oYT10aGlzLnBlZWsoYSxjLGQsZSkpPyh0aGlzLnRva2Vucy5zaGlmdCgpLGEpOiExfSxjb25zdW1lOmZ1bmN0aW9uKGEpe2lmKDA9PT10aGlzLnRva2Vucy5sZW5ndGgpdGhyb3cgZ2EoXCJ1ZW9lXCIsdGhpcy50ZXh0KTt2YXIgYz10aGlzLmV4cGVjdChhKTtjfHx0aGlzLnRocm93RXJyb3IoXCJpcyB1bmV4cGVjdGVkLCBleHBlY3RpbmcgW1wiK2ErXCJdXCIsdGhpcy5wZWVrKCkpO3JldHVybiBjfSx1bmFyeUZuOmZ1bmN0aW9uKGEsYyl7dmFyIGQ9cWJbYV07cmV0dXJuIHcoZnVuY3Rpb24oYSxmKXtyZXR1cm4gZChhLGYsYyl9LHtjb25zdGFudDpjLmNvbnN0YW50LGlucHV0czpbY119KX0sYmluYXJ5Rm46ZnVuY3Rpb24oYSxjLGQsZSl7dmFyIGY9cWJbY107cmV0dXJuIHcoZnVuY3Rpb24oYyxlKXtyZXR1cm4gZihjLGUsYSxkKX0se2NvbnN0YW50OmEuY29uc3RhbnQmJmQuY29uc3RhbnQsaW5wdXRzOiFlJiZbYSxkXX0pfSxpZGVudGlmaWVyOmZ1bmN0aW9uKCl7Zm9yKHZhciBhPVxudGhpcy5jb25zdW1lKCkudGV4dDt0aGlzLnBlZWsoXCIuXCIpJiZ0aGlzLnBlZWtBaGVhZCgxKS5pZGVudGlmaWVyJiYhdGhpcy5wZWVrQWhlYWQoMixcIihcIik7KWErPXRoaXMuY29uc3VtZSgpLnRleHQrdGhpcy5jb25zdW1lKCkudGV4dDtyZXR1cm4gemYoYSx0aGlzLm9wdGlvbnMsdGhpcy50ZXh0KX0sY29uc3RhbnQ6ZnVuY3Rpb24oKXt2YXIgYT10aGlzLmNvbnN1bWUoKS52YWx1ZTtyZXR1cm4gdyhmdW5jdGlvbigpe3JldHVybiBhfSx7Y29uc3RhbnQ6ITAsbGl0ZXJhbDohMH0pfSxzdGF0ZW1lbnRzOmZ1bmN0aW9uKCl7Zm9yKHZhciBhPVtdOzspaWYoMDx0aGlzLnRva2Vucy5sZW5ndGgmJiF0aGlzLnBlZWsoXCJ9XCIsXCIpXCIsXCI7XCIsXCJdXCIpJiZhLnB1c2godGhpcy5maWx0ZXJDaGFpbigpKSwhdGhpcy5leHBlY3QoXCI7XCIpKXJldHVybiAxPT09YS5sZW5ndGg/YVswXTpmdW5jdGlvbihjLGQpe2Zvcih2YXIgZSxmPTAsZz1hLmxlbmd0aDtmPGc7ZisrKWU9YVtmXShjLGQpO3JldHVybiBlfX0sXG5maWx0ZXJDaGFpbjpmdW5jdGlvbigpe2Zvcih2YXIgYT10aGlzLmV4cHJlc3Npb24oKTt0aGlzLmV4cGVjdChcInxcIik7KWE9dGhpcy5maWx0ZXIoYSk7cmV0dXJuIGF9LGZpbHRlcjpmdW5jdGlvbihhKXt2YXIgYz10aGlzLiRmaWx0ZXIodGhpcy5jb25zdW1lKCkudGV4dCksZCxlO2lmKHRoaXMucGVlayhcIjpcIikpZm9yKGQ9W10sZT1bXTt0aGlzLmV4cGVjdChcIjpcIik7KWQucHVzaCh0aGlzLmV4cHJlc3Npb24oKSk7dmFyIGY9W2FdLmNvbmNhdChkfHxbXSk7cmV0dXJuIHcoZnVuY3Rpb24oZixoKXt2YXIgbD1hKGYsaCk7aWYoZSl7ZVswXT1sO2ZvcihsPWQubGVuZ3RoO2wtLTspZVtsKzFdPWRbbF0oZixoKTtyZXR1cm4gYy5hcHBseSh1LGUpfXJldHVybiBjKGwpfSx7Y29uc3RhbnQ6IWMuJHN0YXRlZnVsJiZmLmV2ZXJ5KGVjKSxpbnB1dHM6IWMuJHN0YXRlZnVsJiZmfSl9LGV4cHJlc3Npb246ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5hc3NpZ25tZW50KCl9LGFzc2lnbm1lbnQ6ZnVuY3Rpb24oKXt2YXIgYT1cbnRoaXMudGVybmFyeSgpLGMsZDtyZXR1cm4oZD10aGlzLmV4cGVjdChcIj1cIikpPyhhLmFzc2lnbnx8dGhpcy50aHJvd0Vycm9yKFwiaW1wbGllcyBhc3NpZ25tZW50IGJ1dCBbXCIrdGhpcy50ZXh0LnN1YnN0cmluZygwLGQuaW5kZXgpK1wiXSBjYW4gbm90IGJlIGFzc2lnbmVkIHRvXCIsZCksYz10aGlzLnRlcm5hcnkoKSx3KGZ1bmN0aW9uKGQsZil7cmV0dXJuIGEuYXNzaWduKGQsYyhkLGYpLGYpfSx7aW5wdXRzOlthLGNdfSkpOmF9LHRlcm5hcnk6ZnVuY3Rpb24oKXt2YXIgYT10aGlzLmxvZ2ljYWxPUigpLGM7aWYodGhpcy5leHBlY3QoXCI/XCIpJiYoYz10aGlzLmFzc2lnbm1lbnQoKSx0aGlzLmNvbnN1bWUoXCI6XCIpKSl7dmFyIGQ9dGhpcy5hc3NpZ25tZW50KCk7cmV0dXJuIHcoZnVuY3Rpb24oZSxmKXtyZXR1cm4gYShlLGYpP2MoZSxmKTpkKGUsZil9LHtjb25zdGFudDphLmNvbnN0YW50JiZjLmNvbnN0YW50JiZkLmNvbnN0YW50fSl9cmV0dXJuIGF9LGxvZ2ljYWxPUjpmdW5jdGlvbigpe2Zvcih2YXIgYT1cbnRoaXMubG9naWNhbEFORCgpLGM7Yz10aGlzLmV4cGVjdChcInx8XCIpOylhPXRoaXMuYmluYXJ5Rm4oYSxjLnRleHQsdGhpcy5sb2dpY2FsQU5EKCksITApO3JldHVybiBhfSxsb2dpY2FsQU5EOmZ1bmN0aW9uKCl7Zm9yKHZhciBhPXRoaXMuZXF1YWxpdHkoKSxjO2M9dGhpcy5leHBlY3QoXCImJlwiKTspYT10aGlzLmJpbmFyeUZuKGEsYy50ZXh0LHRoaXMuZXF1YWxpdHkoKSwhMCk7cmV0dXJuIGF9LGVxdWFsaXR5OmZ1bmN0aW9uKCl7Zm9yKHZhciBhPXRoaXMucmVsYXRpb25hbCgpLGM7Yz10aGlzLmV4cGVjdChcIj09XCIsXCIhPVwiLFwiPT09XCIsXCIhPT1cIik7KWE9dGhpcy5iaW5hcnlGbihhLGMudGV4dCx0aGlzLnJlbGF0aW9uYWwoKSk7cmV0dXJuIGF9LHJlbGF0aW9uYWw6ZnVuY3Rpb24oKXtmb3IodmFyIGE9dGhpcy5hZGRpdGl2ZSgpLGM7Yz10aGlzLmV4cGVjdChcIjxcIixcIj5cIixcIjw9XCIsXCI+PVwiKTspYT10aGlzLmJpbmFyeUZuKGEsYy50ZXh0LHRoaXMuYWRkaXRpdmUoKSk7cmV0dXJuIGF9LFxuYWRkaXRpdmU6ZnVuY3Rpb24oKXtmb3IodmFyIGE9dGhpcy5tdWx0aXBsaWNhdGl2ZSgpLGM7Yz10aGlzLmV4cGVjdChcIitcIixcIi1cIik7KWE9dGhpcy5iaW5hcnlGbihhLGMudGV4dCx0aGlzLm11bHRpcGxpY2F0aXZlKCkpO3JldHVybiBhfSxtdWx0aXBsaWNhdGl2ZTpmdW5jdGlvbigpe2Zvcih2YXIgYT10aGlzLnVuYXJ5KCksYztjPXRoaXMuZXhwZWN0KFwiKlwiLFwiL1wiLFwiJVwiKTspYT10aGlzLmJpbmFyeUZuKGEsYy50ZXh0LHRoaXMudW5hcnkoKSk7cmV0dXJuIGF9LHVuYXJ5OmZ1bmN0aW9uKCl7dmFyIGE7cmV0dXJuIHRoaXMuZXhwZWN0KFwiK1wiKT90aGlzLnByaW1hcnkoKTooYT10aGlzLmV4cGVjdChcIi1cIikpP3RoaXMuYmluYXJ5Rm4oa2IuWkVSTyxhLnRleHQsdGhpcy51bmFyeSgpKTooYT10aGlzLmV4cGVjdChcIiFcIikpP3RoaXMudW5hcnlGbihhLnRleHQsdGhpcy51bmFyeSgpKTp0aGlzLnByaW1hcnkoKX0sZmllbGRBY2Nlc3M6ZnVuY3Rpb24oYSl7dmFyIGM9dGhpcy5pZGVudGlmaWVyKCk7XG5yZXR1cm4gdyhmdW5jdGlvbihkLGUsZil7ZD1mfHxhKGQsZSk7cmV0dXJuIG51bGw9PWQ/dTpjKGQpfSx7YXNzaWduOmZ1bmN0aW9uKGQsZSxmKXt2YXIgZz1hKGQsZik7Z3x8YS5hc3NpZ24oZCxnPXt9LGYpO3JldHVybiBjLmFzc2lnbihnLGUpfX0pfSxvYmplY3RJbmRleDpmdW5jdGlvbihhKXt2YXIgYz10aGlzLnRleHQsZD10aGlzLmV4cHJlc3Npb24oKTt0aGlzLmNvbnN1bWUoXCJdXCIpO3JldHVybiB3KGZ1bmN0aW9uKGUsZil7dmFyIGc9YShlLGYpLGg9ZGQoZChlLGYpLGMpO3ZhKGgsYyk7cmV0dXJuIGc/b2EoZ1toXSxjKTp1fSx7YXNzaWduOmZ1bmN0aW9uKGUsZixnKXt2YXIgaD12YShkZChkKGUsZyksYyksYyksbD1vYShhKGUsZyksYyk7bHx8YS5hc3NpZ24oZSxsPXt9LGcpO3JldHVybiBsW2hdPWZ9fSl9LGZ1bmN0aW9uQ2FsbDpmdW5jdGlvbihhLGMpe3ZhciBkPVtdO2lmKFwiKVwiIT09dGhpcy5wZWVrVG9rZW4oKS50ZXh0KXtkbyBkLnB1c2godGhpcy5leHByZXNzaW9uKCkpO1xud2hpbGUodGhpcy5leHBlY3QoXCIsXCIpKX10aGlzLmNvbnN1bWUoXCIpXCIpO3ZhciBlPXRoaXMudGV4dCxmPWQubGVuZ3RoP1tdOm51bGw7cmV0dXJuIGZ1bmN0aW9uKGcsaCl7dmFyIGw9Yz9jKGcsaCk6eShjKT91Omcsaz1hKGcsaCxsKXx8QTtpZihmKWZvcih2YXIgbj1kLmxlbmd0aDtuLS07KWZbbl09b2EoZFtuXShnLGgpLGUpO29hKGwsZSk7aWYoayl7aWYoay5jb25zdHJ1Y3Rvcj09PWspdGhyb3cgZ2EoXCJpc2VjZm5cIixlKTtpZihrPT09V2Z8fGs9PT1YZnx8az09PVlmKXRocm93IGdhKFwiaXNlY2ZmXCIsZSk7fWw9ay5hcHBseT9rLmFwcGx5KGwsZik6ayhmWzBdLGZbMV0sZlsyXSxmWzNdLGZbNF0pO2YmJihmLmxlbmd0aD0wKTtyZXR1cm4gb2EobCxlKX19LGFycmF5RGVjbGFyYXRpb246ZnVuY3Rpb24oKXt2YXIgYT1bXTtpZihcIl1cIiE9PXRoaXMucGVla1Rva2VuKCkudGV4dCl7ZG97aWYodGhpcy5wZWVrKFwiXVwiKSlicmVhazthLnB1c2godGhpcy5leHByZXNzaW9uKCkpfXdoaWxlKHRoaXMuZXhwZWN0KFwiLFwiKSlcbn10aGlzLmNvbnN1bWUoXCJdXCIpO3JldHVybiB3KGZ1bmN0aW9uKGMsZCl7Zm9yKHZhciBlPVtdLGY9MCxnPWEubGVuZ3RoO2Y8ZztmKyspZS5wdXNoKGFbZl0oYyxkKSk7cmV0dXJuIGV9LHtsaXRlcmFsOiEwLGNvbnN0YW50OmEuZXZlcnkoZWMpLGlucHV0czphfSl9LG9iamVjdDpmdW5jdGlvbigpe3ZhciBhPVtdLGM9W107aWYoXCJ9XCIhPT10aGlzLnBlZWtUb2tlbigpLnRleHQpe2Rve2lmKHRoaXMucGVlayhcIn1cIikpYnJlYWs7dmFyIGQ9dGhpcy5jb25zdW1lKCk7ZC5jb25zdGFudD9hLnB1c2goZC52YWx1ZSk6ZC5pZGVudGlmaWVyP2EucHVzaChkLnRleHQpOnRoaXMudGhyb3dFcnJvcihcImludmFsaWQga2V5XCIsZCk7dGhpcy5jb25zdW1lKFwiOlwiKTtjLnB1c2godGhpcy5leHByZXNzaW9uKCkpfXdoaWxlKHRoaXMuZXhwZWN0KFwiLFwiKSl9dGhpcy5jb25zdW1lKFwifVwiKTtyZXR1cm4gdyhmdW5jdGlvbihkLGYpe2Zvcih2YXIgZz17fSxoPTAsbD1jLmxlbmd0aDtoPGw7aCsrKWdbYVtoXV09XG5jW2hdKGQsZik7cmV0dXJuIGd9LHtsaXRlcmFsOiEwLGNvbnN0YW50OmMuZXZlcnkoZWMpLGlucHV0czpjfSl9fTt2YXIgQmY9amEoKSxBZj1qYSgpLENmPU9iamVjdC5wcm90b3R5cGUudmFsdWVPZixCYT1TKFwiJHNjZVwiKSxwYT17SFRNTDpcImh0bWxcIixDU1M6XCJjc3NcIixVUkw6XCJ1cmxcIixSRVNPVVJDRV9VUkw6XCJyZXNvdXJjZVVybFwiLEpTOlwianNcIn0sbWE9UyhcIiRjb21waWxlXCIpLFo9Vy5jcmVhdGVFbGVtZW50KFwiYVwiKSxpZD1BYShSLmxvY2F0aW9uLmhyZWYpO0VjLiRpbmplY3Q9W1wiJHByb3ZpZGVcIl07amQuJGluamVjdD1bXCIkbG9jYWxlXCJdO2xkLiRpbmplY3Q9W1wiJGxvY2FsZVwiXTt2YXIgb2Q9XCIuXCIsTWY9e3l5eXk6VShcIkZ1bGxZZWFyXCIsNCkseXk6VShcIkZ1bGxZZWFyXCIsMiwwLCEwKSx5OlUoXCJGdWxsWWVhclwiLDEpLE1NTU06S2IoXCJNb250aFwiKSxNTU06S2IoXCJNb250aFwiLCEwKSxNTTpVKFwiTW9udGhcIiwyLDEpLE06VShcIk1vbnRoXCIsMSwxKSxkZDpVKFwiRGF0ZVwiLDIpLGQ6VShcIkRhdGVcIixcbjEpLEhIOlUoXCJIb3Vyc1wiLDIpLEg6VShcIkhvdXJzXCIsMSksaGg6VShcIkhvdXJzXCIsMiwtMTIpLGg6VShcIkhvdXJzXCIsMSwtMTIpLG1tOlUoXCJNaW51dGVzXCIsMiksbTpVKFwiTWludXRlc1wiLDEpLHNzOlUoXCJTZWNvbmRzXCIsMiksczpVKFwiU2Vjb25kc1wiLDEpLHNzczpVKFwiTWlsbGlzZWNvbmRzXCIsMyksRUVFRTpLYihcIkRheVwiKSxFRUU6S2IoXCJEYXlcIiwhMCksYTpmdW5jdGlvbihhLGMpe3JldHVybiAxMj5hLmdldEhvdXJzKCk/Yy5BTVBNU1swXTpjLkFNUE1TWzFdfSxaOmZ1bmN0aW9uKGEpe2E9LTEqYS5nZXRUaW1lem9uZU9mZnNldCgpO3JldHVybiBhPSgwPD1hP1wiK1wiOlwiXCIpKyhKYihNYXRoWzA8YT9cImZsb29yXCI6XCJjZWlsXCJdKGEvNjApLDIpK0piKE1hdGguYWJzKGElNjApLDIpKX0sd3c6cWQoMiksdzpxZCgxKSxHOmhjLEdHOmhjLEdHRzpoYyxHR0dHOmZ1bmN0aW9uKGEsYyl7cmV0dXJuIDA+PWEuZ2V0RnVsbFllYXIoKT9jLkVSQU5BTUVTWzBdOmMuRVJBTkFNRVNbMV19fSxMZj0vKCg/OlteeU1kSGhtc2FaRXdHJ10rKXwoPzonKD86W14nXXwnJykqJyl8KD86RSt8eSt8TSt8ZCt8SCt8aCt8bSt8cyt8YXxafEcrfHcrKSkoLiopLyxcbktmPS9eXFwtP1xcZCskLztrZC4kaW5qZWN0PVtcIiRsb2NhbGVcIl07dmFyIEhmPWVhKEspLElmPWVhKHZiKTttZC4kaW5qZWN0PVtcIiRwYXJzZVwiXTt2YXIgVGQ9ZWEoe3Jlc3RyaWN0OlwiRVwiLGNvbXBpbGU6ZnVuY3Rpb24oYSxjKXtpZighYy5ocmVmJiYhYy54bGlua0hyZWYmJiFjLm5hbWUpcmV0dXJuIGZ1bmN0aW9uKGEsYyl7aWYoXCJhXCI9PT1jWzBdLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCkpe3ZhciBmPVwiW29iamVjdCBTVkdBbmltYXRlZFN0cmluZ11cIj09PUNhLmNhbGwoYy5wcm9wKFwiaHJlZlwiKSk/XCJ4bGluazpocmVmXCI6XCJocmVmXCI7Yy5vbihcImNsaWNrXCIsZnVuY3Rpb24oYSl7Yy5hdHRyKGYpfHxhLnByZXZlbnREZWZhdWx0KCl9KX19fX0pLHdiPXt9O3IoRmIsZnVuY3Rpb24oYSxjKXtpZihcIm11bHRpcGxlXCIhPWEpe3ZhciBkPXlhKFwibmctXCIrYyk7d2JbZF09ZnVuY3Rpb24oKXtyZXR1cm57cmVzdHJpY3Q6XCJBXCIscHJpb3JpdHk6MTAwLGxpbms6ZnVuY3Rpb24oYSxmLGcpe2EuJHdhdGNoKGdbZF0sXG5mdW5jdGlvbihhKXtnLiRzZXQoYywhIWEpfSl9fX19fSk7cihPYyxmdW5jdGlvbihhLGMpe3diW2NdPWZ1bmN0aW9uKCl7cmV0dXJue3ByaW9yaXR5OjEwMCxsaW5rOmZ1bmN0aW9uKGEsZSxmKXtpZihcIm5nUGF0dGVyblwiPT09YyYmXCIvXCI9PWYubmdQYXR0ZXJuLmNoYXJBdCgwKSYmKGU9Zi5uZ1BhdHRlcm4ubWF0Y2goT2YpKSl7Zi4kc2V0KFwibmdQYXR0ZXJuXCIsbmV3IFJlZ0V4cChlWzFdLGVbMl0pKTtyZXR1cm59YS4kd2F0Y2goZltjXSxmdW5jdGlvbihhKXtmLiRzZXQoYyxhKX0pfX19fSk7cihbXCJzcmNcIixcInNyY3NldFwiLFwiaHJlZlwiXSxmdW5jdGlvbihhKXt2YXIgYz15YShcIm5nLVwiK2EpO3diW2NdPWZ1bmN0aW9uKCl7cmV0dXJue3ByaW9yaXR5Ojk5LGxpbms6ZnVuY3Rpb24oZCxlLGYpe3ZhciBnPWEsaD1hO1wiaHJlZlwiPT09YSYmXCJbb2JqZWN0IFNWR0FuaW1hdGVkU3RyaW5nXVwiPT09Q2EuY2FsbChlLnByb3AoXCJocmVmXCIpKSYmKGg9XCJ4bGlua0hyZWZcIixmLiRhdHRyW2hdPVwieGxpbms6aHJlZlwiLFxuZz1udWxsKTtmLiRvYnNlcnZlKGMsZnVuY3Rpb24oYyl7Yz8oZi4kc2V0KGgsYyksUmEmJmcmJmUucHJvcChnLGZbaF0pKTpcImhyZWZcIj09PWEmJmYuJHNldChoLG51bGwpfSl9fX19KTt2YXIgTGI9eyRhZGRDb250cm9sOkEsJCRyZW5hbWVDb250cm9sOmZ1bmN0aW9uKGEsYyl7YS4kbmFtZT1jfSwkcmVtb3ZlQ29udHJvbDpBLCRzZXRWYWxpZGl0eTpBLCRzZXREaXJ0eTpBLCRzZXRQcmlzdGluZTpBLCRzZXRTdWJtaXR0ZWQ6QX07cmQuJGluamVjdD1bXCIkZWxlbWVudFwiLFwiJGF0dHJzXCIsXCIkc2NvcGVcIixcIiRhbmltYXRlXCIsXCIkaW50ZXJwb2xhdGVcIl07dmFyIHlkPWZ1bmN0aW9uKGEpe3JldHVybltcIiR0aW1lb3V0XCIsZnVuY3Rpb24oYyl7cmV0dXJue25hbWU6XCJmb3JtXCIscmVzdHJpY3Q6YT9cIkVBQ1wiOlwiRVwiLGNvbnRyb2xsZXI6cmQsY29tcGlsZTpmdW5jdGlvbihkLGUpe2QuYWRkQ2xhc3MoU2EpLmFkZENsYXNzKG9iKTt2YXIgZj1lLm5hbWU/XCJuYW1lXCI6YSYmZS5uZ0Zvcm0/XCJuZ0Zvcm1cIjpcbiExO3JldHVybntwcmU6ZnVuY3Rpb24oYSxkLGUsayl7aWYoIShcImFjdGlvblwiaW4gZSkpe3ZhciBuPWZ1bmN0aW9uKGMpe2EuJGFwcGx5KGZ1bmN0aW9uKCl7ay4kY29tbWl0Vmlld1ZhbHVlKCk7ay4kc2V0U3VibWl0dGVkKCl9KTtjLnByZXZlbnREZWZhdWx0KCl9O2RbMF0uYWRkRXZlbnRMaXN0ZW5lcihcInN1Ym1pdFwiLG4sITEpO2Qub24oXCIkZGVzdHJveVwiLGZ1bmN0aW9uKCl7YyhmdW5jdGlvbigpe2RbMF0ucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInN1Ym1pdFwiLG4sITEpfSwwLCExKX0pfXZhciBwPWsuJCRwYXJlbnRGb3JtO2YmJihqYihhLG51bGwsay4kbmFtZSxrLGsuJG5hbWUpLGUuJG9ic2VydmUoZixmdW5jdGlvbihjKXtrLiRuYW1lIT09YyYmKGpiKGEsbnVsbCxrLiRuYW1lLHUsay4kbmFtZSkscC4kJHJlbmFtZUNvbnRyb2woayxjKSxqYihhLG51bGwsay4kbmFtZSxrLGsuJG5hbWUpKX0pKTtkLm9uKFwiJGRlc3Ryb3lcIixmdW5jdGlvbigpe3AuJHJlbW92ZUNvbnRyb2woayk7XG5mJiZqYihhLG51bGwsZVtmXSx1LGsuJG5hbWUpO3coayxMYil9KX19fX19XX0sVWQ9eWQoKSxnZT15ZCghMCksTmY9L1xcZHs0fS1bMDFdXFxkLVswLTNdXFxkVFswLTJdXFxkOlswLTVdXFxkOlswLTVdXFxkXFwuXFxkKyhbKy1dWzAtMl1cXGQ6WzAtNV1cXGR8WikvLCRmPS9eKGZ0cHxodHRwfGh0dHBzKTpcXC9cXC8oXFx3Kzp7MCwxfVxcdypAKT8oXFxTKykoOlswLTldKyk/KFxcL3xcXC8oW1xcdyMhOi4/Kz0mJUAhXFwtXFwvXSkpPyQvLGFnPS9eW2EtejAtOSEjJCUmJyorXFwvPT9eX2B7fH1+Li1dK0BbYS16MC05XShbYS16MC05LV0qW2EtejAtOV0pPyhcXC5bYS16MC05XShbYS16MC05LV0qW2EtejAtOV0pPykqJC9pLGJnPS9eXFxzKihcXC18XFwrKT8oXFxkK3woXFxkKihcXC5cXGQqKSkpXFxzKiQvLHpkPS9eKFxcZHs0fSktKFxcZHsyfSktKFxcZHsyfSkkLyxBZD0vXihcXGR7NH0pLShcXGRcXGQpLShcXGRcXGQpVChcXGRcXGQpOihcXGRcXGQpKD86OihcXGRcXGQpKFxcLlxcZHsxLDN9KT8pPyQvLGtjPS9eKFxcZHs0fSktVyhcXGRcXGQpJC8sQmQ9L14oXFxkezR9KS0oXFxkXFxkKSQvLFxuQ2Q9L14oXFxkXFxkKTooXFxkXFxkKSg/OjooXFxkXFxkKShcXC5cXGR7MSwzfSk/KT8kLyxEZD17dGV4dDpmdW5jdGlvbihhLGMsZCxlLGYsZyl7bGIoYSxjLGQsZSxmLGcpO2ljKGUpfSxkYXRlOm1iKFwiZGF0ZVwiLHpkLE5iKHpkLFtcInl5eXlcIixcIk1NXCIsXCJkZFwiXSksXCJ5eXl5LU1NLWRkXCIpLFwiZGF0ZXRpbWUtbG9jYWxcIjptYihcImRhdGV0aW1lbG9jYWxcIixBZCxOYihBZCxcInl5eXkgTU0gZGQgSEggbW0gc3Mgc3NzXCIuc3BsaXQoXCIgXCIpKSxcInl5eXktTU0tZGRUSEg6bW06c3Muc3NzXCIpLHRpbWU6bWIoXCJ0aW1lXCIsQ2QsTmIoQ2QsW1wiSEhcIixcIm1tXCIsXCJzc1wiLFwic3NzXCJdKSxcIkhIOm1tOnNzLnNzc1wiKSx3ZWVrOm1iKFwid2Vla1wiLGtjLGZ1bmN0aW9uKGEsYyl7aWYoaGEoYSkpcmV0dXJuIGE7aWYoeChhKSl7a2MubGFzdEluZGV4PTA7dmFyIGQ9a2MuZXhlYyhhKTtpZihkKXt2YXIgZT0rZFsxXSxmPStkWzJdLGc9ZD0wLGg9MCxsPTAsaz1wZChlKSxmPTcqKGYtMSk7YyYmKGQ9Yy5nZXRIb3VycygpLGc9XG5jLmdldE1pbnV0ZXMoKSxoPWMuZ2V0U2Vjb25kcygpLGw9Yy5nZXRNaWxsaXNlY29uZHMoKSk7cmV0dXJuIG5ldyBEYXRlKGUsMCxrLmdldERhdGUoKStmLGQsZyxoLGwpfX1yZXR1cm4gTmFOfSxcInl5eXktV3d3XCIpLG1vbnRoOm1iKFwibW9udGhcIixCZCxOYihCZCxbXCJ5eXl5XCIsXCJNTVwiXSksXCJ5eXl5LU1NXCIpLG51bWJlcjpmdW5jdGlvbihhLGMsZCxlLGYsZyl7dGQoYSxjLGQsZSk7bGIoYSxjLGQsZSxmLGcpO2UuJCRwYXJzZXJOYW1lPVwibnVtYmVyXCI7ZS4kcGFyc2Vycy5wdXNoKGZ1bmN0aW9uKGEpe3JldHVybiBlLiRpc0VtcHR5KGEpP251bGw6YmcudGVzdChhKT9wYXJzZUZsb2F0KGEpOnV9KTtlLiRmb3JtYXR0ZXJzLnB1c2goZnVuY3Rpb24oYSl7aWYoIWUuJGlzRW1wdHkoYSkpe2lmKCFZKGEpKXRocm93IG5iKFwibnVtZm10XCIsYSk7YT1hLnRvU3RyaW5nKCl9cmV0dXJuIGF9KTtpZih5KGQubWluKXx8ZC5uZ01pbil7dmFyIGg7ZS4kdmFsaWRhdG9ycy5taW49ZnVuY3Rpb24oYSl7cmV0dXJuIGUuJGlzRW1wdHkoYSl8fFxuRChoKXx8YT49aH07ZC4kb2JzZXJ2ZShcIm1pblwiLGZ1bmN0aW9uKGEpe3koYSkmJiFZKGEpJiYoYT1wYXJzZUZsb2F0KGEsMTApKTtoPVkoYSkmJiFpc05hTihhKT9hOnU7ZS4kdmFsaWRhdGUoKX0pfWlmKHkoZC5tYXgpfHxkLm5nTWF4KXt2YXIgbDtlLiR2YWxpZGF0b3JzLm1heD1mdW5jdGlvbihhKXtyZXR1cm4gZS4kaXNFbXB0eShhKXx8RChsKXx8YTw9bH07ZC4kb2JzZXJ2ZShcIm1heFwiLGZ1bmN0aW9uKGEpe3koYSkmJiFZKGEpJiYoYT1wYXJzZUZsb2F0KGEsMTApKTtsPVkoYSkmJiFpc05hTihhKT9hOnU7ZS4kdmFsaWRhdGUoKX0pfX0sdXJsOmZ1bmN0aW9uKGEsYyxkLGUsZixnKXtsYihhLGMsZCxlLGYsZyk7aWMoZSk7ZS4kJHBhcnNlck5hbWU9XCJ1cmxcIjtlLiR2YWxpZGF0b3JzLnVybD1mdW5jdGlvbihhLGMpe3ZhciBkPWF8fGM7cmV0dXJuIGUuJGlzRW1wdHkoZCl8fCRmLnRlc3QoZCl9fSxlbWFpbDpmdW5jdGlvbihhLGMsZCxlLGYsZyl7bGIoYSxjLGQsZSxmLGcpO2ljKGUpO1xuZS4kJHBhcnNlck5hbWU9XCJlbWFpbFwiO2UuJHZhbGlkYXRvcnMuZW1haWw9ZnVuY3Rpb24oYSxjKXt2YXIgZD1hfHxjO3JldHVybiBlLiRpc0VtcHR5KGQpfHxhZy50ZXN0KGQpfX0scmFkaW86ZnVuY3Rpb24oYSxjLGQsZSl7RChkLm5hbWUpJiZjLmF0dHIoXCJuYW1lXCIsKytyYik7Yy5vbihcImNsaWNrXCIsZnVuY3Rpb24oYSl7Y1swXS5jaGVja2VkJiZlLiRzZXRWaWV3VmFsdWUoZC52YWx1ZSxhJiZhLnR5cGUpfSk7ZS4kcmVuZGVyPWZ1bmN0aW9uKCl7Y1swXS5jaGVja2VkPWQudmFsdWU9PWUuJHZpZXdWYWx1ZX07ZC4kb2JzZXJ2ZShcInZhbHVlXCIsZS4kcmVuZGVyKX0sY2hlY2tib3g6ZnVuY3Rpb24oYSxjLGQsZSxmLGcsaCxsKXt2YXIgaz11ZChsLGEsXCJuZ1RydWVWYWx1ZVwiLGQubmdUcnVlVmFsdWUsITApLG49dWQobCxhLFwibmdGYWxzZVZhbHVlXCIsZC5uZ0ZhbHNlVmFsdWUsITEpO2Mub24oXCJjbGlja1wiLGZ1bmN0aW9uKGEpe2UuJHNldFZpZXdWYWx1ZShjWzBdLmNoZWNrZWQsYSYmXG5hLnR5cGUpfSk7ZS4kcmVuZGVyPWZ1bmN0aW9uKCl7Y1swXS5jaGVja2VkPWUuJHZpZXdWYWx1ZX07ZS4kaXNFbXB0eT1mdW5jdGlvbihhKXtyZXR1cm4hMT09PWF9O2UuJGZvcm1hdHRlcnMucHVzaChmdW5jdGlvbihhKXtyZXR1cm4gaWEoYSxrKX0pO2UuJHBhcnNlcnMucHVzaChmdW5jdGlvbihhKXtyZXR1cm4gYT9rOm59KX0saGlkZGVuOkEsYnV0dG9uOkEsc3VibWl0OkEscmVzZXQ6QSxmaWxlOkF9LHljPVtcIiRicm93c2VyXCIsXCIkc25pZmZlclwiLFwiJGZpbHRlclwiLFwiJHBhcnNlXCIsZnVuY3Rpb24oYSxjLGQsZSl7cmV0dXJue3Jlc3RyaWN0OlwiRVwiLHJlcXVpcmU6W1wiP25nTW9kZWxcIl0sbGluazp7cHJlOmZ1bmN0aW9uKGYsZyxoLGwpe2xbMF0mJihEZFtLKGgudHlwZSldfHxEZC50ZXh0KShmLGcsaCxsWzBdLGMsYSxkLGUpfX19fV0sY2c9L14odHJ1ZXxmYWxzZXxcXGQrKSQvLHllPWZ1bmN0aW9uKCl7cmV0dXJue3Jlc3RyaWN0OlwiQVwiLHByaW9yaXR5OjEwMCxjb21waWxlOmZ1bmN0aW9uKGEsXG5jKXtyZXR1cm4gY2cudGVzdChjLm5nVmFsdWUpP2Z1bmN0aW9uKGEsYyxmKXtmLiRzZXQoXCJ2YWx1ZVwiLGEuJGV2YWwoZi5uZ1ZhbHVlKSl9OmZ1bmN0aW9uKGEsYyxmKXthLiR3YXRjaChmLm5nVmFsdWUsZnVuY3Rpb24oYSl7Zi4kc2V0KFwidmFsdWVcIixhKX0pfX19fSxaZD1bXCIkY29tcGlsZVwiLGZ1bmN0aW9uKGEpe3JldHVybntyZXN0cmljdDpcIkFDXCIsY29tcGlsZTpmdW5jdGlvbihjKXthLiQkYWRkQmluZGluZ0NsYXNzKGMpO3JldHVybiBmdW5jdGlvbihjLGUsZil7YS4kJGFkZEJpbmRpbmdJbmZvKGUsZi5uZ0JpbmQpO2U9ZVswXTtjLiR3YXRjaChmLm5nQmluZCxmdW5jdGlvbihhKXtlLnRleHRDb250ZW50PWE9PT11P1wiXCI6YX0pfX19fV0sYWU9W1wiJGludGVycG9sYXRlXCIsXCIkY29tcGlsZVwiLGZ1bmN0aW9uKGEsYyl7cmV0dXJue2NvbXBpbGU6ZnVuY3Rpb24oZCl7Yy4kJGFkZEJpbmRpbmdDbGFzcyhkKTtyZXR1cm4gZnVuY3Rpb24oZCxmLGcpe2Q9YShmLmF0dHIoZy4kYXR0ci5uZ0JpbmRUZW1wbGF0ZSkpO1xuYy4kJGFkZEJpbmRpbmdJbmZvKGYsZC5leHByZXNzaW9ucyk7Zj1mWzBdO2cuJG9ic2VydmUoXCJuZ0JpbmRUZW1wbGF0ZVwiLGZ1bmN0aW9uKGEpe2YudGV4dENvbnRlbnQ9YT09PXU/XCJcIjphfSl9fX19XSwkZD1bXCIkc2NlXCIsXCIkcGFyc2VcIixcIiRjb21waWxlXCIsZnVuY3Rpb24oYSxjLGQpe3JldHVybntyZXN0cmljdDpcIkFcIixjb21waWxlOmZ1bmN0aW9uKGUsZil7dmFyIGc9YyhmLm5nQmluZEh0bWwpLGg9YyhmLm5nQmluZEh0bWwsZnVuY3Rpb24oYSl7cmV0dXJuKGF8fFwiXCIpLnRvU3RyaW5nKCl9KTtkLiQkYWRkQmluZGluZ0NsYXNzKGUpO3JldHVybiBmdW5jdGlvbihjLGUsZil7ZC4kJGFkZEJpbmRpbmdJbmZvKGUsZi5uZ0JpbmRIdG1sKTtjLiR3YXRjaChoLGZ1bmN0aW9uKCl7ZS5odG1sKGEuZ2V0VHJ1c3RlZEh0bWwoZyhjKSl8fFwiXCIpfSl9fX19XSx4ZT1lYSh7cmVzdHJpY3Q6XCJBXCIscmVxdWlyZTpcIm5nTW9kZWxcIixsaW5rOmZ1bmN0aW9uKGEsYyxkLGUpe2UuJHZpZXdDaGFuZ2VMaXN0ZW5lcnMucHVzaChmdW5jdGlvbigpe2EuJGV2YWwoZC5uZ0NoYW5nZSl9KX19KSxcbmJlPWpjKFwiXCIsITApLGRlPWpjKFwiT2RkXCIsMCksY2U9amMoXCJFdmVuXCIsMSksZWU9SWEoe2NvbXBpbGU6ZnVuY3Rpb24oYSxjKXtjLiRzZXQoXCJuZ0Nsb2FrXCIsdSk7YS5yZW1vdmVDbGFzcyhcIm5nLWNsb2FrXCIpfX0pLGZlPVtmdW5jdGlvbigpe3JldHVybntyZXN0cmljdDpcIkFcIixzY29wZTohMCxjb250cm9sbGVyOlwiQFwiLHByaW9yaXR5OjUwMH19XSxEYz17fSxkZz17Ymx1cjohMCxmb2N1czohMH07cihcImNsaWNrIGRibGNsaWNrIG1vdXNlZG93biBtb3VzZXVwIG1vdXNlb3ZlciBtb3VzZW91dCBtb3VzZW1vdmUgbW91c2VlbnRlciBtb3VzZWxlYXZlIGtleWRvd24ga2V5dXAga2V5cHJlc3Mgc3VibWl0IGZvY3VzIGJsdXIgY29weSBjdXQgcGFzdGVcIi5zcGxpdChcIiBcIiksZnVuY3Rpb24oYSl7dmFyIGM9eWEoXCJuZy1cIithKTtEY1tjXT1bXCIkcGFyc2VcIixcIiRyb290U2NvcGVcIixmdW5jdGlvbihkLGUpe3JldHVybntyZXN0cmljdDpcIkFcIixjb21waWxlOmZ1bmN0aW9uKGYsZyl7dmFyIGg9XG5kKGdbY10sbnVsbCwhMCk7cmV0dXJuIGZ1bmN0aW9uKGMsZCl7ZC5vbihhLGZ1bmN0aW9uKGQpe3ZhciBmPWZ1bmN0aW9uKCl7aChjLHskZXZlbnQ6ZH0pfTtkZ1thXSYmZS4kJHBoYXNlP2MuJGV2YWxBc3luYyhmKTpjLiRhcHBseShmKX0pfX19fV19KTt2YXIgaWU9W1wiJGFuaW1hdGVcIixmdW5jdGlvbihhKXtyZXR1cm57bXVsdGlFbGVtZW50OiEwLHRyYW5zY2x1ZGU6XCJlbGVtZW50XCIscHJpb3JpdHk6NjAwLHRlcm1pbmFsOiEwLHJlc3RyaWN0OlwiQVwiLCQkdGxiOiEwLGxpbms6ZnVuY3Rpb24oYyxkLGUsZixnKXt2YXIgaCxsLGs7Yy4kd2F0Y2goZS5uZ0lmLGZ1bmN0aW9uKGMpe2M/bHx8ZyhmdW5jdGlvbihjLGYpe2w9ZjtjW2MubGVuZ3RoKytdPVcuY3JlYXRlQ29tbWVudChcIiBlbmQgbmdJZjogXCIrZS5uZ0lmK1wiIFwiKTtoPXtjbG9uZTpjfTthLmVudGVyKGMsZC5wYXJlbnQoKSxkKX0pOihrJiYoay5yZW1vdmUoKSxrPW51bGwpLGwmJihsLiRkZXN0cm95KCksbD1udWxsKSxoJiYoaz1cbnViKGguY2xvbmUpLGEubGVhdmUoaykudGhlbihmdW5jdGlvbigpe2s9bnVsbH0pLGg9bnVsbCkpfSl9fX1dLGplPVtcIiR0ZW1wbGF0ZVJlcXVlc3RcIixcIiRhbmNob3JTY3JvbGxcIixcIiRhbmltYXRlXCIsZnVuY3Rpb24oYSxjLGQpe3JldHVybntyZXN0cmljdDpcIkVDQVwiLHByaW9yaXR5OjQwMCx0ZXJtaW5hbDohMCx0cmFuc2NsdWRlOlwiZWxlbWVudFwiLGNvbnRyb2xsZXI6Y2Eubm9vcCxjb21waWxlOmZ1bmN0aW9uKGUsZil7dmFyIGc9Zi5uZ0luY2x1ZGV8fGYuc3JjLGg9Zi5vbmxvYWR8fFwiXCIsbD1mLmF1dG9zY3JvbGw7cmV0dXJuIGZ1bmN0aW9uKGUsZixwLHEscil7dmFyIHM9MCx1LHYsbSxDPWZ1bmN0aW9uKCl7diYmKHYucmVtb3ZlKCksdj1udWxsKTt1JiYodS4kZGVzdHJveSgpLHU9bnVsbCk7bSYmKGQubGVhdmUobSkudGhlbihmdW5jdGlvbigpe3Y9bnVsbH0pLHY9bSxtPW51bGwpfTtlLiR3YXRjaChnLGZ1bmN0aW9uKGcpe3ZhciBwPWZ1bmN0aW9uKCl7IXkobCl8fGwmJiFlLiRldmFsKGwpfHxcbmMoKX0sTT0rK3M7Zz8oYShnLCEwKS50aGVuKGZ1bmN0aW9uKGEpe2lmKE09PT1zKXt2YXIgYz1lLiRuZXcoKTtxLnRlbXBsYXRlPWE7YT1yKGMsZnVuY3Rpb24oYSl7QygpO2QuZW50ZXIoYSxudWxsLGYpLnRoZW4ocCl9KTt1PWM7bT1hO3UuJGVtaXQoXCIkaW5jbHVkZUNvbnRlbnRMb2FkZWRcIixnKTtlLiRldmFsKGgpfX0sZnVuY3Rpb24oKXtNPT09cyYmKEMoKSxlLiRlbWl0KFwiJGluY2x1ZGVDb250ZW50RXJyb3JcIixnKSl9KSxlLiRlbWl0KFwiJGluY2x1ZGVDb250ZW50UmVxdWVzdGVkXCIsZykpOihDKCkscS50ZW1wbGF0ZT1udWxsKX0pfX19fV0sQWU9W1wiJGNvbXBpbGVcIixmdW5jdGlvbihhKXtyZXR1cm57cmVzdHJpY3Q6XCJFQ0FcIixwcmlvcml0eTotNDAwLHJlcXVpcmU6XCJuZ0luY2x1ZGVcIixsaW5rOmZ1bmN0aW9uKGMsZCxlLGYpey9TVkcvLnRlc3QoZFswXS50b1N0cmluZygpKT8oZC5lbXB0eSgpLGEoR2MoZi50ZW1wbGF0ZSxXKS5jaGlsZE5vZGVzKShjLGZ1bmN0aW9uKGEpe2QuYXBwZW5kKGEpfSxcbntmdXR1cmVQYXJlbnRFbGVtZW50OmR9KSk6KGQuaHRtbChmLnRlbXBsYXRlKSxhKGQuY29udGVudHMoKSkoYykpfX19XSxrZT1JYSh7cHJpb3JpdHk6NDUwLGNvbXBpbGU6ZnVuY3Rpb24oKXtyZXR1cm57cHJlOmZ1bmN0aW9uKGEsYyxkKXthLiRldmFsKGQubmdJbml0KX19fX0pLHdlPWZ1bmN0aW9uKCl7cmV0dXJue3Jlc3RyaWN0OlwiQVwiLHByaW9yaXR5OjEwMCxyZXF1aXJlOlwibmdNb2RlbFwiLGxpbms6ZnVuY3Rpb24oYSxjLGQsZSl7dmFyIGY9Yy5hdHRyKGQuJGF0dHIubmdMaXN0KXx8XCIsIFwiLGc9XCJmYWxzZVwiIT09ZC5uZ1RyaW0saD1nP04oZik6ZjtlLiRwYXJzZXJzLnB1c2goZnVuY3Rpb24oYSl7aWYoIUQoYSkpe3ZhciBjPVtdO2EmJnIoYS5zcGxpdChoKSxmdW5jdGlvbihhKXthJiZjLnB1c2goZz9OKGEpOmEpfSk7cmV0dXJuIGN9fSk7ZS4kZm9ybWF0dGVycy5wdXNoKGZ1bmN0aW9uKGEpe3JldHVybiBIKGEpP2Euam9pbihmKTp1fSk7ZS4kaXNFbXB0eT1mdW5jdGlvbihhKXtyZXR1cm4hYXx8XG4hYS5sZW5ndGh9fX19LG9iPVwibmctdmFsaWRcIix2ZD1cIm5nLWludmFsaWRcIixTYT1cIm5nLXByaXN0aW5lXCIsTWI9XCJuZy1kaXJ0eVwiLHhkPVwibmctcGVuZGluZ1wiLG5iPVMoXCJuZ01vZGVsXCIpLGVnPVtcIiRzY29wZVwiLFwiJGV4Y2VwdGlvbkhhbmRsZXJcIixcIiRhdHRyc1wiLFwiJGVsZW1lbnRcIixcIiRwYXJzZVwiLFwiJGFuaW1hdGVcIixcIiR0aW1lb3V0XCIsXCIkcm9vdFNjb3BlXCIsXCIkcVwiLFwiJGludGVycG9sYXRlXCIsZnVuY3Rpb24oYSxjLGQsZSxmLGcsaCxsLGssbil7dGhpcy4kbW9kZWxWYWx1ZT10aGlzLiR2aWV3VmFsdWU9TnVtYmVyLk5hTjt0aGlzLiQkcmF3TW9kZWxWYWx1ZT11O3RoaXMuJHZhbGlkYXRvcnM9e307dGhpcy4kYXN5bmNWYWxpZGF0b3JzPXt9O3RoaXMuJHBhcnNlcnM9W107dGhpcy4kZm9ybWF0dGVycz1bXTt0aGlzLiR2aWV3Q2hhbmdlTGlzdGVuZXJzPVtdO3RoaXMuJHVudG91Y2hlZD0hMDt0aGlzLiR0b3VjaGVkPSExO3RoaXMuJHByaXN0aW5lPSEwO3RoaXMuJGRpcnR5PSExO1xudGhpcy4kdmFsaWQ9ITA7dGhpcy4kaW52YWxpZD0hMTt0aGlzLiRlcnJvcj17fTt0aGlzLiQkc3VjY2Vzcz17fTt0aGlzLiRwZW5kaW5nPXU7dGhpcy4kbmFtZT1uKGQubmFtZXx8XCJcIiwhMSkoYSk7dmFyIHA9ZihkLm5nTW9kZWwpLHE9cC5hc3NpZ24sdD1wLHM9cSxGPW51bGwsdixtPXRoaXM7dGhpcy4kJHNldE9wdGlvbnM9ZnVuY3Rpb24oYSl7aWYoKG0uJG9wdGlvbnM9YSkmJmEuZ2V0dGVyU2V0dGVyKXt2YXIgYz1mKGQubmdNb2RlbCtcIigpXCIpLGc9ZihkLm5nTW9kZWwrXCIoJCQkcClcIik7dD1mdW5jdGlvbihhKXt2YXIgZD1wKGEpO3ooZCkmJihkPWMoYSkpO3JldHVybiBkfTtzPWZ1bmN0aW9uKGEsYyl7eihwKGEpKT9nKGEseyQkJHA6bS4kbW9kZWxWYWx1ZX0pOnEoYSxtLiRtb2RlbFZhbHVlKX19ZWxzZSBpZighcC5hc3NpZ24pdGhyb3cgbmIoXCJub25hc3NpZ25cIixkLm5nTW9kZWwseGEoZSkpO307dGhpcy4kcmVuZGVyPUE7dGhpcy4kaXNFbXB0eT1mdW5jdGlvbihhKXtyZXR1cm4gRChhKXx8XG5cIlwiPT09YXx8bnVsbD09PWF8fGEhPT1hfTt2YXIgQz1lLmluaGVyaXRlZERhdGEoXCIkZm9ybUNvbnRyb2xsZXJcIil8fExiLHc9MDtzZCh7Y3RybDp0aGlzLCRlbGVtZW50OmUsc2V0OmZ1bmN0aW9uKGEsYyl7YVtjXT0hMH0sdW5zZXQ6ZnVuY3Rpb24oYSxjKXtkZWxldGUgYVtjXX0scGFyZW50Rm9ybTpDLCRhbmltYXRlOmd9KTt0aGlzLiRzZXRQcmlzdGluZT1mdW5jdGlvbigpe20uJGRpcnR5PSExO20uJHByaXN0aW5lPSEwO2cucmVtb3ZlQ2xhc3MoZSxNYik7Zy5hZGRDbGFzcyhlLFNhKX07dGhpcy4kc2V0RGlydHk9ZnVuY3Rpb24oKXttLiRkaXJ0eT0hMDttLiRwcmlzdGluZT0hMTtnLnJlbW92ZUNsYXNzKGUsU2EpO2cuYWRkQ2xhc3MoZSxNYik7Qy4kc2V0RGlydHkoKX07dGhpcy4kc2V0VW50b3VjaGVkPWZ1bmN0aW9uKCl7bS4kdG91Y2hlZD0hMTttLiR1bnRvdWNoZWQ9ITA7Zy5zZXRDbGFzcyhlLFwibmctdW50b3VjaGVkXCIsXCJuZy10b3VjaGVkXCIpfTt0aGlzLiRzZXRUb3VjaGVkPVxuZnVuY3Rpb24oKXttLiR0b3VjaGVkPSEwO20uJHVudG91Y2hlZD0hMTtnLnNldENsYXNzKGUsXCJuZy10b3VjaGVkXCIsXCJuZy11bnRvdWNoZWRcIil9O3RoaXMuJHJvbGxiYWNrVmlld1ZhbHVlPWZ1bmN0aW9uKCl7aC5jYW5jZWwoRik7bS4kdmlld1ZhbHVlPW0uJCRsYXN0Q29tbWl0dGVkVmlld1ZhbHVlO20uJHJlbmRlcigpfTt0aGlzLiR2YWxpZGF0ZT1mdW5jdGlvbigpe2lmKCFZKG0uJG1vZGVsVmFsdWUpfHwhaXNOYU4obS4kbW9kZWxWYWx1ZSkpe3ZhciBhPW0uJCRyYXdNb2RlbFZhbHVlLGM9bS4kdmFsaWQsZD1tLiRtb2RlbFZhbHVlLGU9bS4kb3B0aW9ucyYmbS4kb3B0aW9ucy5hbGxvd0ludmFsaWQ7bS4kJHJ1blZhbGlkYXRvcnMoYSxtLiQkbGFzdENvbW1pdHRlZFZpZXdWYWx1ZSxmdW5jdGlvbihmKXtlfHxjPT09Znx8KG0uJG1vZGVsVmFsdWU9Zj9hOnUsbS4kbW9kZWxWYWx1ZSE9PWQmJm0uJCR3cml0ZU1vZGVsVG9TY29wZSgpKX0pfX07dGhpcy4kJHJ1blZhbGlkYXRvcnM9XG5mdW5jdGlvbihhLGMsZCl7ZnVuY3Rpb24gZSgpe3ZhciBkPSEwO3IobS4kdmFsaWRhdG9ycyxmdW5jdGlvbihlLGYpe3ZhciBoPWUoYSxjKTtkPWQmJmg7ZyhmLGgpfSk7cmV0dXJuIGQ/ITA6KHIobS4kYXN5bmNWYWxpZGF0b3JzLGZ1bmN0aW9uKGEsYyl7ZyhjLG51bGwpfSksITEpfWZ1bmN0aW9uIGYoKXt2YXIgZD1bXSxlPSEwO3IobS4kYXN5bmNWYWxpZGF0b3JzLGZ1bmN0aW9uKGYsaCl7dmFyIGw9ZihhLGMpO2lmKCFsfHwheihsLnRoZW4pKXRocm93IG5iKFwiJGFzeW5jVmFsaWRhdG9yc1wiLGwpO2coaCx1KTtkLnB1c2gobC50aGVuKGZ1bmN0aW9uKCl7ZyhoLCEwKX0sZnVuY3Rpb24oYSl7ZT0hMTtnKGgsITEpfSkpfSk7ZC5sZW5ndGg/ay5hbGwoZCkudGhlbihmdW5jdGlvbigpe2goZSl9LEEpOmgoITApfWZ1bmN0aW9uIGcoYSxjKXtsPT09dyYmbS4kc2V0VmFsaWRpdHkoYSxjKX1mdW5jdGlvbiBoKGEpe2w9PT13JiZkKGEpfXcrKzt2YXIgbD13OyhmdW5jdGlvbigpe3ZhciBhPVxubS4kJHBhcnNlck5hbWV8fFwicGFyc2VcIjtpZih2PT09dSlnKGEsbnVsbCk7ZWxzZSByZXR1cm4gdnx8KHIobS4kdmFsaWRhdG9ycyxmdW5jdGlvbihhLGMpe2coYyxudWxsKX0pLHIobS4kYXN5bmNWYWxpZGF0b3JzLGZ1bmN0aW9uKGEsYyl7ZyhjLG51bGwpfSkpLGcoYSx2KSx2O3JldHVybiEwfSkoKT9lKCk/ZigpOmgoITEpOmgoITEpfTt0aGlzLiRjb21taXRWaWV3VmFsdWU9ZnVuY3Rpb24oKXt2YXIgYT1tLiR2aWV3VmFsdWU7aC5jYW5jZWwoRik7aWYobS4kJGxhc3RDb21taXR0ZWRWaWV3VmFsdWUhPT1hfHxcIlwiPT09YSYmbS4kJGhhc05hdGl2ZVZhbGlkYXRvcnMpbS4kJGxhc3RDb21taXR0ZWRWaWV3VmFsdWU9YSxtLiRwcmlzdGluZSYmdGhpcy4kc2V0RGlydHkoKSx0aGlzLiQkcGFyc2VBbmRWYWxpZGF0ZSgpfTt0aGlzLiQkcGFyc2VBbmRWYWxpZGF0ZT1mdW5jdGlvbigpe3ZhciBjPW0uJCRsYXN0Q29tbWl0dGVkVmlld1ZhbHVlO2lmKHY9RChjKT91OiEwKWZvcih2YXIgZD1cbjA7ZDxtLiRwYXJzZXJzLmxlbmd0aDtkKyspaWYoYz1tLiRwYXJzZXJzW2RdKGMpLEQoYykpe3Y9ITE7YnJlYWt9WShtLiRtb2RlbFZhbHVlKSYmaXNOYU4obS4kbW9kZWxWYWx1ZSkmJihtLiRtb2RlbFZhbHVlPXQoYSkpO3ZhciBlPW0uJG1vZGVsVmFsdWUsZj1tLiRvcHRpb25zJiZtLiRvcHRpb25zLmFsbG93SW52YWxpZDttLiQkcmF3TW9kZWxWYWx1ZT1jO2YmJihtLiRtb2RlbFZhbHVlPWMsbS4kbW9kZWxWYWx1ZSE9PWUmJm0uJCR3cml0ZU1vZGVsVG9TY29wZSgpKTttLiQkcnVuVmFsaWRhdG9ycyhjLG0uJCRsYXN0Q29tbWl0dGVkVmlld1ZhbHVlLGZ1bmN0aW9uKGEpe2Z8fChtLiRtb2RlbFZhbHVlPWE/Yzp1LG0uJG1vZGVsVmFsdWUhPT1lJiZtLiQkd3JpdGVNb2RlbFRvU2NvcGUoKSl9KX07dGhpcy4kJHdyaXRlTW9kZWxUb1Njb3BlPWZ1bmN0aW9uKCl7cyhhLG0uJG1vZGVsVmFsdWUpO3IobS4kdmlld0NoYW5nZUxpc3RlbmVycyxmdW5jdGlvbihhKXt0cnl7YSgpfWNhdGNoKGQpe2MoZCl9fSl9O1xudGhpcy4kc2V0Vmlld1ZhbHVlPWZ1bmN0aW9uKGEsYyl7bS4kdmlld1ZhbHVlPWE7bS4kb3B0aW9ucyYmIW0uJG9wdGlvbnMudXBkYXRlT25EZWZhdWx0fHxtLiQkZGVib3VuY2VWaWV3VmFsdWVDb21taXQoYyl9O3RoaXMuJCRkZWJvdW5jZVZpZXdWYWx1ZUNvbW1pdD1mdW5jdGlvbihjKXt2YXIgZD0wLGU9bS4kb3B0aW9ucztlJiZ5KGUuZGVib3VuY2UpJiYoZT1lLmRlYm91bmNlLFkoZSk/ZD1lOlkoZVtjXSk/ZD1lW2NdOlkoZVtcImRlZmF1bHRcIl0pJiYoZD1lW1wiZGVmYXVsdFwiXSkpO2guY2FuY2VsKEYpO2Q/Rj1oKGZ1bmN0aW9uKCl7bS4kY29tbWl0Vmlld1ZhbHVlKCl9LGQpOmwuJCRwaGFzZT9tLiRjb21taXRWaWV3VmFsdWUoKTphLiRhcHBseShmdW5jdGlvbigpe20uJGNvbW1pdFZpZXdWYWx1ZSgpfSl9O2EuJHdhdGNoKGZ1bmN0aW9uKCl7dmFyIGM9dChhKTtpZihjIT09bS4kbW9kZWxWYWx1ZSYmKG0uJG1vZGVsVmFsdWU9PT1tLiRtb2RlbFZhbHVlfHxjPT09Yykpe20uJG1vZGVsVmFsdWU9XG5tLiQkcmF3TW9kZWxWYWx1ZT1jO3Y9dTtmb3IodmFyIGQ9bS4kZm9ybWF0dGVycyxlPWQubGVuZ3RoLGY9YztlLS07KWY9ZFtlXShmKTttLiR2aWV3VmFsdWUhPT1mJiYobS4kdmlld1ZhbHVlPW0uJCRsYXN0Q29tbWl0dGVkVmlld1ZhbHVlPWYsbS4kcmVuZGVyKCksbS4kJHJ1blZhbGlkYXRvcnMoYyxmLEEpKX1yZXR1cm4gY30pfV0sdmU9W1wiJHJvb3RTY29wZVwiLGZ1bmN0aW9uKGEpe3JldHVybntyZXN0cmljdDpcIkFcIixyZXF1aXJlOltcIm5nTW9kZWxcIixcIl4/Zm9ybVwiLFwiXj9uZ01vZGVsT3B0aW9uc1wiXSxjb250cm9sbGVyOmVnLHByaW9yaXR5OjEsY29tcGlsZTpmdW5jdGlvbihjKXtjLmFkZENsYXNzKFNhKS5hZGRDbGFzcyhcIm5nLXVudG91Y2hlZFwiKS5hZGRDbGFzcyhvYik7cmV0dXJue3ByZTpmdW5jdGlvbihhLGMsZixnKXt2YXIgaD1nWzBdLGw9Z1sxXXx8TGI7aC4kJHNldE9wdGlvbnMoZ1syXSYmZ1syXS4kb3B0aW9ucyk7bC4kYWRkQ29udHJvbChoKTtmLiRvYnNlcnZlKFwibmFtZVwiLFxuZnVuY3Rpb24oYSl7aC4kbmFtZSE9PWEmJmwuJCRyZW5hbWVDb250cm9sKGgsYSl9KTthLiRvbihcIiRkZXN0cm95XCIsZnVuY3Rpb24oKXtsLiRyZW1vdmVDb250cm9sKGgpfSl9LHBvc3Q6ZnVuY3Rpb24oYyxlLGYsZyl7dmFyIGg9Z1swXTtpZihoLiRvcHRpb25zJiZoLiRvcHRpb25zLnVwZGF0ZU9uKWUub24oaC4kb3B0aW9ucy51cGRhdGVPbixmdW5jdGlvbihhKXtoLiQkZGVib3VuY2VWaWV3VmFsdWVDb21taXQoYSYmYS50eXBlKX0pO2Uub24oXCJibHVyXCIsZnVuY3Rpb24oZSl7aC4kdG91Y2hlZHx8KGEuJCRwaGFzZT9jLiRldmFsQXN5bmMoaC4kc2V0VG91Y2hlZCk6Yy4kYXBwbHkoaC4kc2V0VG91Y2hlZCkpfSl9fX19fV0sZmc9LyhcXHMrfF4pZGVmYXVsdChcXHMrfCQpLyx6ZT1mdW5jdGlvbigpe3JldHVybntyZXN0cmljdDpcIkFcIixjb250cm9sbGVyOltcIiRzY29wZVwiLFwiJGF0dHJzXCIsZnVuY3Rpb24oYSxjKXt2YXIgZD10aGlzO3RoaXMuJG9wdGlvbnM9YS4kZXZhbChjLm5nTW9kZWxPcHRpb25zKTtcbnRoaXMuJG9wdGlvbnMudXBkYXRlT24hPT11Pyh0aGlzLiRvcHRpb25zLnVwZGF0ZU9uRGVmYXVsdD0hMSx0aGlzLiRvcHRpb25zLnVwZGF0ZU9uPU4odGhpcy4kb3B0aW9ucy51cGRhdGVPbi5yZXBsYWNlKGZnLGZ1bmN0aW9uKCl7ZC4kb3B0aW9ucy51cGRhdGVPbkRlZmF1bHQ9ITA7cmV0dXJuXCIgXCJ9KSkpOnRoaXMuJG9wdGlvbnMudXBkYXRlT25EZWZhdWx0PSEwfV19fSxsZT1JYSh7dGVybWluYWw6ITAscHJpb3JpdHk6MUUzfSksbWU9W1wiJGxvY2FsZVwiLFwiJGludGVycG9sYXRlXCIsZnVuY3Rpb24oYSxjKXt2YXIgZD0ve30vZyxlPS9ed2hlbihNaW51cyk/KC4rKSQvO3JldHVybntyZXN0cmljdDpcIkVBXCIsbGluazpmdW5jdGlvbihmLGcsaCl7ZnVuY3Rpb24gbChhKXtnLnRleHQoYXx8XCJcIil9dmFyIGs9aC5jb3VudCxuPWguJGF0dHIud2hlbiYmZy5hdHRyKGguJGF0dHIud2hlbikscD1oLm9mZnNldHx8MCxxPWYuJGV2YWwobil8fHt9LHQ9e30sbj1jLnN0YXJ0U3ltYm9sKCkscz1cbmMuZW5kU3ltYm9sKCksdT1uK2srXCItXCIrcCtzLHY9Y2Eubm9vcCxtO3IoaCxmdW5jdGlvbihhLGMpe3ZhciBkPWUuZXhlYyhjKTtkJiYoZD0oZFsxXT9cIi1cIjpcIlwiKStLKGRbMl0pLHFbZF09Zy5hdHRyKGguJGF0dHJbY10pKX0pO3IocSxmdW5jdGlvbihhLGUpe3RbZV09YyhhLnJlcGxhY2UoZCx1KSl9KTtmLiR3YXRjaChrLGZ1bmN0aW9uKGMpe2M9cGFyc2VGbG9hdChjKTt2YXIgZD1pc05hTihjKTtkfHxjIGluIHF8fChjPWEucGx1cmFsQ2F0KGMtcCkpO2M9PT1tfHxkJiZpc05hTihtKXx8KHYoKSx2PWYuJHdhdGNoKHRbY10sbCksbT1jKX0pfX19XSxuZT1bXCIkcGFyc2VcIixcIiRhbmltYXRlXCIsZnVuY3Rpb24oYSxjKXt2YXIgZD1TKFwibmdSZXBlYXRcIiksZT1mdW5jdGlvbihhLGMsZCxlLGssbixwKXthW2RdPWU7ayYmKGFba109bik7YS4kaW5kZXg9YzthLiRmaXJzdD0wPT09YzthLiRsYXN0PWM9PT1wLTE7YS4kbWlkZGxlPSEoYS4kZmlyc3R8fGEuJGxhc3QpO2EuJG9kZD0hKGEuJGV2ZW49XG4wPT09KGMmMSkpfTtyZXR1cm57cmVzdHJpY3Q6XCJBXCIsbXVsdGlFbGVtZW50OiEwLHRyYW5zY2x1ZGU6XCJlbGVtZW50XCIscHJpb3JpdHk6MUUzLHRlcm1pbmFsOiEwLCQkdGxiOiEwLGNvbXBpbGU6ZnVuY3Rpb24oZixnKXt2YXIgaD1nLm5nUmVwZWF0LGw9Vy5jcmVhdGVDb21tZW50KFwiIGVuZCBuZ1JlcGVhdDogXCIraCtcIiBcIiksaz1oLm1hdGNoKC9eXFxzKihbXFxzXFxTXSs/KVxccytpblxccysoW1xcc1xcU10rPykoPzpcXHMrYXNcXHMrKFtcXHNcXFNdKz8pKT8oPzpcXHMrdHJhY2tcXHMrYnlcXHMrKFtcXHNcXFNdKz8pKT9cXHMqJC8pO2lmKCFrKXRocm93IGQoXCJpZXhwXCIsaCk7dmFyIG49a1sxXSxwPWtbMl0scT1rWzNdLHQ9a1s0XSxrPW4ubWF0Y2goL14oPzooXFxzKltcXCRcXHddKyl8XFwoXFxzKihbXFwkXFx3XSspXFxzKixcXHMqKFtcXCRcXHddKylcXHMqXFwpKSQvKTtpZighayl0aHJvdyBkKFwiaWlkZXhwXCIsbik7dmFyIHM9a1szXXx8a1sxXSxGPWtbMl07aWYocSYmKCEvXlskYS16QS1aX11bJGEtekEtWjAtOV9dKiQvLnRlc3QocSl8fFxuL14obnVsbHx1bmRlZmluZWR8dGhpc3xcXCRpbmRleHxcXCRmaXJzdHxcXCRtaWRkbGV8XFwkbGFzdHxcXCRldmVufFxcJG9kZHxcXCRwYXJlbnR8XFwkcm9vdHxcXCRpZCkkLy50ZXN0KHEpKSl0aHJvdyBkKFwiYmFkaWRlbnRcIixxKTt2YXIgdixtLEMseSx3PXskaWQ6TmF9O3Q/dj1hKHQpOihDPWZ1bmN0aW9uKGEsYyl7cmV0dXJuIE5hKGMpfSx5PWZ1bmN0aW9uKGEpe3JldHVybiBhfSk7cmV0dXJuIGZ1bmN0aW9uKGEsZixnLGssbil7diYmKG09ZnVuY3Rpb24oYyxkLGUpe0YmJih3W0ZdPWMpO3dbc109ZDt3LiRpbmRleD1lO3JldHVybiB2KGEsdyl9KTt2YXIgdD1qYSgpO2EuJHdhdGNoQ29sbGVjdGlvbihwLGZ1bmN0aW9uKGcpe3ZhciBrLHAsdj1mWzBdLEcsdz1qYSgpLEQsSSxBLHosSCxPLHg7cSYmKGFbcV09Zyk7aWYoVGEoZykpSD1nLHA9bXx8QztlbHNle3A9bXx8eTtIPVtdO2Zvcih4IGluIGcpZy5oYXNPd25Qcm9wZXJ0eSh4KSYmXCIkXCIhPXguY2hhckF0KDApJiZILnB1c2goeCk7SC5zb3J0KCl9RD1cbkgubGVuZ3RoO3g9QXJyYXkoRCk7Zm9yKGs9MDtrPEQ7aysrKWlmKEk9Zz09PUg/azpIW2tdLEE9Z1tJXSx6PXAoSSxBLGspLHRbel0pTz10W3pdLGRlbGV0ZSB0W3pdLHdbel09Tyx4W2tdPU87ZWxzZXtpZih3W3pdKXRocm93IHIoeCxmdW5jdGlvbihhKXthJiZhLnNjb3BlJiYodFthLmlkXT1hKX0pLGQoXCJkdXBlc1wiLGgseixBKTt4W2tdPXtpZDp6LHNjb3BlOnUsY2xvbmU6dX07d1t6XT0hMH1mb3IoRyBpbiB0KXtPPXRbR107ej11YihPLmNsb25lKTtjLmxlYXZlKHopO2lmKHpbMF0ucGFyZW50Tm9kZSlmb3Ioaz0wLHA9ei5sZW5ndGg7azxwO2srKyl6W2tdLiQkTkdfUkVNT1ZFRD0hMDtPLnNjb3BlLiRkZXN0cm95KCl9Zm9yKGs9MDtrPEQ7aysrKWlmKEk9Zz09PUg/azpIW2tdLEE9Z1tJXSxPPXhba10sTy5zY29wZSl7Rz12O2RvIEc9Ry5uZXh0U2libGluZzt3aGlsZShHJiZHLiQkTkdfUkVNT1ZFRCk7Ty5jbG9uZVswXSE9RyYmYy5tb3ZlKHViKE8uY2xvbmUpLG51bGwsQih2KSk7XG52PU8uY2xvbmVbTy5jbG9uZS5sZW5ndGgtMV07ZShPLnNjb3BlLGsscyxBLEYsSSxEKX1lbHNlIG4oZnVuY3Rpb24oYSxkKXtPLnNjb3BlPWQ7dmFyIGY9bC5jbG9uZU5vZGUoITEpO2FbYS5sZW5ndGgrK109ZjtjLmVudGVyKGEsbnVsbCxCKHYpKTt2PWY7Ty5jbG9uZT1hO3dbTy5pZF09TztlKE8uc2NvcGUsayxzLEEsRixJLEQpfSk7dD13fSl9fX19XSxvZT1bXCIkYW5pbWF0ZVwiLGZ1bmN0aW9uKGEpe3JldHVybntyZXN0cmljdDpcIkFcIixtdWx0aUVsZW1lbnQ6ITAsbGluazpmdW5jdGlvbihjLGQsZSl7Yy4kd2F0Y2goZS5uZ1Nob3csZnVuY3Rpb24oYyl7YVtjP1wicmVtb3ZlQ2xhc3NcIjpcImFkZENsYXNzXCJdKGQsXCJuZy1oaWRlXCIse3RlbXBDbGFzc2VzOlwibmctaGlkZS1hbmltYXRlXCJ9KX0pfX19XSxoZT1bXCIkYW5pbWF0ZVwiLGZ1bmN0aW9uKGEpe3JldHVybntyZXN0cmljdDpcIkFcIixtdWx0aUVsZW1lbnQ6ITAsbGluazpmdW5jdGlvbihjLGQsZSl7Yy4kd2F0Y2goZS5uZ0hpZGUsZnVuY3Rpb24oYyl7YVtjP1xuXCJhZGRDbGFzc1wiOlwicmVtb3ZlQ2xhc3NcIl0oZCxcIm5nLWhpZGVcIix7dGVtcENsYXNzZXM6XCJuZy1oaWRlLWFuaW1hdGVcIn0pfSl9fX1dLHBlPUlhKGZ1bmN0aW9uKGEsYyxkKXthLiR3YXRjaChkLm5nU3R5bGUsZnVuY3Rpb24oYSxkKXtkJiZhIT09ZCYmcihkLGZ1bmN0aW9uKGEsZCl7Yy5jc3MoZCxcIlwiKX0pO2EmJmMuY3NzKGEpfSwhMCl9KSxxZT1bXCIkYW5pbWF0ZVwiLGZ1bmN0aW9uKGEpe3JldHVybntyZXN0cmljdDpcIkVBXCIscmVxdWlyZTpcIm5nU3dpdGNoXCIsY29udHJvbGxlcjpbXCIkc2NvcGVcIixmdW5jdGlvbigpe3RoaXMuY2FzZXM9e319XSxsaW5rOmZ1bmN0aW9uKGMsZCxlLGYpe3ZhciBnPVtdLGg9W10sbD1bXSxrPVtdLG49ZnVuY3Rpb24oYSxjKXtyZXR1cm4gZnVuY3Rpb24oKXthLnNwbGljZShjLDEpfX07Yy4kd2F0Y2goZS5uZ1N3aXRjaHx8ZS5vbixmdW5jdGlvbihjKXt2YXIgZCxlO2Q9MDtmb3IoZT1sLmxlbmd0aDtkPGU7KytkKWEuY2FuY2VsKGxbZF0pO2Q9bC5sZW5ndGg9XG4wO2ZvcihlPWsubGVuZ3RoO2Q8ZTsrK2Qpe3ZhciBzPXViKGhbZF0uY2xvbmUpO2tbZF0uJGRlc3Ryb3koKTsobFtkXT1hLmxlYXZlKHMpKS50aGVuKG4obCxkKSl9aC5sZW5ndGg9MDtrLmxlbmd0aD0wOyhnPWYuY2FzZXNbXCIhXCIrY118fGYuY2FzZXNbXCI/XCJdKSYmcihnLGZ1bmN0aW9uKGMpe2MudHJhbnNjbHVkZShmdW5jdGlvbihkLGUpe2sucHVzaChlKTt2YXIgZj1jLmVsZW1lbnQ7ZFtkLmxlbmd0aCsrXT1XLmNyZWF0ZUNvbW1lbnQoXCIgZW5kIG5nU3dpdGNoV2hlbjogXCIpO2gucHVzaCh7Y2xvbmU6ZH0pO2EuZW50ZXIoZCxmLnBhcmVudCgpLGYpfSl9KX0pfX19XSxyZT1JYSh7dHJhbnNjbHVkZTpcImVsZW1lbnRcIixwcmlvcml0eToxMjAwLHJlcXVpcmU6XCJebmdTd2l0Y2hcIixtdWx0aUVsZW1lbnQ6ITAsbGluazpmdW5jdGlvbihhLGMsZCxlLGYpe2UuY2FzZXNbXCIhXCIrZC5uZ1N3aXRjaFdoZW5dPWUuY2FzZXNbXCIhXCIrZC5uZ1N3aXRjaFdoZW5dfHxbXTtlLmNhc2VzW1wiIVwiK2QubmdTd2l0Y2hXaGVuXS5wdXNoKHt0cmFuc2NsdWRlOmYsXG5lbGVtZW50OmN9KX19KSxzZT1JYSh7dHJhbnNjbHVkZTpcImVsZW1lbnRcIixwcmlvcml0eToxMjAwLHJlcXVpcmU6XCJebmdTd2l0Y2hcIixtdWx0aUVsZW1lbnQ6ITAsbGluazpmdW5jdGlvbihhLGMsZCxlLGYpe2UuY2FzZXNbXCI/XCJdPWUuY2FzZXNbXCI/XCJdfHxbXTtlLmNhc2VzW1wiP1wiXS5wdXNoKHt0cmFuc2NsdWRlOmYsZWxlbWVudDpjfSl9fSksdWU9SWEoe3Jlc3RyaWN0OlwiRUFDXCIsbGluazpmdW5jdGlvbihhLGMsZCxlLGYpe2lmKCFmKXRocm93IFMoXCJuZ1RyYW5zY2x1ZGVcIikoXCJvcnBoYW5cIix4YShjKSk7ZihmdW5jdGlvbihhKXtjLmVtcHR5KCk7Yy5hcHBlbmQoYSl9KX19KSxWZD1bXCIkdGVtcGxhdGVDYWNoZVwiLGZ1bmN0aW9uKGEpe3JldHVybntyZXN0cmljdDpcIkVcIix0ZXJtaW5hbDohMCxjb21waWxlOmZ1bmN0aW9uKGMsZCl7XCJ0ZXh0L25nLXRlbXBsYXRlXCI9PWQudHlwZSYmYS5wdXQoZC5pZCxjWzBdLnRleHQpfX19XSxnZz1TKFwibmdPcHRpb25zXCIpLHRlPWVhKHtyZXN0cmljdDpcIkFcIixcbnRlcm1pbmFsOiEwfSksV2Q9W1wiJGNvbXBpbGVcIixcIiRwYXJzZVwiLGZ1bmN0aW9uKGEsYyl7dmFyIGQ9L15cXHMqKFtcXHNcXFNdKz8pKD86XFxzK2FzXFxzKyhbXFxzXFxTXSs/KSk/KD86XFxzK2dyb3VwXFxzK2J5XFxzKyhbXFxzXFxTXSs/KSk/XFxzK2ZvclxccysoPzooW1xcJFxcd11bXFwkXFx3XSopfCg/OlxcKFxccyooW1xcJFxcd11bXFwkXFx3XSopXFxzKixcXHMqKFtcXCRcXHddW1xcJFxcd10qKVxccypcXCkpKVxccytpblxccysoW1xcc1xcU10rPykoPzpcXHMrdHJhY2tcXHMrYnlcXHMrKFtcXHNcXFNdKz8pKT8kLyxlPXskc2V0Vmlld1ZhbHVlOkF9O3JldHVybntyZXN0cmljdDpcIkVcIixyZXF1aXJlOltcInNlbGVjdFwiLFwiP25nTW9kZWxcIl0sY29udHJvbGxlcjpbXCIkZWxlbWVudFwiLFwiJHNjb3BlXCIsXCIkYXR0cnNcIixmdW5jdGlvbihhLGMsZCl7dmFyIGw9dGhpcyxrPXt9LG49ZSxwO2wuZGF0YWJvdW5kPWQubmdNb2RlbDtsLmluaXQ9ZnVuY3Rpb24oYSxjLGQpe249YTtwPWR9O2wuYWRkT3B0aW9uPWZ1bmN0aW9uKGMsZCl7TWEoYywnXCJvcHRpb24gdmFsdWVcIicpO1xua1tjXT0hMDtuLiR2aWV3VmFsdWU9PWMmJihhLnZhbChjKSxwLnBhcmVudCgpJiZwLnJlbW92ZSgpKTtkJiZkWzBdLmhhc0F0dHJpYnV0ZShcInNlbGVjdGVkXCIpJiYoZFswXS5zZWxlY3RlZD0hMCl9O2wucmVtb3ZlT3B0aW9uPWZ1bmN0aW9uKGEpe3RoaXMuaGFzT3B0aW9uKGEpJiYoZGVsZXRlIGtbYV0sbi4kdmlld1ZhbHVlPT09YSYmdGhpcy5yZW5kZXJVbmtub3duT3B0aW9uKGEpKX07bC5yZW5kZXJVbmtub3duT3B0aW9uPWZ1bmN0aW9uKGMpe2M9XCI/IFwiK05hKGMpK1wiID9cIjtwLnZhbChjKTthLnByZXBlbmQocCk7YS52YWwoYyk7cC5wcm9wKFwic2VsZWN0ZWRcIiwhMCl9O2wuaGFzT3B0aW9uPWZ1bmN0aW9uKGEpe3JldHVybiBrLmhhc093blByb3BlcnR5KGEpfTtjLiRvbihcIiRkZXN0cm95XCIsZnVuY3Rpb24oKXtsLnJlbmRlclVua25vd25PcHRpb249QX0pfV0sbGluazpmdW5jdGlvbihlLGcsaCxsKXtmdW5jdGlvbiBrKGEsYyxkLGUpe2QuJHJlbmRlcj1mdW5jdGlvbigpe3ZhciBhPVxuZC4kdmlld1ZhbHVlO2UuaGFzT3B0aW9uKGEpPyh6LnBhcmVudCgpJiZ6LnJlbW92ZSgpLGMudmFsKGEpLFwiXCI9PT1hJiZ2LnByb3AoXCJzZWxlY3RlZFwiLCEwKSk6bnVsbD09YSYmdj9jLnZhbChcIlwiKTplLnJlbmRlclVua25vd25PcHRpb24oYSl9O2Mub24oXCJjaGFuZ2VcIixmdW5jdGlvbigpe2EuJGFwcGx5KGZ1bmN0aW9uKCl7ei5wYXJlbnQoKSYmei5yZW1vdmUoKTtkLiRzZXRWaWV3VmFsdWUoYy52YWwoKSl9KX0pfWZ1bmN0aW9uIG4oYSxjLGQpe3ZhciBlO2QuJHJlbmRlcj1mdW5jdGlvbigpe3ZhciBhPW5ldyBnYihkLiR2aWV3VmFsdWUpO3IoYy5maW5kKFwib3B0aW9uXCIpLGZ1bmN0aW9uKGMpe2Muc2VsZWN0ZWQ9eShhLmdldChjLnZhbHVlKSl9KX07YS4kd2F0Y2goZnVuY3Rpb24oKXtpYShlLGQuJHZpZXdWYWx1ZSl8fChlPXNhKGQuJHZpZXdWYWx1ZSksZC4kcmVuZGVyKCkpfSk7Yy5vbihcImNoYW5nZVwiLGZ1bmN0aW9uKCl7YS4kYXBwbHkoZnVuY3Rpb24oKXt2YXIgYT1bXTtyKGMuZmluZChcIm9wdGlvblwiKSxcbmZ1bmN0aW9uKGMpe2Muc2VsZWN0ZWQmJmEucHVzaChjLnZhbHVlKX0pO2QuJHNldFZpZXdWYWx1ZShhKX0pfSl9ZnVuY3Rpb24gcChlLGYsZyl7ZnVuY3Rpb24gaChhLGMsZCl7VFtBXT1kO0kmJihUW0ldPWMpO3JldHVybiBhKGUsVCl9ZnVuY3Rpb24gbChhKXt2YXIgYztpZih0KWlmKEsmJkgoYSkpe2M9bmV3IGdiKFtdKTtmb3IodmFyIGQ9MDtkPGEubGVuZ3RoO2QrKyljLnB1dChoKEssbnVsbCxhW2RdKSwhMCl9ZWxzZSBjPW5ldyBnYihhKTtlbHNlIEsmJihhPWgoSyxudWxsLGEpKTtyZXR1cm4gZnVuY3Rpb24oZCxlKXt2YXIgZjtmPUs/Szp4P3g6RTtyZXR1cm4gdD95KGMucmVtb3ZlKGgoZixkLGUpKSk6YT09PWgoZixkLGUpfX1mdW5jdGlvbiBrKCl7bXx8KGUuJCRwb3N0RGlnZXN0KHApLG09ITApfWZ1bmN0aW9uIG4oYSxjLGQpe2FbY109YVtjXXx8MDthW2NdKz1kPzE6LTF9ZnVuY3Rpb24gcCgpe209ITE7dmFyIGE9e1wiXCI6W119LGM9W1wiXCJdLGQsayxzLHUsdjtzPWcuJHZpZXdWYWx1ZTtcbnU9TChlKXx8W107dmFyIEE9ST9PYmplY3Qua2V5cyh1KS5zb3J0KCk6dSx4LEIsSCxFLFA9e307dj1sKHMpO3ZhciBOPSExLFUsVztSPXt9O2ZvcihFPTA7SD1BLmxlbmd0aCxFPEg7RSsrKXt4PUU7aWYoSSYmKHg9QVtFXSxcIiRcIj09PXguY2hhckF0KDApKSljb250aW51ZTtCPXVbeF07ZD1oKE0seCxCKXx8XCJcIjsoaz1hW2RdKXx8KGs9YVtkXT1bXSxjLnB1c2goZCkpO2Q9dih4LEIpO049Tnx8ZDtCPWgoeix4LEIpO0I9eShCKT9COlwiXCI7Vz1LP0soZSxUKTpJP0FbRV06RTtLJiYoUltXXT14KTtrLnB1c2goe2lkOlcsbGFiZWw6QixzZWxlY3RlZDpkfSl9dHx8KHd8fG51bGw9PT1zP2FbXCJcIl0udW5zaGlmdCh7aWQ6XCJcIixsYWJlbDpcIlwiLHNlbGVjdGVkOiFOfSk6Tnx8YVtcIlwiXS51bnNoaWZ0KHtpZDpcIj9cIixsYWJlbDpcIlwiLHNlbGVjdGVkOiEwfSkpO3g9MDtmb3IoQT1jLmxlbmd0aDt4PEE7eCsrKXtkPWNbeF07az1hW2RdO1MubGVuZ3RoPD14PyhzPXtlbGVtZW50OkQuY2xvbmUoKS5hdHRyKFwibGFiZWxcIixcbmQpLGxhYmVsOmsubGFiZWx9LHU9W3NdLFMucHVzaCh1KSxmLmFwcGVuZChzLmVsZW1lbnQpKToodT1TW3hdLHM9dVswXSxzLmxhYmVsIT1kJiZzLmVsZW1lbnQuYXR0cihcImxhYmVsXCIscy5sYWJlbD1kKSk7Tj1udWxsO0U9MDtmb3IoSD1rLmxlbmd0aDtFPEg7RSsrKWQ9a1tFXSwodj11W0UrMV0pPyhOPXYuZWxlbWVudCx2LmxhYmVsIT09ZC5sYWJlbCYmKG4oUCx2LmxhYmVsLCExKSxuKFAsZC5sYWJlbCwhMCksTi50ZXh0KHYubGFiZWw9ZC5sYWJlbCksTi5wcm9wKFwibGFiZWxcIix2LmxhYmVsKSksdi5pZCE9PWQuaWQmJk4udmFsKHYuaWQ9ZC5pZCksTlswXS5zZWxlY3RlZCE9PWQuc2VsZWN0ZWQmJihOLnByb3AoXCJzZWxlY3RlZFwiLHYuc2VsZWN0ZWQ9ZC5zZWxlY3RlZCksUmEmJk4ucHJvcChcInNlbGVjdGVkXCIsdi5zZWxlY3RlZCkpKTooXCJcIj09PWQuaWQmJnc/VT13OihVPUMuY2xvbmUoKSkudmFsKGQuaWQpLnByb3AoXCJzZWxlY3RlZFwiLGQuc2VsZWN0ZWQpLmF0dHIoXCJzZWxlY3RlZFwiLFxuZC5zZWxlY3RlZCkucHJvcChcImxhYmVsXCIsZC5sYWJlbCkudGV4dChkLmxhYmVsKSx1LnB1c2godj17ZWxlbWVudDpVLGxhYmVsOmQubGFiZWwsaWQ6ZC5pZCxzZWxlY3RlZDpkLnNlbGVjdGVkfSksbihQLGQubGFiZWwsITApLE4/Ti5hZnRlcihVKTpzLmVsZW1lbnQuYXBwZW5kKFUpLE49VSk7Zm9yKEUrKzt1Lmxlbmd0aD5FOylkPXUucG9wKCksbihQLGQubGFiZWwsITEpLGQuZWxlbWVudC5yZW1vdmUoKX1mb3IoO1MubGVuZ3RoPng7KXtrPVMucG9wKCk7Zm9yKEU9MTtFPGsubGVuZ3RoOysrRSluKFAsa1tFXS5sYWJlbCwhMSk7a1swXS5lbGVtZW50LnJlbW92ZSgpfXIoUCxmdW5jdGlvbihhLGMpezA8YT9xLmFkZE9wdGlvbihjKTowPmEmJnEucmVtb3ZlT3B0aW9uKGMpfSl9dmFyIHY7aWYoISh2PXMubWF0Y2goZCkpKXRocm93IGdnKFwiaWV4cFwiLHMseGEoZikpO3ZhciB6PWModlsyXXx8dlsxXSksQT12WzRdfHx2WzZdLEI9LyBhcyAvLnRlc3QodlswXSkmJnZbMV0seD1CP2MoQik6XG5udWxsLEk9dls1XSxNPWModlszXXx8XCJcIiksRT1jKHZbMl0/dlsxXTpBKSxMPWModls3XSksSz12WzhdP2Modls4XSk6bnVsbCxSPXt9LFM9W1t7ZWxlbWVudDpmLGxhYmVsOlwiXCJ9XV0sVD17fTt3JiYoYSh3KShlKSx3LnJlbW92ZUNsYXNzKFwibmctc2NvcGVcIiksdy5yZW1vdmUoKSk7Zi5lbXB0eSgpO2Yub24oXCJjaGFuZ2VcIixmdW5jdGlvbigpe2UuJGFwcGx5KGZ1bmN0aW9uKCl7dmFyIGE9TChlKXx8W10sYztpZih0KWM9W10scihmLnZhbCgpLGZ1bmN0aW9uKGQpe2Q9Sz9SW2RdOmQ7Yy5wdXNoKFwiP1wiPT09ZD91OlwiXCI9PT1kP251bGw6aCh4P3g6RSxkLGFbZF0pKX0pO2Vsc2V7dmFyIGQ9Sz9SW2YudmFsKCldOmYudmFsKCk7Yz1cIj9cIj09PWQ/dTpcIlwiPT09ZD9udWxsOmgoeD94OkUsZCxhW2RdKX1nLiRzZXRWaWV3VmFsdWUoYyk7cCgpfSl9KTtnLiRyZW5kZXI9cDtlLiR3YXRjaENvbGxlY3Rpb24oTCxrKTtlLiR3YXRjaENvbGxlY3Rpb24oZnVuY3Rpb24oKXt2YXIgYT1MKGUpLGM7XG5pZihhJiZIKGEpKXtjPUFycmF5KGEubGVuZ3RoKTtmb3IodmFyIGQ9MCxmPWEubGVuZ3RoO2Q8ZjtkKyspY1tkXT1oKHosZCxhW2RdKX1lbHNlIGlmKGEpZm9yKGQgaW4gYz17fSxhKWEuaGFzT3duUHJvcGVydHkoZCkmJihjW2RdPWgoeixkLGFbZF0pKTtyZXR1cm4gY30sayk7dCYmZS4kd2F0Y2hDb2xsZWN0aW9uKGZ1bmN0aW9uKCl7cmV0dXJuIGcuJG1vZGVsVmFsdWV9LGspfWlmKGxbMV0pe3ZhciBxPWxbMF07bD1sWzFdO3ZhciB0PWgubXVsdGlwbGUscz1oLm5nT3B0aW9ucyx3PSExLHYsbT0hMSxDPUIoVy5jcmVhdGVFbGVtZW50KFwib3B0aW9uXCIpKSxEPUIoVy5jcmVhdGVFbGVtZW50KFwib3B0Z3JvdXBcIikpLHo9Qy5jbG9uZSgpO2g9MDtmb3IodmFyIEE9Zy5jaGlsZHJlbigpLHg9QS5sZW5ndGg7aDx4O2grKylpZihcIlwiPT09QVtoXS52YWx1ZSl7dj13PUEuZXEoaCk7YnJlYWt9cS5pbml0KGwsdyx6KTt0JiYobC4kaXNFbXB0eT1mdW5jdGlvbihhKXtyZXR1cm4hYXx8MD09PWEubGVuZ3RofSk7XG5zP3AoZSxnLGwpOnQ/bihlLGcsbCk6ayhlLGcsbCxxKX19fX1dLFlkPVtcIiRpbnRlcnBvbGF0ZVwiLGZ1bmN0aW9uKGEpe3ZhciBjPXthZGRPcHRpb246QSxyZW1vdmVPcHRpb246QX07cmV0dXJue3Jlc3RyaWN0OlwiRVwiLHByaW9yaXR5OjEwMCxjb21waWxlOmZ1bmN0aW9uKGQsZSl7aWYoRChlLnZhbHVlKSl7dmFyIGY9YShkLnRleHQoKSwhMCk7Znx8ZS4kc2V0KFwidmFsdWVcIixkLnRleHQoKSl9cmV0dXJuIGZ1bmN0aW9uKGEsZCxlKXt2YXIgaz1kLnBhcmVudCgpLG49ay5kYXRhKFwiJHNlbGVjdENvbnRyb2xsZXJcIil8fGsucGFyZW50KCkuZGF0YShcIiRzZWxlY3RDb250cm9sbGVyXCIpO24mJm4uZGF0YWJvdW5kfHwobj1jKTtmP2EuJHdhdGNoKGYsZnVuY3Rpb24oYSxjKXtlLiRzZXQoXCJ2YWx1ZVwiLGEpO2MhPT1hJiZuLnJlbW92ZU9wdGlvbihjKTtuLmFkZE9wdGlvbihhLGQpfSk6bi5hZGRPcHRpb24oZS52YWx1ZSxkKTtkLm9uKFwiJGRlc3Ryb3lcIixmdW5jdGlvbigpe24ucmVtb3ZlT3B0aW9uKGUudmFsdWUpfSl9fX19XSxcblhkPWVhKHtyZXN0cmljdDpcIkVcIix0ZXJtaW5hbDohMX0pLEFjPWZ1bmN0aW9uKCl7cmV0dXJue3Jlc3RyaWN0OlwiQVwiLHJlcXVpcmU6XCI/bmdNb2RlbFwiLGxpbms6ZnVuY3Rpb24oYSxjLGQsZSl7ZSYmKGQucmVxdWlyZWQ9ITAsZS4kdmFsaWRhdG9ycy5yZXF1aXJlZD1mdW5jdGlvbihhLGMpe3JldHVybiFkLnJlcXVpcmVkfHwhZS4kaXNFbXB0eShjKX0sZC4kb2JzZXJ2ZShcInJlcXVpcmVkXCIsZnVuY3Rpb24oKXtlLiR2YWxpZGF0ZSgpfSkpfX19LHpjPWZ1bmN0aW9uKCl7cmV0dXJue3Jlc3RyaWN0OlwiQVwiLHJlcXVpcmU6XCI/bmdNb2RlbFwiLGxpbms6ZnVuY3Rpb24oYSxjLGQsZSl7aWYoZSl7dmFyIGYsZz1kLm5nUGF0dGVybnx8ZC5wYXR0ZXJuO2QuJG9ic2VydmUoXCJwYXR0ZXJuXCIsZnVuY3Rpb24oYSl7eChhKSYmMDxhLmxlbmd0aCYmKGE9bmV3IFJlZ0V4cChcIl5cIithK1wiJFwiKSk7aWYoYSYmIWEudGVzdCl0aHJvdyBTKFwibmdQYXR0ZXJuXCIpKFwibm9yZWdleHBcIixnLGEseGEoYykpO2Y9XG5hfHx1O2UuJHZhbGlkYXRlKCl9KTtlLiR2YWxpZGF0b3JzLnBhdHRlcm49ZnVuY3Rpb24oYSxjKXtyZXR1cm4gZS4kaXNFbXB0eShjKXx8RChmKXx8Zi50ZXN0KGMpfX19fX0sQ2M9ZnVuY3Rpb24oKXtyZXR1cm57cmVzdHJpY3Q6XCJBXCIscmVxdWlyZTpcIj9uZ01vZGVsXCIsbGluazpmdW5jdGlvbihhLGMsZCxlKXtpZihlKXt2YXIgZj0tMTtkLiRvYnNlcnZlKFwibWF4bGVuZ3RoXCIsZnVuY3Rpb24oYSl7YT1hYShhKTtmPWlzTmFOKGEpPy0xOmE7ZS4kdmFsaWRhdGUoKX0pO2UuJHZhbGlkYXRvcnMubWF4bGVuZ3RoPWZ1bmN0aW9uKGEsYyl7cmV0dXJuIDA+Znx8ZS4kaXNFbXB0eShjKXx8Yy5sZW5ndGg8PWZ9fX19fSxCYz1mdW5jdGlvbigpe3JldHVybntyZXN0cmljdDpcIkFcIixyZXF1aXJlOlwiP25nTW9kZWxcIixsaW5rOmZ1bmN0aW9uKGEsYyxkLGUpe2lmKGUpe3ZhciBmPTA7ZC4kb2JzZXJ2ZShcIm1pbmxlbmd0aFwiLGZ1bmN0aW9uKGEpe2Y9YWEoYSl8fDA7ZS4kdmFsaWRhdGUoKX0pO2UuJHZhbGlkYXRvcnMubWlubGVuZ3RoPVxuZnVuY3Rpb24oYSxjKXtyZXR1cm4gZS4kaXNFbXB0eShjKXx8Yy5sZW5ndGg+PWZ9fX19fTtSLmFuZ3VsYXIuYm9vdHN0cmFwP2NvbnNvbGUubG9nKFwiV0FSTklORzogVHJpZWQgdG8gbG9hZCBhbmd1bGFyIG1vcmUgdGhhbiBvbmNlLlwiKTooTmQoKSxQZChjYSksQihXKS5yZWFkeShmdW5jdGlvbigpe0pkKFcsdGMpfSkpfSkod2luZG93LGRvY3VtZW50KTshd2luZG93LmFuZ3VsYXIuJCRjc3AoKSYmd2luZG93LmFuZ3VsYXIuZWxlbWVudChkb2N1bWVudC5oZWFkKS5wcmVwZW5kKCc8c3R5bGUgdHlwZT1cInRleHQvY3NzXCI+QGNoYXJzZXQgXCJVVEYtOFwiO1tuZ1xcXFw6Y2xvYWtdLFtuZy1jbG9ha10sW2RhdGEtbmctY2xvYWtdLFt4LW5nLWNsb2FrXSwubmctY2xvYWssLngtbmctY2xvYWssLm5nLWhpZGU6bm90KC5uZy1oaWRlLWFuaW1hdGUpe2Rpc3BsYXk6bm9uZSAhaW1wb3J0YW50O31uZ1xcXFw6Zm9ybXtkaXNwbGF5OmJsb2NrO308L3N0eWxlPicpO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9YW5ndWxhci5taW4uanMubWFwXG4iLCIvKiFcbiAqIEJvb3RzdHJhcCB2My4zLjYgKGh0dHA6Ly9nZXRib290c3RyYXAuY29tKVxuICogQ29weXJpZ2h0IDIwMTEtMjAxNSBUd2l0dGVyLCBJbmMuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqL1xuXG5pZiAodHlwZW9mIGpRdWVyeSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgdGhyb3cgbmV3IEVycm9yKCdCb290c3RyYXBcXCdzIEphdmFTY3JpcHQgcmVxdWlyZXMgalF1ZXJ5Jylcbn1cblxuK2Z1bmN0aW9uICgkKSB7XG4gICd1c2Ugc3RyaWN0JztcbiAgdmFyIHZlcnNpb24gPSAkLmZuLmpxdWVyeS5zcGxpdCgnICcpWzBdLnNwbGl0KCcuJylcbiAgaWYgKCh2ZXJzaW9uWzBdIDwgMiAmJiB2ZXJzaW9uWzFdIDwgOSkgfHwgKHZlcnNpb25bMF0gPT0gMSAmJiB2ZXJzaW9uWzFdID09IDkgJiYgdmVyc2lvblsyXSA8IDEpIHx8ICh2ZXJzaW9uWzBdID4gMikpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0Jvb3RzdHJhcFxcJ3MgSmF2YVNjcmlwdCByZXF1aXJlcyBqUXVlcnkgdmVyc2lvbiAxLjkuMSBvciBoaWdoZXIsIGJ1dCBsb3dlciB0aGFuIHZlcnNpb24gMycpXG4gIH1cbn0oalF1ZXJ5KTtcblxuLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBCb290c3RyYXA6IHRyYW5zaXRpb24uanMgdjMuMy42XG4gKiBodHRwOi8vZ2V0Ym9vdHN0cmFwLmNvbS9qYXZhc2NyaXB0LyN0cmFuc2l0aW9uc1xuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBDb3B5cmlnaHQgMjAxMS0yMDE1IFR3aXR0ZXIsIEluYy5cbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0UpXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuXG4rZnVuY3Rpb24gKCQpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIC8vIENTUyBUUkFOU0lUSU9OIFNVUFBPUlQgKFNob3V0b3V0OiBodHRwOi8vd3d3Lm1vZGVybml6ci5jb20vKVxuICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICBmdW5jdGlvbiB0cmFuc2l0aW9uRW5kKCkge1xuICAgIHZhciBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2Jvb3RzdHJhcCcpXG5cbiAgICB2YXIgdHJhbnNFbmRFdmVudE5hbWVzID0ge1xuICAgICAgV2Via2l0VHJhbnNpdGlvbiA6ICd3ZWJraXRUcmFuc2l0aW9uRW5kJyxcbiAgICAgIE1velRyYW5zaXRpb24gICAgOiAndHJhbnNpdGlvbmVuZCcsXG4gICAgICBPVHJhbnNpdGlvbiAgICAgIDogJ29UcmFuc2l0aW9uRW5kIG90cmFuc2l0aW9uZW5kJyxcbiAgICAgIHRyYW5zaXRpb24gICAgICAgOiAndHJhbnNpdGlvbmVuZCdcbiAgICB9XG5cbiAgICBmb3IgKHZhciBuYW1lIGluIHRyYW5zRW5kRXZlbnROYW1lcykge1xuICAgICAgaWYgKGVsLnN0eWxlW25hbWVdICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgcmV0dXJuIHsgZW5kOiB0cmFuc0VuZEV2ZW50TmFtZXNbbmFtZV0gfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBmYWxzZSAvLyBleHBsaWNpdCBmb3IgaWU4ICggIC5fLilcbiAgfVxuXG4gIC8vIGh0dHA6Ly9ibG9nLmFsZXhtYWNjYXcuY29tL2Nzcy10cmFuc2l0aW9uc1xuICAkLmZuLmVtdWxhdGVUcmFuc2l0aW9uRW5kID0gZnVuY3Rpb24gKGR1cmF0aW9uKSB7XG4gICAgdmFyIGNhbGxlZCA9IGZhbHNlXG4gICAgdmFyICRlbCA9IHRoaXNcbiAgICAkKHRoaXMpLm9uZSgnYnNUcmFuc2l0aW9uRW5kJywgZnVuY3Rpb24gKCkgeyBjYWxsZWQgPSB0cnVlIH0pXG4gICAgdmFyIGNhbGxiYWNrID0gZnVuY3Rpb24gKCkgeyBpZiAoIWNhbGxlZCkgJCgkZWwpLnRyaWdnZXIoJC5zdXBwb3J0LnRyYW5zaXRpb24uZW5kKSB9XG4gICAgc2V0VGltZW91dChjYWxsYmFjaywgZHVyYXRpb24pXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gICQoZnVuY3Rpb24gKCkge1xuICAgICQuc3VwcG9ydC50cmFuc2l0aW9uID0gdHJhbnNpdGlvbkVuZCgpXG5cbiAgICBpZiAoISQuc3VwcG9ydC50cmFuc2l0aW9uKSByZXR1cm5cblxuICAgICQuZXZlbnQuc3BlY2lhbC5ic1RyYW5zaXRpb25FbmQgPSB7XG4gICAgICBiaW5kVHlwZTogJC5zdXBwb3J0LnRyYW5zaXRpb24uZW5kLFxuICAgICAgZGVsZWdhdGVUeXBlOiAkLnN1cHBvcnQudHJhbnNpdGlvbi5lbmQsXG4gICAgICBoYW5kbGU6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIGlmICgkKGUudGFyZ2V0KS5pcyh0aGlzKSkgcmV0dXJuIGUuaGFuZGxlT2JqLmhhbmRsZXIuYXBwbHkodGhpcywgYXJndW1lbnRzKVxuICAgICAgfVxuICAgIH1cbiAgfSlcblxufShqUXVlcnkpO1xuXG4vKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqIEJvb3RzdHJhcDogYWxlcnQuanMgdjMuMy42XG4gKiBodHRwOi8vZ2V0Ym9vdHN0cmFwLmNvbS9qYXZhc2NyaXB0LyNhbGVydHNcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogQ29weXJpZ2h0IDIwMTEtMjAxNSBUd2l0dGVyLCBJbmMuXG4gKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cblxuK2Z1bmN0aW9uICgkKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICAvLyBBTEVSVCBDTEFTUyBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT1cblxuICB2YXIgZGlzbWlzcyA9ICdbZGF0YS1kaXNtaXNzPVwiYWxlcnRcIl0nXG4gIHZhciBBbGVydCAgID0gZnVuY3Rpb24gKGVsKSB7XG4gICAgJChlbCkub24oJ2NsaWNrJywgZGlzbWlzcywgdGhpcy5jbG9zZSlcbiAgfVxuXG4gIEFsZXJ0LlZFUlNJT04gPSAnMy4zLjYnXG5cbiAgQWxlcnQuVFJBTlNJVElPTl9EVVJBVElPTiA9IDE1MFxuXG4gIEFsZXJ0LnByb3RvdHlwZS5jbG9zZSA9IGZ1bmN0aW9uIChlKSB7XG4gICAgdmFyICR0aGlzICAgID0gJCh0aGlzKVxuICAgIHZhciBzZWxlY3RvciA9ICR0aGlzLmF0dHIoJ2RhdGEtdGFyZ2V0JylcblxuICAgIGlmICghc2VsZWN0b3IpIHtcbiAgICAgIHNlbGVjdG9yID0gJHRoaXMuYXR0cignaHJlZicpXG4gICAgICBzZWxlY3RvciA9IHNlbGVjdG9yICYmIHNlbGVjdG9yLnJlcGxhY2UoLy4qKD89I1teXFxzXSokKS8sICcnKSAvLyBzdHJpcCBmb3IgaWU3XG4gICAgfVxuXG4gICAgdmFyICRwYXJlbnQgPSAkKHNlbGVjdG9yKVxuXG4gICAgaWYgKGUpIGUucHJldmVudERlZmF1bHQoKVxuXG4gICAgaWYgKCEkcGFyZW50Lmxlbmd0aCkge1xuICAgICAgJHBhcmVudCA9ICR0aGlzLmNsb3Nlc3QoJy5hbGVydCcpXG4gICAgfVxuXG4gICAgJHBhcmVudC50cmlnZ2VyKGUgPSAkLkV2ZW50KCdjbG9zZS5icy5hbGVydCcpKVxuXG4gICAgaWYgKGUuaXNEZWZhdWx0UHJldmVudGVkKCkpIHJldHVyblxuXG4gICAgJHBhcmVudC5yZW1vdmVDbGFzcygnaW4nKVxuXG4gICAgZnVuY3Rpb24gcmVtb3ZlRWxlbWVudCgpIHtcbiAgICAgIC8vIGRldGFjaCBmcm9tIHBhcmVudCwgZmlyZSBldmVudCB0aGVuIGNsZWFuIHVwIGRhdGFcbiAgICAgICRwYXJlbnQuZGV0YWNoKCkudHJpZ2dlcignY2xvc2VkLmJzLmFsZXJ0JykucmVtb3ZlKClcbiAgICB9XG5cbiAgICAkLnN1cHBvcnQudHJhbnNpdGlvbiAmJiAkcGFyZW50Lmhhc0NsYXNzKCdmYWRlJykgP1xuICAgICAgJHBhcmVudFxuICAgICAgICAub25lKCdic1RyYW5zaXRpb25FbmQnLCByZW1vdmVFbGVtZW50KVxuICAgICAgICAuZW11bGF0ZVRyYW5zaXRpb25FbmQoQWxlcnQuVFJBTlNJVElPTl9EVVJBVElPTikgOlxuICAgICAgcmVtb3ZlRWxlbWVudCgpXG4gIH1cblxuXG4gIC8vIEFMRVJUIFBMVUdJTiBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgZnVuY3Rpb24gUGx1Z2luKG9wdGlvbikge1xuICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgdmFyICR0aGlzID0gJCh0aGlzKVxuICAgICAgdmFyIGRhdGEgID0gJHRoaXMuZGF0YSgnYnMuYWxlcnQnKVxuXG4gICAgICBpZiAoIWRhdGEpICR0aGlzLmRhdGEoJ2JzLmFsZXJ0JywgKGRhdGEgPSBuZXcgQWxlcnQodGhpcykpKVxuICAgICAgaWYgKHR5cGVvZiBvcHRpb24gPT0gJ3N0cmluZycpIGRhdGFbb3B0aW9uXS5jYWxsKCR0aGlzKVxuICAgIH0pXG4gIH1cblxuICB2YXIgb2xkID0gJC5mbi5hbGVydFxuXG4gICQuZm4uYWxlcnQgICAgICAgICAgICAgPSBQbHVnaW5cbiAgJC5mbi5hbGVydC5Db25zdHJ1Y3RvciA9IEFsZXJ0XG5cblxuICAvLyBBTEVSVCBOTyBDT05GTElDVFxuICAvLyA9PT09PT09PT09PT09PT09PVxuXG4gICQuZm4uYWxlcnQubm9Db25mbGljdCA9IGZ1bmN0aW9uICgpIHtcbiAgICAkLmZuLmFsZXJ0ID0gb2xkXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG5cbiAgLy8gQUxFUlQgREFUQS1BUElcbiAgLy8gPT09PT09PT09PT09PT1cblxuICAkKGRvY3VtZW50KS5vbignY2xpY2suYnMuYWxlcnQuZGF0YS1hcGknLCBkaXNtaXNzLCBBbGVydC5wcm90b3R5cGUuY2xvc2UpXG5cbn0oalF1ZXJ5KTtcblxuLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBCb290c3RyYXA6IGJ1dHRvbi5qcyB2My4zLjZcbiAqIGh0dHA6Ly9nZXRib290c3RyYXAuY29tL2phdmFzY3JpcHQvI2J1dHRvbnNcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogQ29weXJpZ2h0IDIwMTEtMjAxNSBUd2l0dGVyLCBJbmMuXG4gKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cblxuK2Z1bmN0aW9uICgkKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICAvLyBCVVRUT04gUFVCTElDIENMQVNTIERFRklOSVRJT05cbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgdmFyIEJ1dHRvbiA9IGZ1bmN0aW9uIChlbGVtZW50LCBvcHRpb25zKSB7XG4gICAgdGhpcy4kZWxlbWVudCAgPSAkKGVsZW1lbnQpXG4gICAgdGhpcy5vcHRpb25zICAgPSAkLmV4dGVuZCh7fSwgQnV0dG9uLkRFRkFVTFRTLCBvcHRpb25zKVxuICAgIHRoaXMuaXNMb2FkaW5nID0gZmFsc2VcbiAgfVxuXG4gIEJ1dHRvbi5WRVJTSU9OICA9ICczLjMuNidcblxuICBCdXR0b24uREVGQVVMVFMgPSB7XG4gICAgbG9hZGluZ1RleHQ6ICdsb2FkaW5nLi4uJ1xuICB9XG5cbiAgQnV0dG9uLnByb3RvdHlwZS5zZXRTdGF0ZSA9IGZ1bmN0aW9uIChzdGF0ZSkge1xuICAgIHZhciBkICAgID0gJ2Rpc2FibGVkJ1xuICAgIHZhciAkZWwgID0gdGhpcy4kZWxlbWVudFxuICAgIHZhciB2YWwgID0gJGVsLmlzKCdpbnB1dCcpID8gJ3ZhbCcgOiAnaHRtbCdcbiAgICB2YXIgZGF0YSA9ICRlbC5kYXRhKClcblxuICAgIHN0YXRlICs9ICdUZXh0J1xuXG4gICAgaWYgKGRhdGEucmVzZXRUZXh0ID09IG51bGwpICRlbC5kYXRhKCdyZXNldFRleHQnLCAkZWxbdmFsXSgpKVxuXG4gICAgLy8gcHVzaCB0byBldmVudCBsb29wIHRvIGFsbG93IGZvcm1zIHRvIHN1Ym1pdFxuICAgIHNldFRpbWVvdXQoJC5wcm94eShmdW5jdGlvbiAoKSB7XG4gICAgICAkZWxbdmFsXShkYXRhW3N0YXRlXSA9PSBudWxsID8gdGhpcy5vcHRpb25zW3N0YXRlXSA6IGRhdGFbc3RhdGVdKVxuXG4gICAgICBpZiAoc3RhdGUgPT0gJ2xvYWRpbmdUZXh0Jykge1xuICAgICAgICB0aGlzLmlzTG9hZGluZyA9IHRydWVcbiAgICAgICAgJGVsLmFkZENsYXNzKGQpLmF0dHIoZCwgZClcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5pc0xvYWRpbmcpIHtcbiAgICAgICAgdGhpcy5pc0xvYWRpbmcgPSBmYWxzZVxuICAgICAgICAkZWwucmVtb3ZlQ2xhc3MoZCkucmVtb3ZlQXR0cihkKVxuICAgICAgfVxuICAgIH0sIHRoaXMpLCAwKVxuICB9XG5cbiAgQnV0dG9uLnByb3RvdHlwZS50b2dnbGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGNoYW5nZWQgPSB0cnVlXG4gICAgdmFyICRwYXJlbnQgPSB0aGlzLiRlbGVtZW50LmNsb3Nlc3QoJ1tkYXRhLXRvZ2dsZT1cImJ1dHRvbnNcIl0nKVxuXG4gICAgaWYgKCRwYXJlbnQubGVuZ3RoKSB7XG4gICAgICB2YXIgJGlucHV0ID0gdGhpcy4kZWxlbWVudC5maW5kKCdpbnB1dCcpXG4gICAgICBpZiAoJGlucHV0LnByb3AoJ3R5cGUnKSA9PSAncmFkaW8nKSB7XG4gICAgICAgIGlmICgkaW5wdXQucHJvcCgnY2hlY2tlZCcpKSBjaGFuZ2VkID0gZmFsc2VcbiAgICAgICAgJHBhcmVudC5maW5kKCcuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgIHRoaXMuJGVsZW1lbnQuYWRkQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICB9IGVsc2UgaWYgKCRpbnB1dC5wcm9wKCd0eXBlJykgPT0gJ2NoZWNrYm94Jykge1xuICAgICAgICBpZiAoKCRpbnB1dC5wcm9wKCdjaGVja2VkJykpICE9PSB0aGlzLiRlbGVtZW50Lmhhc0NsYXNzKCdhY3RpdmUnKSkgY2hhbmdlZCA9IGZhbHNlXG4gICAgICAgIHRoaXMuJGVsZW1lbnQudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICB9XG4gICAgICAkaW5wdXQucHJvcCgnY2hlY2tlZCcsIHRoaXMuJGVsZW1lbnQuaGFzQ2xhc3MoJ2FjdGl2ZScpKVxuICAgICAgaWYgKGNoYW5nZWQpICRpbnB1dC50cmlnZ2VyKCdjaGFuZ2UnKVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLiRlbGVtZW50LmF0dHIoJ2FyaWEtcHJlc3NlZCcsICF0aGlzLiRlbGVtZW50Lmhhc0NsYXNzKCdhY3RpdmUnKSlcbiAgICAgIHRoaXMuJGVsZW1lbnQudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpXG4gICAgfVxuICB9XG5cblxuICAvLyBCVVRUT04gUExVR0lOIERFRklOSVRJT05cbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgZnVuY3Rpb24gUGx1Z2luKG9wdGlvbikge1xuICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgdmFyICR0aGlzICAgPSAkKHRoaXMpXG4gICAgICB2YXIgZGF0YSAgICA9ICR0aGlzLmRhdGEoJ2JzLmJ1dHRvbicpXG4gICAgICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBvcHRpb24gPT0gJ29iamVjdCcgJiYgb3B0aW9uXG5cbiAgICAgIGlmICghZGF0YSkgJHRoaXMuZGF0YSgnYnMuYnV0dG9uJywgKGRhdGEgPSBuZXcgQnV0dG9uKHRoaXMsIG9wdGlvbnMpKSlcblxuICAgICAgaWYgKG9wdGlvbiA9PSAndG9nZ2xlJykgZGF0YS50b2dnbGUoKVxuICAgICAgZWxzZSBpZiAob3B0aW9uKSBkYXRhLnNldFN0YXRlKG9wdGlvbilcbiAgICB9KVxuICB9XG5cbiAgdmFyIG9sZCA9ICQuZm4uYnV0dG9uXG5cbiAgJC5mbi5idXR0b24gICAgICAgICAgICAgPSBQbHVnaW5cbiAgJC5mbi5idXR0b24uQ29uc3RydWN0b3IgPSBCdXR0b25cblxuXG4gIC8vIEJVVFRPTiBOTyBDT05GTElDVFxuICAvLyA9PT09PT09PT09PT09PT09PT1cblxuICAkLmZuLmJ1dHRvbi5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xuICAgICQuZm4uYnV0dG9uID0gb2xkXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG5cbiAgLy8gQlVUVE9OIERBVEEtQVBJXG4gIC8vID09PT09PT09PT09PT09PVxuXG4gICQoZG9jdW1lbnQpXG4gICAgLm9uKCdjbGljay5icy5idXR0b24uZGF0YS1hcGknLCAnW2RhdGEtdG9nZ2xlXj1cImJ1dHRvblwiXScsIGZ1bmN0aW9uIChlKSB7XG4gICAgICB2YXIgJGJ0biA9ICQoZS50YXJnZXQpXG4gICAgICBpZiAoISRidG4uaGFzQ2xhc3MoJ2J0bicpKSAkYnRuID0gJGJ0bi5jbG9zZXN0KCcuYnRuJylcbiAgICAgIFBsdWdpbi5jYWxsKCRidG4sICd0b2dnbGUnKVxuICAgICAgaWYgKCEoJChlLnRhcmdldCkuaXMoJ2lucHV0W3R5cGU9XCJyYWRpb1wiXScpIHx8ICQoZS50YXJnZXQpLmlzKCdpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0nKSkpIGUucHJldmVudERlZmF1bHQoKVxuICAgIH0pXG4gICAgLm9uKCdmb2N1cy5icy5idXR0b24uZGF0YS1hcGkgYmx1ci5icy5idXR0b24uZGF0YS1hcGknLCAnW2RhdGEtdG9nZ2xlXj1cImJ1dHRvblwiXScsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAkKGUudGFyZ2V0KS5jbG9zZXN0KCcuYnRuJykudG9nZ2xlQ2xhc3MoJ2ZvY3VzJywgL15mb2N1cyhpbik/JC8udGVzdChlLnR5cGUpKVxuICAgIH0pXG5cbn0oalF1ZXJ5KTtcblxuLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBCb290c3RyYXA6IGNhcm91c2VsLmpzIHYzLjMuNlxuICogaHR0cDovL2dldGJvb3RzdHJhcC5jb20vamF2YXNjcmlwdC8jY2Fyb3VzZWxcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogQ29weXJpZ2h0IDIwMTEtMjAxNSBUd2l0dGVyLCBJbmMuXG4gKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cblxuK2Z1bmN0aW9uICgkKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICAvLyBDQVJPVVNFTCBDTEFTUyBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICB2YXIgQ2Fyb3VzZWwgPSBmdW5jdGlvbiAoZWxlbWVudCwgb3B0aW9ucykge1xuICAgIHRoaXMuJGVsZW1lbnQgICAgPSAkKGVsZW1lbnQpXG4gICAgdGhpcy4kaW5kaWNhdG9ycyA9IHRoaXMuJGVsZW1lbnQuZmluZCgnLmNhcm91c2VsLWluZGljYXRvcnMnKVxuICAgIHRoaXMub3B0aW9ucyAgICAgPSBvcHRpb25zXG4gICAgdGhpcy5wYXVzZWQgICAgICA9IG51bGxcbiAgICB0aGlzLnNsaWRpbmcgICAgID0gbnVsbFxuICAgIHRoaXMuaW50ZXJ2YWwgICAgPSBudWxsXG4gICAgdGhpcy4kYWN0aXZlICAgICA9IG51bGxcbiAgICB0aGlzLiRpdGVtcyAgICAgID0gbnVsbFxuXG4gICAgdGhpcy5vcHRpb25zLmtleWJvYXJkICYmIHRoaXMuJGVsZW1lbnQub24oJ2tleWRvd24uYnMuY2Fyb3VzZWwnLCAkLnByb3h5KHRoaXMua2V5ZG93biwgdGhpcykpXG5cbiAgICB0aGlzLm9wdGlvbnMucGF1c2UgPT0gJ2hvdmVyJyAmJiAhKCdvbnRvdWNoc3RhcnQnIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCkgJiYgdGhpcy4kZWxlbWVudFxuICAgICAgLm9uKCdtb3VzZWVudGVyLmJzLmNhcm91c2VsJywgJC5wcm94eSh0aGlzLnBhdXNlLCB0aGlzKSlcbiAgICAgIC5vbignbW91c2VsZWF2ZS5icy5jYXJvdXNlbCcsICQucHJveHkodGhpcy5jeWNsZSwgdGhpcykpXG4gIH1cblxuICBDYXJvdXNlbC5WRVJTSU9OICA9ICczLjMuNidcblxuICBDYXJvdXNlbC5UUkFOU0lUSU9OX0RVUkFUSU9OID0gNjAwXG5cbiAgQ2Fyb3VzZWwuREVGQVVMVFMgPSB7XG4gICAgaW50ZXJ2YWw6IDUwMDAsXG4gICAgcGF1c2U6ICdob3ZlcicsXG4gICAgd3JhcDogdHJ1ZSxcbiAgICBrZXlib2FyZDogdHJ1ZVxuICB9XG5cbiAgQ2Fyb3VzZWwucHJvdG90eXBlLmtleWRvd24gPSBmdW5jdGlvbiAoZSkge1xuICAgIGlmICgvaW5wdXR8dGV4dGFyZWEvaS50ZXN0KGUudGFyZ2V0LnRhZ05hbWUpKSByZXR1cm5cbiAgICBzd2l0Y2ggKGUud2hpY2gpIHtcbiAgICAgIGNhc2UgMzc6IHRoaXMucHJldigpOyBicmVha1xuICAgICAgY2FzZSAzOTogdGhpcy5uZXh0KCk7IGJyZWFrXG4gICAgICBkZWZhdWx0OiByZXR1cm5cbiAgICB9XG5cbiAgICBlLnByZXZlbnREZWZhdWx0KClcbiAgfVxuXG4gIENhcm91c2VsLnByb3RvdHlwZS5jeWNsZSA9IGZ1bmN0aW9uIChlKSB7XG4gICAgZSB8fCAodGhpcy5wYXVzZWQgPSBmYWxzZSlcblxuICAgIHRoaXMuaW50ZXJ2YWwgJiYgY2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsKVxuXG4gICAgdGhpcy5vcHRpb25zLmludGVydmFsXG4gICAgICAmJiAhdGhpcy5wYXVzZWRcbiAgICAgICYmICh0aGlzLmludGVydmFsID0gc2V0SW50ZXJ2YWwoJC5wcm94eSh0aGlzLm5leHQsIHRoaXMpLCB0aGlzLm9wdGlvbnMuaW50ZXJ2YWwpKVxuXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gIENhcm91c2VsLnByb3RvdHlwZS5nZXRJdGVtSW5kZXggPSBmdW5jdGlvbiAoaXRlbSkge1xuICAgIHRoaXMuJGl0ZW1zID0gaXRlbS5wYXJlbnQoKS5jaGlsZHJlbignLml0ZW0nKVxuICAgIHJldHVybiB0aGlzLiRpdGVtcy5pbmRleChpdGVtIHx8IHRoaXMuJGFjdGl2ZSlcbiAgfVxuXG4gIENhcm91c2VsLnByb3RvdHlwZS5nZXRJdGVtRm9yRGlyZWN0aW9uID0gZnVuY3Rpb24gKGRpcmVjdGlvbiwgYWN0aXZlKSB7XG4gICAgdmFyIGFjdGl2ZUluZGV4ID0gdGhpcy5nZXRJdGVtSW5kZXgoYWN0aXZlKVxuICAgIHZhciB3aWxsV3JhcCA9IChkaXJlY3Rpb24gPT0gJ3ByZXYnICYmIGFjdGl2ZUluZGV4ID09PSAwKVxuICAgICAgICAgICAgICAgIHx8IChkaXJlY3Rpb24gPT0gJ25leHQnICYmIGFjdGl2ZUluZGV4ID09ICh0aGlzLiRpdGVtcy5sZW5ndGggLSAxKSlcbiAgICBpZiAod2lsbFdyYXAgJiYgIXRoaXMub3B0aW9ucy53cmFwKSByZXR1cm4gYWN0aXZlXG4gICAgdmFyIGRlbHRhID0gZGlyZWN0aW9uID09ICdwcmV2JyA/IC0xIDogMVxuICAgIHZhciBpdGVtSW5kZXggPSAoYWN0aXZlSW5kZXggKyBkZWx0YSkgJSB0aGlzLiRpdGVtcy5sZW5ndGhcbiAgICByZXR1cm4gdGhpcy4kaXRlbXMuZXEoaXRlbUluZGV4KVxuICB9XG5cbiAgQ2Fyb3VzZWwucHJvdG90eXBlLnRvID0gZnVuY3Rpb24gKHBvcykge1xuICAgIHZhciB0aGF0ICAgICAgICA9IHRoaXNcbiAgICB2YXIgYWN0aXZlSW5kZXggPSB0aGlzLmdldEl0ZW1JbmRleCh0aGlzLiRhY3RpdmUgPSB0aGlzLiRlbGVtZW50LmZpbmQoJy5pdGVtLmFjdGl2ZScpKVxuXG4gICAgaWYgKHBvcyA+ICh0aGlzLiRpdGVtcy5sZW5ndGggLSAxKSB8fCBwb3MgPCAwKSByZXR1cm5cblxuICAgIGlmICh0aGlzLnNsaWRpbmcpICAgICAgIHJldHVybiB0aGlzLiRlbGVtZW50Lm9uZSgnc2xpZC5icy5jYXJvdXNlbCcsIGZ1bmN0aW9uICgpIHsgdGhhdC50byhwb3MpIH0pIC8vIHllcywgXCJzbGlkXCJcbiAgICBpZiAoYWN0aXZlSW5kZXggPT0gcG9zKSByZXR1cm4gdGhpcy5wYXVzZSgpLmN5Y2xlKClcblxuICAgIHJldHVybiB0aGlzLnNsaWRlKHBvcyA+IGFjdGl2ZUluZGV4ID8gJ25leHQnIDogJ3ByZXYnLCB0aGlzLiRpdGVtcy5lcShwb3MpKVxuICB9XG5cbiAgQ2Fyb3VzZWwucHJvdG90eXBlLnBhdXNlID0gZnVuY3Rpb24gKGUpIHtcbiAgICBlIHx8ICh0aGlzLnBhdXNlZCA9IHRydWUpXG5cbiAgICBpZiAodGhpcy4kZWxlbWVudC5maW5kKCcubmV4dCwgLnByZXYnKS5sZW5ndGggJiYgJC5zdXBwb3J0LnRyYW5zaXRpb24pIHtcbiAgICAgIHRoaXMuJGVsZW1lbnQudHJpZ2dlcigkLnN1cHBvcnQudHJhbnNpdGlvbi5lbmQpXG4gICAgICB0aGlzLmN5Y2xlKHRydWUpXG4gICAgfVxuXG4gICAgdGhpcy5pbnRlcnZhbCA9IGNsZWFySW50ZXJ2YWwodGhpcy5pbnRlcnZhbClcblxuICAgIHJldHVybiB0aGlzXG4gIH1cblxuICBDYXJvdXNlbC5wcm90b3R5cGUubmV4dCA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAodGhpcy5zbGlkaW5nKSByZXR1cm5cbiAgICByZXR1cm4gdGhpcy5zbGlkZSgnbmV4dCcpXG4gIH1cblxuICBDYXJvdXNlbC5wcm90b3R5cGUucHJldiA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAodGhpcy5zbGlkaW5nKSByZXR1cm5cbiAgICByZXR1cm4gdGhpcy5zbGlkZSgncHJldicpXG4gIH1cblxuICBDYXJvdXNlbC5wcm90b3R5cGUuc2xpZGUgPSBmdW5jdGlvbiAodHlwZSwgbmV4dCkge1xuICAgIHZhciAkYWN0aXZlICAgPSB0aGlzLiRlbGVtZW50LmZpbmQoJy5pdGVtLmFjdGl2ZScpXG4gICAgdmFyICRuZXh0ICAgICA9IG5leHQgfHwgdGhpcy5nZXRJdGVtRm9yRGlyZWN0aW9uKHR5cGUsICRhY3RpdmUpXG4gICAgdmFyIGlzQ3ljbGluZyA9IHRoaXMuaW50ZXJ2YWxcbiAgICB2YXIgZGlyZWN0aW9uID0gdHlwZSA9PSAnbmV4dCcgPyAnbGVmdCcgOiAncmlnaHQnXG4gICAgdmFyIHRoYXQgICAgICA9IHRoaXNcblxuICAgIGlmICgkbmV4dC5oYXNDbGFzcygnYWN0aXZlJykpIHJldHVybiAodGhpcy5zbGlkaW5nID0gZmFsc2UpXG5cbiAgICB2YXIgcmVsYXRlZFRhcmdldCA9ICRuZXh0WzBdXG4gICAgdmFyIHNsaWRlRXZlbnQgPSAkLkV2ZW50KCdzbGlkZS5icy5jYXJvdXNlbCcsIHtcbiAgICAgIHJlbGF0ZWRUYXJnZXQ6IHJlbGF0ZWRUYXJnZXQsXG4gICAgICBkaXJlY3Rpb246IGRpcmVjdGlvblxuICAgIH0pXG4gICAgdGhpcy4kZWxlbWVudC50cmlnZ2VyKHNsaWRlRXZlbnQpXG4gICAgaWYgKHNsaWRlRXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHJldHVyblxuXG4gICAgdGhpcy5zbGlkaW5nID0gdHJ1ZVxuXG4gICAgaXNDeWNsaW5nICYmIHRoaXMucGF1c2UoKVxuXG4gICAgaWYgKHRoaXMuJGluZGljYXRvcnMubGVuZ3RoKSB7XG4gICAgICB0aGlzLiRpbmRpY2F0b3JzLmZpbmQoJy5hY3RpdmUnKS5yZW1vdmVDbGFzcygnYWN0aXZlJylcbiAgICAgIHZhciAkbmV4dEluZGljYXRvciA9ICQodGhpcy4kaW5kaWNhdG9ycy5jaGlsZHJlbigpW3RoaXMuZ2V0SXRlbUluZGV4KCRuZXh0KV0pXG4gICAgICAkbmV4dEluZGljYXRvciAmJiAkbmV4dEluZGljYXRvci5hZGRDbGFzcygnYWN0aXZlJylcbiAgICB9XG5cbiAgICB2YXIgc2xpZEV2ZW50ID0gJC5FdmVudCgnc2xpZC5icy5jYXJvdXNlbCcsIHsgcmVsYXRlZFRhcmdldDogcmVsYXRlZFRhcmdldCwgZGlyZWN0aW9uOiBkaXJlY3Rpb24gfSkgLy8geWVzLCBcInNsaWRcIlxuICAgIGlmICgkLnN1cHBvcnQudHJhbnNpdGlvbiAmJiB0aGlzLiRlbGVtZW50Lmhhc0NsYXNzKCdzbGlkZScpKSB7XG4gICAgICAkbmV4dC5hZGRDbGFzcyh0eXBlKVxuICAgICAgJG5leHRbMF0ub2Zmc2V0V2lkdGggLy8gZm9yY2UgcmVmbG93XG4gICAgICAkYWN0aXZlLmFkZENsYXNzKGRpcmVjdGlvbilcbiAgICAgICRuZXh0LmFkZENsYXNzKGRpcmVjdGlvbilcbiAgICAgICRhY3RpdmVcbiAgICAgICAgLm9uZSgnYnNUcmFuc2l0aW9uRW5kJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICRuZXh0LnJlbW92ZUNsYXNzKFt0eXBlLCBkaXJlY3Rpb25dLmpvaW4oJyAnKSkuYWRkQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgICAgJGFjdGl2ZS5yZW1vdmVDbGFzcyhbJ2FjdGl2ZScsIGRpcmVjdGlvbl0uam9pbignICcpKVxuICAgICAgICAgIHRoYXQuc2xpZGluZyA9IGZhbHNlXG4gICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB0aGF0LiRlbGVtZW50LnRyaWdnZXIoc2xpZEV2ZW50KVxuICAgICAgICAgIH0sIDApXG4gICAgICAgIH0pXG4gICAgICAgIC5lbXVsYXRlVHJhbnNpdGlvbkVuZChDYXJvdXNlbC5UUkFOU0lUSU9OX0RVUkFUSU9OKVxuICAgIH0gZWxzZSB7XG4gICAgICAkYWN0aXZlLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxuICAgICAgJG5leHQuYWRkQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICB0aGlzLnNsaWRpbmcgPSBmYWxzZVxuICAgICAgdGhpcy4kZWxlbWVudC50cmlnZ2VyKHNsaWRFdmVudClcbiAgICB9XG5cbiAgICBpc0N5Y2xpbmcgJiYgdGhpcy5jeWNsZSgpXG5cbiAgICByZXR1cm4gdGhpc1xuICB9XG5cblxuICAvLyBDQVJPVVNFTCBQTFVHSU4gREVGSU5JVElPTlxuICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gIGZ1bmN0aW9uIFBsdWdpbihvcHRpb24pIHtcbiAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciAkdGhpcyAgID0gJCh0aGlzKVxuICAgICAgdmFyIGRhdGEgICAgPSAkdGhpcy5kYXRhKCdicy5jYXJvdXNlbCcpXG4gICAgICB2YXIgb3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCBDYXJvdXNlbC5ERUZBVUxUUywgJHRoaXMuZGF0YSgpLCB0eXBlb2Ygb3B0aW9uID09ICdvYmplY3QnICYmIG9wdGlvbilcbiAgICAgIHZhciBhY3Rpb24gID0gdHlwZW9mIG9wdGlvbiA9PSAnc3RyaW5nJyA/IG9wdGlvbiA6IG9wdGlvbnMuc2xpZGVcblxuICAgICAgaWYgKCFkYXRhKSAkdGhpcy5kYXRhKCdicy5jYXJvdXNlbCcsIChkYXRhID0gbmV3IENhcm91c2VsKHRoaXMsIG9wdGlvbnMpKSlcbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9uID09ICdudW1iZXInKSBkYXRhLnRvKG9wdGlvbilcbiAgICAgIGVsc2UgaWYgKGFjdGlvbikgZGF0YVthY3Rpb25dKClcbiAgICAgIGVsc2UgaWYgKG9wdGlvbnMuaW50ZXJ2YWwpIGRhdGEucGF1c2UoKS5jeWNsZSgpXG4gICAgfSlcbiAgfVxuXG4gIHZhciBvbGQgPSAkLmZuLmNhcm91c2VsXG5cbiAgJC5mbi5jYXJvdXNlbCAgICAgICAgICAgICA9IFBsdWdpblxuICAkLmZuLmNhcm91c2VsLkNvbnN0cnVjdG9yID0gQ2Fyb3VzZWxcblxuXG4gIC8vIENBUk9VU0VMIE5PIENPTkZMSUNUXG4gIC8vID09PT09PT09PT09PT09PT09PT09XG5cbiAgJC5mbi5jYXJvdXNlbC5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xuICAgICQuZm4uY2Fyb3VzZWwgPSBvbGRcbiAgICByZXR1cm4gdGhpc1xuICB9XG5cblxuICAvLyBDQVJPVVNFTCBEQVRBLUFQSVxuICAvLyA9PT09PT09PT09PT09PT09PVxuXG4gIHZhciBjbGlja0hhbmRsZXIgPSBmdW5jdGlvbiAoZSkge1xuICAgIHZhciBocmVmXG4gICAgdmFyICR0aGlzICAgPSAkKHRoaXMpXG4gICAgdmFyICR0YXJnZXQgPSAkKCR0aGlzLmF0dHIoJ2RhdGEtdGFyZ2V0JykgfHwgKGhyZWYgPSAkdGhpcy5hdHRyKCdocmVmJykpICYmIGhyZWYucmVwbGFjZSgvLiooPz0jW15cXHNdKyQpLywgJycpKSAvLyBzdHJpcCBmb3IgaWU3XG4gICAgaWYgKCEkdGFyZ2V0Lmhhc0NsYXNzKCdjYXJvdXNlbCcpKSByZXR1cm5cbiAgICB2YXIgb3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCAkdGFyZ2V0LmRhdGEoKSwgJHRoaXMuZGF0YSgpKVxuICAgIHZhciBzbGlkZUluZGV4ID0gJHRoaXMuYXR0cignZGF0YS1zbGlkZS10bycpXG4gICAgaWYgKHNsaWRlSW5kZXgpIG9wdGlvbnMuaW50ZXJ2YWwgPSBmYWxzZVxuXG4gICAgUGx1Z2luLmNhbGwoJHRhcmdldCwgb3B0aW9ucylcblxuICAgIGlmIChzbGlkZUluZGV4KSB7XG4gICAgICAkdGFyZ2V0LmRhdGEoJ2JzLmNhcm91c2VsJykudG8oc2xpZGVJbmRleClcbiAgICB9XG5cbiAgICBlLnByZXZlbnREZWZhdWx0KClcbiAgfVxuXG4gICQoZG9jdW1lbnQpXG4gICAgLm9uKCdjbGljay5icy5jYXJvdXNlbC5kYXRhLWFwaScsICdbZGF0YS1zbGlkZV0nLCBjbGlja0hhbmRsZXIpXG4gICAgLm9uKCdjbGljay5icy5jYXJvdXNlbC5kYXRhLWFwaScsICdbZGF0YS1zbGlkZS10b10nLCBjbGlja0hhbmRsZXIpXG5cbiAgJCh3aW5kb3cpLm9uKCdsb2FkJywgZnVuY3Rpb24gKCkge1xuICAgICQoJ1tkYXRhLXJpZGU9XCJjYXJvdXNlbFwiXScpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgdmFyICRjYXJvdXNlbCA9ICQodGhpcylcbiAgICAgIFBsdWdpbi5jYWxsKCRjYXJvdXNlbCwgJGNhcm91c2VsLmRhdGEoKSlcbiAgICB9KVxuICB9KVxuXG59KGpRdWVyeSk7XG5cbi8qID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogQm9vdHN0cmFwOiBjb2xsYXBzZS5qcyB2My4zLjZcbiAqIGh0dHA6Ly9nZXRib290c3RyYXAuY29tL2phdmFzY3JpcHQvI2NvbGxhcHNlXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqIENvcHlyaWdodCAyMDExLTIwMTUgVHdpdHRlciwgSW5jLlxuICogTGljZW5zZWQgdW5kZXIgTUlUIChodHRwczovL2dpdGh1Yi5jb20vdHdicy9ib290c3RyYXAvYmxvYi9tYXN0ZXIvTElDRU5TRSlcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG5cbitmdW5jdGlvbiAoJCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgLy8gQ09MTEFQU0UgUFVCTElDIENMQVNTIERFRklOSVRJT05cbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICB2YXIgQ29sbGFwc2UgPSBmdW5jdGlvbiAoZWxlbWVudCwgb3B0aW9ucykge1xuICAgIHRoaXMuJGVsZW1lbnQgICAgICA9ICQoZWxlbWVudClcbiAgICB0aGlzLm9wdGlvbnMgICAgICAgPSAkLmV4dGVuZCh7fSwgQ29sbGFwc2UuREVGQVVMVFMsIG9wdGlvbnMpXG4gICAgdGhpcy4kdHJpZ2dlciAgICAgID0gJCgnW2RhdGEtdG9nZ2xlPVwiY29sbGFwc2VcIl1baHJlZj1cIiMnICsgZWxlbWVudC5pZCArICdcIl0sJyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAnW2RhdGEtdG9nZ2xlPVwiY29sbGFwc2VcIl1bZGF0YS10YXJnZXQ9XCIjJyArIGVsZW1lbnQuaWQgKyAnXCJdJylcbiAgICB0aGlzLnRyYW5zaXRpb25pbmcgPSBudWxsXG5cbiAgICBpZiAodGhpcy5vcHRpb25zLnBhcmVudCkge1xuICAgICAgdGhpcy4kcGFyZW50ID0gdGhpcy5nZXRQYXJlbnQoKVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmFkZEFyaWFBbmRDb2xsYXBzZWRDbGFzcyh0aGlzLiRlbGVtZW50LCB0aGlzLiR0cmlnZ2VyKVxuICAgIH1cblxuICAgIGlmICh0aGlzLm9wdGlvbnMudG9nZ2xlKSB0aGlzLnRvZ2dsZSgpXG4gIH1cblxuICBDb2xsYXBzZS5WRVJTSU9OICA9ICczLjMuNidcblxuICBDb2xsYXBzZS5UUkFOU0lUSU9OX0RVUkFUSU9OID0gMzUwXG5cbiAgQ29sbGFwc2UuREVGQVVMVFMgPSB7XG4gICAgdG9nZ2xlOiB0cnVlXG4gIH1cblxuICBDb2xsYXBzZS5wcm90b3R5cGUuZGltZW5zaW9uID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBoYXNXaWR0aCA9IHRoaXMuJGVsZW1lbnQuaGFzQ2xhc3MoJ3dpZHRoJylcbiAgICByZXR1cm4gaGFzV2lkdGggPyAnd2lkdGgnIDogJ2hlaWdodCdcbiAgfVxuXG4gIENvbGxhcHNlLnByb3RvdHlwZS5zaG93ID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICh0aGlzLnRyYW5zaXRpb25pbmcgfHwgdGhpcy4kZWxlbWVudC5oYXNDbGFzcygnaW4nKSkgcmV0dXJuXG5cbiAgICB2YXIgYWN0aXZlc0RhdGFcbiAgICB2YXIgYWN0aXZlcyA9IHRoaXMuJHBhcmVudCAmJiB0aGlzLiRwYXJlbnQuY2hpbGRyZW4oJy5wYW5lbCcpLmNoaWxkcmVuKCcuaW4sIC5jb2xsYXBzaW5nJylcblxuICAgIGlmIChhY3RpdmVzICYmIGFjdGl2ZXMubGVuZ3RoKSB7XG4gICAgICBhY3RpdmVzRGF0YSA9IGFjdGl2ZXMuZGF0YSgnYnMuY29sbGFwc2UnKVxuICAgICAgaWYgKGFjdGl2ZXNEYXRhICYmIGFjdGl2ZXNEYXRhLnRyYW5zaXRpb25pbmcpIHJldHVyblxuICAgIH1cblxuICAgIHZhciBzdGFydEV2ZW50ID0gJC5FdmVudCgnc2hvdy5icy5jb2xsYXBzZScpXG4gICAgdGhpcy4kZWxlbWVudC50cmlnZ2VyKHN0YXJ0RXZlbnQpXG4gICAgaWYgKHN0YXJ0RXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHJldHVyblxuXG4gICAgaWYgKGFjdGl2ZXMgJiYgYWN0aXZlcy5sZW5ndGgpIHtcbiAgICAgIFBsdWdpbi5jYWxsKGFjdGl2ZXMsICdoaWRlJylcbiAgICAgIGFjdGl2ZXNEYXRhIHx8IGFjdGl2ZXMuZGF0YSgnYnMuY29sbGFwc2UnLCBudWxsKVxuICAgIH1cblxuICAgIHZhciBkaW1lbnNpb24gPSB0aGlzLmRpbWVuc2lvbigpXG5cbiAgICB0aGlzLiRlbGVtZW50XG4gICAgICAucmVtb3ZlQ2xhc3MoJ2NvbGxhcHNlJylcbiAgICAgIC5hZGRDbGFzcygnY29sbGFwc2luZycpW2RpbWVuc2lvbl0oMClcbiAgICAgIC5hdHRyKCdhcmlhLWV4cGFuZGVkJywgdHJ1ZSlcblxuICAgIHRoaXMuJHRyaWdnZXJcbiAgICAgIC5yZW1vdmVDbGFzcygnY29sbGFwc2VkJylcbiAgICAgIC5hdHRyKCdhcmlhLWV4cGFuZGVkJywgdHJ1ZSlcblxuICAgIHRoaXMudHJhbnNpdGlvbmluZyA9IDFcblxuICAgIHZhciBjb21wbGV0ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMuJGVsZW1lbnRcbiAgICAgICAgLnJlbW92ZUNsYXNzKCdjb2xsYXBzaW5nJylcbiAgICAgICAgLmFkZENsYXNzKCdjb2xsYXBzZSBpbicpW2RpbWVuc2lvbl0oJycpXG4gICAgICB0aGlzLnRyYW5zaXRpb25pbmcgPSAwXG4gICAgICB0aGlzLiRlbGVtZW50XG4gICAgICAgIC50cmlnZ2VyKCdzaG93bi5icy5jb2xsYXBzZScpXG4gICAgfVxuXG4gICAgaWYgKCEkLnN1cHBvcnQudHJhbnNpdGlvbikgcmV0dXJuIGNvbXBsZXRlLmNhbGwodGhpcylcblxuICAgIHZhciBzY3JvbGxTaXplID0gJC5jYW1lbENhc2UoWydzY3JvbGwnLCBkaW1lbnNpb25dLmpvaW4oJy0nKSlcblxuICAgIHRoaXMuJGVsZW1lbnRcbiAgICAgIC5vbmUoJ2JzVHJhbnNpdGlvbkVuZCcsICQucHJveHkoY29tcGxldGUsIHRoaXMpKVxuICAgICAgLmVtdWxhdGVUcmFuc2l0aW9uRW5kKENvbGxhcHNlLlRSQU5TSVRJT05fRFVSQVRJT04pW2RpbWVuc2lvbl0odGhpcy4kZWxlbWVudFswXVtzY3JvbGxTaXplXSlcbiAgfVxuXG4gIENvbGxhcHNlLnByb3RvdHlwZS5oaWRlID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICh0aGlzLnRyYW5zaXRpb25pbmcgfHwgIXRoaXMuJGVsZW1lbnQuaGFzQ2xhc3MoJ2luJykpIHJldHVyblxuXG4gICAgdmFyIHN0YXJ0RXZlbnQgPSAkLkV2ZW50KCdoaWRlLmJzLmNvbGxhcHNlJylcbiAgICB0aGlzLiRlbGVtZW50LnRyaWdnZXIoc3RhcnRFdmVudClcbiAgICBpZiAoc3RhcnRFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkgcmV0dXJuXG5cbiAgICB2YXIgZGltZW5zaW9uID0gdGhpcy5kaW1lbnNpb24oKVxuXG4gICAgdGhpcy4kZWxlbWVudFtkaW1lbnNpb25dKHRoaXMuJGVsZW1lbnRbZGltZW5zaW9uXSgpKVswXS5vZmZzZXRIZWlnaHRcblxuICAgIHRoaXMuJGVsZW1lbnRcbiAgICAgIC5hZGRDbGFzcygnY29sbGFwc2luZycpXG4gICAgICAucmVtb3ZlQ2xhc3MoJ2NvbGxhcHNlIGluJylcbiAgICAgIC5hdHRyKCdhcmlhLWV4cGFuZGVkJywgZmFsc2UpXG5cbiAgICB0aGlzLiR0cmlnZ2VyXG4gICAgICAuYWRkQ2xhc3MoJ2NvbGxhcHNlZCcpXG4gICAgICAuYXR0cignYXJpYS1leHBhbmRlZCcsIGZhbHNlKVxuXG4gICAgdGhpcy50cmFuc2l0aW9uaW5nID0gMVxuXG4gICAgdmFyIGNvbXBsZXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgdGhpcy50cmFuc2l0aW9uaW5nID0gMFxuICAgICAgdGhpcy4kZWxlbWVudFxuICAgICAgICAucmVtb3ZlQ2xhc3MoJ2NvbGxhcHNpbmcnKVxuICAgICAgICAuYWRkQ2xhc3MoJ2NvbGxhcHNlJylcbiAgICAgICAgLnRyaWdnZXIoJ2hpZGRlbi5icy5jb2xsYXBzZScpXG4gICAgfVxuXG4gICAgaWYgKCEkLnN1cHBvcnQudHJhbnNpdGlvbikgcmV0dXJuIGNvbXBsZXRlLmNhbGwodGhpcylcblxuICAgIHRoaXMuJGVsZW1lbnRcbiAgICAgIFtkaW1lbnNpb25dKDApXG4gICAgICAub25lKCdic1RyYW5zaXRpb25FbmQnLCAkLnByb3h5KGNvbXBsZXRlLCB0aGlzKSlcbiAgICAgIC5lbXVsYXRlVHJhbnNpdGlvbkVuZChDb2xsYXBzZS5UUkFOU0lUSU9OX0RVUkFUSU9OKVxuICB9XG5cbiAgQ29sbGFwc2UucHJvdG90eXBlLnRvZ2dsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzW3RoaXMuJGVsZW1lbnQuaGFzQ2xhc3MoJ2luJykgPyAnaGlkZScgOiAnc2hvdyddKClcbiAgfVxuXG4gIENvbGxhcHNlLnByb3RvdHlwZS5nZXRQYXJlbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuICQodGhpcy5vcHRpb25zLnBhcmVudClcbiAgICAgIC5maW5kKCdbZGF0YS10b2dnbGU9XCJjb2xsYXBzZVwiXVtkYXRhLXBhcmVudD1cIicgKyB0aGlzLm9wdGlvbnMucGFyZW50ICsgJ1wiXScpXG4gICAgICAuZWFjaCgkLnByb3h5KGZ1bmN0aW9uIChpLCBlbGVtZW50KSB7XG4gICAgICAgIHZhciAkZWxlbWVudCA9ICQoZWxlbWVudClcbiAgICAgICAgdGhpcy5hZGRBcmlhQW5kQ29sbGFwc2VkQ2xhc3MoZ2V0VGFyZ2V0RnJvbVRyaWdnZXIoJGVsZW1lbnQpLCAkZWxlbWVudClcbiAgICAgIH0sIHRoaXMpKVxuICAgICAgLmVuZCgpXG4gIH1cblxuICBDb2xsYXBzZS5wcm90b3R5cGUuYWRkQXJpYUFuZENvbGxhcHNlZENsYXNzID0gZnVuY3Rpb24gKCRlbGVtZW50LCAkdHJpZ2dlcikge1xuICAgIHZhciBpc09wZW4gPSAkZWxlbWVudC5oYXNDbGFzcygnaW4nKVxuXG4gICAgJGVsZW1lbnQuYXR0cignYXJpYS1leHBhbmRlZCcsIGlzT3BlbilcbiAgICAkdHJpZ2dlclxuICAgICAgLnRvZ2dsZUNsYXNzKCdjb2xsYXBzZWQnLCAhaXNPcGVuKVxuICAgICAgLmF0dHIoJ2FyaWEtZXhwYW5kZWQnLCBpc09wZW4pXG4gIH1cblxuICBmdW5jdGlvbiBnZXRUYXJnZXRGcm9tVHJpZ2dlcigkdHJpZ2dlcikge1xuICAgIHZhciBocmVmXG4gICAgdmFyIHRhcmdldCA9ICR0cmlnZ2VyLmF0dHIoJ2RhdGEtdGFyZ2V0JylcbiAgICAgIHx8IChocmVmID0gJHRyaWdnZXIuYXR0cignaHJlZicpKSAmJiBocmVmLnJlcGxhY2UoLy4qKD89I1teXFxzXSskKS8sICcnKSAvLyBzdHJpcCBmb3IgaWU3XG5cbiAgICByZXR1cm4gJCh0YXJnZXQpXG4gIH1cblxuXG4gIC8vIENPTExBUFNFIFBMVUdJTiBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgZnVuY3Rpb24gUGx1Z2luKG9wdGlvbikge1xuICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgdmFyICR0aGlzICAgPSAkKHRoaXMpXG4gICAgICB2YXIgZGF0YSAgICA9ICR0aGlzLmRhdGEoJ2JzLmNvbGxhcHNlJylcbiAgICAgIHZhciBvcHRpb25zID0gJC5leHRlbmQoe30sIENvbGxhcHNlLkRFRkFVTFRTLCAkdGhpcy5kYXRhKCksIHR5cGVvZiBvcHRpb24gPT0gJ29iamVjdCcgJiYgb3B0aW9uKVxuXG4gICAgICBpZiAoIWRhdGEgJiYgb3B0aW9ucy50b2dnbGUgJiYgL3Nob3d8aGlkZS8udGVzdChvcHRpb24pKSBvcHRpb25zLnRvZ2dsZSA9IGZhbHNlXG4gICAgICBpZiAoIWRhdGEpICR0aGlzLmRhdGEoJ2JzLmNvbGxhcHNlJywgKGRhdGEgPSBuZXcgQ29sbGFwc2UodGhpcywgb3B0aW9ucykpKVxuICAgICAgaWYgKHR5cGVvZiBvcHRpb24gPT0gJ3N0cmluZycpIGRhdGFbb3B0aW9uXSgpXG4gICAgfSlcbiAgfVxuXG4gIHZhciBvbGQgPSAkLmZuLmNvbGxhcHNlXG5cbiAgJC5mbi5jb2xsYXBzZSAgICAgICAgICAgICA9IFBsdWdpblxuICAkLmZuLmNvbGxhcHNlLkNvbnN0cnVjdG9yID0gQ29sbGFwc2VcblxuXG4gIC8vIENPTExBUFNFIE5PIENPTkZMSUNUXG4gIC8vID09PT09PT09PT09PT09PT09PT09XG5cbiAgJC5mbi5jb2xsYXBzZS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xuICAgICQuZm4uY29sbGFwc2UgPSBvbGRcbiAgICByZXR1cm4gdGhpc1xuICB9XG5cblxuICAvLyBDT0xMQVBTRSBEQVRBLUFQSVxuICAvLyA9PT09PT09PT09PT09PT09PVxuXG4gICQoZG9jdW1lbnQpLm9uKCdjbGljay5icy5jb2xsYXBzZS5kYXRhLWFwaScsICdbZGF0YS10b2dnbGU9XCJjb2xsYXBzZVwiXScsIGZ1bmN0aW9uIChlKSB7XG4gICAgdmFyICR0aGlzICAgPSAkKHRoaXMpXG5cbiAgICBpZiAoISR0aGlzLmF0dHIoJ2RhdGEtdGFyZ2V0JykpIGUucHJldmVudERlZmF1bHQoKVxuXG4gICAgdmFyICR0YXJnZXQgPSBnZXRUYXJnZXRGcm9tVHJpZ2dlcigkdGhpcylcbiAgICB2YXIgZGF0YSAgICA9ICR0YXJnZXQuZGF0YSgnYnMuY29sbGFwc2UnKVxuICAgIHZhciBvcHRpb24gID0gZGF0YSA/ICd0b2dnbGUnIDogJHRoaXMuZGF0YSgpXG5cbiAgICBQbHVnaW4uY2FsbCgkdGFyZ2V0LCBvcHRpb24pXG4gIH0pXG5cbn0oalF1ZXJ5KTtcblxuLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBCb290c3RyYXA6IGRyb3Bkb3duLmpzIHYzLjMuNlxuICogaHR0cDovL2dldGJvb3RzdHJhcC5jb20vamF2YXNjcmlwdC8jZHJvcGRvd25zXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqIENvcHlyaWdodCAyMDExLTIwMTUgVHdpdHRlciwgSW5jLlxuICogTGljZW5zZWQgdW5kZXIgTUlUIChodHRwczovL2dpdGh1Yi5jb20vdHdicy9ib290c3RyYXAvYmxvYi9tYXN0ZXIvTElDRU5TRSlcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG5cbitmdW5jdGlvbiAoJCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgLy8gRFJPUERPV04gQ0xBU1MgREVGSU5JVElPTlxuICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgdmFyIGJhY2tkcm9wID0gJy5kcm9wZG93bi1iYWNrZHJvcCdcbiAgdmFyIHRvZ2dsZSAgID0gJ1tkYXRhLXRvZ2dsZT1cImRyb3Bkb3duXCJdJ1xuICB2YXIgRHJvcGRvd24gPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICQoZWxlbWVudCkub24oJ2NsaWNrLmJzLmRyb3Bkb3duJywgdGhpcy50b2dnbGUpXG4gIH1cblxuICBEcm9wZG93bi5WRVJTSU9OID0gJzMuMy42J1xuXG4gIGZ1bmN0aW9uIGdldFBhcmVudCgkdGhpcykge1xuICAgIHZhciBzZWxlY3RvciA9ICR0aGlzLmF0dHIoJ2RhdGEtdGFyZ2V0JylcblxuICAgIGlmICghc2VsZWN0b3IpIHtcbiAgICAgIHNlbGVjdG9yID0gJHRoaXMuYXR0cignaHJlZicpXG4gICAgICBzZWxlY3RvciA9IHNlbGVjdG9yICYmIC8jW0EtWmEtel0vLnRlc3Qoc2VsZWN0b3IpICYmIHNlbGVjdG9yLnJlcGxhY2UoLy4qKD89I1teXFxzXSokKS8sICcnKSAvLyBzdHJpcCBmb3IgaWU3XG4gICAgfVxuXG4gICAgdmFyICRwYXJlbnQgPSBzZWxlY3RvciAmJiAkKHNlbGVjdG9yKVxuXG4gICAgcmV0dXJuICRwYXJlbnQgJiYgJHBhcmVudC5sZW5ndGggPyAkcGFyZW50IDogJHRoaXMucGFyZW50KClcbiAgfVxuXG4gIGZ1bmN0aW9uIGNsZWFyTWVudXMoZSkge1xuICAgIGlmIChlICYmIGUud2hpY2ggPT09IDMpIHJldHVyblxuICAgICQoYmFja2Ryb3ApLnJlbW92ZSgpXG4gICAgJCh0b2dnbGUpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgdmFyICR0aGlzICAgICAgICAgPSAkKHRoaXMpXG4gICAgICB2YXIgJHBhcmVudCAgICAgICA9IGdldFBhcmVudCgkdGhpcylcbiAgICAgIHZhciByZWxhdGVkVGFyZ2V0ID0geyByZWxhdGVkVGFyZ2V0OiB0aGlzIH1cblxuICAgICAgaWYgKCEkcGFyZW50Lmhhc0NsYXNzKCdvcGVuJykpIHJldHVyblxuXG4gICAgICBpZiAoZSAmJiBlLnR5cGUgPT0gJ2NsaWNrJyAmJiAvaW5wdXR8dGV4dGFyZWEvaS50ZXN0KGUudGFyZ2V0LnRhZ05hbWUpICYmICQuY29udGFpbnMoJHBhcmVudFswXSwgZS50YXJnZXQpKSByZXR1cm5cblxuICAgICAgJHBhcmVudC50cmlnZ2VyKGUgPSAkLkV2ZW50KCdoaWRlLmJzLmRyb3Bkb3duJywgcmVsYXRlZFRhcmdldCkpXG5cbiAgICAgIGlmIChlLmlzRGVmYXVsdFByZXZlbnRlZCgpKSByZXR1cm5cblxuICAgICAgJHRoaXMuYXR0cignYXJpYS1leHBhbmRlZCcsICdmYWxzZScpXG4gICAgICAkcGFyZW50LnJlbW92ZUNsYXNzKCdvcGVuJykudHJpZ2dlcigkLkV2ZW50KCdoaWRkZW4uYnMuZHJvcGRvd24nLCByZWxhdGVkVGFyZ2V0KSlcbiAgICB9KVxuICB9XG5cbiAgRHJvcGRvd24ucHJvdG90eXBlLnRvZ2dsZSA9IGZ1bmN0aW9uIChlKSB7XG4gICAgdmFyICR0aGlzID0gJCh0aGlzKVxuXG4gICAgaWYgKCR0aGlzLmlzKCcuZGlzYWJsZWQsIDpkaXNhYmxlZCcpKSByZXR1cm5cblxuICAgIHZhciAkcGFyZW50ICA9IGdldFBhcmVudCgkdGhpcylcbiAgICB2YXIgaXNBY3RpdmUgPSAkcGFyZW50Lmhhc0NsYXNzKCdvcGVuJylcblxuICAgIGNsZWFyTWVudXMoKVxuXG4gICAgaWYgKCFpc0FjdGl2ZSkge1xuICAgICAgaWYgKCdvbnRvdWNoc3RhcnQnIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCAmJiAhJHBhcmVudC5jbG9zZXN0KCcubmF2YmFyLW5hdicpLmxlbmd0aCkge1xuICAgICAgICAvLyBpZiBtb2JpbGUgd2UgdXNlIGEgYmFja2Ryb3AgYmVjYXVzZSBjbGljayBldmVudHMgZG9uJ3QgZGVsZWdhdGVcbiAgICAgICAgJChkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKSlcbiAgICAgICAgICAuYWRkQ2xhc3MoJ2Ryb3Bkb3duLWJhY2tkcm9wJylcbiAgICAgICAgICAuaW5zZXJ0QWZ0ZXIoJCh0aGlzKSlcbiAgICAgICAgICAub24oJ2NsaWNrJywgY2xlYXJNZW51cylcbiAgICAgIH1cblxuICAgICAgdmFyIHJlbGF0ZWRUYXJnZXQgPSB7IHJlbGF0ZWRUYXJnZXQ6IHRoaXMgfVxuICAgICAgJHBhcmVudC50cmlnZ2VyKGUgPSAkLkV2ZW50KCdzaG93LmJzLmRyb3Bkb3duJywgcmVsYXRlZFRhcmdldCkpXG5cbiAgICAgIGlmIChlLmlzRGVmYXVsdFByZXZlbnRlZCgpKSByZXR1cm5cblxuICAgICAgJHRoaXNcbiAgICAgICAgLnRyaWdnZXIoJ2ZvY3VzJylcbiAgICAgICAgLmF0dHIoJ2FyaWEtZXhwYW5kZWQnLCAndHJ1ZScpXG5cbiAgICAgICRwYXJlbnRcbiAgICAgICAgLnRvZ2dsZUNsYXNzKCdvcGVuJylcbiAgICAgICAgLnRyaWdnZXIoJC5FdmVudCgnc2hvd24uYnMuZHJvcGRvd24nLCByZWxhdGVkVGFyZ2V0KSlcbiAgICB9XG5cbiAgICByZXR1cm4gZmFsc2VcbiAgfVxuXG4gIERyb3Bkb3duLnByb3RvdHlwZS5rZXlkb3duID0gZnVuY3Rpb24gKGUpIHtcbiAgICBpZiAoIS8oMzh8NDB8Mjd8MzIpLy50ZXN0KGUud2hpY2gpIHx8IC9pbnB1dHx0ZXh0YXJlYS9pLnRlc3QoZS50YXJnZXQudGFnTmFtZSkpIHJldHVyblxuXG4gICAgdmFyICR0aGlzID0gJCh0aGlzKVxuXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuXG4gICAgaWYgKCR0aGlzLmlzKCcuZGlzYWJsZWQsIDpkaXNhYmxlZCcpKSByZXR1cm5cblxuICAgIHZhciAkcGFyZW50ICA9IGdldFBhcmVudCgkdGhpcylcbiAgICB2YXIgaXNBY3RpdmUgPSAkcGFyZW50Lmhhc0NsYXNzKCdvcGVuJylcblxuICAgIGlmICghaXNBY3RpdmUgJiYgZS53aGljaCAhPSAyNyB8fCBpc0FjdGl2ZSAmJiBlLndoaWNoID09IDI3KSB7XG4gICAgICBpZiAoZS53aGljaCA9PSAyNykgJHBhcmVudC5maW5kKHRvZ2dsZSkudHJpZ2dlcignZm9jdXMnKVxuICAgICAgcmV0dXJuICR0aGlzLnRyaWdnZXIoJ2NsaWNrJylcbiAgICB9XG5cbiAgICB2YXIgZGVzYyA9ICcgbGk6bm90KC5kaXNhYmxlZCk6dmlzaWJsZSBhJ1xuICAgIHZhciAkaXRlbXMgPSAkcGFyZW50LmZpbmQoJy5kcm9wZG93bi1tZW51JyArIGRlc2MpXG5cbiAgICBpZiAoISRpdGVtcy5sZW5ndGgpIHJldHVyblxuXG4gICAgdmFyIGluZGV4ID0gJGl0ZW1zLmluZGV4KGUudGFyZ2V0KVxuXG4gICAgaWYgKGUud2hpY2ggPT0gMzggJiYgaW5kZXggPiAwKSAgICAgICAgICAgICAgICAgaW5kZXgtLSAgICAgICAgIC8vIHVwXG4gICAgaWYgKGUud2hpY2ggPT0gNDAgJiYgaW5kZXggPCAkaXRlbXMubGVuZ3RoIC0gMSkgaW5kZXgrKyAgICAgICAgIC8vIGRvd25cbiAgICBpZiAoIX5pbmRleCkgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmRleCA9IDBcblxuICAgICRpdGVtcy5lcShpbmRleCkudHJpZ2dlcignZm9jdXMnKVxuICB9XG5cblxuICAvLyBEUk9QRE9XTiBQTFVHSU4gREVGSU5JVElPTlxuICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gIGZ1bmN0aW9uIFBsdWdpbihvcHRpb24pIHtcbiAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciAkdGhpcyA9ICQodGhpcylcbiAgICAgIHZhciBkYXRhICA9ICR0aGlzLmRhdGEoJ2JzLmRyb3Bkb3duJylcblxuICAgICAgaWYgKCFkYXRhKSAkdGhpcy5kYXRhKCdicy5kcm9wZG93bicsIChkYXRhID0gbmV3IERyb3Bkb3duKHRoaXMpKSlcbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9uID09ICdzdHJpbmcnKSBkYXRhW29wdGlvbl0uY2FsbCgkdGhpcylcbiAgICB9KVxuICB9XG5cbiAgdmFyIG9sZCA9ICQuZm4uZHJvcGRvd25cblxuICAkLmZuLmRyb3Bkb3duICAgICAgICAgICAgID0gUGx1Z2luXG4gICQuZm4uZHJvcGRvd24uQ29uc3RydWN0b3IgPSBEcm9wZG93blxuXG5cbiAgLy8gRFJPUERPV04gTk8gQ09ORkxJQ1RcbiAgLy8gPT09PT09PT09PT09PT09PT09PT1cblxuICAkLmZuLmRyb3Bkb3duLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XG4gICAgJC5mbi5kcm9wZG93biA9IG9sZFxuICAgIHJldHVybiB0aGlzXG4gIH1cblxuXG4gIC8vIEFQUExZIFRPIFNUQU5EQVJEIERST1BET1dOIEVMRU1FTlRTXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgJChkb2N1bWVudClcbiAgICAub24oJ2NsaWNrLmJzLmRyb3Bkb3duLmRhdGEtYXBpJywgY2xlYXJNZW51cylcbiAgICAub24oJ2NsaWNrLmJzLmRyb3Bkb3duLmRhdGEtYXBpJywgJy5kcm9wZG93biBmb3JtJywgZnVuY3Rpb24gKGUpIHsgZS5zdG9wUHJvcGFnYXRpb24oKSB9KVxuICAgIC5vbignY2xpY2suYnMuZHJvcGRvd24uZGF0YS1hcGknLCB0b2dnbGUsIERyb3Bkb3duLnByb3RvdHlwZS50b2dnbGUpXG4gICAgLm9uKCdrZXlkb3duLmJzLmRyb3Bkb3duLmRhdGEtYXBpJywgdG9nZ2xlLCBEcm9wZG93bi5wcm90b3R5cGUua2V5ZG93bilcbiAgICAub24oJ2tleWRvd24uYnMuZHJvcGRvd24uZGF0YS1hcGknLCAnLmRyb3Bkb3duLW1lbnUnLCBEcm9wZG93bi5wcm90b3R5cGUua2V5ZG93bilcblxufShqUXVlcnkpO1xuXG4vKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqIEJvb3RzdHJhcDogbW9kYWwuanMgdjMuMy42XG4gKiBodHRwOi8vZ2V0Ym9vdHN0cmFwLmNvbS9qYXZhc2NyaXB0LyNtb2RhbHNcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogQ29weXJpZ2h0IDIwMTEtMjAxNSBUd2l0dGVyLCBJbmMuXG4gKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cblxuK2Z1bmN0aW9uICgkKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICAvLyBNT0RBTCBDTEFTUyBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT1cblxuICB2YXIgTW9kYWwgPSBmdW5jdGlvbiAoZWxlbWVudCwgb3B0aW9ucykge1xuICAgIHRoaXMub3B0aW9ucyAgICAgICAgICAgICA9IG9wdGlvbnNcbiAgICB0aGlzLiRib2R5ICAgICAgICAgICAgICAgPSAkKGRvY3VtZW50LmJvZHkpXG4gICAgdGhpcy4kZWxlbWVudCAgICAgICAgICAgID0gJChlbGVtZW50KVxuICAgIHRoaXMuJGRpYWxvZyAgICAgICAgICAgICA9IHRoaXMuJGVsZW1lbnQuZmluZCgnLm1vZGFsLWRpYWxvZycpXG4gICAgdGhpcy4kYmFja2Ryb3AgICAgICAgICAgID0gbnVsbFxuICAgIHRoaXMuaXNTaG93biAgICAgICAgICAgICA9IG51bGxcbiAgICB0aGlzLm9yaWdpbmFsQm9keVBhZCAgICAgPSBudWxsXG4gICAgdGhpcy5zY3JvbGxiYXJXaWR0aCAgICAgID0gMFxuICAgIHRoaXMuaWdub3JlQmFja2Ryb3BDbGljayA9IGZhbHNlXG5cbiAgICBpZiAodGhpcy5vcHRpb25zLnJlbW90ZSkge1xuICAgICAgdGhpcy4kZWxlbWVudFxuICAgICAgICAuZmluZCgnLm1vZGFsLWNvbnRlbnQnKVxuICAgICAgICAubG9hZCh0aGlzLm9wdGlvbnMucmVtb3RlLCAkLnByb3h5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLiRlbGVtZW50LnRyaWdnZXIoJ2xvYWRlZC5icy5tb2RhbCcpXG4gICAgICAgIH0sIHRoaXMpKVxuICAgIH1cbiAgfVxuXG4gIE1vZGFsLlZFUlNJT04gID0gJzMuMy42J1xuXG4gIE1vZGFsLlRSQU5TSVRJT05fRFVSQVRJT04gPSAzMDBcbiAgTW9kYWwuQkFDS0RST1BfVFJBTlNJVElPTl9EVVJBVElPTiA9IDE1MFxuXG4gIE1vZGFsLkRFRkFVTFRTID0ge1xuICAgIGJhY2tkcm9wOiB0cnVlLFxuICAgIGtleWJvYXJkOiB0cnVlLFxuICAgIHNob3c6IHRydWVcbiAgfVxuXG4gIE1vZGFsLnByb3RvdHlwZS50b2dnbGUgPSBmdW5jdGlvbiAoX3JlbGF0ZWRUYXJnZXQpIHtcbiAgICByZXR1cm4gdGhpcy5pc1Nob3duID8gdGhpcy5oaWRlKCkgOiB0aGlzLnNob3coX3JlbGF0ZWRUYXJnZXQpXG4gIH1cblxuICBNb2RhbC5wcm90b3R5cGUuc2hvdyA9IGZ1bmN0aW9uIChfcmVsYXRlZFRhcmdldCkge1xuICAgIHZhciB0aGF0ID0gdGhpc1xuICAgIHZhciBlICAgID0gJC5FdmVudCgnc2hvdy5icy5tb2RhbCcsIHsgcmVsYXRlZFRhcmdldDogX3JlbGF0ZWRUYXJnZXQgfSlcblxuICAgIHRoaXMuJGVsZW1lbnQudHJpZ2dlcihlKVxuXG4gICAgaWYgKHRoaXMuaXNTaG93biB8fCBlLmlzRGVmYXVsdFByZXZlbnRlZCgpKSByZXR1cm5cblxuICAgIHRoaXMuaXNTaG93biA9IHRydWVcblxuICAgIHRoaXMuY2hlY2tTY3JvbGxiYXIoKVxuICAgIHRoaXMuc2V0U2Nyb2xsYmFyKClcbiAgICB0aGlzLiRib2R5LmFkZENsYXNzKCdtb2RhbC1vcGVuJylcblxuICAgIHRoaXMuZXNjYXBlKClcbiAgICB0aGlzLnJlc2l6ZSgpXG5cbiAgICB0aGlzLiRlbGVtZW50Lm9uKCdjbGljay5kaXNtaXNzLmJzLm1vZGFsJywgJ1tkYXRhLWRpc21pc3M9XCJtb2RhbFwiXScsICQucHJveHkodGhpcy5oaWRlLCB0aGlzKSlcblxuICAgIHRoaXMuJGRpYWxvZy5vbignbW91c2Vkb3duLmRpc21pc3MuYnMubW9kYWwnLCBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGF0LiRlbGVtZW50Lm9uZSgnbW91c2V1cC5kaXNtaXNzLmJzLm1vZGFsJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgaWYgKCQoZS50YXJnZXQpLmlzKHRoYXQuJGVsZW1lbnQpKSB0aGF0Lmlnbm9yZUJhY2tkcm9wQ2xpY2sgPSB0cnVlXG4gICAgICB9KVxuICAgIH0pXG5cbiAgICB0aGlzLmJhY2tkcm9wKGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciB0cmFuc2l0aW9uID0gJC5zdXBwb3J0LnRyYW5zaXRpb24gJiYgdGhhdC4kZWxlbWVudC5oYXNDbGFzcygnZmFkZScpXG5cbiAgICAgIGlmICghdGhhdC4kZWxlbWVudC5wYXJlbnQoKS5sZW5ndGgpIHtcbiAgICAgICAgdGhhdC4kZWxlbWVudC5hcHBlbmRUbyh0aGF0LiRib2R5KSAvLyBkb24ndCBtb3ZlIG1vZGFscyBkb20gcG9zaXRpb25cbiAgICAgIH1cblxuICAgICAgdGhhdC4kZWxlbWVudFxuICAgICAgICAuc2hvdygpXG4gICAgICAgIC5zY3JvbGxUb3AoMClcblxuICAgICAgdGhhdC5hZGp1c3REaWFsb2coKVxuXG4gICAgICBpZiAodHJhbnNpdGlvbikge1xuICAgICAgICB0aGF0LiRlbGVtZW50WzBdLm9mZnNldFdpZHRoIC8vIGZvcmNlIHJlZmxvd1xuICAgICAgfVxuXG4gICAgICB0aGF0LiRlbGVtZW50LmFkZENsYXNzKCdpbicpXG5cbiAgICAgIHRoYXQuZW5mb3JjZUZvY3VzKClcblxuICAgICAgdmFyIGUgPSAkLkV2ZW50KCdzaG93bi5icy5tb2RhbCcsIHsgcmVsYXRlZFRhcmdldDogX3JlbGF0ZWRUYXJnZXQgfSlcblxuICAgICAgdHJhbnNpdGlvbiA/XG4gICAgICAgIHRoYXQuJGRpYWxvZyAvLyB3YWl0IGZvciBtb2RhbCB0byBzbGlkZSBpblxuICAgICAgICAgIC5vbmUoJ2JzVHJhbnNpdGlvbkVuZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoYXQuJGVsZW1lbnQudHJpZ2dlcignZm9jdXMnKS50cmlnZ2VyKGUpXG4gICAgICAgICAgfSlcbiAgICAgICAgICAuZW11bGF0ZVRyYW5zaXRpb25FbmQoTW9kYWwuVFJBTlNJVElPTl9EVVJBVElPTikgOlxuICAgICAgICB0aGF0LiRlbGVtZW50LnRyaWdnZXIoJ2ZvY3VzJykudHJpZ2dlcihlKVxuICAgIH0pXG4gIH1cblxuICBNb2RhbC5wcm90b3R5cGUuaGlkZSA9IGZ1bmN0aW9uIChlKSB7XG4gICAgaWYgKGUpIGUucHJldmVudERlZmF1bHQoKVxuXG4gICAgZSA9ICQuRXZlbnQoJ2hpZGUuYnMubW9kYWwnKVxuXG4gICAgdGhpcy4kZWxlbWVudC50cmlnZ2VyKGUpXG5cbiAgICBpZiAoIXRoaXMuaXNTaG93biB8fCBlLmlzRGVmYXVsdFByZXZlbnRlZCgpKSByZXR1cm5cblxuICAgIHRoaXMuaXNTaG93biA9IGZhbHNlXG5cbiAgICB0aGlzLmVzY2FwZSgpXG4gICAgdGhpcy5yZXNpemUoKVxuXG4gICAgJChkb2N1bWVudCkub2ZmKCdmb2N1c2luLmJzLm1vZGFsJylcblxuICAgIHRoaXMuJGVsZW1lbnRcbiAgICAgIC5yZW1vdmVDbGFzcygnaW4nKVxuICAgICAgLm9mZignY2xpY2suZGlzbWlzcy5icy5tb2RhbCcpXG4gICAgICAub2ZmKCdtb3VzZXVwLmRpc21pc3MuYnMubW9kYWwnKVxuXG4gICAgdGhpcy4kZGlhbG9nLm9mZignbW91c2Vkb3duLmRpc21pc3MuYnMubW9kYWwnKVxuXG4gICAgJC5zdXBwb3J0LnRyYW5zaXRpb24gJiYgdGhpcy4kZWxlbWVudC5oYXNDbGFzcygnZmFkZScpID9cbiAgICAgIHRoaXMuJGVsZW1lbnRcbiAgICAgICAgLm9uZSgnYnNUcmFuc2l0aW9uRW5kJywgJC5wcm94eSh0aGlzLmhpZGVNb2RhbCwgdGhpcykpXG4gICAgICAgIC5lbXVsYXRlVHJhbnNpdGlvbkVuZChNb2RhbC5UUkFOU0lUSU9OX0RVUkFUSU9OKSA6XG4gICAgICB0aGlzLmhpZGVNb2RhbCgpXG4gIH1cblxuICBNb2RhbC5wcm90b3R5cGUuZW5mb3JjZUZvY3VzID0gZnVuY3Rpb24gKCkge1xuICAgICQoZG9jdW1lbnQpXG4gICAgICAub2ZmKCdmb2N1c2luLmJzLm1vZGFsJykgLy8gZ3VhcmQgYWdhaW5zdCBpbmZpbml0ZSBmb2N1cyBsb29wXG4gICAgICAub24oJ2ZvY3VzaW4uYnMubW9kYWwnLCAkLnByb3h5KGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIGlmICh0aGlzLiRlbGVtZW50WzBdICE9PSBlLnRhcmdldCAmJiAhdGhpcy4kZWxlbWVudC5oYXMoZS50YXJnZXQpLmxlbmd0aCkge1xuICAgICAgICAgIHRoaXMuJGVsZW1lbnQudHJpZ2dlcignZm9jdXMnKVxuICAgICAgICB9XG4gICAgICB9LCB0aGlzKSlcbiAgfVxuXG4gIE1vZGFsLnByb3RvdHlwZS5lc2NhcGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHRoaXMuaXNTaG93biAmJiB0aGlzLm9wdGlvbnMua2V5Ym9hcmQpIHtcbiAgICAgIHRoaXMuJGVsZW1lbnQub24oJ2tleWRvd24uZGlzbWlzcy5icy5tb2RhbCcsICQucHJveHkoZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgZS53aGljaCA9PSAyNyAmJiB0aGlzLmhpZGUoKVxuICAgICAgfSwgdGhpcykpXG4gICAgfSBlbHNlIGlmICghdGhpcy5pc1Nob3duKSB7XG4gICAgICB0aGlzLiRlbGVtZW50Lm9mZigna2V5ZG93bi5kaXNtaXNzLmJzLm1vZGFsJylcbiAgICB9XG4gIH1cblxuICBNb2RhbC5wcm90b3R5cGUucmVzaXplID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICh0aGlzLmlzU2hvd24pIHtcbiAgICAgICQod2luZG93KS5vbigncmVzaXplLmJzLm1vZGFsJywgJC5wcm94eSh0aGlzLmhhbmRsZVVwZGF0ZSwgdGhpcykpXG4gICAgfSBlbHNlIHtcbiAgICAgICQod2luZG93KS5vZmYoJ3Jlc2l6ZS5icy5tb2RhbCcpXG4gICAgfVxuICB9XG5cbiAgTW9kYWwucHJvdG90eXBlLmhpZGVNb2RhbCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdGhhdCA9IHRoaXNcbiAgICB0aGlzLiRlbGVtZW50LmhpZGUoKVxuICAgIHRoaXMuYmFja2Ryb3AoZnVuY3Rpb24gKCkge1xuICAgICAgdGhhdC4kYm9keS5yZW1vdmVDbGFzcygnbW9kYWwtb3BlbicpXG4gICAgICB0aGF0LnJlc2V0QWRqdXN0bWVudHMoKVxuICAgICAgdGhhdC5yZXNldFNjcm9sbGJhcigpXG4gICAgICB0aGF0LiRlbGVtZW50LnRyaWdnZXIoJ2hpZGRlbi5icy5tb2RhbCcpXG4gICAgfSlcbiAgfVxuXG4gIE1vZGFsLnByb3RvdHlwZS5yZW1vdmVCYWNrZHJvcCA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLiRiYWNrZHJvcCAmJiB0aGlzLiRiYWNrZHJvcC5yZW1vdmUoKVxuICAgIHRoaXMuJGJhY2tkcm9wID0gbnVsbFxuICB9XG5cbiAgTW9kYWwucHJvdG90eXBlLmJhY2tkcm9wID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgdmFyIHRoYXQgPSB0aGlzXG4gICAgdmFyIGFuaW1hdGUgPSB0aGlzLiRlbGVtZW50Lmhhc0NsYXNzKCdmYWRlJykgPyAnZmFkZScgOiAnJ1xuXG4gICAgaWYgKHRoaXMuaXNTaG93biAmJiB0aGlzLm9wdGlvbnMuYmFja2Ryb3ApIHtcbiAgICAgIHZhciBkb0FuaW1hdGUgPSAkLnN1cHBvcnQudHJhbnNpdGlvbiAmJiBhbmltYXRlXG5cbiAgICAgIHRoaXMuJGJhY2tkcm9wID0gJChkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKSlcbiAgICAgICAgLmFkZENsYXNzKCdtb2RhbC1iYWNrZHJvcCAnICsgYW5pbWF0ZSlcbiAgICAgICAgLmFwcGVuZFRvKHRoaXMuJGJvZHkpXG5cbiAgICAgIHRoaXMuJGVsZW1lbnQub24oJ2NsaWNrLmRpc21pc3MuYnMubW9kYWwnLCAkLnByb3h5KGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIGlmICh0aGlzLmlnbm9yZUJhY2tkcm9wQ2xpY2spIHtcbiAgICAgICAgICB0aGlzLmlnbm9yZUJhY2tkcm9wQ2xpY2sgPSBmYWxzZVxuICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG4gICAgICAgIGlmIChlLnRhcmdldCAhPT0gZS5jdXJyZW50VGFyZ2V0KSByZXR1cm5cbiAgICAgICAgdGhpcy5vcHRpb25zLmJhY2tkcm9wID09ICdzdGF0aWMnXG4gICAgICAgICAgPyB0aGlzLiRlbGVtZW50WzBdLmZvY3VzKClcbiAgICAgICAgICA6IHRoaXMuaGlkZSgpXG4gICAgICB9LCB0aGlzKSlcblxuICAgICAgaWYgKGRvQW5pbWF0ZSkgdGhpcy4kYmFja2Ryb3BbMF0ub2Zmc2V0V2lkdGggLy8gZm9yY2UgcmVmbG93XG5cbiAgICAgIHRoaXMuJGJhY2tkcm9wLmFkZENsYXNzKCdpbicpXG5cbiAgICAgIGlmICghY2FsbGJhY2spIHJldHVyblxuXG4gICAgICBkb0FuaW1hdGUgP1xuICAgICAgICB0aGlzLiRiYWNrZHJvcFxuICAgICAgICAgIC5vbmUoJ2JzVHJhbnNpdGlvbkVuZCcsIGNhbGxiYWNrKVxuICAgICAgICAgIC5lbXVsYXRlVHJhbnNpdGlvbkVuZChNb2RhbC5CQUNLRFJPUF9UUkFOU0lUSU9OX0RVUkFUSU9OKSA6XG4gICAgICAgIGNhbGxiYWNrKClcblxuICAgIH0gZWxzZSBpZiAoIXRoaXMuaXNTaG93biAmJiB0aGlzLiRiYWNrZHJvcCkge1xuICAgICAgdGhpcy4kYmFja2Ryb3AucmVtb3ZlQ2xhc3MoJ2luJylcblxuICAgICAgdmFyIGNhbGxiYWNrUmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGF0LnJlbW92ZUJhY2tkcm9wKClcbiAgICAgICAgY2FsbGJhY2sgJiYgY2FsbGJhY2soKVxuICAgICAgfVxuICAgICAgJC5zdXBwb3J0LnRyYW5zaXRpb24gJiYgdGhpcy4kZWxlbWVudC5oYXNDbGFzcygnZmFkZScpID9cbiAgICAgICAgdGhpcy4kYmFja2Ryb3BcbiAgICAgICAgICAub25lKCdic1RyYW5zaXRpb25FbmQnLCBjYWxsYmFja1JlbW92ZSlcbiAgICAgICAgICAuZW11bGF0ZVRyYW5zaXRpb25FbmQoTW9kYWwuQkFDS0RST1BfVFJBTlNJVElPTl9EVVJBVElPTikgOlxuICAgICAgICBjYWxsYmFja1JlbW92ZSgpXG5cbiAgICB9IGVsc2UgaWYgKGNhbGxiYWNrKSB7XG4gICAgICBjYWxsYmFjaygpXG4gICAgfVxuICB9XG5cbiAgLy8gdGhlc2UgZm9sbG93aW5nIG1ldGhvZHMgYXJlIHVzZWQgdG8gaGFuZGxlIG92ZXJmbG93aW5nIG1vZGFsc1xuXG4gIE1vZGFsLnByb3RvdHlwZS5oYW5kbGVVcGRhdGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5hZGp1c3REaWFsb2coKVxuICB9XG5cbiAgTW9kYWwucHJvdG90eXBlLmFkanVzdERpYWxvZyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgbW9kYWxJc092ZXJmbG93aW5nID0gdGhpcy4kZWxlbWVudFswXS5zY3JvbGxIZWlnaHQgPiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0XG5cbiAgICB0aGlzLiRlbGVtZW50LmNzcyh7XG4gICAgICBwYWRkaW5nTGVmdDogICF0aGlzLmJvZHlJc092ZXJmbG93aW5nICYmIG1vZGFsSXNPdmVyZmxvd2luZyA/IHRoaXMuc2Nyb2xsYmFyV2lkdGggOiAnJyxcbiAgICAgIHBhZGRpbmdSaWdodDogdGhpcy5ib2R5SXNPdmVyZmxvd2luZyAmJiAhbW9kYWxJc092ZXJmbG93aW5nID8gdGhpcy5zY3JvbGxiYXJXaWR0aCA6ICcnXG4gICAgfSlcbiAgfVxuXG4gIE1vZGFsLnByb3RvdHlwZS5yZXNldEFkanVzdG1lbnRzID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuJGVsZW1lbnQuY3NzKHtcbiAgICAgIHBhZGRpbmdMZWZ0OiAnJyxcbiAgICAgIHBhZGRpbmdSaWdodDogJydcbiAgICB9KVxuICB9XG5cbiAgTW9kYWwucHJvdG90eXBlLmNoZWNrU2Nyb2xsYmFyID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBmdWxsV2luZG93V2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aFxuICAgIGlmICghZnVsbFdpbmRvd1dpZHRoKSB7IC8vIHdvcmthcm91bmQgZm9yIG1pc3Npbmcgd2luZG93LmlubmVyV2lkdGggaW4gSUU4XG4gICAgICB2YXIgZG9jdW1lbnRFbGVtZW50UmVjdCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKVxuICAgICAgZnVsbFdpbmRvd1dpZHRoID0gZG9jdW1lbnRFbGVtZW50UmVjdC5yaWdodCAtIE1hdGguYWJzKGRvY3VtZW50RWxlbWVudFJlY3QubGVmdClcbiAgICB9XG4gICAgdGhpcy5ib2R5SXNPdmVyZmxvd2luZyA9IGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGggPCBmdWxsV2luZG93V2lkdGhcbiAgICB0aGlzLnNjcm9sbGJhcldpZHRoID0gdGhpcy5tZWFzdXJlU2Nyb2xsYmFyKClcbiAgfVxuXG4gIE1vZGFsLnByb3RvdHlwZS5zZXRTY3JvbGxiYXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGJvZHlQYWQgPSBwYXJzZUludCgodGhpcy4kYm9keS5jc3MoJ3BhZGRpbmctcmlnaHQnKSB8fCAwKSwgMTApXG4gICAgdGhpcy5vcmlnaW5hbEJvZHlQYWQgPSBkb2N1bWVudC5ib2R5LnN0eWxlLnBhZGRpbmdSaWdodCB8fCAnJ1xuICAgIGlmICh0aGlzLmJvZHlJc092ZXJmbG93aW5nKSB0aGlzLiRib2R5LmNzcygncGFkZGluZy1yaWdodCcsIGJvZHlQYWQgKyB0aGlzLnNjcm9sbGJhcldpZHRoKVxuICB9XG5cbiAgTW9kYWwucHJvdG90eXBlLnJlc2V0U2Nyb2xsYmFyID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuJGJvZHkuY3NzKCdwYWRkaW5nLXJpZ2h0JywgdGhpcy5vcmlnaW5hbEJvZHlQYWQpXG4gIH1cblxuICBNb2RhbC5wcm90b3R5cGUubWVhc3VyZVNjcm9sbGJhciA9IGZ1bmN0aW9uICgpIHsgLy8gdGh4IHdhbHNoXG4gICAgdmFyIHNjcm9sbERpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXG4gICAgc2Nyb2xsRGl2LmNsYXNzTmFtZSA9ICdtb2RhbC1zY3JvbGxiYXItbWVhc3VyZSdcbiAgICB0aGlzLiRib2R5LmFwcGVuZChzY3JvbGxEaXYpXG4gICAgdmFyIHNjcm9sbGJhcldpZHRoID0gc2Nyb2xsRGl2Lm9mZnNldFdpZHRoIC0gc2Nyb2xsRGl2LmNsaWVudFdpZHRoXG4gICAgdGhpcy4kYm9keVswXS5yZW1vdmVDaGlsZChzY3JvbGxEaXYpXG4gICAgcmV0dXJuIHNjcm9sbGJhcldpZHRoXG4gIH1cblxuXG4gIC8vIE1PREFMIFBMVUdJTiBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgZnVuY3Rpb24gUGx1Z2luKG9wdGlvbiwgX3JlbGF0ZWRUYXJnZXQpIHtcbiAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciAkdGhpcyAgID0gJCh0aGlzKVxuICAgICAgdmFyIGRhdGEgICAgPSAkdGhpcy5kYXRhKCdicy5tb2RhbCcpXG4gICAgICB2YXIgb3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCBNb2RhbC5ERUZBVUxUUywgJHRoaXMuZGF0YSgpLCB0eXBlb2Ygb3B0aW9uID09ICdvYmplY3QnICYmIG9wdGlvbilcblxuICAgICAgaWYgKCFkYXRhKSAkdGhpcy5kYXRhKCdicy5tb2RhbCcsIChkYXRhID0gbmV3IE1vZGFsKHRoaXMsIG9wdGlvbnMpKSlcbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9uID09ICdzdHJpbmcnKSBkYXRhW29wdGlvbl0oX3JlbGF0ZWRUYXJnZXQpXG4gICAgICBlbHNlIGlmIChvcHRpb25zLnNob3cpIGRhdGEuc2hvdyhfcmVsYXRlZFRhcmdldClcbiAgICB9KVxuICB9XG5cbiAgdmFyIG9sZCA9ICQuZm4ubW9kYWxcblxuICAkLmZuLm1vZGFsICAgICAgICAgICAgID0gUGx1Z2luXG4gICQuZm4ubW9kYWwuQ29uc3RydWN0b3IgPSBNb2RhbFxuXG5cbiAgLy8gTU9EQUwgTk8gQ09ORkxJQ1RcbiAgLy8gPT09PT09PT09PT09PT09PT1cblxuICAkLmZuLm1vZGFsLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XG4gICAgJC5mbi5tb2RhbCA9IG9sZFxuICAgIHJldHVybiB0aGlzXG4gIH1cblxuXG4gIC8vIE1PREFMIERBVEEtQVBJXG4gIC8vID09PT09PT09PT09PT09XG5cbiAgJChkb2N1bWVudCkub24oJ2NsaWNrLmJzLm1vZGFsLmRhdGEtYXBpJywgJ1tkYXRhLXRvZ2dsZT1cIm1vZGFsXCJdJywgZnVuY3Rpb24gKGUpIHtcbiAgICB2YXIgJHRoaXMgICA9ICQodGhpcylcbiAgICB2YXIgaHJlZiAgICA9ICR0aGlzLmF0dHIoJ2hyZWYnKVxuICAgIHZhciAkdGFyZ2V0ID0gJCgkdGhpcy5hdHRyKCdkYXRhLXRhcmdldCcpIHx8IChocmVmICYmIGhyZWYucmVwbGFjZSgvLiooPz0jW15cXHNdKyQpLywgJycpKSkgLy8gc3RyaXAgZm9yIGllN1xuICAgIHZhciBvcHRpb24gID0gJHRhcmdldC5kYXRhKCdicy5tb2RhbCcpID8gJ3RvZ2dsZScgOiAkLmV4dGVuZCh7IHJlbW90ZTogIS8jLy50ZXN0KGhyZWYpICYmIGhyZWYgfSwgJHRhcmdldC5kYXRhKCksICR0aGlzLmRhdGEoKSlcblxuICAgIGlmICgkdGhpcy5pcygnYScpKSBlLnByZXZlbnREZWZhdWx0KClcblxuICAgICR0YXJnZXQub25lKCdzaG93LmJzLm1vZGFsJywgZnVuY3Rpb24gKHNob3dFdmVudCkge1xuICAgICAgaWYgKHNob3dFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkgcmV0dXJuIC8vIG9ubHkgcmVnaXN0ZXIgZm9jdXMgcmVzdG9yZXIgaWYgbW9kYWwgd2lsbCBhY3R1YWxseSBnZXQgc2hvd25cbiAgICAgICR0YXJnZXQub25lKCdoaWRkZW4uYnMubW9kYWwnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICR0aGlzLmlzKCc6dmlzaWJsZScpICYmICR0aGlzLnRyaWdnZXIoJ2ZvY3VzJylcbiAgICAgIH0pXG4gICAgfSlcbiAgICBQbHVnaW4uY2FsbCgkdGFyZ2V0LCBvcHRpb24sIHRoaXMpXG4gIH0pXG5cbn0oalF1ZXJ5KTtcblxuLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBCb290c3RyYXA6IHRvb2x0aXAuanMgdjMuMy42XG4gKiBodHRwOi8vZ2V0Ym9vdHN0cmFwLmNvbS9qYXZhc2NyaXB0LyN0b29sdGlwXG4gKiBJbnNwaXJlZCBieSB0aGUgb3JpZ2luYWwgalF1ZXJ5LnRpcHN5IGJ5IEphc29uIEZyYW1lXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqIENvcHlyaWdodCAyMDExLTIwMTUgVHdpdHRlciwgSW5jLlxuICogTGljZW5zZWQgdW5kZXIgTUlUIChodHRwczovL2dpdGh1Yi5jb20vdHdicy9ib290c3RyYXAvYmxvYi9tYXN0ZXIvTElDRU5TRSlcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG5cbitmdW5jdGlvbiAoJCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgLy8gVE9PTFRJUCBQVUJMSUMgQ0xBU1MgREVGSU5JVElPTlxuICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgdmFyIFRvb2x0aXAgPSBmdW5jdGlvbiAoZWxlbWVudCwgb3B0aW9ucykge1xuICAgIHRoaXMudHlwZSAgICAgICA9IG51bGxcbiAgICB0aGlzLm9wdGlvbnMgICAgPSBudWxsXG4gICAgdGhpcy5lbmFibGVkICAgID0gbnVsbFxuICAgIHRoaXMudGltZW91dCAgICA9IG51bGxcbiAgICB0aGlzLmhvdmVyU3RhdGUgPSBudWxsXG4gICAgdGhpcy4kZWxlbWVudCAgID0gbnVsbFxuICAgIHRoaXMuaW5TdGF0ZSAgICA9IG51bGxcblxuICAgIHRoaXMuaW5pdCgndG9vbHRpcCcsIGVsZW1lbnQsIG9wdGlvbnMpXG4gIH1cblxuICBUb29sdGlwLlZFUlNJT04gID0gJzMuMy42J1xuXG4gIFRvb2x0aXAuVFJBTlNJVElPTl9EVVJBVElPTiA9IDE1MFxuXG4gIFRvb2x0aXAuREVGQVVMVFMgPSB7XG4gICAgYW5pbWF0aW9uOiB0cnVlLFxuICAgIHBsYWNlbWVudDogJ3RvcCcsXG4gICAgc2VsZWN0b3I6IGZhbHNlLFxuICAgIHRlbXBsYXRlOiAnPGRpdiBjbGFzcz1cInRvb2x0aXBcIiByb2xlPVwidG9vbHRpcFwiPjxkaXYgY2xhc3M9XCJ0b29sdGlwLWFycm93XCI+PC9kaXY+PGRpdiBjbGFzcz1cInRvb2x0aXAtaW5uZXJcIj48L2Rpdj48L2Rpdj4nLFxuICAgIHRyaWdnZXI6ICdob3ZlciBmb2N1cycsXG4gICAgdGl0bGU6ICcnLFxuICAgIGRlbGF5OiAwLFxuICAgIGh0bWw6IGZhbHNlLFxuICAgIGNvbnRhaW5lcjogZmFsc2UsXG4gICAgdmlld3BvcnQ6IHtcbiAgICAgIHNlbGVjdG9yOiAnYm9keScsXG4gICAgICBwYWRkaW5nOiAwXG4gICAgfVxuICB9XG5cbiAgVG9vbHRpcC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICh0eXBlLCBlbGVtZW50LCBvcHRpb25zKSB7XG4gICAgdGhpcy5lbmFibGVkICAgPSB0cnVlXG4gICAgdGhpcy50eXBlICAgICAgPSB0eXBlXG4gICAgdGhpcy4kZWxlbWVudCAgPSAkKGVsZW1lbnQpXG4gICAgdGhpcy5vcHRpb25zICAgPSB0aGlzLmdldE9wdGlvbnMob3B0aW9ucylcbiAgICB0aGlzLiR2aWV3cG9ydCA9IHRoaXMub3B0aW9ucy52aWV3cG9ydCAmJiAkKCQuaXNGdW5jdGlvbih0aGlzLm9wdGlvbnMudmlld3BvcnQpID8gdGhpcy5vcHRpb25zLnZpZXdwb3J0LmNhbGwodGhpcywgdGhpcy4kZWxlbWVudCkgOiAodGhpcy5vcHRpb25zLnZpZXdwb3J0LnNlbGVjdG9yIHx8IHRoaXMub3B0aW9ucy52aWV3cG9ydCkpXG4gICAgdGhpcy5pblN0YXRlICAgPSB7IGNsaWNrOiBmYWxzZSwgaG92ZXI6IGZhbHNlLCBmb2N1czogZmFsc2UgfVxuXG4gICAgaWYgKHRoaXMuJGVsZW1lbnRbMF0gaW5zdGFuY2VvZiBkb2N1bWVudC5jb25zdHJ1Y3RvciAmJiAhdGhpcy5vcHRpb25zLnNlbGVjdG9yKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ2BzZWxlY3RvcmAgb3B0aW9uIG11c3QgYmUgc3BlY2lmaWVkIHdoZW4gaW5pdGlhbGl6aW5nICcgKyB0aGlzLnR5cGUgKyAnIG9uIHRoZSB3aW5kb3cuZG9jdW1lbnQgb2JqZWN0IScpXG4gICAgfVxuXG4gICAgdmFyIHRyaWdnZXJzID0gdGhpcy5vcHRpb25zLnRyaWdnZXIuc3BsaXQoJyAnKVxuXG4gICAgZm9yICh2YXIgaSA9IHRyaWdnZXJzLmxlbmd0aDsgaS0tOykge1xuICAgICAgdmFyIHRyaWdnZXIgPSB0cmlnZ2Vyc1tpXVxuXG4gICAgICBpZiAodHJpZ2dlciA9PSAnY2xpY2snKSB7XG4gICAgICAgIHRoaXMuJGVsZW1lbnQub24oJ2NsaWNrLicgKyB0aGlzLnR5cGUsIHRoaXMub3B0aW9ucy5zZWxlY3RvciwgJC5wcm94eSh0aGlzLnRvZ2dsZSwgdGhpcykpXG4gICAgICB9IGVsc2UgaWYgKHRyaWdnZXIgIT0gJ21hbnVhbCcpIHtcbiAgICAgICAgdmFyIGV2ZW50SW4gID0gdHJpZ2dlciA9PSAnaG92ZXInID8gJ21vdXNlZW50ZXInIDogJ2ZvY3VzaW4nXG4gICAgICAgIHZhciBldmVudE91dCA9IHRyaWdnZXIgPT0gJ2hvdmVyJyA/ICdtb3VzZWxlYXZlJyA6ICdmb2N1c291dCdcblxuICAgICAgICB0aGlzLiRlbGVtZW50Lm9uKGV2ZW50SW4gICsgJy4nICsgdGhpcy50eXBlLCB0aGlzLm9wdGlvbnMuc2VsZWN0b3IsICQucHJveHkodGhpcy5lbnRlciwgdGhpcykpXG4gICAgICAgIHRoaXMuJGVsZW1lbnQub24oZXZlbnRPdXQgKyAnLicgKyB0aGlzLnR5cGUsIHRoaXMub3B0aW9ucy5zZWxlY3RvciwgJC5wcm94eSh0aGlzLmxlYXZlLCB0aGlzKSlcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLm9wdGlvbnMuc2VsZWN0b3IgP1xuICAgICAgKHRoaXMuX29wdGlvbnMgPSAkLmV4dGVuZCh7fSwgdGhpcy5vcHRpb25zLCB7IHRyaWdnZXI6ICdtYW51YWwnLCBzZWxlY3RvcjogJycgfSkpIDpcbiAgICAgIHRoaXMuZml4VGl0bGUoKVxuICB9XG5cbiAgVG9vbHRpcC5wcm90b3R5cGUuZ2V0RGVmYXVsdHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIFRvb2x0aXAuREVGQVVMVFNcbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLmdldE9wdGlvbnMgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgIG9wdGlvbnMgPSAkLmV4dGVuZCh7fSwgdGhpcy5nZXREZWZhdWx0cygpLCB0aGlzLiRlbGVtZW50LmRhdGEoKSwgb3B0aW9ucylcblxuICAgIGlmIChvcHRpb25zLmRlbGF5ICYmIHR5cGVvZiBvcHRpb25zLmRlbGF5ID09ICdudW1iZXInKSB7XG4gICAgICBvcHRpb25zLmRlbGF5ID0ge1xuICAgICAgICBzaG93OiBvcHRpb25zLmRlbGF5LFxuICAgICAgICBoaWRlOiBvcHRpb25zLmRlbGF5XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIG9wdGlvbnNcbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLmdldERlbGVnYXRlT3B0aW9ucyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgb3B0aW9ucyAgPSB7fVxuICAgIHZhciBkZWZhdWx0cyA9IHRoaXMuZ2V0RGVmYXVsdHMoKVxuXG4gICAgdGhpcy5fb3B0aW9ucyAmJiAkLmVhY2godGhpcy5fb3B0aW9ucywgZnVuY3Rpb24gKGtleSwgdmFsdWUpIHtcbiAgICAgIGlmIChkZWZhdWx0c1trZXldICE9IHZhbHVlKSBvcHRpb25zW2tleV0gPSB2YWx1ZVxuICAgIH0pXG5cbiAgICByZXR1cm4gb3B0aW9uc1xuICB9XG5cbiAgVG9vbHRpcC5wcm90b3R5cGUuZW50ZXIgPSBmdW5jdGlvbiAob2JqKSB7XG4gICAgdmFyIHNlbGYgPSBvYmogaW5zdGFuY2VvZiB0aGlzLmNvbnN0cnVjdG9yID9cbiAgICAgIG9iaiA6ICQob2JqLmN1cnJlbnRUYXJnZXQpLmRhdGEoJ2JzLicgKyB0aGlzLnR5cGUpXG5cbiAgICBpZiAoIXNlbGYpIHtcbiAgICAgIHNlbGYgPSBuZXcgdGhpcy5jb25zdHJ1Y3RvcihvYmouY3VycmVudFRhcmdldCwgdGhpcy5nZXREZWxlZ2F0ZU9wdGlvbnMoKSlcbiAgICAgICQob2JqLmN1cnJlbnRUYXJnZXQpLmRhdGEoJ2JzLicgKyB0aGlzLnR5cGUsIHNlbGYpXG4gICAgfVxuXG4gICAgaWYgKG9iaiBpbnN0YW5jZW9mICQuRXZlbnQpIHtcbiAgICAgIHNlbGYuaW5TdGF0ZVtvYmoudHlwZSA9PSAnZm9jdXNpbicgPyAnZm9jdXMnIDogJ2hvdmVyJ10gPSB0cnVlXG4gICAgfVxuXG4gICAgaWYgKHNlbGYudGlwKCkuaGFzQ2xhc3MoJ2luJykgfHwgc2VsZi5ob3ZlclN0YXRlID09ICdpbicpIHtcbiAgICAgIHNlbGYuaG92ZXJTdGF0ZSA9ICdpbidcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGNsZWFyVGltZW91dChzZWxmLnRpbWVvdXQpXG5cbiAgICBzZWxmLmhvdmVyU3RhdGUgPSAnaW4nXG5cbiAgICBpZiAoIXNlbGYub3B0aW9ucy5kZWxheSB8fCAhc2VsZi5vcHRpb25zLmRlbGF5LnNob3cpIHJldHVybiBzZWxmLnNob3coKVxuXG4gICAgc2VsZi50aW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoc2VsZi5ob3ZlclN0YXRlID09ICdpbicpIHNlbGYuc2hvdygpXG4gICAgfSwgc2VsZi5vcHRpb25zLmRlbGF5LnNob3cpXG4gIH1cblxuICBUb29sdGlwLnByb3RvdHlwZS5pc0luU3RhdGVUcnVlID0gZnVuY3Rpb24gKCkge1xuICAgIGZvciAodmFyIGtleSBpbiB0aGlzLmluU3RhdGUpIHtcbiAgICAgIGlmICh0aGlzLmluU3RhdGVba2V5XSkgcmV0dXJuIHRydWVcbiAgICB9XG5cbiAgICByZXR1cm4gZmFsc2VcbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLmxlYXZlID0gZnVuY3Rpb24gKG9iaikge1xuICAgIHZhciBzZWxmID0gb2JqIGluc3RhbmNlb2YgdGhpcy5jb25zdHJ1Y3RvciA/XG4gICAgICBvYmogOiAkKG9iai5jdXJyZW50VGFyZ2V0KS5kYXRhKCdicy4nICsgdGhpcy50eXBlKVxuXG4gICAgaWYgKCFzZWxmKSB7XG4gICAgICBzZWxmID0gbmV3IHRoaXMuY29uc3RydWN0b3Iob2JqLmN1cnJlbnRUYXJnZXQsIHRoaXMuZ2V0RGVsZWdhdGVPcHRpb25zKCkpXG4gICAgICAkKG9iai5jdXJyZW50VGFyZ2V0KS5kYXRhKCdicy4nICsgdGhpcy50eXBlLCBzZWxmKVxuICAgIH1cblxuICAgIGlmIChvYmogaW5zdGFuY2VvZiAkLkV2ZW50KSB7XG4gICAgICBzZWxmLmluU3RhdGVbb2JqLnR5cGUgPT0gJ2ZvY3Vzb3V0JyA/ICdmb2N1cycgOiAnaG92ZXInXSA9IGZhbHNlXG4gICAgfVxuXG4gICAgaWYgKHNlbGYuaXNJblN0YXRlVHJ1ZSgpKSByZXR1cm5cblxuICAgIGNsZWFyVGltZW91dChzZWxmLnRpbWVvdXQpXG5cbiAgICBzZWxmLmhvdmVyU3RhdGUgPSAnb3V0J1xuXG4gICAgaWYgKCFzZWxmLm9wdGlvbnMuZGVsYXkgfHwgIXNlbGYub3B0aW9ucy5kZWxheS5oaWRlKSByZXR1cm4gc2VsZi5oaWRlKClcblxuICAgIHNlbGYudGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKHNlbGYuaG92ZXJTdGF0ZSA9PSAnb3V0Jykgc2VsZi5oaWRlKClcbiAgICB9LCBzZWxmLm9wdGlvbnMuZGVsYXkuaGlkZSlcbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLnNob3cgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGUgPSAkLkV2ZW50KCdzaG93LmJzLicgKyB0aGlzLnR5cGUpXG5cbiAgICBpZiAodGhpcy5oYXNDb250ZW50KCkgJiYgdGhpcy5lbmFibGVkKSB7XG4gICAgICB0aGlzLiRlbGVtZW50LnRyaWdnZXIoZSlcblxuICAgICAgdmFyIGluRG9tID0gJC5jb250YWlucyh0aGlzLiRlbGVtZW50WzBdLm93bmVyRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCB0aGlzLiRlbGVtZW50WzBdKVxuICAgICAgaWYgKGUuaXNEZWZhdWx0UHJldmVudGVkKCkgfHwgIWluRG9tKSByZXR1cm5cbiAgICAgIHZhciB0aGF0ID0gdGhpc1xuXG4gICAgICB2YXIgJHRpcCA9IHRoaXMudGlwKClcblxuICAgICAgdmFyIHRpcElkID0gdGhpcy5nZXRVSUQodGhpcy50eXBlKVxuXG4gICAgICB0aGlzLnNldENvbnRlbnQoKVxuICAgICAgJHRpcC5hdHRyKCdpZCcsIHRpcElkKVxuICAgICAgdGhpcy4kZWxlbWVudC5hdHRyKCdhcmlhLWRlc2NyaWJlZGJ5JywgdGlwSWQpXG5cbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuYW5pbWF0aW9uKSAkdGlwLmFkZENsYXNzKCdmYWRlJylcblxuICAgICAgdmFyIHBsYWNlbWVudCA9IHR5cGVvZiB0aGlzLm9wdGlvbnMucGxhY2VtZW50ID09ICdmdW5jdGlvbicgP1xuICAgICAgICB0aGlzLm9wdGlvbnMucGxhY2VtZW50LmNhbGwodGhpcywgJHRpcFswXSwgdGhpcy4kZWxlbWVudFswXSkgOlxuICAgICAgICB0aGlzLm9wdGlvbnMucGxhY2VtZW50XG5cbiAgICAgIHZhciBhdXRvVG9rZW4gPSAvXFxzP2F1dG8/XFxzPy9pXG4gICAgICB2YXIgYXV0b1BsYWNlID0gYXV0b1Rva2VuLnRlc3QocGxhY2VtZW50KVxuICAgICAgaWYgKGF1dG9QbGFjZSkgcGxhY2VtZW50ID0gcGxhY2VtZW50LnJlcGxhY2UoYXV0b1Rva2VuLCAnJykgfHwgJ3RvcCdcblxuICAgICAgJHRpcFxuICAgICAgICAuZGV0YWNoKClcbiAgICAgICAgLmNzcyh7IHRvcDogMCwgbGVmdDogMCwgZGlzcGxheTogJ2Jsb2NrJyB9KVxuICAgICAgICAuYWRkQ2xhc3MocGxhY2VtZW50KVxuICAgICAgICAuZGF0YSgnYnMuJyArIHRoaXMudHlwZSwgdGhpcylcblxuICAgICAgdGhpcy5vcHRpb25zLmNvbnRhaW5lciA/ICR0aXAuYXBwZW5kVG8odGhpcy5vcHRpb25zLmNvbnRhaW5lcikgOiAkdGlwLmluc2VydEFmdGVyKHRoaXMuJGVsZW1lbnQpXG4gICAgICB0aGlzLiRlbGVtZW50LnRyaWdnZXIoJ2luc2VydGVkLmJzLicgKyB0aGlzLnR5cGUpXG5cbiAgICAgIHZhciBwb3MgICAgICAgICAgPSB0aGlzLmdldFBvc2l0aW9uKClcbiAgICAgIHZhciBhY3R1YWxXaWR0aCAgPSAkdGlwWzBdLm9mZnNldFdpZHRoXG4gICAgICB2YXIgYWN0dWFsSGVpZ2h0ID0gJHRpcFswXS5vZmZzZXRIZWlnaHRcblxuICAgICAgaWYgKGF1dG9QbGFjZSkge1xuICAgICAgICB2YXIgb3JnUGxhY2VtZW50ID0gcGxhY2VtZW50XG4gICAgICAgIHZhciB2aWV3cG9ydERpbSA9IHRoaXMuZ2V0UG9zaXRpb24odGhpcy4kdmlld3BvcnQpXG5cbiAgICAgICAgcGxhY2VtZW50ID0gcGxhY2VtZW50ID09ICdib3R0b20nICYmIHBvcy5ib3R0b20gKyBhY3R1YWxIZWlnaHQgPiB2aWV3cG9ydERpbS5ib3R0b20gPyAndG9wJyAgICA6XG4gICAgICAgICAgICAgICAgICAgIHBsYWNlbWVudCA9PSAndG9wJyAgICAmJiBwb3MudG9wICAgIC0gYWN0dWFsSGVpZ2h0IDwgdmlld3BvcnREaW0udG9wICAgID8gJ2JvdHRvbScgOlxuICAgICAgICAgICAgICAgICAgICBwbGFjZW1lbnQgPT0gJ3JpZ2h0JyAgJiYgcG9zLnJpZ2h0ICArIGFjdHVhbFdpZHRoICA+IHZpZXdwb3J0RGltLndpZHRoICA/ICdsZWZ0JyAgIDpcbiAgICAgICAgICAgICAgICAgICAgcGxhY2VtZW50ID09ICdsZWZ0JyAgICYmIHBvcy5sZWZ0ICAgLSBhY3R1YWxXaWR0aCAgPCB2aWV3cG9ydERpbS5sZWZ0ICAgPyAncmlnaHQnICA6XG4gICAgICAgICAgICAgICAgICAgIHBsYWNlbWVudFxuXG4gICAgICAgICR0aXBcbiAgICAgICAgICAucmVtb3ZlQ2xhc3Mob3JnUGxhY2VtZW50KVxuICAgICAgICAgIC5hZGRDbGFzcyhwbGFjZW1lbnQpXG4gICAgICB9XG5cbiAgICAgIHZhciBjYWxjdWxhdGVkT2Zmc2V0ID0gdGhpcy5nZXRDYWxjdWxhdGVkT2Zmc2V0KHBsYWNlbWVudCwgcG9zLCBhY3R1YWxXaWR0aCwgYWN0dWFsSGVpZ2h0KVxuXG4gICAgICB0aGlzLmFwcGx5UGxhY2VtZW50KGNhbGN1bGF0ZWRPZmZzZXQsIHBsYWNlbWVudClcblxuICAgICAgdmFyIGNvbXBsZXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgcHJldkhvdmVyU3RhdGUgPSB0aGF0LmhvdmVyU3RhdGVcbiAgICAgICAgdGhhdC4kZWxlbWVudC50cmlnZ2VyKCdzaG93bi5icy4nICsgdGhhdC50eXBlKVxuICAgICAgICB0aGF0LmhvdmVyU3RhdGUgPSBudWxsXG5cbiAgICAgICAgaWYgKHByZXZIb3ZlclN0YXRlID09ICdvdXQnKSB0aGF0LmxlYXZlKHRoYXQpXG4gICAgICB9XG5cbiAgICAgICQuc3VwcG9ydC50cmFuc2l0aW9uICYmIHRoaXMuJHRpcC5oYXNDbGFzcygnZmFkZScpID9cbiAgICAgICAgJHRpcFxuICAgICAgICAgIC5vbmUoJ2JzVHJhbnNpdGlvbkVuZCcsIGNvbXBsZXRlKVxuICAgICAgICAgIC5lbXVsYXRlVHJhbnNpdGlvbkVuZChUb29sdGlwLlRSQU5TSVRJT05fRFVSQVRJT04pIDpcbiAgICAgICAgY29tcGxldGUoKVxuICAgIH1cbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLmFwcGx5UGxhY2VtZW50ID0gZnVuY3Rpb24gKG9mZnNldCwgcGxhY2VtZW50KSB7XG4gICAgdmFyICR0aXAgICA9IHRoaXMudGlwKClcbiAgICB2YXIgd2lkdGggID0gJHRpcFswXS5vZmZzZXRXaWR0aFxuICAgIHZhciBoZWlnaHQgPSAkdGlwWzBdLm9mZnNldEhlaWdodFxuXG4gICAgLy8gbWFudWFsbHkgcmVhZCBtYXJnaW5zIGJlY2F1c2UgZ2V0Qm91bmRpbmdDbGllbnRSZWN0IGluY2x1ZGVzIGRpZmZlcmVuY2VcbiAgICB2YXIgbWFyZ2luVG9wID0gcGFyc2VJbnQoJHRpcC5jc3MoJ21hcmdpbi10b3AnKSwgMTApXG4gICAgdmFyIG1hcmdpbkxlZnQgPSBwYXJzZUludCgkdGlwLmNzcygnbWFyZ2luLWxlZnQnKSwgMTApXG5cbiAgICAvLyB3ZSBtdXN0IGNoZWNrIGZvciBOYU4gZm9yIGllIDgvOVxuICAgIGlmIChpc05hTihtYXJnaW5Ub3ApKSAgbWFyZ2luVG9wICA9IDBcbiAgICBpZiAoaXNOYU4obWFyZ2luTGVmdCkpIG1hcmdpbkxlZnQgPSAwXG5cbiAgICBvZmZzZXQudG9wICArPSBtYXJnaW5Ub3BcbiAgICBvZmZzZXQubGVmdCArPSBtYXJnaW5MZWZ0XG5cbiAgICAvLyAkLmZuLm9mZnNldCBkb2Vzbid0IHJvdW5kIHBpeGVsIHZhbHVlc1xuICAgIC8vIHNvIHdlIHVzZSBzZXRPZmZzZXQgZGlyZWN0bHkgd2l0aCBvdXIgb3duIGZ1bmN0aW9uIEItMFxuICAgICQub2Zmc2V0LnNldE9mZnNldCgkdGlwWzBdLCAkLmV4dGVuZCh7XG4gICAgICB1c2luZzogZnVuY3Rpb24gKHByb3BzKSB7XG4gICAgICAgICR0aXAuY3NzKHtcbiAgICAgICAgICB0b3A6IE1hdGgucm91bmQocHJvcHMudG9wKSxcbiAgICAgICAgICBsZWZ0OiBNYXRoLnJvdW5kKHByb3BzLmxlZnQpXG4gICAgICAgIH0pXG4gICAgICB9XG4gICAgfSwgb2Zmc2V0KSwgMClcblxuICAgICR0aXAuYWRkQ2xhc3MoJ2luJylcblxuICAgIC8vIGNoZWNrIHRvIHNlZSBpZiBwbGFjaW5nIHRpcCBpbiBuZXcgb2Zmc2V0IGNhdXNlZCB0aGUgdGlwIHRvIHJlc2l6ZSBpdHNlbGZcbiAgICB2YXIgYWN0dWFsV2lkdGggID0gJHRpcFswXS5vZmZzZXRXaWR0aFxuICAgIHZhciBhY3R1YWxIZWlnaHQgPSAkdGlwWzBdLm9mZnNldEhlaWdodFxuXG4gICAgaWYgKHBsYWNlbWVudCA9PSAndG9wJyAmJiBhY3R1YWxIZWlnaHQgIT0gaGVpZ2h0KSB7XG4gICAgICBvZmZzZXQudG9wID0gb2Zmc2V0LnRvcCArIGhlaWdodCAtIGFjdHVhbEhlaWdodFxuICAgIH1cblxuICAgIHZhciBkZWx0YSA9IHRoaXMuZ2V0Vmlld3BvcnRBZGp1c3RlZERlbHRhKHBsYWNlbWVudCwgb2Zmc2V0LCBhY3R1YWxXaWR0aCwgYWN0dWFsSGVpZ2h0KVxuXG4gICAgaWYgKGRlbHRhLmxlZnQpIG9mZnNldC5sZWZ0ICs9IGRlbHRhLmxlZnRcbiAgICBlbHNlIG9mZnNldC50b3AgKz0gZGVsdGEudG9wXG5cbiAgICB2YXIgaXNWZXJ0aWNhbCAgICAgICAgICA9IC90b3B8Ym90dG9tLy50ZXN0KHBsYWNlbWVudClcbiAgICB2YXIgYXJyb3dEZWx0YSAgICAgICAgICA9IGlzVmVydGljYWwgPyBkZWx0YS5sZWZ0ICogMiAtIHdpZHRoICsgYWN0dWFsV2lkdGggOiBkZWx0YS50b3AgKiAyIC0gaGVpZ2h0ICsgYWN0dWFsSGVpZ2h0XG4gICAgdmFyIGFycm93T2Zmc2V0UG9zaXRpb24gPSBpc1ZlcnRpY2FsID8gJ29mZnNldFdpZHRoJyA6ICdvZmZzZXRIZWlnaHQnXG5cbiAgICAkdGlwLm9mZnNldChvZmZzZXQpXG4gICAgdGhpcy5yZXBsYWNlQXJyb3coYXJyb3dEZWx0YSwgJHRpcFswXVthcnJvd09mZnNldFBvc2l0aW9uXSwgaXNWZXJ0aWNhbClcbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLnJlcGxhY2VBcnJvdyA9IGZ1bmN0aW9uIChkZWx0YSwgZGltZW5zaW9uLCBpc1ZlcnRpY2FsKSB7XG4gICAgdGhpcy5hcnJvdygpXG4gICAgICAuY3NzKGlzVmVydGljYWwgPyAnbGVmdCcgOiAndG9wJywgNTAgKiAoMSAtIGRlbHRhIC8gZGltZW5zaW9uKSArICclJylcbiAgICAgIC5jc3MoaXNWZXJ0aWNhbCA/ICd0b3AnIDogJ2xlZnQnLCAnJylcbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLnNldENvbnRlbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyICR0aXAgID0gdGhpcy50aXAoKVxuICAgIHZhciB0aXRsZSA9IHRoaXMuZ2V0VGl0bGUoKVxuXG4gICAgJHRpcC5maW5kKCcudG9vbHRpcC1pbm5lcicpW3RoaXMub3B0aW9ucy5odG1sID8gJ2h0bWwnIDogJ3RleHQnXSh0aXRsZSlcbiAgICAkdGlwLnJlbW92ZUNsYXNzKCdmYWRlIGluIHRvcCBib3R0b20gbGVmdCByaWdodCcpXG4gIH1cblxuICBUb29sdGlwLnByb3RvdHlwZS5oaWRlID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgdmFyIHRoYXQgPSB0aGlzXG4gICAgdmFyICR0aXAgPSAkKHRoaXMuJHRpcClcbiAgICB2YXIgZSAgICA9ICQuRXZlbnQoJ2hpZGUuYnMuJyArIHRoaXMudHlwZSlcblxuICAgIGZ1bmN0aW9uIGNvbXBsZXRlKCkge1xuICAgICAgaWYgKHRoYXQuaG92ZXJTdGF0ZSAhPSAnaW4nKSAkdGlwLmRldGFjaCgpXG4gICAgICB0aGF0LiRlbGVtZW50XG4gICAgICAgIC5yZW1vdmVBdHRyKCdhcmlhLWRlc2NyaWJlZGJ5JylcbiAgICAgICAgLnRyaWdnZXIoJ2hpZGRlbi5icy4nICsgdGhhdC50eXBlKVxuICAgICAgY2FsbGJhY2sgJiYgY2FsbGJhY2soKVxuICAgIH1cblxuICAgIHRoaXMuJGVsZW1lbnQudHJpZ2dlcihlKVxuXG4gICAgaWYgKGUuaXNEZWZhdWx0UHJldmVudGVkKCkpIHJldHVyblxuXG4gICAgJHRpcC5yZW1vdmVDbGFzcygnaW4nKVxuXG4gICAgJC5zdXBwb3J0LnRyYW5zaXRpb24gJiYgJHRpcC5oYXNDbGFzcygnZmFkZScpID9cbiAgICAgICR0aXBcbiAgICAgICAgLm9uZSgnYnNUcmFuc2l0aW9uRW5kJywgY29tcGxldGUpXG4gICAgICAgIC5lbXVsYXRlVHJhbnNpdGlvbkVuZChUb29sdGlwLlRSQU5TSVRJT05fRFVSQVRJT04pIDpcbiAgICAgIGNvbXBsZXRlKClcblxuICAgIHRoaXMuaG92ZXJTdGF0ZSA9IG51bGxcblxuICAgIHJldHVybiB0aGlzXG4gIH1cblxuICBUb29sdGlwLnByb3RvdHlwZS5maXhUaXRsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgJGUgPSB0aGlzLiRlbGVtZW50XG4gICAgaWYgKCRlLmF0dHIoJ3RpdGxlJykgfHwgdHlwZW9mICRlLmF0dHIoJ2RhdGEtb3JpZ2luYWwtdGl0bGUnKSAhPSAnc3RyaW5nJykge1xuICAgICAgJGUuYXR0cignZGF0YS1vcmlnaW5hbC10aXRsZScsICRlLmF0dHIoJ3RpdGxlJykgfHwgJycpLmF0dHIoJ3RpdGxlJywgJycpXG4gICAgfVxuICB9XG5cbiAgVG9vbHRpcC5wcm90b3R5cGUuaGFzQ29udGVudCA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRUaXRsZSgpXG4gIH1cblxuICBUb29sdGlwLnByb3RvdHlwZS5nZXRQb3NpdGlvbiA9IGZ1bmN0aW9uICgkZWxlbWVudCkge1xuICAgICRlbGVtZW50ICAgPSAkZWxlbWVudCB8fCB0aGlzLiRlbGVtZW50XG5cbiAgICB2YXIgZWwgICAgID0gJGVsZW1lbnRbMF1cbiAgICB2YXIgaXNCb2R5ID0gZWwudGFnTmFtZSA9PSAnQk9EWSdcblxuICAgIHZhciBlbFJlY3QgICAgPSBlbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKVxuICAgIGlmIChlbFJlY3Qud2lkdGggPT0gbnVsbCkge1xuICAgICAgLy8gd2lkdGggYW5kIGhlaWdodCBhcmUgbWlzc2luZyBpbiBJRTgsIHNvIGNvbXB1dGUgdGhlbSBtYW51YWxseTsgc2VlIGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9pc3N1ZXMvMTQwOTNcbiAgICAgIGVsUmVjdCA9ICQuZXh0ZW5kKHt9LCBlbFJlY3QsIHsgd2lkdGg6IGVsUmVjdC5yaWdodCAtIGVsUmVjdC5sZWZ0LCBoZWlnaHQ6IGVsUmVjdC5ib3R0b20gLSBlbFJlY3QudG9wIH0pXG4gICAgfVxuICAgIHZhciBlbE9mZnNldCAgPSBpc0JvZHkgPyB7IHRvcDogMCwgbGVmdDogMCB9IDogJGVsZW1lbnQub2Zmc2V0KClcbiAgICB2YXIgc2Nyb2xsICAgID0geyBzY3JvbGw6IGlzQm9keSA/IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3AgfHwgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgOiAkZWxlbWVudC5zY3JvbGxUb3AoKSB9XG4gICAgdmFyIG91dGVyRGltcyA9IGlzQm9keSA/IHsgd2lkdGg6ICQod2luZG93KS53aWR0aCgpLCBoZWlnaHQ6ICQod2luZG93KS5oZWlnaHQoKSB9IDogbnVsbFxuXG4gICAgcmV0dXJuICQuZXh0ZW5kKHt9LCBlbFJlY3QsIHNjcm9sbCwgb3V0ZXJEaW1zLCBlbE9mZnNldClcbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLmdldENhbGN1bGF0ZWRPZmZzZXQgPSBmdW5jdGlvbiAocGxhY2VtZW50LCBwb3MsIGFjdHVhbFdpZHRoLCBhY3R1YWxIZWlnaHQpIHtcbiAgICByZXR1cm4gcGxhY2VtZW50ID09ICdib3R0b20nID8geyB0b3A6IHBvcy50b3AgKyBwb3MuaGVpZ2h0LCAgIGxlZnQ6IHBvcy5sZWZ0ICsgcG9zLndpZHRoIC8gMiAtIGFjdHVhbFdpZHRoIC8gMiB9IDpcbiAgICAgICAgICAgcGxhY2VtZW50ID09ICd0b3AnICAgID8geyB0b3A6IHBvcy50b3AgLSBhY3R1YWxIZWlnaHQsIGxlZnQ6IHBvcy5sZWZ0ICsgcG9zLndpZHRoIC8gMiAtIGFjdHVhbFdpZHRoIC8gMiB9IDpcbiAgICAgICAgICAgcGxhY2VtZW50ID09ICdsZWZ0JyAgID8geyB0b3A6IHBvcy50b3AgKyBwb3MuaGVpZ2h0IC8gMiAtIGFjdHVhbEhlaWdodCAvIDIsIGxlZnQ6IHBvcy5sZWZ0IC0gYWN0dWFsV2lkdGggfSA6XG4gICAgICAgIC8qIHBsYWNlbWVudCA9PSAncmlnaHQnICovIHsgdG9wOiBwb3MudG9wICsgcG9zLmhlaWdodCAvIDIgLSBhY3R1YWxIZWlnaHQgLyAyLCBsZWZ0OiBwb3MubGVmdCArIHBvcy53aWR0aCB9XG5cbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLmdldFZpZXdwb3J0QWRqdXN0ZWREZWx0YSA9IGZ1bmN0aW9uIChwbGFjZW1lbnQsIHBvcywgYWN0dWFsV2lkdGgsIGFjdHVhbEhlaWdodCkge1xuICAgIHZhciBkZWx0YSA9IHsgdG9wOiAwLCBsZWZ0OiAwIH1cbiAgICBpZiAoIXRoaXMuJHZpZXdwb3J0KSByZXR1cm4gZGVsdGFcblxuICAgIHZhciB2aWV3cG9ydFBhZGRpbmcgPSB0aGlzLm9wdGlvbnMudmlld3BvcnQgJiYgdGhpcy5vcHRpb25zLnZpZXdwb3J0LnBhZGRpbmcgfHwgMFxuICAgIHZhciB2aWV3cG9ydERpbWVuc2lvbnMgPSB0aGlzLmdldFBvc2l0aW9uKHRoaXMuJHZpZXdwb3J0KVxuXG4gICAgaWYgKC9yaWdodHxsZWZ0Ly50ZXN0KHBsYWNlbWVudCkpIHtcbiAgICAgIHZhciB0b3BFZGdlT2Zmc2V0ICAgID0gcG9zLnRvcCAtIHZpZXdwb3J0UGFkZGluZyAtIHZpZXdwb3J0RGltZW5zaW9ucy5zY3JvbGxcbiAgICAgIHZhciBib3R0b21FZGdlT2Zmc2V0ID0gcG9zLnRvcCArIHZpZXdwb3J0UGFkZGluZyAtIHZpZXdwb3J0RGltZW5zaW9ucy5zY3JvbGwgKyBhY3R1YWxIZWlnaHRcbiAgICAgIGlmICh0b3BFZGdlT2Zmc2V0IDwgdmlld3BvcnREaW1lbnNpb25zLnRvcCkgeyAvLyB0b3Agb3ZlcmZsb3dcbiAgICAgICAgZGVsdGEudG9wID0gdmlld3BvcnREaW1lbnNpb25zLnRvcCAtIHRvcEVkZ2VPZmZzZXRcbiAgICAgIH0gZWxzZSBpZiAoYm90dG9tRWRnZU9mZnNldCA+IHZpZXdwb3J0RGltZW5zaW9ucy50b3AgKyB2aWV3cG9ydERpbWVuc2lvbnMuaGVpZ2h0KSB7IC8vIGJvdHRvbSBvdmVyZmxvd1xuICAgICAgICBkZWx0YS50b3AgPSB2aWV3cG9ydERpbWVuc2lvbnMudG9wICsgdmlld3BvcnREaW1lbnNpb25zLmhlaWdodCAtIGJvdHRvbUVkZ2VPZmZzZXRcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIGxlZnRFZGdlT2Zmc2V0ICA9IHBvcy5sZWZ0IC0gdmlld3BvcnRQYWRkaW5nXG4gICAgICB2YXIgcmlnaHRFZGdlT2Zmc2V0ID0gcG9zLmxlZnQgKyB2aWV3cG9ydFBhZGRpbmcgKyBhY3R1YWxXaWR0aFxuICAgICAgaWYgKGxlZnRFZGdlT2Zmc2V0IDwgdmlld3BvcnREaW1lbnNpb25zLmxlZnQpIHsgLy8gbGVmdCBvdmVyZmxvd1xuICAgICAgICBkZWx0YS5sZWZ0ID0gdmlld3BvcnREaW1lbnNpb25zLmxlZnQgLSBsZWZ0RWRnZU9mZnNldFxuICAgICAgfSBlbHNlIGlmIChyaWdodEVkZ2VPZmZzZXQgPiB2aWV3cG9ydERpbWVuc2lvbnMucmlnaHQpIHsgLy8gcmlnaHQgb3ZlcmZsb3dcbiAgICAgICAgZGVsdGEubGVmdCA9IHZpZXdwb3J0RGltZW5zaW9ucy5sZWZ0ICsgdmlld3BvcnREaW1lbnNpb25zLndpZHRoIC0gcmlnaHRFZGdlT2Zmc2V0XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGRlbHRhXG4gIH1cblxuICBUb29sdGlwLnByb3RvdHlwZS5nZXRUaXRsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdGl0bGVcbiAgICB2YXIgJGUgPSB0aGlzLiRlbGVtZW50XG4gICAgdmFyIG8gID0gdGhpcy5vcHRpb25zXG5cbiAgICB0aXRsZSA9ICRlLmF0dHIoJ2RhdGEtb3JpZ2luYWwtdGl0bGUnKVxuICAgICAgfHwgKHR5cGVvZiBvLnRpdGxlID09ICdmdW5jdGlvbicgPyBvLnRpdGxlLmNhbGwoJGVbMF0pIDogIG8udGl0bGUpXG5cbiAgICByZXR1cm4gdGl0bGVcbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLmdldFVJRCA9IGZ1bmN0aW9uIChwcmVmaXgpIHtcbiAgICBkbyBwcmVmaXggKz0gfn4oTWF0aC5yYW5kb20oKSAqIDEwMDAwMDApXG4gICAgd2hpbGUgKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHByZWZpeCkpXG4gICAgcmV0dXJuIHByZWZpeFxuICB9XG5cbiAgVG9vbHRpcC5wcm90b3R5cGUudGlwID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICghdGhpcy4kdGlwKSB7XG4gICAgICB0aGlzLiR0aXAgPSAkKHRoaXMub3B0aW9ucy50ZW1wbGF0ZSlcbiAgICAgIGlmICh0aGlzLiR0aXAubGVuZ3RoICE9IDEpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKHRoaXMudHlwZSArICcgYHRlbXBsYXRlYCBvcHRpb24gbXVzdCBjb25zaXN0IG9mIGV4YWN0bHkgMSB0b3AtbGV2ZWwgZWxlbWVudCEnKVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gdGhpcy4kdGlwXG4gIH1cblxuICBUb29sdGlwLnByb3RvdHlwZS5hcnJvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gKHRoaXMuJGFycm93ID0gdGhpcy4kYXJyb3cgfHwgdGhpcy50aXAoKS5maW5kKCcudG9vbHRpcC1hcnJvdycpKVxuICB9XG5cbiAgVG9vbHRpcC5wcm90b3R5cGUuZW5hYmxlID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuZW5hYmxlZCA9IHRydWVcbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLmRpc2FibGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5lbmFibGVkID0gZmFsc2VcbiAgfVxuXG4gIFRvb2x0aXAucHJvdG90eXBlLnRvZ2dsZUVuYWJsZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5lbmFibGVkID0gIXRoaXMuZW5hYmxlZFxuICB9XG5cbiAgVG9vbHRpcC5wcm90b3R5cGUudG9nZ2xlID0gZnVuY3Rpb24gKGUpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXNcbiAgICBpZiAoZSkge1xuICAgICAgc2VsZiA9ICQoZS5jdXJyZW50VGFyZ2V0KS5kYXRhKCdicy4nICsgdGhpcy50eXBlKVxuICAgICAgaWYgKCFzZWxmKSB7XG4gICAgICAgIHNlbGYgPSBuZXcgdGhpcy5jb25zdHJ1Y3RvcihlLmN1cnJlbnRUYXJnZXQsIHRoaXMuZ2V0RGVsZWdhdGVPcHRpb25zKCkpXG4gICAgICAgICQoZS5jdXJyZW50VGFyZ2V0KS5kYXRhKCdicy4nICsgdGhpcy50eXBlLCBzZWxmKVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChlKSB7XG4gICAgICBzZWxmLmluU3RhdGUuY2xpY2sgPSAhc2VsZi5pblN0YXRlLmNsaWNrXG4gICAgICBpZiAoc2VsZi5pc0luU3RhdGVUcnVlKCkpIHNlbGYuZW50ZXIoc2VsZilcbiAgICAgIGVsc2Ugc2VsZi5sZWF2ZShzZWxmKVxuICAgIH0gZWxzZSB7XG4gICAgICBzZWxmLnRpcCgpLmhhc0NsYXNzKCdpbicpID8gc2VsZi5sZWF2ZShzZWxmKSA6IHNlbGYuZW50ZXIoc2VsZilcbiAgICB9XG4gIH1cblxuICBUb29sdGlwLnByb3RvdHlwZS5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciB0aGF0ID0gdGhpc1xuICAgIGNsZWFyVGltZW91dCh0aGlzLnRpbWVvdXQpXG4gICAgdGhpcy5oaWRlKGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoYXQuJGVsZW1lbnQub2ZmKCcuJyArIHRoYXQudHlwZSkucmVtb3ZlRGF0YSgnYnMuJyArIHRoYXQudHlwZSlcbiAgICAgIGlmICh0aGF0LiR0aXApIHtcbiAgICAgICAgdGhhdC4kdGlwLmRldGFjaCgpXG4gICAgICB9XG4gICAgICB0aGF0LiR0aXAgPSBudWxsXG4gICAgICB0aGF0LiRhcnJvdyA9IG51bGxcbiAgICAgIHRoYXQuJHZpZXdwb3J0ID0gbnVsbFxuICAgIH0pXG4gIH1cblxuXG4gIC8vIFRPT0xUSVAgUExVR0lOIERFRklOSVRJT05cbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gIGZ1bmN0aW9uIFBsdWdpbihvcHRpb24pIHtcbiAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciAkdGhpcyAgID0gJCh0aGlzKVxuICAgICAgdmFyIGRhdGEgICAgPSAkdGhpcy5kYXRhKCdicy50b29sdGlwJylcbiAgICAgIHZhciBvcHRpb25zID0gdHlwZW9mIG9wdGlvbiA9PSAnb2JqZWN0JyAmJiBvcHRpb25cblxuICAgICAgaWYgKCFkYXRhICYmIC9kZXN0cm95fGhpZGUvLnRlc3Qob3B0aW9uKSkgcmV0dXJuXG4gICAgICBpZiAoIWRhdGEpICR0aGlzLmRhdGEoJ2JzLnRvb2x0aXAnLCAoZGF0YSA9IG5ldyBUb29sdGlwKHRoaXMsIG9wdGlvbnMpKSlcbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9uID09ICdzdHJpbmcnKSBkYXRhW29wdGlvbl0oKVxuICAgIH0pXG4gIH1cblxuICB2YXIgb2xkID0gJC5mbi50b29sdGlwXG5cbiAgJC5mbi50b29sdGlwICAgICAgICAgICAgID0gUGx1Z2luXG4gICQuZm4udG9vbHRpcC5Db25zdHJ1Y3RvciA9IFRvb2x0aXBcblxuXG4gIC8vIFRPT0xUSVAgTk8gQ09ORkxJQ1RcbiAgLy8gPT09PT09PT09PT09PT09PT09PVxuXG4gICQuZm4udG9vbHRpcC5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xuICAgICQuZm4udG9vbHRpcCA9IG9sZFxuICAgIHJldHVybiB0aGlzXG4gIH1cblxufShqUXVlcnkpO1xuXG4vKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqIEJvb3RzdHJhcDogcG9wb3Zlci5qcyB2My4zLjZcbiAqIGh0dHA6Ly9nZXRib290c3RyYXAuY29tL2phdmFzY3JpcHQvI3BvcG92ZXJzXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqIENvcHlyaWdodCAyMDExLTIwMTUgVHdpdHRlciwgSW5jLlxuICogTGljZW5zZWQgdW5kZXIgTUlUIChodHRwczovL2dpdGh1Yi5jb20vdHdicy9ib290c3RyYXAvYmxvYi9tYXN0ZXIvTElDRU5TRSlcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG5cbitmdW5jdGlvbiAoJCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgLy8gUE9QT1ZFUiBQVUJMSUMgQ0xBU1MgREVGSU5JVElPTlxuICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgdmFyIFBvcG92ZXIgPSBmdW5jdGlvbiAoZWxlbWVudCwgb3B0aW9ucykge1xuICAgIHRoaXMuaW5pdCgncG9wb3ZlcicsIGVsZW1lbnQsIG9wdGlvbnMpXG4gIH1cblxuICBpZiAoISQuZm4udG9vbHRpcCkgdGhyb3cgbmV3IEVycm9yKCdQb3BvdmVyIHJlcXVpcmVzIHRvb2x0aXAuanMnKVxuXG4gIFBvcG92ZXIuVkVSU0lPTiAgPSAnMy4zLjYnXG5cbiAgUG9wb3Zlci5ERUZBVUxUUyA9ICQuZXh0ZW5kKHt9LCAkLmZuLnRvb2x0aXAuQ29uc3RydWN0b3IuREVGQVVMVFMsIHtcbiAgICBwbGFjZW1lbnQ6ICdyaWdodCcsXG4gICAgdHJpZ2dlcjogJ2NsaWNrJyxcbiAgICBjb250ZW50OiAnJyxcbiAgICB0ZW1wbGF0ZTogJzxkaXYgY2xhc3M9XCJwb3BvdmVyXCIgcm9sZT1cInRvb2x0aXBcIj48ZGl2IGNsYXNzPVwiYXJyb3dcIj48L2Rpdj48aDMgY2xhc3M9XCJwb3BvdmVyLXRpdGxlXCI+PC9oMz48ZGl2IGNsYXNzPVwicG9wb3Zlci1jb250ZW50XCI+PC9kaXY+PC9kaXY+J1xuICB9KVxuXG5cbiAgLy8gTk9URTogUE9QT1ZFUiBFWFRFTkRTIHRvb2x0aXAuanNcbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICBQb3BvdmVyLnByb3RvdHlwZSA9ICQuZXh0ZW5kKHt9LCAkLmZuLnRvb2x0aXAuQ29uc3RydWN0b3IucHJvdG90eXBlKVxuXG4gIFBvcG92ZXIucHJvdG90eXBlLmNvbnN0cnVjdG9yID0gUG9wb3ZlclxuXG4gIFBvcG92ZXIucHJvdG90eXBlLmdldERlZmF1bHRzID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBQb3BvdmVyLkRFRkFVTFRTXG4gIH1cblxuICBQb3BvdmVyLnByb3RvdHlwZS5zZXRDb250ZW50ID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciAkdGlwICAgID0gdGhpcy50aXAoKVxuICAgIHZhciB0aXRsZSAgID0gdGhpcy5nZXRUaXRsZSgpXG4gICAgdmFyIGNvbnRlbnQgPSB0aGlzLmdldENvbnRlbnQoKVxuXG4gICAgJHRpcC5maW5kKCcucG9wb3Zlci10aXRsZScpW3RoaXMub3B0aW9ucy5odG1sID8gJ2h0bWwnIDogJ3RleHQnXSh0aXRsZSlcbiAgICAkdGlwLmZpbmQoJy5wb3BvdmVyLWNvbnRlbnQnKS5jaGlsZHJlbigpLmRldGFjaCgpLmVuZCgpWyAvLyB3ZSB1c2UgYXBwZW5kIGZvciBodG1sIG9iamVjdHMgdG8gbWFpbnRhaW4ganMgZXZlbnRzXG4gICAgICB0aGlzLm9wdGlvbnMuaHRtbCA/ICh0eXBlb2YgY29udGVudCA9PSAnc3RyaW5nJyA/ICdodG1sJyA6ICdhcHBlbmQnKSA6ICd0ZXh0J1xuICAgIF0oY29udGVudClcblxuICAgICR0aXAucmVtb3ZlQ2xhc3MoJ2ZhZGUgdG9wIGJvdHRvbSBsZWZ0IHJpZ2h0IGluJylcblxuICAgIC8vIElFOCBkb2Vzbid0IGFjY2VwdCBoaWRpbmcgdmlhIHRoZSBgOmVtcHR5YCBwc2V1ZG8gc2VsZWN0b3IsIHdlIGhhdmUgdG8gZG9cbiAgICAvLyB0aGlzIG1hbnVhbGx5IGJ5IGNoZWNraW5nIHRoZSBjb250ZW50cy5cbiAgICBpZiAoISR0aXAuZmluZCgnLnBvcG92ZXItdGl0bGUnKS5odG1sKCkpICR0aXAuZmluZCgnLnBvcG92ZXItdGl0bGUnKS5oaWRlKClcbiAgfVxuXG4gIFBvcG92ZXIucHJvdG90eXBlLmhhc0NvbnRlbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0VGl0bGUoKSB8fCB0aGlzLmdldENvbnRlbnQoKVxuICB9XG5cbiAgUG9wb3Zlci5wcm90b3R5cGUuZ2V0Q29udGVudCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgJGUgPSB0aGlzLiRlbGVtZW50XG4gICAgdmFyIG8gID0gdGhpcy5vcHRpb25zXG5cbiAgICByZXR1cm4gJGUuYXR0cignZGF0YS1jb250ZW50JylcbiAgICAgIHx8ICh0eXBlb2Ygby5jb250ZW50ID09ICdmdW5jdGlvbicgP1xuICAgICAgICAgICAgby5jb250ZW50LmNhbGwoJGVbMF0pIDpcbiAgICAgICAgICAgIG8uY29udGVudClcbiAgfVxuXG4gIFBvcG92ZXIucHJvdG90eXBlLmFycm93ID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiAodGhpcy4kYXJyb3cgPSB0aGlzLiRhcnJvdyB8fCB0aGlzLnRpcCgpLmZpbmQoJy5hcnJvdycpKVxuICB9XG5cblxuICAvLyBQT1BPVkVSIFBMVUdJTiBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICBmdW5jdGlvbiBQbHVnaW4ob3B0aW9uKSB7XG4gICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgJHRoaXMgICA9ICQodGhpcylcbiAgICAgIHZhciBkYXRhICAgID0gJHRoaXMuZGF0YSgnYnMucG9wb3ZlcicpXG4gICAgICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBvcHRpb24gPT0gJ29iamVjdCcgJiYgb3B0aW9uXG5cbiAgICAgIGlmICghZGF0YSAmJiAvZGVzdHJveXxoaWRlLy50ZXN0KG9wdGlvbikpIHJldHVyblxuICAgICAgaWYgKCFkYXRhKSAkdGhpcy5kYXRhKCdicy5wb3BvdmVyJywgKGRhdGEgPSBuZXcgUG9wb3Zlcih0aGlzLCBvcHRpb25zKSkpXG4gICAgICBpZiAodHlwZW9mIG9wdGlvbiA9PSAnc3RyaW5nJykgZGF0YVtvcHRpb25dKClcbiAgICB9KVxuICB9XG5cbiAgdmFyIG9sZCA9ICQuZm4ucG9wb3ZlclxuXG4gICQuZm4ucG9wb3ZlciAgICAgICAgICAgICA9IFBsdWdpblxuICAkLmZuLnBvcG92ZXIuQ29uc3RydWN0b3IgPSBQb3BvdmVyXG5cblxuICAvLyBQT1BPVkVSIE5PIENPTkZMSUNUXG4gIC8vID09PT09PT09PT09PT09PT09PT1cblxuICAkLmZuLnBvcG92ZXIubm9Db25mbGljdCA9IGZ1bmN0aW9uICgpIHtcbiAgICAkLmZuLnBvcG92ZXIgPSBvbGRcbiAgICByZXR1cm4gdGhpc1xuICB9XG5cbn0oalF1ZXJ5KTtcblxuLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBCb290c3RyYXA6IHNjcm9sbHNweS5qcyB2My4zLjZcbiAqIGh0dHA6Ly9nZXRib290c3RyYXAuY29tL2phdmFzY3JpcHQvI3Njcm9sbHNweVxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBDb3B5cmlnaHQgMjAxMS0yMDE1IFR3aXR0ZXIsIEluYy5cbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0UpXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuXG4rZnVuY3Rpb24gKCQpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIC8vIFNDUk9MTFNQWSBDTEFTUyBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgZnVuY3Rpb24gU2Nyb2xsU3B5KGVsZW1lbnQsIG9wdGlvbnMpIHtcbiAgICB0aGlzLiRib2R5ICAgICAgICAgID0gJChkb2N1bWVudC5ib2R5KVxuICAgIHRoaXMuJHNjcm9sbEVsZW1lbnQgPSAkKGVsZW1lbnQpLmlzKGRvY3VtZW50LmJvZHkpID8gJCh3aW5kb3cpIDogJChlbGVtZW50KVxuICAgIHRoaXMub3B0aW9ucyAgICAgICAgPSAkLmV4dGVuZCh7fSwgU2Nyb2xsU3B5LkRFRkFVTFRTLCBvcHRpb25zKVxuICAgIHRoaXMuc2VsZWN0b3IgICAgICAgPSAodGhpcy5vcHRpb25zLnRhcmdldCB8fCAnJykgKyAnIC5uYXYgbGkgPiBhJ1xuICAgIHRoaXMub2Zmc2V0cyAgICAgICAgPSBbXVxuICAgIHRoaXMudGFyZ2V0cyAgICAgICAgPSBbXVxuICAgIHRoaXMuYWN0aXZlVGFyZ2V0ICAgPSBudWxsXG4gICAgdGhpcy5zY3JvbGxIZWlnaHQgICA9IDBcblxuICAgIHRoaXMuJHNjcm9sbEVsZW1lbnQub24oJ3Njcm9sbC5icy5zY3JvbGxzcHknLCAkLnByb3h5KHRoaXMucHJvY2VzcywgdGhpcykpXG4gICAgdGhpcy5yZWZyZXNoKClcbiAgICB0aGlzLnByb2Nlc3MoKVxuICB9XG5cbiAgU2Nyb2xsU3B5LlZFUlNJT04gID0gJzMuMy42J1xuXG4gIFNjcm9sbFNweS5ERUZBVUxUUyA9IHtcbiAgICBvZmZzZXQ6IDEwXG4gIH1cblxuICBTY3JvbGxTcHkucHJvdG90eXBlLmdldFNjcm9sbEhlaWdodCA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy4kc2Nyb2xsRWxlbWVudFswXS5zY3JvbGxIZWlnaHQgfHwgTWF0aC5tYXgodGhpcy4kYm9keVswXS5zY3JvbGxIZWlnaHQsIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxIZWlnaHQpXG4gIH1cblxuICBTY3JvbGxTcHkucHJvdG90eXBlLnJlZnJlc2ggPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHRoYXQgICAgICAgICAgPSB0aGlzXG4gICAgdmFyIG9mZnNldE1ldGhvZCAgPSAnb2Zmc2V0J1xuICAgIHZhciBvZmZzZXRCYXNlICAgID0gMFxuXG4gICAgdGhpcy5vZmZzZXRzICAgICAgPSBbXVxuICAgIHRoaXMudGFyZ2V0cyAgICAgID0gW11cbiAgICB0aGlzLnNjcm9sbEhlaWdodCA9IHRoaXMuZ2V0U2Nyb2xsSGVpZ2h0KClcblxuICAgIGlmICghJC5pc1dpbmRvdyh0aGlzLiRzY3JvbGxFbGVtZW50WzBdKSkge1xuICAgICAgb2Zmc2V0TWV0aG9kID0gJ3Bvc2l0aW9uJ1xuICAgICAgb2Zmc2V0QmFzZSAgID0gdGhpcy4kc2Nyb2xsRWxlbWVudC5zY3JvbGxUb3AoKVxuICAgIH1cblxuICAgIHRoaXMuJGJvZHlcbiAgICAgIC5maW5kKHRoaXMuc2VsZWN0b3IpXG4gICAgICAubWFwKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyICRlbCAgID0gJCh0aGlzKVxuICAgICAgICB2YXIgaHJlZiAgPSAkZWwuZGF0YSgndGFyZ2V0JykgfHwgJGVsLmF0dHIoJ2hyZWYnKVxuICAgICAgICB2YXIgJGhyZWYgPSAvXiMuLy50ZXN0KGhyZWYpICYmICQoaHJlZilcblxuICAgICAgICByZXR1cm4gKCRocmVmXG4gICAgICAgICAgJiYgJGhyZWYubGVuZ3RoXG4gICAgICAgICAgJiYgJGhyZWYuaXMoJzp2aXNpYmxlJylcbiAgICAgICAgICAmJiBbWyRocmVmW29mZnNldE1ldGhvZF0oKS50b3AgKyBvZmZzZXRCYXNlLCBocmVmXV0pIHx8IG51bGxcbiAgICAgIH0pXG4gICAgICAuc29ydChmdW5jdGlvbiAoYSwgYikgeyByZXR1cm4gYVswXSAtIGJbMF0gfSlcbiAgICAgIC5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhhdC5vZmZzZXRzLnB1c2godGhpc1swXSlcbiAgICAgICAgdGhhdC50YXJnZXRzLnB1c2godGhpc1sxXSlcbiAgICAgIH0pXG4gIH1cblxuICBTY3JvbGxTcHkucHJvdG90eXBlLnByb2Nlc3MgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHNjcm9sbFRvcCAgICA9IHRoaXMuJHNjcm9sbEVsZW1lbnQuc2Nyb2xsVG9wKCkgKyB0aGlzLm9wdGlvbnMub2Zmc2V0XG4gICAgdmFyIHNjcm9sbEhlaWdodCA9IHRoaXMuZ2V0U2Nyb2xsSGVpZ2h0KClcbiAgICB2YXIgbWF4U2Nyb2xsICAgID0gdGhpcy5vcHRpb25zLm9mZnNldCArIHNjcm9sbEhlaWdodCAtIHRoaXMuJHNjcm9sbEVsZW1lbnQuaGVpZ2h0KClcbiAgICB2YXIgb2Zmc2V0cyAgICAgID0gdGhpcy5vZmZzZXRzXG4gICAgdmFyIHRhcmdldHMgICAgICA9IHRoaXMudGFyZ2V0c1xuICAgIHZhciBhY3RpdmVUYXJnZXQgPSB0aGlzLmFjdGl2ZVRhcmdldFxuICAgIHZhciBpXG5cbiAgICBpZiAodGhpcy5zY3JvbGxIZWlnaHQgIT0gc2Nyb2xsSGVpZ2h0KSB7XG4gICAgICB0aGlzLnJlZnJlc2goKVxuICAgIH1cblxuICAgIGlmIChzY3JvbGxUb3AgPj0gbWF4U2Nyb2xsKSB7XG4gICAgICByZXR1cm4gYWN0aXZlVGFyZ2V0ICE9IChpID0gdGFyZ2V0c1t0YXJnZXRzLmxlbmd0aCAtIDFdKSAmJiB0aGlzLmFjdGl2YXRlKGkpXG4gICAgfVxuXG4gICAgaWYgKGFjdGl2ZVRhcmdldCAmJiBzY3JvbGxUb3AgPCBvZmZzZXRzWzBdKSB7XG4gICAgICB0aGlzLmFjdGl2ZVRhcmdldCA9IG51bGxcbiAgICAgIHJldHVybiB0aGlzLmNsZWFyKClcbiAgICB9XG5cbiAgICBmb3IgKGkgPSBvZmZzZXRzLmxlbmd0aDsgaS0tOykge1xuICAgICAgYWN0aXZlVGFyZ2V0ICE9IHRhcmdldHNbaV1cbiAgICAgICAgJiYgc2Nyb2xsVG9wID49IG9mZnNldHNbaV1cbiAgICAgICAgJiYgKG9mZnNldHNbaSArIDFdID09PSB1bmRlZmluZWQgfHwgc2Nyb2xsVG9wIDwgb2Zmc2V0c1tpICsgMV0pXG4gICAgICAgICYmIHRoaXMuYWN0aXZhdGUodGFyZ2V0c1tpXSlcbiAgICB9XG4gIH1cblxuICBTY3JvbGxTcHkucHJvdG90eXBlLmFjdGl2YXRlID0gZnVuY3Rpb24gKHRhcmdldCkge1xuICAgIHRoaXMuYWN0aXZlVGFyZ2V0ID0gdGFyZ2V0XG5cbiAgICB0aGlzLmNsZWFyKClcblxuICAgIHZhciBzZWxlY3RvciA9IHRoaXMuc2VsZWN0b3IgK1xuICAgICAgJ1tkYXRhLXRhcmdldD1cIicgKyB0YXJnZXQgKyAnXCJdLCcgK1xuICAgICAgdGhpcy5zZWxlY3RvciArICdbaHJlZj1cIicgKyB0YXJnZXQgKyAnXCJdJ1xuXG4gICAgdmFyIGFjdGl2ZSA9ICQoc2VsZWN0b3IpXG4gICAgICAucGFyZW50cygnbGknKVxuICAgICAgLmFkZENsYXNzKCdhY3RpdmUnKVxuXG4gICAgaWYgKGFjdGl2ZS5wYXJlbnQoJy5kcm9wZG93bi1tZW51JykubGVuZ3RoKSB7XG4gICAgICBhY3RpdmUgPSBhY3RpdmVcbiAgICAgICAgLmNsb3Nlc3QoJ2xpLmRyb3Bkb3duJylcbiAgICAgICAgLmFkZENsYXNzKCdhY3RpdmUnKVxuICAgIH1cblxuICAgIGFjdGl2ZS50cmlnZ2VyKCdhY3RpdmF0ZS5icy5zY3JvbGxzcHknKVxuICB9XG5cbiAgU2Nyb2xsU3B5LnByb3RvdHlwZS5jbGVhciA9IGZ1bmN0aW9uICgpIHtcbiAgICAkKHRoaXMuc2VsZWN0b3IpXG4gICAgICAucGFyZW50c1VudGlsKHRoaXMub3B0aW9ucy50YXJnZXQsICcuYWN0aXZlJylcbiAgICAgIC5yZW1vdmVDbGFzcygnYWN0aXZlJylcbiAgfVxuXG5cbiAgLy8gU0NST0xMU1BZIFBMVUdJTiBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gIGZ1bmN0aW9uIFBsdWdpbihvcHRpb24pIHtcbiAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciAkdGhpcyAgID0gJCh0aGlzKVxuICAgICAgdmFyIGRhdGEgICAgPSAkdGhpcy5kYXRhKCdicy5zY3JvbGxzcHknKVxuICAgICAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygb3B0aW9uID09ICdvYmplY3QnICYmIG9wdGlvblxuXG4gICAgICBpZiAoIWRhdGEpICR0aGlzLmRhdGEoJ2JzLnNjcm9sbHNweScsIChkYXRhID0gbmV3IFNjcm9sbFNweSh0aGlzLCBvcHRpb25zKSkpXG4gICAgICBpZiAodHlwZW9mIG9wdGlvbiA9PSAnc3RyaW5nJykgZGF0YVtvcHRpb25dKClcbiAgICB9KVxuICB9XG5cbiAgdmFyIG9sZCA9ICQuZm4uc2Nyb2xsc3B5XG5cbiAgJC5mbi5zY3JvbGxzcHkgICAgICAgICAgICAgPSBQbHVnaW5cbiAgJC5mbi5zY3JvbGxzcHkuQ29uc3RydWN0b3IgPSBTY3JvbGxTcHlcblxuXG4gIC8vIFNDUk9MTFNQWSBOTyBDT05GTElDVFxuICAvLyA9PT09PT09PT09PT09PT09PT09PT1cblxuICAkLmZuLnNjcm9sbHNweS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xuICAgICQuZm4uc2Nyb2xsc3B5ID0gb2xkXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG5cbiAgLy8gU0NST0xMU1BZIERBVEEtQVBJXG4gIC8vID09PT09PT09PT09PT09PT09PVxuXG4gICQod2luZG93KS5vbignbG9hZC5icy5zY3JvbGxzcHkuZGF0YS1hcGknLCBmdW5jdGlvbiAoKSB7XG4gICAgJCgnW2RhdGEtc3B5PVwic2Nyb2xsXCJdJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgJHNweSA9ICQodGhpcylcbiAgICAgIFBsdWdpbi5jYWxsKCRzcHksICRzcHkuZGF0YSgpKVxuICAgIH0pXG4gIH0pXG5cbn0oalF1ZXJ5KTtcblxuLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBCb290c3RyYXA6IHRhYi5qcyB2My4zLjZcbiAqIGh0dHA6Ly9nZXRib290c3RyYXAuY29tL2phdmFzY3JpcHQvI3RhYnNcbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogQ29weXJpZ2h0IDIwMTEtMjAxNSBUd2l0dGVyLCBJbmMuXG4gKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cblxuK2Z1bmN0aW9uICgkKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICAvLyBUQUIgQ0xBU1MgREVGSU5JVElPTlxuICAvLyA9PT09PT09PT09PT09PT09PT09PVxuXG4gIHZhciBUYWIgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgIC8vIGpzY3M6ZGlzYWJsZSByZXF1aXJlRG9sbGFyQmVmb3JlalF1ZXJ5QXNzaWdubWVudFxuICAgIHRoaXMuZWxlbWVudCA9ICQoZWxlbWVudClcbiAgICAvLyBqc2NzOmVuYWJsZSByZXF1aXJlRG9sbGFyQmVmb3JlalF1ZXJ5QXNzaWdubWVudFxuICB9XG5cbiAgVGFiLlZFUlNJT04gPSAnMy4zLjYnXG5cbiAgVGFiLlRSQU5TSVRJT05fRFVSQVRJT04gPSAxNTBcblxuICBUYWIucHJvdG90eXBlLnNob3cgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyICR0aGlzICAgID0gdGhpcy5lbGVtZW50XG4gICAgdmFyICR1bCAgICAgID0gJHRoaXMuY2xvc2VzdCgndWw6bm90KC5kcm9wZG93bi1tZW51KScpXG4gICAgdmFyIHNlbGVjdG9yID0gJHRoaXMuZGF0YSgndGFyZ2V0JylcblxuICAgIGlmICghc2VsZWN0b3IpIHtcbiAgICAgIHNlbGVjdG9yID0gJHRoaXMuYXR0cignaHJlZicpXG4gICAgICBzZWxlY3RvciA9IHNlbGVjdG9yICYmIHNlbGVjdG9yLnJlcGxhY2UoLy4qKD89I1teXFxzXSokKS8sICcnKSAvLyBzdHJpcCBmb3IgaWU3XG4gICAgfVxuXG4gICAgaWYgKCR0aGlzLnBhcmVudCgnbGknKS5oYXNDbGFzcygnYWN0aXZlJykpIHJldHVyblxuXG4gICAgdmFyICRwcmV2aW91cyA9ICR1bC5maW5kKCcuYWN0aXZlOmxhc3QgYScpXG4gICAgdmFyIGhpZGVFdmVudCA9ICQuRXZlbnQoJ2hpZGUuYnMudGFiJywge1xuICAgICAgcmVsYXRlZFRhcmdldDogJHRoaXNbMF1cbiAgICB9KVxuICAgIHZhciBzaG93RXZlbnQgPSAkLkV2ZW50KCdzaG93LmJzLnRhYicsIHtcbiAgICAgIHJlbGF0ZWRUYXJnZXQ6ICRwcmV2aW91c1swXVxuICAgIH0pXG5cbiAgICAkcHJldmlvdXMudHJpZ2dlcihoaWRlRXZlbnQpXG4gICAgJHRoaXMudHJpZ2dlcihzaG93RXZlbnQpXG5cbiAgICBpZiAoc2hvd0V2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpIHx8IGhpZGVFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkgcmV0dXJuXG5cbiAgICB2YXIgJHRhcmdldCA9ICQoc2VsZWN0b3IpXG5cbiAgICB0aGlzLmFjdGl2YXRlKCR0aGlzLmNsb3Nlc3QoJ2xpJyksICR1bClcbiAgICB0aGlzLmFjdGl2YXRlKCR0YXJnZXQsICR0YXJnZXQucGFyZW50KCksIGZ1bmN0aW9uICgpIHtcbiAgICAgICRwcmV2aW91cy50cmlnZ2VyKHtcbiAgICAgICAgdHlwZTogJ2hpZGRlbi5icy50YWInLFxuICAgICAgICByZWxhdGVkVGFyZ2V0OiAkdGhpc1swXVxuICAgICAgfSlcbiAgICAgICR0aGlzLnRyaWdnZXIoe1xuICAgICAgICB0eXBlOiAnc2hvd24uYnMudGFiJyxcbiAgICAgICAgcmVsYXRlZFRhcmdldDogJHByZXZpb3VzWzBdXG4gICAgICB9KVxuICAgIH0pXG4gIH1cblxuICBUYWIucHJvdG90eXBlLmFjdGl2YXRlID0gZnVuY3Rpb24gKGVsZW1lbnQsIGNvbnRhaW5lciwgY2FsbGJhY2spIHtcbiAgICB2YXIgJGFjdGl2ZSAgICA9IGNvbnRhaW5lci5maW5kKCc+IC5hY3RpdmUnKVxuICAgIHZhciB0cmFuc2l0aW9uID0gY2FsbGJhY2tcbiAgICAgICYmICQuc3VwcG9ydC50cmFuc2l0aW9uXG4gICAgICAmJiAoJGFjdGl2ZS5sZW5ndGggJiYgJGFjdGl2ZS5oYXNDbGFzcygnZmFkZScpIHx8ICEhY29udGFpbmVyLmZpbmQoJz4gLmZhZGUnKS5sZW5ndGgpXG5cbiAgICBmdW5jdGlvbiBuZXh0KCkge1xuICAgICAgJGFjdGl2ZVxuICAgICAgICAucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgIC5maW5kKCc+IC5kcm9wZG93bi1tZW51ID4gLmFjdGl2ZScpXG4gICAgICAgICAgLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxuICAgICAgICAuZW5kKClcbiAgICAgICAgLmZpbmQoJ1tkYXRhLXRvZ2dsZT1cInRhYlwiXScpXG4gICAgICAgICAgLmF0dHIoJ2FyaWEtZXhwYW5kZWQnLCBmYWxzZSlcblxuICAgICAgZWxlbWVudFxuICAgICAgICAuYWRkQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgIC5maW5kKCdbZGF0YS10b2dnbGU9XCJ0YWJcIl0nKVxuICAgICAgICAgIC5hdHRyKCdhcmlhLWV4cGFuZGVkJywgdHJ1ZSlcblxuICAgICAgaWYgKHRyYW5zaXRpb24pIHtcbiAgICAgICAgZWxlbWVudFswXS5vZmZzZXRXaWR0aCAvLyByZWZsb3cgZm9yIHRyYW5zaXRpb25cbiAgICAgICAgZWxlbWVudC5hZGRDbGFzcygnaW4nKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZWxlbWVudC5yZW1vdmVDbGFzcygnZmFkZScpXG4gICAgICB9XG5cbiAgICAgIGlmIChlbGVtZW50LnBhcmVudCgnLmRyb3Bkb3duLW1lbnUnKS5sZW5ndGgpIHtcbiAgICAgICAgZWxlbWVudFxuICAgICAgICAgIC5jbG9zZXN0KCdsaS5kcm9wZG93bicpXG4gICAgICAgICAgICAuYWRkQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgICAgLmVuZCgpXG4gICAgICAgICAgLmZpbmQoJ1tkYXRhLXRvZ2dsZT1cInRhYlwiXScpXG4gICAgICAgICAgICAuYXR0cignYXJpYS1leHBhbmRlZCcsIHRydWUpXG4gICAgICB9XG5cbiAgICAgIGNhbGxiYWNrICYmIGNhbGxiYWNrKClcbiAgICB9XG5cbiAgICAkYWN0aXZlLmxlbmd0aCAmJiB0cmFuc2l0aW9uID9cbiAgICAgICRhY3RpdmVcbiAgICAgICAgLm9uZSgnYnNUcmFuc2l0aW9uRW5kJywgbmV4dClcbiAgICAgICAgLmVtdWxhdGVUcmFuc2l0aW9uRW5kKFRhYi5UUkFOU0lUSU9OX0RVUkFUSU9OKSA6XG4gICAgICBuZXh0KClcblxuICAgICRhY3RpdmUucmVtb3ZlQ2xhc3MoJ2luJylcbiAgfVxuXG5cbiAgLy8gVEFCIFBMVUdJTiBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PVxuXG4gIGZ1bmN0aW9uIFBsdWdpbihvcHRpb24pIHtcbiAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciAkdGhpcyA9ICQodGhpcylcbiAgICAgIHZhciBkYXRhICA9ICR0aGlzLmRhdGEoJ2JzLnRhYicpXG5cbiAgICAgIGlmICghZGF0YSkgJHRoaXMuZGF0YSgnYnMudGFiJywgKGRhdGEgPSBuZXcgVGFiKHRoaXMpKSlcbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9uID09ICdzdHJpbmcnKSBkYXRhW29wdGlvbl0oKVxuICAgIH0pXG4gIH1cblxuICB2YXIgb2xkID0gJC5mbi50YWJcblxuICAkLmZuLnRhYiAgICAgICAgICAgICA9IFBsdWdpblxuICAkLmZuLnRhYi5Db25zdHJ1Y3RvciA9IFRhYlxuXG5cbiAgLy8gVEFCIE5PIENPTkZMSUNUXG4gIC8vID09PT09PT09PT09PT09PVxuXG4gICQuZm4udGFiLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XG4gICAgJC5mbi50YWIgPSBvbGRcbiAgICByZXR1cm4gdGhpc1xuICB9XG5cblxuICAvLyBUQUIgREFUQS1BUElcbiAgLy8gPT09PT09PT09PT09XG5cbiAgdmFyIGNsaWNrSGFuZGxlciA9IGZ1bmN0aW9uIChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgUGx1Z2luLmNhbGwoJCh0aGlzKSwgJ3Nob3cnKVxuICB9XG5cbiAgJChkb2N1bWVudClcbiAgICAub24oJ2NsaWNrLmJzLnRhYi5kYXRhLWFwaScsICdbZGF0YS10b2dnbGU9XCJ0YWJcIl0nLCBjbGlja0hhbmRsZXIpXG4gICAgLm9uKCdjbGljay5icy50YWIuZGF0YS1hcGknLCAnW2RhdGEtdG9nZ2xlPVwicGlsbFwiXScsIGNsaWNrSGFuZGxlcilcblxufShqUXVlcnkpO1xuXG4vKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqIEJvb3RzdHJhcDogYWZmaXguanMgdjMuMy42XG4gKiBodHRwOi8vZ2V0Ym9vdHN0cmFwLmNvbS9qYXZhc2NyaXB0LyNhZmZpeFxuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBDb3B5cmlnaHQgMjAxMS0yMDE1IFR3aXR0ZXIsIEluYy5cbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0UpXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuXG4rZnVuY3Rpb24gKCQpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIC8vIEFGRklYIENMQVNTIERFRklOSVRJT05cbiAgLy8gPT09PT09PT09PT09PT09PT09PT09PVxuXG4gIHZhciBBZmZpeCA9IGZ1bmN0aW9uIChlbGVtZW50LCBvcHRpb25zKSB7XG4gICAgdGhpcy5vcHRpb25zID0gJC5leHRlbmQoe30sIEFmZml4LkRFRkFVTFRTLCBvcHRpb25zKVxuXG4gICAgdGhpcy4kdGFyZ2V0ID0gJCh0aGlzLm9wdGlvbnMudGFyZ2V0KVxuICAgICAgLm9uKCdzY3JvbGwuYnMuYWZmaXguZGF0YS1hcGknLCAkLnByb3h5KHRoaXMuY2hlY2tQb3NpdGlvbiwgdGhpcykpXG4gICAgICAub24oJ2NsaWNrLmJzLmFmZml4LmRhdGEtYXBpJywgICQucHJveHkodGhpcy5jaGVja1Bvc2l0aW9uV2l0aEV2ZW50TG9vcCwgdGhpcykpXG5cbiAgICB0aGlzLiRlbGVtZW50ICAgICA9ICQoZWxlbWVudClcbiAgICB0aGlzLmFmZml4ZWQgICAgICA9IG51bGxcbiAgICB0aGlzLnVucGluICAgICAgICA9IG51bGxcbiAgICB0aGlzLnBpbm5lZE9mZnNldCA9IG51bGxcblxuICAgIHRoaXMuY2hlY2tQb3NpdGlvbigpXG4gIH1cblxuICBBZmZpeC5WRVJTSU9OICA9ICczLjMuNidcblxuICBBZmZpeC5SRVNFVCAgICA9ICdhZmZpeCBhZmZpeC10b3AgYWZmaXgtYm90dG9tJ1xuXG4gIEFmZml4LkRFRkFVTFRTID0ge1xuICAgIG9mZnNldDogMCxcbiAgICB0YXJnZXQ6IHdpbmRvd1xuICB9XG5cbiAgQWZmaXgucHJvdG90eXBlLmdldFN0YXRlID0gZnVuY3Rpb24gKHNjcm9sbEhlaWdodCwgaGVpZ2h0LCBvZmZzZXRUb3AsIG9mZnNldEJvdHRvbSkge1xuICAgIHZhciBzY3JvbGxUb3AgICAgPSB0aGlzLiR0YXJnZXQuc2Nyb2xsVG9wKClcbiAgICB2YXIgcG9zaXRpb24gICAgID0gdGhpcy4kZWxlbWVudC5vZmZzZXQoKVxuICAgIHZhciB0YXJnZXRIZWlnaHQgPSB0aGlzLiR0YXJnZXQuaGVpZ2h0KClcblxuICAgIGlmIChvZmZzZXRUb3AgIT0gbnVsbCAmJiB0aGlzLmFmZml4ZWQgPT0gJ3RvcCcpIHJldHVybiBzY3JvbGxUb3AgPCBvZmZzZXRUb3AgPyAndG9wJyA6IGZhbHNlXG5cbiAgICBpZiAodGhpcy5hZmZpeGVkID09ICdib3R0b20nKSB7XG4gICAgICBpZiAob2Zmc2V0VG9wICE9IG51bGwpIHJldHVybiAoc2Nyb2xsVG9wICsgdGhpcy51bnBpbiA8PSBwb3NpdGlvbi50b3ApID8gZmFsc2UgOiAnYm90dG9tJ1xuICAgICAgcmV0dXJuIChzY3JvbGxUb3AgKyB0YXJnZXRIZWlnaHQgPD0gc2Nyb2xsSGVpZ2h0IC0gb2Zmc2V0Qm90dG9tKSA/IGZhbHNlIDogJ2JvdHRvbSdcbiAgICB9XG5cbiAgICB2YXIgaW5pdGlhbGl6aW5nICAgPSB0aGlzLmFmZml4ZWQgPT0gbnVsbFxuICAgIHZhciBjb2xsaWRlclRvcCAgICA9IGluaXRpYWxpemluZyA/IHNjcm9sbFRvcCA6IHBvc2l0aW9uLnRvcFxuICAgIHZhciBjb2xsaWRlckhlaWdodCA9IGluaXRpYWxpemluZyA/IHRhcmdldEhlaWdodCA6IGhlaWdodFxuXG4gICAgaWYgKG9mZnNldFRvcCAhPSBudWxsICYmIHNjcm9sbFRvcCA8PSBvZmZzZXRUb3ApIHJldHVybiAndG9wJ1xuICAgIGlmIChvZmZzZXRCb3R0b20gIT0gbnVsbCAmJiAoY29sbGlkZXJUb3AgKyBjb2xsaWRlckhlaWdodCA+PSBzY3JvbGxIZWlnaHQgLSBvZmZzZXRCb3R0b20pKSByZXR1cm4gJ2JvdHRvbSdcblxuICAgIHJldHVybiBmYWxzZVxuICB9XG5cbiAgQWZmaXgucHJvdG90eXBlLmdldFBpbm5lZE9mZnNldCA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAodGhpcy5waW5uZWRPZmZzZXQpIHJldHVybiB0aGlzLnBpbm5lZE9mZnNldFxuICAgIHRoaXMuJGVsZW1lbnQucmVtb3ZlQ2xhc3MoQWZmaXguUkVTRVQpLmFkZENsYXNzKCdhZmZpeCcpXG4gICAgdmFyIHNjcm9sbFRvcCA9IHRoaXMuJHRhcmdldC5zY3JvbGxUb3AoKVxuICAgIHZhciBwb3NpdGlvbiAgPSB0aGlzLiRlbGVtZW50Lm9mZnNldCgpXG4gICAgcmV0dXJuICh0aGlzLnBpbm5lZE9mZnNldCA9IHBvc2l0aW9uLnRvcCAtIHNjcm9sbFRvcClcbiAgfVxuXG4gIEFmZml4LnByb3RvdHlwZS5jaGVja1Bvc2l0aW9uV2l0aEV2ZW50TG9vcCA9IGZ1bmN0aW9uICgpIHtcbiAgICBzZXRUaW1lb3V0KCQucHJveHkodGhpcy5jaGVja1Bvc2l0aW9uLCB0aGlzKSwgMSlcbiAgfVxuXG4gIEFmZml4LnByb3RvdHlwZS5jaGVja1Bvc2l0aW9uID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICghdGhpcy4kZWxlbWVudC5pcygnOnZpc2libGUnKSkgcmV0dXJuXG5cbiAgICB2YXIgaGVpZ2h0ICAgICAgID0gdGhpcy4kZWxlbWVudC5oZWlnaHQoKVxuICAgIHZhciBvZmZzZXQgICAgICAgPSB0aGlzLm9wdGlvbnMub2Zmc2V0XG4gICAgdmFyIG9mZnNldFRvcCAgICA9IG9mZnNldC50b3BcbiAgICB2YXIgb2Zmc2V0Qm90dG9tID0gb2Zmc2V0LmJvdHRvbVxuICAgIHZhciBzY3JvbGxIZWlnaHQgPSBNYXRoLm1heCgkKGRvY3VtZW50KS5oZWlnaHQoKSwgJChkb2N1bWVudC5ib2R5KS5oZWlnaHQoKSlcblxuICAgIGlmICh0eXBlb2Ygb2Zmc2V0ICE9ICdvYmplY3QnKSAgICAgICAgIG9mZnNldEJvdHRvbSA9IG9mZnNldFRvcCA9IG9mZnNldFxuICAgIGlmICh0eXBlb2Ygb2Zmc2V0VG9wID09ICdmdW5jdGlvbicpICAgIG9mZnNldFRvcCAgICA9IG9mZnNldC50b3AodGhpcy4kZWxlbWVudClcbiAgICBpZiAodHlwZW9mIG9mZnNldEJvdHRvbSA9PSAnZnVuY3Rpb24nKSBvZmZzZXRCb3R0b20gPSBvZmZzZXQuYm90dG9tKHRoaXMuJGVsZW1lbnQpXG5cbiAgICB2YXIgYWZmaXggPSB0aGlzLmdldFN0YXRlKHNjcm9sbEhlaWdodCwgaGVpZ2h0LCBvZmZzZXRUb3AsIG9mZnNldEJvdHRvbSlcblxuICAgIGlmICh0aGlzLmFmZml4ZWQgIT0gYWZmaXgpIHtcbiAgICAgIGlmICh0aGlzLnVucGluICE9IG51bGwpIHRoaXMuJGVsZW1lbnQuY3NzKCd0b3AnLCAnJylcblxuICAgICAgdmFyIGFmZml4VHlwZSA9ICdhZmZpeCcgKyAoYWZmaXggPyAnLScgKyBhZmZpeCA6ICcnKVxuICAgICAgdmFyIGUgICAgICAgICA9ICQuRXZlbnQoYWZmaXhUeXBlICsgJy5icy5hZmZpeCcpXG5cbiAgICAgIHRoaXMuJGVsZW1lbnQudHJpZ2dlcihlKVxuXG4gICAgICBpZiAoZS5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkgcmV0dXJuXG5cbiAgICAgIHRoaXMuYWZmaXhlZCA9IGFmZml4XG4gICAgICB0aGlzLnVucGluID0gYWZmaXggPT0gJ2JvdHRvbScgPyB0aGlzLmdldFBpbm5lZE9mZnNldCgpIDogbnVsbFxuXG4gICAgICB0aGlzLiRlbGVtZW50XG4gICAgICAgIC5yZW1vdmVDbGFzcyhBZmZpeC5SRVNFVClcbiAgICAgICAgLmFkZENsYXNzKGFmZml4VHlwZSlcbiAgICAgICAgLnRyaWdnZXIoYWZmaXhUeXBlLnJlcGxhY2UoJ2FmZml4JywgJ2FmZml4ZWQnKSArICcuYnMuYWZmaXgnKVxuICAgIH1cblxuICAgIGlmIChhZmZpeCA9PSAnYm90dG9tJykge1xuICAgICAgdGhpcy4kZWxlbWVudC5vZmZzZXQoe1xuICAgICAgICB0b3A6IHNjcm9sbEhlaWdodCAtIGhlaWdodCAtIG9mZnNldEJvdHRvbVxuICAgICAgfSlcbiAgICB9XG4gIH1cblxuXG4gIC8vIEFGRklYIFBMVUdJTiBERUZJTklUSU9OXG4gIC8vID09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgZnVuY3Rpb24gUGx1Z2luKG9wdGlvbikge1xuICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgdmFyICR0aGlzICAgPSAkKHRoaXMpXG4gICAgICB2YXIgZGF0YSAgICA9ICR0aGlzLmRhdGEoJ2JzLmFmZml4JylcbiAgICAgIHZhciBvcHRpb25zID0gdHlwZW9mIG9wdGlvbiA9PSAnb2JqZWN0JyAmJiBvcHRpb25cblxuICAgICAgaWYgKCFkYXRhKSAkdGhpcy5kYXRhKCdicy5hZmZpeCcsIChkYXRhID0gbmV3IEFmZml4KHRoaXMsIG9wdGlvbnMpKSlcbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9uID09ICdzdHJpbmcnKSBkYXRhW29wdGlvbl0oKVxuICAgIH0pXG4gIH1cblxuICB2YXIgb2xkID0gJC5mbi5hZmZpeFxuXG4gICQuZm4uYWZmaXggICAgICAgICAgICAgPSBQbHVnaW5cbiAgJC5mbi5hZmZpeC5Db25zdHJ1Y3RvciA9IEFmZml4XG5cblxuICAvLyBBRkZJWCBOTyBDT05GTElDVFxuICAvLyA9PT09PT09PT09PT09PT09PVxuXG4gICQuZm4uYWZmaXgubm9Db25mbGljdCA9IGZ1bmN0aW9uICgpIHtcbiAgICAkLmZuLmFmZml4ID0gb2xkXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG5cbiAgLy8gQUZGSVggREFUQS1BUElcbiAgLy8gPT09PT09PT09PT09PT1cblxuICAkKHdpbmRvdykub24oJ2xvYWQnLCBmdW5jdGlvbiAoKSB7XG4gICAgJCgnW2RhdGEtc3B5PVwiYWZmaXhcIl0nKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciAkc3B5ID0gJCh0aGlzKVxuICAgICAgdmFyIGRhdGEgPSAkc3B5LmRhdGEoKVxuXG4gICAgICBkYXRhLm9mZnNldCA9IGRhdGEub2Zmc2V0IHx8IHt9XG5cbiAgICAgIGlmIChkYXRhLm9mZnNldEJvdHRvbSAhPSBudWxsKSBkYXRhLm9mZnNldC5ib3R0b20gPSBkYXRhLm9mZnNldEJvdHRvbVxuICAgICAgaWYgKGRhdGEub2Zmc2V0VG9wICAgICE9IG51bGwpIGRhdGEub2Zmc2V0LnRvcCAgICA9IGRhdGEub2Zmc2V0VG9wXG5cbiAgICAgIFBsdWdpbi5jYWxsKCRzcHksIGRhdGEpXG4gICAgfSlcbiAgfSlcblxufShqUXVlcnkpO1xuIiwiLypcbiAqICBBbmd1bGFyIFJhbmdlU2xpZGVyIERpcmVjdGl2ZVxuICpcbiAqICBWZXJzaW9uOiAwLjAuMTNcbiAqXG4gKiAgQXV0aG9yOiBEYW5pZWwgQ3Jpc3AsIGRhbmllbGNyaXNwLmNvbVxuICpcbiAqICBUaGUgcmFuZ2VTbGlkZXIgaGFzIGJlZW4gc3R5bGVkIHRvIG1hdGNoIHRoZSBkZWZhdWx0IHN0eWxpbmdcbiAqICBvZiBmb3JtIGVsZW1lbnRzIHN0eWxlZCB1c2luZyBUd2l0dGVyJ3MgQm9vdHN0cmFwXG4gKlxuICogIE9yaWdpbmFsbHkgZm9ya2VkIGZyb20gaHR0cHM6Ly9naXRodWIuY29tL2xlb25nZXJzZW4vbm9VaVNsaWRlclxuICpcblxuICAgIFRoaXMgY29kZSBpcyByZWxlYXNlZCB1bmRlciB0aGUgTUlUIExpY2VuY2UgLSBodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXG5cbiAgICBDb3B5cmlnaHQgKGMpIDIwMTMgRGFuaWVsIENyaXNwXG5cbiAgICBQZXJtaXNzaW9uIGlzIGhlcmVieSBncmFudGVkLCBmcmVlIG9mIGNoYXJnZSwgdG8gYW55IHBlcnNvbiBvYnRhaW5pbmcgYSBjb3B5XG4gICAgb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGUgXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbFxuICAgIGluIHRoZSBTb2Z0d2FyZSB3aXRob3V0IHJlc3RyaWN0aW9uLCBpbmNsdWRpbmcgd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHNcbiAgICB0byB1c2UsIGNvcHksIG1vZGlmeSwgbWVyZ2UsIHB1Ymxpc2gsIGRpc3RyaWJ1dGUsIHN1YmxpY2Vuc2UsIGFuZC9vciBzZWxsXG4gICAgY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdCBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzXG4gICAgZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZSBmb2xsb3dpbmcgY29uZGl0aW9uczpcblxuICAgIFRoZSBhYm92ZSBjb3B5cmlnaHQgbm90aWNlIGFuZCB0aGlzIHBlcm1pc3Npb24gbm90aWNlIHNoYWxsIGJlIGluY2x1ZGVkIGluXG4gICAgYWxsIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG5cbiAgICBUSEUgU09GVFdBUkUgSVMgUFJPVklERUQgXCJBUyBJU1wiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTIE9SXG4gICAgSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRiBNRVJDSEFOVEFCSUxJVFksXG4gICAgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQU5EIE5PTklORlJJTkdFTUVOVC4gSU4gTk8gRVZFTlQgU0hBTEwgVEhFXG4gICAgQVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSwgREFNQUdFUyBPUiBPVEhFUlxuICAgIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1IgT1RIRVJXSVNFLCBBUklTSU5HIEZST00sXG4gICAgT1VUIE9GIE9SIElOIENPTk5FQ1RJT04gV0lUSCBUSEUgU09GVFdBUkUgT1IgVEhFIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTlxuICAgIFRIRSBTT0ZUV0FSRS5cblxuKi9cblxuKGZ1bmN0aW9uKCkge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIC8vIGNoZWNrIGlmIHdlIG5lZWQgdG8gc3VwcG9ydCBsZWdhY3kgYW5ndWxhclxuICAgIHZhciBsZWdhY3lTdXBwb3J0ID0gKGFuZ3VsYXIudmVyc2lvbi5tYWpvciA9PT0gMSAmJiBhbmd1bGFyLnZlcnNpb24ubWlub3IgPT09IDApO1xuXG4gICAgLyoqXG4gICAgICogUmFuZ2VTbGlkZXIsIGFsbG93cyB1c2VyIHRvIGRlZmluZSBhIHJhbmdlIG9mIHZhbHVlcyB1c2luZyBhIHNsaWRlclxuICAgICAqIFRvdWNoIGZyaWVuZGx5LlxuICAgICAqIEBkaXJlY3RpdmVcbiAgICAgKi9cbiAgICBhbmd1bGFyLm1vZHVsZSgndWktcmFuZ2VTbGlkZXInLCBbXSlcbiAgICAgICAgLmRpcmVjdGl2ZSgncmFuZ2VTbGlkZXInLCBbJyRkb2N1bWVudCcsICckZmlsdGVyJywgJyRsb2cnLCBmdW5jdGlvbigkZG9jdW1lbnQsICRmaWx0ZXIsICRsb2cpIHtcblxuICAgICAgICAgICAgLy8gdGVzdCBmb3IgbW91c2UsIHBvaW50ZXIgb3IgdG91Y2hcbiAgICAgICAgICAgIHZhciBldmVudE5hbWVzcGFjZSA9ICcucmFuZ2VTbGlkZXInLFxuXG4gICAgICAgICAgICAgICAgZGVmYXVsdHMgPSB7XG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgb3JpZW50YXRpb246ICdob3Jpem9udGFsJyxcbiAgICAgICAgICAgICAgICAgICAgc3RlcDogMCxcbiAgICAgICAgICAgICAgICAgICAgZGVjaW1hbFBsYWNlczogMCxcbiAgICAgICAgICAgICAgICAgICAgc2hvd1ZhbHVlczogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgcHJldmVudEVxdWFsTWluTWF4OiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgYXR0YWNoSGFuZGxlVmFsdWVzOiBmYWxzZVxuICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAvLyBEZXRlcm1pbmUgdGhlIGV2ZW50cyB0byBiaW5kLiBJRTExIGltcGxlbWVudHMgcG9pbnRlckV2ZW50cyB3aXRob3V0XG4gICAgICAgICAgICAgICAgLy8gYSBwcmVmaXgsIHdoaWNoIGJyZWFrcyBjb21wYXRpYmlsaXR5IHdpdGggdGhlIElFMTAgaW1wbGVtZW50YXRpb24uXG4gICAgICAgICAgICAgICAgLyoqIEBjb25zdCAqL1xuICAgICAgICAgICAgICAgIGFjdGlvbnMgPSB3aW5kb3cubmF2aWdhdG9yLnBvaW50ZXJFbmFibGVkID8ge1xuICAgICAgICAgICAgICAgICAgICBzdGFydDogJ3BvaW50ZXJkb3duJyxcbiAgICAgICAgICAgICAgICAgICAgbW92ZTogJ3BvaW50ZXJtb3ZlJyxcbiAgICAgICAgICAgICAgICAgICAgZW5kOiAncG9pbnRlcnVwJyxcbiAgICAgICAgICAgICAgICAgICAgb3ZlcjogJ3BvaW50ZXJkb3duJyxcbiAgICAgICAgICAgICAgICAgICAgb3V0OiAnbW91c2VvdXQnXG4gICAgICAgICAgICAgICAgfSA6IHdpbmRvdy5uYXZpZ2F0b3IubXNQb2ludGVyRW5hYmxlZCA/IHtcbiAgICAgICAgICAgICAgICAgICAgc3RhcnQ6ICdNU1BvaW50ZXJEb3duJyxcbiAgICAgICAgICAgICAgICAgICAgbW92ZTogJ01TUG9pbnRlck1vdmUnLFxuICAgICAgICAgICAgICAgICAgICBlbmQ6ICdNU1BvaW50ZXJVcCcsXG4gICAgICAgICAgICAgICAgICAgIG92ZXI6ICdNU1BvaW50ZXJEb3duJyxcbiAgICAgICAgICAgICAgICAgICAgb3V0OiAnbW91c2VvdXQnXG4gICAgICAgICAgICAgICAgfSA6IHtcbiAgICAgICAgICAgICAgICAgICAgc3RhcnQ6ICdtb3VzZWRvd24gdG91Y2hzdGFydCcsXG4gICAgICAgICAgICAgICAgICAgIG1vdmU6ICdtb3VzZW1vdmUgdG91Y2htb3ZlJyxcbiAgICAgICAgICAgICAgICAgICAgZW5kOiAnbW91c2V1cCB0b3VjaGVuZCcsXG4gICAgICAgICAgICAgICAgICAgIG92ZXI6ICdtb3VzZW92ZXIgdG91Y2hzdGFydCcsXG4gICAgICAgICAgICAgICAgICAgIG91dDogJ21vdXNlb3V0J1xuICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICBvbkV2ZW50ID0gYWN0aW9ucy5zdGFydCArIGV2ZW50TmFtZXNwYWNlLFxuICAgICAgICAgICAgICAgIG1vdmVFdmVudCA9IGFjdGlvbnMubW92ZSArIGV2ZW50TmFtZXNwYWNlLFxuICAgICAgICAgICAgICAgIG9mZkV2ZW50ID0gYWN0aW9ucy5lbmQgKyBldmVudE5hbWVzcGFjZSxcbiAgICAgICAgICAgICAgICBvdmVyRXZlbnQgPSBhY3Rpb25zLm92ZXIgKyBldmVudE5hbWVzcGFjZSxcbiAgICAgICAgICAgICAgICBvdXRFdmVudCA9IGFjdGlvbnMub3V0ICsgZXZlbnROYW1lc3BhY2UsXG5cbiAgICAgICAgICAgICAgICAvLyBnZXQgc3RhbmRhcmlzZWQgY2xpZW50WCBhbmQgY2xpZW50WVxuICAgICAgICAgICAgICAgIGNsaWVudCA9IGZ1bmN0aW9uKGYpIHtcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbKGYuY2xpZW50WCB8fCBmLm9yaWdpbmFsRXZlbnQuY2xpZW50WCB8fCBmLm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXS5jbGllbnRYKSwgKGYuY2xpZW50WSB8fCBmLm9yaWdpbmFsRXZlbnQuY2xpZW50WSB8fCBmLm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXS5jbGllbnRZKV07XG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbJ3gnLCAneSddO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgICAgIHJlc3RyaWN0ID0gZnVuY3Rpb24odmFsdWUpIHtcblxuICAgICAgICAgICAgICAgICAgICAvLyBub3JtYWxpemUgc28gaXQgY2FuJ3QgbW92ZSBvdXQgb2YgYm91bmRzXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAodmFsdWUgPCAwID8gMCA6ICh2YWx1ZSA+IDEwMCA/IDEwMCA6IHZhbHVlKSk7XG5cbiAgICAgICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAgICAgaXNOdW1iZXIgPSBmdW5jdGlvbihuKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKG4pO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gIWlzTmFOKHBhcnNlRmxvYXQobikpICYmIGlzRmluaXRlKG4pO1xuICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICBzY29wZU9wdGlvbnMgPSB7XG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiAnPT8nLFxuICAgICAgICAgICAgICAgICAgICBtaW46ICc9JyxcbiAgICAgICAgICAgICAgICAgICAgbWF4OiAnPScsXG4gICAgICAgICAgICAgICAgICAgIG1vZGVsTWluOiAnPT8nLFxuICAgICAgICAgICAgICAgICAgICBtb2RlbE1heDogJz0/JyxcbiAgICAgICAgICAgICAgICAgICAgb25IYW5kbGVEb3duOiAnJicsIC8vIGNhbGxzIG9wdGlvbmFsIGZ1bmN0aW9uIHdoZW4gaGFuZGxlIGlzIGdyYWJiZWRcbiAgICAgICAgICAgICAgICAgICAgb25IYW5kbGVVcDogJyYnLCAvLyBjYWxscyBvcHRpb25hbCBmdW5jdGlvbiB3aGVuIGhhbmRsZSBpcyByZWxlYXNlZFxuICAgICAgICAgICAgICAgICAgICBvcmllbnRhdGlvbjogJ0AnLCAvLyBvcHRpb25zOiBob3Jpem9udGFsIHwgdmVydGljYWwgfCB2ZXJ0aWNhbCBsZWZ0IHwgdmVydGljYWwgcmlnaHRcbiAgICAgICAgICAgICAgICAgICAgc3RlcDogJ0AnLFxuICAgICAgICAgICAgICAgICAgICBkZWNpbWFsUGxhY2VzOiAnQCcsXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcjogJ0AnLFxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJPcHRpb25zOiAnQCcsXG4gICAgICAgICAgICAgICAgICAgIHNob3dWYWx1ZXM6ICdAJyxcbiAgICAgICAgICAgICAgICAgICAgcGluSGFuZGxlOiAnQCcsXG4gICAgICAgICAgICAgICAgICAgIHByZXZlbnRFcXVhbE1pbk1heDogJ0AnLFxuICAgICAgICAgICAgICAgICAgICBhdHRhY2hIYW5kbGVWYWx1ZXM6ICdAJyxcbiAgICAgICAgICAgICAgICAgICAgZ2V0dGVyU2V0dGVyOiAnQCcgLy8gQWxsb3cgdGhlIHVzZSBvZiBnZXR0ZXJTZXR0ZXJzIGZvciBtb2RlbCB2YWx1ZXNcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBpZiAobGVnYWN5U3VwcG9ydCkge1xuICAgICAgICAgICAgICAgIC8vIG1ha2Ugb3B0aW9uYWwgcHJvcGVydGllcyByZXF1aXJlZFxuICAgICAgICAgICAgICAgIHNjb3BlT3B0aW9ucy5kaXNhYmxlZCA9ICc9JztcbiAgICAgICAgICAgICAgICBzY29wZU9wdGlvbnMubW9kZWxNaW4gPSAnPSc7XG4gICAgICAgICAgICAgICAgc2NvcGVPcHRpb25zLm1vZGVsTWF4ID0gJz0nO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBpZiAoRVZFTlQgPCA0KSB7XG4gICAgICAgICAgICAvLyAgICAgLy8gc29tZSBzb3J0IG9mIHRvdWNoIGhhcyBiZWVuIGRldGVjdGVkXG4gICAgICAgICAgICAvLyAgICAgYW5ndWxhci5lbGVtZW50KCdodG1sJykuYWRkQ2xhc3MoJ25ncnMtdG91Y2gnKTtcbiAgICAgICAgICAgIC8vIH0gZWxzZSB7XG4gICAgICAgICAgICAvLyAgICAgYW5ndWxhci5lbGVtZW50KCdodG1sJykuYWRkQ2xhc3MoJ25ncnMtbm8tdG91Y2gnKTtcbiAgICAgICAgICAgIC8vIH1cblxuXG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHJlc3RyaWN0OiAnQScsXG4gICAgICAgICAgICAgICAgcmVwbGFjZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZTogWyc8ZGl2IGNsYXNzPVwibmdycy1yYW5nZS1zbGlkZXJcIj4nLFxuICAgICAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cIm5ncnMtcnVubmVyXCI+JyxcbiAgICAgICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJuZ3JzLWhhbmRsZSBuZ3JzLWhhbmRsZS1taW5cIj48aT48L2k+PC9kaXY+JyxcbiAgICAgICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJuZ3JzLWhhbmRsZSBuZ3JzLWhhbmRsZS1tYXhcIj48aT48L2k+PC9kaXY+JyxcbiAgICAgICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJuZ3JzLWpvaW5cIj48L2Rpdj4nLFxuICAgICAgICAgICAgICAgICAgICAnPC9kaXY+JyxcbiAgICAgICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJuZ3JzLXZhbHVlLXJ1bm5lclwiPicsXG4gICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwibmdycy12YWx1ZSBuZ3JzLXZhbHVlLW1pblwiIG5nLXNob3c9XCJzaG93VmFsdWVzXCI+PGRpdj57e2ZpbHRlcmVkTW9kZWxNaW59fTwvZGl2PjwvZGl2PicsXG4gICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwibmdycy12YWx1ZSBuZ3JzLXZhbHVlLW1heFwiIG5nLXNob3c9XCJzaG93VmFsdWVzXCI+PGRpdj57e2ZpbHRlcmVkTW9kZWxNYXh9fTwvZGl2PjwvZGl2PicsXG4gICAgICAgICAgICAgICAgICAgICc8L2Rpdj4nLFxuICAgICAgICAgICAgICAgICAgICAnPC9kaXY+J1xuICAgICAgICAgICAgICAgIF0uam9pbignJyksXG4gICAgICAgICAgICAgICAgc2NvcGU6IHNjb3BlT3B0aW9ucyxcbiAgICAgICAgICAgICAgICBsaW5rOiBmdW5jdGlvbihzY29wZSwgZWxlbWVudCwgYXR0cnMsIGNvbnRyb2xsZXIpIHtcblxuICAgICAgICAgICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAgICAgICAgICogIEZJTkQgRUxFTUVOVFNcbiAgICAgICAgICAgICAgICAgICAgICovXG5cbiAgICAgICAgICAgICAgICAgICAgdmFyICRzbGlkZXIgPSBhbmd1bGFyLmVsZW1lbnQoZWxlbWVudCksXG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVzID0gW2VsZW1lbnQuZmluZCgnLm5ncnMtaGFuZGxlLW1pbicpLCBlbGVtZW50LmZpbmQoJy5uZ3JzLWhhbmRsZS1tYXgnKV0sXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZXMgPSBbZWxlbWVudC5maW5kKCcubmdycy12YWx1ZS1taW4nKSwgZWxlbWVudC5maW5kKCcubmdycy12YWx1ZS1tYXgnKV0sXG4gICAgICAgICAgICAgICAgICAgICAgICBqb2luID0gZWxlbWVudC5maW5kKCcubmdycy1qb2luJyksXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3MgPSAnbGVmdCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NPcHAgPSAncmlnaHQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgb3JpZW50YXRpb24gPSAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgYWxsb3dlZFJhbmdlID0gWzAsIDBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgcmFuZ2UgPSAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgZG93biA9IGZhbHNlO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGZpbHRlcmVkXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmZpbHRlcmVkTW9kZWxNaW4gPSBtb2RlbE1pbigpO1xuICAgICAgICAgICAgICAgICAgICBzY29wZS5maWx0ZXJlZE1vZGVsTWF4ID0gbW9kZWxNYXgoKTtcblxuICAgICAgICAgICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAgICAgICAgICogIEZBTEwgQkFDSyBUTyBERUZBVUxUUyBGT1IgU09NRSBBVFRSSUJVVEVTXG4gICAgICAgICAgICAgICAgICAgICAqL1xuXG4gICAgICAgICAgICAgICAgICAgIGF0dHJzLiRvYnNlcnZlKCdkaXNhYmxlZCcsIGZ1bmN0aW9uKHZhbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFhbmd1bGFyLmlzRGVmaW5lZCh2YWwpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuZGlzYWJsZWQgPSBkZWZhdWx0cy5kaXNhYmxlZDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuJHdhdGNoKCdkaXNhYmxlZCcsIHNldERpc2FibGVkU3RhdHVzKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgYXR0cnMuJG9ic2VydmUoJ29yaWVudGF0aW9uJywgZnVuY3Rpb24odmFsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWFuZ3VsYXIuaXNEZWZpbmVkKHZhbCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5vcmllbnRhdGlvbiA9IGRlZmF1bHRzLm9yaWVudGF0aW9uO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgY2xhc3NOYW1lcyA9IHNjb3BlLm9yaWVudGF0aW9uLnNwbGl0KCcgJyksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlQ2xhc3M7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwLCBsID0gY2xhc3NOYW1lcy5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWVzW2ldID0gJ25ncnMtJyArIGNsYXNzTmFtZXNbaV07XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZUNsYXNzID0gY2xhc3NOYW1lcy5qb2luKCcgJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGFkZCBjbGFzcyB0byBlbGVtZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2xpZGVyLmFkZENsYXNzKHVzZUNsYXNzKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXBkYXRlIHBvc1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNjb3BlLm9yaWVudGF0aW9uID09PSAndmVydGljYWwnIHx8IHNjb3BlLm9yaWVudGF0aW9uID09PSAndmVydGljYWwgbGVmdCcgfHwgc2NvcGUub3JpZW50YXRpb24gPT09ICd2ZXJ0aWNhbCByaWdodCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3MgPSAndG9wJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3NPcHAgPSAnYm90dG9tJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcmllbnRhdGlvbiA9IDE7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIGF0dHJzLiRvYnNlcnZlKCdzdGVwJywgZnVuY3Rpb24odmFsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWFuZ3VsYXIuaXNEZWZpbmVkKHZhbCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5zdGVwID0gZGVmYXVsdHMuc3RlcDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgYXR0cnMuJG9ic2VydmUoJ2RlY2ltYWxQbGFjZXMnLCBmdW5jdGlvbih2YWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghYW5ndWxhci5pc0RlZmluZWQodmFsKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmRlY2ltYWxQbGFjZXMgPSBkZWZhdWx0cy5kZWNpbWFsUGxhY2VzO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICBhdHRycy4kb2JzZXJ2ZSgnc2hvd1ZhbHVlcycsIGZ1bmN0aW9uKHZhbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFhbmd1bGFyLmlzRGVmaW5lZCh2YWwpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuc2hvd1ZhbHVlcyA9IGRlZmF1bHRzLnNob3dWYWx1ZXM7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh2YWwgPT09ICdmYWxzZScpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuc2hvd1ZhbHVlcyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLnNob3dWYWx1ZXMgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgYXR0cnMuJG9ic2VydmUoJ3BpbkhhbmRsZScsIGZ1bmN0aW9uKHZhbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFhbmd1bGFyLmlzRGVmaW5lZCh2YWwpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUucGluSGFuZGxlID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHZhbCA9PT0gJ21pbicgfHwgdmFsID09PSAnbWF4Jykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5waW5IYW5kbGUgPSB2YWw7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUucGluSGFuZGxlID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLiR3YXRjaCgncGluSGFuZGxlJywgc2V0UGluSGFuZGxlKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgYXR0cnMuJG9ic2VydmUoJ3ByZXZlbnRFcXVhbE1pbk1heCcsIGZ1bmN0aW9uKHZhbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFhbmd1bGFyLmlzRGVmaW5lZCh2YWwpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUucHJldmVudEVxdWFsTWluTWF4ID0gZGVmYXVsdHMucHJldmVudEVxdWFsTWluTWF4O1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodmFsID09PSAnZmFsc2UnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLnByZXZlbnRFcXVhbE1pbk1heCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLnByZXZlbnRFcXVhbE1pbk1heCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICBhdHRycy4kb2JzZXJ2ZSgnYXR0YWNoSGFuZGxlVmFsdWVzJywgZnVuY3Rpb24odmFsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWFuZ3VsYXIuaXNEZWZpbmVkKHZhbCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5hdHRhY2hIYW5kbGVWYWx1ZXMgPSBkZWZhdWx0cy5hdHRhY2hIYW5kbGVWYWx1ZXM7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh2YWwgPT09ICd0cnVlJyB8fCB2YWwgPT09ICcnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGZsYWcgYXMgdHJ1ZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5hdHRhY2hIYW5kbGVWYWx1ZXMgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBhZGQgY2xhc3MgdG8gcnVubmVyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuZmluZCgnLm5ncnMtdmFsdWUtcnVubmVyJykuYWRkQ2xhc3MoJ25ncnMtYXR0YWNoZWQtaGFuZGxlcycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmF0dGFjaEhhbmRsZVZhbHVlcyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gR2V0dGVyU2V0dGVycyBmb3IgbW9kZWwgdmFsdWVzXG5cbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gbW9kZWxNaW4obmV3VmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHNjb3BlLmdldHRlclNldHRlcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhcmd1bWVudHMubGVuZ3RoID8gc2NvcGUubW9kZWxNaW4obmV3VmFsdWUpIDogc2NvcGUubW9kZWxNaW4oKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFyZ3VtZW50cy5sZW5ndGggPyAoc2NvcGUubW9kZWxNaW4gPSBuZXdWYWx1ZSkgOiBzY29wZS5tb2RlbE1pbjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIG1vZGVsTWF4KG5ld1ZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZihzY29wZS5nZXR0ZXJTZXR0ZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYXJndW1lbnRzLmxlbmd0aCA/IHNjb3BlLm1vZGVsTWF4KG5ld1ZhbHVlKSA6IHNjb3BlLm1vZGVsTWF4KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhcmd1bWVudHMubGVuZ3RoID8gKHNjb3BlLm1vZGVsTWF4ID0gbmV3VmFsdWUpIDogc2NvcGUubW9kZWxNYXg7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAvLyBsaXN0ZW4gZm9yIGNoYW5nZXMgdG8gdmFsdWVzXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLiR3YXRjaCgnbWluJywgc2V0TWluTWF4KTtcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUuJHdhdGNoKCdtYXgnLCBzZXRNaW5NYXgpO1xuXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLiR3YXRjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbW9kZWxNaW4oKTtcbiAgICAgICAgICAgICAgICAgICAgfSwgc2V0TW9kZWxNaW5NYXgpO1xuICAgICAgICAgICAgICAgICAgICBzY29wZS4kd2F0Y2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG1vZGVsTWF4KCk7XG4gICAgICAgICAgICAgICAgICAgIH0sIHNldE1vZGVsTWluTWF4KTtcblxuICAgICAgICAgICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAgICAgICAgICogSEFORExFIENIQU5HRVNcbiAgICAgICAgICAgICAgICAgICAgICovXG5cbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gc2V0UGluSGFuZGxlKHN0YXR1cykge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHN0YXR1cyA9PT0gXCJtaW5cIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuZ3VsYXIuZWxlbWVudChoYW5kbGVzWzBdKS5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuZ3VsYXIuZWxlbWVudChoYW5kbGVzWzFdKS5jc3MoJ2Rpc3BsYXknLCAnYmxvY2snKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3RhdHVzID09PSBcIm1heFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5lbGVtZW50KGhhbmRsZXNbMF0pLmNzcygnZGlzcGxheScsICdibG9jaycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuZ3VsYXIuZWxlbWVudChoYW5kbGVzWzFdKS5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmVsZW1lbnQoaGFuZGxlc1swXSkuY3NzKCdkaXNwbGF5JywgJ2Jsb2NrJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5lbGVtZW50KGhhbmRsZXNbMV0pLmNzcygnZGlzcGxheScsICdibG9jaycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gc2V0RGlzYWJsZWRTdGF0dXMoc3RhdHVzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc3RhdHVzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNsaWRlci5hZGRDbGFzcygnbmdycy1kaXNhYmxlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2xpZGVyLnJlbW92ZUNsYXNzKCduZ3JzLWRpc2FibGVkJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbiBzZXRNaW5NYXgoKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzY29wZS5taW4gPiBzY29wZS5tYXgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvd0Vycm9yKCdtaW4gbXVzdCBiZSBsZXNzIHRoYW4gb3IgZXF1YWwgdG8gbWF4Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIG9ubHkgZG8gc3R1ZmYgd2hlbiBib3RoIHZhbHVlcyBhcmUgcmVhZHlcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhbmd1bGFyLmlzRGVmaW5lZChzY29wZS5taW4pICYmIGFuZ3VsYXIuaXNEZWZpbmVkKHNjb3BlLm1heCkpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIG1ha2Ugc3VyZSB0aGV5IGFyZSBudW1iZXJzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFpc051bWJlcihzY29wZS5taW4pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRocm93RXJyb3IoJ21pbiBtdXN0IGJlIGEgbnVtYmVyJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFpc051bWJlcihzY29wZS5tYXgpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRocm93RXJyb3IoJ21heCBtdXN0IGJlIGEgbnVtYmVyJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmFuZ2UgPSBzY29wZS5tYXggLSBzY29wZS5taW47XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxsb3dlZFJhbmdlID0gW3Njb3BlLm1pbiwgc2NvcGUubWF4XTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHVwZGF0ZSBtb2RlbHMgdG9vXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0TW9kZWxNaW5NYXgoKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gc2V0TW9kZWxNaW5NYXgoKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtb2RlbE1pbigpID4gbW9kZWxNYXgoKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRocm93V2FybmluZygnbW9kZWxNaW4gbXVzdCBiZSBsZXNzIHRoYW4gb3IgZXF1YWwgdG8gbW9kZWxNYXgnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyByZXNldCB2YWx1ZXMgdG8gY29ycmVjdFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsTWluKG1vZGVsTWF4KCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBvbmx5IGRvIHN0dWZmIHdoZW4gYm90aCB2YWx1ZXMgYXJlIHJlYWR5XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKGFuZ3VsYXIuaXNEZWZpbmVkKG1vZGVsTWluKCkpIHx8IHNjb3BlLnBpbkhhbmRsZSA9PT0gJ21pbicpICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKGFuZ3VsYXIuaXNEZWZpbmVkKG1vZGVsTWF4KCkpIHx8IHNjb3BlLnBpbkhhbmRsZSA9PT0gJ21heCcpXG4gICAgICAgICAgICAgICAgICAgICAgICApIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIG1ha2Ugc3VyZSB0aGV5IGFyZSBudW1iZXJzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFpc051bWJlcihtb2RlbE1pbigpKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2NvcGUucGluSGFuZGxlICE9PSAnbWluJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3dXYXJuaW5nKCdtb2RlbE1pbiBtdXN0IGJlIGEgbnVtYmVyJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbW9kZWxNaW4oc2NvcGUubWluKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWlzTnVtYmVyKG1vZGVsTWF4KCkpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzY29wZS5waW5IYW5kbGUgIT09ICdtYXgnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvd1dhcm5pbmcoJ21vZGVsTWF4IG11c3QgYmUgYSBudW1iZXInKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbE1heChzY29wZS5tYXgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBoYW5kbGUxcG9zID0gcmVzdHJpY3QoKChtb2RlbE1pbigpIC0gc2NvcGUubWluKSAvIHJhbmdlKSAqIDEwMCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZTJwb3MgPSByZXN0cmljdCgoKG1vZGVsTWF4KCkgLSBzY29wZS5taW4pIC8gcmFuZ2UpICogMTAwKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUxcG9zLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTJwb3M7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2NvcGUuYXR0YWNoSGFuZGxlVmFsdWVzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlMXBvcyA9IGhhbmRsZTFwb3M7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlMnBvcyA9IGhhbmRsZTJwb3M7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gbWFrZSBzdXJlIHRoZSBtb2RlbCB2YWx1ZXMgYXJlIHdpdGhpbiB0aGUgYWxsb3dlZCByYW5nZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsTWluKE1hdGgubWF4KHNjb3BlLm1pbiwgbW9kZWxNaW4oKSkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsTWF4KE1hdGgubWluKHNjb3BlLm1heCwgbW9kZWxNYXgoKSkpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNjb3BlLmZpbHRlciAmJiBzY29wZS5maWx0ZXJPcHRpb25zKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmZpbHRlcmVkTW9kZWxNaW4gPSAkZmlsdGVyKHNjb3BlLmZpbHRlcikobW9kZWxNaW4oKSwgc2NvcGUuZmlsdGVyT3B0aW9ucyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmZpbHRlcmVkTW9kZWxNYXggPSAkZmlsdGVyKHNjb3BlLmZpbHRlcikobW9kZWxNYXgoKSwgc2NvcGUuZmlsdGVyT3B0aW9ucyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzY29wZS5maWx0ZXIpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgZmlsdGVyVG9rZW5zID0gc2NvcGUuZmlsdGVyLnNwbGl0KCc6JyksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWx0ZXJOYW1lID0gc2NvcGUuZmlsdGVyLnNwbGl0KCc6JylbMF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWx0ZXJPcHRpb25zID0gZmlsdGVyVG9rZW5zLnNsaWNlKCkuc2xpY2UoMSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbE1pbk9wdGlvbnMsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbE1heE9wdGlvbnM7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcHJvcGVybHkgcGFyc2Ugc3RyaW5nIGFuZCBudW1iZXIgYXJnc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWx0ZXJPcHRpb25zID0gZmlsdGVyT3B0aW9ucy5tYXAoZnVuY3Rpb24gKGFyZykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzTnVtYmVyKGFyZykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gK2FyZztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoKGFyZ1swXSA9PSBcIlxcXCJcIiAmJiBhcmdbYXJnLmxlbmd0aC0xXSA9PSBcIlxcXCJcIikgfHwgKGFyZ1swXSA9PSBcIlxcJ1wiICYmIGFyZ1thcmcubGVuZ3RoLTFdID09IFwiXFwnXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFyZy5zbGljZSgxLCAtMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsTWluT3B0aW9ucyA9IGZpbHRlck9wdGlvbnMuc2xpY2UoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbW9kZWxNYXhPcHRpb25zID0gZmlsdGVyT3B0aW9ucy5zbGljZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbE1pbk9wdGlvbnMudW5zaGlmdChtb2RlbE1pbigpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbW9kZWxNYXhPcHRpb25zLnVuc2hpZnQobW9kZWxNYXgoKSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuZmlsdGVyZWRNb2RlbE1pbiA9ICRmaWx0ZXIoZmlsdGVyTmFtZSkuYXBwbHkobnVsbCwgbW9kZWxNaW5PcHRpb25zKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuZmlsdGVyZWRNb2RlbE1heCA9ICRmaWx0ZXIoZmlsdGVyTmFtZSkuYXBwbHkobnVsbCwgbW9kZWxNYXhPcHRpb25zKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5maWx0ZXJlZE1vZGVsTWluID0gbW9kZWxNaW4oKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuZmlsdGVyZWRNb2RlbE1heCA9IG1vZGVsTWF4KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gY2hlY2sgZm9yIG5vIHJhbmdlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNjb3BlLm1pbiA9PT0gc2NvcGUubWF4ICYmIG1vZGVsTWluKCkgPT0gbW9kZWxNYXgoKSkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHJlcG9zaXRpb24gaGFuZGxlc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmVsZW1lbnQoaGFuZGxlc1swXSkuY3NzKHBvcywgJzAlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuZ3VsYXIuZWxlbWVudChoYW5kbGVzWzFdKS5jc3MocG9zLCAnMTAwJScpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzY29wZS5hdHRhY2hIYW5kbGVWYWx1ZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHJlcG9zaXRpb24gdmFsdWVzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmVsZW1lbnQodmFsdWVzWzBdKS5jc3MocG9zLCAnMCUnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuZ3VsYXIuZWxlbWVudCh2YWx1ZXNbMV0pLmNzcyhwb3MsICcxMDAlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyByZXBvc2l0aW9uIGpvaW5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5lbGVtZW50KGpvaW4pLmNzcyhwb3MsICcwJScpLmNzcyhwb3NPcHAsICcwJScpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyByZXBvc2l0aW9uIGhhbmRsZXNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5lbGVtZW50KGhhbmRsZXNbMF0pLmNzcyhwb3MsIGhhbmRsZTFwb3MgKyAnJScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmVsZW1lbnQoaGFuZGxlc1sxXSkuY3NzKHBvcywgaGFuZGxlMnBvcyArICclJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNjb3BlLmF0dGFjaEhhbmRsZVZhbHVlcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcmVwb3NpdGlvbiB2YWx1ZXNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuZ3VsYXIuZWxlbWVudCh2YWx1ZXNbMF0pLmNzcyhwb3MsIHZhbHVlMXBvcyArICclJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmVsZW1lbnQodmFsdWVzWzFdKS5jc3MocG9zLCB2YWx1ZTJwb3MgKyAnJScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5lbGVtZW50KHZhbHVlc1sxXSkuY3NzKHBvc09wcCwgJ2F1dG8nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHJlcG9zaXRpb24gam9pblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmVsZW1lbnQoam9pbikuY3NzKHBvcywgaGFuZGxlMXBvcyArICclJykuY3NzKHBvc09wcCwgKDEwMCAtIGhhbmRsZTJwb3MpICsgJyUnKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBlbnN1cmUgbWluIGhhbmRsZSBjYW4ndCBiZSBoaWRkZW4gYmVoaW5kIG1heCBoYW5kbGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGhhbmRsZTFwb3MgPiA5NSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5lbGVtZW50KGhhbmRsZXNbMF0pLmNzcygnei1pbmRleCcsIDMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIGhhbmRsZU1vdmUoaW5kZXgpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRoYW5kbGUgPSBoYW5kbGVzW2luZGV4XTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gb24gbW91c2Vkb3duIC8gdG91Y2hzdGFydFxuICAgICAgICAgICAgICAgICAgICAgICAgJGhhbmRsZS5iaW5kKG9uRXZlbnQgKyAnWCcsIGZ1bmN0aW9uKGV2ZW50KSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgaGFuZGxlRG93bkNsYXNzID0gKGluZGV4ID09PSAwID8gJ25ncnMtaGFuZGxlLW1pbicgOiAnbmdycy1oYW5kbGUtbWF4JykgKyAnLWRvd24nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL3VuYmluZCA9ICRoYW5kbGUuYWRkKCRkb2N1bWVudCkuYWRkKCdib2R5JyksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsVmFsdWUgPSAoaW5kZXggPT09IDAgPyBtb2RlbE1pbigpIDogbW9kZWxNYXgoKSkgLSBzY29wZS5taW4sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9yaWdpbmFsUG9zaXRpb24gPSAobW9kZWxWYWx1ZSAvIHJhbmdlKSAqIDEwMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3JpZ2luYWxDbGljayA9IGNsaWVudChldmVudCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByZXZpb3VzQ2xpY2sgPSBvcmlnaW5hbENsaWNrLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmV2aW91c1Byb3Bvc2FsID0gZmFsc2U7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYW5ndWxhci5pc0Z1bmN0aW9uKHNjb3BlLm9uSGFuZGxlRG93bikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUub25IYW5kbGVEb3duKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gc3RvcCB1c2VyIGFjY2lkZW50YWxseSBzZWxlY3Rpbmcgc3R1ZmZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmVsZW1lbnQoJ2JvZHknKS5iaW5kKCdzZWxlY3RzdGFydCcgKyBldmVudE5hbWVzcGFjZSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIG9ubHkgZG8gc3R1ZmYgaWYgd2UgYXJlIGRpc2FibGVkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFzY29wZS5kaXNhYmxlZCkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGZsYWcgYXMgZG93blxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb3duID0gdHJ1ZTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBhZGQgZG93biBjbGFzc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkaGFuZGxlLmFkZENsYXNzKCduZ3JzLWRvd24nKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2xpZGVyLmFkZENsYXNzKCduZ3JzLWZvY3VzICcgKyBoYW5kbGVEb3duQ2xhc3MpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGFkZCB0b3VjaCBjbGFzcyBmb3IgTVMgc3R5bGluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmVsZW1lbnQoJ2JvZHknKS5hZGRDbGFzcygnbmdycy10b3VjaGluZycpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGxpc3RlbiBmb3IgbW91c2Vtb3ZlIC8gdG91Y2htb3ZlIGRvY3VtZW50IGV2ZW50c1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZG9jdW1lbnQuYmluZChtb3ZlRXZlbnQsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHByZXZlbnQgZGVmYXVsdFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY3VycmVudENsaWNrID0gY2xpZW50KGUpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vdmVtZW50LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb3Bvc2FsLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG90aGVyLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBlciA9IChzY29wZS5zdGVwIC8gcmFuZ2UpICogMTAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG90aGVyTW9kZWxQb3NpdGlvbiA9ICgoKGluZGV4ID09PSAwID8gbW9kZWxNYXgoKSA6IG1vZGVsTWluKCkpIC0gc2NvcGUubWluKSAvIHJhbmdlKSAqIDEwMDtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGN1cnJlbnRDbGlja1swXSA9PT0gXCJ4XCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNhbGN1bGF0ZSBkZWx0YXNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN1cnJlbnRDbGlja1swXSAtPSBvcmlnaW5hbENsaWNrWzBdO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudENsaWNrWzFdIC09IG9yaWdpbmFsQ2xpY2tbMV07XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhcyBtb3ZlbWVudCBvY2N1cnJlZCBvbiBlaXRoZXIgYXhpcz9cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vdmVtZW50ID0gW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChwcmV2aW91c0NsaWNrWzBdICE9PSBjdXJyZW50Q2xpY2tbMF0pLCAocHJldmlvdXNDbGlja1sxXSAhPT0gY3VycmVudENsaWNrWzFdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcHJvcG9zZSBhIG1vdmVtZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9wb3NhbCA9IG9yaWdpbmFsUG9zaXRpb24gKyAoKGN1cnJlbnRDbGlja1tvcmllbnRhdGlvbl0gKiAxMDApIC8gKG9yaWVudGF0aW9uID8gJHNsaWRlci5oZWlnaHQoKSA6ICRzbGlkZXIud2lkdGgoKSkpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBub3JtYWxpemUgc28gaXQgY2FuJ3QgbW92ZSBvdXQgb2YgYm91bmRzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9wb3NhbCA9IHJlc3RyaWN0KHByb3Bvc2FsKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNjb3BlLnByZXZlbnRFcXVhbE1pbk1heCkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHBlciA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwZXIgPSAoMSAvIHJhbmdlKSAqIDEwMDsgLy8gcmVzdHJpY3QgdG8gMVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpbmRleCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdGhlck1vZGVsUG9zaXRpb24gPSBvdGhlck1vZGVsUG9zaXRpb24gLSBwZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChpbmRleCA9PT0gMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdGhlck1vZGVsUG9zaXRpb24gPSBvdGhlck1vZGVsUG9zaXRpb24gKyBwZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBjaGVjayB3aGljaCBoYW5kbGUgaXMgYmVpbmcgbW92ZWQgYW5kIGFkZCAvIHJlbW92ZSBtYXJnaW5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpbmRleCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb3Bvc2FsID0gcHJvcG9zYWwgPiBvdGhlck1vZGVsUG9zaXRpb24gPyBvdGhlck1vZGVsUG9zaXRpb24gOiBwcm9wb3NhbDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoaW5kZXggPT09IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9wb3NhbCA9IHByb3Bvc2FsIDwgb3RoZXJNb2RlbFBvc2l0aW9uID8gb3RoZXJNb2RlbFBvc2l0aW9uIDogcHJvcG9zYWw7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzY29wZS5zdGVwID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIG9ubHkgY2hhbmdlIGlmIHdlIGFyZSB3aXRoaW4gdGhlIGV4dHJlbWVzLCBvdGhlcndpc2Ugd2UgZ2V0IHN0cmFuZ2Ugcm91bmRpbmdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocHJvcG9zYWwgPCAxMDAgJiYgcHJvcG9zYWwgPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb3Bvc2FsID0gTWF0aC5yb3VuZChwcm9wb3NhbCAvIHBlcikgKiBwZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocHJvcG9zYWwgPiA5NSAmJiBpbmRleCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRoYW5kbGUuY3NzKCd6LWluZGV4JywgMyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRoYW5kbGUuY3NzKCd6LWluZGV4JywgJycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobW92ZW1lbnRbb3JpZW50YXRpb25dICYmIHByb3Bvc2FsICE9IHByZXZpb3VzUHJvcG9zYWwpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpbmRleCA9PT0gMCkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHVwZGF0ZSBtb2RlbCBhcyB3ZSBzbGlkZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbE1pbihwYXJzZUZsb2F0KHBhcnNlRmxvYXQoKCgocHJvcG9zYWwgKiByYW5nZSkgLyAxMDApICsgc2NvcGUubWluKSkudG9GaXhlZChzY29wZS5kZWNpbWFsUGxhY2VzKSkpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChpbmRleCA9PT0gMSkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsTWF4KHBhcnNlRmxvYXQocGFyc2VGbG9hdCgoKChwcm9wb3NhbCAqIHJhbmdlKSAvIDEwMCkgKyBzY29wZS5taW4pKS50b0ZpeGVkKHNjb3BlLmRlY2ltYWxQbGFjZXMpKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXBkYXRlIGFuZ3VsYXJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS4kYXBwbHkoKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByZXZpb3VzUHJvcG9zYWwgPSBwcm9wb3NhbDtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmV2aW91c0NsaWNrID0gY3VycmVudENsaWNrO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLmJpbmQob2ZmRXZlbnQsIGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYW5ndWxhci5pc0Z1bmN0aW9uKHNjb3BlLm9uSGFuZGxlVXApKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUub25IYW5kbGVVcCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB1bmJpbmQgbGlzdGVuZXJzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZG9jdW1lbnQub2ZmKG1vdmVFdmVudCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZG9jdW1lbnQub2ZmKG9mZkV2ZW50KTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5lbGVtZW50KCdib2R5JykucmVtb3ZlQ2xhc3MoJ25ncnMtdG91Y2hpbmcnKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gY2FuY2VsIGRvd24gZmxhZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZG93biA9IGZhbHNlO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyByZW1vdmUgZG93biBhbmQgb3ZlciBjbGFzc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGhhbmRsZS5yZW1vdmVDbGFzcygnbmdycy1kb3duJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkaGFuZGxlLnJlbW92ZUNsYXNzKCduZ3JzLW92ZXInKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcmVtb3ZlIGFjdGl2ZSBjbGFzc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNsaWRlci5yZW1vdmVDbGFzcygnbmdycy1mb2N1cyAnICsgaGFuZGxlRG93bkNsYXNzKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLm9uKG92ZXJFdmVudCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRoYW5kbGUuYWRkQ2xhc3MoJ25ncnMtb3ZlcicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSkub24ob3V0RXZlbnQsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWRvd24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGhhbmRsZS5yZW1vdmVDbGFzcygnbmdycy1vdmVyJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbiB0aHJvd0Vycm9yKG1lc3NhZ2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmRpc2FibGVkID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignUmFuZ2VTbGlkZXI6ICcgKyBtZXNzYWdlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIHRocm93V2FybmluZyhtZXNzYWdlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkbG9nLndhcm4obWVzc2FnZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAgICAgICAgICogREVTVFJPWVxuICAgICAgICAgICAgICAgICAgICAgKi9cblxuICAgICAgICAgICAgICAgICAgICBzY29wZS4kb24oJyRkZXN0cm95JywgZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHVuYmluZCBldmVudCBmcm9tIHNsaWRlclxuICAgICAgICAgICAgICAgICAgICAgICAgJHNsaWRlci5vZmYoZXZlbnROYW1lc3BhY2UpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1bmJpbmQgZnJvbSBib2R5XG4gICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmVsZW1lbnQoJ2JvZHknKS5vZmYoZXZlbnROYW1lc3BhY2UpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1bmJpbmQgZnJvbSBkb2N1bWVudFxuICAgICAgICAgICAgICAgICAgICAgICAgJGRvY3VtZW50Lm9mZihldmVudE5hbWVzcGFjZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHVuYmluZCBmcm9tIGhhbmRsZXNcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwLCBsID0gaGFuZGxlcy5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVzW2ldLm9mZihldmVudE5hbWVzcGFjZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlc1tpXS5vZmYoZXZlbnROYW1lc3BhY2UgKyAnWCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICAgICAgICAgKiBJTklUXG4gICAgICAgICAgICAgICAgICAgICAqL1xuXG4gICAgICAgICAgICAgICAgICAgICRzbGlkZXJcbiAgICAgICAgICAgICAgICAgICAgLy8gZGlzYWJsZSBzZWxlY3Rpb25cbiAgICAgICAgICAgICAgICAgICAgICAgIC5iaW5kKCdzZWxlY3RzdGFydCcgKyBldmVudE5hbWVzcGFjZSwgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gc3RvcCBwcm9wYWdhdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgLmJpbmQoJ2NsaWNrJywgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGJpbmQgZXZlbnRzIHRvIGVhY2ggaGFuZGxlXG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZU1vdmUoMCk7XG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZU1vdmUoMSk7XG5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuICAgICAgICB9XSk7XG5cbiAgICAvLyByZXF1ZXN0QW5pbWF0aW9uRnJhbWVQb2x5RmlsbFxuICAgIC8vIGh0dHA6Ly93d3cucGF1bGlyaXNoLmNvbS8yMDExL3JlcXVlc3RhbmltYXRpb25mcmFtZS1mb3Itc21hcnQtYW5pbWF0aW5nL1xuICAgIC8vIHNoaW0gbGF5ZXIgd2l0aCBzZXRUaW1lb3V0IGZhbGxiYWNrXG4gICAgd2luZG93LnJlcXVlc3RBbmltRnJhbWUgPSAoZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICAgICAgICB3aW5kb3cud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICAgICAgICB3aW5kb3cubW96UmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICAgICAgICBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgICAgICAgIHdpbmRvdy5zZXRUaW1lb3V0KGNhbGxiYWNrLCAxMDAwIC8gNjApO1xuICAgICAgICAgICAgfTtcbiAgICB9KSgpO1xufSgpKTtcbiIsIi8qIVxuICogTWFzb25yeSBQQUNLQUdFRCB2NC4wLjBcbiAqIENhc2NhZGluZyBncmlkIGxheW91dCBsaWJyYXJ5XG4gKiBodHRwOi8vbWFzb25yeS5kZXNhbmRyby5jb21cbiAqIE1JVCBMaWNlbnNlXG4gKiBieSBEYXZpZCBEZVNhbmRyb1xuICovXG5cbiFmdW5jdGlvbih0LGUpe1widXNlIHN0cmljdFwiO1wiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoXCJqcXVlcnktYnJpZGdldC9qcXVlcnktYnJpZGdldFwiLFtcImpxdWVyeVwiXSxmdW5jdGlvbihpKXtlKHQsaSl9KTpcIm9iamVjdFwiPT10eXBlb2YgbW9kdWxlJiZtb2R1bGUuZXhwb3J0cz9tb2R1bGUuZXhwb3J0cz1lKHQscmVxdWlyZShcImpxdWVyeVwiKSk6dC5qUXVlcnlCcmlkZ2V0PWUodCx0LmpRdWVyeSl9KHdpbmRvdyxmdW5jdGlvbih0LGUpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGkoaSxyLGEpe2Z1bmN0aW9uIGgodCxlLG4pe3ZhciBvLHI9XCIkKCkuXCIraSsnKFwiJytlKydcIiknO3JldHVybiB0LmVhY2goZnVuY3Rpb24odCxoKXt2YXIgdT1hLmRhdGEoaCxpKTtpZighdSlyZXR1cm4gdm9pZCBzKGkrXCIgbm90IGluaXRpYWxpemVkLiBDYW5ub3QgY2FsbCBtZXRob2RzLCBpLmUuIFwiK3IpO3ZhciBkPXVbZV07aWYoIWR8fFwiX1wiPT1lLmNoYXJBdCgwKSlyZXR1cm4gdm9pZCBzKHIrXCIgaXMgbm90IGEgdmFsaWQgbWV0aG9kXCIpO3ZhciBjPWQuYXBwbHkodSxuKTtvPXZvaWQgMD09PW8/YzpvfSksdm9pZCAwIT09bz9vOnR9ZnVuY3Rpb24gdSh0LGUpe3QuZWFjaChmdW5jdGlvbih0LG4pe3ZhciBvPWEuZGF0YShuLGkpO28/KG8ub3B0aW9uKGUpLG8uX2luaXQoKSk6KG89bmV3IHIobixlKSxhLmRhdGEobixpLG8pKX0pfWE9YXx8ZXx8dC5qUXVlcnksYSYmKHIucHJvdG90eXBlLm9wdGlvbnx8KHIucHJvdG90eXBlLm9wdGlvbj1mdW5jdGlvbih0KXthLmlzUGxhaW5PYmplY3QodCkmJih0aGlzLm9wdGlvbnM9YS5leHRlbmQoITAsdGhpcy5vcHRpb25zLHQpKX0pLGEuZm5baV09ZnVuY3Rpb24odCl7aWYoXCJzdHJpbmdcIj09dHlwZW9mIHQpe3ZhciBlPW8uY2FsbChhcmd1bWVudHMsMSk7cmV0dXJuIGgodGhpcyx0LGUpfXJldHVybiB1KHRoaXMsdCksdGhpc30sbihhKSl9ZnVuY3Rpb24gbih0KXshdHx8dCYmdC5icmlkZ2V0fHwodC5icmlkZ2V0PWkpfXZhciBvPUFycmF5LnByb3RvdHlwZS5zbGljZSxyPXQuY29uc29sZSxzPVwidW5kZWZpbmVkXCI9PXR5cGVvZiByP2Z1bmN0aW9uKCl7fTpmdW5jdGlvbih0KXtyLmVycm9yKHQpfTtyZXR1cm4gbihlfHx0LmpRdWVyeSksaX0pLGZ1bmN0aW9uKHQsZSl7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShcImV2LWVtaXR0ZXIvZXYtZW1pdHRlclwiLGUpOlwib2JqZWN0XCI9PXR5cGVvZiBtb2R1bGUmJm1vZHVsZS5leHBvcnRzP21vZHVsZS5leHBvcnRzPWUoKTp0LkV2RW1pdHRlcj1lKCl9KHRoaXMsZnVuY3Rpb24oKXtmdW5jdGlvbiB0KCl7fXZhciBlPXQucHJvdG90eXBlO3JldHVybiBlLm9uPWZ1bmN0aW9uKHQsZSl7aWYodCYmZSl7dmFyIGk9dGhpcy5fZXZlbnRzPXRoaXMuX2V2ZW50c3x8e30sbj1pW3RdPWlbdF18fFtdO3JldHVybi0xPT1uLmluZGV4T2YoZSkmJm4ucHVzaChlKSx0aGlzfX0sZS5vbmNlPWZ1bmN0aW9uKHQsZSl7aWYodCYmZSl7dGhpcy5vbih0LGUpO3ZhciBpPXRoaXMuX29uY2VFdmVudHM9dGhpcy5fb25jZUV2ZW50c3x8e30sbj1pW3RdPWlbdF18fFtdO3JldHVybiBuW2VdPSEwLHRoaXN9fSxlLm9mZj1mdW5jdGlvbih0LGUpe3ZhciBpPXRoaXMuX2V2ZW50cyYmdGhpcy5fZXZlbnRzW3RdO2lmKGkmJmkubGVuZ3RoKXt2YXIgbj1pLmluZGV4T2YoZSk7cmV0dXJuLTEhPW4mJmkuc3BsaWNlKG4sMSksdGhpc319LGUuZW1pdEV2ZW50PWZ1bmN0aW9uKHQsZSl7dmFyIGk9dGhpcy5fZXZlbnRzJiZ0aGlzLl9ldmVudHNbdF07aWYoaSYmaS5sZW5ndGgpe3ZhciBuPTAsbz1pW25dO2U9ZXx8W107Zm9yKHZhciByPXRoaXMuX29uY2VFdmVudHMmJnRoaXMuX29uY2VFdmVudHNbdF07bzspe3ZhciBzPXImJnJbb107cyYmKHRoaXMub2ZmKHQsbyksZGVsZXRlIHJbb10pLG8uYXBwbHkodGhpcyxlKSxuKz1zPzA6MSxvPWlbbl19cmV0dXJuIHRoaXN9fSx0fSksZnVuY3Rpb24odCxlKXtcInVzZSBzdHJpY3RcIjtcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKFwiZ2V0LXNpemUvZ2V0LXNpemVcIixbXSxmdW5jdGlvbigpe3JldHVybiBlKCl9KTpcIm9iamVjdFwiPT10eXBlb2YgbW9kdWxlJiZtb2R1bGUuZXhwb3J0cz9tb2R1bGUuZXhwb3J0cz1lKCk6dC5nZXRTaXplPWUoKX0od2luZG93LGZ1bmN0aW9uKCl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gdCh0KXt2YXIgZT1wYXJzZUZsb2F0KHQpLGk9LTE9PXQuaW5kZXhPZihcIiVcIikmJiFpc05hTihlKTtyZXR1cm4gaSYmZX1mdW5jdGlvbiBlKCl7fWZ1bmN0aW9uIGkoKXtmb3IodmFyIHQ9e3dpZHRoOjAsaGVpZ2h0OjAsaW5uZXJXaWR0aDowLGlubmVySGVpZ2h0OjAsb3V0ZXJXaWR0aDowLG91dGVySGVpZ2h0OjB9LGU9MDt1PmU7ZSsrKXt2YXIgaT1oW2VdO3RbaV09MH1yZXR1cm4gdH1mdW5jdGlvbiBuKHQpe3ZhciBlPWdldENvbXB1dGVkU3R5bGUodCk7cmV0dXJuIGV8fGEoXCJTdHlsZSByZXR1cm5lZCBcIitlK1wiLiBBcmUgeW91IHJ1bm5pbmcgdGhpcyBjb2RlIGluIGEgaGlkZGVuIGlmcmFtZSBvbiBGaXJlZm94PyBTZWUgaHR0cDovL2JpdC5seS9nZXRzaXplYnVnMVwiKSxlfWZ1bmN0aW9uIG8oKXtpZighZCl7ZD0hMDt2YXIgZT1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO2Uuc3R5bGUud2lkdGg9XCIyMDBweFwiLGUuc3R5bGUucGFkZGluZz1cIjFweCAycHggM3B4IDRweFwiLGUuc3R5bGUuYm9yZGVyU3R5bGU9XCJzb2xpZFwiLGUuc3R5bGUuYm9yZGVyV2lkdGg9XCIxcHggMnB4IDNweCA0cHhcIixlLnN0eWxlLmJveFNpemluZz1cImJvcmRlci1ib3hcIjt2YXIgaT1kb2N1bWVudC5ib2R5fHxkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7aS5hcHBlbmRDaGlsZChlKTt2YXIgbz1uKGUpO3IuaXNCb3hTaXplT3V0ZXI9cz0yMDA9PXQoby53aWR0aCksaS5yZW1vdmVDaGlsZChlKX19ZnVuY3Rpb24gcihlKXtpZihvKCksXCJzdHJpbmdcIj09dHlwZW9mIGUmJihlPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZSkpLGUmJlwib2JqZWN0XCI9PXR5cGVvZiBlJiZlLm5vZGVUeXBlKXt2YXIgcj1uKGUpO2lmKFwibm9uZVwiPT1yLmRpc3BsYXkpcmV0dXJuIGkoKTt2YXIgYT17fTthLndpZHRoPWUub2Zmc2V0V2lkdGgsYS5oZWlnaHQ9ZS5vZmZzZXRIZWlnaHQ7Zm9yKHZhciBkPWEuaXNCb3JkZXJCb3g9XCJib3JkZXItYm94XCI9PXIuYm94U2l6aW5nLGM9MDt1PmM7YysrKXt2YXIgbD1oW2NdLGY9cltsXSxtPXBhcnNlRmxvYXQoZik7YVtsXT1pc05hTihtKT8wOm19dmFyIHA9YS5wYWRkaW5nTGVmdCthLnBhZGRpbmdSaWdodCxnPWEucGFkZGluZ1RvcCthLnBhZGRpbmdCb3R0b20seT1hLm1hcmdpbkxlZnQrYS5tYXJnaW5SaWdodCx2PWEubWFyZ2luVG9wK2EubWFyZ2luQm90dG9tLF89YS5ib3JkZXJMZWZ0V2lkdGgrYS5ib3JkZXJSaWdodFdpZHRoLEU9YS5ib3JkZXJUb3BXaWR0aCthLmJvcmRlckJvdHRvbVdpZHRoLHo9ZCYmcyxiPXQoci53aWR0aCk7YiE9PSExJiYoYS53aWR0aD1iKyh6PzA6cCtfKSk7dmFyIHg9dChyLmhlaWdodCk7cmV0dXJuIHghPT0hMSYmKGEuaGVpZ2h0PXgrKHo/MDpnK0UpKSxhLmlubmVyV2lkdGg9YS53aWR0aC0ocCtfKSxhLmlubmVySGVpZ2h0PWEuaGVpZ2h0LShnK0UpLGEub3V0ZXJXaWR0aD1hLndpZHRoK3ksYS5vdXRlckhlaWdodD1hLmhlaWdodCt2LGF9fXZhciBzLGE9XCJ1bmRlZmluZWRcIj09dHlwZW9mIGNvbnNvbGU/ZTpmdW5jdGlvbih0KXtjb25zb2xlLmVycm9yKHQpfSxoPVtcInBhZGRpbmdMZWZ0XCIsXCJwYWRkaW5nUmlnaHRcIixcInBhZGRpbmdUb3BcIixcInBhZGRpbmdCb3R0b21cIixcIm1hcmdpbkxlZnRcIixcIm1hcmdpblJpZ2h0XCIsXCJtYXJnaW5Ub3BcIixcIm1hcmdpbkJvdHRvbVwiLFwiYm9yZGVyTGVmdFdpZHRoXCIsXCJib3JkZXJSaWdodFdpZHRoXCIsXCJib3JkZXJUb3BXaWR0aFwiLFwiYm9yZGVyQm90dG9tV2lkdGhcIl0sdT1oLmxlbmd0aCxkPSExO3JldHVybiByfSksZnVuY3Rpb24odCxlKXtcInVzZSBzdHJpY3RcIjtcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKFwibWF0Y2hlcy1zZWxlY3Rvci9tYXRjaGVzLXNlbGVjdG9yXCIsZSk6XCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZSYmbW9kdWxlLmV4cG9ydHM/bW9kdWxlLmV4cG9ydHM9ZSgpOnQubWF0Y2hlc1NlbGVjdG9yPWUoKX0od2luZG93LGZ1bmN0aW9uKCl7XCJ1c2Ugc3RyaWN0XCI7dmFyIHQ9ZnVuY3Rpb24oKXt2YXIgdD1FbGVtZW50LnByb3RvdHlwZTtpZih0Lm1hdGNoZXMpcmV0dXJuXCJtYXRjaGVzXCI7aWYodC5tYXRjaGVzU2VsZWN0b3IpcmV0dXJuXCJtYXRjaGVzU2VsZWN0b3JcIjtmb3IodmFyIGU9W1wid2Via2l0XCIsXCJtb3pcIixcIm1zXCIsXCJvXCJdLGk9MDtpPGUubGVuZ3RoO2krKyl7dmFyIG49ZVtpXSxvPW4rXCJNYXRjaGVzU2VsZWN0b3JcIjtpZih0W29dKXJldHVybiBvfX0oKTtyZXR1cm4gZnVuY3Rpb24oZSxpKXtyZXR1cm4gZVt0XShpKX19KSxmdW5jdGlvbih0LGUpe1widXNlIHN0cmljdFwiO1wiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoXCJmaXp6eS11aS11dGlscy91dGlsc1wiLFtcIm1hdGNoZXMtc2VsZWN0b3IvbWF0Y2hlcy1zZWxlY3RvclwiXSxmdW5jdGlvbihpKXtyZXR1cm4gZSh0LGkpfSk6XCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZSYmbW9kdWxlLmV4cG9ydHM/bW9kdWxlLmV4cG9ydHM9ZSh0LHJlcXVpcmUoXCJkZXNhbmRyby1tYXRjaGVzLXNlbGVjdG9yXCIpKTp0LmZpenp5VUlVdGlscz1lKHQsdC5tYXRjaGVzU2VsZWN0b3IpfSh3aW5kb3csZnVuY3Rpb24odCxlKXt2YXIgaT17fTtpLmV4dGVuZD1mdW5jdGlvbih0LGUpe2Zvcih2YXIgaSBpbiBlKXRbaV09ZVtpXTtyZXR1cm4gdH0saS5tb2R1bG89ZnVuY3Rpb24odCxlKXtyZXR1cm4odCVlK2UpJWV9LGkubWFrZUFycmF5PWZ1bmN0aW9uKHQpe3ZhciBlPVtdO2lmKEFycmF5LmlzQXJyYXkodCkpZT10O2Vsc2UgaWYodCYmXCJudW1iZXJcIj09dHlwZW9mIHQubGVuZ3RoKWZvcih2YXIgaT0wO2k8dC5sZW5ndGg7aSsrKWUucHVzaCh0W2ldKTtlbHNlIGUucHVzaCh0KTtyZXR1cm4gZX0saS5yZW1vdmVGcm9tPWZ1bmN0aW9uKHQsZSl7dmFyIGk9dC5pbmRleE9mKGUpOy0xIT1pJiZ0LnNwbGljZShpLDEpfSxpLmdldFBhcmVudD1mdW5jdGlvbih0LGkpe2Zvcig7dCE9ZG9jdW1lbnQuYm9keTspaWYodD10LnBhcmVudE5vZGUsZSh0LGkpKXJldHVybiB0fSxpLmdldFF1ZXJ5RWxlbWVudD1mdW5jdGlvbih0KXtyZXR1cm5cInN0cmluZ1wiPT10eXBlb2YgdD9kb2N1bWVudC5xdWVyeVNlbGVjdG9yKHQpOnR9LGkuaGFuZGxlRXZlbnQ9ZnVuY3Rpb24odCl7dmFyIGU9XCJvblwiK3QudHlwZTt0aGlzW2VdJiZ0aGlzW2VdKHQpfSxpLmZpbHRlckZpbmRFbGVtZW50cz1mdW5jdGlvbih0LG4pe3Q9aS5tYWtlQXJyYXkodCk7dmFyIG89W107cmV0dXJuIHQuZm9yRWFjaChmdW5jdGlvbih0KXtpZih0IGluc3RhbmNlb2YgSFRNTEVsZW1lbnQpe2lmKCFuKXJldHVybiB2b2lkIG8ucHVzaCh0KTtlKHQsbikmJm8ucHVzaCh0KTtmb3IodmFyIGk9dC5xdWVyeVNlbGVjdG9yQWxsKG4pLHI9MDtyPGkubGVuZ3RoO3IrKylvLnB1c2goaVtyXSl9fSksb30saS5kZWJvdW5jZU1ldGhvZD1mdW5jdGlvbih0LGUsaSl7dmFyIG49dC5wcm90b3R5cGVbZV0sbz1lK1wiVGltZW91dFwiO3QucHJvdG90eXBlW2VdPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpc1tvXTt0JiZjbGVhclRpbWVvdXQodCk7dmFyIGU9YXJndW1lbnRzLHI9dGhpczt0aGlzW29dPXNldFRpbWVvdXQoZnVuY3Rpb24oKXtuLmFwcGx5KHIsZSksZGVsZXRlIHJbb119LGl8fDEwMCl9fSxpLmRvY1JlYWR5PWZ1bmN0aW9uKHQpe1wiY29tcGxldGVcIj09ZG9jdW1lbnQucmVhZHlTdGF0ZT90KCk6ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIix0KX0saS50b0Rhc2hlZD1mdW5jdGlvbih0KXtyZXR1cm4gdC5yZXBsYWNlKC8oLikoW0EtWl0pL2csZnVuY3Rpb24odCxlLGkpe3JldHVybiBlK1wiLVwiK2l9KS50b0xvd2VyQ2FzZSgpfTt2YXIgbj10LmNvbnNvbGU7cmV0dXJuIGkuaHRtbEluaXQ9ZnVuY3Rpb24oZSxvKXtpLmRvY1JlYWR5KGZ1bmN0aW9uKCl7dmFyIHI9aS50b0Rhc2hlZChvKSxzPVwiZGF0YS1cIityLGE9ZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIltcIitzK1wiXVwiKSxoPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIuanMtXCIrciksdT1pLm1ha2VBcnJheShhKS5jb25jYXQoaS5tYWtlQXJyYXkoaCkpLGQ9cytcIi1vcHRpb25zXCIsYz10LmpRdWVyeTt1LmZvckVhY2goZnVuY3Rpb24odCl7dmFyIGkscj10LmdldEF0dHJpYnV0ZShzKXx8dC5nZXRBdHRyaWJ1dGUoZCk7dHJ5e2k9ciYmSlNPTi5wYXJzZShyKX1jYXRjaChhKXtyZXR1cm4gdm9pZChuJiZuLmVycm9yKFwiRXJyb3IgcGFyc2luZyBcIitzK1wiIG9uIFwiK3QuY2xhc3NOYW1lK1wiOiBcIithKSl9dmFyIGg9bmV3IGUodCxpKTtjJiZjLmRhdGEodCxvLGgpfSl9KX0saX0pLGZ1bmN0aW9uKHQsZSl7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShcIm91dGxheWVyL2l0ZW1cIixbXCJldi1lbWl0dGVyL2V2LWVtaXR0ZXJcIixcImdldC1zaXplL2dldC1zaXplXCJdLGZ1bmN0aW9uKGksbil7cmV0dXJuIGUodCxpLG4pfSk6XCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZSYmbW9kdWxlLmV4cG9ydHM/bW9kdWxlLmV4cG9ydHM9ZSh0LHJlcXVpcmUoXCJldi1lbWl0dGVyXCIpLHJlcXVpcmUoXCJnZXQtc2l6ZVwiKSk6KHQuT3V0bGF5ZXI9e30sdC5PdXRsYXllci5JdGVtPWUodCx0LkV2RW1pdHRlcix0LmdldFNpemUpKX0od2luZG93LGZ1bmN0aW9uKHQsZSxpKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBuKHQpe2Zvcih2YXIgZSBpbiB0KXJldHVybiExO3JldHVybiBlPW51bGwsITB9ZnVuY3Rpb24gbyh0LGUpe3QmJih0aGlzLmVsZW1lbnQ9dCx0aGlzLmxheW91dD1lLHRoaXMucG9zaXRpb249e3g6MCx5OjB9LHRoaXMuX2NyZWF0ZSgpKX1mdW5jdGlvbiByKHQpe3JldHVybiB0LnJlcGxhY2UoLyhbQS1aXSkvZyxmdW5jdGlvbih0KXtyZXR1cm5cIi1cIit0LnRvTG93ZXJDYXNlKCl9KX12YXIgcz1kb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUsYT1cInN0cmluZ1wiPT10eXBlb2Ygcy50cmFuc2l0aW9uP1widHJhbnNpdGlvblwiOlwiV2Via2l0VHJhbnNpdGlvblwiLGg9XCJzdHJpbmdcIj09dHlwZW9mIHMudHJhbnNmb3JtP1widHJhbnNmb3JtXCI6XCJXZWJraXRUcmFuc2Zvcm1cIix1PXtXZWJraXRUcmFuc2l0aW9uOlwid2Via2l0VHJhbnNpdGlvbkVuZFwiLHRyYW5zaXRpb246XCJ0cmFuc2l0aW9uZW5kXCJ9W2FdLGQ9W2gsYSxhK1wiRHVyYXRpb25cIixhK1wiUHJvcGVydHlcIl0sYz1vLnByb3RvdHlwZT1PYmplY3QuY3JlYXRlKGUucHJvdG90eXBlKTtjLmNvbnN0cnVjdG9yPW8sYy5fY3JlYXRlPWZ1bmN0aW9uKCl7dGhpcy5fdHJhbnNuPXtpbmdQcm9wZXJ0aWVzOnt9LGNsZWFuOnt9LG9uRW5kOnt9fSx0aGlzLmNzcyh7cG9zaXRpb246XCJhYnNvbHV0ZVwifSl9LGMuaGFuZGxlRXZlbnQ9ZnVuY3Rpb24odCl7dmFyIGU9XCJvblwiK3QudHlwZTt0aGlzW2VdJiZ0aGlzW2VdKHQpfSxjLmdldFNpemU9ZnVuY3Rpb24oKXt0aGlzLnNpemU9aSh0aGlzLmVsZW1lbnQpfSxjLmNzcz1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmVsZW1lbnQuc3R5bGU7Zm9yKHZhciBpIGluIHQpe3ZhciBuPWRbaV18fGk7ZVtuXT10W2ldfX0sYy5nZXRQb3NpdGlvbj1mdW5jdGlvbigpe3ZhciB0PWdldENvbXB1dGVkU3R5bGUodGhpcy5lbGVtZW50KSxlPXRoaXMubGF5b3V0Ll9nZXRPcHRpb24oXCJvcmlnaW5MZWZ0XCIpLGk9dGhpcy5sYXlvdXQuX2dldE9wdGlvbihcIm9yaWdpblRvcFwiKSxuPXRbZT9cImxlZnRcIjpcInJpZ2h0XCJdLG89dFtpP1widG9wXCI6XCJib3R0b21cIl0scj10aGlzLmxheW91dC5zaXplLHM9LTEhPW4uaW5kZXhPZihcIiVcIik/cGFyc2VGbG9hdChuKS8xMDAqci53aWR0aDpwYXJzZUludChuLDEwKSxhPS0xIT1vLmluZGV4T2YoXCIlXCIpP3BhcnNlRmxvYXQobykvMTAwKnIuaGVpZ2h0OnBhcnNlSW50KG8sMTApO3M9aXNOYU4ocyk/MDpzLGE9aXNOYU4oYSk/MDphLHMtPWU/ci5wYWRkaW5nTGVmdDpyLnBhZGRpbmdSaWdodCxhLT1pP3IucGFkZGluZ1RvcDpyLnBhZGRpbmdCb3R0b20sdGhpcy5wb3NpdGlvbi54PXMsdGhpcy5wb3NpdGlvbi55PWF9LGMubGF5b3V0UG9zaXRpb249ZnVuY3Rpb24oKXt2YXIgdD10aGlzLmxheW91dC5zaXplLGU9e30saT10aGlzLmxheW91dC5fZ2V0T3B0aW9uKFwib3JpZ2luTGVmdFwiKSxuPXRoaXMubGF5b3V0Ll9nZXRPcHRpb24oXCJvcmlnaW5Ub3BcIiksbz1pP1wicGFkZGluZ0xlZnRcIjpcInBhZGRpbmdSaWdodFwiLHI9aT9cImxlZnRcIjpcInJpZ2h0XCIscz1pP1wicmlnaHRcIjpcImxlZnRcIixhPXRoaXMucG9zaXRpb24ueCt0W29dO2Vbcl09dGhpcy5nZXRYVmFsdWUoYSksZVtzXT1cIlwiO3ZhciBoPW4/XCJwYWRkaW5nVG9wXCI6XCJwYWRkaW5nQm90dG9tXCIsdT1uP1widG9wXCI6XCJib3R0b21cIixkPW4/XCJib3R0b21cIjpcInRvcFwiLGM9dGhpcy5wb3NpdGlvbi55K3RbaF07ZVt1XT10aGlzLmdldFlWYWx1ZShjKSxlW2RdPVwiXCIsdGhpcy5jc3MoZSksdGhpcy5lbWl0RXZlbnQoXCJsYXlvdXRcIixbdGhpc10pfSxjLmdldFhWYWx1ZT1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmxheW91dC5fZ2V0T3B0aW9uKFwiaG9yaXpvbnRhbFwiKTtyZXR1cm4gdGhpcy5sYXlvdXQub3B0aW9ucy5wZXJjZW50UG9zaXRpb24mJiFlP3QvdGhpcy5sYXlvdXQuc2l6ZS53aWR0aCoxMDArXCIlXCI6dCtcInB4XCJ9LGMuZ2V0WVZhbHVlPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMubGF5b3V0Ll9nZXRPcHRpb24oXCJob3Jpem9udGFsXCIpO3JldHVybiB0aGlzLmxheW91dC5vcHRpb25zLnBlcmNlbnRQb3NpdGlvbiYmZT90L3RoaXMubGF5b3V0LnNpemUuaGVpZ2h0KjEwMCtcIiVcIjp0K1wicHhcIn0sYy5fdHJhbnNpdGlvblRvPWZ1bmN0aW9uKHQsZSl7dGhpcy5nZXRQb3NpdGlvbigpO3ZhciBpPXRoaXMucG9zaXRpb24ueCxuPXRoaXMucG9zaXRpb24ueSxvPXBhcnNlSW50KHQsMTApLHI9cGFyc2VJbnQoZSwxMCkscz1vPT09dGhpcy5wb3NpdGlvbi54JiZyPT09dGhpcy5wb3NpdGlvbi55O2lmKHRoaXMuc2V0UG9zaXRpb24odCxlKSxzJiYhdGhpcy5pc1RyYW5zaXRpb25pbmcpcmV0dXJuIHZvaWQgdGhpcy5sYXlvdXRQb3NpdGlvbigpO3ZhciBhPXQtaSxoPWUtbix1PXt9O3UudHJhbnNmb3JtPXRoaXMuZ2V0VHJhbnNsYXRlKGEsaCksdGhpcy50cmFuc2l0aW9uKHt0bzp1LG9uVHJhbnNpdGlvbkVuZDp7dHJhbnNmb3JtOnRoaXMubGF5b3V0UG9zaXRpb259LGlzQ2xlYW5pbmc6ITB9KX0sYy5nZXRUcmFuc2xhdGU9ZnVuY3Rpb24odCxlKXt2YXIgaT10aGlzLmxheW91dC5fZ2V0T3B0aW9uKFwib3JpZ2luTGVmdFwiKSxuPXRoaXMubGF5b3V0Ll9nZXRPcHRpb24oXCJvcmlnaW5Ub3BcIik7cmV0dXJuIHQ9aT90Oi10LGU9bj9lOi1lLFwidHJhbnNsYXRlM2QoXCIrdCtcInB4LCBcIitlK1wicHgsIDApXCJ9LGMuZ29Ubz1mdW5jdGlvbih0LGUpe3RoaXMuc2V0UG9zaXRpb24odCxlKSx0aGlzLmxheW91dFBvc2l0aW9uKCl9LGMubW92ZVRvPWMuX3RyYW5zaXRpb25UbyxjLnNldFBvc2l0aW9uPWZ1bmN0aW9uKHQsZSl7dGhpcy5wb3NpdGlvbi54PXBhcnNlSW50KHQsMTApLHRoaXMucG9zaXRpb24ueT1wYXJzZUludChlLDEwKX0sYy5fbm9uVHJhbnNpdGlvbj1mdW5jdGlvbih0KXt0aGlzLmNzcyh0LnRvKSx0LmlzQ2xlYW5pbmcmJnRoaXMuX3JlbW92ZVN0eWxlcyh0LnRvKTtmb3IodmFyIGUgaW4gdC5vblRyYW5zaXRpb25FbmQpdC5vblRyYW5zaXRpb25FbmRbZV0uY2FsbCh0aGlzKX0sYy5fdHJhbnNpdGlvbj1mdW5jdGlvbih0KXtpZighcGFyc2VGbG9hdCh0aGlzLmxheW91dC5vcHRpb25zLnRyYW5zaXRpb25EdXJhdGlvbikpcmV0dXJuIHZvaWQgdGhpcy5fbm9uVHJhbnNpdGlvbih0KTt2YXIgZT10aGlzLl90cmFuc247Zm9yKHZhciBpIGluIHQub25UcmFuc2l0aW9uRW5kKWUub25FbmRbaV09dC5vblRyYW5zaXRpb25FbmRbaV07Zm9yKGkgaW4gdC50byllLmluZ1Byb3BlcnRpZXNbaV09ITAsdC5pc0NsZWFuaW5nJiYoZS5jbGVhbltpXT0hMCk7aWYodC5mcm9tKXt0aGlzLmNzcyh0LmZyb20pO3ZhciBuPXRoaXMuZWxlbWVudC5vZmZzZXRIZWlnaHQ7bj1udWxsfXRoaXMuZW5hYmxlVHJhbnNpdGlvbih0LnRvKSx0aGlzLmNzcyh0LnRvKSx0aGlzLmlzVHJhbnNpdGlvbmluZz0hMH07dmFyIGw9XCJvcGFjaXR5LFwiK3IoZC50cmFuc2Zvcm18fFwidHJhbnNmb3JtXCIpO2MuZW5hYmxlVHJhbnNpdGlvbj1mdW5jdGlvbigpe3RoaXMuaXNUcmFuc2l0aW9uaW5nfHwodGhpcy5jc3Moe3RyYW5zaXRpb25Qcm9wZXJ0eTpsLHRyYW5zaXRpb25EdXJhdGlvbjp0aGlzLmxheW91dC5vcHRpb25zLnRyYW5zaXRpb25EdXJhdGlvbn0pLHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKHUsdGhpcywhMSkpfSxjLnRyYW5zaXRpb249by5wcm90b3R5cGVbYT9cIl90cmFuc2l0aW9uXCI6XCJfbm9uVHJhbnNpdGlvblwiXSxjLm9ud2Via2l0VHJhbnNpdGlvbkVuZD1mdW5jdGlvbih0KXt0aGlzLm9udHJhbnNpdGlvbmVuZCh0KX0sYy5vbm90cmFuc2l0aW9uZW5kPWZ1bmN0aW9uKHQpe3RoaXMub250cmFuc2l0aW9uZW5kKHQpfTt2YXIgZj17XCItd2Via2l0LXRyYW5zZm9ybVwiOlwidHJhbnNmb3JtXCJ9O2Mub250cmFuc2l0aW9uZW5kPWZ1bmN0aW9uKHQpe2lmKHQudGFyZ2V0PT09dGhpcy5lbGVtZW50KXt2YXIgZT10aGlzLl90cmFuc24saT1mW3QucHJvcGVydHlOYW1lXXx8dC5wcm9wZXJ0eU5hbWU7aWYoZGVsZXRlIGUuaW5nUHJvcGVydGllc1tpXSxuKGUuaW5nUHJvcGVydGllcykmJnRoaXMuZGlzYWJsZVRyYW5zaXRpb24oKSxpIGluIGUuY2xlYW4mJih0aGlzLmVsZW1lbnQuc3R5bGVbdC5wcm9wZXJ0eU5hbWVdPVwiXCIsZGVsZXRlIGUuY2xlYW5baV0pLGkgaW4gZS5vbkVuZCl7dmFyIG89ZS5vbkVuZFtpXTtvLmNhbGwodGhpcyksZGVsZXRlIGUub25FbmRbaV19dGhpcy5lbWl0RXZlbnQoXCJ0cmFuc2l0aW9uRW5kXCIsW3RoaXNdKX19LGMuZGlzYWJsZVRyYW5zaXRpb249ZnVuY3Rpb24oKXt0aGlzLnJlbW92ZVRyYW5zaXRpb25TdHlsZXMoKSx0aGlzLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcih1LHRoaXMsITEpLHRoaXMuaXNUcmFuc2l0aW9uaW5nPSExfSxjLl9yZW1vdmVTdHlsZXM9ZnVuY3Rpb24odCl7dmFyIGU9e307Zm9yKHZhciBpIGluIHQpZVtpXT1cIlwiO3RoaXMuY3NzKGUpfTt2YXIgbT17dHJhbnNpdGlvblByb3BlcnR5OlwiXCIsdHJhbnNpdGlvbkR1cmF0aW9uOlwiXCJ9O3JldHVybiBjLnJlbW92ZVRyYW5zaXRpb25TdHlsZXM9ZnVuY3Rpb24oKXt0aGlzLmNzcyhtKX0sYy5yZW1vdmVFbGVtPWZ1bmN0aW9uKCl7dGhpcy5lbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5lbGVtZW50KSx0aGlzLmNzcyh7ZGlzcGxheTpcIlwifSksdGhpcy5lbWl0RXZlbnQoXCJyZW1vdmVcIixbdGhpc10pfSxjLnJlbW92ZT1mdW5jdGlvbigpe3JldHVybiBhJiZwYXJzZUZsb2F0KHRoaXMubGF5b3V0Lm9wdGlvbnMudHJhbnNpdGlvbkR1cmF0aW9uKT8odGhpcy5vbmNlKFwidHJhbnNpdGlvbkVuZFwiLGZ1bmN0aW9uKCl7dGhpcy5yZW1vdmVFbGVtKCl9KSx2b2lkIHRoaXMuaGlkZSgpKTp2b2lkIHRoaXMucmVtb3ZlRWxlbSgpfSxjLnJldmVhbD1mdW5jdGlvbigpe2RlbGV0ZSB0aGlzLmlzSGlkZGVuLHRoaXMuY3NzKHtkaXNwbGF5OlwiXCJ9KTt2YXIgdD10aGlzLmxheW91dC5vcHRpb25zLGU9e30saT10aGlzLmdldEhpZGVSZXZlYWxUcmFuc2l0aW9uRW5kUHJvcGVydHkoXCJ2aXNpYmxlU3R5bGVcIik7ZVtpXT10aGlzLm9uUmV2ZWFsVHJhbnNpdGlvbkVuZCx0aGlzLnRyYW5zaXRpb24oe2Zyb206dC5oaWRkZW5TdHlsZSx0bzp0LnZpc2libGVTdHlsZSxpc0NsZWFuaW5nOiEwLG9uVHJhbnNpdGlvbkVuZDplfSl9LGMub25SZXZlYWxUcmFuc2l0aW9uRW5kPWZ1bmN0aW9uKCl7dGhpcy5pc0hpZGRlbnx8dGhpcy5lbWl0RXZlbnQoXCJyZXZlYWxcIil9LGMuZ2V0SGlkZVJldmVhbFRyYW5zaXRpb25FbmRQcm9wZXJ0eT1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmxheW91dC5vcHRpb25zW3RdO2lmKGUub3BhY2l0eSlyZXR1cm5cIm9wYWNpdHlcIjtmb3IodmFyIGkgaW4gZSlyZXR1cm4gaX0sYy5oaWRlPWZ1bmN0aW9uKCl7dGhpcy5pc0hpZGRlbj0hMCx0aGlzLmNzcyh7ZGlzcGxheTpcIlwifSk7dmFyIHQ9dGhpcy5sYXlvdXQub3B0aW9ucyxlPXt9LGk9dGhpcy5nZXRIaWRlUmV2ZWFsVHJhbnNpdGlvbkVuZFByb3BlcnR5KFwiaGlkZGVuU3R5bGVcIik7ZVtpXT10aGlzLm9uSGlkZVRyYW5zaXRpb25FbmQsdGhpcy50cmFuc2l0aW9uKHtmcm9tOnQudmlzaWJsZVN0eWxlLHRvOnQuaGlkZGVuU3R5bGUsaXNDbGVhbmluZzohMCxvblRyYW5zaXRpb25FbmQ6ZX0pfSxjLm9uSGlkZVRyYW5zaXRpb25FbmQ9ZnVuY3Rpb24oKXt0aGlzLmlzSGlkZGVuJiYodGhpcy5jc3Moe2Rpc3BsYXk6XCJub25lXCJ9KSx0aGlzLmVtaXRFdmVudChcImhpZGVcIikpfSxjLmRlc3Ryb3k9ZnVuY3Rpb24oKXt0aGlzLmNzcyh7cG9zaXRpb246XCJcIixsZWZ0OlwiXCIscmlnaHQ6XCJcIix0b3A6XCJcIixib3R0b206XCJcIix0cmFuc2l0aW9uOlwiXCIsdHJhbnNmb3JtOlwiXCJ9KX0sb30pLGZ1bmN0aW9uKHQsZSl7XCJ1c2Ugc3RyaWN0XCI7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShcIm91dGxheWVyL291dGxheWVyXCIsW1wiZXYtZW1pdHRlci9ldi1lbWl0dGVyXCIsXCJnZXQtc2l6ZS9nZXQtc2l6ZVwiLFwiZml6enktdWktdXRpbHMvdXRpbHNcIixcIi4vaXRlbVwiXSxmdW5jdGlvbihpLG4sbyxyKXtyZXR1cm4gZSh0LGksbixvLHIpfSk6XCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZSYmbW9kdWxlLmV4cG9ydHM/bW9kdWxlLmV4cG9ydHM9ZSh0LHJlcXVpcmUoXCJldi1lbWl0dGVyXCIpLHJlcXVpcmUoXCJnZXQtc2l6ZVwiKSxyZXF1aXJlKFwiZml6enktdWktdXRpbHNcIikscmVxdWlyZShcIi4vaXRlbVwiKSk6dC5PdXRsYXllcj1lKHQsdC5FdkVtaXR0ZXIsdC5nZXRTaXplLHQuZml6enlVSVV0aWxzLHQuT3V0bGF5ZXIuSXRlbSl9KHdpbmRvdyxmdW5jdGlvbih0LGUsaSxuLG8pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHIodCxlKXt2YXIgaT1uLmdldFF1ZXJ5RWxlbWVudCh0KTtpZighaSlyZXR1cm4gdm9pZChhJiZhLmVycm9yKFwiQmFkIGVsZW1lbnQgZm9yIFwiK3RoaXMuY29uc3RydWN0b3IubmFtZXNwYWNlK1wiOiBcIisoaXx8dCkpKTt0aGlzLmVsZW1lbnQ9aSxoJiYodGhpcy4kZWxlbWVudD1oKHRoaXMuZWxlbWVudCkpLHRoaXMub3B0aW9ucz1uLmV4dGVuZCh7fSx0aGlzLmNvbnN0cnVjdG9yLmRlZmF1bHRzKSx0aGlzLm9wdGlvbihlKTt2YXIgbz0rK2Q7dGhpcy5lbGVtZW50Lm91dGxheWVyR1VJRD1vLGNbb109dGhpcyx0aGlzLl9jcmVhdGUoKTt2YXIgcj10aGlzLl9nZXRPcHRpb24oXCJpbml0TGF5b3V0XCIpO3ImJnRoaXMubGF5b3V0KCl9ZnVuY3Rpb24gcyh0KXtmdW5jdGlvbiBlKCl7dC5hcHBseSh0aGlzLGFyZ3VtZW50cyl9cmV0dXJuIGUucHJvdG90eXBlPU9iamVjdC5jcmVhdGUodC5wcm90b3R5cGUpLGUucHJvdG90eXBlLmNvbnN0cnVjdG9yPWUsZX12YXIgYT10LmNvbnNvbGUsaD10LmpRdWVyeSx1PWZ1bmN0aW9uKCl7fSxkPTAsYz17fTtyLm5hbWVzcGFjZT1cIm91dGxheWVyXCIsci5JdGVtPW8sci5kZWZhdWx0cz17Y29udGFpbmVyU3R5bGU6e3Bvc2l0aW9uOlwicmVsYXRpdmVcIn0saW5pdExheW91dDohMCxvcmlnaW5MZWZ0OiEwLG9yaWdpblRvcDohMCxyZXNpemU6ITAscmVzaXplQ29udGFpbmVyOiEwLHRyYW5zaXRpb25EdXJhdGlvbjpcIjAuNHNcIixoaWRkZW5TdHlsZTp7b3BhY2l0eTowLHRyYW5zZm9ybTpcInNjYWxlKDAuMDAxKVwifSx2aXNpYmxlU3R5bGU6e29wYWNpdHk6MSx0cmFuc2Zvcm06XCJzY2FsZSgxKVwifX07dmFyIGw9ci5wcm90b3R5cGU7cmV0dXJuIG4uZXh0ZW5kKGwsZS5wcm90b3R5cGUpLGwub3B0aW9uPWZ1bmN0aW9uKHQpe24uZXh0ZW5kKHRoaXMub3B0aW9ucyx0KX0sbC5fZ2V0T3B0aW9uPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuY29uc3RydWN0b3IuY29tcGF0T3B0aW9uc1t0XTtyZXR1cm4gZSYmdm9pZCAwIT09dGhpcy5vcHRpb25zW2VdP3RoaXMub3B0aW9uc1tlXTp0aGlzLm9wdGlvbnNbdF19LHIuY29tcGF0T3B0aW9ucz17aW5pdExheW91dDpcImlzSW5pdExheW91dFwiLGhvcml6b250YWw6XCJpc0hvcml6b250YWxcIixsYXlvdXRJbnN0YW50OlwiaXNMYXlvdXRJbnN0YW50XCIsb3JpZ2luTGVmdDpcImlzT3JpZ2luTGVmdFwiLG9yaWdpblRvcDpcImlzT3JpZ2luVG9wXCIscmVzaXplOlwiaXNSZXNpemVCb3VuZFwiLHJlc2l6ZUNvbnRhaW5lcjpcImlzUmVzaXppbmdDb250YWluZXJcIn0sbC5fY3JlYXRlPWZ1bmN0aW9uKCl7dGhpcy5yZWxvYWRJdGVtcygpLHRoaXMuc3RhbXBzPVtdLHRoaXMuc3RhbXAodGhpcy5vcHRpb25zLnN0YW1wKSxuLmV4dGVuZCh0aGlzLmVsZW1lbnQuc3R5bGUsdGhpcy5vcHRpb25zLmNvbnRhaW5lclN0eWxlKTt2YXIgdD10aGlzLl9nZXRPcHRpb24oXCJyZXNpemVcIik7dCYmdGhpcy5iaW5kUmVzaXplKCl9LGwucmVsb2FkSXRlbXM9ZnVuY3Rpb24oKXt0aGlzLml0ZW1zPXRoaXMuX2l0ZW1pemUodGhpcy5lbGVtZW50LmNoaWxkcmVuKX0sbC5faXRlbWl6ZT1mdW5jdGlvbih0KXtmb3IodmFyIGU9dGhpcy5fZmlsdGVyRmluZEl0ZW1FbGVtZW50cyh0KSxpPXRoaXMuY29uc3RydWN0b3IuSXRlbSxuPVtdLG89MDtvPGUubGVuZ3RoO28rKyl7dmFyIHI9ZVtvXSxzPW5ldyBpKHIsdGhpcyk7bi5wdXNoKHMpfXJldHVybiBufSxsLl9maWx0ZXJGaW5kSXRlbUVsZW1lbnRzPWZ1bmN0aW9uKHQpe3JldHVybiBuLmZpbHRlckZpbmRFbGVtZW50cyh0LHRoaXMub3B0aW9ucy5pdGVtU2VsZWN0b3IpfSxsLmdldEl0ZW1FbGVtZW50cz1mdW5jdGlvbigpe3JldHVybiB0aGlzLml0ZW1zLm1hcChmdW5jdGlvbih0KXtyZXR1cm4gdC5lbGVtZW50fSl9LGwubGF5b3V0PWZ1bmN0aW9uKCl7dGhpcy5fcmVzZXRMYXlvdXQoKSx0aGlzLl9tYW5hZ2VTdGFtcHMoKTt2YXIgdD10aGlzLl9nZXRPcHRpb24oXCJsYXlvdXRJbnN0YW50XCIpLGU9dm9pZCAwIT09dD90OiF0aGlzLl9pc0xheW91dEluaXRlZDt0aGlzLmxheW91dEl0ZW1zKHRoaXMuaXRlbXMsZSksdGhpcy5faXNMYXlvdXRJbml0ZWQ9ITB9LGwuX2luaXQ9bC5sYXlvdXQsbC5fcmVzZXRMYXlvdXQ9ZnVuY3Rpb24oKXt0aGlzLmdldFNpemUoKX0sbC5nZXRTaXplPWZ1bmN0aW9uKCl7dGhpcy5zaXplPWkodGhpcy5lbGVtZW50KX0sbC5fZ2V0TWVhc3VyZW1lbnQ9ZnVuY3Rpb24odCxlKXt2YXIgbixvPXRoaXMub3B0aW9uc1t0XTtvPyhcInN0cmluZ1wiPT10eXBlb2Ygbz9uPXRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKG8pOm8gaW5zdGFuY2VvZiBIVE1MRWxlbWVudCYmKG49byksdGhpc1t0XT1uP2kobilbZV06byk6dGhpc1t0XT0wfSxsLmxheW91dEl0ZW1zPWZ1bmN0aW9uKHQsZSl7dD10aGlzLl9nZXRJdGVtc0ZvckxheW91dCh0KSx0aGlzLl9sYXlvdXRJdGVtcyh0LGUpLHRoaXMuX3Bvc3RMYXlvdXQoKX0sbC5fZ2V0SXRlbXNGb3JMYXlvdXQ9ZnVuY3Rpb24odCl7cmV0dXJuIHQuZmlsdGVyKGZ1bmN0aW9uKHQpe3JldHVybiF0LmlzSWdub3JlZH0pfSxsLl9sYXlvdXRJdGVtcz1mdW5jdGlvbih0LGUpe2lmKHRoaXMuX2VtaXRDb21wbGV0ZU9uSXRlbXMoXCJsYXlvdXRcIix0KSx0JiZ0Lmxlbmd0aCl7dmFyIGk9W107dC5mb3JFYWNoKGZ1bmN0aW9uKHQpe3ZhciBuPXRoaXMuX2dldEl0ZW1MYXlvdXRQb3NpdGlvbih0KTtuLml0ZW09dCxuLmlzSW5zdGFudD1lfHx0LmlzTGF5b3V0SW5zdGFudCxpLnB1c2gobil9LHRoaXMpLHRoaXMuX3Byb2Nlc3NMYXlvdXRRdWV1ZShpKX19LGwuX2dldEl0ZW1MYXlvdXRQb3NpdGlvbj1mdW5jdGlvbigpe3JldHVybnt4OjAseTowfX0sbC5fcHJvY2Vzc0xheW91dFF1ZXVlPWZ1bmN0aW9uKHQpe3QuZm9yRWFjaChmdW5jdGlvbih0KXt0aGlzLl9wb3NpdGlvbkl0ZW0odC5pdGVtLHQueCx0LnksdC5pc0luc3RhbnQpfSx0aGlzKX0sbC5fcG9zaXRpb25JdGVtPWZ1bmN0aW9uKHQsZSxpLG4pe24/dC5nb1RvKGUsaSk6dC5tb3ZlVG8oZSxpKX0sbC5fcG9zdExheW91dD1mdW5jdGlvbigpe3RoaXMucmVzaXplQ29udGFpbmVyKCl9LGwucmVzaXplQ29udGFpbmVyPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5fZ2V0T3B0aW9uKFwicmVzaXplQ29udGFpbmVyXCIpO2lmKHQpe3ZhciBlPXRoaXMuX2dldENvbnRhaW5lclNpemUoKTtlJiYodGhpcy5fc2V0Q29udGFpbmVyTWVhc3VyZShlLndpZHRoLCEwKSx0aGlzLl9zZXRDb250YWluZXJNZWFzdXJlKGUuaGVpZ2h0LCExKSl9fSxsLl9nZXRDb250YWluZXJTaXplPXUsbC5fc2V0Q29udGFpbmVyTWVhc3VyZT1mdW5jdGlvbih0LGUpe2lmKHZvaWQgMCE9PXQpe3ZhciBpPXRoaXMuc2l6ZTtpLmlzQm9yZGVyQm94JiYodCs9ZT9pLnBhZGRpbmdMZWZ0K2kucGFkZGluZ1JpZ2h0K2kuYm9yZGVyTGVmdFdpZHRoK2kuYm9yZGVyUmlnaHRXaWR0aDppLnBhZGRpbmdCb3R0b20raS5wYWRkaW5nVG9wK2kuYm9yZGVyVG9wV2lkdGgraS5ib3JkZXJCb3R0b21XaWR0aCksdD1NYXRoLm1heCh0LDApLHRoaXMuZWxlbWVudC5zdHlsZVtlP1wid2lkdGhcIjpcImhlaWdodFwiXT10K1wicHhcIn19LGwuX2VtaXRDb21wbGV0ZU9uSXRlbXM9ZnVuY3Rpb24odCxlKXtmdW5jdGlvbiBpKCl7by5kaXNwYXRjaEV2ZW50KHQrXCJDb21wbGV0ZVwiLG51bGwsW2VdKX1mdW5jdGlvbiBuKCl7cysrLHM9PXImJmkoKX12YXIgbz10aGlzLHI9ZS5sZW5ndGg7aWYoIWV8fCFyKXJldHVybiB2b2lkIGkoKTt2YXIgcz0wO2UuZm9yRWFjaChmdW5jdGlvbihlKXtlLm9uY2UodCxuKX0pfSxsLmRpc3BhdGNoRXZlbnQ9ZnVuY3Rpb24odCxlLGkpe3ZhciBuPWU/W2VdLmNvbmNhdChpKTppO2lmKHRoaXMuZW1pdEV2ZW50KHQsbiksaClpZih0aGlzLiRlbGVtZW50PXRoaXMuJGVsZW1lbnR8fGgodGhpcy5lbGVtZW50KSxlKXt2YXIgbz1oLkV2ZW50KGUpO28udHlwZT10LHRoaXMuJGVsZW1lbnQudHJpZ2dlcihvLGkpfWVsc2UgdGhpcy4kZWxlbWVudC50cmlnZ2VyKHQsaSl9LGwuaWdub3JlPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuZ2V0SXRlbSh0KTtlJiYoZS5pc0lnbm9yZWQ9ITApfSxsLnVuaWdub3JlPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuZ2V0SXRlbSh0KTtlJiZkZWxldGUgZS5pc0lnbm9yZWR9LGwuc3RhbXA9ZnVuY3Rpb24odCl7dD10aGlzLl9maW5kKHQpLHQmJih0aGlzLnN0YW1wcz10aGlzLnN0YW1wcy5jb25jYXQodCksdC5mb3JFYWNoKHRoaXMuaWdub3JlLHRoaXMpKX0sbC51bnN0YW1wPWZ1bmN0aW9uKHQpe3Q9dGhpcy5fZmluZCh0KSx0JiZ0LmZvckVhY2goZnVuY3Rpb24odCl7bi5yZW1vdmVGcm9tKHRoaXMuc3RhbXBzLHQpLHRoaXMudW5pZ25vcmUodCl9LHRoaXMpfSxsLl9maW5kPWZ1bmN0aW9uKHQpe3JldHVybiB0PyhcInN0cmluZ1wiPT10eXBlb2YgdCYmKHQ9dGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwodCkpLHQ9bi5tYWtlQXJyYXkodCkpOnZvaWQgMH0sbC5fbWFuYWdlU3RhbXBzPWZ1bmN0aW9uKCl7dGhpcy5zdGFtcHMmJnRoaXMuc3RhbXBzLmxlbmd0aCYmKHRoaXMuX2dldEJvdW5kaW5nUmVjdCgpLHRoaXMuc3RhbXBzLmZvckVhY2godGhpcy5fbWFuYWdlU3RhbXAsdGhpcykpfSxsLl9nZXRCb3VuZGluZ1JlY3Q9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLmVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksZT10aGlzLnNpemU7dGhpcy5fYm91bmRpbmdSZWN0PXtsZWZ0OnQubGVmdCtlLnBhZGRpbmdMZWZ0K2UuYm9yZGVyTGVmdFdpZHRoLHRvcDp0LnRvcCtlLnBhZGRpbmdUb3ArZS5ib3JkZXJUb3BXaWR0aCxyaWdodDp0LnJpZ2h0LShlLnBhZGRpbmdSaWdodCtlLmJvcmRlclJpZ2h0V2lkdGgpLGJvdHRvbTp0LmJvdHRvbS0oZS5wYWRkaW5nQm90dG9tK2UuYm9yZGVyQm90dG9tV2lkdGgpfX0sbC5fbWFuYWdlU3RhbXA9dSxsLl9nZXRFbGVtZW50T2Zmc2V0PWZ1bmN0aW9uKHQpe3ZhciBlPXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksbj10aGlzLl9ib3VuZGluZ1JlY3Qsbz1pKHQpLHI9e2xlZnQ6ZS5sZWZ0LW4ubGVmdC1vLm1hcmdpbkxlZnQsdG9wOmUudG9wLW4udG9wLW8ubWFyZ2luVG9wLHJpZ2h0Om4ucmlnaHQtZS5yaWdodC1vLm1hcmdpblJpZ2h0LGJvdHRvbTpuLmJvdHRvbS1lLmJvdHRvbS1vLm1hcmdpbkJvdHRvbX07cmV0dXJuIHJ9LGwuaGFuZGxlRXZlbnQ9bi5oYW5kbGVFdmVudCxsLmJpbmRSZXNpemU9ZnVuY3Rpb24oKXt0LmFkZEV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIix0aGlzKSx0aGlzLmlzUmVzaXplQm91bmQ9ITB9LGwudW5iaW5kUmVzaXplPWZ1bmN0aW9uKCl7dC5yZW1vdmVFdmVudExpc3RlbmVyKFwicmVzaXplXCIsdGhpcyksdGhpcy5pc1Jlc2l6ZUJvdW5kPSExfSxsLm9ucmVzaXplPWZ1bmN0aW9uKCl7dGhpcy5yZXNpemUoKX0sbi5kZWJvdW5jZU1ldGhvZChyLFwib25yZXNpemVcIiwxMDApLGwucmVzaXplPWZ1bmN0aW9uKCl7dGhpcy5pc1Jlc2l6ZUJvdW5kJiZ0aGlzLm5lZWRzUmVzaXplTGF5b3V0KCkmJnRoaXMubGF5b3V0KCl9LGwubmVlZHNSZXNpemVMYXlvdXQ9ZnVuY3Rpb24oKXt2YXIgdD1pKHRoaXMuZWxlbWVudCksZT10aGlzLnNpemUmJnQ7cmV0dXJuIGUmJnQuaW5uZXJXaWR0aCE9PXRoaXMuc2l6ZS5pbm5lcldpZHRofSxsLmFkZEl0ZW1zPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuX2l0ZW1pemUodCk7cmV0dXJuIGUubGVuZ3RoJiYodGhpcy5pdGVtcz10aGlzLml0ZW1zLmNvbmNhdChlKSksZX0sbC5hcHBlbmRlZD1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmFkZEl0ZW1zKHQpO2UubGVuZ3RoJiYodGhpcy5sYXlvdXRJdGVtcyhlLCEwKSx0aGlzLnJldmVhbChlKSl9LGwucHJlcGVuZGVkPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuX2l0ZW1pemUodCk7aWYoZS5sZW5ndGgpe3ZhciBpPXRoaXMuaXRlbXMuc2xpY2UoMCk7dGhpcy5pdGVtcz1lLmNvbmNhdChpKSx0aGlzLl9yZXNldExheW91dCgpLHRoaXMuX21hbmFnZVN0YW1wcygpLHRoaXMubGF5b3V0SXRlbXMoZSwhMCksdGhpcy5yZXZlYWwoZSksdGhpcy5sYXlvdXRJdGVtcyhpKX19LGwucmV2ZWFsPWZ1bmN0aW9uKHQpe3RoaXMuX2VtaXRDb21wbGV0ZU9uSXRlbXMoXCJyZXZlYWxcIix0KSx0JiZ0Lmxlbmd0aCYmdC5mb3JFYWNoKGZ1bmN0aW9uKHQpe3QucmV2ZWFsKCl9KX0sbC5oaWRlPWZ1bmN0aW9uKHQpe3RoaXMuX2VtaXRDb21wbGV0ZU9uSXRlbXMoXCJoaWRlXCIsdCksdCYmdC5sZW5ndGgmJnQuZm9yRWFjaChmdW5jdGlvbih0KXt0LmhpZGUoKX0pfSxsLnJldmVhbEl0ZW1FbGVtZW50cz1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmdldEl0ZW1zKHQpO3RoaXMucmV2ZWFsKGUpfSxsLmhpZGVJdGVtRWxlbWVudHM9ZnVuY3Rpb24odCl7dmFyIGU9dGhpcy5nZXRJdGVtcyh0KTt0aGlzLmhpZGUoZSl9LGwuZ2V0SXRlbT1mdW5jdGlvbih0KXtmb3IodmFyIGU9MDtlPHRoaXMuaXRlbXMubGVuZ3RoO2UrKyl7dmFyIGk9dGhpcy5pdGVtc1tlXTtpZihpLmVsZW1lbnQ9PXQpcmV0dXJuIGl9fSxsLmdldEl0ZW1zPWZ1bmN0aW9uKHQpe3Q9bi5tYWtlQXJyYXkodCk7dmFyIGU9W107cmV0dXJuIHQuZm9yRWFjaChmdW5jdGlvbih0KXt2YXIgaT10aGlzLmdldEl0ZW0odCk7aSYmZS5wdXNoKGkpfSx0aGlzKSxlfSxsLnJlbW92ZT1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmdldEl0ZW1zKHQpO3RoaXMuX2VtaXRDb21wbGV0ZU9uSXRlbXMoXCJyZW1vdmVcIixlKSxlJiZlLmxlbmd0aCYmZS5mb3JFYWNoKGZ1bmN0aW9uKHQpe3QucmVtb3ZlKCksbi5yZW1vdmVGcm9tKHRoaXMuaXRlbXMsdCl9LHRoaXMpfSxsLmRlc3Ryb3k9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLmVsZW1lbnQuc3R5bGU7dC5oZWlnaHQ9XCJcIix0LnBvc2l0aW9uPVwiXCIsdC53aWR0aD1cIlwiLHRoaXMuaXRlbXMuZm9yRWFjaChmdW5jdGlvbih0KXt0LmRlc3Ryb3koKX0pLHRoaXMudW5iaW5kUmVzaXplKCk7dmFyIGU9dGhpcy5lbGVtZW50Lm91dGxheWVyR1VJRDtkZWxldGUgY1tlXSxkZWxldGUgdGhpcy5lbGVtZW50Lm91dGxheWVyR1VJRCxoJiZoLnJlbW92ZURhdGEodGhpcy5lbGVtZW50LHRoaXMuY29uc3RydWN0b3IubmFtZXNwYWNlKX0sci5kYXRhPWZ1bmN0aW9uKHQpe3Q9bi5nZXRRdWVyeUVsZW1lbnQodCk7dmFyIGU9dCYmdC5vdXRsYXllckdVSUQ7cmV0dXJuIGUmJmNbZV19LHIuY3JlYXRlPWZ1bmN0aW9uKHQsZSl7dmFyIGk9cyhyKTtyZXR1cm4gaS5kZWZhdWx0cz1uLmV4dGVuZCh7fSxyLmRlZmF1bHRzKSxuLmV4dGVuZChpLmRlZmF1bHRzLGUpLGkuY29tcGF0T3B0aW9ucz1uLmV4dGVuZCh7fSxyLmNvbXBhdE9wdGlvbnMpLGkubmFtZXNwYWNlPXQsaS5kYXRhPXIuZGF0YSxpLkl0ZW09cyhvKSxuLmh0bWxJbml0KGksdCksaCYmaC5icmlkZ2V0JiZoLmJyaWRnZXQodCxpKSxpfSxyLkl0ZW09byxyfSksZnVuY3Rpb24odCxlKXtcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKFtcIm91dGxheWVyL291dGxheWVyXCIsXCJnZXQtc2l6ZS9nZXQtc2l6ZVwiXSxlKTpcIm9iamVjdFwiPT10eXBlb2YgbW9kdWxlJiZtb2R1bGUuZXhwb3J0cz9tb2R1bGUuZXhwb3J0cz1lKHJlcXVpcmUoXCJvdXRsYXllclwiKSxyZXF1aXJlKFwiZ2V0LXNpemVcIikpOnQuTWFzb25yeT1lKHQuT3V0bGF5ZXIsdC5nZXRTaXplKX0od2luZG93LGZ1bmN0aW9uKHQsZSl7dmFyIGk9dC5jcmVhdGUoXCJtYXNvbnJ5XCIpO3JldHVybiBpLmNvbXBhdE9wdGlvbnMuZml0V2lkdGg9XCJpc0ZpdFdpZHRoXCIsaS5wcm90b3R5cGUuX3Jlc2V0TGF5b3V0PWZ1bmN0aW9uKCl7dGhpcy5nZXRTaXplKCksdGhpcy5fZ2V0TWVhc3VyZW1lbnQoXCJjb2x1bW5XaWR0aFwiLFwib3V0ZXJXaWR0aFwiKSx0aGlzLl9nZXRNZWFzdXJlbWVudChcImd1dHRlclwiLFwib3V0ZXJXaWR0aFwiKSx0aGlzLm1lYXN1cmVDb2x1bW5zKCksdGhpcy5jb2xZcz1bXTtmb3IodmFyIHQ9MDt0PHRoaXMuY29sczt0KyspdGhpcy5jb2xZcy5wdXNoKDApO3RoaXMubWF4WT0wfSxpLnByb3RvdHlwZS5tZWFzdXJlQ29sdW1ucz1mdW5jdGlvbigpe2lmKHRoaXMuZ2V0Q29udGFpbmVyV2lkdGgoKSwhdGhpcy5jb2x1bW5XaWR0aCl7dmFyIHQ9dGhpcy5pdGVtc1swXSxpPXQmJnQuZWxlbWVudDt0aGlzLmNvbHVtbldpZHRoPWkmJmUoaSkub3V0ZXJXaWR0aHx8dGhpcy5jb250YWluZXJXaWR0aH12YXIgbj10aGlzLmNvbHVtbldpZHRoKz10aGlzLmd1dHRlcixvPXRoaXMuY29udGFpbmVyV2lkdGgrdGhpcy5ndXR0ZXIscj1vL24scz1uLW8lbixhPXMmJjE+cz9cInJvdW5kXCI6XCJmbG9vclwiO3I9TWF0aFthXShyKSx0aGlzLmNvbHM9TWF0aC5tYXgociwxKX0saS5wcm90b3R5cGUuZ2V0Q29udGFpbmVyV2lkdGg9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLl9nZXRPcHRpb24oXCJmaXRXaWR0aFwiKSxpPXQ/dGhpcy5lbGVtZW50LnBhcmVudE5vZGU6dGhpcy5lbGVtZW50LG49ZShpKTt0aGlzLmNvbnRhaW5lcldpZHRoPW4mJm4uaW5uZXJXaWR0aH0saS5wcm90b3R5cGUuX2dldEl0ZW1MYXlvdXRQb3NpdGlvbj1mdW5jdGlvbih0KXt0LmdldFNpemUoKTt2YXIgZT10LnNpemUub3V0ZXJXaWR0aCV0aGlzLmNvbHVtbldpZHRoLGk9ZSYmMT5lP1wicm91bmRcIjpcImNlaWxcIixuPU1hdGhbaV0odC5zaXplLm91dGVyV2lkdGgvdGhpcy5jb2x1bW5XaWR0aCk7bj1NYXRoLm1pbihuLHRoaXMuY29scyk7Zm9yKHZhciBvPXRoaXMuX2dldENvbEdyb3VwKG4pLHI9TWF0aC5taW4uYXBwbHkoTWF0aCxvKSxzPW8uaW5kZXhPZihyKSxhPXt4OnRoaXMuY29sdW1uV2lkdGgqcyx5OnJ9LGg9cit0LnNpemUub3V0ZXJIZWlnaHQsdT10aGlzLmNvbHMrMS1vLmxlbmd0aCxkPTA7dT5kO2QrKyl0aGlzLmNvbFlzW3MrZF09aDtyZXR1cm4gYX0saS5wcm90b3R5cGUuX2dldENvbEdyb3VwPWZ1bmN0aW9uKHQpe2lmKDI+dClyZXR1cm4gdGhpcy5jb2xZcztmb3IodmFyIGU9W10saT10aGlzLmNvbHMrMS10LG49MDtpPm47bisrKXt2YXIgbz10aGlzLmNvbFlzLnNsaWNlKG4sbit0KTtlW25dPU1hdGgubWF4LmFwcGx5KE1hdGgsbyl9cmV0dXJuIGV9LGkucHJvdG90eXBlLl9tYW5hZ2VTdGFtcD1mdW5jdGlvbih0KXt2YXIgaT1lKHQpLG49dGhpcy5fZ2V0RWxlbWVudE9mZnNldCh0KSxvPXRoaXMuX2dldE9wdGlvbihcIm9yaWdpbkxlZnRcIikscj1vP24ubGVmdDpuLnJpZ2h0LHM9citpLm91dGVyV2lkdGgsYT1NYXRoLmZsb29yKHIvdGhpcy5jb2x1bW5XaWR0aCk7YT1NYXRoLm1heCgwLGEpO3ZhciBoPU1hdGguZmxvb3Iocy90aGlzLmNvbHVtbldpZHRoKTtoLT1zJXRoaXMuY29sdW1uV2lkdGg/MDoxLGg9TWF0aC5taW4odGhpcy5jb2xzLTEsaCk7Zm9yKHZhciB1PXRoaXMuX2dldE9wdGlvbihcIm9yaWdpblRvcFwiKSxkPSh1P24udG9wOm4uYm90dG9tKStpLm91dGVySGVpZ2h0LGM9YTtoPj1jO2MrKyl0aGlzLmNvbFlzW2NdPU1hdGgubWF4KGQsdGhpcy5jb2xZc1tjXSl9LGkucHJvdG90eXBlLl9nZXRDb250YWluZXJTaXplPWZ1bmN0aW9uKCl7dGhpcy5tYXhZPU1hdGgubWF4LmFwcGx5KE1hdGgsdGhpcy5jb2xZcyk7dmFyIHQ9e2hlaWdodDp0aGlzLm1heFl9O3JldHVybiB0aGlzLl9nZXRPcHRpb24oXCJmaXRXaWR0aFwiKSYmKHQud2lkdGg9dGhpcy5fZ2V0Q29udGFpbmVyRml0V2lkdGgoKSksdH0saS5wcm90b3R5cGUuX2dldENvbnRhaW5lckZpdFdpZHRoPWZ1bmN0aW9uKCl7Zm9yKHZhciB0PTAsZT10aGlzLmNvbHM7LS1lJiYwPT09dGhpcy5jb2xZc1tlXTspdCsrO3JldHVybih0aGlzLmNvbHMtdCkqdGhpcy5jb2x1bW5XaWR0aC10aGlzLmd1dHRlcn0saS5wcm90b3R5cGUubmVlZHNSZXNpemVMYXlvdXQ9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLmNvbnRhaW5lcldpZHRoO3JldHVybiB0aGlzLmdldENvbnRhaW5lcldpZHRoKCksdCE9dGhpcy5jb250YWluZXJXaWR0aH0saX0pO1xuXG4vKiFcbiAqIGltYWdlc0xvYWRlZCB2NC4xLjBcbiAqIEphdmFTY3JpcHQgaXMgYWxsIGxpa2UgXCJZb3UgaW1hZ2VzIGFyZSBkb25lIHlldCBvciB3aGF0P1wiXG4gKiBNSVQgTGljZW5zZVxuICovXG5cbiggZnVuY3Rpb24oIHdpbmRvdywgZmFjdG9yeSApIHsgJ3VzZSBzdHJpY3QnO1xuICAvLyB1bml2ZXJzYWwgbW9kdWxlIGRlZmluaXRpb25cblxuICAvKmdsb2JhbCBkZWZpbmU6IGZhbHNlLCBtb2R1bGU6IGZhbHNlLCByZXF1aXJlOiBmYWxzZSAqL1xuXG4gIGlmICggdHlwZW9mIGRlZmluZSA9PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQgKSB7XG4gICAgLy8gQU1EXG4gICAgZGVmaW5lKCBbXG4gICAgICAnZXYtZW1pdHRlci9ldi1lbWl0dGVyJ1xuICAgIF0sIGZ1bmN0aW9uKCBFdkVtaXR0ZXIgKSB7XG4gICAgICByZXR1cm4gZmFjdG9yeSggd2luZG93LCBFdkVtaXR0ZXIgKTtcbiAgICB9KTtcbiAgfSBlbHNlIGlmICggdHlwZW9mIG1vZHVsZSA9PSAnb2JqZWN0JyAmJiBtb2R1bGUuZXhwb3J0cyApIHtcbiAgICAvLyBDb21tb25KU1xuICAgIG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeShcbiAgICAgIHdpbmRvdyxcbiAgICAgIHJlcXVpcmUoJ2V2LWVtaXR0ZXInKVxuICAgICk7XG4gIH0gZWxzZSB7XG4gICAgLy8gYnJvd3NlciBnbG9iYWxcbiAgICB3aW5kb3cuaW1hZ2VzTG9hZGVkID0gZmFjdG9yeShcbiAgICAgIHdpbmRvdyxcbiAgICAgIHdpbmRvdy5FdkVtaXR0ZXJcbiAgICApO1xuICB9XG5cbn0pKCB3aW5kb3csXG5cbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tICBmYWN0b3J5IC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC8vXG5cbmZ1bmN0aW9uIGZhY3RvcnkoIHdpbmRvdywgRXZFbWl0dGVyICkge1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciAkID0gd2luZG93LmpRdWVyeTtcbnZhciBjb25zb2xlID0gd2luZG93LmNvbnNvbGU7XG5cbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIGhlbHBlcnMgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLy9cblxuLy8gZXh0ZW5kIG9iamVjdHNcbmZ1bmN0aW9uIGV4dGVuZCggYSwgYiApIHtcbiAgZm9yICggdmFyIHByb3AgaW4gYiApIHtcbiAgICBhWyBwcm9wIF0gPSBiWyBwcm9wIF07XG4gIH1cbiAgcmV0dXJuIGE7XG59XG5cbi8vIHR1cm4gZWxlbWVudCBvciBub2RlTGlzdCBpbnRvIGFuIGFycmF5XG5mdW5jdGlvbiBtYWtlQXJyYXkoIG9iaiApIHtcbiAgdmFyIGFyeSA9IFtdO1xuICBpZiAoIEFycmF5LmlzQXJyYXkoIG9iaiApICkge1xuICAgIC8vIHVzZSBvYmplY3QgaWYgYWxyZWFkeSBhbiBhcnJheVxuICAgIGFyeSA9IG9iajtcbiAgfSBlbHNlIGlmICggdHlwZW9mIG9iai5sZW5ndGggPT0gJ251bWJlcicgKSB7XG4gICAgLy8gY29udmVydCBub2RlTGlzdCB0byBhcnJheVxuICAgIGZvciAoIHZhciBpPTA7IGkgPCBvYmoubGVuZ3RoOyBpKysgKSB7XG4gICAgICBhcnkucHVzaCggb2JqW2ldICk7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIC8vIGFycmF5IG9mIHNpbmdsZSBpbmRleFxuICAgIGFyeS5wdXNoKCBvYmogKTtcbiAgfVxuICByZXR1cm4gYXJ5O1xufVxuXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSBpbWFnZXNMb2FkZWQgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLy9cblxuLyoqXG4gKiBAcGFyYW0ge0FycmF5LCBFbGVtZW50LCBOb2RlTGlzdCwgU3RyaW5nfSBlbGVtXG4gKiBAcGFyYW0ge09iamVjdCBvciBGdW5jdGlvbn0gb3B0aW9ucyAtIGlmIGZ1bmN0aW9uLCB1c2UgYXMgY2FsbGJhY2tcbiAqIEBwYXJhbSB7RnVuY3Rpb259IG9uQWx3YXlzIC0gY2FsbGJhY2sgZnVuY3Rpb25cbiAqL1xuZnVuY3Rpb24gSW1hZ2VzTG9hZGVkKCBlbGVtLCBvcHRpb25zLCBvbkFsd2F5cyApIHtcbiAgLy8gY29lcmNlIEltYWdlc0xvYWRlZCgpIHdpdGhvdXQgbmV3LCB0byBiZSBuZXcgSW1hZ2VzTG9hZGVkKClcbiAgaWYgKCAhKCB0aGlzIGluc3RhbmNlb2YgSW1hZ2VzTG9hZGVkICkgKSB7XG4gICAgcmV0dXJuIG5ldyBJbWFnZXNMb2FkZWQoIGVsZW0sIG9wdGlvbnMsIG9uQWx3YXlzICk7XG4gIH1cbiAgLy8gdXNlIGVsZW0gYXMgc2VsZWN0b3Igc3RyaW5nXG4gIGlmICggdHlwZW9mIGVsZW0gPT0gJ3N0cmluZycgKSB7XG4gICAgZWxlbSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoIGVsZW0gKTtcbiAgfVxuXG4gIHRoaXMuZWxlbWVudHMgPSBtYWtlQXJyYXkoIGVsZW0gKTtcbiAgdGhpcy5vcHRpb25zID0gZXh0ZW5kKCB7fSwgdGhpcy5vcHRpb25zICk7XG5cbiAgaWYgKCB0eXBlb2Ygb3B0aW9ucyA9PSAnZnVuY3Rpb24nICkge1xuICAgIG9uQWx3YXlzID0gb3B0aW9ucztcbiAgfSBlbHNlIHtcbiAgICBleHRlbmQoIHRoaXMub3B0aW9ucywgb3B0aW9ucyApO1xuICB9XG5cbiAgaWYgKCBvbkFsd2F5cyApIHtcbiAgICB0aGlzLm9uKCAnYWx3YXlzJywgb25BbHdheXMgKTtcbiAgfVxuXG4gIHRoaXMuZ2V0SW1hZ2VzKCk7XG5cbiAgaWYgKCAkICkge1xuICAgIC8vIGFkZCBqUXVlcnkgRGVmZXJyZWQgb2JqZWN0XG4gICAgdGhpcy5qcURlZmVycmVkID0gbmV3ICQuRGVmZXJyZWQoKTtcbiAgfVxuXG4gIC8vIEhBQ0sgY2hlY2sgYXN5bmMgdG8gYWxsb3cgdGltZSB0byBiaW5kIGxpc3RlbmVyc1xuICBzZXRUaW1lb3V0KCBmdW5jdGlvbigpIHtcbiAgICB0aGlzLmNoZWNrKCk7XG4gIH0uYmluZCggdGhpcyApKTtcbn1cblxuSW1hZ2VzTG9hZGVkLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoIEV2RW1pdHRlci5wcm90b3R5cGUgKTtcblxuSW1hZ2VzTG9hZGVkLnByb3RvdHlwZS5vcHRpb25zID0ge307XG5cbkltYWdlc0xvYWRlZC5wcm90b3R5cGUuZ2V0SW1hZ2VzID0gZnVuY3Rpb24oKSB7XG4gIHRoaXMuaW1hZ2VzID0gW107XG5cbiAgLy8gZmlsdGVyICYgZmluZCBpdGVtcyBpZiB3ZSBoYXZlIGFuIGl0ZW0gc2VsZWN0b3JcbiAgdGhpcy5lbGVtZW50cy5mb3JFYWNoKCB0aGlzLmFkZEVsZW1lbnRJbWFnZXMsIHRoaXMgKTtcbn07XG5cbi8qKlxuICogQHBhcmFtIHtOb2RlfSBlbGVtZW50XG4gKi9cbkltYWdlc0xvYWRlZC5wcm90b3R5cGUuYWRkRWxlbWVudEltYWdlcyA9IGZ1bmN0aW9uKCBlbGVtICkge1xuICAvLyBmaWx0ZXIgc2libGluZ3NcbiAgaWYgKCBlbGVtLm5vZGVOYW1lID09ICdJTUcnICkge1xuICAgIHRoaXMuYWRkSW1hZ2UoIGVsZW0gKTtcbiAgfVxuICAvLyBnZXQgYmFja2dyb3VuZCBpbWFnZSBvbiBlbGVtZW50XG4gIGlmICggdGhpcy5vcHRpb25zLmJhY2tncm91bmQgPT09IHRydWUgKSB7XG4gICAgdGhpcy5hZGRFbGVtZW50QmFja2dyb3VuZEltYWdlcyggZWxlbSApO1xuICB9XG5cbiAgLy8gZmluZCBjaGlsZHJlblxuICAvLyBubyBub24tZWxlbWVudCBub2RlcywgIzE0M1xuICB2YXIgbm9kZVR5cGUgPSBlbGVtLm5vZGVUeXBlO1xuICBpZiAoICFub2RlVHlwZSB8fCAhZWxlbWVudE5vZGVUeXBlc1sgbm9kZVR5cGUgXSApIHtcbiAgICByZXR1cm47XG4gIH1cbiAgdmFyIGNoaWxkSW1ncyA9IGVsZW0ucXVlcnlTZWxlY3RvckFsbCgnaW1nJyk7XG4gIC8vIGNvbmNhdCBjaGlsZEVsZW1zIHRvIGZpbHRlckZvdW5kIGFycmF5XG4gIGZvciAoIHZhciBpPTA7IGkgPCBjaGlsZEltZ3MubGVuZ3RoOyBpKysgKSB7XG4gICAgdmFyIGltZyA9IGNoaWxkSW1nc1tpXTtcbiAgICB0aGlzLmFkZEltYWdlKCBpbWcgKTtcbiAgfVxuXG4gIC8vIGdldCBjaGlsZCBiYWNrZ3JvdW5kIGltYWdlc1xuICBpZiAoIHR5cGVvZiB0aGlzLm9wdGlvbnMuYmFja2dyb3VuZCA9PSAnc3RyaW5nJyApIHtcbiAgICB2YXIgY2hpbGRyZW4gPSBlbGVtLnF1ZXJ5U2VsZWN0b3JBbGwoIHRoaXMub3B0aW9ucy5iYWNrZ3JvdW5kICk7XG4gICAgZm9yICggaT0wOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKysgKSB7XG4gICAgICB2YXIgY2hpbGQgPSBjaGlsZHJlbltpXTtcbiAgICAgIHRoaXMuYWRkRWxlbWVudEJhY2tncm91bmRJbWFnZXMoIGNoaWxkICk7XG4gICAgfVxuICB9XG59O1xuXG52YXIgZWxlbWVudE5vZGVUeXBlcyA9IHtcbiAgMTogdHJ1ZSxcbiAgOTogdHJ1ZSxcbiAgMTE6IHRydWVcbn07XG5cbkltYWdlc0xvYWRlZC5wcm90b3R5cGUuYWRkRWxlbWVudEJhY2tncm91bmRJbWFnZXMgPSBmdW5jdGlvbiggZWxlbSApIHtcbiAgdmFyIHN0eWxlID0gZ2V0Q29tcHV0ZWRTdHlsZSggZWxlbSApO1xuICBpZiAoICFzdHlsZSApIHtcbiAgICAvLyBGaXJlZm94IHJldHVybnMgbnVsbCBpZiBpbiBhIGhpZGRlbiBpZnJhbWUgaHR0cHM6Ly9idWd6aWwubGEvNTQ4Mzk3XG4gICAgcmV0dXJuO1xuICB9XG4gIC8vIGdldCB1cmwgaW5zaWRlIHVybChcIi4uLlwiKVxuICB2YXIgcmVVUkwgPSAvdXJsXFwoKFsnXCJdKT8oLio/KVxcMVxcKS9naTtcbiAgdmFyIG1hdGNoZXMgPSByZVVSTC5leGVjKCBzdHlsZS5iYWNrZ3JvdW5kSW1hZ2UgKTtcbiAgd2hpbGUgKCBtYXRjaGVzICE9PSBudWxsICkge1xuICAgIHZhciB1cmwgPSBtYXRjaGVzICYmIG1hdGNoZXNbMl07XG4gICAgaWYgKCB1cmwgKSB7XG4gICAgICB0aGlzLmFkZEJhY2tncm91bmQoIHVybCwgZWxlbSApO1xuICAgIH1cbiAgICBtYXRjaGVzID0gcmVVUkwuZXhlYyggc3R5bGUuYmFja2dyb3VuZEltYWdlICk7XG4gIH1cbn07XG5cbi8qKlxuICogQHBhcmFtIHtJbWFnZX0gaW1nXG4gKi9cbkltYWdlc0xvYWRlZC5wcm90b3R5cGUuYWRkSW1hZ2UgPSBmdW5jdGlvbiggaW1nICkge1xuICB2YXIgbG9hZGluZ0ltYWdlID0gbmV3IExvYWRpbmdJbWFnZSggaW1nICk7XG4gIHRoaXMuaW1hZ2VzLnB1c2goIGxvYWRpbmdJbWFnZSApO1xufTtcblxuSW1hZ2VzTG9hZGVkLnByb3RvdHlwZS5hZGRCYWNrZ3JvdW5kID0gZnVuY3Rpb24oIHVybCwgZWxlbSApIHtcbiAgdmFyIGJhY2tncm91bmQgPSBuZXcgQmFja2dyb3VuZCggdXJsLCBlbGVtICk7XG4gIHRoaXMuaW1hZ2VzLnB1c2goIGJhY2tncm91bmQgKTtcbn07XG5cbkltYWdlc0xvYWRlZC5wcm90b3R5cGUuY2hlY2sgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF90aGlzID0gdGhpcztcbiAgdGhpcy5wcm9ncmVzc2VkQ291bnQgPSAwO1xuICB0aGlzLmhhc0FueUJyb2tlbiA9IGZhbHNlO1xuICAvLyBjb21wbGV0ZSBpZiBubyBpbWFnZXNcbiAgaWYgKCAhdGhpcy5pbWFnZXMubGVuZ3RoICkge1xuICAgIHRoaXMuY29tcGxldGUoKTtcbiAgICByZXR1cm47XG4gIH1cblxuICBmdW5jdGlvbiBvblByb2dyZXNzKCBpbWFnZSwgZWxlbSwgbWVzc2FnZSApIHtcbiAgICAvLyBIQUNLIC0gQ2hyb21lIHRyaWdnZXJzIGV2ZW50IGJlZm9yZSBvYmplY3QgcHJvcGVydGllcyBoYXZlIGNoYW5nZWQuICM4M1xuICAgIHNldFRpbWVvdXQoIGZ1bmN0aW9uKCkge1xuICAgICAgX3RoaXMucHJvZ3Jlc3MoIGltYWdlLCBlbGVtLCBtZXNzYWdlICk7XG4gICAgfSk7XG4gIH1cblxuICB0aGlzLmltYWdlcy5mb3JFYWNoKCBmdW5jdGlvbiggbG9hZGluZ0ltYWdlICkge1xuICAgIGxvYWRpbmdJbWFnZS5vbmNlKCAncHJvZ3Jlc3MnLCBvblByb2dyZXNzICk7XG4gICAgbG9hZGluZ0ltYWdlLmNoZWNrKCk7XG4gIH0pO1xufTtcblxuSW1hZ2VzTG9hZGVkLnByb3RvdHlwZS5wcm9ncmVzcyA9IGZ1bmN0aW9uKCBpbWFnZSwgZWxlbSwgbWVzc2FnZSApIHtcbiAgdGhpcy5wcm9ncmVzc2VkQ291bnQrKztcbiAgdGhpcy5oYXNBbnlCcm9rZW4gPSB0aGlzLmhhc0FueUJyb2tlbiB8fCAhaW1hZ2UuaXNMb2FkZWQ7XG4gIC8vIHByb2dyZXNzIGV2ZW50XG4gIHRoaXMuZW1pdEV2ZW50KCAncHJvZ3Jlc3MnLCBbIHRoaXMsIGltYWdlLCBlbGVtIF0gKTtcbiAgaWYgKCB0aGlzLmpxRGVmZXJyZWQgJiYgdGhpcy5qcURlZmVycmVkLm5vdGlmeSApIHtcbiAgICB0aGlzLmpxRGVmZXJyZWQubm90aWZ5KCB0aGlzLCBpbWFnZSApO1xuICB9XG4gIC8vIGNoZWNrIGlmIGNvbXBsZXRlZFxuICBpZiAoIHRoaXMucHJvZ3Jlc3NlZENvdW50ID09IHRoaXMuaW1hZ2VzLmxlbmd0aCApIHtcbiAgICB0aGlzLmNvbXBsZXRlKCk7XG4gIH1cblxuICBpZiAoIHRoaXMub3B0aW9ucy5kZWJ1ZyAmJiBjb25zb2xlICkge1xuICAgIGNvbnNvbGUubG9nKCAncHJvZ3Jlc3M6ICcgKyBtZXNzYWdlLCBpbWFnZSwgZWxlbSApO1xuICB9XG59O1xuXG5JbWFnZXNMb2FkZWQucHJvdG90eXBlLmNvbXBsZXRlID0gZnVuY3Rpb24oKSB7XG4gIHZhciBldmVudE5hbWUgPSB0aGlzLmhhc0FueUJyb2tlbiA/ICdmYWlsJyA6ICdkb25lJztcbiAgdGhpcy5pc0NvbXBsZXRlID0gdHJ1ZTtcbiAgdGhpcy5lbWl0RXZlbnQoIGV2ZW50TmFtZSwgWyB0aGlzIF0gKTtcbiAgdGhpcy5lbWl0RXZlbnQoICdhbHdheXMnLCBbIHRoaXMgXSApO1xuICBpZiAoIHRoaXMuanFEZWZlcnJlZCApIHtcbiAgICB2YXIganFNZXRob2QgPSB0aGlzLmhhc0FueUJyb2tlbiA/ICdyZWplY3QnIDogJ3Jlc29sdmUnO1xuICAgIHRoaXMuanFEZWZlcnJlZFsganFNZXRob2QgXSggdGhpcyApO1xuICB9XG59O1xuXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLy9cblxuZnVuY3Rpb24gTG9hZGluZ0ltYWdlKCBpbWcgKSB7XG4gIHRoaXMuaW1nID0gaW1nO1xufVxuXG5Mb2FkaW5nSW1hZ2UucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZSggRXZFbWl0dGVyLnByb3RvdHlwZSApO1xuXG5Mb2FkaW5nSW1hZ2UucHJvdG90eXBlLmNoZWNrID0gZnVuY3Rpb24oKSB7XG4gIC8vIElmIGNvbXBsZXRlIGlzIHRydWUgYW5kIGJyb3dzZXIgc3VwcG9ydHMgbmF0dXJhbCBzaXplcyxcbiAgLy8gdHJ5IHRvIGNoZWNrIGZvciBpbWFnZSBzdGF0dXMgbWFudWFsbHkuXG4gIHZhciBpc0NvbXBsZXRlID0gdGhpcy5nZXRJc0ltYWdlQ29tcGxldGUoKTtcbiAgaWYgKCBpc0NvbXBsZXRlICkge1xuICAgIC8vIHJlcG9ydCBiYXNlZCBvbiBuYXR1cmFsV2lkdGhcbiAgICB0aGlzLmNvbmZpcm0oIHRoaXMuaW1nLm5hdHVyYWxXaWR0aCAhPT0gMCwgJ25hdHVyYWxXaWR0aCcgKTtcbiAgICByZXR1cm47XG4gIH1cblxuICAvLyBJZiBub25lIG9mIHRoZSBjaGVja3MgYWJvdmUgbWF0Y2hlZCwgc2ltdWxhdGUgbG9hZGluZyBvbiBkZXRhY2hlZCBlbGVtZW50LlxuICB0aGlzLnByb3h5SW1hZ2UgPSBuZXcgSW1hZ2UoKTtcbiAgdGhpcy5wcm94eUltYWdlLmFkZEV2ZW50TGlzdGVuZXIoICdsb2FkJywgdGhpcyApO1xuICB0aGlzLnByb3h5SW1hZ2UuYWRkRXZlbnRMaXN0ZW5lciggJ2Vycm9yJywgdGhpcyApO1xuICAvLyBiaW5kIHRvIGltYWdlIGFzIHdlbGwgZm9yIEZpcmVmb3guICMxOTFcbiAgdGhpcy5pbWcuYWRkRXZlbnRMaXN0ZW5lciggJ2xvYWQnLCB0aGlzICk7XG4gIHRoaXMuaW1nLmFkZEV2ZW50TGlzdGVuZXIoICdlcnJvcicsIHRoaXMgKTtcbiAgdGhpcy5wcm94eUltYWdlLnNyYyA9IHRoaXMuaW1nLnNyYztcbn07XG5cbkxvYWRpbmdJbWFnZS5wcm90b3R5cGUuZ2V0SXNJbWFnZUNvbXBsZXRlID0gZnVuY3Rpb24oKSB7XG4gIHJldHVybiB0aGlzLmltZy5jb21wbGV0ZSAmJiB0aGlzLmltZy5uYXR1cmFsV2lkdGggIT09IHVuZGVmaW5lZDtcbn07XG5cbkxvYWRpbmdJbWFnZS5wcm90b3R5cGUuY29uZmlybSA9IGZ1bmN0aW9uKCBpc0xvYWRlZCwgbWVzc2FnZSApIHtcbiAgdGhpcy5pc0xvYWRlZCA9IGlzTG9hZGVkO1xuICB0aGlzLmVtaXRFdmVudCggJ3Byb2dyZXNzJywgWyB0aGlzLCB0aGlzLmltZywgbWVzc2FnZSBdICk7XG59O1xuXG4vLyAtLS0tLSBldmVudHMgLS0tLS0gLy9cblxuLy8gdHJpZ2dlciBzcGVjaWZpZWQgaGFuZGxlciBmb3IgZXZlbnQgdHlwZVxuTG9hZGluZ0ltYWdlLnByb3RvdHlwZS5oYW5kbGVFdmVudCA9IGZ1bmN0aW9uKCBldmVudCApIHtcbiAgdmFyIG1ldGhvZCA9ICdvbicgKyBldmVudC50eXBlO1xuICBpZiAoIHRoaXNbIG1ldGhvZCBdICkge1xuICAgIHRoaXNbIG1ldGhvZCBdKCBldmVudCApO1xuICB9XG59O1xuXG5Mb2FkaW5nSW1hZ2UucHJvdG90eXBlLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xuICB0aGlzLmNvbmZpcm0oIHRydWUsICdvbmxvYWQnICk7XG4gIHRoaXMudW5iaW5kRXZlbnRzKCk7XG59O1xuXG5Mb2FkaW5nSW1hZ2UucHJvdG90eXBlLm9uZXJyb3IgPSBmdW5jdGlvbigpIHtcbiAgdGhpcy5jb25maXJtKCBmYWxzZSwgJ29uZXJyb3InICk7XG4gIHRoaXMudW5iaW5kRXZlbnRzKCk7XG59O1xuXG5Mb2FkaW5nSW1hZ2UucHJvdG90eXBlLnVuYmluZEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuICB0aGlzLnByb3h5SW1hZ2UucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ2xvYWQnLCB0aGlzICk7XG4gIHRoaXMucHJveHlJbWFnZS5yZW1vdmVFdmVudExpc3RlbmVyKCAnZXJyb3InLCB0aGlzICk7XG4gIHRoaXMuaW1nLnJlbW92ZUV2ZW50TGlzdGVuZXIoICdsb2FkJywgdGhpcyApO1xuICB0aGlzLmltZy5yZW1vdmVFdmVudExpc3RlbmVyKCAnZXJyb3InLCB0aGlzICk7XG59O1xuXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSBCYWNrZ3JvdW5kIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC8vXG5cbmZ1bmN0aW9uIEJhY2tncm91bmQoIHVybCwgZWxlbWVudCApIHtcbiAgdGhpcy51cmwgPSB1cmw7XG4gIHRoaXMuZWxlbWVudCA9IGVsZW1lbnQ7XG4gIHRoaXMuaW1nID0gbmV3IEltYWdlKCk7XG59XG5cbi8vIGluaGVyaXQgTG9hZGluZ0ltYWdlIHByb3RvdHlwZVxuQmFja2dyb3VuZC5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKCBMb2FkaW5nSW1hZ2UucHJvdG90eXBlICk7XG5cbkJhY2tncm91bmQucHJvdG90eXBlLmNoZWNrID0gZnVuY3Rpb24oKSB7XG4gIHRoaXMuaW1nLmFkZEV2ZW50TGlzdGVuZXIoICdsb2FkJywgdGhpcyApO1xuICB0aGlzLmltZy5hZGRFdmVudExpc3RlbmVyKCAnZXJyb3InLCB0aGlzICk7XG4gIHRoaXMuaW1nLnNyYyA9IHRoaXMudXJsO1xuICAvLyBjaGVjayBpZiBpbWFnZSBpcyBhbHJlYWR5IGNvbXBsZXRlXG4gIHZhciBpc0NvbXBsZXRlID0gdGhpcy5nZXRJc0ltYWdlQ29tcGxldGUoKTtcbiAgaWYgKCBpc0NvbXBsZXRlICkge1xuICAgIHRoaXMuY29uZmlybSggdGhpcy5pbWcubmF0dXJhbFdpZHRoICE9PSAwLCAnbmF0dXJhbFdpZHRoJyApO1xuICAgIHRoaXMudW5iaW5kRXZlbnRzKCk7XG4gIH1cbn07XG5cbkJhY2tncm91bmQucHJvdG90eXBlLnVuYmluZEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuICB0aGlzLmltZy5yZW1vdmVFdmVudExpc3RlbmVyKCAnbG9hZCcsIHRoaXMgKTtcbiAgdGhpcy5pbWcucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ2Vycm9yJywgdGhpcyApO1xufTtcblxuQmFja2dyb3VuZC5wcm90b3R5cGUuY29uZmlybSA9IGZ1bmN0aW9uKCBpc0xvYWRlZCwgbWVzc2FnZSApIHtcbiAgdGhpcy5pc0xvYWRlZCA9IGlzTG9hZGVkO1xuICB0aGlzLmVtaXRFdmVudCggJ3Byb2dyZXNzJywgWyB0aGlzLCB0aGlzLmVsZW1lbnQsIG1lc3NhZ2UgXSApO1xufTtcblxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0galF1ZXJ5IC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC8vXG5cbkltYWdlc0xvYWRlZC5tYWtlSlF1ZXJ5UGx1Z2luID0gZnVuY3Rpb24oIGpRdWVyeSApIHtcbiAgalF1ZXJ5ID0galF1ZXJ5IHx8IHdpbmRvdy5qUXVlcnk7XG4gIGlmICggIWpRdWVyeSApIHtcbiAgICByZXR1cm47XG4gIH1cbiAgLy8gc2V0IGxvY2FsIHZhcmlhYmxlXG4gICQgPSBqUXVlcnk7XG4gIC8vICQoKS5pbWFnZXNMb2FkZWQoKVxuICAkLmZuLmltYWdlc0xvYWRlZCA9IGZ1bmN0aW9uKCBvcHRpb25zLCBjYWxsYmFjayApIHtcbiAgICB2YXIgaW5zdGFuY2UgPSBuZXcgSW1hZ2VzTG9hZGVkKCB0aGlzLCBvcHRpb25zLCBjYWxsYmFjayApO1xuICAgIHJldHVybiBpbnN0YW5jZS5qcURlZmVycmVkLnByb21pc2UoICQodGhpcykgKTtcbiAgfTtcbn07XG4vLyB0cnkgbWFraW5nIHBsdWdpblxuSW1hZ2VzTG9hZGVkLm1ha2VKUXVlcnlQbHVnaW4oKTtcblxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIC8vXG5cbnJldHVybiBJbWFnZXNMb2FkZWQ7XG5cbn0pOyIsImFuZ3VsYXIubW9kdWxlKFwiYW5ndWxhci1wcmVsb2FkLWltYWdlXCIsW10pO2FuZ3VsYXIubW9kdWxlKFwiYW5ndWxhci1wcmVsb2FkLWltYWdlXCIpLmZhY3RvcnkoXCJwcmVMb2FkZXJcIixmdW5jdGlvbigpe3JldHVybiBmdW5jdGlvbihlLHQsbil7YW5ndWxhci5lbGVtZW50KG5ldyBJbWFnZSkuYmluZChcImxvYWRcIixmdW5jdGlvbigpe3QoKX0pLmJpbmQoXCJlcnJvclwiLGZ1bmN0aW9uKCl7bigpfSkuYXR0cihcInNyY1wiLGUpfX0pO2FuZ3VsYXIubW9kdWxlKFwiYW5ndWxhci1wcmVsb2FkLWltYWdlXCIpLmRpcmVjdGl2ZShcInByZWxvYWRJbWFnZVwiLFtcInByZUxvYWRlclwiLGZ1bmN0aW9uKGUpe3JldHVybntyZXN0cmljdDpcIkFcIix0ZXJtaW5hbDp0cnVlLHByaW9yaXR5OjEwMCxsaW5rOmZ1bmN0aW9uKHQsbixyKXt2YXIgaT1yLm5nU3JjO3QuZGVmYXVsdD1yLmRlZmF1bHRJbWFnZXx8XCJkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQUFFQUFBQUJDQUlBQUFDUWQxUGVBQUFBQ1hCSVdYTUFBQXNUQUFBTEV3RUFtcHdZQUFBQUIzUkpUVVVIM3dFV0V5Z05XaUxxbHdBQUFCbDBSVmgwUTI5dGJXVnVkQUJEY21WaGRHVmtJSGRwZEdnZ1IwbE5VRmVCRGhjQUFBQU1TVVJCVkFqWFkvai8vejhBQmY0Qy90ek1XZWNBQUFBQVNVVk9SSzVDWUlJPVwiO3IuJHNldChcInNyY1wiLHQuZGVmYXVsdCk7ZShpLGZ1bmN0aW9uKCl7ci4kc2V0KFwic3JjXCIsaSl9LGZ1bmN0aW9uKCl7aWYoci5mYWxsYmFja0ltYWdlIT11bmRlZmluZWQpe3IuJHNldChcInNyY1wiLHIuZmFsbGJhY2tJbWFnZSl9fSl9fX1dKTthbmd1bGFyLm1vZHVsZShcImFuZ3VsYXItcHJlbG9hZC1pbWFnZVwiKS5kaXJlY3RpdmUoXCJwcmVsb2FkQmdJbWFnZVwiLFtcInByZUxvYWRlclwiLGZ1bmN0aW9uKGUpe3JldHVybntyZXN0cmljdDpcIkFcIixsaW5rOmZ1bmN0aW9uKHQsbixyKXtpZihyLnByZWxvYWRCZ0ltYWdlIT11bmRlZmluZWQpe3QuZGVmYXVsdD1yLmRlZmF1bHRJbWFnZXx8XCJkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQUFFQUFBQUJDQUlBQUFDUWQxUGVBQUFBQ1hCSVdYTUFBQXNUQUFBTEV3RUFtcHdZQUFBQUIzUkpUVVVIM3dFV0V5Z05XaUxxbHdBQUFCbDBSVmgwUTI5dGJXVnVkQUJEY21WaGRHVmtJSGRwZEdnZ1IwbE5VRmVCRGhjQUFBQU1TVVJCVkFqWFkvai8vejhBQmY0Qy90ek1XZWNBQUFBQVNVVk9SSzVDWUlJPVwiO24uY3NzKHtcImJhY2tncm91bmQtaW1hZ2VcIjondXJsKFwiJyt0LmRlZmF1bHQrJ1wiKSd9KTtlKHIucHJlbG9hZEJnSW1hZ2UsZnVuY3Rpb24oKXtuLmNzcyh7XCJiYWNrZ3JvdW5kLWltYWdlXCI6J3VybChcIicrci5wcmVsb2FkQmdJbWFnZSsnXCIpJ30pfSxmdW5jdGlvbigpe2lmKHIuZmFsbGJhY2tJbWFnZSE9dW5kZWZpbmVkKXtuLmNzcyh7XCJiYWNrZ3JvdW5kLWltYWdlXCI6J3VybChcIicrci5mYWxsYmFja0ltYWdlKydcIiknfSl9fSl9fX19XSkiLCIndXNlIHN0cmljdCdcblxuLyoqXG4qIE1hZ2VudG8gTW9iaWxlIFRoZW1lIGpuMlxuKiBEZXZlbG9wZXI6IElnb3IgR290dHNjaGFsZ1xuKlxuKiBKdXN0IGZvciB0aGVtZSBleGVwdGlvblxuKi9cblxuLyoqXG4qIEFwcCBJbml0XG4qL1xudmFyIGFwcCA9IGFuZ3VsYXIubW9kdWxlKCdKbjJNb2JpbGVBcHAnLCBbJ2FuZ3VsYXItcHJlbG9hZC1pbWFnZScsJ3VpLXJhbmdlU2xpZGVyJ10pO1xuXG52YXIgRk9STV9LRVk9bnVsbDtcbihmdW5jdGlvbigkKXtcbiAgdmFyIGluaXRBbmd1bGFyID0gZnVuY3Rpb24oKXtcbiAgICAgIGFuZ3VsYXIuYm9vdHN0cmFwKGRvY3VtZW50LCBbJ0puMk1vYmlsZUFwcCddKTtcbiAgfVxuXG4gJChmdW5jdGlvbigpe1xuICAgIC8vIGlmKHdpbmRvdy5KbjIgJiYgd2luZG93LkpuMi5DYWNoZUJ5cGFzcyAhPT0gdW5kZWZpbmVkKXtcbiAgICAgICQoZG9jdW1lbnQpLm9uKCdjYWNoZTpmaW5pc2gnLGZ1bmN0aW9uKGV2dCxjb250ZW50KXtcbiAgICAgICAgdmFyIGRhdGEgICAgID0gY29udGVudC5kYXRhO1xuICAgICAgICBGT1JNX0tFWSAgICAgPSBkYXRhLmZvcm1fa2V5O1xuICAgICAgICBpbml0QW5ndWxhcigpO1xuICAgICAgfSk7XG4gICAgICAkKGRvY3VtZW50KS5vbignY2FjaGU6ZXJyb3InLGluaXRBbmd1bGFyKTtcbiAgICAvLyB9ZWxzZXtcbiAgICAgIC8vIGluaXRBbmd1bGFyKCk7XG4gICAgLy8gfVxuICB9KTtcbn0pKGpRdWVyeSk7XG4vL1xuIiwiLyoqXG4qIE1hZ2VudG8gTW9iaWxlIFRoZW1lIGpuMlxuKiBEZXZlbG9wZXI6IElnb3IgR290dHNjaGFsZ1xuKlxuKiBKdXN0IGZvciB0aGVtZSBleGVwdGlvblxuKi9cblxuYXBwLmNvbnRyb2xsZXIoJ2NhcnRDb250cm9sbGVyJyxmdW5jdGlvbigkc2NvcGUpe1xuXG4gICRzY29wZS5wbHVzUHJvZHVjdCA9IGZ1bmN0aW9uKGlkKXtcbiAgICB2YXIgdmFsdWUgPSBwYXJzZUludChqUXVlcnkoaWQpLnZhbCgpKTtcbiAgICBpZih2YWx1ZSA8IGpRdWVyeShpZCkuZGF0YShcIm1heFwiKSl7XG4gICAgICBqUXVlcnkoaWQpLnZhbCh2YWx1ZSsxKTtcbiAgICB9XG5cbiAgICBpZih2YWx1ZSA8IGpRdWVyeShpZCkuZGF0YShcIm1heFwiKSl7XG4gICAgICBqUXVlcnkoaWQpLnBhcmVudCgpLmZpbmQoXCIucGx1c1wiKS5yZW1vdmVDbGFzcyhcImRpc2FibGVcIik7XG4gICAgfWVsc2V7XG4gICAgICBqUXVlcnkoaWQpLnBhcmVudCgpLmZpbmQoXCIucGx1c1wiKS5hZGRDbGFzcyhcImRpc2FibGVcIik7XG4gICAgfVxuXG4gICAgaWYodmFsdWUgPiAxKXtcbiAgICAgIGpRdWVyeShpZCkucGFyZW50KCkuZmluZChcIi5taW51c1wiKS5yZW1vdmVDbGFzcyhcImRpc2FibGVcIik7XG4gICAgfWVsc2V7XG4gICAgICBqUXVlcnkoaWQpLnBhcmVudCgpLmZpbmQoXCIubWludXNcIikuYWRkQ2xhc3MoXCJkaXNhYmxlXCIpO1xuICAgIH1cblxuICAgIGpRdWVyeShcIiN1cGRhdGUtY2FydFwiKS5zdWJtaXQoKTtcbiAgfVxuXG4gICRzY29wZS5ibHVyUXR5SXRlbSA9IGZ1bmN0aW9uKGlkKXtcbiAgICB2YXIgdmFsdWUgPSBwYXJzZUludChqUXVlcnkoaWQpLnZhbCgpKTtcbiAgICBpZih2YWx1ZSA+IGpRdWVyeShpZCkuZGF0YShcIm1heFwiKSkgalF1ZXJ5KGlkKS52YWwoalF1ZXJ5KGlkKS5kYXRhKFwibWF4XCIpKTtcblxuICAgIGlmKHZhbHVlIDwgalF1ZXJ5KGlkKS5kYXRhKFwibWF4XCIpKXtcbiAgICAgIGpRdWVyeShpZCkucGFyZW50KCkuZmluZChcIi5wbHVzXCIpLnJlbW92ZUNsYXNzKFwiZGlzYWJsZVwiKTtcbiAgICB9ZWxzZXtcbiAgICAgIGpRdWVyeShpZCkucGFyZW50KCkuZmluZChcIi5wbHVzXCIpLmFkZENsYXNzKFwiZGlzYWJsZVwiKTtcbiAgICB9XG5cbiAgICBpZih2YWx1ZSA+IDEpe1xuICAgICAgalF1ZXJ5KGlkKS5wYXJlbnQoKS5maW5kKFwiLm1pbnVzXCIpLnJlbW92ZUNsYXNzKFwiZGlzYWJsZVwiKTtcbiAgICB9ZWxzZXtcbiAgICAgIGpRdWVyeShpZCkucGFyZW50KCkuZmluZChcIi5taW51c1wiKS5hZGRDbGFzcyhcImRpc2FibGVcIik7XG4gICAgfVxuXG4gICAgalF1ZXJ5KFwiI3VwZGF0ZS1jYXJ0XCIpLnN1Ym1pdCgpO1xuICB9XG5cbiAgJHNjb3BlLm1pbnVzUHJvZHVjdCA9IGZ1bmN0aW9uKGlkKXtcbiAgICB2YXIgdmFsdWUgPSBwYXJzZUludChqUXVlcnkoaWQpLnZhbCgpKTtcbiAgICBpZih2YWx1ZSA+IDEpe1xuICAgICAgalF1ZXJ5KGlkKS52YWwodmFsdWUtMSk7XG4gICAgfVxuXG4gICAgaWYodmFsdWUgPCBqUXVlcnkoaWQpLmRhdGEoXCJtYXhcIikpe1xuICAgICAgalF1ZXJ5KGlkKS5wYXJlbnQoKS5maW5kKFwiLnBsdXNcIikucmVtb3ZlQ2xhc3MoXCJkaXNhYmxlXCIpO1xuICAgIH1lbHNle1xuICAgICAgalF1ZXJ5KGlkKS5wYXJlbnQoKS5maW5kKFwiLnBsdXNcIikuYWRkQ2xhc3MoXCJkaXNhYmxlXCIpO1xuICAgIH1cblxuICAgIGlmKHZhbHVlID4gMSl7XG4gICAgICBqUXVlcnkoaWQpLnBhcmVudCgpLmZpbmQoXCIubWludXNcIikucmVtb3ZlQ2xhc3MoXCJkaXNhYmxlXCIpO1xuICAgIH1lbHNle1xuICAgICAgalF1ZXJ5KGlkKS5wYXJlbnQoKS5maW5kKFwiLm1pbnVzXCIpLmFkZENsYXNzKFwiZGlzYWJsZVwiKTtcbiAgICB9XG5cbiAgICBqUXVlcnkoXCIjdXBkYXRlLWNhcnRcIikuc3VibWl0KCk7XG4gIH1cblxuICBqUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCQpe1xuXG4gICAgJChcIi5mcmFtZS1jYXJkXCIpLmZpbmQoXCIuaXRlbVwiKS5lYWNoKGZ1bmN0aW9uKGluZGV4LGVsZW1lbnQpe1xuICAgICAgdmFyICRoZWlnaHQgPSAkKGVsZW1lbnQpLmhlaWdodCgpO1xuICAgICAgJChlbGVtZW50KS5maW5kKFwiLnF0eVwiKS5oZWlnaHQoJGhlaWdodCk7XG4gICAgICAkKGVsZW1lbnQpLmZpbmQoXCIuaW1hZ2VcIikuaGVpZ2h0KCRoZWlnaHQpO1xuICAgICAgJChlbGVtZW50KS5maW5kKFwiLmluZm9cIikuaGVpZ2h0KCRoZWlnaHQpO1xuICAgIH0pO1xuXG4gICAgJChcIiN1cGRhdGUtY2FydFwiKS5zdWJtaXQoZnVuY3Rpb24oZSl7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBqUXVlcnkoXCIubG9hZGluZ1wiKS5zaG93KCk7XG4gICAgICAkLmFqYXgoe1xuICAgICAgICB0eXBlOiAkKHRoaXMpLmF0dHIoJ21ldGhvZCcpLFxuICAgICAgICB1cmw6ICQodGhpcykuYXR0cignYWN0aW9uJyksXG4gICAgICAgIGRhdGE6ICQodGhpcykuc2VyaWFsaXplKCksXG4gICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgbG9jYXRpb24ucmVsb2FkKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0pO1xuXG4gIH0pO1xuXG59KTtcbiIsIi8qKlxuKiBNYWdlbnRvIE1vYmlsZSBUaGVtZSBqbjJcbiogRGV2ZWxvcGVyOiBJZ29yIEdvdHRzY2hhbGdcbipcbiogSnVzdCBmb3IgdGhlbWUgZXhlcHRpb25cbiovXG5cblxuYXBwLmNvbnRyb2xsZXIoJ2NhdGVnb3J5Q29udHJvbGxlcicsZnVuY3Rpb24oJHNjb3BlLCAkaW50ZXJ2YWwpe1xuICAkc2NvcGUucHJpY2UgPSB7XG4gICAgbWluOiBudWxsLFxuICAgIG1heDogbnVsbFxuICB9XG5cbiAgc3RyID0galF1ZXJ5KFwiZGl2LmZpbHRlclwiKS5odG1sKCkudHJpbSgpO1xuICBpZihzdHIubGVuZ3RoID09PSAwKXtcbiAgICBqUXVlcnkoXCJzcGFuLmZpbHRlclwiKS5oaWRlKCk7XG4gIH1cblxuICAkc2NvcGUuYXBsbHlGaWx0ZXIgPSBmdW5jdGlvbigpe1xuICAgIHZhciB1cmwgPSB3aW5kb3cubG9jYXRpb24uaHJlZi5zcGxpdChcIj9cIilbMF07XG4gICAgc2V0TG9jYXRpb24odXJsK1wiP3ByaWNlPVwiKyRzY29wZS5wcmljZS5taW4rXCItXCIrJHNjb3BlLnByaWNlLm1heCk7XG4gIH1cblxuICBpbWFnZXNMb2FkZWQoICcucHJvZHVjdHMtZ3JpZCcsIGZ1bmN0aW9uKCl7XG4gICAgdmFyIG1zbnJ5ID0gbmV3IE1hc29ucnkoJy5wcm9kdWN0cy1ncmlkJywge1xuICAgICAgaXRlbXNlbGVjdG9yOiAnLmdyaWQtaXRlbScsXG4gICAgICBjb2x1bW53aWR0aDogJy5ncmlkLWl0ZW0nLFxuICAgIH0pO1xuICB9KTtcblxuICAkc2NvcGUuZmlsdGVyU3RhdHVzID0gMDtcblxuICAkc2NvcGUuZmlsdGVyVG9nZ2xlID0gZnVuY3Rpb24oKXtcbiAgICAkc2NvcGUuZmlsdGVyU3RhdHVzID0gISRzY29wZS5maWx0ZXJTdGF0dXM7XG4gIH1cblxufSk7XG4iLCIvKipcbiogTWFnZW50byBNb2JpbGUgVGhlbWUgam4yXG4qIERldmVsb3BlcjogSWdvciBHb3R0c2NoYWxnXG4qXG4qIEp1c3QgZm9yIHRoZW1lIGV4ZXB0aW9uXG4qL1xuXG5cbmFwcC5jb250cm9sbGVyKCdjaGVja291dENvbnRyb2xsZXInLGZ1bmN0aW9uKCRzY29wZSl7XG5cbiAgJHNjb3BlLm9wZW5UZXJtb3MgPSBmdW5jdGlvbigpe1xuICAgIGpRdWVyeSgnI3Rlcm1vcy1jb25kaWNvZXMnKS5tb2RhbCgnc2hvdycpO1xuICB9XG5cbn0pO1xuIiwiLyoqXG4qIE1hZ2VudG8gTW9iaWxlIFRoZW1lIGpuMlxuKiBEZXZlbG9wZXI6IElnb3IgR290dHNjaGFsZ1xuKlxuKiBKdXN0IGZvciB0aGVtZSBleGVwdGlvblxuKi9cblxuXG5hcHAuY29udHJvbGxlcignbG9naW5Db250cm9sbGVyJyxmdW5jdGlvbigkc2NvcGUpe1xuXG59KTtcbiIsIi8qKlxuKiBNYWdlbnRvIE1vYmlsZSBUaGVtZSBqbjJcbiogRGV2ZWxvcGVyOiBJZ29yIEdvdHRzY2hhbGdcbipcbiogSnVzdCBmb3IgdGhlbWUgZXhlcHRpb25cbiovXG5cbmFwcC5jb250cm9sbGVyKCdtYWluQ29udHJvbGxlcicsZnVuY3Rpb24oJHNjb3BlKXtcbiAgalF1ZXJ5KFwiLnBpdGJhci1tb2JpbGUgLmppY29uXCIpLnRleHQoXCJcIik7XG4gIGpRdWVyeShcIi52aWV3XCIpLmFkZENsYXNzKFwibG9hZFwiKVxuXG4gIGpRdWVyeSh3aW5kb3cpLnVubG9hZChmdW5jdGlvbigpe1xuICAgIGpRdWVyeShcIi52aWV3XCIpLnJlbW92ZUNsYXNzKFwibG9hZFwiKTtcbiAgfSk7XG5cbiAgdmFyIHBpbkhlaWdodCA9IGpRdWVyeShcIi5waXRiYXItbW9iaWxlXCIpLmZpbmQoXCIuY29sLXhzLTZcIikuZmlyc3QoKS5oZWlnaHQoKTtcbiAgalF1ZXJ5KFwiLnBpdGJhci1tb2JpbGVcIikuZmluZChcIi5jb2wteHMtNlwiKS5lYWNoKGZ1bmN0aW9uKGluZGV4LGVsZW1lbnQpe1xuICAgIGlmKGpRdWVyeShlbGVtZW50KS5oZWlnaHQoKSA+IHBpbkhlaWdodCl7XG4gICAgICBwaW5IZWlnaHQgPSBqUXVlcnkoZWxlbWVudCkuaGVpZ2h0KCk7XG4gICAgfVxuICB9KTtcblxuICBqUXVlcnkoXCIucGl0YmFyLW1vYmlsZVwiKS5maW5kKFwiLmNvbC14cy02XCIpLmhlaWdodChwaW5IZWlnaHQpO1xuXG4gIGpRdWVyeSgnLmJ0bicpLmFkZENsYXNzKFwid2F2ZXMtZWZmZWN0XCIpO1xuICBqUXVlcnkoJy5pdGVtJykuYWRkQ2xhc3MoXCJ3YXZlcy1lZmZlY3RcIik7XG4gIGpRdWVyeSgnYnV0dG9uJykuYWRkQ2xhc3MoXCJ3YXZlcy1lZmZlY3RcIik7XG4gIGpRdWVyeSgnYVtocmVmXScpLmFkZENsYXNzKFwid2F2ZXMtZWZmZWN0XCIpO1xuICBqUXVlcnkoJy5idXR0b24nKS5hZGRDbGFzcyhcIndhdmVzLWVmZmVjdFwiKTtcbiAgalF1ZXJ5KCcuZnJhbWUtY2FyZCcpLmFkZENsYXNzKFwid2F2ZXMtZWZmZWN0XCIpO1xuXG59KTtcbiIsIi8qKlxuKiBNYWdlbnRvIE1vYmlsZSBUaGVtZSBqbjJcbiogRGV2ZWxvcGVyOiBJZ29yIEdvdHRzY2hhbGdcbipcbiogSnVzdCBmb3IgdGhlbWUgZXhlcHRpb25cbiovXG5cbmFwcC5jb250cm9sbGVyKCdwcm9kdWN0Q29udHJvbGxlcicsZnVuY3Rpb24oJHNjb3BlKXtcblxuICAkc2NvcGUuZGVzY3JpcHRpb24gPSAwO1xuICAkc2NvcGUub3B0aW9ucyA9IDE7XG4gICRzY29wZS5zaGlwcGluZyA9IDA7XG4gICRzY29wZS5jb21tZW50cyA9IDA7XG5cbiAgJHNjb3BlLmJ1eV9hY3Rpb24gPSBmdW5jdGlvbigpe1xuXG4gIH1cblxuICAkc2NvcGUucGx1c1Byb2R1Y3QgPSBmdW5jdGlvbihpZCl7XG4gICAgdmFyIHZhbHVlID0gcGFyc2VJbnQoalF1ZXJ5KGlkKS52YWwoKSk7XG4gICAgaWYodmFsdWUgPCBqUXVlcnkoaWQpLmRhdGEoXCJtYXhcIikpe1xuICAgICAgalF1ZXJ5KGlkKS52YWwodmFsdWUrMSk7XG4gICAgfVxuXG4gICAgaWYodmFsdWUgPCBqUXVlcnkoaWQpLmRhdGEoXCJtYXhcIikpe1xuICAgICAgalF1ZXJ5KGlkKS5wYXJlbnQoKS5maW5kKFwiLnBsdXNcIikucmVtb3ZlQ2xhc3MoXCJkaXNhYmxlXCIpO1xuICAgIH1lbHNle1xuICAgICAgalF1ZXJ5KGlkKS5wYXJlbnQoKS5maW5kKFwiLnBsdXNcIikuYWRkQ2xhc3MoXCJkaXNhYmxlXCIpO1xuICAgIH1cblxuICAgIGlmKHZhbHVlID4gMSl7XG4gICAgICBqUXVlcnkoaWQpLnBhcmVudCgpLmZpbmQoXCIubWludXNcIikucmVtb3ZlQ2xhc3MoXCJkaXNhYmxlXCIpO1xuICAgIH1lbHNle1xuICAgICAgalF1ZXJ5KGlkKS5wYXJlbnQoKS5maW5kKFwiLm1pbnVzXCIpLmFkZENsYXNzKFwiZGlzYWJsZVwiKTtcbiAgICB9XG4gIH1cblxuICAkc2NvcGUubWludXNQcm9kdWN0ID0gZnVuY3Rpb24oaWQpe1xuICAgIHZhciB2YWx1ZSA9IHBhcnNlSW50KGpRdWVyeShpZCkudmFsKCkpO1xuICAgIGlmKHZhbHVlID4gMSl7XG4gICAgICBqUXVlcnkoaWQpLnZhbCh2YWx1ZS0xKTtcbiAgICB9XG5cbiAgICBpZih2YWx1ZSA8IGpRdWVyeShpZCkuZGF0YShcIm1heFwiKSl7XG4gICAgICBqUXVlcnkoaWQpLnBhcmVudCgpLmZpbmQoXCIucGx1c1wiKS5yZW1vdmVDbGFzcyhcImRpc2FibGVcIik7XG4gICAgfWVsc2V7XG4gICAgICBqUXVlcnkoaWQpLnBhcmVudCgpLmZpbmQoXCIucGx1c1wiKS5hZGRDbGFzcyhcImRpc2FibGVcIik7XG4gICAgfVxuXG4gICAgaWYodmFsdWUgPiAxKXtcbiAgICAgIGpRdWVyeShpZCkucGFyZW50KCkuZmluZChcIi5taW51c1wiKS5yZW1vdmVDbGFzcyhcImRpc2FibGVcIik7XG4gICAgfWVsc2V7XG4gICAgICBqUXVlcnkoaWQpLnBhcmVudCgpLmZpbmQoXCIubWludXNcIikuYWRkQ2xhc3MoXCJkaXNhYmxlXCIpO1xuICAgIH1cbiAgfVxuXG4gIGpRdWVyeShmdW5jdGlvbigkKXtcbiAgICB2YXIgJG1lZGlhID0gJCgnI21lZGlhLXNsaWRlcicpLm93bENhcm91c2VsKHtcbiAgICAgIG5hdmlnYXRpb246IGZhbHNlLFxuICAgICAgcGFnaW5hdGlvbjogdHJ1ZSxcbiAgICAgIHNpbmdsZUl0ZW06IHRydWUsXG4gICAgICBpdGVtczogMSxcbiAgICAgIGF1dG9QbGF5OiBmYWxzZVxuICAgIH0pO1xuXG4gICAgalF1ZXJ5KFwiLnN3YXRjaENvbnRhaW5lciBhXCIpLmFkZENsYXNzKFwibm8tbG9hZGluZ1wiKTtcblxuICAgIGpRdWVyeShkb2N1bWVudCkub24oXCJjbGlja1wiLCBcIi5zd2F0Y2hDb250YWluZXIgYVwiLCBmdW5jdGlvbihlKXtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIHZhciB1cmwgPSAkKHRoaXMpLmF0dHIoXCJocmVmXCIpLnNwbGl0KCctem9vbS4nKVswXTtcbiAgICAgIHVybCA9IHVybC5zcGxpdChcIi9cIik7XG4gICAgICB1cmwgPSB1cmxbdXJsLmxlbmd0aC0xXTtcbiAgICAgIHZhciBpbWFnZSA9ICQoXCIubW9yZS12aWV3c1wiKS5maW5kKFwiaW1nW3NyYyo9J1wiICsgdXJsICsgXCInXVwiKS5wYXJlbnQoKTtcbiAgICAgICRtZWRpYS50cmlnZ2VyKCdvd2wuanVtcFRvJywgaW1hZ2UuaW5kZXgoKSk7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSk7XG5cbiAgICBqUXVlcnkoXCIub3B0aW9uc1wiKS5vbihcImNoYW5nZVwiLFwiLnN3YXRjaFNlbGVjdFwiLCBmdW5jdGlvbigpe1xuICAgICAgdmFyIGxpbmsgPSAkKFwiLnN3YXRjaGVzQ29udGFpbmVyXCIpLmZpbmQoXCIuc3dhdGNoU2VsZWN0ZWRcIikucGFyZW50KCk7XG4gICAgICB2YXIgdXJsID0gJChsaW5rKS5hdHRyKFwiaHJlZlwiKS5zcGxpdCgnLXpvb20uJylbMF07XG4gICAgICB1cmwgPSB1cmwuc3BsaXQoXCIvXCIpO1xuICAgICAgdXJsID0gdXJsW3VybC5sZW5ndGgtMV07XG4gICAgICB2YXIgaW1hZ2UgPSAkKFwiLm1vcmUtdmlld3NcIikuZmluZChcImltZ1tzcmMqPSdcIiArIHVybCArIFwiJ11cIikucGFyZW50KCk7XG4gICAgICAkbWVkaWEudHJpZ2dlcignb3dsLmp1bXBUbycsIGltYWdlLmluZGV4KCkpO1xuICAgIH0pO1xuXG4gICAgZnVuY3Rpb24gZ2V0RW1wdHlPcHRpb25zKCl7XG4gICAgICByZXR1cm4galF1ZXJ5KCcucHJvZHVjdC1vcHRpb25zIC5yZXF1aXJlZC1lbnRyeScpLmZpbHRlcihmdW5jdGlvbigpe1xuICAgICAgICByZXR1cm4gIXRoaXMudmFsdWU7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBqUXVlcnkoXCIuYWRkLXRvLWNhcnQgLmJ0bi1jYXJ0LCAuYmxvY2stc2hpcHBpbmctZXN0aW1hdGUgYnV0dG9uXCIpLmNsaWNrKGZ1bmN0aW9uKCl7XG4gICAgICBpZihnZXRFbXB0eU9wdGlvbnMoKS5sZW5ndGggPiAwKXtcbiAgICAgICAgYWxlcnQoXCLDiSBuZWNlc3PDoXJpbyBlc2NvbGhlciB1bWEgb3Bhw6fDo28gcGFyYSBvIHByb2R1dG9cIik7XG4gICAgICAgIHdpbmRvdy5zY3JvbGxUbyhnZXRFbXB0eU9wdGlvbnMoKS5maXJzdCgpLm9mZnNldCgpLnRvcCwwKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfSk7XG5cbn0pO1xuIiwiLyoqXG4qIE1hZ2VudG8gTW9iaWxlIFRoZW1lIGpuMlxuKiBEZXZlbG9wZXI6IElnb3IgR290dHNjaGFsZ1xuKlxuKiBKdXN0IGZvciB0aGVtZSBleGVwdGlvblxuKi9cblxuXG5hcHAuY29udHJvbGxlcigncmVnaXN0ZXJDb250cm9sbGVyJyxmdW5jdGlvbigkc2NvcGUpe1xuXHQkc2NvcGUucGVzc29hID0gMDtcblx0JHNjb3BlLnppcGNvZGUgPSAwO1xuXHQkc2NvcGUuZmlyc3RuYW1lID0gXCJcIjtcblx0JHNjb3BlLmxhc3RuYW1lICA9IFwiXCI7XG5cdCRzY29wZS5jbnBqICA9IFwiXCI7XG5cdCRzY29wZS50YXh2YXQgID0gXCJcIjtcblxuXHQkc2NvcGUudXBkYXRlQ05QSiA9IGZ1bmN0aW9uKCl7XG5cdFx0JHNjb3BlLnRheHZhdCAgPSAkc2NvcGUuY25wajtcblx0fVxuXG5cdCRzY29wZS51cGRhdGVOYW1lID0gZnVuY3Rpb24oKXtcblx0XHR2YXIgbmFtZSA9ICRzY29wZS5maXJzdG5hbWUuc3BsaXQoXCIgXCIpO1xuXHRcdGlmKG5hbWUubGVuZ3RoPDIpe1xuXHRcdFx0aWYoalF1ZXJ5KFwiI2ZpcnN0bmFtZVwiKS5wYXJlbnQoKS5maW5kKFwiLmZvcm0tY29udHJvbC1mZWVkYmFja1wiKS5sZW5ndGggPT0gMCApe1xuXHRcdFx0XHRqUXVlcnkoXCIjZmlyc3RuYW1lXCIpLnBhcmVudCgpLmFkZENsYXNzKFwiaGFzLWVycm9yIGhhcy1mZWVkYmFja1wiKTtcblx0XHRcdFx0alF1ZXJ5KFwiI2ZpcnN0bmFtZVwiKS5hdHRyKFwiYXJpYS1kZXNjcmliZWRieVwiLFwiaW5wdXRFcnJvcjJTdGF0dXNcIik7XG5cdFx0XHRcdGpRdWVyeShcIiNmaXJzdG5hbWVcIikuYWZ0ZXIoalF1ZXJ5KCc8c3BhbiBjbGFzcz1cImdseXBoaWNvbiBnbHlwaGljb24tcmVtb3ZlIGZvcm0tY29udHJvbC1mZWVkYmFja1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvc3Bhbj48c3BhbiBpZD1cImlucHV0RXJyb3IyU3RhdHVzXCIgY2xhc3M9XCJzci1vbmx5XCI+KGVycm9yKTwvc3Bhbj4nKSlcblx0XHRcdH1cblx0XHR9ZWxzZXtcblx0XHRcdGpRdWVyeShcIi5mb3JtLWNvbnRyb2wtZmVlZGJhY2tcIikucmVtb3ZlKCk7XG5cdFx0XHRqUXVlcnkoXCIjZmlyc3RuYW1lXCIpLmFmdGVyKCc8c3BhbiBjbGFzcz1cImdseXBoaWNvbiBnbHlwaGljb24tb2sgZm9ybS1jb250cm9sLWZlZWRiYWNrXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9zcGFuPjxzcGFuIGlkPVwiaW5wdXRTdWNjZXNzMlN0YXR1c1wiIGNsYXNzPVwic3Itb25seVwiPihzdWNjZXNzKTwvc3Bhbj4nKTtcblx0XHRcdGpRdWVyeShcIiNmaXJzdG5hbWVcIikucGFyZW50KCkucmVtb3ZlQ2xhc3MoXCJoYXMtZXJyb3JcIikuYWRkQ2xhc3MoXCIgaGFzLXN1Y2Nlc3NcIik7XG5cdFx0XHQkc2NvcGUubGFzdG5hbWUgPSBuYW1lW25hbWUubGVuZ3RoLTFdO1xuXHRcdH1cblx0fVxuXG5cdGpRdWVyeShcIiNmb3JtLXZhbGlkYXRlXCIpLm9uKFwiYmx1clwiLCBcImlucHV0XCIsIGZ1bmN0aW9uKCl7XG5cdFx0aWYoalF1ZXJ5KHRoaXMpLmF0dHIoXCJpZFwiKSE9IFwiZmlyc3RuYW1lXCIpe1xuXHRcdFx0alF1ZXJ5KHRoaXMpLnBhcmVudCgpLmZpbmQoXCIuZm9ybS1jb250cm9sLWZlZWRiYWNrXCIpLnJlbW92ZSgpO1xuXHRcdFx0dmFyIHZhbCA9IChqUXVlcnkodGhpcykuYXR0cihcImlkXCIpPT1cInRheHZhdFwiKSA/IGpRdWVyeSh0aGlzKS52YWwoKS5yZXBsYWNlKC9cXEQvZywgXCJcIikgOiBqUXVlcnkodGhpcykudmFsKCk7XG5cdFx0XHRpZih2YWwubGVuZ3RoID09IDApe1xuXHRcdFx0XHRqUXVlcnkodGhpcykucGFyZW50KCkuYWRkQ2xhc3MoXCJoYXMtZXJyb3IgaGFzLWZlZWRiYWNrXCIpO1xuXHRcdFx0XHRqUXVlcnkodGhpcykuYXR0cihcImFyaWEtZGVzY3JpYmVkYnlcIixcImlucHV0RXJyb3IyU3RhdHVzXCIpO1xuXHRcdFx0XHRqUXVlcnkodGhpcykuYWZ0ZXIoalF1ZXJ5KCc8c3BhbiBjbGFzcz1cImdseXBoaWNvbiBnbHlwaGljb24tcmVtb3ZlIGZvcm0tY29udHJvbC1mZWVkYmFja1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvc3Bhbj4nKSlcblx0XHRcdH1lbHNle1xuXHRcdFx0XHRqUXVlcnkodGhpcykuYWZ0ZXIoJzxzcGFuIGNsYXNzPVwiZ2x5cGhpY29uIGdseXBoaWNvbi1vayBmb3JtLWNvbnRyb2wtZmVlZGJhY2tcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L3NwYW4+Jyk7XG5cdFx0XHRcdGpRdWVyeSh0aGlzKS5wYXJlbnQoKS5yZW1vdmVDbGFzcyhcImhhcy1lcnJvclwiKS5hZGRDbGFzcyhcIiBoYXMtc3VjY2Vzc1wiKTtcblx0XHRcdH1cblx0XHR9XG5cdH0pO1xuXG59KTtcbiIsIid1c2Ugc3RyaWN0J1xuXG4vKipcbiogTWFnZW50byBNb2JpbGUgVGhlbWUgam4yXG4qIERldmVsb3BlcjogSWdvciBHb3R0c2NoYWxnXG4qXG4qIEp1c3QgZm9yIHRoZW1lIGV4ZXB0aW9uXG4qL1xuYXBwLmNvbnRyb2xsZXIoJ3RvcEJhckNvbnRyb2xsZXInLGZ1bmN0aW9uKCRzY29wZSl7XG5cbiAgalF1ZXJ5KFwiLmxvYWRpbmdcIikuaGlkZSgpO1xuXG4gIGpRdWVyeShkb2N1bWVudCkub24oJ2NsaWNrJywgXCJhW2hyZWYhPScjJ11cIiwgZnVuY3Rpb24oKXtcbiAgICBpZihqUXVlcnkodGhpcykuYXR0cihcInRhcmdldFwiKSE9J19ibGFuaycgJiYgIWpRdWVyeSh0aGlzKS5oYXNDbGFzcygnbm8tbG9hZGluZycpKXtcbiAgICAgIGpRdWVyeShcIi5sb2FkaW5nXCIpLnNob3coKTtcbiAgICB9XG4gIH0pO1xuXG5cblxuICBqUXVlcnkoZG9jdW1lbnQpLm9uKFwiY2xpY2tcIixcIi5sb2dvLXN0b3JlXCIsIGZ1bmN0aW9uKCl7XG4gICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBCQVNFX1VSTDtcbiAgfSk7XG5cbiAgJHNjb3BlLm1lbnVTdGF0ZSA9IDA7XG4gICRzY29wZS5zZWFyY2hTdGF0ZSA9IDA7XG4gICRzY29wZS5xID0gXCJcIjtcblxuICAvLyBqUXVlcnkoZG9jdW1lbnQpLm9uKFwiY2xpY2tcIixcIltkYXRhLWNhY2hlLWJ5cGFzcz0nMSddIFtuZy1jbGlja11cIiwgZnVuY3Rpb24oKXtcbiAgLy8gICB2YXIgZm4gPSBqUXVlcnkodGhpcykuYXR0cihcIm5nLWNsaWNrXCIpLnNwbGl0KCcoJylbMF0ucmVwbGFjZSgvXFxzKy9nLCcnKTtcbiAgLy8gICAkc2NvcGVbZm5dKCk7XG4gIC8vICAgLy9ldmFsKFwiJHNjb3BlLlwiICsgalF1ZXJ5KHRoaXMpLmF0dHIoXCJuZy1jbGlja1wiKSk7XG4gIC8vIH0pO1xuXG4gICRzY29wZS5tZW51VG9nZ2xlID0gZnVuY3Rpb24oKXtcbiAgICAkc2NvcGUubWVudVN0YXRlID0gJHNjb3BlLm1lbnVTdGF0ZSA/IDAgOiAxO1xuICB9XG5cbiAgJHNjb3BlLnNlYXJjaFRvZ2dsZSA9IGZ1bmN0aW9uKCl7XG4gICAgJHNjb3BlLnNlYXJjaFN0YXRlID0gISRzY29wZS5zZWFyY2hTdGF0ZTtcbiAgICBpZigkc2NvcGUuc2VhcmNoU3RhdGUpe1xuICAgICAgalF1ZXJ5KFwiaGVhZGVyLmhlYWRlckZpeGVkIGRpdi5zZWFyY2ggLmZvcm0tY29udHJvbFwiKS50cmlnZ2VyKFwiZm9jdXNcIik7XG4gICAgfVxuICB9O1xuXG4gICRzY29wZS5zZWFyY2ggPSBmdW5jdGlvbigpe1xuICAgIGpRdWVyeShcIi5sb2FkaW5nXCIpLnNob3coKTtcbiAgICAvLyBpZihGT1JNX0tFWSl7XG4gICAgICBzZXRMb2NhdGlvbihCQVNFX1VSTCtcImNhdGFsb2dzZWFyY2gvcmVzdWx0Lz9xPVwiICsgZW5jb2RlVVJJQ29tcG9uZW50KCRzY29wZS5xKStcIiZmb3JtX2tleT1cIitGT1JNX0tFWSk7XG4gICAgLy8gfWVsc2V7XG4gICAgLy8gICBqUXVlcnkoZG9jdW1lbnQpLm9uKCdjYWNoZTpmaW5pc2gnLGZ1bmN0aW9uKGV2dCxjb250ZW50KXtcbiAgICAvLyAgICAgdmFyIGRhdGEgPSBjb250ZW50LmRhdGE7XG4gICAgLy8gICAgIHNldExvY2F0aW9uKEJBU0VfVVJMK1wiY2F0YWxvZ3NlYXJjaC9yZXN1bHQvP3E9XCIgKyBlbmNvZGVVUklDb21wb25lbnQoJHNjb3BlLnEpK1wiJmZvcm1fa2V5PVwiK2RhdGEuZm9ybV9rZXkpO1xuICAgIC8vICAgfSk7XG4gICAgLy8gfVxuICB9XG5cbiAgJHNjb3BlLmdvQmFnID0gZnVuY3Rpb24oKXtcbiAgICBqUXVlcnkoXCIubG9hZGluZ1wiKS5zaG93KCk7XG4gICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBCQVNFX1VSTCArIFwiY2hlY2tvdXQvY2FydC9cIjtcbiAgfTtcblxuICBqUXVlcnkoZnVuY3Rpb24oKXtcblxuICAgIGpRdWVyeShcIi5tZW51LW9mZmNhbnZhcyAuY2F0ZWdvcmllcyB1bCB1bFwiKS5zbGlkZVVwKCk7XG5cbiAgICBqUXVlcnkoZG9jdW1lbnQpLm9uKFwiY2xpY2tcIixcIi5tZW51LW9mZmNhbnZhcyAuY2F0ZWdvcmllcyBhXCIsIGZ1bmN0aW9uKCl7XG4gICAgICAkc2NvcGUubWVudVRvZ2dsZSgpO1xuICAgIH0pO1xuXG4gICAgalF1ZXJ5KGRvY3VtZW50KS5vbihcImNsaWNrXCIsIFwiLm1lbnUtb2ZmY2FudmFzIC5jYXRlZ29yaWVzIC5leHBhbmQtbWVudVwiLCBmdW5jdGlvbigpe1xuICAgICAgalF1ZXJ5KHRoaXMpLnBhcmVudCgpLnRvZ2dsZUNsYXNzKFwib3BlblwiKS5maW5kKFwidWxcIikuZmlyc3QoKS5zbGlkZVRvZ2dsZSgpO1xuICAgIH0pO1xuXG4gICAgalF1ZXJ5KGRvY3VtZW50KS5vbihcImNsaWNrXCIsXCIubWVudS1vZmZjYW52YXMgLmNhdGVnb3JpZXMgYSBzcGFuXCIsIGZ1bmN0aW9uKCl7XG4gICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGpRdWVyeSh0aGlzKS5wYXJlbnQoKS5hdHRyKFwiaHJlZlwiKTtcbiAgICB9KTtcblxuICB9KTtcblxuXG59KTtcbiIsIi8qKlxuKiBNYWdlbnRvIE1vYmlsZSBUaGVtZSBqbjJcbiogRGV2ZWxvcGVyOiBJZ29yIEdvdHRzY2hhbGdcbipcbiogSnVzdCBmb3IgdGhlbWUgZXhlcHRpb25cbiovXG5cbmFwcC5kaXJlY3RpdmUoJ25nRXF1YWxlcicsZnVuY3Rpb24oKSB7XG4gIHJldHVybiB7XG4gICAgcmVzdHJpY3Q6ICdBJyxcbiAgICBsaW5rOiBmdW5jdGlvbihzY29wZSwgZWxlbWVudCwgYXR0cikge1xuICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICB2YXIgYXR0cnMgPSBldmFsKCcoJyArIGF0dHIubmdFcXVhbGVyICsgJyknKTtcblxuICAgICAgICBpZihhdHRycy50eXBlID09PSBcIm1heFwiKXtcbiAgICAgICAgICB2YXIgaGVpZ2h0ID0gMDtcbiAgICAgICAgICB2YXIgZWxlbWVudHMgPSBlbGVtZW50LmNoaWxkcmVuKCk7XG4gICAgICAgICAgZWxlbWVudHMuZWFjaChmdW5jdGlvbihpbmRleCwgZWxlbWVudEVxdWFsZXIpe1xuICAgICAgICAgICAgaWYoYXR0cnMuZWxlbWVudC5sZW5ndGggPiAwKXtcbiAgICAgICAgICAgICAgdmFyIGVsZW1lbnRhcnkgPSBqUXVlcnkoZWxlbWVudEVxdWFsZXIpLmZpbmQoYXR0cnMuZWxlbWVudCk7XG4gICAgICAgICAgICAgIGlmKGVsZW1lbnRhcnkuaGVpZ2h0KCkgPiBoZWlnaHQpe1xuICAgICAgICAgICAgICAgIGhlaWdodCA9IGVsZW1lbnRhcnkuaGVpZ2h0KCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICBpZihqUXVlcnkoZWxlbWVudEVxdWFsZXIpLmhlaWdodCgpID4gaGVpZ2h0KXtcbiAgICAgICAgICAgICAgICBoZWlnaHQgPSBqUXVlcnkoZWxlbWVudEVxdWFsZXIpLmhlaWdodCgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgaWYoYXR0cnMuZWxlbWVudC5sZW5ndGggPiAwKXtcbiAgICAgICAgICAgIGVsZW1lbnRzLmVhY2goZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnRFcXVhbGVyKXtcbiAgICAgICAgICAgICAgalF1ZXJ5KGVsZW1lbnRFcXVhbGVyKS5maW5kKGF0dHJzLmVsZW1lbnQpLmNzcyh7XCJoZWlnaHRcIjogXCJjYWxjKFwiK2hlaWdodCtcInB4ICsgM3JlbSlcIn0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICBlbGVtZW50cy5jc3Moe1wiaGVpZ2h0XCI6IFwiY2FsYyhcIitoZWlnaHQrXCJweCArIDJyZW0pXCJ9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH1lbHNle1xuICAgICAgICAgIHZhciBoZWlnaHQgPSBlbGVtZW50LmNoaWxkcmVuKCkuZmlyc3QoKS5oZWlnaHQoKTtcbiAgICAgICAgICB2YXIgZWxlbWVudHMgPSBlbGVtZW50LmNoaWxkcmVuKCk7XG4gICAgICAgICAgZWxlbWVudHMuZWFjaChmdW5jdGlvbihpbmRleCwgZWxlbWVudEVxdWFsZXIpe1xuICAgICAgICAgICAgaWYoalF1ZXJ5KGVsZW1lbnRFcXVhbGVyKS5oZWlnaHQoKSA8IGhlaWdodCl7XG4gICAgICAgICAgICAgIGhlaWdodCA9IGpRdWVyeShlbGVtZW50RXF1YWxlcikuaGVpZ2h0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgZWxlbWVudHMuaGVpZ2h0KGhlaWdodCk7XG4gICAgICAgIH1cbiAgICAgIH0sNTAwKTtcbiAgICB9XG4gIH1cbn0pO1xuIiwiLyohXG4qIFdhdmVzIHYwLjYuNFxuKiBodHRwOi8vZmlhbi5teS5pZC9XYXZlc1xuKlxuKiBDb3B5cmlnaHQgMjAxNCBBbGZpYW5hIEUuIFNpYnVlYSBhbmQgb3RoZXIgY29udHJpYnV0b3JzXG4qIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuKiBodHRwczovL2dpdGh1Yi5jb20vZmlhbnMvV2F2ZXMvYmxvYi9tYXN0ZXIvTElDRU5TRVxuKi9cblxuOyhmdW5jdGlvbih3aW5kb3cpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIHZhciBXYXZlcyA9IFdhdmVzIHx8IHt9O1xuICB2YXIgJCQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsLmJpbmQoZG9jdW1lbnQpO1xuXG4gIC8vIEZpbmQgZXhhY3QgcG9zaXRpb24gb2YgZWxlbWVudFxuICBmdW5jdGlvbiBpc1dpbmRvdyhvYmopIHtcbiAgICByZXR1cm4gb2JqICE9PSBudWxsICYmIG9iaiA9PT0gb2JqLndpbmRvdztcbiAgfVxuXG4gIGZ1bmN0aW9uIGdldFdpbmRvdyhlbGVtKSB7XG4gICAgcmV0dXJuIGlzV2luZG93KGVsZW0pID8gZWxlbSA6IGVsZW0ubm9kZVR5cGUgPT09IDkgJiYgZWxlbS5kZWZhdWx0VmlldztcbiAgfVxuXG4gIGZ1bmN0aW9uIG9mZnNldChlbGVtKSB7XG4gICAgdmFyIGRvY0VsZW0sIHdpbixcbiAgICBib3ggPSB7dG9wOiAwLCBsZWZ0OiAwfSxcbiAgICBkb2MgPSBlbGVtICYmIGVsZW0ub3duZXJEb2N1bWVudDtcblxuICAgIGRvY0VsZW0gPSBkb2MuZG9jdW1lbnRFbGVtZW50O1xuXG4gICAgaWYgKHR5cGVvZiBlbGVtLmdldEJvdW5kaW5nQ2xpZW50UmVjdCAhPT0gdHlwZW9mIHVuZGVmaW5lZCkge1xuICAgICAgYm94ID0gZWxlbS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICB9XG4gICAgd2luID0gZ2V0V2luZG93KGRvYyk7XG4gICAgcmV0dXJuIHtcbiAgICAgIHRvcDogYm94LnRvcCArIHdpbi5wYWdlWU9mZnNldCAtIGRvY0VsZW0uY2xpZW50VG9wLFxuICAgICAgbGVmdDogYm94LmxlZnQgKyB3aW4ucGFnZVhPZmZzZXQgLSBkb2NFbGVtLmNsaWVudExlZnRcbiAgICB9O1xuICB9XG5cbiAgZnVuY3Rpb24gY29udmVydFN0eWxlKG9iaikge1xuICAgIHZhciBzdHlsZSA9ICcnO1xuXG4gICAgZm9yICh2YXIgYSBpbiBvYmopIHtcbiAgICAgIGlmIChvYmouaGFzT3duUHJvcGVydHkoYSkpIHtcbiAgICAgICAgc3R5bGUgKz0gKGEgKyAnOicgKyBvYmpbYV0gKyAnOycpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBzdHlsZTtcbiAgfVxuXG4gIHZhciBFZmZlY3QgPSB7XG5cbiAgICAvLyBFZmZlY3QgZGVsYXlcbiAgICBkdXJhdGlvbjogNzUwLFxuXG4gICAgc2hvdzogZnVuY3Rpb24oZSwgZWxlbWVudCkge1xuXG4gICAgICAvLyBEaXNhYmxlIHJpZ2h0IGNsaWNrXG4gICAgICBpZiAoZS5idXR0b24gPT09IDIpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICB2YXIgZWwgPSBlbGVtZW50IHx8IHRoaXM7XG5cbiAgICAgIC8vIENyZWF0ZSByaXBwbGVcbiAgICAgIHZhciByaXBwbGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgIHJpcHBsZS5jbGFzc05hbWUgPSAnd2F2ZXMtcmlwcGxlJztcbiAgICAgIGVsLmFwcGVuZENoaWxkKHJpcHBsZSk7XG5cbiAgICAgIC8vIEdldCBjbGljayBjb29yZGluYXRlIGFuZCBlbGVtZW50IHdpdGRoXG4gICAgICB2YXIgcG9zICAgICAgICAgPSBvZmZzZXQoZWwpO1xuICAgICAgdmFyIHJlbGF0aXZlWSAgID0gKGUucGFnZVkgLSBwb3MudG9wKTtcbiAgICAgIHZhciByZWxhdGl2ZVggICA9IChlLnBhZ2VYIC0gcG9zLmxlZnQpO1xuICAgICAgdmFyIHNjYWxlICAgICAgID0gJ3NjYWxlKCcrKChlbC5jbGllbnRXaWR0aCAvIDEwMCkgKiAxMCkrJyknO1xuXG4gICAgICAvLyBTdXBwb3J0IGZvciB0b3VjaCBkZXZpY2VzXG4gICAgICBpZiAoJ3RvdWNoZXMnIGluIGUpIHtcbiAgICAgICAgcmVsYXRpdmVZICAgPSAoZS50b3VjaGVzWzBdLnBhZ2VZIC0gcG9zLnRvcCk7XG4gICAgICAgIHJlbGF0aXZlWCAgID0gKGUudG91Y2hlc1swXS5wYWdlWCAtIHBvcy5sZWZ0KTtcbiAgICAgIH1cblxuICAgICAgLy8gQXR0YWNoIGRhdGEgdG8gZWxlbWVudFxuICAgICAgcmlwcGxlLnNldEF0dHJpYnV0ZSgnZGF0YS1ob2xkJywgRGF0ZS5ub3coKSk7XG4gICAgICByaXBwbGUuc2V0QXR0cmlidXRlKCdkYXRhLXNjYWxlJywgc2NhbGUpO1xuICAgICAgcmlwcGxlLnNldEF0dHJpYnV0ZSgnZGF0YS14JywgcmVsYXRpdmVYKTtcbiAgICAgIHJpcHBsZS5zZXRBdHRyaWJ1dGUoJ2RhdGEteScsIHJlbGF0aXZlWSk7XG5cbiAgICAgIC8vIFNldCByaXBwbGUgcG9zaXRpb25cbiAgICAgIHZhciByaXBwbGVTdHlsZSA9IHtcbiAgICAgICAgJ3RvcCc6IHJlbGF0aXZlWSsncHgnLFxuICAgICAgICAnbGVmdCc6IHJlbGF0aXZlWCsncHgnXG4gICAgICB9O1xuXG4gICAgICByaXBwbGUuY2xhc3NOYW1lID0gcmlwcGxlLmNsYXNzTmFtZSArICcgd2F2ZXMtbm90cmFuc2l0aW9uJztcbiAgICAgIHJpcHBsZS5zZXRBdHRyaWJ1dGUoJ3N0eWxlJywgY29udmVydFN0eWxlKHJpcHBsZVN0eWxlKSk7XG4gICAgICByaXBwbGUuY2xhc3NOYW1lID0gcmlwcGxlLmNsYXNzTmFtZS5yZXBsYWNlKCd3YXZlcy1ub3RyYW5zaXRpb24nLCAnJyk7XG5cbiAgICAgIC8vIFNjYWxlIHRoZSByaXBwbGVcbiAgICAgIHJpcHBsZVN0eWxlWyctd2Via2l0LXRyYW5zZm9ybSddID0gc2NhbGU7XG4gICAgICByaXBwbGVTdHlsZVsnLW1vei10cmFuc2Zvcm0nXSA9IHNjYWxlO1xuICAgICAgcmlwcGxlU3R5bGVbJy1tcy10cmFuc2Zvcm0nXSA9IHNjYWxlO1xuICAgICAgcmlwcGxlU3R5bGVbJy1vLXRyYW5zZm9ybSddID0gc2NhbGU7XG4gICAgICByaXBwbGVTdHlsZS50cmFuc2Zvcm0gPSBzY2FsZTtcbiAgICAgIHJpcHBsZVN0eWxlLm9wYWNpdHkgICA9ICcxJztcblxuICAgICAgcmlwcGxlU3R5bGVbJy13ZWJraXQtdHJhbnNpdGlvbi1kdXJhdGlvbiddID0gRWZmZWN0LmR1cmF0aW9uICsgJ21zJztcbiAgICAgIHJpcHBsZVN0eWxlWyctbW96LXRyYW5zaXRpb24tZHVyYXRpb24nXSAgICA9IEVmZmVjdC5kdXJhdGlvbiArICdtcyc7XG4gICAgICByaXBwbGVTdHlsZVsnLW8tdHJhbnNpdGlvbi1kdXJhdGlvbiddICAgICAgPSBFZmZlY3QuZHVyYXRpb24gKyAnbXMnO1xuICAgICAgcmlwcGxlU3R5bGVbJ3RyYW5zaXRpb24tZHVyYXRpb24nXSAgICAgICAgID0gRWZmZWN0LmR1cmF0aW9uICsgJ21zJztcblxuICAgICAgcmlwcGxlU3R5bGVbJy13ZWJraXQtdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb24nXSA9ICdjdWJpYy1iZXppZXIoMC4yNTAsIDAuNDYwLCAwLjQ1MCwgMC45NDApJztcbiAgICAgIHJpcHBsZVN0eWxlWyctbW96LXRyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uJ10gICAgPSAnY3ViaWMtYmV6aWVyKDAuMjUwLCAwLjQ2MCwgMC40NTAsIDAuOTQwKSc7XG4gICAgICByaXBwbGVTdHlsZVsnLW8tdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb24nXSAgICAgID0gJ2N1YmljLWJlemllcigwLjI1MCwgMC40NjAsIDAuNDUwLCAwLjk0MCknO1xuICAgICAgcmlwcGxlU3R5bGVbJ3RyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uJ10gICAgICAgICA9ICdjdWJpYy1iZXppZXIoMC4yNTAsIDAuNDYwLCAwLjQ1MCwgMC45NDApJztcblxuICAgICAgcmlwcGxlLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBjb252ZXJ0U3R5bGUocmlwcGxlU3R5bGUpKTtcbiAgICB9LFxuXG4gICAgaGlkZTogZnVuY3Rpb24oZSkge1xuICAgICAgVG91Y2hIYW5kbGVyLnRvdWNodXAoZSk7XG5cbiAgICAgIHZhciBlbCA9IHRoaXM7XG4gICAgICB2YXIgd2lkdGggPSBlbC5jbGllbnRXaWR0aCAqIDEuNDtcblxuICAgICAgLy8gR2V0IGZpcnN0IHJpcHBsZVxuICAgICAgdmFyIHJpcHBsZSA9IG51bGw7XG4gICAgICB2YXIgcmlwcGxlcyA9IGVsLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3dhdmVzLXJpcHBsZScpO1xuICAgICAgaWYgKHJpcHBsZXMubGVuZ3RoID4gMCkge1xuICAgICAgICByaXBwbGUgPSByaXBwbGVzW3JpcHBsZXMubGVuZ3RoIC0gMV07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHZhciByZWxhdGl2ZVggICA9IHJpcHBsZS5nZXRBdHRyaWJ1dGUoJ2RhdGEteCcpO1xuICAgICAgdmFyIHJlbGF0aXZlWSAgID0gcmlwcGxlLmdldEF0dHJpYnV0ZSgnZGF0YS15Jyk7XG4gICAgICB2YXIgc2NhbGUgICAgICAgPSByaXBwbGUuZ2V0QXR0cmlidXRlKCdkYXRhLXNjYWxlJyk7XG5cbiAgICAgIC8vIEdldCBkZWxheSBiZWV0d2VlbiBtb3VzZWRvd24gYW5kIG1vdXNlIGxlYXZlXG4gICAgICB2YXIgZGlmZiA9IERhdGUubm93KCkgLSBOdW1iZXIocmlwcGxlLmdldEF0dHJpYnV0ZSgnZGF0YS1ob2xkJykpO1xuICAgICAgdmFyIGRlbGF5ID0gMzUwIC0gZGlmZjtcblxuICAgICAgaWYgKGRlbGF5IDwgMCkge1xuICAgICAgICBkZWxheSA9IDA7XG4gICAgICB9XG5cbiAgICAgIC8vIEZhZGUgb3V0IHJpcHBsZSBhZnRlciBkZWxheVxuICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIHN0eWxlID0ge1xuICAgICAgICAgICd0b3AnOiByZWxhdGl2ZVkrJ3B4JyxcbiAgICAgICAgICAnbGVmdCc6IHJlbGF0aXZlWCsncHgnLFxuICAgICAgICAgICdvcGFjaXR5JzogJzAnLFxuXG4gICAgICAgICAgLy8gRHVyYXRpb25cbiAgICAgICAgICAnLXdlYmtpdC10cmFuc2l0aW9uLWR1cmF0aW9uJzogRWZmZWN0LmR1cmF0aW9uICsgJ21zJyxcbiAgICAgICAgICAnLW1vei10cmFuc2l0aW9uLWR1cmF0aW9uJzogRWZmZWN0LmR1cmF0aW9uICsgJ21zJyxcbiAgICAgICAgICAnLW8tdHJhbnNpdGlvbi1kdXJhdGlvbic6IEVmZmVjdC5kdXJhdGlvbiArICdtcycsXG4gICAgICAgICAgJ3RyYW5zaXRpb24tZHVyYXRpb24nOiBFZmZlY3QuZHVyYXRpb24gKyAnbXMnLFxuICAgICAgICAgICctd2Via2l0LXRyYW5zZm9ybSc6IHNjYWxlLFxuICAgICAgICAgICctbW96LXRyYW5zZm9ybSc6IHNjYWxlLFxuICAgICAgICAgICctbXMtdHJhbnNmb3JtJzogc2NhbGUsXG4gICAgICAgICAgJy1vLXRyYW5zZm9ybSc6IHNjYWxlLFxuICAgICAgICAgICd0cmFuc2Zvcm0nOiBzY2FsZSxcbiAgICAgICAgfTtcblxuICAgICAgICByaXBwbGUuc2V0QXR0cmlidXRlKCdzdHlsZScsIGNvbnZlcnRTdHlsZShzdHlsZSkpO1xuXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGVsLnJlbW92ZUNoaWxkKHJpcHBsZSk7XG4gICAgICAgICAgfSBjYXRjaChlKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfVxuICAgICAgICB9LCBFZmZlY3QuZHVyYXRpb24pO1xuICAgICAgfSwgZGVsYXkpO1xuICAgIH0sXG5cbiAgICAvLyBMaXR0bGUgaGFjayB0byBtYWtlIDxpbnB1dD4gY2FuIHBlcmZvcm0gd2F2ZXMgZWZmZWN0XG4gICAgd3JhcElucHV0OiBmdW5jdGlvbihlbGVtZW50cykge1xuICAgICAgZm9yICh2YXIgYSA9IDA7IGEgPCBlbGVtZW50cy5sZW5ndGg7IGErKykge1xuICAgICAgICB2YXIgZWwgPSBlbGVtZW50c1thXTtcblxuICAgICAgICBpZiAoZWwudGFnTmFtZS50b0xvd2VyQ2FzZSgpID09PSAnaW5wdXQnKSB7XG4gICAgICAgICAgdmFyIHBhcmVudCA9IGVsLnBhcmVudE5vZGU7XG5cbiAgICAgICAgICAvLyBJZiBpbnB1dCBhbHJlYWR5IGhhdmUgcGFyZW50IGp1c3QgcGFzcyB0aHJvdWdoXG4gICAgICAgICAgaWYgKHBhcmVudC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgPT09ICdpJyAmJiBwYXJlbnQuY2xhc3NOYW1lLmluZGV4T2YoJ3dhdmVzLWVmZmVjdCcpICE9PSAtMSkge1xuICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gUHV0IGVsZW1lbnQgY2xhc3MgYW5kIHN0eWxlIHRvIHRoZSBzcGVjaWZpZWQgcGFyZW50XG4gICAgICAgICAgdmFyIHdyYXBwZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpJyk7XG4gICAgICAgICAgd3JhcHBlci5jbGFzc05hbWUgPSBlbC5jbGFzc05hbWUgKyAnIHdhdmVzLWlucHV0LXdyYXBwZXInO1xuXG4gICAgICAgICAgdmFyIGVsZW1lbnRTdHlsZSA9IGVsLmdldEF0dHJpYnV0ZSgnc3R5bGUnKTtcblxuICAgICAgICAgIGlmICghZWxlbWVudFN0eWxlKSB7XG4gICAgICAgICAgICBlbGVtZW50U3R5bGUgPSAnJztcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB3cmFwcGVyLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBlbGVtZW50U3R5bGUpO1xuXG4gICAgICAgICAgZWwuY2xhc3NOYW1lID0gJ3dhdmVzLWJ1dHRvbi1pbnB1dCc7XG4gICAgICAgICAgZWwucmVtb3ZlQXR0cmlidXRlKCdzdHlsZScpO1xuXG4gICAgICAgICAgLy8gUHV0IGVsZW1lbnQgYXMgY2hpbGRcbiAgICAgICAgICBwYXJlbnQucmVwbGFjZUNoaWxkKHdyYXBwZXIsIGVsKTtcbiAgICAgICAgICB3cmFwcGVyLmFwcGVuZENoaWxkKGVsKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfTtcblxuXG4gIC8qKlxuICAqIERpc2FibGUgbW91c2Vkb3duIGV2ZW50IGZvciA1MDBtcyBkdXJpbmcgYW5kIGFmdGVyIHRvdWNoXG4gICovXG4gIHZhciBUb3VjaEhhbmRsZXIgPSB7XG4gICAgLyogdXNlcyBhbiBpbnRlZ2VyIHJhdGhlciB0aGFuIGJvb2wgc28gdGhlcmUncyBubyBpc3N1ZXMgd2l0aFxuICAgICogbmVlZGluZyB0byBjbGVhciB0aW1lb3V0cyBpZiBhbm90aGVyIHRvdWNoIGV2ZW50IG9jY3VycmVkXG4gICAgKiB3aXRoaW4gdGhlIDUwMG1zLiBDYW5ub3QgbW91c2V1cCBiZXR3ZWVuIHRvdWNoc3RhcnQgYW5kXG4gICAgKiB0b3VjaGVuZCwgbm9yIGluIHRoZSA1MDBtcyBhZnRlciB0b3VjaGVuZC4gKi9cbiAgICB0b3VjaGVzOiAwLFxuICAgIGFsbG93RXZlbnQ6IGZ1bmN0aW9uKGUpIHtcbiAgICAgIHZhciBhbGxvdyA9IHRydWU7XG5cbiAgICAgIGlmIChlLnR5cGUgPT09ICd0b3VjaHN0YXJ0Jykge1xuICAgICAgICBUb3VjaEhhbmRsZXIudG91Y2hlcyArPSAxOyAvL3B1c2hcbiAgICAgIH0gZWxzZSBpZiAoZS50eXBlID09PSAndG91Y2hlbmQnIHx8IGUudHlwZSA9PT0gJ3RvdWNoY2FuY2VsJykge1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmIChUb3VjaEhhbmRsZXIudG91Y2hlcyA+IDApIHtcbiAgICAgICAgICAgIFRvdWNoSGFuZGxlci50b3VjaGVzIC09IDE7IC8vcG9wIGFmdGVyIDUwMG1zXG4gICAgICAgICAgfVxuICAgICAgICB9LCA1MDApO1xuICAgICAgfSBlbHNlIGlmIChlLnR5cGUgPT09ICdtb3VzZWRvd24nICYmIFRvdWNoSGFuZGxlci50b3VjaGVzID4gMCkge1xuICAgICAgICBhbGxvdyA9IGZhbHNlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gYWxsb3c7XG4gICAgfSxcbiAgICB0b3VjaHVwOiBmdW5jdGlvbihlKSB7XG4gICAgICBUb3VjaEhhbmRsZXIuYWxsb3dFdmVudChlKTtcbiAgICB9XG4gIH07XG5cblxuICAvKipcbiAgKiBEZWxlZ2F0ZWQgY2xpY2sgaGFuZGxlciBmb3IgLndhdmVzLWVmZmVjdCBlbGVtZW50LlxuICAqIHJldHVybnMgbnVsbCB3aGVuIC53YXZlcy1lZmZlY3QgZWxlbWVudCBub3QgaW4gXCJjbGljayB0cmVlXCJcbiAgKi9cbiAgZnVuY3Rpb24gZ2V0V2F2ZXNFZmZlY3RFbGVtZW50KGUpIHtcbiAgICBpZiAoVG91Y2hIYW5kbGVyLmFsbG93RXZlbnQoZSkgPT09IGZhbHNlKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICB2YXIgZWxlbWVudCA9IG51bGw7XG4gICAgdmFyIHRhcmdldCA9IGUudGFyZ2V0IHx8IGUuc3JjRWxlbWVudDtcblxuICAgIHdoaWxlICh0YXJnZXQucGFyZW50RWxlbWVudCAhPT0gbnVsbCkge1xuICAgICAgaWYgKCEodGFyZ2V0IGluc3RhbmNlb2YgU1ZHRWxlbWVudCkgJiYgdGFyZ2V0LmNsYXNzTmFtZS5pbmRleE9mKCd3YXZlcy1lZmZlY3QnKSAhPT0gLTEpIHtcbiAgICAgICAgZWxlbWVudCA9IHRhcmdldDtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9IGVsc2UgaWYgKHRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoJ3dhdmVzLWVmZmVjdCcpKSB7XG4gICAgICAgIGVsZW1lbnQgPSB0YXJnZXQ7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgICAgdGFyZ2V0ID0gdGFyZ2V0LnBhcmVudEVsZW1lbnQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIGVsZW1lbnQ7XG4gIH1cblxuICAvKipcbiAgKiBCdWJibGUgdGhlIGNsaWNrIGFuZCBzaG93IGVmZmVjdCBpZiAud2F2ZXMtZWZmZWN0IGVsZW0gd2FzIGZvdW5kXG4gICovXG4gIGZ1bmN0aW9uIHNob3dFZmZlY3QoZSkge1xuICAgIHZhciBlbGVtZW50ID0gZ2V0V2F2ZXNFZmZlY3RFbGVtZW50KGUpO1xuXG4gICAgaWYgKGVsZW1lbnQgIT09IG51bGwpIHtcbiAgICAgIEVmZmVjdC5zaG93KGUsIGVsZW1lbnQpO1xuXG4gICAgICBpZiAoJ29udG91Y2hzdGFydCcgaW4gd2luZG93KSB7XG4gICAgICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCBFZmZlY3QuaGlkZSwgZmFsc2UpO1xuICAgICAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoY2FuY2VsJywgRWZmZWN0LmhpZGUsIGZhbHNlKTtcbiAgICAgIH1cblxuICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZXVwJywgRWZmZWN0LmhpZGUsIGZhbHNlKTtcbiAgICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsIEVmZmVjdC5oaWRlLCBmYWxzZSk7XG4gICAgfVxuICB9XG5cbiAgV2F2ZXMuZGlzcGxheUVmZmVjdCA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcblxuICAgIGlmICgnZHVyYXRpb24nIGluIG9wdGlvbnMpIHtcbiAgICAgIEVmZmVjdC5kdXJhdGlvbiA9IG9wdGlvbnMuZHVyYXRpb247XG4gICAgfVxuXG4gICAgLy9XcmFwIGlucHV0IGluc2lkZSA8aT4gdGFnXG4gICAgRWZmZWN0LndyYXBJbnB1dCgkJCgnLndhdmVzLWVmZmVjdCcpKTtcblxuICAgIGlmICgnb250b3VjaHN0YXJ0JyBpbiB3aW5kb3cpIHtcbiAgICAgIGRvY3VtZW50LmJvZHkuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hzdGFydCcsIHNob3dFZmZlY3QsIGZhbHNlKTtcbiAgICB9XG5cbiAgICBkb2N1bWVudC5ib2R5LmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZG93bicsIHNob3dFZmZlY3QsIGZhbHNlKTtcbiAgfTtcblxuICAvKipcbiAgKiBBdHRhY2ggV2F2ZXMgdG8gYW4gaW5wdXQgZWxlbWVudCAob3IgYW55IGVsZW1lbnQgd2hpY2ggZG9lc24ndFxuICAqIGJ1YmJsZSBtb3VzZXVwL21vdXNlZG93biBldmVudHMpLlxuICAqICAgSW50ZW5kZWQgdG8gYmUgdXNlZCB3aXRoIGR5bmFtaWNhbGx5IGxvYWRlZCBmb3Jtcy9pbnB1dHMsIG9yXG4gICogd2hlcmUgdGhlIHVzZXIgZG9lc24ndCB3YW50IGEgZGVsZWdhdGVkIGNsaWNrIGhhbmRsZXIuXG4gICovXG4gIFdhdmVzLmF0dGFjaCA9IGZ1bmN0aW9uKGVsZW1lbnQpIHtcbiAgICAvL0ZVVFVSRTogYXV0b21hdGljYWxseSBhZGQgd2F2ZXMgY2xhc3NlcyBhbmQgYWxsb3cgdXNlcnNcbiAgICAvLyB0byBzcGVjaWZ5IHRoZW0gd2l0aCBhbiBvcHRpb25zIHBhcmFtPyBFZy4gbGlnaHQvY2xhc3NpYy9idXR0b25cbiAgICBpZiAoZWxlbWVudC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgPT09ICdpbnB1dCcpIHtcbiAgICAgIEVmZmVjdC53cmFwSW5wdXQoW2VsZW1lbnRdKTtcbiAgICAgIGVsZW1lbnQgPSBlbGVtZW50LnBhcmVudEVsZW1lbnQ7XG4gICAgfVxuXG4gICAgaWYgKCdvbnRvdWNoc3RhcnQnIGluIHdpbmRvdykge1xuICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0Jywgc2hvd0VmZmVjdCwgZmFsc2UpO1xuICAgIH1cblxuICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vkb3duJywgc2hvd0VmZmVjdCwgZmFsc2UpO1xuICB9O1xuXG4gIHdpbmRvdy5XYXZlcyA9IFdhdmVzO1xuXG4gIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBmdW5jdGlvbigpIHtcbiAgICBXYXZlcy5kaXNwbGF5RWZmZWN0KCk7XG4gIH0sIGZhbHNlKTtcblxufSkod2luZG93KTtcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
