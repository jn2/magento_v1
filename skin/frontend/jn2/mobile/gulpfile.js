var gulp         = require('gulp');
var gp_concat    = require('gulp-concat');
var sourcemaps   = require('gulp-sourcemaps');
var minify       = require('gulp-minify');
var dependencies = require('./src/depedencies.json');

var paths = {
  scripts: ['src/**/*.js']
};

/*
* Compile all JS dependece & Minify
*/
gulp.task('scripts', function() {
  return gulp.src(dependencies)
  .pipe(sourcemaps.init())
  .pipe(gp_concat('main.js'))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('./js'));
});

/*
* Gulp Watch Task
*/
gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['scripts']);
});


gulp.task('default', ['watch', 'scripts']);
