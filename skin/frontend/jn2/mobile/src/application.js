'use strict'

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/

/**
* App Init
*/
var app = angular.module('Jn2MobileApp', ['angular-preload-image','ui-rangeSlider']);

var FORM_KEY=null;
(function($){
  var initAngular = function(){
      angular.bootstrap(document, ['Jn2MobileApp']);
  }

 $(function(){
    // if(window.Jn2 && window.Jn2.CacheBypass !== undefined){
      $(document).on('cache:finish',function(evt,content){
        var data     = content.data;
        FORM_KEY     = data.form_key;
        initAngular();
      });
      $(document).on('cache:error',initAngular);
    // }else{
      // initAngular();
    // }
  });
})(jQuery);
//
