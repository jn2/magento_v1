/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/


app.controller('categoryController',function($scope, $interval){
  $scope.price = {
    min: null,
    max: null
  }

  str = jQuery("div.filter").html().trim();
  if(str.length === 0){
    jQuery("span.filter").hide();
  }

  $scope.apllyFilter = function(){
    var url = window.location.href.split("?")[0];
    setLocation(url+"?price="+$scope.price.min+"-"+$scope.price.max);
  }

  imagesLoaded( '.products-grid', function(){
    var msnry = new Masonry('.products-grid', {
      itemselector: '.grid-item',
      columnwidth: '.grid-item',
    });
  });

  $scope.filterStatus = 0;

  $scope.filterToggle = function(){
    $scope.filterStatus = !$scope.filterStatus;
  }

});
