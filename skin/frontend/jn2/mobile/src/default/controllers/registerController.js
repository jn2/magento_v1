/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/


app.controller('registerController',function($scope){
	$scope.pessoa = 0;
	$scope.zipcode = 0;
	$scope.firstname = "";
	$scope.lastname  = "";
	$scope.cnpj  = "";
	$scope.taxvat  = "";

	$scope.updateCNPJ = function(){
		$scope.taxvat  = $scope.cnpj;
	}

	$scope.updateName = function(){
		var name = $scope.firstname.split(" ");
		if(name.length<2){
			if(jQuery("#firstname").parent().find(".form-control-feedback").length == 0 ){
				jQuery("#firstname").parent().addClass("has-error has-feedback");
				jQuery("#firstname").attr("aria-describedby","inputError2Status");
				jQuery("#firstname").after(jQuery('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span><span id="inputError2Status" class="sr-only">(error)</span>'))
			}
		}else{
			jQuery(".form-control-feedback").remove();
			jQuery("#firstname").after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span><span id="inputSuccess2Status" class="sr-only">(success)</span>');
			jQuery("#firstname").parent().removeClass("has-error").addClass(" has-success");
			$scope.lastname = name[name.length-1];
		}
	}

	jQuery("#form-validate").on("blur", "input", function(){
		if(jQuery(this).attr("id")!= "firstname"){
			jQuery(this).parent().find(".form-control-feedback").remove();
			var val = (jQuery(this).attr("id")=="taxvat") ? jQuery(this).val().replace(/\D/g, "") : jQuery(this).val();
			if(val.length == 0){
				jQuery(this).parent().addClass("has-error has-feedback");
				jQuery(this).attr("aria-describedby","inputError2Status");
				jQuery(this).after(jQuery('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'))
			}else{
				jQuery(this).after('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
				jQuery(this).parent().removeClass("has-error").addClass(" has-success");
			}
		}
	});

});
