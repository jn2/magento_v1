/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/

app.controller('productController',function($scope){

  $scope.description = 0;
  $scope.options = 1;
  $scope.shipping = 0;
  $scope.comments = 0;

  $scope.buy_action = function(){

  }

  $scope.plusProduct = function(id){
    var value = parseInt(jQuery(id).val());
    if(value < jQuery(id).data("max")){
      jQuery(id).val(value+1);
    }

    if(value < jQuery(id).data("max")){
      jQuery(id).parent().find(".plus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".plus").addClass("disable");
    }

    if(value > 1){
      jQuery(id).parent().find(".minus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".minus").addClass("disable");
    }
  }

  $scope.minusProduct = function(id){
    var value = parseInt(jQuery(id).val());
    if(value > 1){
      jQuery(id).val(value-1);
    }

    if(value < jQuery(id).data("max")){
      jQuery(id).parent().find(".plus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".plus").addClass("disable");
    }

    if(value > 1){
      jQuery(id).parent().find(".minus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".minus").addClass("disable");
    }
  }

  jQuery(function($){
    var $media = $('#media-slider').owlCarousel({
      navigation: false,
      pagination: true,
      singleItem: true,
      items: 1,
      autoPlay: false
    });

    jQuery(".swatchContainer a").addClass("no-loading");

    jQuery(document).on("click", ".swatchContainer a", function(e){
      e.preventDefault();
      var url = $(this).attr("href").split('-zoom.')[0];
      url = url.split("/");
      url = url[url.length-1];
      var image = $(".more-views").find("img[src*='" + url + "']").parent();
      $media.trigger('owl.jumpTo', image.index());
      return false;
    });

    jQuery(".options").on("change",".swatchSelect", function(){
      var link = $(".swatchesContainer").find(".swatchSelected").parent();
      var url = $(link).attr("href").split('-zoom.')[0];
      url = url.split("/");
      url = url[url.length-1];
      var image = $(".more-views").find("img[src*='" + url + "']").parent();
      $media.trigger('owl.jumpTo', image.index());
    });

    function getEmptyOptions(){
      return jQuery('.product-options .required-entry').filter(function(){
        return !this.value;
      });
    }

    jQuery(".add-to-cart .btn-cart, .block-shipping-estimate button").click(function(){
      if(getEmptyOptions().length > 0){
        alert("É necessário escolher uma opção para o produto");
        window.scrollTo(getEmptyOptions().first().offset().top,0);
      }
    });
  });

});
