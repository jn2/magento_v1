/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/

app.controller('cartController',function($scope){

  $scope.plusProduct = function(id){
    var value = parseInt(jQuery(id).val());
    if(value < jQuery(id).data("max")){
      jQuery(id).val(value+1);
    }

    if(value < jQuery(id).data("max")){
      jQuery(id).parent().find(".plus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".plus").addClass("disable");
    }

    if(value > 1){
      jQuery(id).parent().find(".minus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".minus").addClass("disable");
    }

    jQuery("#update-cart").submit();
  }

  $scope.blurQtyItem = function(id){
    var value = parseInt(jQuery(id).val());
    if(value > jQuery(id).data("max")) jQuery(id).val(jQuery(id).data("max"));

    if(value < jQuery(id).data("max")){
      jQuery(id).parent().find(".plus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".plus").addClass("disable");
    }

    if(value > 1){
      jQuery(id).parent().find(".minus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".minus").addClass("disable");
    }

    jQuery("#update-cart").submit();
  }

  $scope.minusProduct = function(id){
    var value = parseInt(jQuery(id).val());
    if(value > 1){
      jQuery(id).val(value-1);
    }

    if(value < jQuery(id).data("max")){
      jQuery(id).parent().find(".plus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".plus").addClass("disable");
    }

    if(value > 1){
      jQuery(id).parent().find(".minus").removeClass("disable");
    }else{
      jQuery(id).parent().find(".minus").addClass("disable");
    }

    jQuery("#update-cart").submit();
  }

  jQuery(document).ready(function($){

    $(".frame-card").find(".item").each(function(index,element){
      var $height = $(element).height();
      $(element).find(".qty").height($height);
      $(element).find(".image").height($height);
      $(element).find(".info").height($height);
    });

    $("#update-cart").submit(function(e){
      e.preventDefault();
      jQuery(".loading").show();
      $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success: function (data) {
          location.reload();
        }
      });
      return false;
    });

  });

});
