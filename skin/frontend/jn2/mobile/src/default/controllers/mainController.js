/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/

app.controller('mainController',function($scope){
  jQuery(".pitbar-mobile .jicon").text("");
  jQuery(".view").addClass("load")

  jQuery(window).unload(function(){
    jQuery(".view").removeClass("load");
  });

  var pinHeight = jQuery(".pitbar-mobile").find(".col-xs-6").first().height();
  jQuery(".pitbar-mobile").find(".col-xs-6").each(function(index,element){
    if(jQuery(element).height() > pinHeight){
      pinHeight = jQuery(element).height();
    }
  });

  jQuery(".pitbar-mobile").find(".col-xs-6").height(pinHeight);

  jQuery('.btn').addClass("waves-effect");
  jQuery('.item').addClass("waves-effect");
  jQuery('button').addClass("waves-effect");
  jQuery('a[href]').addClass("waves-effect");
  jQuery('.button').addClass("waves-effect");
  jQuery('.frame-card').addClass("waves-effect");

});
