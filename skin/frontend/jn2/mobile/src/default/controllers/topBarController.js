'use strict'

/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/
app.controller('topBarController',function($scope){

//  jQuery(".loading").hide();

//  jQuery(document).on('click', "a[href!='#']", function(){
//    if(jQuery(this).attr("target")!='_blank' && !jQuery(this).hasClass('no-loading')){
//      jQuery(".loading").show();
//    }
//  });



  jQuery(document).on("click",".logo-store", function(){
    window.location.href = BASE_URL;
  });

  $scope.menuState = 0;
  $scope.searchState = 0;
  $scope.q = "";

  // jQuery(document).on("click","[data-cache-bypass='1'] [ng-click]", function(){
  //   var fn = jQuery(this).attr("ng-click").split('(')[0].replace(/\s+/g,'');
  //   $scope[fn]();
  //   //eval("$scope." + jQuery(this).attr("ng-click"));
  // });

  $scope.menuToggle = function(){
    $scope.menuState = $scope.menuState ? 0 : 1;
  }

  $scope.searchToggle = function(){
    $scope.searchState = !$scope.searchState;
    if($scope.searchState){
      jQuery("header.headerFixed div.search .form-control").trigger("focus");
    }
  };

  $scope.search = function(){
    jQuery(".loading").show();
    // if(FORM_KEY){
      setLocation(BASE_URL+"catalogsearch/result/?q=" + encodeURIComponent($scope.q)+"&form_key="+FORM_KEY);
    // }else{
    //   jQuery(document).on('cache:finish',function(evt,content){
    //     var data = content.data;
    //     setLocation(BASE_URL+"catalogsearch/result/?q=" + encodeURIComponent($scope.q)+"&form_key="+data.form_key);
    //   });
    // }
  }

  $scope.goBag = function(){
    jQuery(".loading").show();
    window.location.href = BASE_URL + "checkout/cart/";
  };

  jQuery(function(){

    jQuery(".menu-offcanvas .categories ul ul").slideUp();

    jQuery(document).on("click",".menu-offcanvas .categories a", function(){
      $scope.menuToggle();
    });

    jQuery(document).on("click", ".menu-offcanvas .categories .expand-menu", function(){
      jQuery(this).parent().toggleClass("open").find("ul").first().slideToggle();
    });

    jQuery(document).on("click",".menu-offcanvas .categories a span", function(){
      window.location.href = jQuery(this).parent().attr("href");
    });

  });


});
