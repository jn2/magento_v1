/**
* Magento Mobile Theme jn2
* Developer: Igor Gottschalg
*
* Just for theme exeption
*/

app.directive('ngEqualer',function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      setTimeout(function(){
        var attrs = eval('(' + attr.ngEqualer + ')');

        if(attrs.type === "max"){
          var height = 0;
          var elements = element.children();
          elements.each(function(index, elementEqualer){
            if(attrs.element.length > 0){
              var elementary = jQuery(elementEqualer).find(attrs.element);
              if(elementary.height() > height){
                height = elementary.height();
              }
            }else{
              if(jQuery(elementEqualer).height() > height){
                height = jQuery(elementEqualer).height();
              }
            }
          });
          if(attrs.element.length > 0){
            elements.each(function(index, elementEqualer){
              jQuery(elementEqualer).find(attrs.element).css({"height": "calc("+height+"px + 3rem)"});
            });
          }else{
            elements.css({"height": "calc("+height+"px + 2rem)"});
          }
        }else{
          var height = element.children().first().height();
          var elements = element.children();
          elements.each(function(index, elementEqualer){
            if(jQuery(elementEqualer).height() < height){
              height = jQuery(elementEqualer).height();
            }
          });
          elements.height(height);
        }
      },500);
    }
  }
});
