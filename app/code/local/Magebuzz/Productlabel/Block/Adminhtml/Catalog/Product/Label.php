<?php
/*
* Copyright (c) 2014 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Block_Adminhtml_Catalog_Product_Label extends Mage_Core_Block_Template {
	public function _prepareLayout() {
    $this->setTemplate('productlabel/addlabel.phtml');
		return parent::_prepareLayout();    
  }  
  public function getGridLabelHtml(){
    return  $this->getLayout()->createBlock('productlabel/adminhtml_catalog_product_edit_grid')->toHtml();
  }
  
  public function getPosition(){
    return $position = Mage::getModel('productlabel/labelposition')->getOptionArray();
  }
}