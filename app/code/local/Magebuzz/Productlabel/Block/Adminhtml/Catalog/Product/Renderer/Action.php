<?php
/*
* @copyright   Copyright ( c ) 2013 www.magebuzz.com
*/

class Magebuzz_Productlabel_Block_Adminhtml_Catalog_Product_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{    
  public function render(Varien_Object $row)
  {  
    $html = '';
    if($row->getId()==""){
      return $html;
    }
    else{
        $product = $this->getRequest()->getParam('id');
         $url= $this->getUrl('productlabel/adminhtml_productlabel/edit',array('id'=>$row->getId(),'product'=>$product));
         $html .= "<a href='".$url."'>Edit</a>";
    }
    return $html;
  }
}