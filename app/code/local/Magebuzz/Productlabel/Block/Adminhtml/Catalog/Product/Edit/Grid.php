<?php
/*
* Copyright (c) 2014 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Block_Adminhtml_Catalog_Product_Edit_Grid extends Mage_Adminhtml_Block_Widget_Grid {
  public function __construct() {
    parent::__construct();
    $this->setId('productlabelGrid');
    $this->setDefaultSort('label_id');
    $this->setDefaultDir('ASC');
    $this->setSaveParametersInSession(true);
    $this->setUseAjax(true);   
  }

  // get collection Label filter status Enabel
  protected function _prepareCollection() {
    $collection = Mage::getModel('productlabel/productlabel')->getCollection()->addFieldToFilter('status',Magebuzz_Productlabel_Model_Status::STATUS_ENABLED);
    $this->setCollection($collection);
    return parent::_prepareCollection();
  }
  // filter collection by label_id when label_id is selected
  protected function _addColumnFilterToCollection($column)
  {
    // Set custom filter for in product flag
    if ($column->getId() == 'in_label_id') {
      $labelId = $this->_getSelectedCodes();
      if (empty($labelId)) {
        $labelId = 0;
      }
      if ($column->getFilter()->getValue()) {
        $this->getCollection()->addFieldToFilter('label_id', array('in' => $codeIds));
      } else {
        if($productIds) {
          $this->getCollection()->addFieldToFilter('label_id', array('nin' => $codeIds));
        }
      }
    } else {
      parent::_addColumnFilterToCollection($column);
    }
    return $this;
  }

  protected function _prepareColumns() {

    $this->addColumn('in_label_id', array(
    'header_css_class'  => 'a-center',
    'type'              => 'checkbox',
    'html_name'         => 'label_id[]',
    'field_name'         => 'label_id[]',
    'align'             => 'center',  
    'index'             => 'label_id',
    'values'             => $this->_getSelectedCodes(),
    ));

    $this->addColumn('label_id', array(
    'header'    => Mage::helper('productlabel')->__('ID'),
    'align'     =>'right',
    'width'     => '50px',
    'index'     => 'label_id',
    ));

    $this->addColumn('title', array(
    'header'    => Mage::helper('productlabel')->__('Title'),
    'align'     =>'left',
    'index'     => 'title',
    ));

    $this->addColumn('category_img', array(
    'header'    => Mage::helper('productlabel')->__('Category Image'),
    'align'     =>'center',
    'width'     =>'110px',    
    'index'     => 'category_img',
    'renderer'  => 'Magebuzz_Productlabel_Block_Adminhtml_Productlabel_Renderer_Catalogimage'
    ));

    $this->addColumn('product_img', array(
    'header'    => Mage::helper('productlabel')->__('Product Image'),
    'align'     =>'center',
    'width'     =>'110px',    
    'index'     => 'product_img',
    'renderer'  => 'Magebuzz_Productlabel_Block_Adminhtml_Productlabel_Renderer_Productimage'
    ));

    $this->addColumn('status', array(
    'header'    => Mage::helper('productlabel')->__('Status'),
    'align'     => 'left',
    'width'     => '80px',
    'index'     => 'status',
    'type'      => 'options',
    'options'   => array(
    1 => 'Enabled',
    2 => 'Disabled',
    ),
    ));

    $this->addColumn('status', array(
    'header'    => Mage::helper('productlabel')->__('Status'),
    'align'     => 'left',
    'width'     => '80px',
    'index'     => 'status',
    'type'      => 'options',
    'options'   => array(
    1 => 'Enabled',
    2 => 'Disabled',
    ),
    ));

    $this->addColumn('action',
    array(
    'header'    =>  Mage::helper('productlabel')->__('Action'),
    'width'     => '100',
    'type'      => 'action',
    'getter'    => 'getId',     
    'renderer'  => Magebuzz_Productlabel_Block_Adminhtml_Catalog_Product_Renderer_Action,
    'filter'    => false,
    'sortable'  => false,
    'index'     => 'stores',
    'is_system' => true,
    ));
    return parent::_prepareColumns();
  }



  public function getRowUrl($row) {
    return false;
  }

  public function getGridUrl()
  {
    return $this->getData('grid_url')
    ? $this->getData('grid_url')
    : $this->getUrl('*/*/labelGrid',array('id'=>$this->getRequest()->getParam('id')));
  }

  protected function _getSelectedCodes()
  {
    $codes = $this->getSelectedCodes();        
    return $codes;
  }

  /**
  * Retrieve related products
  *
  * @return array
  */
  public function getSelectedCodes()
  {
    $codeIds = array();
    $productId = $this->getRequest()->getParam('id');  
    $codeIds = Mage::getModel('productlabel/product')->getCollection()->addFieldToFilter('product_id',$productId)->getColumnValues('label_id')  ;
    return $codeIds;
  }
}