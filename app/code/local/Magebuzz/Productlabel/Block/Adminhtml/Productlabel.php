<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Block_Adminhtml_Productlabel extends Mage_Adminhtml_Block_Widget_Grid_Container {
  public function __construct() {
    $this->_controller = 'adminhtml_productlabel';
    $this->_blockGroup = 'productlabel';
    $this->_headerText = Mage::helper('productlabel')->__('Manage Product Label');
    $this->_addButtonLabel = Mage::helper('productlabel')->__('Add Label');
    parent::__construct();
  }
}