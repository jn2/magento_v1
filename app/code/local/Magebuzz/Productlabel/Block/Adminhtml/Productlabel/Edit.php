<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Block_Adminhtml_Productlabel_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {
  public function __construct() {
    parent::__construct();						 
    $this->_objectId = 'id';
    $this->_blockGroup = 'productlabel';
    $this->_controller = 'adminhtml_productlabel';
    $back = $this->getRequest()->getParam('product');    
    if($back !=''){
      $this->_removeButton('delete');
      $this->_removeButton('reset');
      $this->_removeButton('back');      

      $this->_addButton('back_button', array(
      'label'     => Mage::helper('adminhtml')->__('Back'),
      'onclick'   => 'setLocation(\''
      . $this->getUrl('*/*/', array('store'=>$this->getRequest()->getParam('store', 0))).'\')',
      'class'     => 'scalable back',
      ));

      $urlDelete = Mage::helper("adminhtml")->getUrl('adminhtml/productlabel/delete/',array('id'=>$this->getRequest()->getParam('id'),'product'=>$back));
      $this->_addButton('delete_button', array(
      'label'     => Mage::helper('adminhtml')->__('Delete'),
      'onclick'   => 'confirmSetLocation(\''
      . Mage::helper('productlabel')->__('Are you sure?').'\', \''.$urlDelete.'\')',                            
      'class'     => 'delete',
      ));        
    }else{
      $this->_updateButton('save', 'label', Mage::helper('productlabel')->__('Save Item'));
      $this->_updateButton('delete', 'label', Mage::helper('productlabel')->__('Delete Item'));

      $this->_addButton('saveandcontinue', array(
      'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
      'onclick'   => 'saveAndContinueEdit()',
      'class'     => 'save',
      ), -100);

      $this->_formScripts[] = "
      function toggleEditor() {
      if (tinyMCE.getInstanceById('productlabel_content') == null) {
      tinyMCE.execCommand('mceAddControl', false, 'productlabel_content');
      } else {
      tinyMCE.execCommand('mceRemoveControl', false, 'productlabel_content');
      }
      }

      function saveAndContinueEdit(){
      editForm.submit($('edit_form').action+'back/edit/');
      }
      ";
    }


  }

  public function getHeaderText() {
    if( Mage::registry('productlabel_data') && Mage::registry('productlabel_data')->getId() ) {
      return Mage::helper('productlabel')->__("Edit Label '%s'", $this->htmlEscape(Mage::registry('productlabel_data')->getTitle()));
    } else {
      return Mage::helper('productlabel')->__('Add Label');
    }
  }
}