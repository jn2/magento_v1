<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Block_Adminhtml_Productlabel_Renderer_Catalogimage
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action {
  // return renderer html label of category
  public function render(Varien_Object $row) {
    $contentHtml = '';
    $rowData = $row->getData();    
    if ( count($rowData)>0) {
      $contentHtml = $this->getLayout()->createBlock('core/template')->setTemplate('productlabel/preview/category.phtml')->setRow($row)->toHtml();      
    }		
    return $contentHtml;  
  }
}