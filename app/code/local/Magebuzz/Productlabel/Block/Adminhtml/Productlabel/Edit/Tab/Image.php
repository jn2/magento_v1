<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Block_Adminhtml_Productlabel_Edit_Tab_Image extends Mage_Adminhtml_Block_Widget_Form {
  protected function _prepareForm() {
    $form = new Varien_Data_Form();
    $this->setForm($form);
    $media_data = array(); 
    if (Mage::getSingleton('adminhtml/session')->getProductlabelData()) {
      $media_data =Mage::getSingleton('adminhtml/session')->getProductlabelData();
      Mage::getSingleton('adminhtml/session')->setProductlabelData(null);
    } elseif ( Mage::registry('productlabel_data') ) {
      $media_data = Mage::registry('productlabel_data')->getData();
    }

    if (isset($media_data['product_img']) && $media_data['product_img'] != '') {
      $media_data['product_img'] = 'productlabel/product_img/' . $media_data['product_img'];
    }
    if (isset($media_data['category_img']) && $media_data['category_img'] != '') {
      $media_data['category_img'] = 'productlabel/category_img/' . $media_data['category_img'];
    }

    $fieldsetCategory = $form->addFieldset('caretory_image_form', array('legend'=>Mage::helper('productlabel')->__('Category Page')));

    $fieldsetCategory->addField('category_txt', 'text', array(
			'label'     => Mage::helper('productlabel')->__('Text'),        
			'name'      => 'category_txt',    
			'required'  => false,
    ));

    $fieldsetCategory->addField('category_img', 'image', array(
			'label'     => Mage::helper('productlabel')->__('Image'),
			'required'  => false,      
			'name'      => 'category_img',
    )); 

    $position = Mage::getModel('productlabel/labelposition')->getOptionArray();

    $fieldsetCategory->addField('category_position', 'select', array(
			'label'     => Mage::helper('productlabel')->__('Position'),
			'name'      => 'category_position',
			'values'    =>$position
    ));

    $fieldsetCategory->addField('category_style', 'text', array(
			'label'     => Mage::helper('productlabel')->__('Text Style'),
			'name'      => 'category_style',
    ));  

    $fieldsetProduct = $form->addFieldset('product_image_form', array('legend'=>Mage::helper('productlabel')->__('Product Page')));

    $fieldsetProduct->addField('product_txt', 'text', array(
			'label'     => Mage::helper('productlabel')->__('Text'),    
			'required'  => false,
			'name'      => 'product_txt',
    ));

    $fieldsetProduct->addField('product_img', 'image', array(
			'label'     => Mage::helper('productlabel')->__('Image'),
			'required'  => false,      
			'name'      => 'product_img',
    )); 

    $fieldsetProduct->addField('product_position', 'select', array(
			'label'     => Mage::helper('productlabel')->__('Position'),
			'name'      => 'product_position',
			'values'    =>$position
    ));

    $fieldsetProduct->addField('product_style', 'text', array(
			'label'     => Mage::helper('productlabel')->__('Text Style'),
			'name'      => 'product_style',
    ));  

    $form->setValues($media_data);
    return parent::_prepareForm();
  }
}
