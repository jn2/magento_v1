<?php
/*
* Copyright (c) 2014 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Block_Adminhtml_Productlabel_Renderer_Productimage
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
  // return renderer html label of product
  public function render(Varien_Object $row)
  {    
    $contentHtml = '';
    $rowData = $row->getData();    
    if( count($rowData)>0){      
      $contentHtml = $this->getLayout()->createBlock('core/template')->setTemplate('productlabel/preview/product.phtml')->setRow($row)->toHtml();      
    }
    return $contentHtml;  
  }

}
