<?php
/*
* Copyright (c) 2015 www.magebuzz.com
*/
class Magebuzz_Productlabel_Block_Adminhtml_Productlabel_Edit_Tab_Conditions extends Mage_Adminhtml_Block_Widget_Form {
  protected function _prepareForm() {
		$data = array();
		if (Mage::registry('productlabel_data')) {
			$data = Mage::registry('productlabel_data')->getData(); 
		}		
         
    $form = new Varien_Data_Form();   
		
		$fieldset = $form->addFieldset('conditions_fieldset', 
			array('legend' => Mage::helper('productlabel')->__('Individual Products'))
		);
		
		$fieldset->addField('sku_filter_type', 'select', array(
			'label'     => Mage::helper('productlabel')->__('Apply label to'),
			'name'      => 'sku_filter_type',
			'values'    => array(
				0 => Mage::helper('productlabel')->__('SKUs listed below only'),  
				1 => Mage::helper('productlabel')->__('All matching products except SKUs listed below'),                 
			 ),              
		)); 
		
		$fieldset->addField('product_sku', 'text', array(
			'label' => 'Product Sku',
			'name' => 'product_sku',
			'required' => false,
			'class' => 'rule_conditions_fieldset',
			//'readonly' => true,
		//	'onclick' => $this->getProductChooserURL(),
			'after_element_html' => $this->_getTriggerChooserButtonHtml(),
		));		
		
		$fieldset->addFieldset('product_chooser', array(
			'legend' => (''),			
			'class' => 'label-sku-chooser'
		));
		
		$fldCat = $form->addFieldset('cat', array('legend'=> Mage::helper('productlabel')->__('Categories')));
		$fldCat->addField('categories', 'multiselect', array(
			'label'     => Mage::helper('productlabel')->__('Categories'),
			'name'      => 'category[]',
			'values'    => $this->getTree(),
		));
		
		$fldState = $form->addFieldset('state', array('legend'=> Mage::helper('productlabel')->__('State')));
		$fldState->addField('is_new', 'select', array(
			'label'     => Mage::helper('productlabel')->__('Is New'),
			'name'      => 'is_new',
			'values'    => array(
				0 => Mage::helper('productlabel')->__('Does not matter'), 
				1 => Mage::helper('productlabel')->__('No'), 
				2 => Mage::helper('productlabel')->__('Yes'), 
			),
		));
		
		$isSale = $fldState->addField('is_sale', 'select', array(
			'label' => Mage::helper('productlabel')->__('Is on Sale'),
			'name' => 'is_sale',
			'values' => array(
					0 => Mage::helper('productlabel')->__('Does not matter'), 
					1 => Mage::helper('productlabel')->__('No'), 
					2 => Mage::helper('productlabel')->__('Yes'), 
			),
			'note' => Mage::helper('productlabel')->__('Show on products that have special price'),
		));
		
    $fieldsetStock = $form->addFieldset('check_stock_fieldset', array(
			'legend' => Mage::helper('productlabel')->__('Check Product Stock ')
			)
    );

    $fieldsetStock->addField('stock_status', 'select', array(
			'label'     => Mage::helper('productlabel')->__('Display for Products'),
			'name'      => 'stock_status',
			'values'    => Mage::helper('productlabel')->getListStock(),
    ));

    $form->setValues($data);
    $this->setForm($form);
    return $this;
  }
	
	public function getProductChooserURL() {
		return 'getProductChooser(\'' . Mage::getUrl(
			'adminhtml/promo_widget/chooser/attribute/sku/form/rule_conditions_fieldset', array('_secure' => Mage::app()->getStore()->isAdminUrlSecure())
		) . '?isAjax=true\'); return false;';
	}
	protected function _getTriggerChooserButtonHtml() {
		$html = '<a class="rule-chooser-trigger" onclick="'.$this->getProductChooserURL().'" href="javascript:void(0)"><img title="Open Chooser" class="v-middle rule-chooser-trigger" alt="" src="'.$this->getSkinUrl('images/rule_chooser_trigger.gif').'"></a>';
		
		//$html .= '<a class="rule-param-apply" onclick="hideProductChooser()" href="javascript:void(0)"><img title="Apply" alt="Apply" class="v-middle" src="'.$this->getSkinUrl('images/rule_component_apply.gif').'"></a>';
		
		//$html .= '<div id="product_chooser"></div>';
		return $html;
	}
	
	protected function getTree() {
		$rootId = Mage::app()->getStore(0)->getRootCategoryId();         
		$tree = array();
		
		$collection = Mage::getModel('catalog/category')
				->getCollection()->addNameToResult();
		
		$pos = array();
		foreach ($collection as $cat){
				$path = explode('/', $cat->getPath());
				if ((!$rootId || in_array($rootId, $path)) && $cat->getLevel()){
						$tree[$cat->getId()] = array(
								'label' => str_repeat('--', $cat->getLevel()) . $cat->getName(), 
								'value' => $cat->getId(),
								'path'  => $path,
						);
				}
				$pos[$cat->getId()] = $cat->getPosition();
		}
		
		foreach ($tree as $catId => $cat){
				$order = array();
				foreach ($cat['path'] as $id){
					if (isset($pos[$id])){
							$order[] = $pos[$id];
					}
				}
				$tree[$catId]['order'] = $order;
		}
		
		usort($tree, array($this, 'compare'));
		array_unshift($tree, array('value'=>'', 'label'=>''));
		
		return $tree;
	}
	
	public function compare($a, $b) {
		foreach ($a['path'] as $i => $id){
			if (!isset($b['path'][$i])){ 
				// B path is shorther then A, and values before were equal
				return 1;
			}
			if ($id != $b['path'][$i]){
				// compare category positions at the same level
				$p = isset($a['order'][$i]) ? $a['order'][$i] : 0;
				$p2 = isset($b['order'][$i]) ? $b['order'][$i] : 0;
				return ($p < $p2) ? -1 : 1;
			}
		}
		// B path is longer or equal then A, and values before were equal
		return ($a['value'] == $b['value']) ? 0 : -1;
	}
}