<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Block_Adminhtml_Productlabel_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {
  public function __construct() {
    parent::__construct();
    $this->setId('productlabel_tabs');
    $this->setDestElementId('edit_form');
    $this->setTitle(Mage::helper('productlabel')->__('Label Information'));
  }

  protected function _beforeToHtml() {
    // add tab general infor of label
    $this->addTab('form_section', array(
			'label'     => Mage::helper('productlabel')->__('General Information'),
			'title'     => Mage::helper('productlabel')->__('General Information'),
			'content'   => $this->getLayout()->createBlock('productlabel/adminhtml_productlabel_edit_tab_form')->toHtml(),
    ));
		
    // add tab Image of label     
    $this->addTab('image_section', array(
			'label'     => Mage::helper('productlabel')->__('Image Label'),
			'title'     => Mage::helper('productlabel')->__('Image Label'),
			'content'   => $this->getLayout()->createBlock('productlabel/adminhtml_productlabel_edit_tab_image')->toHtml(),
    )); 
		
    // add tab condition check product
    $data = Mage::registry('productlabel_data')->getData();
    //if (isset($data['is_single']) && $data['is_single'] != 1) {  
    if (!isset($data['is_single']) || $data['is_single'] != 1) {  
      $this->addTab('conditions_section', array(
				'label'     => Mage::helper('productlabel')->__('Conditions'),
				'title'     => Mage::helper('productlabel')->__('Conditions'),
				'content'   => $this->getLayout()->createBlock('productlabel/adminhtml_productlabel_edit_tab_conditions')->toHtml(),
      ));    
    }
    return parent::_beforeToHtml();
  }
}