<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Block_Adminhtml_Productlabel_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {
  protected function _prepareForm() {
    $form = new Varien_Data_Form();
    $this->setForm($form);

    $fieldset = $form->addFieldset('productlabel_form', array('legend'=>Mage::helper('productlabel')->__('General Information')));

    $fieldset->addField('title', 'text', array(
			'label'     => Mage::helper('productlabel')->__('Title'),
			'class'     => 'required-entry',
			'required'  => true,
			'name'      => 'title',
    ));

    $fieldset->addField('product_id', 'hidden', array(
			'name'      => 'product_id',
			'values'   => $this->getRequest()->getParam('product'),
    ));

    $fieldset->addField('priority', 'text', array(
			'label'     => Mage::helper('productlabel')->__('Priority'),
			'required'  => false,
			'name'      => 'priority',
			'after_element_html' => '<small id="comment-small" class="note">Use 0 to show label first.</small>' 
    ));

    $fieldset->addField('status', 'select', array (
			'label'     => Mage::helper('productlabel')->__('Status'),
			'name'      => 'status',
			'values'    => array (
				array(
					'value'     => Magebuzz_Productlabel_Model_Status::STATUS_ENABLED,
					'label'     => Mage::helper('productlabel')->__('Enabled'),
				),
				array(
					'value'     => Magebuzz_Productlabel_Model_Status::STATUS_DISABLED,
					'label'     => Mage::helper('productlabel')->__('Disabled'),
				),
			),
    ));

    $fieldset->addField('start_time', 'date', array(
			'name'   => 'start_time',
			'label'  => Mage::helper('productlabel')->__('Start Date'),
			'title'  => Mage::helper('productlabel')->__('Start Date'),
			'image'  => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
			'format'       => 'yyyy-MM-dd',
			'time' => true,
    ));
		
    $fieldset->addField('end_time', 'date', array(
			'name'   => 'end_time',
			'label'  => Mage::helper('productlabel')->__('End Date'),
			'title'  => Mage::helper('productlabel')->__('End Date'),
			'image'  => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
			'format'       => 'yyyy-MM-dd',
			'time' => true,
    ));  

    if (!Mage::app()->isSingleStoreMode()) {
      $fieldset->addField('store_id', 'multiselect', array(
				'name'      => 'store_id[]',
				'label'     => Mage::helper('productlabel')->__('Store View'),
				'title'     => Mage::helper('productlabel')->__('Store View'),
				'required'  => true,
				'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
      ));
    }
    else {
      $fieldset->addField('store_id', 'hidden', array(
      'name'      => 'store_id[]',
      'value'     => Mage::app()->getStore(true)->getId()
      ));      
      Mage::registry('productlabel_data')->setStoreId(Mage::app()->getStore(true)->getId());
    }
    $customerGroups = Mage::getResourceModel('customer/group_collection')->load()->toOptionArray();
    $found = false;

    foreach ($customerGroups as $group) {
      if ($group['value']==0) {
        $found = true;
      }
    }
    if (!$found) {
      array_unshift($customerGroups, array(
      'value' => 0,
      'label' => Mage::helper('productlabel')->__('NOT LOGGED IN'))
      );
    }

    $fieldset->addField('customer_group', 'multiselect', array(
			'name'      => 'customer_group_ids[]',
			'label'     => Mage::helper('productlabel')->__('Customer Groups'),
			'title'     => Mage::helper('productlabel')->__('Customer Groups'),
			'required'  => true,
			'values'    => Mage::getResourceModel('customer/group_collection')->toOptionArray(),    
    ));    

    if (Mage::getSingleton('adminhtml/session')->getProductlabelData()) {
      $dataForm = Mage::getSingleton('adminhtml/session')->getProductlabelData();
      Mage::getSingleton('adminhtml/session')->setProductlabelData(null);
    } elseif ( Mage::registry('productlabel_data') ) {
      $dataForm =Mage::registry('productlabel_data')->getData();
    } 
    $productId = $this->getRequest()->getParam('product');    
    if( $productId !=""){
      $dataForm['product_id'] = $productId;
    }
    $form->setValues($dataForm);
    return parent::_prepareForm();
  }
}
