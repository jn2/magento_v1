<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Helper_Data extends Mage_Core_Helper_Abstract {
  protected $_labels = null; 
  const PRODUCT_INSTOCK = 1;       // product is in stock
  const PRODUCT_OUTSTOCK= 0;      // product is out stock
  const PRODUCT_BOTH = 2;         // product is in stock or out stock

  public function getLabels($product, $mode = 'category') {
    $html = ''; 
    
		$applied = false;
		foreach ($this->_getCollection() as $label) {
			if ($label->getIsSingle() && $applied) {
				continue;
			}
			$label->init($product, $mode);
			if ($label->isApplicable()) {
				$html .= $this->_generateHtml($label, $mode);
				$applied = true;
			}			
		}
    
    return $html;
  }

  public function getUsedProducts($product) {
    if ($product->isConfigurable()) {
      return $product->getTypeInstance(true)->getUsedProducts(null, $product);
    } else { 
      return $product->getTypeInstance(true)->getAssociatedProducts($product);
    }
  }

  /*
  * function Generate Label Html
  * Input :  - Label : label data , Mode : show in category or product , Type : normal Label or Label of product
  * Output : Html of Label
  */
  protected function _generateHtml($label, $mode='category', $type='normal') { 
    $html = '';
    $imgTxt = $mode.'_img';
    $txt = $mode.'_txt';
    $postionTxt = $mode.'_position';
    $customStyleTxt = $mode.'_style';
    $imgName = $label->getData($imgTxt);    
    $classTypeName = $type.'_type_label';
    $customStyle = $label->getData($customStyleTxt);
    if ($customStyle) {
      $divStyle .= $customStyle;
    }      
    $classStyle = $this->getCssByPosition($label->getData($postionTxt));
    $priority = $label->getPriority();
    $imagePath = $this->getImagePath($label->getData($imgTxt),$mode);
    $size = getimagesize($imagePath);
    if ($label->getData($txt)!='' || $label->getData($imgTxt)!=='') {
      $html = '<table id="productlabel_'.$label->getData('label_id').'" style="width:'.$size[0].'px; height:'.$size[1].'px" class="'.$classStyle.' '.$classTypeName.'">';  
      $html .= '<tbody>';
      $html .= '<tr>';
      if($label->getData($imgTxt)!==''){
        $html .= '<td style="background:url('.$this->getImagePath($label->getData($imgTxt),$mode).')">';
      }
      $html .= '<label style="'.$customStyle.'">'.$label->getData($txt).'</label>';
      $html .= '</td>';
      $html .= '</tr>';
      $html .= '</tbody>';
      $html .= '</table>';
    }

    return $html;        
  }
	
  public function getImagePath($imageName,$mode) {
    $folderType = $mode.'_img';        
    return $imgPath = Mage::getBaseUrl('media')."productlabel/".$folderType."/".$imageName;  
  }
	
  protected function getImage($imgName,$mode) {
    $folderType = $mode.'_img'; 
    $imgPath = Mage::getBaseDir('media') . '/productlabel/'.$folderType.'/'.$imageName;    
    $info = getimagesize($imgPath);
    return array('w'=>$info[0], 'h'=>$info[1]);
  }  

  public function getCssByPosition($position) {
    $styleClass = '';

    switch ($position) {
      case Magebuzz_Productlabel_Model_Labelposition::TOP_LEFT:
        $styleClass = 'label_top_left'; 
        break;
      case Magebuzz_Productlabel_Model_Labelposition::TOP_CENTER:
        $styleClass = 'label_top_center';
        break;
      case Magebuzz_Productlabel_Model_Labelposition::TOP_RIGHT:
        $styleClass = 'label_top_right';
        break;
      case Magebuzz_Productlabel_Model_Labelposition::MIDDLE_LEFT:
        $styleClass = 'label_middle_left';
        break;
      case Magebuzz_Productlabel_Model_Labelposition::MIDDLE_RIGHT:
        $styleClass = 'label_middle_right';
        break;
      case Magebuzz_Productlabel_Model_Labelposition::MIDDLE_CENTER:
        $styleClass = 'label_middle_center';
        break;
      case Magebuzz_Productlabel_Model_Labelposition::BOTTOM_LEFT:
        $styleClass = 'label_bottom_left';
        break;
      case Magebuzz_Productlabel_Model_Labelposition::BOTTOM_CENTER:
        $styleClass = 'label_bottom_center';
        break;
      case Magebuzz_Productlabel_Model_Labelposition::BOTTOM_RIGHT:
        $styleClass = 'label_bottom_right';
        break;
      default :
        return 'label_top_right'  ;
    }

    return $styleClass;
  }  

  protected function _getCollection() {
		$storeId = Mage::app()->getStore()->getId();
    $collection = Mage::getModel('productlabel/productlabel')->getCollection()
			->addFieldToFilter('status', Magebuzz_Productlabel_Model_Status::STATUS_ENABLED);
		$collection->getSelect()->where("stores like '%$storeId%' or stores  = '0'")
			->order('priority', 'ASC');
    return $collection; 
  }

  public function getLabelByProduct($productId){
    return Mage::getModel('productlabel/product')->getCollection()
			->addFieldToFilter('product_id',$productId)->getColumnValues('label_id');
  }

  public function getListStock(){
    return array(
    array(
    'value'     => self::PRODUCT_INSTOCK,
    'label'     => Mage::helper('productlabel')->__('In stock'),
    ),
    array(
    'value'     => self::PRODUCT_OUTSTOCK,
    'label'     => Mage::helper('productlabel')->__('Out of stock'),
    ),
    array(
    'value'     => self::PRODUCT_BOTH,
    'label'     => Mage::helper('productlabel')->__('In stock Vs Out of stock'),
    ),
    ) ;  
  } 
	
	public function getIsNewSetting() {
		$storeId = Mage::app()->getStore()->getId();
		return (bool) Mage::getStoreConfig('productlabel/new/is_new', $storeId);
	}
	
	public function canUseCreatedDate() {
		$storeId = Mage::app()->getStore()->getId();
		return (bool) Mage::getStoreConfig('productlabel/new/creation_date', $storeId);
	}
	
	public function getCreatedDateRange() {
		$storeId = Mage::app()->getStore()->getId();
		return (int) Mage::getStoreConfig('productlabel/new/days', $storeId);
	}
	
}
