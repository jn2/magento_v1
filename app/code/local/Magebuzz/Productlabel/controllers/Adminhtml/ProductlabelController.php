<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Adminhtml_ProductlabelController extends Mage_Adminhtml_Controller_Action {
  protected function _initAction() {
    $this->loadLayout()
    ->_setActiveMenu('productlabel/items')
    ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));

    return $this;
  }   

  public function indexAction() {
    $this->_initAction()  ;
    $this->renderLayout();
  }

  public function editAction() {
    $id     = $this->getRequest()->getParam('id');
    $model  = Mage::getModel('productlabel/productlabel')->load($id);
  
    if ($model->getId() || $id == 0) {
      $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
      if (!empty($data)) {
        $model->setData($data);
      }

      Mage::register('productlabel_data', $model);

      $this->loadLayout();
      $this->_setActiveMenu('productlabel/items');

      $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
      $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

      $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

      $this->_addContent($this->getLayout()->createBlock('productlabel/adminhtml_productlabel_edit'))
      ->_addLeft($this->getLayout()->createBlock('productlabel/adminhtml_productlabel_edit_tabs'));

      $this->renderLayout();
    } else {
      Mage::getSingleton('adminhtml/session')->addError(Mage::helper('productlabel')->__('Item does not exist'));
      $this->_redirect('*/*/');
    }
  }

  public function newAction() {
    $this->_forward('edit');
  }

  public function saveAction() {
    if ($data = $this->getRequest()->getPost()) {
      $model = Mage::getModel('productlabel/rule'); 
      if ($id = $this->getRequest()->getParam('id')) {
        $model->load($id);
      }         
      $customerGroupId = implode($data['customer_group_ids'], ','); 	
      $data['customer_group'] = $customerGroupId;    
      $imageType = array('category','product')  ;
      try {	
        /* Starting upload */	
        foreach ($imageType as $image) {
          $fileName = $image.'_img';
          if (isset($data[$fileName]['delete']) && $data[$fileName]['delete'] == 1) {
            $data[$fileName] = '';
          }
          elseif (isset($_FILES[$fileName]['name']) && $_FILES[$fileName]['name'] != '') {
            $uploader = new Varien_File_Uploader($fileName);
            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
            $uploader->setAllowRenameFiles(false);      
            $uploader->setFilesDispersion(false);
            // We set media as the upload dir
            $path = Mage::getBaseDir('media') . DS . 'productlabel' . DS.$fileName.DS; 
            $uploader->save($path, $_FILES[$fileName]['name'] );
            $data[$fileName] = $uploader->getUploadedFileName();      
          } else {
            $data[$fileName] = $model->getData($fileName);        
          }     
        }     
      } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        return;
      }     

			$newStores = (array)$data['store_id'];
			if (in_array(0, $newStores)) {
				$stores = '0';
			}
			else {
				$stores = implode($newStores, ',');
			}			
			$data['stores'] = $stores;
			
			$category = (array)$data['category'];
			$categories = implode($category, ',');
			$data['categories'] = $categories;
				
      $model->setData($data);        
      //$model->loadPost($data); 
      $model->setId($this->getRequest()->getParam('id'));
      try {    								
        $model->save();
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('productlabel')->__('Item was successfully saved'));
        Mage::getSingleton('adminhtml/session')->setFormData(false);

        if ($this->getRequest()->getParam('back')=='edit') {
          $this->_redirect('*/*/edit', array('id' => $model->getId()));
          return;
        }
        if(isset($data['product_id']) && $data['product_id'] !=''){          
          $this->_redirect('adminhtml/catalog_product/edit', array('id'=>$data['product_id'],'tab'=>'product_info_tabs_product_label'));        
          return; 
        }

        $this->_redirect('*/*/');
        return;
      } catch (Exception $e) {        
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        return;
      }
    }
    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('productlabel')->__('Unable to find item to save'));
    $this->_redirect('*/*/');
  }

  public function deleteAction() {
    if ($this->getRequest()->getParam('id') > 0 ) {
      try {
        $model = Mage::getModel('productlabel/productlabel');

        $model->setId($this->getRequest()->getParam('id'))
        ->delete();

        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));

        $productId = $this->getRequest()->getParam('product'); 
        if(isset($productId) && $productId !=''){          
          $this->_redirect('adminhtml/catalog_product/edit', array('id'=>$productId,'tab'=>'product_info_tabs_product_label'));        
          return; 
        }

        $this->_redirect('*/*/');
      } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
      }
    }
    $this->_redirect('*/*/');
  }

  public function massDeleteAction() {
    $productlabelIds = $this->getRequest()->getParam('productlabel');
    if(!is_array($productlabelIds)) {
      Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
    } else {
      try {
        foreach ($productlabelIds as $productlabelId) {
          $productlabel = Mage::getModel('productlabel/productlabel')->load($productlabelId);
          $productlabel->delete();
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(
        Mage::helper('adminhtml')->__(
        'Total of %d record(s) were successfully deleted', count($productlabelIds)
        )
        );
      } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
      }
    }
    $this->_redirect('*/*/index');
  }

  public function massStatusAction() {
    $productlabelIds = $this->getRequest()->getParam('productlabel');
    if(!is_array($productlabelIds)) {
      Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
    } else {
      try {
        foreach ($productlabelIds as $productlabelId) {
          $productlabel = Mage::getSingleton('productlabel/productlabel')
          ->load($productlabelId)
          ->setStatus($this->getRequest()->getParam('status'))
          ->setIsMassupdate(true)
          ->save();
        }
        $this->_getSession()->addSuccess(
        $this->__('Total of %d record(s) were successfully updated', count($productlabelIds))
        );
      } catch (Exception $e) {
        $this->_getSession()->addError($e->getMessage());
      }
    }
    $this->_redirect('*/*/index');
  }

  public function customlabelAction(){
    $this->loadLayout() ;
    $this->renderLayout();
  }

  public function labelGridAction() {
    $this->loadLayout();
    $this->getLayout()->getBlock('product.edit.productlabel')
    ->setProductLabel($this->getRequest()->getPost('oproduct', null));
    $this->renderLayout();
  }

  public function newConditionHtmlAction()
  {
    $id = $this->getRequest()->getParam('id');
    $typeArr = explode('|', str_replace('-', '/', $this->getRequest()->getParam('type')));
    $type = $typeArr[0];

    $model = Mage::getModel($type)
    ->setId($id)
    ->setType($type)
    ->setRule(Mage::getModel('catalogrule/rule'))
    ->setPrefix('conditions');
    if (!empty($typeArr[1])) {
      $model->setAttribute($typeArr[1]);
    }

    if ($model instanceof Mage_Rule_Model_Condition_Abstract) {
      $model->setJsFormObject($this->getRequest()->getParam('form'));
      $html = $model->asHtmlRecursive();
    } else {
      $html = '';
    }
    $this->getResponse()->setBody($html);
  }
	
	protected function _isAllowed()	{
		return Mage::getSingleton('admin/session')->isAllowed('productlabel/manage_productlabel');
	}
}