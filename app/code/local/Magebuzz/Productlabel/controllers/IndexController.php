<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_IndexController extends Mage_Core_Controller_Front_Action {     
  public function uploadimagecategoryAction() {
    try {
      $file = $_FILES['category_img'];
      $image_name = $_FILES['category_img']['name'];    
      $uploader = new Varien_File_Uploader('category_img');
      $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
      $uploader->setAllowRenameFiles(true);                    
      $uploader->setFilesDispersion(false);
      $path = Mage::getBaseDir('media') . DS . 'productlabel' . DS . 'category_img';
      if (!is_dir($path)) {
        mkdir($path, 0777, true);
      }        
      $rs = $uploader->save($path, $_FILES['category_img']['name']);        
      $new_image_name = $uploader->getUploadedFileName();            

      $location = $path. DS . $new_image_name;
      //check if valid image size
      $imageObj = new Varien_Image($location);
      $width = $imageObj->getOriginalWidth();
      $height = $imageObj->getOriginalHeight();            
      $result = array(
      'location' => $location,
      'imageUrl' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'productlabel/category_img/'.$new_image_name,
      'imageName' => $new_image_name
      );      
    }
    catch (Exception $e) {
      $result = array(
      'error' => true,
      'message' => $e->getMessage()
      );
    }
    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
  } 
  
  public function uploadimageproductAction() {
    try {
      $file = $_FILES['product_img'];
      $image_name = $_FILES['product_img']['name'];    
      $uploader = new Varien_File_Uploader('product_img');
      $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
      $uploader->setAllowRenameFiles(true);                    
      $uploader->setFilesDispersion(false);
      $path = Mage::getBaseDir('media') . DS . 'productlabel' . DS . 'product_img';
      if (!is_dir($path)) {
        mkdir($path, 0777, true);
      }        
      $rs = $uploader->save($path, $_FILES['product_img']['name']);        
      $new_image_name = $uploader->getUploadedFileName();            

      $location = $path. DS . $new_image_name;
      //check if valid image size
      $imageObj = new Varien_Image($location);
      $width = $imageObj->getOriginalWidth();
      $height = $imageObj->getOriginalHeight();            
      $result = array(
      'location' => $location,
      'imageUrl' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'productlabel/product_img/'.$new_image_name,
      'imageName' => $new_image_name
      );      
    }
    catch (Exception $e) {
      $result = array(
      'error' => true,
      'message' => $e->getMessage()
      );
    }
    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
  }
	
	public function testAction() {
		
	}
}