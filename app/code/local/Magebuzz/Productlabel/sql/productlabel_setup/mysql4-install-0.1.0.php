<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('productlabel')};
 CREATE TABLE `{$this->getTable('productlabel')}` (
  `label_id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `customer_group` varchar(255) NOT NULL,  
  `product_txt` varchar(255) NOT NULL,
  `product_img` varchar(255) NOT NULL,
  `product_position` int(11) NOT NULL,
  `product_style` varchar(255) NOT NULL,
  `category_txt` varchar(255) NOT NULL,
  `category_img` varchar(255) NOT NULL,
  `category_position` int(11) NOT NULL,
  `category_style` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `conditions_serialized` text NULL,  
  `start_time` datetime NULL,
  `end_time` datetime NULL,
  `stock_status` smallint(6) NOT NULL default '0',
  `status` smallint(6) NOT NULL default '0',
  `is_single` smallint(6) NOT NULL default '0',
  PRIMARY KEY  (`label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('productlabel_product')};
 CREATE TABLE `{$this->getTable('productlabel_product')}` (
  `label_product_id` int(11) NOT NULL auto_increment,
  `product_id` int(10) UNSIGNED NOT NULL,
  `label_id` int(11) NOT NULL,
  PRIMARY KEY  (`label_product_id`),
  CONSTRAINT `FK_LABEL_REFERENCES_LABEL` FOREIGN KEY (`label_id`) REFERENCES `{$this->getTable('productlabel')}` (`label_id`) ON UPDATE CASCADE ON DELETE CASCADE,   
  CONSTRAINT `FK_LABEL_REFERENCES_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `{$this->getTable('catalog/product')}` (`entity_id`) ON UPDATE CASCADE ON DELETE CASCADE  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('productlabel_store')};
  CREATE TABLE `{$this->getTable('productlabel_store')}` (    
    `label_store_id` int(11) NOT NULL auto_increment, 
    `label_id` int(11) NOT NULL,
    `store_id` smallint(5) UNSIGNED NOT NULL,
    PRIMARY KEY  (`label_store_id`),  
    CONSTRAINT `FK_LABEL_REFERENCES_STORE` FOREIGN KEY (`label_id`) REFERENCES `{$this->getTable('productlabel')}` (`label_id`) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT `FK_LABEL_STORE_STORE` FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core/store')}` (`store_id`) ON UPDATE CASCADE ON DELETE CASCADE
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup(); 