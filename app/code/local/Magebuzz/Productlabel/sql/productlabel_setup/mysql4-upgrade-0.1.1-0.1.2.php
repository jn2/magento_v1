<?php
$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE `{$this->getTable('productlabel')}` CHANGE `start_time` `start_time` DATE NULL DEFAULT NULL;
	ALTER TABLE `{$this->getTable('productlabel')}` CHANGE `end_time` `end_time` DATE NULL DEFAULT NULL;
	ALTER TABLE `{$this->getTable('productlabel')}` ADD `stores` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
	ALTER TABLE `{$this->getTable('productlabel')}` ADD `product_sku` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
	ALTER TABLE `{$this->getTable('productlabel')}` ADD `sku_filter_type` SMALLINT(6) NOT NULL DEFAULT '0';
	ALTER TABLE `{$this->getTable('productlabel')}` ADD `categories` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
	ALTER TABLE `{$this->getTable('productlabel')}` DROP `conditions_serialized`;
	
	DROP TABLE IF EXISTS {$this->getTable('productlabel_product')};	
	DROP TABLE IF EXISTS {$this->getTable('productlabel_store')};
");

$installer->endSetup(); 