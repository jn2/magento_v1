<?php
$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE `{$this->getTable('productlabel')}` ADD `is_new` SMALLINT(6) NOT NULL DEFAULT '0';
	ALTER TABLE `{$this->getTable('productlabel')}` ADD `is_sale` SMALLINT(6) NOT NULL DEFAULT '0';
");

$installer->endSetup(); 