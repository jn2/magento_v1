<?php
/*
* Copyright (c) 2014 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Model_Rule extends Magebuzz_Productlabel_Model_Rule_Rule {
  protected $_eventPrefix = 'label_rule'; 
  public function _construct() {
    parent::_construct();
    $this->_init('productlabel/productlabel');
  }
}