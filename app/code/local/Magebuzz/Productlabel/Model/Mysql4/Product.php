<?php
/*
* Copyright (c) 2014 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Model_Mysql4_Product extends Mage_Core_Model_Mysql4_Abstract {
	public function _construct() {    
		// Note that the productlabel_id refers to the key field in your database table.
		$this->_init('productlabel/product', 'label_product_id');
	}
}