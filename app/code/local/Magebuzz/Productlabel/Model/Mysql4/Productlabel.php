<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Model_Mysql4_Productlabel extends Mage_Core_Model_Mysql4_Abstract {
  public function _construct() {
    $this->_init('productlabel/productlabel', 'label_id');
  }

  // protected function _afterSave(Mage_Core_Model_Abstract $object) {    
    // //$oldStores = $this->lookupStoreIds($object->getId());
    // $newStores = (array)$object->getStoreId();
		// $stores = implode($newStores, ',');
		// echo $stores;
		// //Zend_Debug::dump($newStores);
		// die('ab');
    // if (empty($newStores)) {
      // $newStores = (array)$object->getStoreId();
    // }
    // $table  = $this->getTable('productlabel/labelstore');
    // $insert = array_diff($newStores, $oldStores);
    // $delete = array_diff($oldStores, $newStores);
    // if ($delete) {
      // $where = array(
      // 'label_id = ?'     => (int) $object->getId(),
      // 'store_id IN (?)' => $delete
      // );
      // $this->_getWriteAdapter()->delete($table, $where);
    // }

    // if ($insert) {
      // $data = array();
      // foreach ($insert as $storeId) {
        // $data[] = array(
        // 'label_id'  => (int) $object->getId(),
        // 'store_id' => (int) $storeId
        // );
      // }
     
      // $this->_getWriteAdapter()->insertMultiple($table, $data);
    // }
    // return parent::_afterSave($object);
  // }
  
  protected function _afterLoad(Mage_Core_Model_Abstract $object) {	
		$storeIds = explode(',', $object->getStores());
		$object->setData('store_id', $storeIds);		
		
		$customerGroupIds = $object->getCustomerGroup();
		//$object->setData('')
    return parent::_afterLoad($object);
  }

  public function lookupStoreIds($labelId) {
		return array();
    $adapter = $this->_getReadAdapter();
    $select  = $adapter->select()
    ->from($this->getTable('productlabel/labelstore'), 'store_id')
    ->where('label_id = ?',(int)$labelId);

    return $adapter->fetchCol($select);
  }
}