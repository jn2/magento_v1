<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Model_Observer {
  public function addProductLabelToProduct($observer){
    $block = $observer->getEvent()->getBlock();        
    if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs)
    {
      if ($this->_getRequest()->getActionName() == 'edit' || $this->_getRequest()->getParam('type'))
      {
        $block->addTab('product_label', array(
        'label' => Mage::helper('productlabel')->__('Product Label'),
        'title' => Mage::helper('productlabel')->__('Product Label'), 
        'content' => $block->getLayout()->createBlock('productlabel/adminhtml_catalog_product_label')->toHtml(), 
        // 'url'   => $block->getUrl('productlabel/adminhtml_productlabel/customlabel', array('_current' => true)),    
        //'class' =>'ajax',     
        ));          
      }
    }    
  }
  protected function _getRequest()
  {
    return Mage::app()->getRequest();
  }

  public function addProductLabel($observer) 
  {
    $product = $observer->getEvent()->getProduct();    
    $productId= $product->getEntityId();
    $request = $this->_getRequest()->getParams();
    $labelParam = $request['label'];     
    $newLabel = array();
    if(isset($request['is_show_custom_label']) && $request['is_show_custom_label'] !=null){
      if($request['is_show_custom_label'] ==1){
        if($labelParam['category_txt'] == '' && $labelParam['category_img'] == '' && $labelParam['product_txt'] == '' && $labelParam['product_img'] == ''){
          Mage::getSingleton('adminhtml/session')->addError(Mage::helper('productlabel')->__('Add custom label error,please input image labels and text labels'));   
        }else{
          $labelModel = Mage::getModel('productlabel/productlabel')   ;        
          $labelModel->setCategoryTxt($labelParam['category_txt']);
          $labelModel->setCategoryImg($labelParam['category_img']);
          $labelModel->setCategoryPosition($labelParam['category_postion']);
          $labelModel->setCategoryStyle($labelParam['category_style']);
          $labelModel->setProductTxt($labelParam['product_txt']);
          $labelModel->setProductImg($labelParam['product_img']);
          $labelModel->setProductPosition($labelParam['product_position']);
          $labelModel->setProductStyle($labelParam['product_style']);
          $labelModel->setStatus(Magebuzz_Productlabel_Model_Status::STATUS_ENABLED);
          $labelModel->setStockStatus(Magebuzz_Productlabel_Helper_Data::PRODUCT_BOTH);
          // set all store view
          $store = array(Mage_Core_Model_App::ADMIN_STORE_ID);          
          $labelModel->setStoreId($store);
          // set All Customer group
          $customerGroup = Mage::getModel('customer/group')->getCollection()->getColumnValues('customer_group_id');                
          $groupSerialize = serialize($customerGroup);      
          $labelModel->setCustomerGroup($groupSerialize); 
          // set model is label of product
          $labelModel->setIsSingle(1);
          try{
            $labelModel->save();
            $newLabel[]= $labelModel->getLabelId();          
          }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('productlabel')->__('Unable to find item to save'));
          } 
        }       
      }
    }

    if($request['label_id'] == ''){
      $listLabel = array();  
    }else{
      $listLabel = $request['label_id'];
    }                         
    if(count($newLabel)>0){
      $listLabel = array_merge($listLabel,$newLabel);
    }
    // save label by product   
    $productLabel = Mage::getModel('productlabel/product')->updateListLabel($listLabel,$productId);    
  }
	
	public function onCoreBlockAbstractToHtmlAfter(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        if ($block instanceof Mage_Catalog_Block_Product_Price) {
            $id = $block->getProduct()->getId();
            if (!Mage::registry('amlabel_product_id_' . $id)) {
                // add product ID info in output
                Mage::register('amlabel_product_id_' . $id, true, true);
                $html = $html = $observer->getTransport()->getHtml();
                $observer->getTransport()->setHtml('<div class="price" id="amlabel-product-price-' . $id . '" style="display:none"></div>' . $html);
                // add label for product
                $product = $block->getProduct();
                /*
                 * old method was:
                 *  $type    = strpos($block->getModuleName(), 'Catalog') !== false ? 'category' : 'product';
                 *
                 * new method:
                 */
                $controller = Mage::app()->getRequest()->getControllerName();
                $action     = Mage::app()->getRequest()->getActionName();
                $type       = ($controller == 'product' && $action == 'view') ? 'product' : 'category';

                $label   = 'ssssssssssssssssss';
                if ($label) {
                    $this->addScript($id, addslashes($label));
                }
            }
        }

        return $this;
    }
}