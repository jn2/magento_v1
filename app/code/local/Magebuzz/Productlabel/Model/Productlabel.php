<?php
/*
* Copyright (c) 2015 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Model_Productlabel extends Mage_Core_Model_Abstract {
	public function _construct() {
		parent::_construct();
		$this->_init('productlabel/productlabel','label_id');
	}
	
	public function init($product, $mode=null, $parent = null) {
		$this->setProduct($product);
		
		$regularPrice = $product->getPrice();
		$specialPrice = 0;
		if ($this->getIsSale()) {
			$now = Mage::getModel('core/date')->date('Y-m-d 00:00:00');
			if ($product->getSpecialFromDate() && $now >= $product->getSpecialFromDate()) {
				$specialPrice = $product->getData('special_price');
				if ($product->getSpecialToDate() && $now > $product->getSpecialToDate()) {
					$specialPrice = 0;
				}
			}
		}
		@$this->_info['price']         = $regularPrice;
        @$this->_info['special_price'] = $specialPrice;
	}
	
	/*
	* function to check if a label can apply for current product
	*/
	public function isApplicable() {
		$product = $this->getProduct();
                $product = Mage::getModel('catalog/product')->load($product->getId());
		if (!$product) {
			return false;
		}
				
		// check valid date
		if ($this->getStartTime() != '' && $this->getEndTime() != '') {
			$now = Mage::getModel('core/date')->date('Y-m-d');
			if (($now < $this->getStartTime() || $now > $this->getEndTime())) {
				return false;
			}
		} 
		
		//check customer group 
		$customerGroups = $this->getCustomerGroup();
		if ($customerGroups != '') {
			$customerGroupIds = explode(',', $customerGroups);
			$currentGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
			if (!in_array($currentGroupId, $customerGroupIds)) {
				return false;
			}
		}
		
		// check by product skus 		
		$inArray = false;				
		$productSkus = array();
		if ($this->getProductSku() != '') {
			//Function entered to clear white space when selecting more than one product on product label manager
			$productSkus = explode(',', preg_replace('/[\s]/i','',$this->getProductSku()));
		}		

		if (in_array($product->getSku(), $productSkus)) {
			$inArray = true;
		}		
		//display only listed skus
		if ($this->getSkuFilterType() == 0 && !empty($productSkus)) {
			return $inArray; 
		}
		//exclude skus
		if ($this->getSkuFilterType() == 1 && $inArray) {
			return false; 
		}
		
		//check by product status
		$stockStatus = $this->getStockStatus();
		if ($stockStatus != 2) {			
			$inStock = $product->getStockItem()->getIsInStock() ? 1 : 0;
			if ($inStock != $stockStatus)
				return false;
		}
			
		//categories 
		$catIds = $this->getCategories();
		if ($catIds) {
			$ids = $product->getCategoryIds();
			if (!is_array($ids))
				return false;
			$found = false;
			foreach (explode(',', $catIds) as $catId) {
				if (in_array($catId, $ids))
					$found = true;
			}
			if (!$found)
				return false;
		}
		
		if ($this->getIsNew()){
			$isNew = $this->_isNew($product) ? 2 : 1;
			if ($this->getIsNew() != $isNew)
				return false;
		}

		if ($this->getIsSale()){
			$isSale = $this->_isSale($product) ? 2 : 1;
			if ($this->getIsSale() != $isSale)
				return false;
		}
		
		return true;		
	}
	
	protected function _isNew($product) {
		$fromDate = '';
		$toDate   = '';
		$helper = Mage::helper('productlabel');
		if ($helper->getIsNewSetting()) {
			$fromDate = $product->getNewsFromDate();
			$toDate   = $product->getNewsToDate();
		}

		if (!$fromDate && !$toDate) {
			if ($helper->canUseCreatedDate()) {
				$dateRange = $helper->getCreatedDateRange();
				if (!$dateRange)
					return false;
				$createdAt = strtotime($product->getCreatedAt());
				return (time() - $createdAt <= $dateRange*86400);
			} else {
				return false;
			}
		}

		if ($fromDate && time() < strtotime($fromDate))
			return false;

		if ($toDate && time() > strtotime($toDate))
			return false;

		return true;
	}
	
	protected function _isSale($product) {
		$price = $product->getPrice();
		$specialPrice = $product->getData('special_price');
		if ($price < 0) {
			return false;
		}
		
		if (!$specialPrice) return false;
 
		$value = $price - $specialPrice;
		if ($value < 0.001 )
			return false;
		
		return true;
	}
}