<?php
/*
* Copyright (c) 2014 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Model_Product extends Mage_Core_Model_Abstract {
  public function _construct() {
    parent::_construct();
    $this->_init('productlabel/product');
  }

  public function updateListLabel($newListLabel,$productId){

    $labelByProduct = Mage::getModel('productlabel/product');
    $oldLabel = $labelByProduct->getCollection()->addFieldToFilter('product_id',$productId)->getColumnValues('label_id');    
    $insert = array_diff($newListLabel, $oldLabel);
    $delete = array_diff($oldLabel, $newListLabel);        
    if ($delete) {
      $deleteCollection = $labelByProduct->getCollection()->addFieldToFilter('product_id',$productId)->addFieldToFilter('label_id',array('in'=>$delete));

      foreach($deleteCollection as $del){
        try{
          $del->delete();  
        }catch(Exception $e){
          Mage::log('Error - Delete label by Product',true,'productlabel.log');
        }      
      }
    }    
    if ($insert) {
      $data = array();
      foreach ($insert as $label) {
        $model = Mage::getModel('productlabel/product');        
        $data = array(
        'product_id'  => $productId,
        'label_id' => $label
        );
        $model->setData($data);
        try{
          $model->save();
        }catch(Exception $e){
          $message = 'Error - Save label #'.$label.' by Product';
          Mage::log($message,true,'productlabel.log');
        }
      }
    }
  }
}