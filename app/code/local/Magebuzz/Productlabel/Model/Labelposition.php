<?php
/*
* Copyright (c) 2014 www.magebuzz.com 
*/
class Magebuzz_Productlabel_Model_Labelposition extends Varien_Object {
  // define position of Label

  const TOP_LEFT  = 1;
  const TOP_RIGHT  = 2;
  const TOP_CENTER  = 3;
  const MIDDLE_LEFT  = 4;
  const MIDDLE_RIGHT  = 5;
  const MIDDLE_CENTER  = 6;
  const BOTTOM_LEFT	= 7;
  const BOTTOM_RIGHT  = 8;
  const BOTTOM_CENTER	= 9;

  static public function getOptionArray() {
    return array(
    self::TOP_LEFT    => Mage::helper('productlabel')->__('Top-Left'),
    self::TOP_CENTER   => Mage::helper('productlabel')->__('Top-Center'),
    self::TOP_RIGHT   => Mage::helper('productlabel')->__('Top-Right'),
    self::MIDDLE_LEFT   => Mage::helper('productlabel')->__('Middle-Left'),
    self::MIDDLE_CENTER   => Mage::helper('productlabel')->__('Middle-Center'),
    self::MIDDLE_RIGHT   => Mage::helper('productlabel')->__('Middle-Right'),
    self::BOTTOM_LEFT   => Mage::helper('productlabel')->__('Bottom-Left'),
    self::BOTTOM_CENTER   => Mage::helper('productlabel')->__('Bottom-Center'),
    self::BOTTOM_RIGHT   => Mage::helper('productlabel')->__('Bottom-Right'),
    );
  }
}