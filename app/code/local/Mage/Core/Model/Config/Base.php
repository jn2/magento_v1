<?php

/**
 * Abstract configuration class
 *
 * Used to retrieve core configuration values
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Config_Base extends Varien_Simplexml_Config {

    /**
     *
     * @var Modules which is disabled by Admin config disable_output modules
     */
    protected static $_disabledModules = null;

    /**
     *
     * @var bool
     */
    protected $_isLoadingMageModules = false;

    /**
     * Constructor
     *
     */
    public function __construct($sourceData = null) {
        $this->_elementClass = 'Mage_Core_Model_Config_ElementExtend';
        parent::__construct($sourceData);
    }

    /**
     * 
     * Check if module is disabled on advanced/disabled_module_output
     * @param string $module
     * @return boolean
     */
    public static function isModuleDisabled($module) {
        self::_loadDisabledModulesFromDatabase();
        #jn2_MaxLoja enabled by default Mage_ enabled by default
        $defaultIsDisabled = strpos($module, 'Mage_') === false && $module !== 'Jn2_MaxLoja';        
        
        return isset(self::$_disabledModules[$module]) ? (bool) self::$_disabledModules[$module] : $defaultIsDisabled;
    }

    protected static function _loadDisabledModulesFromDatabase() {
        if (null === self::$_disabledModules) {
            //load global sopces        
            $configCollection = Mage::getModel('core/config_data')
                    ->getCollection()
                    ->addFieldToFilter('path', array('like' => 'advanced/modules_disable_output/%'))
                    ->addFieldToFilter('scope', array('eq' => 'default')) #global Scope
                    ->addFieldToFilter('scope_id', array('eq' => '0'));  #global Scope

            foreach ($configCollection as $config) {
                $ary = explode('modules_disable_output/', $config->getPath());
                $module = end($ary);
                $disabled = (int) trim($config->getValue()) > 0;
                $isMageModule = strpos($module, 'Mage_') === 0;
                /* We can't disable Mage_ modules */
                if ($disabled && !$isMageModule) {
                    self::$_disabledModules[$module] = true;
                } else {
                    self::$_disabledModules[$module] = false;
                }
            }
        }
    }

//    /**
//     * Override loadFile
//     * @param string $file
//     */
//    public function loadFile($file) {
//        $result = parent::loadFile($file);
//        if (strpos($file, '/app/code/core/Mage/') !== false) {
//            if (!$this->_isLoadingMageModules) {
//                $this->_beforeLoadMageModules();
//            }
//            $this->_isLoadingMageModules = true;
//        } else {
//            if ($this->_isLoadingMageModules) {
//                $this->_afterLoadMageModules();
//            }
//            $this->_isLoadingMageModules = false;
//        }
//        return $result;
//    }
//
//    /**
//     * After load all app/etc/modules but not loaded app/code/core/mage/...etc/config.xml yet
//     */
//    protected function _beforeLoadMageModules() {
//        
//    }
//
//    /**
//     * After loaded app/code/core/mage/.../etc/config.xml 
//     */
//    protected function _afterLoadMageModules() {
//        
//    }


}
