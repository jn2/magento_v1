<?php

/**
 * Abstract configuration class
 *
 * Used to retrieve core configuration values
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Config_ElementExtend extends Mage_Core_Model_Config_Element {

    /**
     * 
     * {@inheritdoc}
     */
    public function is($var, $value = true) {
        /**
         * override checks of active node from module if module is disabled
         * Only if active == true do checks
         */
        if (!is_null($this->getParent()) && $this->getParent()->getName() === 'modules' && $var == 'active') {
            if ("" === (string) $this->original_active) {
                $this->setNode('original_active', parent::is($var, $value));
            }
            if (Mage_Core_Model_Config_Base::isModuleDisabled($this->getName())) {
                $this->setNode('active', 'false');
            }
        }
        $return = parent::is($var, $value);
        return $return;
    }

}
