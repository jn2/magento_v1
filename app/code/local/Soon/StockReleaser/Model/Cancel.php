<?php

/**
 * Agence Soon
 *
 * @category    Soon
 * @package     Soon_StockReleaser
 * @copyright   Copyright (c) 2011 Agence Soon. (http://www.agence-soon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Hervé Guétin
 */
class Soon_StockReleaser_Model_Cancel extends Mage_Core_Model_Abstract {
    const ORDER_STATUSES_CONFIG = 'stockreleaser/settings/order_statuses';

    public function _construct() {
        $this->_init('stockreleaser/cancel');
    }

    /**
     * Save cancellation date of order
     *
     * @param Mage_Sales_Model_Order $order
     * @return Soon_StockReleaser_Model_Cancel
     */
    public function registerCancel($order) {
        $paymentMethod = $order->getPayment()->getMethod();

        $leadtimeValue = Mage::getStoreConfig('stockreleaser/leadtime/' . $paymentMethod);

        if ($leadtimeValue != '') { //Only continue if leadtime value is set.
            $leadtime = $leadtimeValue * $this->_getLeadtimeMultiplier(Mage::getStoreConfig('stockreleaser/leadtime/' . $paymentMethod . '-unit'));
            $autoCancelDate = date("Y-m-d H:i:s", strtotime($order->getCreatedAt()) + $leadtime);

            $data = array(
                'order_id' => $order->getId(),
                'autocancel_date' => $autoCancelDate,
            );

            $this->setData($data);
            $this->save();
        }

        return $this;
    }

    /**
     * Process order cancellation
     *
     * @return Soon_StockReleaser_Model_Cancel
     */
    public function processCancel() {

        $orders = Mage::getModel('sales/order')
                ->getCollection()
                ->addAttributeToFilter('status', array('in' => $this->_getOrderStatuses()));

        foreach ($orders as $order) {
            $ordersIds[] = $order->getId();
        }

        $ordersToCancel = $this->getResourceCollection()
                ->addFieldToFilter('order_id', array('in' => $ordersIds))
                ->addFieldToFilter('autocancel_date', array('lt' => now()))
                ->addIsNotCanceledFilter();

        foreach ($ordersToCancel as $orderToCancel) {
            $order = Mage::getModel('sales/order')->load($orderToCancel->getOrderId());
            $order->cancel()
                    ->save();

            $this->sendMail($order, 'order_cancel');

            $orderComment = Mage::helper('stockreleaser')->__('This order has been automatically cancelled by the "Soon_StockReleaser" module.');
            $order->setStatus(Mage_Sales_Model_Order::STATE_CANCELED)
                    ->addStatusHistoryComment($orderComment)
                    ->save();

            $orderToCancel->setAutocancelStatus(1)->save();
        }

        return $this;
    }

    public function resendBillet() {

      $orders = Mage::getModel('sales/order')
              ->getCollection()
              ->addAttributeToFilter('status', array('in' => $this->_getOrderStatuses()));

      foreach ($orders as $order) {
          $ordersIds[] = $order->getId();
      }
      $ordersToRemember = $this->getResourceCollection()
              ->addFieldToFilter('order_id', array('in' => $ordersIds))
              ->addIsNotCanceledFilter();

      foreach ($ordersToRemember as $orderToRemember) {
        $order = Mage::getModel('sales/order')->load($orderToRemember->getOrderId());

        $now          = new DateTime("now");
        $created_at   = date_create($order->getCreatedAt());
        $datediff     = date_diff($now,$created_at);

        $daysdiff     = $datediff->format("%d");   // Days
        // $daysdiff     = $datediff->format("%i");      // Minutes

        $store_id     = Mage::app()->getStore()->getId();
        $days         = Mage::getStoreConfig('stockreleaser/emails/remember_days', $store_id);

        try {
          $days = explode(',', $days);
        } catch(Exception $e) {
          $days = array(0);
        }

        if (in_array($daysdiff, $days)) {
          $this->sendMail($order, 'billet_remember');
        }

      }
    }

    public function sendMail($order, $email_template) {
      //Codigo da loja
      $storeId = Mage::app()->getStore()->getId();

      //Dados do pagamento
      $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())->setIsSecureMode(true);
      $paymentBlock->getMethod()->setStore($storeId);
      $paymentBlockHtml = $paymentBlock->toHtml();

      //Ajustes do email
      $templateId = $email_template;

      $mailer     = Mage::getModel('core/email_template_mailer');
      $emailInfo  = Mage::getModel('core/email_info');
      $emailInfo->addTo($order->getCustomerEmail(), $order->getCustomerName());
      $mailer->addEmailInfo($emailInfo);

      // Set sender information
	     $senderName   = Mage::getStoreConfig('trans_email/ident_support/name');
	     $senderEmail  = Mage::getStoreConfig('trans_email/ident_support/email');
       $sender       = array(
            'name' => $senderName,
		        'email' => $senderEmail);

      //Registrando order no current order
      Mage::unregister('current_order');
      Mage::register('current_order', $order);
      $dadosboleto = Mage::getModel ( 'boleto/standard' )->prepareValues ();
      foreach ( $dadosboleto as $key => $value ) {
        $dadosboleto [$key] = utf8_decode ( $value );
      }

      $path = BP . DS . 'skin' . DS . 'boletophp' . DS . 'include' . DS;
      $nomebanco = Mage::getStoreConfig ( 'payment/boleto_bancario/banconome' );
      include $path . 'funcoes_' . $nomebanco . '.php';

      // Set all required params and send emails
      $mailer->setSender($sender);
      $mailer->setStoreId($storeId);
      $mailer->setTemplateId($templateId);
      $mailer->setTemplateParams(array(
        'order'        => $order,
        'billing'      => $order->getBillingAddress(),
        'payment_html' => $paymentBlockHtml,
        'billet'       => $dadosboleto["linha_digitavel"],
        'billet_date'  => $dadosboleto["data_vencimento"]
        )
      );
      $mailer->send();

      return true;
    }

    /**
     * Delete outdated orders to automatically cancel
     *
     * Those are the ones that are not automatically canceled yet but completed.
     *
     * @return Soon_StockReleaser_Model_Cancel
     */
    public function cleanCompletedOrders() {
        $collection = $this->getResourceCollection()->getCompletedOrders();
        $collection->walk('delete');
        return $this;
    }

    /**
     * Returns multiplier for seconds based on unit used in config
     *
     * @param string $unit
     * @return int
     */
    protected function _getLeadtimeMultiplier($unit) {
        $leadtimeMultipliers = array(
            'min' => 60,
            'hour' => 3600,
            'day' => 86400
        );

        return (int) $leadtimeMultipliers[$unit];
    }

    /**
     * Retrieve array of order statuses that must be automatically cancelled
     *
     * @return array
     */
    protected function _getOrderStatuses() {
        $orderStatusesConfig = Mage::getStoreConfig(self::ORDER_STATUSES_CONFIG);
        $orderStatuses = explode(',', $orderStatusesConfig);

        return $orderStatuses;
    }

}
