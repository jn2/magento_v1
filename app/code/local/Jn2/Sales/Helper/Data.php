<?php
/**
 * Title
 *
 * @category
 * @package
 * @author      Gustavo Bacelar <gustavo@jn2.com.br>
 * @description
 */

class Jn2_Sales_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCustomerIe($order) {
        try {
            $customer_id = $order->getCustomerId();
            $customer = Mage::getModel('customer/customer')->load($customer_id);
            return $customer->getIe();
        } catch (Exception $e) {
            return false;
        }
    }
    
}