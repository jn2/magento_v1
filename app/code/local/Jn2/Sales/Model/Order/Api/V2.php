<?php

class Jn2_Sales_Model_Order_Api_V2 extends Mage_Sales_Model_Order_Api_V2
{
    
    /**
     * Retrieve full order information
     *
     * @param string $orderIncrementId
     * @return array
     */
    public function info($orderIncrementId)
    {
        $order = $this->_initOrder($orderIncrementId);
        
        if ($order->getGiftMessageId() > 0) {
            $order->setGiftMessage(
                Mage::getSingleton('giftmessage/message')->load($order->getGiftMessageId())->getMessage()
            );
        }
        
        $result = $this->_getAttributes($order, 'order');
        
        $result['shipping_address'] = $this->_getAttributes($order->getShippingAddress(), 'order_address');
        $result['billing_address'] = $this->_getAttributes($order->getBillingAddress(), 'order_address');
        $result['items'] = array();
        
        foreach ($order->getAllItems() as $item) {
            if ($item->getGiftMessageId() > 0) {
                $item->setGiftMessage(
                    Mage::getSingleton('giftmessage/message')->load($item->getGiftMessageId())->getMessage()
                );
            }
            
            $result['items'][] = $this->_getAttributes($item, 'order_item');
        }
        
        $result['payment'] = $this->_getAttributes($order->getPayment(), 'order_payment');
        
        if(Mage::getStoreConfigFlag('base/api_extra_fields/additional_information')) {
            if(empty($order->getPayment()->getAddidionalInformation())){
                $order->getPayment()->setAdditionalInformation($order->getPayment()->getData());
            }
            
            //Get object serialized and not pure object
            $result['payment']['additional_information'] = serialize($order->getPayment()->getAdditionalInformation());
        } else {
            unset($result['payment']['additional_information']);
        }
        
        if(!Mage::getStoreConfigFlag('base/api_extra_fields/customer_taxvat')) {
            unset($result['customer_taxvat']);
        }
        
        if(!Mage::getStoreConfigFlag('base/api_extra_fields/additional_data')) {
            unset($result['payment']['additional_data']);
        }
        
        if(!Mage::getStoreConfigFlag('base/api_extra_fields/cc_trans_id')) {
            unset($result['payment']['cc_trans_id']);
        }
        
        $result['status_history'] = array();
        
        foreach ($order->getAllStatusHistory() as $history) {
            $result['status_history'][] = $this->_getAttributes($history, 'order_status_history');
        }
        
        $helper = Mage::helper('jn2_sales');
        if (Mage::getStoreConfigFlag('base/api_extra_fields/customer_ie') && ($customer_ie = $helper->getCustomerIe($order)) ) {
            $result['customer_ie'] = $customer_ie;
        } else {
            unset($result['customer_ie']);
        }
        
        
        if (Mage::helper('core')->isModuleEnabled('Rede_Adquirencia')) {
            $transacoes = Mage::getModel('rede_adquirencia/transacoes')->getCollection()->addFieldToFilter('order_id', $order->getId());
            
            if ($transacao = $transacoes->getFirstItem()) {
                $result['rede_tid']                     = $transacao->getTid();
                $result['rede_nsu']                     = $transacao->getNsu();
                $result['rede_installments']            = $transacao->getInstallments();
                $result['rede_authorization_number']    = $transacao->getAuthorizationNumber();
                $result['rede_card_brand']              = $transacao->getCardBrand();
                $result['rede_card_number']             = $transacao->getCardNumber();
            }
            
        }
        
        return $result;
    }
}
