<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Integrator abstract class
 *
 * @category   BSellerERP
 * @package    BSellerERP_Core
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
abstract class BSellerERP_Core_Model_Integrator_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * Path store config homologation endpoint
     *
     * @const string
     */
    const HOMOLOGATION_ENDPOINT = 'bsellererp_core/settings/homologation_endpoint';

    /**
     * Path store config production endpoint
     *
     * @const string
     */
    const PRODUCTION_ENDPOINT = 'bsellererp_core/settings/production_endpoint';

    /**
     * Path store config mode integration
     *
     * @const string
     */
    const MODE_INTEGRATION = 'bsellererp_core/settings/mode';

    /**
     * Default endpoint mode
     *
     * @const string
     */
    const DEFAULT_ENDPOINT = 'production';

    /**
     * Endpoint for start api client
     *
     * @var string
     */
    public $endpoint;

    /**
     * Set schema for export integration
     *
     * @var array
     */
    public $schema;

    /**
     * Returns helper object
     *
     * @var BSellerERP_Core_Helper_Data
     */
    public $helper;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'core';

    /**
     * Status code success
     *
     * @var array
     */
    protected $statusCodeSuccess = array(
        200, 201, 204
    );

    /**
     * Status code failed
     *
     * @var array
     */
    protected $statusCodeFailed = array(
        400, 401, 415, 422, 500, 503
    );

    /**
     * Getting endpoint url according to the integration mode
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->helper   = Mage::helper('bsellererp_' . $this->currentHelper);
        $this->endpoint = $this->getEndpoint();
    }

    /**
     * Set schema for export integration
     *
     * @param $schema
     * @return $this
     */
    public function setSchema($schema)
    {
        if (!$schema) {
            return $this;
        }

        $this->schema = $schema;

        return $this;
    }

    /**
     * Return endpoint based in integration mode
     *
     * @return mixed
     */
    protected function getEndpoint()
    {
        if (Mage::getStoreConfig(self::MODE_INTEGRATION) == self::DEFAULT_ENDPOINT) {
            return Mage::getStoreConfig(self::PRODUCTION_ENDPOINT);
        }

        return Mage::getStoreConfig(self::HOMOLOGATION_ENDPOINT);
    }

    /**
     * Return a client object.
     *
     * @param string $uri
     * @param array  $options
     *
     * @return Zend_Soap_Client|Zend_Http_Client
     */
    abstract protected function setClient($uri, $options = array());

    /**
     * Start integration
     *
     * @return $this
     */
    abstract protected function init();

    /**
     * Flag integration as success.
     *
     * @return mixed
     */
    abstract protected function success();

    /**
     * Flag integration as failed.
     *
     * @return mixed
     */
    abstract protected function failed();

}
