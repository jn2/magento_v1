<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Rest service abstract class
 *
 * @category   BSellerERP
 * @package    BSellerERP_Core
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
abstract class BSellerERP_Core_Model_Integrator_Rest extends BSellerERP_Core_Model_Integrator_Abstract
{
    /**
     * Path store config homologation token
     *
     * @const string
     */
    const HOMOLOGATION_TOKEN = 'bsellererp_core/settings/homologation_token';

    /**
     * Path store config production token
     *
     * @const string
     */
    const PRODUCTION_TOKEN = 'bsellererp_core/settings/production_token';

    /**
     * Connection timeout Zend Http Client (seconds)
     *
     * @const int
     */
    const HTTP_CLIENT_TIMEOUT = 30;

    /**
     * Path for append in endpoint url
     *
     * @var string
     */
    public $servicePath;

    /**
     * Body param for setting in raw data
     *
     * @var array
     */
    public $bodyParam = array();

    /**
     * Path params for append in endpoint url
     *
     * @var array
     */
    public $pathParams = array();

    /**
     * Query params for append in endpoint url
     *
     * @var array
     */
    public $queryParams = array();

    /**
     * Request Method
     *
     * @var array
     */
    public $requestMethod = Zend_Http_Client::GET;

    /**
     * Response
     *
     * @var Zend_Http_Response
     */
    public $response;

    /**
     * Http Client
     *
     * @var Zend_Http_Client
     */
    protected $client;

    /**
     * Send client request
     *
     * @throws Mage_Core_Exception
     * @return $this
     */
    public function init()
    {
        /**
         * Create new HTTP Client
         */
        $this->setClient(
            $this->getFullEndpoint(),
            array('timeout' => self::HTTP_CLIENT_TIMEOUT)
        );

        if (count($this->bodyParam)) {
            $this->client->setRawData(json_encode($this->bodyParam), 'application/json');
        }

        /**
         * Set token in header client for access API
         */
        $this->authenticate();

        try {
            $this->helper->log('URI: ' . $this->client->getUri());

            $this->response = $this->client->request($this->requestMethod);
            
            return $this->onSuccess();
        } catch (Exception $e) {
            Mage::throwException($this->helper->__($e->getMessage()));
        }

        return $this;
    }

    /**
     * Starting integrator for all stores
     *
     * @return $this
     */
    public function startIntegratorAllStores()
    {
        $allStores = Mage::app()->getStores();

        /**
         * @var Mage_Core_Model_Store $store
         */
        foreach ($allStores as $store) {
            Mage::app()->setCurrentStore($store->getCode());

            $this->helper->log('Starting integration for store: ' . $store->getName());

            /**
             * Set interface name in URI and init integration by store
             */
            $this->setInterface();
            $this->init();
        }

        return $this;
    }

    /**
     * Get interface name by store and set URI Client HTTP
     *
     * @return $this
     */
    protected function setInterface()
    {
        $interfaceName = Mage::getStoreConfig('bsellererp_core/settings/interface_name');

        $params = array(
            'tipoInterface' => $interfaceName
        );

        $this->setQueryParams($params);

        return $this;
    }

    /**
     * Return a restful client according to URI.
     *
     * @param string $uri
     * @param array  $options
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function setClient($uri, $options = array())
    {
        if (!$uri) {
            Mage::throwException($this->helper->__('Invalid URI for create new HTTP Client'));
        }

        $this->client = new Zend_Http_Client($uri, $options);

        return $this;
    }

    /**
     * Set authentication in client
     *
     * @return $this
     * @throws Zend_Http_Client_Exception
     */
    protected function authenticate()
    {
        $token = $this->getToken();

        if (!$token) {
            Mage::throwException($this->helper->__('Invalid token for authentication'));
        }

        $header = array(
            'X-Auth-Token' => $token
        );

        $this->client->setHeaders($header);

        return $this;
    }

    /**
     * Get complete endpoint with the service path
     *
     * @return string
     */
    protected function getFullEndpoint()
    {
        $url = $this->endpoint . $this->servicePath;

        /**
         * Prepare params
         */
        $pathParams  = $this->preparePathParams();
        $queryParams = $this->prepareQueryParams();

        /**
         * Create full endpoint
         */
        $url = $url . $pathParams . $queryParams;

        return $url;
    }

    /**
     * Set body param
     *
     * @param  array $data
     * @return array
     */
    protected function setBodyParam($data)
    {
        $this->bodyParam = $data;

        return $this->bodyParam;
    }

    /**
     * Set path params
     *
     * @param $params
     * @return array
     */
    protected function setPathParams($params)
    {
        return $this->pathParams = array_merge($this->pathParams, $params);
    }

    /**
     * Set query params
     *
     * @param $params
     * @return array
     */
    protected function setQueryParams($params)
    {
        return $this->queryParams = array_merge($this->queryParams, $params);
    }

    /**
     * Prepare path params for create full endpoint
     *
     * @return string
     */
    protected function preparePathParams()
    {
        $path = '/';

        foreach ($this->pathParams as $param) {
            $path .= $param . '/';
        }

        return $path;
    }

    /**
     * Prepare query params for create full endpoint
     *
     * @return string
     */
    protected function prepareQueryParams()
    {
        if (!count($this->queryParams)) {
            return '';
        }

        $path = '?';

        foreach ($this->queryParams as $key => $value) {
            $path .= $key . '=' . $value;

            if ($value !== end($this->queryParams)) {
                $path .= '&';
            }
        }

        return $path;
    }

    /**
     * Return token based in integration mode
     *
     * @return mixed
     */
    protected function getToken()
    {
        if (Mage::getStoreConfig(self::MODE_INTEGRATION) == self::DEFAULT_ENDPOINT) {
            return Mage::getStoreConfig(self::PRODUCTION_TOKEN);
        }

        return Mage::getStoreConfig(self::HOMOLOGATION_TOKEN);
    }

    /**
     * Process response client for dispatch success or failed
     *
     * @return mixed
     */
    protected function onSuccess()
    {
        if (!$this->response) {
            return $this->failed();
        }

        if (in_array($this->response->getStatus(), $this->statusCodeSuccess)) {
            return $this->success();
        }

        return $this->failed();
    }
}
