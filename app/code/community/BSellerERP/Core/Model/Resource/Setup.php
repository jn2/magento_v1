<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Resource Setup
 *
 * @category   BSellerERP
 * @package    BSellerERP_Core
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Core_Model_Resource_Setup extends Mage_Eav_Model_Entity_Setup
{
    /**
     * Prepare catalog attribute values to save
     *
     * @param array $attr
     * @return array
     */
    protected function _prepareValues($attr)
    {
        $data = parent::_prepareValues($attr);

        $data = array_merge(
            $data,
            array(
                'frontend_input_renderer'       => $this->_getValue($attr, 'input_renderer'),
                'is_global'                     => $this->_getValue(
                    $attr,
                    'global',
                    Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
                ),
                'is_visible'                    => $this->_getValue($attr, 'visible', 1),
                'is_searchable'                 => $this->_getValue($attr, 'searchable', 0),
                'is_filterable'                 => $this->_getValue($attr, 'filterable', 0),
                'is_comparable'                 => $this->_getValue($attr, 'comparable', 0),
                'is_visible_on_front'           => $this->_getValue($attr, 'visible_on_front', 0),
                'is_wysiwyg_enabled'            => $this->_getValue($attr, 'wysiwyg_enabled', 0),
                'is_html_allowed_on_front'      => $this->_getValue($attr, 'is_html_allowed_on_front', 0),
                'is_visible_in_advanced_search' => $this->_getValue($attr, 'visible_in_advanced_search', 0),
                'is_filterable_in_search'       => $this->_getValue($attr, 'filterable_in_search', 0),
                'used_in_product_listing'       => $this->_getValue($attr, 'used_in_product_listing', 0),
                'used_for_sort_by'              => $this->_getValue($attr, 'used_for_sort_by', 0),
                'apply_to'                      => $this->_getValue($attr, 'apply_to'),
                'position'                      => $this->_getValue($attr, 'position', 0),
                'is_configurable'               => $this->_getValue($attr, 'is_configurable', 1),
                'is_used_for_promo_rules'       => $this->_getValue($attr, 'used_for_promo_rules', 0)
            )
        );

        return $data;
    }

    /**
     * Create catalog attribute options for select list
     *
     * @param $attributeCode
     * @param array $optionsArray
     */
    public function addAttributeOptions($attributeCode, array $optionsArray)
    {
        $tableOptions      = $this->getTable('eav_attribute_option');
        $tableOptionValues = $this->getTable('eav_attribute_option_value');
        $attributeId       = (int) $this->getAttribute('catalog_product', $attributeCode, 'attribute_id');

        foreach ($optionsArray as $sortOrder => $label) {
            // Add option
            $data = array(
                'attribute_id' => $attributeId,
                'sort_order'   => $sortOrder
            );
            $this->getConnection()->insert($tableOptions, $data);

            // Add option label
            $optionId = (int) $this->getConnection()->lastInsertId($tableOptions, 'option_id');
            $data = array(
                'option_id' => $optionId,
                'store_id'  => 0,
                'value'     => $label
            );
            $this->getConnection()->insert($tableOptionValues, $data);
        }
    }
}
