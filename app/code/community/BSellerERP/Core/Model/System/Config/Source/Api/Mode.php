<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Options to set the integration environment (Homologation/Production)
 *
 * @category   BSellerERP
 * @package    BSellerERP_Core
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Core_Model_System_Config_Source_Api_Mode
{
    /**
     * Mode PRODUCTION
     * @const string
     */
    const MODE_PRODUCTION = 'production';

    /**
     * Mode HOMOLOGATION
     * @const string
     */
    const MODE_HOMOLOGATION = 'homologation';

    /**
     * Get Mode types
     *
     * @return array
     */
    public function toOptionArray()
    {
        /** @var BSellerERP_Core_Helper_Data $helper */
        $helper = Mage::helper('bsellererp_core');

        return array(
            array(
                'value' => self::MODE_HOMOLOGATION,
                'label' => $helper->__('Homologation'),
            ),
            array(
                'value' => self::MODE_PRODUCTION,
                'label' => $helper->__('Production'),
            ),
        );
    }
}