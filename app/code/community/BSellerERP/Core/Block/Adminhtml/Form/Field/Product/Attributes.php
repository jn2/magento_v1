<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Mapping product attributes model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Core
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Core_Block_Adminhtml_Form_Field_Product_Attributes
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    /**
     * Column attributes ERP
     *
     * @const string
     */
    const COLUMN_CODE_ERP = 'attributes_erp';

    /**
     * Column attributes application
     *
     * @const string
     */
    const COLUMN_CODE_APPLICATION = 'attributes';

    /**
     * Renderer types
     *
     * @var array
     */
    protected $renderer = array();

    /**
     * Predefined attributes ERP
     *
     * @var array
     */
    protected $predefinedAttributes = array(
        'brand'             => 'Brand',
        'ncm'               => 'NCM',
        'dimension_height'  => 'Dimension Height',
        'dimension_length'  => 'Dimension Length',
        'dimension_width'   => 'Dimension Width',
        'ean'               => 'EAN'
    );

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $helper = Mage::helper('bsellererp_core');

        $this->addColumn(
            self::COLUMN_CODE_ERP,
            array(
                'label' => $helper->__('ERP Attributes'),
                'class' => 'input-text required-entry',
                'renderer' => $this->getRenderer(self::COLUMN_CODE_ERP)
            )
        );

        $this->addColumn(
            self::COLUMN_CODE_APPLICATION,
            array(
                'label' => $helper->__('Magento Attributes'),
                'class' => 'input-text required-entry',
                'renderer' => $this->getRenderer(self::COLUMN_CODE_APPLICATION)
            )
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = $helper->__('Add new mapping');
    }

    /**
     * Get renderer by type
     *
     * @param string $column
     * @return mixed
     */
    protected function getRenderer($column)
    {
        if (isset($this->renderer[$column])) {
            return $this->renderer[$column];
        }

        $options = $this->getVariationsOptions();

        if ($column == self::COLUMN_CODE_APPLICATION) {
            $options = $this->getAttributesOptions();
        }

        $this->renderer[$column] = $this->getLayout()
            ->createBlock('core/html_select')
            ->setIsRenderToJsTemplate(true)
            ->setOptions($options);

        return $this->renderer[$column];
    }

    /**
     * Prepare row and set option selected
     *
     * @param Varien_Object $row
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->getRenderer(self::COLUMN_CODE_ERP)->calcOptionHash($row->getData(self::COLUMN_CODE_ERP)),
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->getRenderer(self::COLUMN_CODE_APPLICATION)->calcOptionHash($row->getData(self::COLUMN_CODE_APPLICATION)),
            'selected="selected"'
        );
    }

    /**
     * Render array cell for prototypeJS template
     *
     * @param string $columnName
     * @return string
     * @throws Exception
     */
    protected function _renderCellTemplate($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new Exception('Wrong column name specified.');
        }
        $column     = $this->_columns[$columnName];
        $inputName  = $this->getElement()->getName() . '[#{_id}][' . $columnName . ']';

        if ($column['renderer']) {

            return $column['renderer']->setInputName($inputName)
                ->setName($inputName)
                ->setColumnName($columnName)
                ->setColumn($column)
                ->toHtml();
        }

        return '<input type="text" name="' . $inputName . '" value="#{' . $columnName . '}" ' .
        ($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' .
        (isset($column['class']) ? $column['class'] : 'input-text') . '"'.
        (isset($column['style']) ? ' style="'.$column['style'] . '"' : '') . '/>';
    }

    /**
     * Get all attributes for mapping
     *
     * @return array
     */
    protected function getAttributesOptions()
    {
        $attributes = Mage::getResourceSingleton('catalog/product_attribute_collection')->addVisibleFilter();
        $options    = array();

        $options[] = array(
            'label' => Mage::helper('bsellererp_product')->__('-- Please Select --'),
            'value' => ''
        );

        /**
         * @var Mage_Catalog_Model_Entity_Attribute $attribute
         */
        foreach ($attributes as $attribute) {
            $options[$attribute->getData('attribute_code')] = $attribute->getData('frontend_label');
        }

        return $options;
    }

    /**
     * Get all variations for mapping
     *
     * @return array
     */
    protected function getVariationsOptions()
    {
        $variations = Mage::getSingleton('bsellererp_variation/items')->getCollection();
        $options    = array();

        $options[] = array(
            'label' => Mage::helper('bsellererp_product')->__('-- Please Select --'),
            'value' => ''
        );

        /**
         * Adding predefined attributes
         */
        $options = array_merge($options, $this->predefinedAttributes);

        /**
         * @var BSellerERP_Variation_Model_Items $attribute
         */
        foreach ($variations as $variation) {
            $options[$variation->getData('variation_id')] = $variation->getData('name');
        }

        return $options;
    }
}
