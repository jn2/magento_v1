<?php

/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSellerERP_Core_AttributeController extends Mage_Core_Controller_Front_Action
{
    /**
     * Init integration
     */
    public function initAction()
    {
        Mage::getModel('bsellererp_attribute/cron')->import();
    }

    /**
     * List attributes
     */
    public function listAction()
    {
        $data = array(
            0 => array(
                'id' => 1,
                'descricao' => 'Voltagem',
                'visivel' => false,
                'filtro' => false,
                'agrupamentos' => array(
                    0 => array(
                        'id' => 1,
                        'descricao' => 'Eletrônicos',
                    ),
                    1 => array(
                        'id' => 2,
                        'descricao' => 'Computadores',
                    )
                )
            ),
            1 => array(
                'id' => 2,
                'descricao' => 'Processador',
                'visivel' => false,
                'filtro' => false,
                'agrupamentos' => array(
                    0 => array(
                        'id' => 2,
                        'descricao' => 'Computadores'
                    )
                )
            ),
            2 => array(
                'id' => 3,
                'descricao' => 'Cache',
                'visivel' => false,
                'filtro' => false,
                'agrupamentos' => array()
            ),
            3 => array(
                'id' => 4,
                'descricao' => 'Chipset',
                'visivel' => false,
                'filtro' => false,
                'agrupamentos' => array()
            ),
            4 => array(
                'id' => 5,
                'descricao' => 'Memória Ram',
                'visivel' => false,
                'filtro' => false,
                'agrupamentos' => array()
            ),
            5 => array(
                'id' => 6,
                'descricao' => 'Hd',
                'visivel' => false,
                'filtro' => false,
                'agrupamentos' => array()
            ),
            6 => array(
                'id' => 7,
                'descricao' => 'Placa Mãe',
                'visivel' => false,
                'filtro' => false,
                'agrupamentos' => array()
            ),
            7 => array(
                'id' => 8,
                'descricao' => 'Poras Usb',
                'visivel' => false,
                'filtro' => false,
                'agrupamentos' => array()
            ),
            8 => array(
                'id' => 9,
                'descricao' => 'Som',
                'visivel' => false,
                'filtro' => false,
                'agrupamentos' => array()
            ),
            9 => array(
                'id' => 10,
                'descricao' => 'Memória De Vídeo',
                'visivel' => false,
                'filtro' => false,
                'agrupamentos' => array()
            )
        );

        $this->getResponse()->setBody(json_encode($data));
    }
}
