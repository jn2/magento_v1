<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Core_CouponController extends Mage_Core_Controller_Front_Action
{
    /**
     * Init integration
     */
    public function initAction()
    {
        Mage::getModel('bsellererp_coupon/cron')->import();
    }

    public function listAction()
    {
        $data = array (
            'batchNumber' => 0,
            'content' =>
                array (
                    0 =>
                        array (
                            'cpfCliente' => 0,
                            'idVale' => rand(1000, 3000),
                            'situacao' => 'A',
                            'validade' => '2016-05-31T21:09:29.002Z',
                            'valor' => 50,
                        ),
                    1 =>
                        array (
                            'cpfCliente' => 0,
                            'idVale' => rand(4000, 6000),
                            'situacao' => 'I',
                            'validade' => '2016-06-22T21:09:29.002Z',
                            'valor' => 90,
                        ),
                    2 =>
                        array (
                            'cpfCliente' => 0,
                            'idVale' => rand(7000, 9000),
                            'situacao' => 'A',
                            'validade' => '2017-07-12T21:09:29.002Z',
                            'valor' => 12,
                        ),
                ),
            'totalElements' => 0,
        );

        $this->getResponse()->setBody(json_encode($data));
    }
}
