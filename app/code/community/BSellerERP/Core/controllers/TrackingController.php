<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Core_TrackingController extends Mage_Core_Controller_Front_Action
{
    /**
     * Init integration
     */
    public function initAction()
    {
        Mage::getModel('bsellererp_tracking/cron')->import();
    }

    /**
     * Prepare data and return json (Endpoint Test)
     */
    public function listAction()
    {
        $data = array (
            'content' =>
                array (
                    0 =>
                        array (
                            'idEntrega' => 22025873,
                            'dataEntregaCorrigida' => '2016-06-22T00:00:00.000-04:00',
                            'ponto' =>
                                array (
                                    'id' => 'ETR',
                                    'descricao' => 'ENVIADO',
                                    'data' => '2016-03-16T10:40:59.000-04:00',
                                ),
                            'transportadora' =>
                                array (
                                    'id' => null,
                                    'nome' => null,
                                    'contrato' => null,
                                ),
                            'notaFiscal' =>
                                array (
                                    'id' => null,
                                    'serie' => null,
                                    'dataEmissao' => null,
                                    'chaveAcesso' => '12381028302198301928',
                                    'linkNfe' => 'http://link.nfe',
                                ),
                            'pedido' =>
                                array (
                                    'numeroPedido' => '100000208',
                                    'numeroPedidoExterno' => null,
                                ),
                            'codigosRastreio' =>
                                array (
                                    '23409240PDSAKPODSK019329083',
                                    'ASD304928DSAP'
                                ),
                        ),
                ),
            'totalElements' => 371,
            'batchNumber' => 23975,
        );

        $this->getResponse()->setBody(json_encode($data));
    }
}
