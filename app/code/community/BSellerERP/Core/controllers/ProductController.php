<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

class BSellerERP_Core_ProductController extends Mage_Core_Controller_Front_Action
{
    /**
     * Init integration
     */
    public function initAction()
    {
        Mage::getModel('bsellererp_product/cron')->import();
    }

    /**
     * Confirm batch
     */
    public function confirmAction()
    {
        Mage::getModel('bsellererp_product/batch', 256)->init();
    }

    /**
     * Prepare data and return json (Endpoint Test)
     */
    public function listAction()
    {
        $configurable = array (
            'content' =>
                array (
                    1 =>
                        array (
                            'codigoItem' => '934706',
                            'codigoTerceiro' => '26.09845',
                            'codigoFornecedor' => 'CODIGODOITEMFILHOPARAFORNECEDOR1',
                            'nome' => 'Item filho 1',
                            'titulo' => 'Titulo do item filho 1',
                            'descricao' => 'DescriÃ§Ã£o completa do item filho 1',
                            'marca' => 'Nome da marca do produto',
                            'situacao' => 'A',
                            'dataCadastro' => '2016-05-03T15:11:48.000-04:00',
                            'dataAtualizacaoCadastro' => '2016-05-06T17:25:08.000-04:00',
                            'operacao' => 'A',
                            'itemPai' => false,
                            'codigoItemPai' => 'ABC10000',
                            'brinde' => false,
                            'agrupamentoFichaTecnica' => array(
                                'descricao' => 'Agrupamento Teste',
                                'id' => 1
                            ),
                            'tipoItem' => 'P',
                            'itemVirtual' =>
                                array (
                                    'tipoItemVirtual' => NULL,
                                    'itemFisico' => false,
                                    'prazoValidadeGarantia' => NULL,
                                    'tipoGarantia' => NULL,
                                ),
                            'prazoGarantiaFornecedor' => 12,
                            'tipoTransporte' =>
                                array (
                                    'id' => 1,
                                    'nome' => 'Leve',
                                ),
                            'unidadeMedida' =>
                                array (
                                    'id' => 'UN',
                                    'nome' => 'Unidade',
                                ),
                            'okFiscal' => true,
                            'vendavel' => true,
                            'controlaVenda' => true,
                            'estruturaMercadologica' =>
                                array (
                                    'departamento' =>
                                        array (
                                            'id' => 1,
                                            'nome' => 'CANÃ‡Ã‚O NOVA',
                                        ),
                                    'setor' =>
                                        array (
                                            'id' => 1,
                                            'nome' => 'PRODUTOS SIMPLES',
                                        ),
                                    'familia' =>
                                        array (
                                            'id' => 1,
                                            'nome' => 'LIVROS',
                                        ),
                                    'subFamilia' =>
                                        array (
                                            'id' => 1,
                                            'nome' => 'CURA E LIBERTAÃ‡ÃƒO',
                                        ),
                                ),
                            'dataLancamento' => NULL,
                            'quantidadePrevenda' => NULL,
                            'dimensoes' =>
                                array (
                                    'altura' => 0.40000000000000002,
                                    'largura' => 0.55000000000000004,
                                    'comprimento' => 0.77000000000000002,
                                ),
                            'peso' =>
                                array (
                                    'unitario' => 0.55000000000000004,
                                    'bruto' => 0.55000000000000004,
                                ),
                            'ean' =>
                                array (
                                    0 =>
                                        array (
                                            'codEan' => 7894568765435,
                                        ),
                                ),
                            'componentesKit' =>
                                array (
                                ),
                            'fichaTecnica' =>
                                array (
                                    array(
                                        'sequencial'=> 29,
                                        'chave'=> array(
                                            'id'=> 29,
                                            'nome'=> 'Data do evento',
                                            'valor'=> 'meu test'
                                        ),
                                        'ordem'=> 29,
                                        'visivel'=> true,
                                        'filtro'=> false,
                                        'situacao'=> 'A'
                                    )
                                ),
                            'categorias' =>
                                array (
                                ),
                            'variacoes' =>
                                array (
                                    0 =>
                                        array (
                                            'idTipoVariacao' => 1,
                                            'tipoVariacao' => 'COR PREDOMINANTE',
                                            'idVariacao' => 1,
                                            'variacao' => 'BRANCO',
                                        ),
                                    1 =>
                                        array (
                                            'idTipoVariacao' => 3,
                                            'tipoVariacao' => 'TAMANHO',
                                            'idVariacao' => 1,
                                            'variacao' => 'PP',
                                        ),
                                ),
                            'preco' =>
                                array (
                                    0 => array(
                                        'custoMedio' => 0,
                                        'precoDe' => 20,
                                        'precoPor' => 30,
                                        'precoUltimaCompra' => 0
                                    )
                                ),
                            'tags' => 'tags do item filho 1',
                            'ncm' =>
                                array (
                                    'codigoNcm' => 34060000,
                                    'sequencial' => 0,
                                    'descricao' => 'VELAS,PAVIOS,CIRIOS	E	ARTIGOS	SEMELHANTES	   						',
                                ),
                            'procedencia' =>
                                array (
                                    'id' => 0,
                                    'nome' => 'Nacional',
                                ),
                        ),
                ),
        );

        $virtual = array (
            'content' =>
                array (
                    0 =>
                        array (
                            'codigoItem' => '934709',
                            'codigoTerceiro' => 'Itemvirtual123',
                            'codigoFornecedor' => NULL,
                            'nome' => 'Item Virtual',
                            'titulo' => NULL,
                            'descricao' => NULL,
                            'marca' => NULL,
                            'situacao' => 'A',
                            'dataCadastro' => '2016-05-03T15:42:30.000-04:00',
                            'dataAtualizacaoCadastro' => NULL,
                            'operacao' => 'N',
                            'itemPai' => false,
                            'codigoItemPai' => NULL,
                            'brinde' => false,
                            'agrupamentoFichaTecnica' => array(
                                'descricao' => 'Agrupamento Teste',
                                'id' => 1
                            ),
                            'tipoItem' => 'N',
                            'itemVirtual' =>
                                array (
                                    'tipoItemVirtual' => 'N',
                                    'itemFisico' => false,
                                    'prazoValidadeGarantia' => NULL,
                                    'tipoGarantia' => NULL,
                                ),
                            'prazoGarantiaFornecedor' => NULL,
                            'tipoTransporte' =>
                                array (
                                    'id' => NULL,
                                    'nome' => NULL,
                                ),
                            'unidadeMedida' =>
                                array (
                                    'id' => 'UN',
                                    'nome' => 'Unidade',
                                ),
                            'okFiscal' => true,
                            'vendavel' => false,
                            'controlaVenda' => true,
                            'estruturaMercadologica' =>
                                array (
                                    'departamento' =>
                                        array (
                                            'id' => NULL,
                                            'nome' => NULL,
                                        ),
                                    'setor' =>
                                        array (
                                            'id' => NULL,
                                            'nome' => NULL,
                                        ),
                                    'familia' =>
                                        array (
                                            'id' => NULL,
                                            'nome' => NULL,
                                        ),
                                    'subFamilia' =>
                                        array (
                                            'id' => NULL,
                                            'nome' => NULL,
                                        ),
                                ),
                            'dataLancamento' => NULL,
                            'quantidadePrevenda' => NULL,
                            'dimensoes' =>
                                array (
                                    'altura' => NULL,
                                    'largura' => NULL,
                                    'comprimento' => NULL,
                                ),
                            'peso' =>
                                array (
                                    'unitario' => NULL,
                                    'bruto' => NULL,
                                ),
                            'ean' =>
                                array (
                                ),
                            'componentesKit' =>
                                array (
                                ),
                            'fichaTecnica' =>
                                array (
                                ),
                            'categorias' =>
                                array (
                                ),
                            'variacoes' =>
                                array (
                                ),
                            'preco' =>
                                array (
                                ),
                            'tags' => NULL,
                            'ncm' =>
                                array (
                                    'codigoNcm' => NULL,
                                    'sequencial' => NULL,
                                    'descricao' => NULL,
                                ),
                            'procedencia' =>
                                array (
                                    'id' => 0,
                                    'nome' => 'Nacional',
                                ),
                        ),
                ),
        );

        $this->getResponse()->setBody(json_encode($configurable));
    }
}
