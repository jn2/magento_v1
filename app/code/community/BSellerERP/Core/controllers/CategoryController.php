<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

class BSellerERP_Core_CategoryController extends Mage_Core_Controller_Front_Action
{
    /**
     * Init catalog category integration
     */
    public function initAction()
    {
        Mage::getModel('bsellererp_category/cron')->import();
    }

    /**
     * Clear all categories and preserve
     *
     * @return $this
     * @throws Exception
     */
    public function clearAllAction()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $allCategories = Mage::getModel('catalog/category')->getCollection();

        foreach ($allCategories as $category) {
            if ($category->getLevel() == 0 || $category->getLevel() == 1) {
                continue;
            }

            $category->delete();
        }

        return $this;
    }
}
