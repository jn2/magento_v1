<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller core module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Core
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Core_Helper_Customer extends BSellerERP_Core_Helper_Data
{
    /**
     * Person type attribute
     *
     * @const string
     */
    const PERSON_TYPE = 'person_type';

    /**
     * Person type options
     *
     * @const string
     */
    const LEGAL_PERSON   = 'legal_person';
    const NATURAL_PERSON = 'natural_person';

    /**
     * Attribute contributor
     *
     * @const string
     */
    const CONTRIBUTOR = 'contributor';

    /**
     * Customer type by contributor
     *
     * @const int
     */
    const CUSTOMER_TYPE_NATURAL        = 2;
    const CUSTOMER_TYPE_CONTRIBUTOR    = 1;
    const CUSTOMER_TYPE_NO_CONTRIBUTOR = 5;

    /**
     * Attribute state registration
     *
     * @const string
     */
    const STATE_REGISTRATION = 'state_registration';

    /**
     * CRT Regime
     *
     * @const int
     */
    const CRT_REGIME_SIMPLE   = 1;
    const CRT_REGIME_STANDARD = 3;

    /**
     * Tax regime configuration
     *
     * @const string
     */
    const TAX_REGIME          = 'tax_regime';
    const TAX_REGIME_SIMPLE   = 'bsellererp_core/mapping/tax_regime_simple';
    const TAX_REGIME_STANDARD = 'bsellererp_core/mapping/tax_regime_standard';
    
    /**
     * Customer
     *
     * @var Mage_Customer_Model_Customer
     */
    public $customer;

    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'core_customer';

    /**
     * Person Type
     *
     * @var string
     */
    protected $personType = null;

    /**
     * Current address by customer
     *
     * @var Mage_Customer_Model_Address
     */
    protected $currentAddress;

    /**
     * Set customer
     *
     * @param  Mage_Customer_Model_Customer $customer
     * @return $this
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Set customer address
     *
     * @param  Mage_Customer_Model_Address $address
     * @return $this
     */
    public function setAddress($address = null)
    {
        if (is_null($address)) {
            $this->currentAddress = $this->customer->getDefaultBillingAddress();

            return $this;
        }

        $this->currentAddress = $address;

        return $this;
    }

    /**
     * Prepare data for person type
     *
     * @return array
     */
    public function preparePersonType()
    {
        if ($this->getPersonType() == self::LEGAL_PERSON) {
            return $this->getLegalPersonData();
        }

        return $this->getNaturalPersonData();
    }

    /**
     * Get data for Legal Person
     *
     * @return array
     */
    public function getLegalPersonData()
    {
        $data = array();

        /**
         * Get contributor value
         */
        $contributor               = $this->getPredefinedAttributeValue(self::CONTRIBUTOR, $this->customer);
        $data['tipoCliente']       = self::CUSTOMER_TYPE_NO_CONTRIBUTOR;
        $data['tipoRepresentante'] = self::CUSTOMER_TYPE_NO_CONTRIBUTOR;

        if ($contributor) {
            $data['tipoCliente']       = self::CUSTOMER_TYPE_CONTRIBUTOR;
            $data['tipoRepresentante'] = self::CUSTOMER_TYPE_CONTRIBUTOR;
            $data['inscricaoEstadual'] = strtoupper($this->getPredefinedAttributeValue(self::STATE_REGISTRATION, $this->customer));
        }

        /**
         * Get Tax Regime value
         */
        $taxRegime = $this->getPredefinedAttributeValue(self::TAX_REGIME, $this->customer);
        switch ($taxRegime) {
            case Mage::getStoreConfig(self::TAX_REGIME_SIMPLE):
                $data['crt'] = self::CRT_REGIME_SIMPLE;

                break;
            case Mage::getStoreConfig(self::TAX_REGIME_STANDARD):
                $data['crt'] = self::CRT_REGIME_STANDARD;

                break;
            default:
                $data['crt'] = null;

                break;
        }

        return $data;
    }

    /**
     * Get data for Natural Person
     *
     * @return array
     */
    public function getNaturalPersonData()
    {
        $data = array();

        /**
         * Set default value for natural person
         */
        $data['tipoCliente']       = self::CUSTOMER_TYPE_NATURAL;
        $data['tipoRepresentante'] = self::CUSTOMER_TYPE_NATURAL;

        return $data;
    }

    /**
     * Get person type by mapping
     * Returns default value Natural Person
     *
     * @return string
     */
    public function getPersonType()
    {
        $personType       = $this->getPredefinedAttributeValue(self::PERSON_TYPE, $this->customer);
        $legalPerson      = Mage::getStoreConfig('bsellererp_core/mapping/' . self::LEGAL_PERSON);
        $this->personType = self::NATURAL_PERSON;

        if ($personType == $legalPerson) {
            $this->personType = self::LEGAL_PERSON;
        }

        return $this->personType;
    }

    /**
     * Returns name by person type
     *
     * @return string
     */
    public function getName()
    {
        if ($this->getPersonType() == self::LEGAL_PERSON) {
            return $this->getPredefinedAttributeValue('company_name', $this->customer);
        }

        return $this->currentAddress->getName();
    }

    /**
     * Returns document value by person type
     *
     * @return string
     */
    public function getDocumentValue()
    {
        $documentType = 'cpf';

        if ($this->getPersonType() == self::LEGAL_PERSON) {
            $documentType = 'cnpj';
        }

        return $this->onlyNumbers($this->getPredefinedAttributeValue($documentType, $this->customer));
    }

    public function getTipoPedido()
    {
        if (Mage::getStoreConfig('bsellererp_order/settings/order_type')) {
            if ($this->getPersonType() == self::LEGAL_PERSON) {
                return 'R';
            }
        }
        return 'N';
    }

    /**
     * Format address by ERP rules
     *
     * @param  $number
     * @param  $street
     * @param  $complement
     * @return array
     */
    public function formatAddress($number, $street, $complement)
    {
        $data = array(
            'number'     => $number,
            'street'     => $street,
            'complement' => $complement
        );

        if (!is_int($number)) {
            $data['number']     = 0;
            $data['street']     = $street . ' ' . $number;
            $data['complement'] = $complement . ' ' . $number;
        }

        return $data;
    }
}
