<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller core module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Core
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Core_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Path store config debugging mode
     *
     * @const string
     */
    const DEBUGGING_ACTIVE = 'bsellererp_core/settings/debug_mode';

    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'core';

    /**
     * Default prefix for merge in general prefix
     *
     * @var string
     */
    private $defaultPrefix = 'bsellererp';

    /**
     * Save logs by prefix file
     *
     * @param  $message
     * @return $this
     */
    public function log($message)
    {
        if (!Mage::getStoreConfig(self::DEBUGGING_ACTIVE)) {
            return $this;
        }
        
        Mage::log($message, null, $this->getPrefix() . '.log', true);

        return $this;
    }

    /**
     * Return prefix formatted to the module
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->defaultPrefix . '_' . $this->prefix;
    }

    /**
     * Converts input date into date of default ERP
     *
     * @param  string $date
     * @return string
     */
    public function formatDate($date)
    {
        return Mage::getModel('core/date')->date('Y-m-d\TH:i:s.000\Z', $date);
    }

    /**
     * Get street by name
     *
     * @param  string $name
     * @param  mixed $instance
     * @return $this
     */
    public function getStreetByName($name, $instance)
    {
        if (!$code = Mage::getStoreConfig('bsellererp_core/mapping/' . $name)) {
            return '';
        }

        /**
         * @var Mage_Customer_Model_Address $instance
         */
        return $instance->getStreet($code);
    }

    /**
     * Get setting store config by key
     *
     * @param  $key
     * @return mixed
     */
    public function getSettingStoreConfig($key)
    {
        return Mage::getStoreConfig('bsellererp_core/settings/' . $key);
    }

    /**
     * Verify if module is active
     *
     * @return mixed
     */
    public function moduleIsActive()
    {
        return Mage::getStoreConfig($this->getPrefix() . '/settings/active');
    }

    /**
     * Get attribute in store config by name
     *
     * @param  string $name
     * @param  mixed $instance
     * @return $this
     */
    public function getPredefinedAttributeValue($name, $instance)
    {
        if (!$attribute = Mage::getStoreConfig('bsellererp_core/mapping/' . $name)) {
            return '';
        }

        /**
         * @var Varien_Object $instance
         */
        return $instance->getData($attribute);
    }

    /**
     * Returns only numbers
     *
     * @param  $string
     * @return mixed
     */
    public function onlyNumbers($string)
    {
        return preg_replace('/[^0-9]/', '', $string);
    }

    /**
     * Truncate a string to a certain length
     *
     * @param  string $string
     * @param  int $size
     * @return string
     */
    public function truncate($string, $size)
    {
        return Mage::helper('core/string')->truncate($string, $size, '');
    }
}
