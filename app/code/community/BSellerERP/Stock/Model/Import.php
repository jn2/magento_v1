<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Stock
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing stocks in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Stock
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Stock_Model_Import extends BSellerERP_Stock_Model_Schema
{
    /**
     * Content returned integration to import
     * @var object
     */
    protected $stocks;

    /**
     * Init importing stocks
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('stocks' => $this->stocks));

        foreach ($this->stocks as $stock) {
            if (!$this->validate($stock)) {
                continue;
            }

            $this->createOrUpdate($stock);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('stocks' => $this->stocks));

        return $this;
    }

    /**
     * Set data stocks
     *
     * @param $stocks
     * @return $this
     */
    public function setStocks($stocks)
    {
        $this->stocks = $stocks;

        return $this;
    }

    /**
     * Create/Update stock by response ERP
     *
     * @param $stockERP
     * @return $this
     * @throws Exception
     */
    public function createOrUpdate($stockERP)
    {
        $this->stockERP     = $stockERP;
        $productId          = Mage::getModel('catalog/product')->getIdBySku($this->stockERP->codigoItem);
        $this->currentStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);

        if (!$this->currentStock->getId()) {
            $this->helper->log('Impossible update stock, stock item not exist - SKU: ' . $this->stockERP->codigoItem);

            return $this;
        }

        /**
         * Adding data in schema
         */
        $this->currentStock->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_before', array('stock' => $this->currentStock));
        $this->currentStock->save();
        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('stock' => $this->currentStock));

        return $this;
    }

    /**
     * Validate stock for create in application
     *
     * @param object $stock
     * @return bool
     */
    protected function validate($stock)
    {
        if (!$stock->codigoItem || $stock->estoqueEstabelecimento[0]->quantidade === false) {
            $this->helper->log('Stock is not valid for importing, missing codigoItem/quantidade param');
            $this->helper->log($stock);

            return false;
        }

        return true;
    }
}
