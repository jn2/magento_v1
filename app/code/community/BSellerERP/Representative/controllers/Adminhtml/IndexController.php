<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Representative
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Index Controller
 *
 * @category   BSellerERP
 * @package    BSellerERP_Representative
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Representative_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Resend order
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    public function resendAction()
    {
        $customerId = $this->getRequest()->getParam('customer_id');

        if (!$customerId) {
            $this->_getSession()->addError($this->_getHelper()->__('It was not possible to integrate the customer'));

            return $this->_redirectReferer();
        }

        $order = Mage::getModel('customer/customer')->load($customerId);
        $order->setData('erp_status', 'pending');
        $order->save();

        $this->_getSession()->addSuccess($this->_getHelper()->__('Customer sent for queue of integration'));

        return $this->_redirectReferer();
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
       return true;
    }
}
