<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Representative
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller core module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Representative
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Representative_Helper_Data extends BSellerERP_Core_Helper_Data
{
    /**
     * Customer created
     *
     * @const string
     */
    const PENDING = 'pending';

    /**
     * Problems in initial integration
     *
     * @const string
     */
    const BLOCKED = 'blocked';

    /**
     * Customer integrated success
     *
     * @const string
     */
    const SUCCESS = 'success';

    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'representative';
}
