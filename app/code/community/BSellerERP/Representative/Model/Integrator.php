<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Representative
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Integrator category rest
 *
 * @category   BSellerERP
 * @package    BSellerERP_Representative
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Representative_Model_Integrator extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Request Method
     *
     * @var array
     */
    public $requestMethod = Zend_Http_Client::POST;

    /**
     * Service path api
     *
     * @var string
     */
    public $servicePath = 'representantes';

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'representative';

    /**
     * Set object item
     *
     * @var Mage_Customer_Model_Customer
     */
    public $objectItem;

    /**
     * Set parameters and send request
     *
     * @return $this
     */
    public function init()
    {
        if (!count($this->schema)) {
            $this->helper->log('The schema has no data');
        }

        $this->setBodyParam($this->schema);

        return parent::init();
    }

    /**
     * Set object item
     *
     * @param object $objectItem
     * @return $this
     */
    public function setObjectItem($objectItem)
    {
        if (!$objectItem) {
            return $this;
        }

        $this->objectItem = $objectItem;

        return $this;
    }

    /**
     * Flag integration as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $this->helper->log($this->response->getBody());

        $this->objectItem->setData('erp_status', 'success');
        $this->objectItem->save();

        return $this;
    }

    /**
     * Flag integration as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log('Customer ID: ' . $this->objectItem->getId());
        $this->helper->log($this->response->getBody());

        $this->objectItem->setData('erp_status', 'blocked');
        $this->objectItem->save();

        return $this;
    }
}
