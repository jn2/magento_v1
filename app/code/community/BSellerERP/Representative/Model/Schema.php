<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Representative
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Schema model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Representative
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Representative_Model_Schema extends BSellerERP_Core_Model_Schema_Abstract
{
    /**
     * Object Item
     *
     * @var Mage_Customer_Model_Customer $objectItem
     */
    public $objectItem;

    /**
     * Returns helper object
     *
     * @var BSellerERP_Representative_Helper_Data
     */
    protected $helper;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'representative';

    /**
     * Prepare schema and returns for adding in order instance
     *
     * @return array
     */
    protected function prepareSchema()
    {
        parent::prepareSchema();

        if (!$this->objectItem->getId()) {
            $this->helper->log('Object not exist ID for create schema- ID: ' . $this->objectItem->getId());
            return array();
        }

        $this->setGeneralData();
        $this->setAddressData();

        return $this->getSchema();
    }

    /**
     * Setting general data for schema
     *
     * @return $this
     */
    protected function setGeneralData()
    {
        /**
         * @var Mage_Customer_Model_Address $address
         */
        $address = $this->objectItem->getDefaultBillingAddress();

        if (!$address) {
            $this->helper->log('Customer no has default billing address - ID: ' . $this->objectItem->getId());
            return $this;
        }

        $customerHelper = Mage::helper('bsellererp_core/customer')
            ->setCustomer($this->objectItem)
            ->setAddress($address);

        $cellphone = $this->helper->getPredefinedAttributeValue('cellphone', $address);
        $storeId   = $this->objectItem->getStore()->getId();

        $generalInfo = array(
            'dataNascimento'     => $this->helper->formatDate($this->objectItem->getCustomerDob()),
            'email'              => $this->helper->truncate($this->objectItem->getEmail(), 60),
            'nome'               => $this->helper->truncate($address->getName(), 40),
            'fax'                => $this->helper->truncate($address->getFax(), 15),
            'fone1'              => $this->helper->truncate($this->helper->onlyNumbers($address->getTelephone()), 15),
            'fone2'              => $this->helper->truncate($this->helper->onlyNumbers($cellphone), 15),
            'rg'                 => $this->helper->truncate($this->helper->getPredefinedAttributeValue('rg', $this->objectItem), 20),
            'unidadeNegocio'     => Mage::getStoreConfig('bsellererp_core/settings/business_unit', $storeId),
            'id'                 => $this->helper->truncate($customerHelper->getDocumentValue(), 14)
        );

        /**
         * Append person type info
         */
        $generalInfo = array_merge($generalInfo, $customerHelper->preparePersonType());

        $this->addData($generalInfo);

        return $this;
    }

    /**
     * Setting address data for schema
     *
     * @return $this
     */
    protected function setAddressData()
    {
        /**
         * @var Mage_Customer_Model_Address $address
         */
        $address = $this->objectItem->getDefaultBillingAddress();

        if (!$address) {
            $this->helper->log('Customer no has default billing address - ID: ' . $this->objectItem->getId());
            return $this;
        }

        $customerHelper = Mage::helper('bsellererp_core/customer');

        $formatAddress = $customerHelper->formatAddress(
            $this->helper->getStreetByName('address_number', $address),
            $this->helper->getStreetByName('address_street', $address),
            $this->helper->getStreetByName('address_complement', $address)
        );

        $addressInfo = array(
            'bairro'            => $this->helper->truncate($this->helper->getStreetByName('address_neighborhood', $address), 50),
            'logradouro'        => $this->helper->truncate($formatAddress['street'], 60),
            'numero'            => $formatAddress['number'],
            'complemento'       => $this->helper->truncate($formatAddress['complement'], 50),
            'referencia'        => $this->helper->truncate($this->helper->getPredefinedAttributeValue('reference_point', $address), 100),
            'cep'               => $this->helper->onlyNumbers($address->getPostcode()),
            'cidade'            => $address->getCity(),
            'estado'            => $address->getRegionCode(),
            'pais'              => 'BRA',
            'zipCode'           => null,
            'tipoEndereco'      => 'F'
        );


        $this->addData(
            array('enderecos' => array($addressInfo))
        );

        return $this;
    }
}
