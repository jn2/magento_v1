<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Representative
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Export order model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Representative
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Representative_Model_Export extends BSellerERP_Representative_Model_Schema
{
    /**
     * Init export
     *
     * @return $this
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_export_init', array('customer' => $this->objectItem));

        /**
         * Init integration order
         */
        Mage::getModel('bsellererp_representative/integrator')
            ->setSchema($this->prepareSchema())
            ->setObjectItem($this->objectItem)
            ->init();

        Mage::dispatchEvent($this->helper->getPrefix() . '_export_complete', array('customer' => $this->objectItem));

        return $this;
    }

    /**
     * Set object item
     *
     * @param  int $entityId
     * @return $this
     */
    public function setObjectItem($entityId)
    {
        $this->objectItem = Mage::getModel('customer/customer')->load($entityId);

        return $this;
    }
}
