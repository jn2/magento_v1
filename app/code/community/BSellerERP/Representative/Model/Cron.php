<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Representative
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Cron schedule model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Representative
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Representative_Model_Cron
{
    const RECORDS_LIMIT = 'bsellererp_representative/schedule/limit';

    /**
     * Returns helper object
     *
     * @var BSellerERP_Representative_Helper_Data
     */
    public $helper;

    /**
     * Returns records limit
     *
     * @var int
     */
    protected $limit;

    /**
     * Initialize helper and factory model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_representative');
        $this->limit  = Mage::getStoreConfig(self::RECORDS_LIMIT);
    }

    /**
     * Init import orders
     *
     * @return $this
     */
    public function export()
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        /**
         * Get all pending representatives customers
         */
        $customerGroup = Mage::getStoreConfig('bsellererp_representative/settings/group');
        $customers     = Mage::getResourceModel('customer/customer_collection')
            ->addFieldToFilter('group_id', $customerGroup)
            ->addFieldToFilter('erp_status', 'pending')
            ->setPageSize($this->limit);

        try {
            /**
             * Prepare schema and init integration for all orders
             */
            foreach ($customers as $customer) {
                $export = Mage::getModel('bsellererp_representative/export');
                $export->setObjectItem($customer->getId());
                $export->init();
            }

            $this->helper->log('Integration success');
        } catch (Exception $e) {
            $this->helper->log($e->getMessage());
            Mage::throwException($e->getMessage());
        }

        return $this;
    }
}
