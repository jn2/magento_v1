<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Tracking
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing trackings in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Tracking
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Tracking_Model_Import
{
    /**
     * ERP status for relationship
     *
     * @const string
     */
    const STATUS_PROCESSING = 'PAP';
    const STATUS_NFE        = 'NFS';
    const STATUS_SHIPPING   = 'ETR';
    const STATUS_COMPLETE   = 'ENT';
    const STATUS_CANCELED   = 'CAN';

    /**
     * Content returned integration to import
     *
     * @var object
     */
    protected $responseObject;

    /**
     * Helper model
     *
     * @var BSellerERP_Tracking_Helper_Data
     */
    protected $helper;

    protected $statusModelClasses = array(
        self::STATUS_PROCESSING => 'processing',
        self::STATUS_NFE        => 'nfe',
        self::STATUS_SHIPPING   => 'shipping',
        self::STATUS_COMPLETE   => 'complete',
        self::STATUS_CANCELED   => 'canceled'
    );

    /**
     * Initialize helper model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_tracking');
    }

    /**
     * Set response object data
     *
     * @param object $responseObject
     * @return $this
     */
    public function setResponse($responseObject)
    {
        $this->responseObject = $responseObject;

        return $this;
    }

    /**
     * Init importing data
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('deliveries' => $this->responseObject));

        foreach ($this->responseObject as $responseObjectItem) {
            if (!$this->validate($responseObjectItem)) {
                continue;
            }

            $this->import($responseObjectItem);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('deliveries' => $this->responseObject));

        return $this;
    }

    protected function import($responseObjectItem)
    {
        $incrementId = $responseObjectItem->pedido->numeroPedido;
        $order       = Mage::getModel('sales/order')->loadByIncrementId($incrementId);

        if (!$order->getId()) {
            $this->helper->log('Impossible load order for importing tracking: ' . $incrementId);

            return false;
        } else {
            $this->helper->log('Updating tracking for order (increment id): ' . $incrementId);
        }

        /**
         * @var BSellerERP_Tracking_Model_Status_Default $statusModel
         */
        $statusModel = Mage::getModel('bsellererp_tracking/status_default');
        $statusERP   = $responseObjectItem->ponto->id;

        if (key_exists($statusERP, $this->statusModelClasses)) {
            $statusModel = Mage::getModel('bsellererp_tracking/status_' . $this->statusModelClasses[$statusERP]);
        }

        /**
         * Configure model status
         */
        $statusModel->setTracking($responseObjectItem);
        $statusModel->setOrder($order);

        return $statusModel->init();
    }

    /**
     * Validate tracking for create in application
     *
     * @param object $product
     * @return bool
     */
    protected function validate($product)
    {
        $requiredParams = array('numeroPedido', 'entregas');

        foreach ($requiredParams as $param) {
            if ($product->$param === false) {
                $this->helper->log('Tracking is not valid for importing, missing param: ' . $param);
                $this->helper->log($product);

                return false;
            }
        }

        return true;
    }
}
