<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Tracking
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Integrator rest
 *
 * @category   BSellerERP
 * @package    BSellerERP_Tracking
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Tracking_Model_Integrator extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Service path api
     *
     * @var string
     */
    public $servicePath = 'pedidos/tracking/massivo';

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'tracking';

    /**
     * Query Params
     *
     * @var array
     */
    public $queryParams = array(
        'maxRegistros' => 100
    );

    /**
     * Configure query params
     */
    public function __construct()
    {
        /**
         * Adding query params
         */
        $params = array(
            'unidadeNegocio' => Mage::getStoreConfig('bsellererp_core/settings/business_unit')
        );

        $this->setQueryParams($params);

        return parent::__construct();
    }

    /**
     * Flag integration as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $body = Mage::helper('core')->jsonDecode($this->response->getBody(), Zend_Json::TYPE_OBJECT);

        if (!$body || !count($body->content)) {
            Mage::throwException($this->helper->__('Body response not exist data for importing'));

            return $this;
        }

        $import = Mage::getModel('bsellererp_tracking/import');
        $import->setResponse($body->content);
        $import->init();

        /**
         * Confirm batch
         */
        if ($batchNumber = $body->batchNumber) {
            Mage::getModel('bsellererp_tracking/batch', $batchNumber)->init();
        }

        return $this;
    }

    /**
     * Flag integration as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log($this->response->getBody());

        return $this;
    }
}
