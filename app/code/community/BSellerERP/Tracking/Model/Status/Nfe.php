    <?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Tracking
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Status model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Tracking
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Tracking_Model_Status_Nfe extends BSellerERP_Tracking_Model_Status_Abstract
{
    const DEFAULT_STATUS = 'processing';

    /**
     * Initialize status import
     */
    public function prepare()
    {
        $this->order->setData('erp_nfe_key', $this->tracking->notaFiscal->chaveAcesso);
        $this->order->setData('erp_nfe_link', $this->tracking->notaFiscal->linkNfe);

        /**
         * Set status
         */
        $this->order->setStatus(self::DEFAULT_STATUS);
        
        return $this;
    }
}
