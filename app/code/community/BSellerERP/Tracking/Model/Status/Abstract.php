<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Tracking
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Status schema model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Tracking
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
abstract class BSellerERP_Tracking_Model_Status_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * Returns helper object
     *
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper data
     *
     * @var string
     */
    protected $currentHelper = 'tracking';

    /**
     * Current order
     *
     * @var Mage_Sales_Model_Order
     */
    protected $order;

    /**
     * Current tracking
     *
     * @var object
     */
    protected $tracking;

    /**
     * Load helper model and get current store
     */
    public function __construct()
    {
        parent::__construct();

        $this->helper = Mage::helper('bsellererp_' . $this->currentHelper);
    }

    /**
     * Set order for integration status
     *
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Set ERP tracking
     *
     * @param object $tracking
     * @return $this
     */
    public function setTracking($tracking)
    {
        $this->tracking = $tracking;

        return $this;
    }

    /**
     * Initialize status import
     *
     * @return Mage_Core_Model_Abstract
     * @throws Exception
     */
    public function init()
    {
        $this->prepare();
        $this->addDefaultComment();

        /**
         * Save order
         */
        return $this->order->save();
    }

    public function addDefaultComment()
    {
        $description = $this->tracking->ponto->descricao;
        $message     = $this->helper->__('The ERP status update: %s', $description);

        $this->order->addStatusHistoryComment($message);

        return $this;
    }

    /**
     * Prepare order
     *
     * @return mixed
     */
    abstract protected function prepare();
}
