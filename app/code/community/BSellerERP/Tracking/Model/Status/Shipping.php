    <?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Tracking
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Status model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Tracking
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Tracking_Model_Status_Shipping extends BSellerERP_Tracking_Model_Status_Abstract
{
    const DEFAULT_STATUS = 'shipping';

    /**
     * Initialize status import
     */
    public function prepare()
    {
        try {
            $this->createShipment();
        } catch (Exception $e) {
            $this->helper->log($e->getMessage());
        }

        return $this;
    }

    /**
     * Create shipment, trackings and set status
     *
     * @return $this
     * @throws Exception
     * @throws bool
     */
    protected function createShipment()
    {
        $trackingCodes = $this->tracking->codigosRastreio;

        if (!$this->order->canShip() && !count($trackingCodes)) {
            $this->helper->log('Can\'t create shipment, no has tracking code or order not allowed');

            return $this;
        }

        $items = array();

        foreach ($this->order->getAllItems() as $item) {
            $items[$item->getId()] = $item->getQtyOrdered();
        }

        $shipment = Mage::getModel('sales/service_order', $this->order)->prepareShipment($items);

        /**
         * Create tracking codes
         */
        foreach ($trackingCodes as $trackingCode) {
            $dataTracking = array(
                'carrier_code' => 'custom',
                'title'        => $this->order->getShippingDescription(),
                'number'       => $trackingCode
            );

            /** @var Mage_Sales_Model_Order_Shipment_Track $track */
            $track = Mage::getModel('sales/order_shipment_track')->addData($dataTracking);
            $shipment->addTrack($track);
        }

        $shipment->setEmailSent(true);
        $shipment->sendEmail(true);
        $shipment->getOrder()->setCustomerNoteNotify(true);
        $shipment->register();
        $shipment->getOrder()->setIsInProcess(true);

        Mage::getModel('core/resource_transaction')
            ->addObject($shipment)
            ->addObject($shipment->getOrder())
            ->save();

        /**
         * Set status
         */
        $this->order->setStatus(self::DEFAULT_STATUS);

        return $this;
    }
}
