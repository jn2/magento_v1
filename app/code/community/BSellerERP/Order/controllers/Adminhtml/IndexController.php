<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

class BSellerERP_Order_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Resend order
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    public function resendAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');

        if (!$orderId) {
            $this->_getSession()->addError($this->_getHelper()->__('It was not possible to integrate the order'));

            return $this->_redirectReferer();
        }

        $order = Mage::getModel('sales/order')->load($orderId);
        $order->setData('erp_status', 'pending');
        $order->save();

        $this->_getSession()->addSuccess($this->_getHelper()->__('Order sent for queue of integration'));

        return $this->_redirectReferer();
    }

    /**
     * Resend payment
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    public function resendPaymentAction()
    {
        $invoiceId = $this->getRequest()->getParam('invoice_id');

        if (!$invoiceId) {
            $this->_getSession()->addError($this->_getHelper()->__('It was not possible to approve payment'));

            return $this->_redirectReferer();
        }

        $invoice = Mage::getModel('sales/order_invoice')->load($invoiceId);

        if (!$invoice->getId()) {
            $this->_getSession()->addError($this->_getHelper()->__('Invoice not exist'));

            return $this->_redirectReferer();
        }

        Mage::getModel('bsellererp_order/payment_approve_export')
            ->setOrder($invoice->getOrder())
            ->init();

        $this->_getSession()->addSuccess($this->_getHelper()->__('Request sent, please see comments in order'));

        $urlRedirect = Mage::helper('adminhtml')
            ->getUrl('adminhtml/sales_order/view', array('order_id' => $invoice->getOrderId()));

        return $this->_redirectUrl($urlRedirect);
    }

    public function resendMassAction() {
        $orderIds = $this->getRequest()->getPost('order_ids', array());

        $maxOrdersToUpdate = 20;
        if(count($orderIds) > $maxOrdersToUpdate) {
            $this->_getSession()->addError($this->_getHelper()->__('Only %s items are elegible to be resend to ERP.', $maxOrdersToUpdate));
            return $this->_redirect('adminhtml/sales_order');
        }

        foreach ($orderIds as $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            $order->setData('erp_status', 'pending');
            $order->save();
        }
        $this->_getSession()->addSuccess($this->_getHelper()->__('All selected items was sent for queue of integration.'));
        $this->_redirect('adminhtml/sales_order');
    }

    public function resendPaymentMassAction()
    {
        $orderIds = $this->getRequest()->getPost('order_ids', array());

        $maxOrdersToUpdate = 20;
        if (count($orderIds) > $maxOrdersToUpdate) {
            $this->_getSession()->addError($this->_getHelper()->__('Only %s items are elegible to be resend to ERP.', $maxOrdersToUpdate));
            return $this->_redirect('*/*/');
        }

        foreach ($orderIds as $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);

            if ($order->getErpStatus() == BSellerERP_Order_Helper_Status::BLOCKED
                || $order->getErpStatus() == BSellerERP_Order_Helper_Status::BLOCKED_APPROVE
                || $order->getErpStatus() == BSellerERP_Order_Helper_Status::BLOCKED_DISAPPROVE
            ) {
                $order->setData('erp_status', BSellerERP_Order_Helper_Status::SUCCESS);
                $order->save();
            }
        }
        $this->_getSession()->addSuccess($this->_getHelper()->__('All selected items was sent for queue of integration.'));
        $this->_redirect('adminhtml/sales_order');
    }

    /**
     * Is allowed to access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
       return true;
    }

}
