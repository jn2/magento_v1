<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller status helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Helper_Status extends BSellerERP_Order_Helper_Data
{
    /**
     * Order created
     *
     * @const string
     */
    const PENDING = 'pending';

    /**
     * Problems in initial integration
     *
     * @const string
     */
    const BLOCKED = 'blocked';

    /**
     * Problems in payment approving
     *
     * @const string
     */
    const BLOCKED_APPROVE = 'blocked_payment_approve';

    /**
     * Success in payment approving
     *
     * @const string
     */
    const SUCCESS_APPROVE = 'success_payment_approve';

    /**
     * Problems in payment disapproving
     *
     * @const string
     */
    const BLOCKED_DISAPPROVE = 'blocked_payment_disapprove';

    /**
     * Order integrated success
     *
     * @const string
     */
    const SUCCESS = 'success';
}
