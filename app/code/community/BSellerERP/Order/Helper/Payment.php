<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller order module payment helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Helper_Payment extends BSellerERP_Order_Helper_Data
{
    /**
     * Return payment information data
     *
     * @param Mage_Sales_Model_Order_Payment $paymentMethod
     * @return bool|string
     */
    public function getPaymentType($paymentMethod)
    {
        switch ($paymentMethod->getMethod()) {
            case $this->getPaymentCodeDefault('credit_card'):
                return 'creditCard';

                break;
            case $this->getPaymentCodeDefault('debit'):
                return 'debit';

                break;
            case $this->getPaymentCodeDefault('billet_bank'):
                return 'billetBank';

                break;
        }

        return false;
    }

    /**
     * Get default payment code by type (Credit Card, Debit or Billet Bank)
     *
     * @param string $type
     * @return bool|string
     */
    public function getPaymentCodeDefault($type)
    {
        if ($code = Mage::getStoreConfig('bsellererp_order/payment/' . $type)) {
            return $code;
        }

        return false;
    }
}
