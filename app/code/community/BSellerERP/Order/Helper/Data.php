<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller order module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Helper_Data extends BSellerERP_Core_Helper_Data
{
    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'order';

    /**
     * Get order settings store config by key
     *
     * @param $key
     * @return mixed
     */
    public function getOrderStoreConfig($key)
    {
        return Mage::getStoreConfig('bsellererp_order/settings/' . $key);
    }

    function getErpStatusFilterOptions()
    {
        $select = array(
            array('label' => '', 'value' => ''),
            array('label' => '[NI] Aguardando integração', 'value' => BSellerERP_Order_Helper_Status::PENDING),
            array('label' => '[I] Integrado', 'value' => BSellerERP_Order_Helper_Status::SUCCESS),
            array('label' => '[I] Finalizado', 'value' => BSellerERP_Order_Helper_Status::SUCCESS_APPROVE),
            array('label' => '[NI] Bloqueado', 'value' => BSellerERP_Order_Helper_Status::BLOCKED),
            array('label' => '[I] Aprovação de pagamento não enviada', 'value' => BSellerERP_Order_Helper_Status::BLOCKED_APPROVE),
            array('label' => '[I] Desaprovação de pagamento não enviada', 'value' => BSellerERP_Order_Helper_Status::BLOCKED_DISAPPROVE)
        );

        return $select;
    }

    function getErpStatusLabel($value)
    {
        $array = $this->getErpStatusFilterOptions();

        foreach ($array as $statusItem) {
            if ($statusItem['value'] == $value) {
                return $statusItem['label'];
            }
        }

        return $value;
    }
}
