<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Observer model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Observer
{
    /**
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_order');
    }

    /**
     * Starting integrate order
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function salesOrderPlaceBefore(Varien_Event_Observer $observer)
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        /**
         * @var Mage_Sales_Model_Order $order
         */
        $order = $observer->getEvent()->getOrder();
        $order->setData('erp_status', BSellerERP_Order_Helper_Status::PENDING);

        return $this;
    }

    /**
     * Add button for resend integration in order view
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function addIntegrationButtonOrder(Varien_Event_Observer $observer)
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        $block = $observer->getBlock();

        if (!$block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            return $this;
        }

        if ($block->getOrder()->getData('erp_status') != BSellerERP_Order_Helper_Status::BLOCKED) {
            return $this;
        }

        $message = $this->helper->__('Are you sure you want to do this?');
        $url     = $block->getUrl('bsellererp_order/adminhtml_index/resend');

        $block->addButton(
            'resend_order_integration',
            array(
                'label'     => $this->helper->__('Resend Integration'),
                'onclick'   => "confirmSetLocation('{$message}', '{$url}')"
            )
        );

        return $this;
    }

    //Adding mass action options to sales_grid page
    //Adding erp_status column to sales_grid page
    public function beforeBlockToHtml(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();

        if (Mage::getStoreConfig('bsellererp_order/settings/order_grid_mass_actions')) {
            if ($block instanceOf Mage_Adminhtml_Block_Widget_Grid_Massaction
                && $block->getRequest()->getControllerName() == 'sales_order'
            ) {
                $block->addItem('bselererp_order_resendorders', array(
                    'label' => $this->helper->__('[BsellerERP] Resend orders'),
                    'url' => Mage::app()->getStore()->getUrl('bsellererp_order/adminhtml_index/resendmass'),
                ));

                $block->addItem('bselererp_order_resendpayments', array(
                    'label' => $this->helper->__('[BsellerERP] Resend payments'),
                    'url' => Mage::app()->getStore()->getUrl('bsellererp_order/adminhtml_index/resendpaymentmass'),
                ));
            }
        }

        if (Mage::getStoreConfig('bsellererp_order/settings/order_grid_add_statuserp_column')) {
            if ($block instanceof Mage_Adminhtml_Block_Sales_Order_Grid) {
                $block->addColumnAfter(
                    'erp_status',
                    array(
                        'header' => Mage::helper('bsellererp_order')->__('ERP Status'),
                        'index' => 'sfo.erp_status',
                        'filter' => 'bsellererp_order/adminhtml_sales_order_filter_erpstatuscolumn',
                        'renderer' => 'bsellererp_order/adminhtml_sales_order_grid_erpstatuscolumn',
                        'type' => 'text'
                    ),
                    'status'
                );
            }
        }
    }

    public function beforeCollectionLoad(Varien_Event_Observer $observer)
    {
        if(!Mage::getStoreConfig('bsellererp_order/settings/order_grid_add_statuserp_column')) {
            return;
        }

        $collection = $observer->getOrderGridCollection();

        if (!isset($collection)) {
            return;
        }

        if ($collection instanceof Mage_Sales_Model_Resource_Order_Grid_Collection) {
            $aliases = $collection->getSelect()->getPart('columns');

            foreach($aliases as $aliase) {
                if(isset($aliase['0']) && ($aliase['0'] == 'sfo')) {
                    return;
                }
            }

            $subselect = new Zend_Db_Expr('(select entity_id, erp_status from sales_flat_order)');
            $collection->getSelect()
                ->joinLeft(array('sfo' => $subselect), "main_table.entity_id = sfo.entity_id", array("erp_status" => "sfo.erp_status"));
        }
    }
}
