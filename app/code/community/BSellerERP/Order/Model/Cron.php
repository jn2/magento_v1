<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Cron schedule model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Cron
{
    /**
     * Returns helper object
     * 
     * @var BSellerERP_Order_Helper_Data
     */
    public $helper;

    /**
     * Initialize helper and factory model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_order');
    }

    /**
     * Init import orders
     *
     * @return $this
     */
    public function export()
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        /**
         * Get all pending orders
         */
        $orders = Mage::getResourceModel('sales/order_collection')->addFieldToFilter('erp_status', 'pending');
        
        try {
            /**
             * Prepare schema and init integration for all orders
             */
            foreach ($orders as $order) {
                try {
                    $export = Mage::getModel('bsellererp_order/export');
                    $export->setOrder($order->getIncrementId());
                    $export->init();
                } catch (Exception $e) {
                    $this->helper->log($e->getMessage());
                }
            }
            $this->helper->log('Integration order success');
        } catch (Exception $e) {
            $this->helper->log($e->getMessage());
        }

        return $this;
    }
}
