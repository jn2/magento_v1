<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Schema model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Schema extends BSellerERP_Core_Model_Schema_Abstract
{
    /**
     * Available product type
     */
    const TYPE_DOWNLOADABLE = 'downloadable';

    /**
     * Gift card product type
     */
    const TYPE_GIFTCARD = 'giftcard';

    /**
     * Product virtual type ERP
     */
    const ITEM_TYPE_VIRTUAL = 'N';

    /**
     * Product simple/configurable type ERP
     */
    const ITEM_TYPE_PRODUCT = 'P';

    /**
     * Content returned integration to import
     *
     * @var Mage_Sales_Model_Order
     */
    protected $order;

    /**
     * Customer by order
     *
     * @var Mage_Customer_Model_Customer
     */
    protected $customer;

    /**
     * Returns helper object
     *
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'order';

    /**
     * Load helper data
     *
     * @var float
     */
    public $deductionAmount;

    /**
     * Prepare schema and returns for adding in order instance
     *
     * @return array
     */
    protected function prepareSchema()
    {
        parent::prepareSchema();

        if (!$this->order->getId()) {
            Mage::throwException('Order not exist for create schema');
        }

        $this->customer = Mage::getModel('customer/customer')->load($this->order->getCustomerId());

        $this->setGeneralData();
        $this->setCustomerAddressData($this->order->getBillingAddress());
        $this->setCustomerAddressData($this->order->getShippingAddress() ? $this->order->getShippingAddress() : $this->order->getBillingAddress(), 'shipping');
        $this->setItemsData();
        $this->setShippingData();
        $this->setPaymentData();
        $this->setTotalsData();

        return $this->getSchema();
    }

    /**
     * Setting general data for schema
     *
     * @return $this
     */
    protected function setGeneralData()
    {
        $incrementId = $this->order->getIncrementId();

        $customerHelper = Mage::helper('bsellererp_core/customer');
        $customerHelper->setCustomer($this->customer);

        $this->addData(
            array(
                'canalVenda'            => $this->helper->getOrderStoreConfig('sales_channel'),
                'codigoVendedor'        => $this->order->getData('sac_operator'),
                'dataEmissao'           => $this->helper->formatDate($this->order->getCreatedAt()),
                'dataInclusao'          => $this->helper->formatDate($this->order->getCreatedAt()),
                'numeroPedido'          => $incrementId,
                'numeroPedidoExterno'   => $incrementId,
                'numeroPedidoLoja'      => $incrementId,
                'origemPedido'          => $this->helper->getOrderStoreConfig('sale_origin'),
                'pedidoParaConsumo'     => (bool) $this->helper->getOrderStoreConfig('consumption'),
                'tipoPedido'            => $customerHelper->getTipoPedido(),
                'entregasQuebradas'     => false,
                'observacoesEtiqueta'   => null,
                'tipoFrete'             => 'C',
                'unidadeNegocio'        => $this->helper->getSettingStoreConfig('business_unit'),
            )
        );

        return $this;
    }

    /**
     * Setting customer address by type for schema
     *
     * @param Mage_Sales_Model_Order_Address $address
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function setCustomerAddressData(Mage_Sales_Model_Order_Address $address, $addressType = 'billing')
    {
        if (!$address) {
            Mage::throwException('Address not exist for create schema - Order: ' . $this->order->getIncrementId());
            return $this;
        }

        $customerHelper = Mage::helper('bsellererp_core/customer');
        $customerHelper->setCustomer($this->customer);
        $customerHelper->setAddress($address);

        $formatAddress = $customerHelper->formatAddress(
            $this->helper->getStreetByName('address_number', $address),
            $this->helper->getStreetByName('address_street', $address),
            $this->helper->getStreetByName('address_complement', $address)
        );

        $addressInfo = array(
            'bairro'            => $this->helper->truncate($this->helper->getStreetByName('address_neighborhood', $address), 50),
            'logradouro'        => $this->helper->truncate($formatAddress['street'], 60),
            'numero'            => $formatAddress['number'],
            'complemento'       => $this->helper->truncate($formatAddress['complement'], 50),
            'pontoReferencia'   => $this->helper->truncate($this->helper->getPredefinedAttributeValue('reference_point', $address), 100),
            'cep'               => $this->helper->onlyNumbers($address->getPostcode()),
            'cidade'            => $address->getCity(),
            'estado'            => $address->getRegionCode(),
            'pais'              => Mage::app()->getLocale()->getCountryTranslation($address->getCountry()),
            'zipCode'           => null,
        );

        $cellphone = $this->helper->getPredefinedAttributeValue('cellphone', $address);

        $generalInfo = array(
            'classificacao'         => 0,
            'dataNascimento'        => $this->helper->formatDate($this->order->getCustomerDob()),
            'email'                 => $address->getEmail(),
            'endereco'              => $addressInfo,
            'fax'                   => $address->getFax(),
            'nome'                  => $this->helper->truncate($customerHelper->getName(), 40),
            'rg'                    => $this->helper->getPredefinedAttributeValue('rg', $this->customer),
            'telefoneResidencial'   => $this->helper->onlyNumbers($address->getTelephone()),
            'telefoneCelular'       => $this->helper->onlyNumbers($cellphone),
            'telefoneComercial'     => null,
            'id'                    => $customerHelper->getDocumentValue()
        );

        /**
         * Append person type info
         */
        $generalInfo = array_merge($generalInfo, $customerHelper->preparePersonType());

        /**
         * Configure type node customer address
         */
        $node = 'clienteFaturamento';
        if ($addressType == 'shipping') {
            $node = 'clienteEntrega';
        }

        $this->addData(
            array(
                $node => $generalInfo
            )
        );

        return $this;
    }

    /**
     * Setting items data for schema
     *
     * @return $this
     */
    protected function setItemsData()
    {
        $orderItems     = $this->order->getAllVisibleItems();
        $idCarrier      = Mage::helper('bsellererp_shipping')->getCarrierCodeByShipping($this->order->getShippingMethod());
        $carrier        = Mage::getModel('bsellererp_shipping/carrier_items')->load($idCarrier, 'carrier_id');
        $items          = array();
        $shippingAmount = $this->order->getShippingAmount();
        $amountItem     = $this->roundDown(($shippingAmount / count($orderItems)), 2);
        $transitTime    = $this->helper->onlyNumbers($this->order->getShippingDescription());

        /**
         * Set default transit time
         */
        if (!$transitTime) {
            $transitTime = (int) $this->helper->getOrderStoreConfig('transit_time');
        }

        /**
         * @var Mage_Sales_Model_Order_Item $orderItem
         */
        $totalItemsAmountDiscount = 0;
        $totalItemsQty = 0;
        
        foreach ($orderItems as $key => $orderItem) {
            $qtyOrdered         = $orderItem->getQtyOrdered() * 1;
            $discountAmount     = (float) number_format($orderItem->getDiscountAmount(), 2);
            $discountAmountItem = $discountAmount;

            $totalItemsAmountDiscount += $discountAmountItem;
            $totalItemsQty += $qtyOrdered;

            /**
             * Calculate discount amount per item
             */
            if ($discountAmount && $qtyOrdered > 1) {
                $calcDiscount = $this->roundDown($discountAmount/$qtyOrdered, 2);
                $discountAmountItem = $calcDiscount;
                if (($calcDiscount * $qtyOrdered) != $discountAmount) {
                    $discountAmountItem = $this->roundDown($discountAmountItem, 2);
                    $discountDiff = round($discountAmount - ($discountAmountItem * $qtyOrdered), 2);

                    /**
                     * Increase deduction value
                     */
                    if ($discountDiff > 0) {
                        $this->deductionAmount += $discountDiff;
                    }
                }
            }

            $carrierContractId = null;

            if($carrier->getData('carrier_id')) {
                if (Mage::getStoreConfig('bsellererp_shipping/carrier/use_default_carrier_codes')) {
                    $carrierContractId = substr($this->order->getShippingMethod(), (strrpos($this->order->getShippingMethod(), '_') ?: -1) + 1);
                } else {
                    $carrierContractId = $carrier->getData('contract_id');
                }
            }

            $items[] = array(
                'cnpjFilial'                    => $this->helper->getSettingStoreConfig('cnpj_filial'),
                'codigoEstabelecimentoSaida'    => $this->helper->getSettingStoreConfig('output_establishment'),
                'codigoEstabelecimentoEstoque'  => $this->helper->getSettingStoreConfig('stock_establishment'),
                'descontoCondicionalUnitario'   => 0,
                'descontoIncondicionalUnitario' => $discountAmountItem,
                'prazoCentroDistribuicao'       => (int) $this->helper->getOrderStoreConfig('lead_time'),
                'prazoFornecedor'               => 0,
                'prazoTransitTime'              => $transitTime,
                'idContratoTransportadora'      => $carrierContractId,
                'idTransportadora'              => $carrier->getData('carrier_id'),
                'sequencial'                    => $key,
                'valorFrete'                    => $amountItem,
                'codigoItem'                    => $orderItem->getSku(),
                'tipoItem'                      => $this->getItemType($orderItem),
                'quantidade'                    => $qtyOrdered,
                'precoUnitario'                 => number_format($orderItem->getPrice(), 2),
                'valorDespesas'                 => 0
            );
        }

        if($totalItemsAmountDiscount != $this->order->getDiscountAmount()) {
            $this->deductionAmount += (float) number_format((($this->order->getDiscountAmount()*-1) - $totalItemsAmountDiscount), 2);
        }

        /**
         * Calculate shipping amount and correct prices
         */
        $totalAmount = count($orderItems) * $amountItem;
        $calculation = $shippingAmount - $totalAmount;

        if ($calculation) {
            $items[0]['valorFrete'] = $items[0]['valorFrete'] + $calculation;
        }

        $this->addData(
            array(
                'itens' => $items
            )
        );

        return $this;
    }

    /**
     * Round price
     *
     * @param  $decimal
     * @param  $precision
     * @return float
     */
    public function roundDown($decimal, $precision)
    {
        $fraction = substr($decimal - floor($decimal), 2, $precision);
        $newDecimal = floor($decimal). '.' .$fraction;

        return floatval($newDecimal);
    }

    /**
     * Setting shipping method data for schema
     *
     * @return $this
     */
    protected function setShippingData()
    {
        $this->addData(
            array(
                'entrega' => array(
                    'dataEntregaAgendada'    => null,
                    'periodoEntregaAgendada' => null,
                    'tipoEntrega'            => 1,
                )
            )
        );

        return $this;
    }

    /**
     * Setting payment data for schema
     *
     * @return $this
     */
    protected function setPaymentData()
    {
        $paymentData   = new Varien_Object();
        $paymentHelper = Mage::helper('bsellererp_payment');
        $address       = $this->order->getBillingAddress();

        $formatAddress = Mage::helper('bsellererp_core/customer')->formatAddress(
            $this->helper->getStreetByName('address_number', $address),
            $this->helper->getStreetByName('address_street', $address),
            $this->helper->getStreetByName('address_complement', $address)
        );

        $generalData = array(
            'endereco' => array(
                'bairro'            => $this->helper->truncate($this->helper->getStreetByName('address_neighborhood', $address), 20),
                'logradouro'        => $this->helper->truncate($formatAddress['street'], 60),
                'numero'            => $formatAddress['number'],
                'complemento'       => $this->helper->truncate($formatAddress['complement'], 50),
                'pontoReferencia'   => $this->helper->truncate($this->helper->getPredefinedAttributeValue('reference_point', $address), 100),
                'cep'               => $this->helper->onlyNumbers($address->getPostcode()),
                'cidade'            => $address->getCity(),
                'estado'            => $address->getRegionCode(),
                'pais'              => Mage::app()->getLocale()->getCountryTranslation($address->getCountry()),
                'zipCode'           => null,
            ),

            'valor'               => number_format($this->order->getGrandTotal() + $this->deductionAmount, 2, '.', ''),
            'sequencial'          => 0,
            'codigoMeioPagamento' => $paymentHelper->getPaymentByCode($this->order->getPayment()->getMethod()),
            'codigoCondicaoPagamento' => $paymentHelper->getPaymentCondition($this->order),
        );

        /**
         * Add general data
         */
        $paymentData->addData($generalData);

        Mage::dispatchEvent(
            $this->helper->getPrefix() . '_payment',
            array(
                'payment' => $paymentData,
                'order'   => $this->order
            )
        );

        $this->addData(
            array(
                'pagamentos' => array(
                    $paymentData->toArray()
                )
            )
        );

        return $this;
    }

    /**
     * Setting totals data for schema
     *
     * @return $this
     */
    protected function setTotalsData()
    {
        $totals = array(
            'valorDespesasFinanceiras' => 0.0,
            'valorDespesasItem'        => 0.0,
            'valorFrete'               => number_format($this->order->getBaseShippingAmount(), 2),
            'valorTotalProdutos'       => number_format($this->order->getSubtotal(), 2, '.', '')
        );

        $this->addData(
            array(
                'valores' => $totals
            )
        );

        return $this;
    }

    /**
     * Returns item type
     *
     * @param Mage_Sales_Model_Order_Item $item
     * @return string
     */
    protected function getItemType($item)
    {
        switch ($item->getProductType()) {
            case Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL:
            case self::TYPE_GIFTCARD:
            case self::TYPE_DOWNLOADABLE:
                return self::ITEM_TYPE_VIRTUAL;

                break;

            default:
                return self::ITEM_TYPE_PRODUCT;

                break;
        }
    }
}
