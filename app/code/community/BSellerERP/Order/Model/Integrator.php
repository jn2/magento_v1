<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Integrator category rest
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Integrator extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Request Method
     * @var array
     */
    public $requestMethod = Zend_Http_Client::POST;

    /**
     * Service path order api
     * @var string
     */
    public $servicePath = 'pedidos';

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'order';

    /**
     * Set order
     *
     * @var Mage_Sales_Model_Order
     */
    public $order;

    /**
     * Set parameters and send request
     *
     * @return $this
     */
    public function init()
    {
        $errors = $this->_validateSchema();
        if (!empty($errors)) {
            Mage::throwException(json_encode($errors));
        }
        $this->setBodyParam($this->schema);
        return parent::init();
    }

    /**
     * Set order
     *
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function setOrder($order)
    {
        if (!$order) {
            return $this;
        }

        $this->order = $order;

        return $this;
    }

    /**
     * Flag integration as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $this->helper->log('Success integration order');
        $this->helper->log($this->response->getBody());

        $this->order->addStatusHistoryComment($this->response->getBody());
        $this->order->setData('erp_status', BSellerERP_Order_Helper_Status::SUCCESS);
        $this->order->save();

        return $this;
    }

    /**
     * Flag integration as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log($this->response->getBody());
        
        $this->order->addStatusHistoryComment($this->response->getBody());
        $this->order->setData('erp_status', BSellerERP_Order_Helper_Status::BLOCKED);
        $this->order->save();

        return $this;
    }

    private function _validateSchema()
    {
        $return = array();

        if (!$this->schema) {
            $return[] = 'The schema has no data';
        } else {
            $orderId = isset($this->schema['numeroPedido']) ? ('#' . $this->schema['numeroPedido'] . ' ') : '';

            if (!isset($this->schema['clienteFaturamento'])) {
                $return[] = $orderId . 'The clienteFaturamento has no data';
            } else {
                if (!isset($this->schema['clienteFaturamento']['id']) || !$this->schema['clienteFaturamento']['id']) {
                    $return[] = $orderId . 'The clienteFaturamento.id has no data';
                }

                if (!isset($this->schema['clienteFaturamento']['nome']) || !$this->schema['clienteFaturamento']['nome']) {
                    $return[] = $orderId . 'The clienteFaturamento.name has no data';
                }
            }
        }
        return $return;
    }
}
