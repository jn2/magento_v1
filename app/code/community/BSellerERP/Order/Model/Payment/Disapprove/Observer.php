<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Observer model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Payment_Disapprove_Observer
{
    /**
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_order');
    }

    /**
     * Sending payment disapproved for ERP
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function orderCancelAfter(Varien_Event_Observer $observer)
    {
        if (!$this->helper->getOrderStoreConfig('cancel_active')) {
            return $this;
        }

        /**
         * @var Mage_Sales_Model_Order $order
         */
        $order = $observer->getEvent()->getData('order');

        $blockedStatus = array(
            BSellerERP_Order_Helper_Status::PENDING,
            BSellerERP_Order_Helper_Status::BLOCKED
        );

        if (in_array($order->getData('erp_status'), $blockedStatus)) {
            return $this;
        }

        $this->helper->log('Starting disapprove payment for to order: ' . $order->getIncrementId());

        Mage::getModel('bsellererp_order/payment_disapprove_export')
            ->setOrder($order)
            ->init();

        return $this;
    }
}
