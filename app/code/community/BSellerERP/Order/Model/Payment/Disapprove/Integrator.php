<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Integrator category rest
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Payment_Disapprove_Integrator extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Request Method
     * @var array
     */
    public $requestMethod = Zend_Http_Client::POST;

    /**
     * Service path order api
     * @var string
     */
    public $servicePath = 'pedidos/pagamentos/reprovar';

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'order';

    /**
     * Set order
     *
     * @var Mage_Sales_Model_Order
     */
    public $order;

    /**
     * Set parameters and send request
     *
     * @return $this
     */
    public function init()
    {
        if (!count($this->schema)) {
            Mage::throwException('The invoice schema has no data');
        }

        $this->setBodyParam($this->schema);

        return parent::init();
    }

    /**
     * Set order
     *
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function setOrder($order)
    {
        if (!$order) {
            return $this;
        }

        $this->order = $order;

        return $this;
    }

    /**
     * Flag integration as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $this->helper->log('Success disapprove payment to order');
        $this->helper->log($this->response->getBody());

        $this->order->addStatusHistoryComment('[Disapprove Payment] ' . $this->response->getBody());
        $this->order->save();

        return $this;
    }

    /**
     * Flag integration as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log($this->response->getBody());

        $this->order->addStatusHistoryComment('[Disapprove Payment] ' . $this->response->getBody());
        $this->order->setData('erp_status', BSellerERP_Order_Helper_Status::BLOCKED_DISAPPROVE);
        $this->order->save();

        Mage::throwException('Unable to disapprove payment in ERP');
    }
}
