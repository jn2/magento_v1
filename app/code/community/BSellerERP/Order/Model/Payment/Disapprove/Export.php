<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Export order model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Payment_Disapprove_Export extends BSellerERP_Order_Model_Payment_Disapprove_Schema
{
    /**
     * Init export order
     *
     * @return $this
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_disapprove_init', array('order' => $this->order));

        /**
         * Init integration invoice
         */
        Mage::getModel('bsellererp_order/payment_disapprove_integrator')
            ->setSchema($this->prepareSchema())
            ->setOrder($this->order)
            ->init();

        Mage::dispatchEvent($this->helper->getPrefix() . '_disapprove_complete', array('order' => $this->order));

        return $this;
    }

    /**
     * Set data invoice
     *
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;

        /**
         * Set current store by order
         */
        if ($storeCode = $this->order->getStore()->getCode()) {
            Mage::app()->setCurrentStore($storeCode);
        }

        return $this;
    }
}
