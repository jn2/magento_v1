<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Observer model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Payment_Methods_Observer
{
    /**
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper model
     */
    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_order');
    }

    /**
     * Add payment data for Adyen
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function addPaymentAdyenData(Varien_Event_Observer $observer)
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        /**
         * @var Varien_Object $payment
         */
        $payment = $observer->getEvent()->getPayment();

        /**
         * @var Mage_Sales_Model_Order $order
         */
        $order = $observer->getEvent()->getOrder();

        $allowedPayments = array(
            BSellerERP_Order_Model_Payment_Methods_Adyen::PAYMENT_CODE_CREDIT_CARD,
            BSellerERP_Order_Model_Payment_Methods_Adyen::PAYMENT_CODE_ON_CLICK,
            BSellerERP_Order_Model_Payment_Methods_Adyen::PAYMENT_CODE_BILLET_BANK,
        );

        if (!in_array($order->getPayment()->getMethod() ,$allowedPayments)) {
            return $this;
        }

        $adyen = Mage::getModel('bsellererp_order/payment_methods_adyen');
        $adyen->setPayment($payment);
        $adyen->setOrder($order);

        $payment->addData($adyen->getSchema());

        return $this;
    }

    /**
     * Add payment data for Billet Term
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function addPaymentBilletTermData(Varien_Event_Observer $observer)
    {
        if (!$this->helper->moduleIsActive()) {
            return $this;
        }

        /**
         * @var Varien_Object $payment
         */
        $payment = $observer->getEvent()->getPayment();

        /**
         * @var Mage_Sales_Model_Order $order
         */
        $order = $observer->getEvent()->getOrder();

        if ($order->getPayment()->getMethod() != 'bseller_billet') {
            return $this;
        }

        $billet = Mage::getModel('bsellererp_order/payment_methods_billetTerm');

        $payment->addData($billet->getSchema());

        return $this;
    }
}
