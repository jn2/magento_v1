<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Adyen payment model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Payment_Methods_Adyen extends Varien_Object
{
    /**
     * Payment code for credit card method
     *
     * @const string
     */
    const PAYMENT_CODE_CREDIT_CARD = 'adyen_cc';

    /**
     * Payment code for onclick method
     *
     * @const string
     */
    const PAYMENT_CODE_ON_CLICK = 'adyen_oneclick';

    /**
     * Payment code for billet bank method
     *
     * @const string
     */
    const PAYMENT_CODE_BILLET_BANK = 'adyen_boleto';

    /**
     * Default value for situation CVV
     *
     * @const int
     */
    const DEFAULT_SITUATION_CVV = 1;

    /**
     * Returns helper object
     *
     * @var BSellerERP_Core_Helper_Data
     */
    protected $helper;

    /**
     * Payment data
     *
     * @var object
     */
    public $payment;

    /**
     * Order
     *
     * @var Mage_Sales_Model_Order
     */
    public $order;

    /**
     * Po Number (Payment Info)
     *
     * @var array
     */
    public $poNumber;

    /**
     * Order payment info
     *
     * @var Mage_Sales_Model_Order_Payment
     */
    public $orderPayment;

    /**
     * Customer by order
     *
     * @var Mage_Customer_Model_Customer
     */
    public $customer;

    /**
     * Load helper model and get current store
     */
    public function __construct()
    {
        parent::__construct();

        $this->helper = Mage::helper('bsellererp_order');
    }

    /**
     * Starting payment schema
     *
     * @return array
     */
    public function init()
    {
        $poNumber = $this->order->getPayment()->getData('po_number');

        if (!is_numeric($poNumber)) {
            $this->poNumber = unserialize($poNumber);
        }

        $this->orderPayment = $this->order->getPayment();
        $this->customer     = Mage::getModel('customer/customer')->load($this->order->getCustomerId());
            
        switch ($this->order->getPayment()->getMethod()) {
            case self::PAYMENT_CODE_CREDIT_CARD:
            case self::PAYMENT_CODE_ON_CLICK:
                $this->addCreditCardData();

                break;
            case self::PAYMENT_CODE_BILLET_BANK:
                $this->addBilletData();

                break;
        }
        
        return $this;
    }

    /**
     * Adding information credit card payment
     *
     * @return Varien_Object
     */
    public function addCreditCardData()
    {
        $brand = Mage::helper('bsellererp_payment_brands')
            ->getBrandCode($this->orderPayment->getCcType());

        $customerHelper = Mage::helper('bsellererp_core/customer')->setCustomer($this->customer);

        $creditCard = array(
            'cartaoCredito' => array(
                'bandeira'                  => $brand,
                'cpfTitular'                => $customerHelper->getDocumentValue(),
                'dataVencimento'            => $this->orderPayment->getCcExpMonth() . '/' . $this->orderPayment->getCcExpYear(),
                'numero'                    => $this->orderPayment->getCcLast4(),
                'numeroParcelas'            => $this->orderPayment->getAdditionalInformation('number_of_installments'),
                'primeiros6digitos'         => $this->orderPayment->getCcLast4(),
                'situacaoCodigoSeguranca'   => self::DEFAULT_SITUATION_CVV,
                'titular'                   => substr($this->orderPayment->getCcOwner(), 0, 30),
                'valorJuros'                => 0,
                'valorJurosAdministradora'  => 0,
                'percentualJuros'           => 0,
            ),
        );

        return $this->addData($creditCard);
    }

    /**
     * Adding information billet payment
     *
     * @return Varien_Object
     */
    public function addBilletData()
    {
        $deliveryDate = $this->helper->formatDate(now());

        if (isset($this->poNumber['delivery_date'])) {
            $deliveryDate = $this->helper->formatDate($this->poNumber['delivery_date']);
        }

        $data = array(
            'codigoBanco'           => $this->helper->getSettingStoreConfig('bank_code'),
            'codigoAgencia'         => $this->helper->getSettingStoreConfig('number_agency'),
            'numeroConta'           => $this->helper->getSettingStoreConfig('number_account'),
            'dataVencimentoBoleto'  => $deliveryDate
        );

        return $this->addData($data);
    }

    /**
     * Returns schema
     *
     * @return array
     */
    public function getSchema()
    {
        $this->init();

        return $this->toArray();
    }

    /**
     * Set payment object
     *
     * @param $paymentData
     * @return Mage_Core_Model_Store
     */
    public function setPayment($paymentData)
    {
        $this->payment = $paymentData;

        return $this->payment;
    }

    /**
     * Set order
     *
     * @param  Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Order
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this->order;
    }
}
