<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Schema model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Payment_Approve_Schema extends BSellerERP_Core_Model_Schema_Abstract
{
    /**
     * Order
     *
     * @var Mage_Sales_Model_Order
     */
    protected $order;
    
    /**
     * Invoice
     *
     * @var Mage_Sales_Model_Order_Invoice
     */
    protected $invoice;

    /**
     * Returns helper object
     *
     * @var BSellerERP_Order_Helper_Data
     */
    protected $helper;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'order';

    /**
     * Prepare schema and returns for adding in order instance
     *
     * @return array
     */
    protected function prepareSchema()
    {
        parent::prepareSchema();

        if (!$this->invoice->getId()) {
            $this->helper->log('Invoice not exist for create approve schema: ' . $this->order->getIncrementId());
            
            return $this->getSchema();
        }

        $this->setGeneralData();

        return $this->getSchema();
    }

    /**
     * Setting general data for schema
     *
     * @return $this
     */
    protected function setGeneralData()
    {
        $this->addData(
            array(
                'codigoRetorno'     => 0,
                'dataTransacao'     => $this->helper->formatDate($this->invoice->getCreatedAtDate()),
                'idAutorizacao'     => 0,
                'idPedido'          => $this->invoice->getOrder()->getIncrementId(),
                'nsuAutorizadora'   => 0,
                'nsuCtf'            => 0,
                'sequencial'        => 0,
                'valorJuros'        => 0
            )
        );

        return $this;
    }
}
