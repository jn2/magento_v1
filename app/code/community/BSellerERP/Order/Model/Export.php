<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Export order model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Order_Model_Export extends BSellerERP_Order_Model_Schema
{
    /**
     * Init export order
     *
     * @return $this
     */
    public function init()
    {
        Mage::dispatchEvent(
            $this->helper->getPrefix() . '_export_init',
            array(
                'schema' => $this,
                'order'  => $this->order
            )
        );

        /**
         * Init integration order
         */
        Mage::getModel('bsellererp_order/integrator')
            ->setSchema($this->prepareSchema())
            ->setOrder($this->order)
            ->init();

        Mage::dispatchEvent(
            $this->helper->getPrefix() . '_export_complete',
            array(
                'schema' => $this,
                'order'  => $this->order
            )
        );

        return $this;
    }

    /**
     * Set data order
     *
     * @param int $incrementId
     * @return $this
     */
    public function setOrder($incrementId)
    {
        $this->order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);

        /**
         * Set current store by order
         */
        if ($storeCode = $this->order->getStore()->getCode()) {
            Mage::app()->setCurrentStore($storeCode);
        }

        return $this;
    }
}
