<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing products in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Product
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Product_Model_Import extends BSellerERP_Product_Model_Schema
{
    /**
     * Content returned integration to import
     * @var object
     */
    protected $products;

    /**
     * Init importing categories
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('products' => $this->products));

        $this->helper->log('Numers of products: ' . count($this->products));

        foreach ($this->products as $product) {
            if (!$this->validate($product)) {
                $this->helper->log('Item id not valid: ' . $product->codigoTerceiro);
                continue;
            }

            $this->createOrUpdate($product);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('products' => $this->products));

        return $this;
    }

    /**
     * Set data categories
     *
     * @param $products
     * @return $this
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * Create/Update by response ERP
     *
     * @param $productERP
     * @return $this
     * @throws Exception
     */
    protected function createOrUpdate($productERP)
    {
        $this->productERP     = $productERP;
        $idProduct            = Mage::getModel('catalog/product')->getIdBySku($this->getSku());
        $this->currentProduct = Mage::getModel('catalog/product')->load($idProduct);

        if (!$this->currentProduct->getId()) {
            $this->currentProduct = Mage::getModel('catalog/product');
        }

        /**
         * Adding data as schema
         */
        $this->currentProduct->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_before', array('product' => $this->currentProduct));

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $this->currentProduct->save();

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('product' => $this->currentProduct));

        /**
         * Configurable products
         */
        if ($this->helper->isChild($this->productERP)) {
            $this->helper->log('Start association children');

            try {
                $this->associateChildren();
            } catch (Exception $e) {
                $this->helper->log($e->getMessage());
            }
        }

        return $this;
    }

    /**
     * Validate product for create in application
     *
     * @param object $product
     * @return bool
     */
    protected function validate($product)
    {
        $requiredParams = array(
            'codigoItem', 'codigoTerceiro'
        );

        foreach ($requiredParams as $param) {
            if (empty($product->$param)) {
                $this->helper->log('Product is not valid for importing, missing param: ' . $param);
                $this->helper->log($product);

                return false;
            }
        }

        return true;
    }
}
