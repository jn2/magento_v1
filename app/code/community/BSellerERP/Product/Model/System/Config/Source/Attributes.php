<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * List of attributes used in system config
 *
 * @category   BSellerERP
 * @package    BSellerERP_Product
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Product_Model_System_Config_Source_Attributes
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $attributes = Mage::getResourceSingleton('catalog/product_attribute_collection')->addVisibleFilter();
        $options    = array();

        $options[] = array(
            'label' => Mage::helper('bsellererp_product')->__('-- Please Select --'),
            'value' => ''
        );

        /**
         * @var Mage_Catalog_Model_Entity_Attribute $attribute
         */
        foreach ($attributes as $attribute) {
            $options[] = array(
                'label' => Mage::helper('catalog/product')->__($attribute->getData('frontend_label')),
                'value' => $attribute->getData('attribute_code')
            );
        }

        return $options;
    }
}