<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Weight setting model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Product
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Product_Model_System_Config_Source_Weight
{
    /**
     * Weight GROSS
     *
     * @const string
     */
    const WEIGHT_GROSS = 'gross';

    /**
     * Weight UNITARY
     *
     * @const string
     */
    const WEIGHT_UNITARY = 'unitary';

    /**
     * Get Weight types
     *
     * @return array
     */
    public function toOptionArray()
    {
        /** @var BSellerERP_Product_Helper_Data $helper */
        $helper = Mage::helper('bsellererp_product');

        return array(
            array(
                'value' => self::WEIGHT_GROSS,
                'label' => $helper->__('Gross'),
            ),
            array(
                'value' => self::WEIGHT_UNITARY,
                'label' => $helper->__('Unitary'),
            ),
        );
    }
}
