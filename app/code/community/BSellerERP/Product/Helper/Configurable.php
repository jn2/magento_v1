<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Product
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Configurable product module helper, associate children in parent product.
 * Class developed by Jreinke.
 *
 * @author     Jreinke (https://github.com/jreinke/magento-improve-api)
 */
class BSellerERP_Product_Helper_Configurable extends BSellerERP_Core_Helper_Data
{
    /**
     * Assign children in configurable product
     *
     * @param Mage_Catalog_Model_Product $parent
     * @param array $children
     * @param array $attributesId
     * @return $this
     */
    public function assignChildren(Mage_Catalog_Model_Product $parent, $children, $attributesId = array())
    {
        if (!$parent->isConfigurable()) {
            $this->log('Impossible to associate simple products');

            return $this;
        }

        if (!count($children)) {
            $this->log('Impossible to associate empty children products');

            return $this;
        }

        $typeConfigurable = Mage::getModel('catalog/product_type_configurable');
        $typeConfigurable->setProduct($parent);

        $productsAssociated = $typeConfigurable->getUsedProductCollection()
            ->addFilterByRequiredOptions()
            ->getAllIds();

        $newProducts = array_diff($children, $productsAssociated);
        $allProducts = array_merge($newProducts, $productsAssociated);

        if (!count($newProducts)) {
            $this->log('Not exist news products for association for parent product: ' . $parent->getId());

            return $this;
        }

        $this->startingAssignment($parent, $newProducts, $allProducts, $attributesId);

        return $this;
    }

    /**
     * Starting associate children
     *
     * @param Mage_Catalog_Model_Product $parent
     * @param array $newProducts
     * @param array $allProducts
     * @param array $attributesId
     * @return $this
     * @throws Exception
     */
    protected function startingAssignment(Mage_Catalog_Model_Product $parent, $newProducts, $allProducts, $attributesId)
    {
        /**
         * Set all products children
         */
        $parent->setConfigurableProductsData(array_flip($allProducts));

        $productType = $parent->getTypeInstance(true);
        $productType->setProduct($parent);

        $attributesData = $productType->getConfigurableAttributesAsArray();

        /**
         * Auto generation if configurable product has no attribute
         */
        if (empty($attributesData)) {
            $attributeIds = array();

            foreach ($productType->getSetAttributes() as $attribute) {
                if (!$productType->canUseAttribute($attribute) && !in_array($attribute->getId(), $attributesId)) {
                    continue;
                }

                $attributeIds[] = $attribute->getAttributeId();
            }

            $productType->setUsedProductAttributeIds($attributeIds);
            $attributesData = $productType->getConfigurableAttributesAsArray();
        }

        /**
         * Unset attributes not allowed
         */
        if (count($attributesId)) {
            foreach ($attributesData as $id => $value) {
                if (!in_array($value['attribute_id'], $attributesId)) {
                    unset($attributesData[$id]);
                }
            }
        }

        /**
         * Get news products collection
         */
        $products = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addIdFilter($newProducts);

        if (!count($products)) {
            $this->log('Not possible load collection for association news products, parent product id: ' . $parent->getId());

            return $this;
        }

        /**
         * Set new values for attribute by children
         */
        foreach ($attributesData as $key => $attribute) {
            $newData = array();
            $newData['label'] = $attribute['frontend_label'];

            foreach ($products as $product) {
                $optionId  = $product->getData($attribute['attribute_code']);

                $newData['values'][$optionId] = array(
                    'value_index'   => $optionId,
                    'is_percent'    => '',
                    'pricing_value' => '',
                );
            }

            $attributesData[$key] = array_merge($attribute, $newData);
        }

        $parent->setConfigurableAttributesData($attributesData);
        $parent->save();

        return $this;
    }
}
