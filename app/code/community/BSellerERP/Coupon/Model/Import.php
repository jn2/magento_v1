<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Coupon
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing coupons in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Coupon
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Coupon_Model_Import extends BSellerERP_Coupon_Model_Schema
{
    /**
     * Content returned integration to import
     * @var object
     */
    protected $coupons;

    /**
     * Init importing coupons
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('coupons' => $this->coupons));

        foreach ($this->coupons as $coupon) {
            if (!$this->validate($coupon)) {
                continue;
            }

            $this->createOrUpdate($coupon);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('coupons' => $this->coupons));

        return $this;
    }

    /**
     * Set data coupons
     *
     * @param $coupons
     * @return $this
     */
    public function setCoupons($coupons)
    {
        $this->coupons = $coupons;

        return $this;
    }

    /**
     * Create/Update coupon by response ERP
     *
     * @param $couponERP
     * @return $this
     * @throws Exception
     */
    public function createOrUpdate($couponERP)
    {
        $this->couponERP     = $couponERP;
        $this->currentCoupon = Mage::getModel('salesrule/rule');

        /**
         * Validate if already coupon exists
         */
        $coupon = Mage::getSingleton('salesrule/coupon')->loadByCode($this->couponERP->idVale);
        if ($coupon->getId()) {
            $this->helper->log('Impossible importing, coupon already exists - ' . $this->couponERP->idVale);

            return $this;
        }

        /**
         * Adding data as scheme
         */
        $this->currentCoupon->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix(). '_item_save_before', array('coupon' => $this->currentCoupon));
        $this->currentCoupon->save();
        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('coupon' => $this->currentCoupon));

        return $this;
    }

    /**
     * Validate coupon for create in application
     *
     * @param object $coupon
     * @return bool
     */
    protected function validate($coupon)
    {
        $requiredParams = array(
            'cpfCliente', 'idVale', 'situacao', 'preco', 'valor'
        );

        foreach ($requiredParams as $param) {
            if ($coupon->$param === false) {
                $this->helper->log('Coupon is not valid for importing, missing param: ' . $param);
                $this->helper->log($coupon);

                return false;
            }
        }

        return true;
    }
}
