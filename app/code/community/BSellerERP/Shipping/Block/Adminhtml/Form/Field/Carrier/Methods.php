<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Core
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Mapping product attributes model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Core
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Shipping_Block_Adminhtml_Form_Field_Carrier_Methods
    extends BSellerERP_Core_Block_Adminhtml_Form_Field_Abstract
{
    /**
     * Column carriers ERP
     *
     * @const string
     */
    const COLUMN_CARRIER_ERP = 'carriers_erp';

    /**
     * Column carriers application
     *
     * @const string
     */
    const COLUMN_CARRIER_APPLICATION = 'carriers';

    /**
     * Renderer types
     *
     * @var array
     */
    protected $renderer = array();

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $helper = Mage::helper('bsellererp_shipping');

        $this->addColumn(
            self::COLUMN_CARRIER_ERP,
            array(
                'label' => $helper->__('ERP Carriers'),
                'class' => 'input-text required-entry',
                'renderer' => $this->getRenderer(self::COLUMN_CARRIER_ERP)
            )
        );

        $this->addColumn(
            self::COLUMN_CARRIER_APPLICATION,
            array(
                'label' => $helper->__('Magento Carriers (Code)'),
                'class' => 'input-text required-entry'
            )
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = $helper->__('Add new mapping');
    }

    /**
     * Get renderer by type
     *
     * @param string $column
     * @return mixed
     */
    protected function getRenderer($column)
    {
        if (isset($this->renderer[$column])) {
            return $this->renderer[$column];
        }

        $options = $this->getCarriersERPOptions();

        $this->renderer[$column] = $this->getLayout()
            ->createBlock('core/html_select')
            ->setIsRenderToJsTemplate(true)
            ->setOptions($options);

        return $this->renderer[$column];
    }

    /**
     * Prepare row and set option selected
     *
     * @param Varien_Object $row
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->getRenderer(self::COLUMN_CARRIER_ERP)->calcOptionHash($row->getData(self::COLUMN_CARRIER_ERP)),
            'selected="selected"'
        );
    }

    /**
     * Get all variations for mapping
     *
     * @return array
     */
    protected function getCarriersERPOptions()
    {
        $carriers = Mage::getSingleton('bsellererp_shipping/carrier_items')->getCollection();
        $options  = array();

        $options[] = array(
            'label' => Mage::helper('bsellererp_shipping')->__('-- Please Select --'),
            'value' => ''
        );

        /**
         * @var BSellerERP_Shipping_Model_Carrier_Items $carriers
         */
        foreach ($carriers as $carrier) {
            $options[$carrier->getData('carrier_id')] = $carrier->getData('title');
        }

        return $options;
    }
}
