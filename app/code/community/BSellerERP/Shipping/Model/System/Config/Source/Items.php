<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Returns all sales shipping
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Shipping_Model_System_Config_Source_Items
{
    /**
     * Get all sales shipping
     *
     * @return array
     */
    public function toOptionArray()
    {
        $shippings = Mage::getSingleton('bsellererp_shipping/items')->getCollection();
        $options  = array();

        $options[] = array(
            'label' => Mage::helper('bsellererp_product')->__('-- Please Select --'),
            'value' => ''
        );

        /**
         * @var BSellerERP_Shipping_Model_Items $attribute
         */
        foreach ($shippings as $shipping) {
            $options[$shipping->getData('shipping_id')] = $shipping->getData('name');
        }

        return $options;
    }
}
