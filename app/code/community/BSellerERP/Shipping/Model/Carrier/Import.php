<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Shipping
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing carriers in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Shipping
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Shipping_Model_Carrier_Import extends BSellerERP_Shipping_Model_Carrier_Schema
{
    /**
     * Content returned integration to import
     * @var object
     */
    protected $carriers;

    /**
     * Init importing carriers
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('carriers' => $this->carriers));

        foreach ($this->carriers as $shipping) {
            if (!$this->validate($shipping)) {
                continue;
            }

            $this->create($shipping);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('carriers' => $this->carriers));

        return $this;
    }

    /**
     * Set data carriers
     *
     * @param $carriers
     * @return $this
     */
    public function setCarriers($carriers)
    {
        $this->carriers = $carriers;

        return $this;
    }

    /**
     * Create carrier by response ERP
     *
     * @param $carrierERP
     * @return $this
     * @throws Exception
     */
    protected function create($carrierERP)
    {
        $this->carrierERP     = $carrierERP;
        $this->currentCarrier = Mage::getModel('bsellererp_shipping/carrier_items');

        /**
         * Adding data as scheme
         */
        $this->currentCarrier->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_carrier_item_save_before', array('carrier' => $this->currentCarrier));
        $this->currentCarrier->save();
        Mage::dispatchEvent($this->helper->getPrefix() . '_carrier_item_save_after', array('carrier' => $this->currentCarrier));

        return $this;
    }

    /**
     * Validate carrier for create in application
     *
     * @param object $product
     * @return bool
     */
    protected function validate($product)
    {
        $requiredParams = array('id', 'descricao');

        foreach ($requiredParams as $param) {
            if ($product->$param === false) {
                $this->helper->log('[Carrier] Carrier is not valid for importing, missing param: ' . $param);
                $this->helper->log($product);

                return false;
            }
        }

        return true;
    }
}
