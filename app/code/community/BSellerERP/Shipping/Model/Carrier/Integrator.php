<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Shipping
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Integrator rest
 *
 * @category   BSellerERP
 * @package    BSellerERP_Shipping
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Shipping_Model_Carrier_Integrator extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Service path api
     *
     * @var string
     */
    public $servicePath = 'cias/transportadoras';

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'shipping';

    /**
     * Flag integration as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $body = Mage::helper('core')->jsonDecode($this->response->getBody(), Zend_Json::TYPE_OBJECT);

        if (!$body || !count($body)) {
            Mage::throwException($this->helper->__('[Carrier] Body response not exist data for importing'));

            return $this;
        }

        /**
         * Clear all carriers for new integration
         */
        Mage::getModel('bsellererp_shipping/carrier_items')->clearAllCarriers();

        /**
         * Starting carrier integration
         */
        $import = Mage::getModel('bsellererp_shipping/carrier_import');
        $import->setCarriers($body);
        $import->init();

        /**
         * Starting contracts integration
         */
        Mage::getModel('bsellererp_shipping/contract_import')->init();

        return $this;
    }

    /**
     * Flag integration as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log('[Carrier] ' . $this->response->getBody());

        return $this;
    }
}
