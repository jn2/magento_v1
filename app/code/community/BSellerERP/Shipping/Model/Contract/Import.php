<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Shipping
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Getting contracts by carrier
 *
 * @category   BSellerERP
 * @package    BSellerERP_Shipping
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Shipping_Model_Contract_Import
{
    /**
     * Init importing contracts
     */
    public function init()
    {
        $carriers = Mage::getSingleton('bsellererp_shipping/carrier_items')->getCollection();
        $helper   = Mage::helper('bsellererp_shipping');

        Mage::dispatchEvent($helper->getPrefix() . '_contract_export_init', array('carriers' => $carriers));

        /**
         * @var BSellerERP_Shipping_Model_Carrier_Items $carrier
         */
        foreach ($carriers as $carrier) {
            Mage::getModel('bsellererp_shipping/contract_integrator', $carrier)->init();
        }

        Mage::dispatchEvent($helper->getPrefix() . '_contract_export_complete', array('carriers' => $carriers));

        $helper->log('[Contract] Integration contracts success');

        return $this;
    }
}
