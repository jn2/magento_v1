<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Shipping
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Integrator rest
 *
 * @category   BSellerERP
 * @package    BSellerERP_Shipping
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Shipping_Model_Contract_Integrator extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Service path api
     *
     * @var string
     */
    public $servicePath = 'cias/transportadoras/%s/contratos';

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'shipping';

    /**
     * Set carrier
     *
     * @var BSellerERP_Shipping_Model_Carrier_Items
     */
    public $carrier;

    /**
     * Integrator constructor
     *
     * @param BSellerERP_Shipping_Model_Carrier_Items $carrier
     */
    public function __construct($carrier)
    {
        if (!$carrierId = $carrier->getData('carrier_id')) {
            Mage::throwException('[Contract] Not exist carrier for importing contract');
        }

        $this->servicePath = sprintf($this->servicePath, $carrierId);
        $this->carrier     = Mage::getModel('bsellererp_shipping/carrier_items')->load($carrierId, 'carrier_id');

        parent::__construct();
    }

    /**
     * Flag integration as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $body = Mage::helper('core')->jsonDecode($this->response->getBody(), Zend_Json::TYPE_OBJECT);

        if (!$body || !count($body)) {
            $this->helper->log(
                '[Contract] Body response not exist data for importing: ' . $this->carrier->getData('carrier_id')
            );

            return $this;
        }

        /**
         * Save contract id in carrier
         */
        $contract = array_shift($body);
        $this->carrier->setData('contract_id', $contract->contrato);
        $this->carrier->save();

        return $this;
    }

    /**
     * Flag integration as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log('[Contract] ' . $this->response->getBody());

        return $this;
    }
}
