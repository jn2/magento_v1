<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Credit
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller credit module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Credit
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Credit_Helper_Data extends BSellerERP_Core_Helper_Data
{
    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'credit';

    /**
     * Get credit limit by customer and total purchase
     *
     * @param string $customer (CPF/CNPJ)
     * @param float $total (Total purchase)
     *
     * @return array
     */
    public function getCreditLimit($customer, $total = 0.00)
    {
        $data = array(
            'customer' => $customer,
            'total'    => $total,
            'error'    => false
        );

        try {
            $integration = Mage::getModel('bsellererp_credit/integrator', $data)->init();
        } catch (Exception $e) {
            return array(
                'error' => $e
            );
        }

        return $integration;
    }
}
