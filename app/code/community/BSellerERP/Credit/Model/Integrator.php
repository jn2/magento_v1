<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Credit
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Integrator category rest
 *
 * @category   BSellerERP
 * @package    BSellerERP_Credit
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Credit_Model_Integrator extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Service path category api
     *
     * @var string
     */
    public $servicePath = 'representantes/limites-credito';

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'credit';

    /**
     * Data response
     *
     * @var string
     */
    public $returnData = array(
        'available'       => false,
        'limit_available' => 0
    );

    /**
     * Integrator constructor
     *
     * @param array $data
     */
    public function __construct($data)
    {
        /**
         * Adding query params
         */
        $params = array(
            'idRepresentante' => $data['customer'],
            'valorCompra'      => $data['total'],
            'unidadeNegocio'   => Mage::getStoreConfig('bsellererp_core/settings/business_unit'),
        );

        $this->setQueryParams($params);

        parent::__construct();
    }

    /**
     * Flag integration as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $body = Mage::helper('core')->jsonDecode($this->response->getBody(), Zend_Json::TYPE_OBJECT);

        if (!$body || !count($body)) {
            $this->helper->log('Body response not exist: ' . $this->paramsPath);

            return $this->returnData;
        }

        if ($available = $body->liberacao) {
            $this->returnData['available'] = $available;
        }

        if ($limitAvailable = $body->limiteDisponivel) {
            $this->returnData['limit_available'] = $limitAvailable;
        }

        return $this->returnData;
    }

    /**
     * Flag integration as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log('Integration failed for getting credit limit');
        $this->helper->log($this->response->getBody());

        return $this->returnData;
    }
}
