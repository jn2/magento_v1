<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_PaymentBrands
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Mapping brands model
 *
 * @category   BSellerERP
 * @package    BSellerERP_PaymentBrands
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_PaymentBrands_Block_Adminhtml_Form_Field_Brand
    extends BSellerERP_Core_Block_Adminhtml_Form_Field_Abstract
{
    /**
     * Column ERP
     *
     * @const string
     */
    const COLUMN_ERP = 'erp';

    /**
     * Column application
     *
     * @const string
     */
    const COLUMN_APPLICATION = 'application';

    /**
     * Renderer types
     *
     * @var array
     */
    protected $renderer = array();

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $helper = Mage::helper('bsellererp_payment_brands');

        $this->addColumn(
            self::COLUMN_ERP,
            array(
                'label' => $helper->__('Code ERP'),
                'class' => 'input-text required-entry',
                'renderer' => $this->getRenderer(self::COLUMN_ERP)
            )
        );

        $this->addColumn(
            self::COLUMN_APPLICATION,
            array(
                'label' => $helper->__('Code Magento'),
                'class' => 'input-text required-entry'
            )
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = $helper->__('Add');
    }

    /**
     * Get renderer by type
     *
     * @param  string $column
     * @return mixed
     */
    protected function getRenderer($column)
    {
        if (isset($this->renderer[$column])) {
            return $this->renderer[$column];
        }

        $options = $this->getERPOptions();

        $this->renderer[$column] = $this->getLayout()
            ->createBlock('core/html_select')
            ->setIsRenderToJsTemplate(true)
            ->setOptions($options);

        return $this->renderer[$column];
    }

    /**
     * Prepare row and set option selected
     *
     * @param Varien_Object $row
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->getRenderer(self::COLUMN_ERP)->calcOptionHash($row->getData(self::COLUMN_ERP)),
            'selected="selected"'
        );
    }

    /**
     * Get all options ERP
     *
     * @return array
     */
    protected function getERPOptions()
    {
        $optionsERP = Mage::getSingleton('bsellererp_payment_brands/item')->getCollection();
        $options  = array();

        $options[] = array(
            'label' => Mage::helper('core')->__('-- Please Select --'),
            'value' => ''
        );

        /**
         * @var BSellerERP_PaymentBrands_Model_Item $optionsERP
         */
        foreach ($optionsERP as $optionERP) {
            $options[$optionERP->getData('brand_id')] = $optionERP->getData('brand_name');
        }

        return $options;
    }
}
