<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_PaymentBrands
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing payment_brandss in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_PaymentBrands
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_PaymentBrands_Model_Import extends BSellerERP_PaymentBrands_Model_Schema
{
    /**
     * Content returned integration to import
     *
     * @var object
     */
    protected $responseObject;

    /**
     * Helper model
     *
     * @var BSellerERP_PaymentBrands_Helper_Data
     */
    protected $helper;

    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_payment_brands');
    }

    /**
     * Set response object data
     *
     * @param  object $responseObject
     * @return $this
     */
    public function setResponse($responseObject)
    {
        $this->responseObject = $responseObject;

        return $this;
    }

    /**
     * Init importing data
     *
     * @return $this
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('deliveries' => $this->responseObject));

        foreach ($this->responseObject as $responseObjectItem) {
            if (!$this->validate($responseObjectItem)) {
                continue;
            }

            $this->create($responseObjectItem);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('deliveries' => $this->responseObject));

        return $this;
    }

    /**
     * Create attribute by response item
     *
     * @param  object $responseObjectItem
     * @return $this
     * @throws Exception
     */
    protected function create($responseObjectItem)
    {
        $this->responseObjectItem = $responseObjectItem;
        $this->objectModel        = Mage::getModel('bsellererp_payment_brands/item');

        /**
         * Adding data as scheme
         */
        $this->objectModel->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_before', array('brand' => $this->objectModel));

        $this->objectModel->save();

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('brand' => $this->objectModel));

        return $this;
    }

    /**
     * @param  Mage_Sales_Model_Order $order
     * @param  $deliveries
     * @return bool
     */
    protected function importDeliveriesToOrder($order, $deliveries)
    {
        if (!count($deliveries)) {
            return false;
        }

        foreach ($deliveries as $payment_brands) {
            $idDelivery = $payment_brands->idEntrega;
            $items      = $this->getItemsByDelivery($payment_brands);

            /**
             * @var Mage_Sales_Model_Order_Item $item
             */
            foreach ($order->getAllVisibleItems() as $item) {
                if (array_key_exists($item->getSku(), $items)) {
                    $item->setData('id_payment_brands', $idDelivery);
                    $item->save();
                }
            }
        }

        return true;
    }

    /**
     * Validate payment_brands for create in application
     *
     * @param  object $product
     * @return bool
     */
    protected function validate($product)
    {
        $requiredParams = array('descricao', 'id');

        foreach ($requiredParams as $param) {
            if ($product->$param === false) {
                $this->helper->log('Delivery is not valid for importing, missing param: ' . $param);
                $this->helper->log($product);

                return false;
            }
        }

        return true;
    }
}
