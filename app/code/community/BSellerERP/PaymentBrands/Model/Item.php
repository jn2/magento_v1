<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_PaymentBrands
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Items model class
 *
 * @category   BSellerERP
 * @package    BSellerERP_PaymentBrands
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_PaymentBrands_Model_Item extends Mage_Core_Model_Abstract
{
    /**
     * Initialization items model
     */
    protected function _construct()
    {
        $this->_init('bsellererp_payment_brands/item');
    }
}
