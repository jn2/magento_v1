<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Channel
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Resource model class
 *
 * @category   BSellerERP
 * @package    BSellerERP_Channel
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Channel_Model_Resource_Items extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Resource model initialization
     */
    protected function _construct()
    {
        $this->_init('bsellererp_channel/items', 'entity_id');
    }
}