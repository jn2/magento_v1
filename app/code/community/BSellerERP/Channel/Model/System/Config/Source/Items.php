<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Returns all sales channel
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Channel_Model_System_Config_Source_Items
{
    /**
     * Get all sales channel
     *
     * @return array
     */
    public function toOptionArray()
    {
        $channels = Mage::getSingleton('bsellererp_channel/items')->getCollection();
        $options  = array();

        $options[] = array(
            'label' => Mage::helper('bsellererp_product')->__('-- Please Select --'),
            'value' => ''
        );

        /**
         * @var BSellerERP_Channel_Model_Items $attribute
         */
        foreach ($channels as $channel) {
            $options[$channel->getData('channel_id')] = $channel->getData('name');
        }

        return $options;
    }
}
