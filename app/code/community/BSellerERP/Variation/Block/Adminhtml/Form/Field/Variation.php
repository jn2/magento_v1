<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Variation
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Mapping product attributes model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Variation
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Variation_Block_Adminhtml_Form_Field_Variation
    extends BSellerERP_Core_Block_Adminhtml_Form_Field_Abstract
{
    /**
     * Column attributes ERP
     *
     * @const string
     */
    const COLUMN_CODE_ERP = 'variations_erp';

    /**
     * Column attributes application
     *
     * @const string
     */
    const COLUMN_CODE_APPLICATION = 'variatons';

    /**
     * Renderer types
     *
     * @var array
     */
    protected $renderer = array();

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $helper = Mage::helper('bsellererp_attribute');

        $this->addColumn(
            self::COLUMN_CODE_ERP,
            array(
                'label' => $helper->__('ERP Variations'),
                'class' => 'input-text required-entry',
                'renderer' => $this->getRenderer(self::COLUMN_CODE_ERP)
            )
        );

        $this->addColumn(
            self::COLUMN_CODE_APPLICATION,
            array(
                'label' => $helper->__('Magento Attributes'),
                'class' => 'input-text required-entry',
                'renderer' => $this->getRenderer(self::COLUMN_CODE_APPLICATION)
            )
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = $helper->__('Add new mapping');
    }

    /**
     * Get renderer by type
     *
     * @param string $column
     * @return mixed
     */
    protected function getRenderer($column)
    {
        if (isset($this->renderer[$column])) {
            return $this->renderer[$column];
        }

        $options = $this->getVariations();

        if ($column == self::COLUMN_CODE_APPLICATION) {
            $options = $this->getMagentoAttributes();
        }

        $this->renderer[$column] = $this->getLayout()
            ->createBlock('core/html_select')
            ->setIsRenderToJsTemplate(true)
            ->setOptions($options);

        return $this->renderer[$column];
    }

    /**
     * Prepare row and set option selected
     *
     * @param Varien_Object $row
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->getRenderer(self::COLUMN_CODE_ERP)->calcOptionHash($row->getData(self::COLUMN_CODE_ERP)),
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->getRenderer(self::COLUMN_CODE_APPLICATION)->calcOptionHash($row->getData(self::COLUMN_CODE_APPLICATION)),
            'selected="selected"'
        );
    }

    /**
     * Returns ERP attributes
     *
     * @return array
     */
    protected function getVariations()
    {
        $collection = Mage::getResourceSingleton('bsellererp_variation/items_collection');
        $options    = array();

        $options[] = array(
            'label' => Mage::helper('bsellererp_product')->__('-- Please Select --'),
            'value' => ''
        );

        /** @var BSellerERP_Variation_Model_Items $object */
        foreach ($collection as $object) {
            $options[$object->getData('variation_id')] = $object->getData('name');
        }

        return $options;
    }

    /**
     * Returns visible attributes of Magento
     *
     * @return array
     */
    protected function getMagentoAttributes()
    {
        $attributes = Mage::getResourceSingleton('catalog/product_attribute_collection')->addVisibleFilter();
        $options    = array();

        $options[] = array(
            'label' => Mage::helper('bsellererp_product')->__('-- Please Select --'),
            'value' => ''
        );

        /**
         * @var Mage_Catalog_Model_Entity_Attribute $attribute
         */
        foreach ($attributes as $attribute) {
            $options[$attribute->getData('attribute_code')] = $attribute->getData('frontend_label');
        }

        return $options;
    }
}
