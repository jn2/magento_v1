<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Variation
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Items model class
 *
 * @category   BSellerERP
 * @package    BSellerERP_Variation
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Variation_Model_Items extends Mage_Core_Model_Abstract
{
    /**
     * Initialization items model
     */
    protected function _construct()
    {
        $this->_init('bsellererp_variation/items');
    }

    /**
     * Clear all variations for new importing data
     *
     * @return $this
     * @throws Exception
     */
    public function clearAllVariations()
    {
        $collection = $this->getCollection();

        if (!$collection->getSize()) {
            return $this;
        }

        /**
         * @var BSellerERP_Variation_Model_Items $variation
         */
        foreach ($collection as $variation) {
            $variation->delete();
        }

        return $this;
    }
}
