<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Variation
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing variations in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Variation
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Variation_Model_Import extends BSellerERP_Variation_Model_Schema
{
    /**
     * Content returned integration to import
     * @var object
     */
    protected $variations;

    /**
     * Init importing variations
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('variations' => $this->variations));

        foreach ($this->variations as $variation) {
            if (!$this->validate($variation)) {
                continue;
            }

            $this->create($variation);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('variations' => $this->variations));

        return $this;
    }

    /**
     * Set data variations
     *
     * @param $variations
     * @return $this
     */
    public function setVariations($variations)
    {
        $this->variations = $variations;

        return $this;
    }

    /**
     * Create variation by response ERP
     *
     * @param $variationERP
     * @return $this
     * @throws Exception
     */
    protected function create($variationERP)
    {
        $this->variationERP     = $variationERP;
        $this->currentVariation = Mage::getModel('bsellererp_variation/items');

        /**
         * Adding data as scheme
         */
        $this->currentVariation->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_before', array('variation' => $this->currentVariation));
        $this->currentVariation->save();
        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('variation' => $this->currentVariation));

        return $this;
    }

    /**
     * Validate variation for create in application
     *
     * @param object $product
     * @return bool
     */
    protected function validate($product)
    {
        $requiredParams = array('id', 'nome');

        foreach ($requiredParams as $param) {
            if ($product->$param === false) {
                $this->helper->log('Variation is not valid for importing, missing param: ' . $param);
                $this->helper->log($product);

                return false;
            }
        }

        return true;
    }
}
