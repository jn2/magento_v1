<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Attribute
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Cron schedule model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Attribute
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author     Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSellerERP_Attribute_Model_Cron
{
    /**
     * Init import data
     *
     * @return $this
     */
    public function import()
    {
        if (!$this->helper()->moduleIsActive()) {
            return $this;
        }

        try {
            Mage::getModel('bsellererp_attribute/integrator')->init();

            $this->helper()->log('Integration attributes success');
        } catch (Exception $e) {
            $this->helper()->log($e->getMessage());

            Mage::throwException($e->getMessage());
        }

        return $this;
    }

    /**
     * Return data helper object
     *
     * @return BSellerERP_Attribute_Helper_Data
     */
    protected function helper()
    {
        return Mage::helper('bsellererp_attribute');
    }
}
