<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Attribute
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * Schema model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Attribute
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author     Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSellerERP_Attribute_Model_Group_Schema extends BSellerERP_Core_Model_Schema_Abstract
{
    /**
     * Object model
     *
     * @var BSellerERP_Attribute_Model_Attribute
     */
    public $objectModel;

    /**
     * Response object item
     *
     * @var object
     */
    public $responseObjectItem;

    /**
     * Prepare schema and returns for adding in variation instance
     *
     * @return array
     */
    protected function prepareSchema()
    {
        parent::prepareSchema();

        $this->setGeneralData();

        return $this->getSchema();
    }

    /**
     * Setting general data for schema
     *
     * @return $this
     */
    protected function setGeneralData()
    {
        $this->addData(
            array(
                'group_id'   => $this->responseObjectItem->id,
                'group_name' => $this->responseObjectItem->descricao
            )
        );

        return $this;
    }
}
