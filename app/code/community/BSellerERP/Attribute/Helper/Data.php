<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Attribute
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author    Alan Carvalho <alan.carvalho@e-smart.com.br>
 */

/**
 * BSeller core module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Attribute
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 * @author     Alan Carvalho <alan.carvalho@e-smart.com.br>
 */
class BSellerERP_Attribute_Helper_Data extends BSellerERP_Core_Helper_Data
{
    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'attribute';

    /**
     * Returns attribute set ID by ERP group
     *
     * @param string $groupId
     * @return bool|int
     */
    public function getAttributeSetId($groupId)
    {
        $groupsSerialized = Mage::getStoreConfig('bsellererp_attribute/mapping/groups');

        if (!$groupsSerialized) {
            $this->log('There is no registered groups mapping');

            return false;
        }
        
        $groups = (array) Zend_Serializer::unserialize($groupsSerialized);

        if (!count($groups)) {
            return false;
        }

        foreach ($groups as $group) {
            if ((int) $group['groups_erp'] !== $groupId) {
                continue;
            }

            return (int) $group['groups'];
        }
        
        return false;
    }

    /**
     * Get attribute by ERP code
     *
     * @param string $code
     * @return bool
     */
    public function getAttribute($code)
    {
        $attributesSerialized = Mage::getStoreConfig('bsellererp_attribute/mapping/attributes');

        if (!$attributesSerialized) {
            $this->log('There is no registered attributes mapping');

            return false;
        }

        $attributes = (array) Zend_Serializer::unserialize($attributesSerialized);

        if (!count($attributes)) {
            return false;
        }

        foreach ($attributes as $attribute) {
            if ($attribute['attributes_erp'] != $code) {
                continue;
            }

            return $attribute['attributes'];
        }

        return false;
    }
}
