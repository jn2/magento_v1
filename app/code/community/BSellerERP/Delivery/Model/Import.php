<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Delivery
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing deliverys in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Delivery
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Delivery_Model_Import
{
    /**
     * Content returned integration to import
     *
     * @var object
     */
    protected $responseObject;

    /**
     * Helper model
     *
     * @var BSellerERP_Delivery_Helper_Data
     */
    protected $helper;

    public function __construct()
    {
        $this->helper = Mage::helper('bsellererp_delivery');
    }

    /**
     * Set response object data
     *
     * @param object $responseObject
     * @return $this
     */
    public function setResponse($responseObject)
    {
        $this->responseObject = $responseObject;

        return $this;
    }

    /**
     * Init importing data
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('deliveries' => $this->responseObject));

        foreach ($this->responseObject as $responseObjectItem) {
            if (!$this->validate($responseObjectItem)) {
                continue;
            }

            $this->import($responseObjectItem);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('deliveries' => $this->responseObject));

        return $this;
    }

    protected function import($responseObjectItem)
    {
        $order = Mage::getModel('sales/order')->loadByIncrementId($responseObjectItem->numeroPedido);

        if (!$order) {
            $this->helper->log('Impossible load order for importing delivery: ' . $responseObjectItem->numeroPedido);

            return false;
        }

        $deliveries = $responseObjectItem->entregas;

        if (!count($deliveries)) {
            $this->helper->log('Not exist deliveries for importing for order ' . $responseObjectItem->numeroPedido);

            return false;
        }

        return $this->importDeliveriesToOrder($order, $deliveries);
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param $deliveries
     * @return bool
     */
    protected function importDeliveriesToOrder($order, $deliveries)
    {
        if (!count($deliveries)) {
            return false;
        }

        foreach ($deliveries as $delivery) {
            $idDelivery = $delivery->idEntrega;
            $items      = $this->getItemsByDelivery($delivery);

            /**
             * @var Mage_Sales_Model_Order_Item $item
             */
            foreach ($order->getAllVisibleItems() as $item) {
                if (array_key_exists($item->getSku(), $items)) {
                    $item->setData('id_delivery', $idDelivery);
                    $item->save();
                }
            }
        }

        return true;
    }

    /**
     * Get and format items by delivery
     *
     * @param object $delivery
     * @return array
     */
    protected function getItemsByDelivery($delivery)
    {
        $items = $delivery->itens;
        $data  = array();

        if (!count($items)) {
            return $data;
        }

        foreach ($items as $item) {
            $data[$item->codigoItem] = $item->quantidade;
        }

        return $data;
    }

    /**
     * Validate delivery for create in application
     *
     * @param object $product
     * @return bool
     */
    protected function validate($product)
    {
        $requiredParams = array('numeroPedido', 'entregas');

        foreach ($requiredParams as $param) {
            if ($product->$param === false) {
                $this->helper->log('Delivery is not valid for importing, missing param: ' . $param);
                $this->helper->log($product);

                return false;
            }
        }

        return true;
    }
}
