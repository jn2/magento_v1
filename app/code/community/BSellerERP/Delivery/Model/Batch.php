<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Delivery
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Confirm batch model
 *
 * @category   BSellerERP
 * @package    BSellerERP_Delivery
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Delivery_Model_Batch extends BSellerERP_Core_Model_Integrator_Rest
{
    /**
     * Service path api
     * 
     * @var string
     */
    public $servicePath = 'entregas/massivo';

    /**
     * Request Method
     *
     * @var array
     */
    public $requestMethod = Zend_Http_Client::PUT;

    /**
     * Load helper data
     *
     * @var string
     */
    public $currentHelper = 'delivery';

    /**
     * BSellerERP_Delivery_Model_Batch constructor
     *
     * @param null|int $batchNumber
     */
    public function __construct($batchNumber = null)
    {
        if (!is_null($batchNumber)) {
            $this->setPathParams(array($batchNumber));
        }

        return parent::__construct();
    }

    /**
     * Flag batch as success
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function success()
    {
        $body = Mage::helper('core')->jsonDecode($this->response->getBody(), Zend_Json::TYPE_OBJECT);

        if (!$body->message) {
            Mage::throwException($this->helper->__('Body response not exist data for confirm batch'));

            return $this;
        }

        $this->helper->log('The batch was successfully confirmed: #' . $this->paramsPath);

        return $this;
    }

    /**
     * Flag batch as failed
     *
     * @return $this
     */
    protected function failed()
    {
        $this->helper->log($this->response->getBody());

        return $this;
    }
}
