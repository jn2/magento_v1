<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Order
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Returns all sales channel
 *
 * @category   BSellerERP
 * @package    BSellerERP_Order
 * @author     Julio Reis <julio.reis@e-smart.com.br>
 */
class BSellerERP_PaymentConditions_Model_Schema extends BSellerERP_Core_Model_Schema_Abstract
{
    /**
     * Object model
     *
     * @var BSellerERP_Attribute_Model_Attribute
     */
    public $objectModel;

    /**
     * Response object item
     *
     * @var object
     */
    public $responseObjectItem;

    /**
     * Prepare schema and returns for adding in variation instance
     *
     * @return array
     */
    protected function prepareSchema()
    {
        parent::prepareSchema();

        $this->setGeneralData();

        return $this->getSchema();
    }

    /**
     * Setting general data for schema
     *
     * @return $this
     */
    protected function setGeneralData()
    {
        $this->addData(
            array(
                'condition_id'   => $this->responseObjectItem->id,
                'condition_name' => $this->responseObjectItem->nome
            )
        );

        return $this;
    }
}
