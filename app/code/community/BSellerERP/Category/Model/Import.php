<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Category
 *
 * @copyright Copyright (c) 2015 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * Initialize importing categories in application
 *
 * @category   BSellerERP
 * @package    BSellerERP_Category
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Category_Model_Import extends BSellerERP_Category_Model_Schema
{
    /**
     * Content returned integration to import
     * 
     * @var object
     */
    protected $categories;

    /**
     * Init importing categories
     */
    public function init()
    {
        Mage::dispatchEvent($this->helper->getPrefix() . '_import_init', array('categories' => $this->categories));

        foreach ($this->categories as $category) {
            if (!$this->validate($category)) {
                continue;
            }

            $this->createOrUpdate($category);
        }

        Mage::dispatchEvent($this->helper->getPrefix() . '_import_complete', array('categories' => $this->categories));

        return $this;
    }

    /**
     * Set data categories
     *
     * @param $categories
     * @return $this
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Create/Update category by response ERP
     *
     * @param $categoryERP
     * @return $this
     * @throws Exception
     */
    public function createOrUpdate($categoryERP)
    {
        $this->categoryERP      = $categoryERP;
        $this->currentCategory  = Mage::getModel('catalog/category')->loadByAttribute('erp_id', $this->getErpId());

        if (!$this->currentCategory) {
            $this->currentCategory = Mage::getModel('catalog/category');
        }

        /**
         * Adding data in schema
         */
        $this->currentCategory->addData($this->prepareSchema());

        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_before', array('category' => $this->currentCategory));
        $this->currentCategory->save();
        Mage::dispatchEvent($this->helper->getPrefix() . '_item_save_after', array('category' => $this->currentCategory));

        return $this;
    }

    /**
     * Validate category for create in application
     *
     * @param object $category
     * @return bool
     */
    protected function validate($category)
    {
        if (!$category->idCategoria || !$category->nome) {
            $this->helper->log('Category is not valid for importing, missing name/id param');
            $this->helper->log($category);

            return false;
        }

        return true;
    }
}
