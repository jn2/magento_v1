<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Payment
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

/**
 * BSeller core module helper
 *
 * @category   BSellerERP
 * @package    BSellerERP_Payment
 * @author     Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */
class BSellerERP_Payment_Helper_Data extends BSellerERP_Core_Helper_Data
{
    /**
     * General prefix for using in logs and events
     *
     * @var string
     */
    protected $prefix = 'payment';

    /**
     * Return payment by code ERP
     *
     * @param string $code
     * @return bool|int
     */
    public function getPaymentByCode($code)
    {
        $payments = (array) unserialize(Mage::getStoreConfig('bsellererp_payment/mapping/payment'));

        foreach ($payments as $payment) {
            if ($payment['payments'] !== $code) {
                continue;
            }

            return (int) $payment['payments_erp'];
        }

        Mage::helper('bsellererp_payment')->log('There is no mapping for the payment code: ' . $code);

        return false;
    }

    public function getPaymentCondition($order) {
        $info = $order->getPayment()->getAdditionalInformation();
        if(isset($info['bsellererp_payment_condition'])) {
            return $info['bsellererp_payment_condition'];
        }
        return '';
    }
}
