<?php
/**
 * B Seller Platform
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.bseller.com.br for more information.
 *
 * @category  BSellerERP
 * @package   BSellerERP_Payment
 *
 * @copyright Copyright (c) 2016 B2W Digital. (http://www.b2wdigital.com)
 *
 * @author    Plínio Cardoso <plinio.cardoso@e-smart.com.br>
 */

class BSellerERP_Payment_Block_Adminhtml_Form_Field_Methods
    extends BSellerERP_Core_Block_Adminhtml_Form_Field_Abstract
{
    /**
     * Column payment groups ERP
     *
     * @const string
     */
    const COLUMN_CODE_CUSTOM = 'payments_erp';

    /**
     * Column payment groups application
     *
     * @const string
     */
    const COLUMN_CODE_APP = 'payments';

    /**
     * Renderer types
     *
     * @var array
     */
    protected $renderer = array();

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $helper = $this->helper('bsellererp_payment');

        $this->addColumn(
            self::COLUMN_CODE_CUSTOM,
            array(
                'label'    => $helper->__('ERP Payments'),
                'class'    => 'input-text required-entry',
                'renderer' => $this->getRenderer(self::COLUMN_CODE_CUSTOM)
            )
        );

        $this->addColumn(
            self::COLUMN_CODE_APP,
            array(
                'label'    => $helper->__('Magento Payments'),
                'class'    => 'input-text required-entry',
                'renderer' => $this->getRenderer(self::COLUMN_CODE_APP)
            )
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = $helper->__('Add new mapping');
    }

    /**
     * Return renderer by type
     *
     * @param string $column
     * @return array
     */
    protected function getRenderer($column)
    {
        if (isset($this->renderer[$column])) {
            return $this->renderer[$column];
        }

        $options = array();

        if ($column === self::COLUMN_CODE_CUSTOM) {
            $options = $this->getErpPayments();
        }

        if ($column === self::COLUMN_CODE_APP) {
            $options = $this->getPayments();
        }

        $this->renderer[$column] = $this->getLayout()
            ->createBlock('core/html_select')
            ->setIsRenderToJsTemplate(true)
            ->setOptions($options);

        return $this->renderer[$column];
    }

    /**
     * Prepare row and set option selected
     *
     * @param Varien_Object $row
     * @return void
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->getRenderer(self::COLUMN_CODE_CUSTOM)
                ->calcOptionHash($row->getData(self::COLUMN_CODE_CUSTOM)),
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->getRenderer(self::COLUMN_CODE_APP)
                ->calcOptionHash($row->getData(self::COLUMN_CODE_APP)),
            'selected="selected"'
        );
    }

    /**
     * Return groups options in array
     *
     * @return array
     */
    protected function getPayments()
    {
        $options[] = array(
            'label' => $this->helper('bsellererp_payment')->__('-- Please Select --'),
            'value' => ''
        );

        $collection = Mage::getSingleton('payment/config')->getAllMethods();

        /** @var BSellerERP_Payment_Model_Items $object */
        foreach ($collection as $code => $model) {
            $paymentTitle = Mage::getStoreConfig('payment/'.$code.'/title');
            $options[$code] = $paymentTitle;
        }

        return $options;
    }

    /**
     * Return custom group options in array
     *
     * @return array
     */
    protected function getErpPayments()
    {
        $options[] = array(
            'label' => $this->helper('bsellererp_payment')->__('-- Please Select --'),
            'value' => ''
        );

        $collection = Mage::getResourceSingleton('bsellererp_payment/items_collection');

        /** @var BSellerERP_Payment_Model_Items $object */
        foreach ($collection as $object) {
            $options[$object->getData('payment_id')] = $object->getData('name');
        }

        return $options;
    }
}
