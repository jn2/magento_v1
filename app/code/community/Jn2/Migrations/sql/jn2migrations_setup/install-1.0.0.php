<?php

/**
 * Remove estados duplicados na tabela directory_country_region
 * 
 */
$installer = $this;
$installer->startSetup();
/* @var $installer  Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$con = $installer->getConnection();
/* @var $con Varien_Db_Adapter_Interface */
$tableRegion = $installer->getTable('directory_country_region');
$duplicateCodeQuery = "SELECT * FROM `directory_country_region` group by country_id, code
having count(code) > 1 ";
$duplicateIds = array();
$con->beginTransaction();
$rows = $con->fetchAll($duplicateCodeQuery);
foreach ($rows as $row) {
    $duplicateIds[] = $row['region_id'];
}

if ($duplicateIds) {
    $where = $con->quoteInto("region_id in (?)", $duplicateIds);
    $con->delete($tableRegion, $where);
}
//###################
//update related records
//###################
$oldRegionRows = $rows;

// seleciona eav_attribute
$queryEavAttribute = "select e.attribute_id,e.entity_type_id,e.backend_type from eav_attribute e where attribute_code='region_id'";
$attributeRegionRows = $con->fetchAll($queryEavAttribute);

//itera sobre registros que foram excluidos
foreach ($oldRegionRows as $region) {
    $oldValue = $region['region_id']; // region_id do registro que foi excuido
    $newValue = Mage::getResourceModel('directory/region_collection')
            ->addFieldToFilter('code', array('eq' => $region['code']))
            ->addFieldToFilter('country_id', array('eq' => $region['country_id']))
            ->getFirstItem()
            ->getId(); //region_id que continua no banco de dados e estava duplicado


    /**
     * Customer_Address
     */
    foreach ($attributeRegionRows as $attributeRow) {//update eav region_ids
        $attributeCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($attributeRow['entity_type_id'])
                ->addFieldToFilter('main_table.attribute_id', array('eq' => $attributeRow['attribute_id']));

        foreach ($attributeCollection as $attribute) {
            /* @var $field Mage_Eav_Model_Entity_Attribute */
            $backendTable = $attribute->getBackend()->getTable();
            $queryUpdate = "update {$backendTable} set value = '{$newValue}' where value = '{$oldValue}' and attribute_id= '{$attributeRow['attribute_id']}'";
            #  echo $queryUpdate . "<br/>";
            $con->query($queryUpdate);
        }
    }//end update eav
    //update flat region_id (flat_order quote_order)
    $flatTables = array('sales_flat_order_address', 'sales_flat_quote_address');
    foreach ($flatTables as $flatTable) {
        $queryUpdate = "update {$flatTable} set region_id = '{$newValue}' where region_id = '{$oldValue}'";
        #  echo $queryUpdate . "<br/>";
        $con->query($queryUpdate);
    }
}

$con->commit();
$installer->endSetup();