<?php
/**
 * mysql4-data-upgrade-1.2.7-1.2.8.php
 *
 * @author Thiago Machado <thiago.machado@jn2.com.br>
 * @version 1.0
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

if (Mage::getStoreConfigFlag('payment/pagseguro/active')) {
    $sql = <<<SQL
DELETE FROM `core_config_data` 
  WHERE `path` = 'payment/pagseguro_default_lightbox/active';

INSERT INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`)
  VALUES ('default', 0, 'payment/pagseguro_default_lightbox/active', '1');

DELETE FROM `core_config_data`
  WHERE `path` = 'payment/pagseguro_default_lightbox/checkout';

INSERT INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`)
  VALUES ('default', 0, 'payment/pagseguro_default_lightbox/checkout', 'LIGHTBOX');

SQL;

    $installer->run($sql);
}

$installer->endSetup();