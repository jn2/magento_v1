<?php
/**
 * Verifica se  Campo Celular esta ativo no onestepcheckout e desativa devido a alteracoes no register.phtml
 */
$installer = $this;
$installer->startSetup();

$js_to_footer = Mage::getStoreConfigFlag('dev/js/meanbee_footer_js_enabled');
if ($js_to_footer == '0') {
    Mage::getModel('core/config')->saveConfig("dev/js/meanbee_footer_js_enabled", '1');
    Mage::app()->getCacheInstance()->clean();
}

$installer->endSetup();
