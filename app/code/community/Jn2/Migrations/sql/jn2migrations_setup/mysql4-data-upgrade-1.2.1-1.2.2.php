<?php
$installer = $this;
$installer->startSetup();

$sql = <<<SQL
    UPDATE eav_attribute
    SET default_value = '1'
    WHERE attribute_code = 'is_anchor'    
SQL;

$installer->run($sql);
$installer->endSetup();
