<?php
$installer = $this;
$installer->startSetup();

//Bloco 1-----------------------------------------------------------------------------------------------
  $content1 = '
    <p class="info">Central de Atendimento</p>
    <p class="phone">00 - 0000 0000</p>
  ';

  $newBlock1 = Mage::getModel('cms/block')->load('block_rodapesimples_ca', 'identifier');

  if (!$newBlock1->getId()) {
    $newBlock1 = Mage::getModel('cms/block')
      ->setTitle('Rodapé Simples - Central de Atendimento')
      ->setContent($content1)
      ->setIdentifier('block_rodapesimples_ca')
      ->setIsActive(true)
      ->setStores(array(1))
      ->save();
  }
      
  $confBlock1 = Mage::getStoreConfig('custom_footer/blocks_footer_simple/bloco_1');
  
  if (!$confBlock1){
    $sql1 = "
      DELETE FROM core_config_data WHERE path = 'custom_footer/blocks_footer_simple/bloco_1';
      INSERT INTO 
        `core_config_data` (`scope`, `scope_id`, `path`, `value`)
      VALUES
        ('default', 0, 'custom_footer/blocks_footer_simple/bloco_1', '".$newBlock1->getId()."');    
    ";
  }

  $installer->run($sql1);
//--------------------------------------------------------------------------------------------------------

//Bloco 2-----------------------------------------------------------------------------------------------
  $content2 = '
    <ul>
      <li><a href="#">Tire Suas Dúvidas</a></li>
      <li><a href="#">Politica de Privacidade</a></li>
    </ul>
  ';

  $newBlock2 = Mage::getModel('cms/block')->load('block_rodapesimples_dp', 'identifier');

  if (!$newBlock2->getId()) {
    $newBlock2 = Mage::getModel('cms/block')
      ->setTitle('Rodapé Simples - Dúvidas e Privacidade')
      ->setContent($content2)
      ->setIdentifier('block_rodapesimples_dp')
      ->setIsActive(true)
      ->setStores(array(1))
      ->save();
  }
  $confBlock2 = Mage::getStoreConfig('custom_footer/blocks_footer_simple/bloco_2');
  
  if (!$confBlock2){
    $sql2 = "
      DELETE FROM core_config_data WHERE path = 'custom_footer/blocks_footer_simple/bloco_2';
      INSERT INTO 
        `core_config_data` (`scope`, `scope_id`, `path`, `value`)
      VALUES
        ('default', 0, 'custom_footer/blocks_footer_simple/bloco_2', '".$newBlock2->getId()."');    
    ";
  }

  $installer->run($sql2);
//--------------------------------------------------------------------------------------------------------

//Bloco 3-----------------------------------------------------------------------------------------------
  $content3 = '
    <img src="'.Mage::getDesign()->getSkinUrl('images/selos-payment-checkout.png').'" alt="Bandeira Pagamento" />
  ';

  $newBlock3 = Mage::getModel('cms/block')->load('block_rodapesimples_sp', 'identifier');

  if (!$newBlock3->getId()) {
    $newBlock3 = Mage::getModel('cms/block')
      ->setTitle('Rodapé Simples - Selos Pagamentos')
      ->setContent($content3)
      ->setIdentifier('block_rodapesimples_sp')
      ->setIsActive(true)
      ->setStores(array(1))
      ->save();
  }

  $confBlock3 = Mage::getStoreConfig('custom_footer/blocks_footer_simple/bloco_3');
  
  if (!$confBlock3){
    $sql3 = "
      DELETE FROM core_config_data WHERE path = 'custom_footer/blocks_footer_simple/bloco_3';
      INSERT INTO 
        `core_config_data` (`scope`, `scope_id`, `path`, `value`)
      VALUES
        ('default', 0, 'custom_footer/blocks_footer_simple/bloco_3', '".$newBlock3->getId()."');    
    ";
  }

  $installer->run($sql3);
//--------------------------------------------------------------------------------------------------------

//Bloco 4-----------------------------------------------------------------------------------------------
  $content4 = '
    <img src="'.Mage::getDesign()->getSkinUrl('images/selos-secury-checkout.png').'" alt="Selos de Segurança" />
  ';

  $newBlock4 = Mage::getModel('cms/block')->load('block_rodapesimples_ss', 'identifier');

  if (!$newBlock4->getId()) {
    $newBlock4 = Mage::getModel('cms/block')
      ->setTitle('Rodapé Simples - Selos Segurança')
      ->setContent($content4)
      ->setIdentifier('block_rodapesimples_ss')
      ->setIsActive(true)
      ->setStores(array(1))
      ->save();
  }

  $confBlock4 = Mage::getStoreConfig('custom_footer/blocks_footer_simple/bloco_4');
  
  if (!$confBlock4){
    $sql4 = "
      DELETE FROM core_config_data WHERE path = 'custom_footer/blocks_footer_simple/bloco_4';
      INSERT INTO 
        `core_config_data` (`scope`, `scope_id`, `path`, `value`)
      VALUES
        ('default', 0, 'custom_footer/blocks_footer_simple/bloco_4', '".$newBlock4->getId()."');    
    ";
  }

  $installer->run($sql4);
//--------------------------------------------------------------------------------------------------------

$installer->endSetup();