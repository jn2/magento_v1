<?php
/**
 * Reduz tempo de sessão
 */
$installer = $this;
$installer->startSetup();
$sql = "UPDATE core_config_data SET value = '3600' WHERE path = 'web/cookie/cookie_lifetime'";
$installer->run($sql);
$installer->endSetup();
