<?php
/**
 * Atualização do módulo Maxipago
 *
 * @author Raphael Mário
 *
 */
$installer = $this;
$installer->startSetup();

$allStores = Mage::app()->getStores();
foreach ($allStores as $store => $key){

   $storeId   = Mage::app()->getStore($key)->getStoreId();
   $storeCode = Mage::app()->getStore($store)->getCode();

   // Caso seja multi lojas
   $scope   = ($storeCode != 'default') ? 'stores' : 'default';
   $storeId = ($storeCode == 'default') ? 0 : $storeId;

   // Módulo header
   Mage::getModel('core/config')->saveConfig("payment/maxipagocheckoutapi_global_config/sellerId",  Mage::getStoreConfig('payment/maxipagocheckoutapi_creditcard/sellerId',$storeId ),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("payment/maxipagocheckoutapi_global_config/sellerKey", Mage::getStoreConfig('payment/maxipagocheckoutapi_creditcard/sellerKey',$storeId ),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("payment/maxipagocheckoutapi_global_config/secretKey", Mage::getStoreConfig('payment/maxipagocheckoutapi_creditcard/secretKey',$storeId ),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("payment/maxipagocheckoutapi_redepay/active", 0,$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("payment/maxipagocheckoutapi_global_config/sandbox", 0,$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("payment/maxipagocheckoutapi_global_config/order_status", 'pending',$scope, $storeId);
}

$installer->endSetup();
