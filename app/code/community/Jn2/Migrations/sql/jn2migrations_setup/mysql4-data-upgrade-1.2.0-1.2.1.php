<?php
$installer = $this;
$installer->startSetup();

$sql = <<<SQL
    UPDATE core_config_data
    SET value = '0'
    WHERE path = 'wishlist/general/active'
    AND DATABASE() <> 'twomag_p'
    AND DATABASE() <> 'teor_p'
SQL;

$installer->run($sql);
$installer->endSetup();
