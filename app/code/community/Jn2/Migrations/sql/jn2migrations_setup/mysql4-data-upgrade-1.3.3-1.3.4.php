<?php
/**
 * Verifica se  Campo Celular esta ativo no onestepcheckout e desativa devido a alteracoes no register.phtml
 */
$installer = $this;
$installer->startSetup();

$is_fax = Mage::getStoreConfigFlag('onestepcheckout/exclude_include_fields/is_fax');
if ($is_fax == '1') {
    Mage::getModel('core/config')->saveConfig("onestepcheckout/exclude_include_fields/is_fax", '0');
    Mage::app()->getCacheInstance()->clean();
}

$installer->endSetup();
