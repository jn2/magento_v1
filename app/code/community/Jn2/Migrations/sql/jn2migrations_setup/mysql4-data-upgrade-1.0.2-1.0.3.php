<?php
/**
 * habilita telefone obrigatório no simplecheckout
 *
 * @author Raphael Mário
 *
 */
$installer = $this;
$installer->startSetup();

$allStores = Mage::app()->getStores();
foreach ($allStores as $store => $key){
   $storeId = Mage::app()->getStore($key)->getStoreId();
   $storeCode = Mage::app()->getStore($store)->getCode();

   // Caso seja multi lojas
   $scope   = ($storeCode != 'default') ? 'stores' : 'default';
   $storeId = ($storeCode == 'default') ? 0 : $storeId;

   // Habilitar campo telefone como obrigatório
   Mage::getModel('core/config')->saveConfig("jn2_checkout/general/phone", 1,$scope, $storeId);
}

$installer->endSetup();
