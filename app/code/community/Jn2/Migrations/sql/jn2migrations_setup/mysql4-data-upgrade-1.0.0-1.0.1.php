<?php
/**
 * Desativa backup nativo do magento.
 * 
 * @author Reinaldo Mendes<reinaldorock@gmail.com>
 * 
 */
$installer = $this;
/* @var $installer  Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer->startSetup();

$con = $installer->getConnection();
/* @var $con Varien_Db_Adapter_Interface */
$con->beginTransaction();
$configCollection = Mage::getResourceModel('core/config_data_collection');
/* @var $configCollection Mage_Core_Model_Resource_Config_Data_Collection */
$configCollection->addFieldToFilter('path', array('eq', 'system/backup/enabled'));
foreach ($configCollection as $config) {
    $config->setValue(0);
    $config->save();
}
$con->commit();

Mage::app()->getCacheInstance()->clean();
        
        
$installer->endSetup();
