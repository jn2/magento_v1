<?php
/**
 * Desabilita módulo stelo por default
 *
 * @author Raphael Mário
 *
 */
$installer = $this;
$installer->startSetup();
$sql = "UPDATE core_config_data SET value = 0 WHERE path = 'system/smtp/set_return_path'";
$installer->run($sql);
$installer->endSetup();
