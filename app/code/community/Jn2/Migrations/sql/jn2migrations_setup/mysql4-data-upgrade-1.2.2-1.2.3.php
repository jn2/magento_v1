<?php
$installer = $this;
$installer->startSetup();


//Isso foi criado devido a uso do modman, porque sem essa configuração os módulos não funcionam
//As duas querys as seguir irão a apagar a configuração de symlinks caso ela exista e recriá-la habilitando os symlinks

    $sql = "
        DELETE FROM core_config_data 
        WHERE path = 'dev/template/allow_symlink';    
    ";
    $installer->run($sql);

    $sql2 = "
        INSERT INTO core_config_data (config_id, scope, scope_id, path, value) 
        VALUES (NULL , 'default', '0', 'dev/template/allow_symlink', '1')
    ";    
    $installer->run($sql2);

//Essa query é para apagar atributos que o módulo da mundipagg cria porém não são utilizados e geram problemas se o módulo for retirado da loja

    $sql3 = "
        DELETE FROM eav_attribute 
        WHERE attribute_code 
        LIKE '%mundipagg%';    
    ";
    $installer->run($sql3);


$installer->endSetup();