<?php
/**
 * Verifica se Mercado Pago está ativo e com chave que vem do banco. Se estiver, desativa.
 */
$installer = $this;
$installer->startSetup();

$clientId = Mage::getStoreConfigFlag('payment/mercadopago_standard/client_id');
if ($clientId == '7674935995189970') {
    Mage::getModel('core/config')->saveConfig("payment/mercadopago_standard/active", '0');
}

$installer->endSetup();
