<?php
/**
 * Reconfigura Cron de acordo com as configurações do servidor (30 em 30 minutos)
 */
$installer = $this;
$installer->startSetup();

// Configurações gerais
Mage::getModel('core/config')->saveConfig("system/cron/schedule_generate_every", '30');
Mage::getModel('core/config')->saveConfig("system/cron/schedule_ahead_for", '150');
Mage::getModel('core/config')->saveConfig("system/cron/schedule_lifetime", '180');
Mage::getModel('core/config')->saveConfig("system/cron/history_cleanup_every", '60');
Mage::getModel('core/config')->saveConfig("system/cron/history_success_lifetime", '1440');
Mage::getModel('core/config')->saveConfig("system/cron/history_failure_lifetime", '1440');
Mage::getModel('core/config')->saveConfig("system/cron/mark_as_error_after", '180');
Mage::getModel('core/config')->saveConfig("system/cron/max_job_runtime", '180');
Mage::getModel('core/config')->saveConfig("system/cron/scheduler_cron_expr_heartbeat", '*/30 * * * *');

// Reconfigurando jobs com tempo inferior a 30 minutos
Mage::getModel('core/config')->saveConfig("crontab/jobs/asyncindex/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/stockreleaser_cancel/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/stockreleaser_clean_completed_orders/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/aw_all_cron/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/captcha_delete_expired_images/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/core_email_queue_send_all/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/ebizmarts_abandoned_cart/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/mailchimp_bulksync_ecommerce_data/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/mailchimp_bulksync_subscriber_data/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/mailchimp_process_webhook_data/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/newsletter_send_all/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/order_status_inspector/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/captcha_delete_old_attempts/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/correios_status_check/schedule/cron_expr", '*/30 * * * *');
Mage::getModel('core/config')->saveConfig("crontab/jobs/searchsphinx_reindex_delta_job/schedule/cron_expr", '*/30 * * * *');

$installer->endSetup();
