<?php
/**
 * Desabilita módulo stelo por default
 *
 * @author Raphael Mário
 *
 */
$installer = $this;
$installer->startSetup();
/*** Update customer address attributes*/

$installer->updateAttribute('catalog_product', 'mundipagg_frequency_enum', 'is_required', false);
$installer->updateAttribute('catalog_product','mundipagg_frequency_enum', 'is_visible_on_front', false);
$installer->updateAttribute('catalog_product','mundipagg_frequency_enum', 'used_in_product_listing',false);

$installer->updateAttribute('catalog_product','mundipagg_recurrences', 'is_required', false);
$installer->updateAttribute('catalog_product','mundipagg_recurrences', 'is_visible_on_front', false);
$installer->updateAttribute('catalog_product','mundipagg_recurrences', 'used_in_product_listing', false);

$installer->updateAttribute('catalog_product','mundipagg_recurrent', 'is_required', false);
$installer->updateAttribute('catalog_product','mundipagg_recurrent', 'is_visible_on_front', false);
$installer->updateAttribute('catalog_product','mundipagg_recurrent', 'used_in_product_listing', false);

$installer->endSetup();
