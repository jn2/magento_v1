<?php
/**
 * Importa configurações antigas
 *
 * @author Raphael Mário
 *
 */
$installer = $this;
$installer->startSetup();

$allStores = Mage::app()->getStores();
foreach ($allStores as $store => $key){

   $storeId = Mage::app()->getStore($key)->getStoreId();
   $storeCode = Mage::app()->getStore($store)->getCode();

   // Caso seja multi lojas
   $scope   = ($storeCode != 'default') ? 'stores' : 'default';
   $storeId = ($storeCode == 'default') ? 0 : $storeId;

   // Módulo header
   Mage::getModel('core/config')->saveConfig("custom_header/template/active", Mage::getStoreConfig('custom_theme/header/active',$storeId ),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_header/template/theme", Mage::getStoreConfig('custom_theme/header/theme',$storeId ),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_header/blocks/logo", Mage::getStoreConfig('custom_theme/general/logo',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_header/blocks/search_text", Mage::getStoreConfig('custom_theme/general/search_text',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_header/blocks/block_1", Mage::getStoreConfig('custom_theme/header/block_1',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_header/blocks/block_2", Mage::getStoreConfig('custom_theme/header/block_2',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_header/blocks/block_3", Mage::getStoreConfig('custom_theme/header/block_3',$storeId),$scope, $storeId);

   // Módulo megamenu
   Mage::getModel('core/config')->saveConfig("megamenu/template/active", Mage::getStoreConfig('megamenu/config/active',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("megamenu/template/items_per_line", Mage::getStoreConfig('megamenu/config/items_per_line',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("megamenu/template/theme", Mage::getStoreConfig('megamenu/config/theme',$storeId),$scope, $storeId);

   // Módulo home
   Mage::getModel('core/config')->saveConfig("custom_home/template/active", Mage::getStoreConfig('custom_theme/home/active',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/template/home_theme", Mage::getStoreConfig('custom_theme/home/home_theme',$storeId),$scope, $storeId);

   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_1", Mage::getStoreConfig('custom_theme/home/bloco_1',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_2", Mage::getStoreConfig('custom_theme/home/bloco_2',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_3", Mage::getStoreConfig('custom_theme/home/bloco_3',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_4", Mage::getStoreConfig('custom_theme/home/bloco_4',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_5", Mage::getStoreConfig('custom_theme/home/bloco_5',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_6", Mage::getStoreConfig('custom_theme/home/bloco_6',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_7", Mage::getStoreConfig('custom_theme/home/bloco_7',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_8", Mage::getStoreConfig('custom_theme/home/bloco_8',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_9", Mage::getStoreConfig('custom_theme/home/bloco_9',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_10", Mage::getStoreConfig('custom_theme/home/bloco_10',$storeId),$scope, $storeId);

   Mage::getModel('core/config')->saveConfig("custom_home/blocks/bloco_1_fluid", Mage::getStoreConfig('custom_theme/home/first_row_fluid',$storeId),$scope, $storeId);

   Mage::getModel('core/config')->saveConfig("custom_home/blocks/products_1", Mage::getStoreConfig('custom_theme/home/products_1',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/products_show_category_1", Mage::getStoreConfig('custom_theme/home/products_show_category_1',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/qty_product_1", Mage::getStoreConfig('custom_theme/home/qty_product_1',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/column_product_1", Mage::getStoreConfig('custom_theme/home/column_product_1',$storeId),$scope, $storeId);

   Mage::getModel('core/config')->saveConfig("custom_home/blocks/products_2", Mage::getStoreConfig('custom_theme/home/products_2',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/products_show_category_2", Mage::getStoreConfig('custom_theme/home/products_show_category_2 ',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/qty_product_2", Mage::getStoreConfig('custom_theme/home/qty_product_2 ',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/column_product_2", Mage::getStoreConfig('custom_theme/home/column_product_2',$storeId),$scope, $storeId);

   Mage::getModel('core/config')->saveConfig("custom_home/blocks/products_3", Mage::getStoreConfig('custom_theme/home/products_3',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/products_show_category_3", Mage::getStoreConfig('custom_theme/home/products_show_category_3 ',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/qty_product_3", Mage::getStoreConfig('custom_theme/home/qty_product_3 ',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_home/blocks/column_product_3", Mage::getStoreConfig('custom_theme/home/column_product_3',$storeId),$scope, $storeId);

   // Módulo página do produto
   Mage::getModel('core/config')->saveConfig("product_page/template/active", 1 ,$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("product_page/template/theme", Mage::getStoreConfig('product_page/page/theme',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("product_page/general/active_product_info_bar", Mage::getStoreConfig('product_page/page/active_product_info_bar',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("product_page/general/active_product_option_empty", Mage::getStoreConfig('product_page/page/active_product_option_empty',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("product_page/blocks/bloco_1", Mage::getStoreConfig('product_page/page/social_proof1',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("product_page/blocks/bloco_2", Mage::getStoreConfig('product_page/page/social_proof2',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("product_page/blocks/bloco_3", Mage::getStoreConfig('product_page/page/seal1',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("product_page/blocks/bloco_4", Mage::getStoreConfig('product_page/page/seal2',$storeId),$scope, $storeId);

   // Módulo de Footer
   Mage::getModel('core/config')->saveConfig("custom_footer/template/active", Mage::getStoreConfig('custom_theme/footer/active',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/template/theme", Mage::getStoreConfig('custom_theme/footer/footer_theme',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/blocks/bloco_1", Mage::getStoreConfig('custom_theme/footer/bloco_1',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/blocks/bloco_2", Mage::getStoreConfig('custom_theme/footer/bloco_2',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/blocks/bloco_3", Mage::getStoreConfig('custom_theme/footer/bloco_3',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/blocks/bloco_4", Mage::getStoreConfig('custom_theme/footer/bloco_4',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/blocks/bloco_5", Mage::getStoreConfig('custom_theme/footer/bloco_5',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/blocks/bloco_6", Mage::getStoreConfig('custom_theme/footer/bloco_6',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/blocks/bloco_7", Mage::getStoreConfig('custom_theme/footer/bloco_7',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/blocks/bloco_8", Mage::getStoreConfig('custom_theme/footer/bloco_8',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/template_newsletter/active", Mage::getStoreConfig('custom_theme/footer/newsletter',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/template_newsletter/newsletter_desc", Mage::getStoreConfig('custom_theme/footer/newsletter_desc',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/template_newsletter/newsletter_desc_loja", Mage::getStoreConfig('custom_theme/footer/store_desc',$storeId),$scope, $storeId);
   Mage::getModel('core/config')->saveConfig("custom_footer/blocks/logo", Mage::getStoreConfig('custom_theme/general/logo_footer',$storeId),$scope, $storeId);

   // Módulo de Grid de Produto
   Mage::getModel('core/config')->saveConfig("product_grid/general/show_button", Mage::getStoreConfig('custom_theme/general/show_button',$storeId),$scope, $storeId);
}

$installer->endSetup();
