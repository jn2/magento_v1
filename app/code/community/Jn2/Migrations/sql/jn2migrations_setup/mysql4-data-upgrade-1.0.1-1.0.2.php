<?php
/**
 * Desabilita módulo stelo por default
 *
 * @author Raphael Mário
 *
 */
$installer = $this;
$installer->startSetup();
$sql = "Update core_config_data
        set value = 0
        where
             path = 'payment/sub/active' OR path = 'payment/subBoleto/active';";
$installer->run($sql);
$installer->endSetup();
