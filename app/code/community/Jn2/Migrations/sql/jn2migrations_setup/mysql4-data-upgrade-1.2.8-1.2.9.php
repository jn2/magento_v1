<?php
/**
 * Title
 *
 * @category
 * @package
 * @author      Gustavo Bacelar <gustavo@jn2.com.br>
 * @description Magento não foi atualizado corretamente, por isso foi necessário rodar um migration separadamente.
 */

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

$tableName = $installer->getTable('customer/eav_attribute');
$columnName = 'password_created_at';

//Verifica se o atributo ja esta instalado pois o magento não foi atualizado corretamente
if ($connection->tableColumnExists($tableName, $columnName) === false) {

    $installer->addAttribute('customer', 'password_created_at', array(
        'label'    => 'Password created at',
        'visible'  => false,
        'required' => false,
        'type'     => 'int'
    ));

}

$installer->endSetup();