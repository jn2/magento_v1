<?php

$installer = $this;
$installer->startSetup();

$allStores = Mage::app()->getStores();
foreach ($allStores as $store => $key){

    $storeId   = Mage::app()->getStore($key)->getStoreId();
    $storeCode = Mage::app()->getStore($store)->getCode();

    // Caso seja multi lojas
    $scope   = ($storeCode != 'default') ? 'stores' : 'default';
    $storeId = ($storeCode == 'default') ? 0 : $storeId;
    Mage::getModel('core/config')->saveConfig("crontab/jobs/catalogrule_apply_all/schedule/cron_expr", '*/30 * * * *',$scope, $storeId);
}

$sql = "UPDATE index_process SET mode = 'manual' WHERE mode = 'real_time'";
$installer->run($sql);
$installer->endSetup();
