<?php
/**
 *
 * @category   Jn2
 * @package    Jn2_BuscapeBp
 * @author     Raphael Mário <raphaellmario@gmail.com>
 */

class Jn2_BuscapeBp_Helper_Connection extends Mage_Core_Helper_Abstract
{
	/* Types */
	const POST 	= 'POST';
	const GET 	= 'GET';
	const PUT 	= 'PUT';

	/* Urls do Buscape */
	const PRODUCAO = 'http://api.buscape.com.br/';
	const SANDBOX  = 'http://sandbox-api.buscape.com.br/';

	/* Variables of class */
	private $appToken;
	private $authToken;
	private $idEbit;
	private $nameStoreEbit;
	private $url;

	private $user_agent;

	public function __construct()
	{
		$this->appToken      = Mage::getStoreConfig('buscape/config/app_token');
		$this->authToken     = Mage::getStoreConfig('buscape/config/auth_token');
		$this->idEbit  			 = Mage::getStoreConfig('buscape/config/ebit_id');
		$this->nameStoreEbit = Mage::getStoreConfig('buscape/config/ebit_name');
		$this->user_agent    ='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36';
	}

	/**
	 * Fornece a url correta com base na configuração feita pelo usuário.
	 * @return string
	 */
	public function getEnvironment()
	{
		if (Mage::getStoreConfig('buscape/config/sandBox')){
			return Jn2_BuscapeBp_Helper_Connection::SANDBOX;
		}else{
			return Jn2_BuscapeBp_Helper_Connection::PRODUCAO;
		}
	}

	/**
	 * Recebe alguns parâmetros que especifica que tipo de conexão será feita com a API buscapé
	 * @param	string
	 * @param	const
	 * @param	array
	 * @param	string
	 * @return json
	 */
	public function getConnection($path, $type, $array = array(), $key = null)
	{
		$header = array(
			'Content-Type: application/json; charset=utf-8',
			"app-token:  {$this->appToken}" ,
			"auth-token: {$this->authToken}"
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->getEnvironment() . $path . (is_null($key) ? '' : "/{$key}"));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		if(count($array) > 0){
			curl_setopt($ch, CURLOPT_POSTFIELDS, $array);
		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		$result = curl_exec($ch);
		curl_close($ch);

		//Registra retorno no log
		Mage::log('Resposta API buscape: ' . $result);
		Mage::log('Lista enviada: ' . $array);
		return json_decode($result);
	}

}
