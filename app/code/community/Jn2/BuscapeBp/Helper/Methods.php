<?php
/**
 *
 * @category   Jn2
 * @package    Jn2_BuscapeBp
 * @author     Raphael Mário <raphaellmario@gmail.com>
 */
class Jn2_BuscapeBp_Helper_Methods extends Mage_Core_Helper_Abstract
{
	/* Methods to Buscape - API */
	private $collection = 'product/t1/collection';
	private $inventory = 'product/t1/inventory';
	private $search = 'product/search';
	private $order = 'collection/session/cart';

/**
 * Responsável por enviar os produtos para a api do buscapé
 * @param	array
 * @return array
 */
	public function postCollection($products) {
		$listProducts = Mage::helper("buscapebp/formatJson")->getProductsJson($products);
		$results = array();
		if(count($listProducts) > 1 ){
			foreach ($listProducts as $productJson) {
				$result = Mage::helper("buscapebp/connection")->getConnection(
					$this->collection, Jn2_BuscapeBp_Helper_Connection::POST, $productJson
				);
				array_push($results,$result);
			}
			return $results;
		}else{
			return Mage::helper("buscapebp/connection")->getConnection(
				$this->collection,
				Jn2_BuscapeBp_Helper_Connection::POST,
				$listProducts
			);
		}
	}

	/**
	 * Responsável por enviar dados do produto para a api do buscapé atualizar informações
	 * @param	array
	 * @return array
	 */
	public function putInventory($product) {
		return Mage::helper("buscapebp/connection")->getConnection(
			$this->inventory,
			Jn2_BuscapeBp_Helper_Connection::PUT,
			Mage::helper("buscapebp/formatJson")->getInventory($product)
		);
	}

	/**
	 * Responsável por busca todos os produtos ou apenas um produto pelo seu sku
	 * @param	string
	 * @return array
	 */
	public function getSearch($sku = null) {
		return Mage::helper("buscapebp/connection")->getConnection(
			$this->search,
			Jn2_BuscapeBp_Helper_Connection::GET,array(),
			$sku
		);
	}

	/**
     * Responsável por enviar dados do pedido concluído para a API do Buscapé.
     * @param $order
     * @return array
     */
    public function postOrder($order)
    {
        return Mage::helper('buscapebp/connection')->getConnection(
            $this->order,
            Jn2_BuscapeBp_Helper_Connection::POST,
            Mage::helper('buscapebp/formatJson')->formatOrder($order)
        );
    }

}
