<?php
/**
*
* @category   Jn2
* @package    Jn2_BuscapeBp
* @author     Raphael Mário <raphaellmario@gmail.com>
*/
class Jn2_BuscapeBp_Helper_FormatJson extends Mage_Core_Helper_Abstract
{
	// Max number of products request by Buscapé
	const MAX = 500;

	/**
	* Responsável por buscar todos os produtos cadastrados
	* @return array
	*/
	public function getAllProducts()
	{
		$_allProducts = array();
		$_collection  = Mage::getResourceModel('catalog/product_collection')->addAttributeToSelect('price')->addFinalPrice();
		return $_collection;
	}

	/**
	* Recebe um produto e formata ele no padrão buscapé para atualizar dados
	* @param	object
	* @return json
	*/
	public function getInventory($product)
	{
		$id   	  = $product->getId();
		$product  = Mage::getModel('catalog/product')->load($id);
		$stock    = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
		$_product = array(
			'sku' 	   => "2799",//$product->getSKU(),
			'prices' 	 => json_encode($this->getPrices($product)),
			'quantity' => number_format($stock->getQty(),0)
		);

		return '[' . json_encode($_product) .']';
	}

	/**
	* Recebe um array de produtos e chama a função que formata eles no padrão da API buscapé
	* @param	array
	* @return array
	*/
	public function getProductsJson($_collection)
	{
		$count = 0;
		foreach($_collection as $product)
		{
			$product = Mage::getModel('catalog/product')->load($product->getId());
			if($count < self::MAX && $product->getData('send_buscape')) {
				$_allProducts[] = $this->formatProduct($product);
				$count++;
			}elseif($count >= self::MAX){
				$listProducts[] = json_encode($_allProducts);
				unset($_allProducts);
				$_allProducts[] = $this->formatProduct($product);
				$count = 1;
			}
		}
		if(empty($listProducts)){
			return json_encode($_allProducts);
		}else{
			$listProducts[] = json_encode($_allProducts);
			return $listProducts;
		}
	}

	/**
     * Recebe um pedido e formata no padrão esperado pela API do Buscapé
     *
     * @param $order
     * @return string
     */
    public function formatOrder($order)
    {
        $genders = array(0 => '', 1 => 'm', 2 => 'f');

        $json = array(
            'email' => $order->getCustomer()->getEmail(),
            'gender' => $order->getCustomer()->getGender() ? $genders[(int) $order->getCustomer()->getGender()] : '',
            'birthDate' => $order->getCustomer()->getDob() ? (new DateTime($order->getCustomer()->getDob()))->format("d/m/Y") : '',
            'zipCode' => $order->getBillingAddress()->getPostcode(),
            'parcels' => max((int) $order->getPayment()->getInstallments(), 1),
            'deliveryTax' => number_format($order->getShippingAmount(), 2),
            'totalSpent' => number_format($order->getGrandTotal(), 2),
            'storeId' => Mage::getStoreConfig('buscape/config/store_id'),
            'customerId' => Mage::helper('buscapebp')->getCustomerId(),
            'transactionId' => $order->getIncrementId(),
            'items' => array(),
        );

        foreach ($order->getAllVisibleItems() as $item) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());

            $json['items'][] = array(
                'sku' => $item->getSku(),
                'quantity' => $item->getQtyOrdered(),
                'value' => number_format($item->getPrice() - $item->getDiscountAmount(), 2),
                'productName' => $item->getName(),
                'ean' => $product->getBarcode(),
            );
        }

        return json_encode($json);
    }

	/**
	* Recebe um um produto e formata ele no padrão da API buscapé
	* @param	object
	* @return array
	*/
	private function formatProduct($product)
	{
		$stock   = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);

		if($product->getTypeId() == "simple"){
			$parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($product->getId());
			if(!$parentIds)
			$parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
			if(isset($parentIds[0])){
				$parent = Mage::getModel('catalog/product')->load($parentIds[0]);
			}
		}

		$produtoBp = array(
			//Campos Obrigatórios
			'sku' 				=> $product->getSKU(),
			'title'       => $product->getName(),
			'category'    => (is_null($product->getData('category_buscape'))) ? 'HOME' : $product->getData('category_buscape'),
			'description' => $product->getData('description_buscape'),
			'images'		  => $this->getImages($product),
			'link' 			  => $product->getProductUrl(),
			'prices'		  => $this->getPrices($product),
			'technicalSpecification' =>  $this->getTechnicalSpecification($product),
			'quantity' 	  => number_format($stock->getQty(),0),
			'sizeHeight'  => (is_null($product->getData('volume_altura'))) ? 0 : $product->getVolume_altura(),
			'sizeLength'  => (is_null($product->getData('volume_comprimento'))) ? 0 : $product->getVolume_comprimento(),
			'sizeWidth'   => (is_null($product->getData('volume_largura'))) ? 0 : $product->getVolume_largura(),
			'weightValue' => (is_null($product->getWeight())) ? 0 : $product->getWeight(),

			//Campos opcionais
			'groupId'		    => (isset($parent)) ? $parent->getEntityId() : '',
			'barcode'		    => (is_null($product->getData('barcode'))) ? '' : $product->getData('barcode'),
			'isbn'		        => (is_null($product->getData('isbn'))) ? '' : $product->getData('isbn'),
			'linkLomadee'	    => (is_null($product->getData('linkLomadee'))) ? '' : $product->getData('linkLomadee'),
			'handlingTimeDays'  =>  (is_null($product->getData('handlingTimeDays'))) ? 0 : $product->getData('handlingTimeDays'),
			//O valor de marketplace sempre será false segundo a equipe do buscapé.
			'marketplace'       =>  /*(is_null($product->getData('marketplace'))) ? 0 :*/ 'false',
			'marketplaceName'   =>  (is_null($product->getData('marketplaceName'))) ? '' : $product->getData('marketplaceName'),
		);

		// Se não possuir atributos não deve enviar
		$attProduct = $this->getAttributesProduct($product);
		if(count($attProduct) > 0){
			$produtoBp['productAttributes'] = $attProduct;
		}

		return $produtoBp;
	}

	/**
	* Recebe um um produto e busca suas imagens cadastradas
	* @param	object
	* @return array
	*/
	private function getImages($product)
	{
		$imagens = array();
		foreach ($product->getMediaGalleryImages() as $image) {
			$imagens[] = $image->getUrl();
		}
		return $imagens;
	}

	/**
	* Recebe um um produto, busca seus preços e formata ele no padrão da API buscapé
	* @param	object
	* @return array
	*/
	private function getPrices($product)
	{
		$parcels = max(Mage::getStoreConfig('buscape/config/num_parcels'),1);
		$value   = max($product->getFinalPrice(), 1);

		$prices[] = array(
			'type' 					   => 'cartao_avista',
			'price'						 => number_format((float) $value, 1, '.', ''),
			'priceLomadee'     => number_format((float) $value, 1, '.', ''),
			'installment'			 => 1,
			'installmentValue' => number_format((float) $value, 1, '.', ''),
		);

		$prices[] = array(
			'type' 					   => 'cartao_parcelado_sem_juros',
			'price'						 => number_format((float) $value, 1, '.', ''),
			'priceLomadee'     => number_format((float) $value, 1, '.', ''),
			'installment'			 => 1,
			'installmentValue' => number_format((float) $value/$parcels, 1, '.', ''),
		);

		return $prices;
	}

	// /**
	// * Recebe um um produto, busca seus preços e formata ele no padrão da API buscapé
	// * @param	object
	// * @return array
	// */
	// private function getPrices($product)
	// {
	// 	$prices = array();
	// 	foreach (Mage::helper('detailedprice')->getPaymentValues($product) as $payment) {
	// 		$price = array(
	// 			'type' 					   => $payment['type'],
	// 			'price'						 => number_format($payment['price'], 1, '.', ''),
	// 			'priceLomadee'     => 0,
	// 			'installment'			 => number_format($payment['installments'], 1, '.', ''),
	// 			'installmentValue' => number_format($payment['installment_value'], 1, '.', ''),
	// 		);
	// 		$prices[] = $price;
	// 	}
	// 	return $prices;
	// }

	/**
	* Recebe um produto e monta a lista de atributos do produto no padrão da API buscapé
	* @param	object
	* @return array
	*/
	private function getAttributesProduct($product)
	{
		$atributos = array();
		$attr = Mage::getStoreConfig('buscape/config/attributes');
		$attr = explode(',',$attr);

		if(count($attr) > 0){
			foreach ($attr as $value) {
				if(!is_null($product->getData($value))){
					$result = $this->getAttributeText($product,$value);
					if($result){
						$atributos[$value] = $result;
					}
				}
			}
		}
		return $atributos;
	}

	/**
	* Recebe um produto e monta a lista de atributos da especificação técnica no padrão da API buscapé
	* @param	object
	* @return array
	*/
	private function getTechnicalSpecification($product)
	{
		// Envia sku por default
		$atributos = array('sku' => $product->getSKU());
		$attr = Mage::getStoreConfig('buscape/config/technicalSpecification');
		$attr = explode(',',$attr);

		if(count($attr) > 0){
			foreach ($attr as $value) {
				$result = $this->getAttributeText($product,$value);
				if($result){
					$atributos[$value] = $result;
				}elseif(!is_null($product->getData($value))){
					$atributos[$value] = $product->getData($value);
				}
			}
		}
		return $atributos;
	}

	/**
	* Método para para sobrescrever o getAttributeText do produto
	* @param	object
	* @param	int
	* @return string
	*/
	private function getAttributeText($product,$attributeCode)
	{
		if ($attribute = $product->getResource()->getAttribute($attributeCode)) {
			return $attribute->getSource()->getOptionText($product->getData($attributeCode));
		} else {
			return false;
		}
	}
}
