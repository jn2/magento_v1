<?php

/**
 * Class Jn2_BuscapeBp_Helper_Data
 *
 * @author Thiago Machado <thiago.machado@jn2.com.br>
 * @version 1.0
 */
class Jn2_BuscapeBp_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SESSION_CUSTOMER_ID_KEY = 'buscapebp_customer_id';

    /**
     * Retorna o Customer ID do usuário que será usado no Buscapé
     * @return string
     */
    public function getCustomerId()
    {
        $customer_session = Mage::getSingleton('customer/session');

        if (!$customer_session->hasData(self::SESSION_CUSTOMER_ID_KEY)) {
            $customer_session->setData(self::SESSION_CUSTOMER_ID_KEY, md5((new DateTime())->getTimestamp() . rand(0, 1000000)));
        }

        return $customer_session->getData(self::SESSION_CUSTOMER_ID_KEY);
    }

    /**
     * Limpa o Customer ID do usuário da sessão após o fim do pedido.
     */
    public function clearCustomerId()
    {
        $customer_session = Mage::getSingleton('customer/session');
        $customer_session->unsetData(self::SESSION_CUSTOMER_ID_KEY);
    }
}
