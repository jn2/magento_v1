<?php

/**
 * Class Jn2_BuscapeBp_Block_Frame
 *
 * @author Thiago Machado <thiago.machado@jn2.com.br>
 * @version 0.2.0
 */
class Jn2_BuscapeBp_Block_Frame extends Mage_Core_Block_Template
{
    const BASE_URL = 'https://o.lomadee.com/loc/session/%s?customerid=%s';

    /**
     * Retorna a URL do iframe do Buscapé
     * @return string
     */
    public function getFrameUrl()
    {
        return sprintf(
            self::BASE_URL,
            Mage::getStoreConfig('buscape/config/store_id'),
            Mage::helper('buscapebp')->getCustomerId()
        );
    }

}
