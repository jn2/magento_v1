<?php
/**
 *
 * @category   Jn2
 * @package    Jn2_BuscapeBp
 * @author     Raphael Mário <raphaellmario@gmail.com>
 */

$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$codigo = 'send_buscape';
$config = array(
	'position' => 1,
	'required' => 0,
	'label'    => 'Enviar para o buscapé?',
	'type'     => 'int',
	'input'    => 'boolean',
	'apply_to' => 'simple,bundle,grouped,configurable',
	'note'	   => 'Ao selecionar SIM, toda vez que for enviado os produtos para o buscapé ele será enviado.
	Também quando salvo, será atualizado os valores de preço no buscapé.'
);
$setup->addAttribute('catalog_product', $codigo, $config);

$codigo = 'barcode';
$config = array(
	'position' => 1,
	'required' => 0,
	'label'    => 'Código de barras',
	'type'     => 'varchar',
	'input'    => 'text',
	'apply_to' => 'simple,bundle,grouped,configurable',
	'note' 		 => 'Código de barras da produto. (Tamanho máx 240 caracteres). Somente números.'
);
$setup->addAttribute('catalog_product', $codigo, $config);

$codigo = 'category_buscape';
$config = array(
	'position' => 1,
	'required' => 1,
	'label'    => 'Categorias',
	'type'     => 'varchar',
	'input'    => 'text',
	'apply_to' => 'simple,bundle,grouped,configurable',
	'note' 		 => 'Categoria que o produto se encontra. (Exemplo. Eletrônicos>TV) (Tamanho máx. 255 caracteres)'
);
$setup->addAttribute('catalog_product', $codigo, $config);

$codigo = 'handlingTimeDays';
$config = array(
	'position' => 1,
	'required' => 0,
	'label'    => 'Tempo de manuseio',
	'type'     => 'varchar',
	'input'    => 'text',
	'apply_to' => 'simple,bundle,grouped,configurable',
	'note'     => 'Tempo de manuseamento do produto. Deve ser acrescido ao prazo de frete do produto.
	(Exemplo. Tempo necessário para encordoar uma raquete de tênis)'
);
$setup->addAttribute('catalog_product', $codigo, $config);

$codigo = 'isbn';
$config = array(
	'position' => 1,
	'required' => 0,
	'label'    => 'ISBN',
	'type'     => 'varchar',
	'input'    => 'text',
	'apply_to' => 'simple,bundle,grouped,configurable',
	'note'     => 'Código ISBN para livros'
);
$setup->addAttribute('catalog_product', $codigo, $config);

$codigo = 'linkLomadee';
$config = array(
	'position' => 1,
	'required' => 0,
	'label'    => 'Link do produto para Lomadee',
	'type'     => 'varchar',
	'input'    => 'text',
	'apply_to' => 'simple,bundle,grouped,configurable',
	'note'     => 'Link do produto para os publishers da Lomadee. (Tamanho máx. 4094 caracteres)'
);
$setup->addAttribute('catalog_product', $codigo, $config);

$codigo = 'marketplace';
$config = array(
	'position' => 1,
	'required' => 0,
	'label'    => 'É vendido no modelo de marketplace ?',
	'type'     => 'int',
	'input'    => 'boolean',
	'apply_to' => 'simple,bundle,grouped,configurable',
	'note'     => 'Indica se esse produto é vendido no modelo de marketplace, isto é, não vendido pela loja.'
);
$setup->addAttribute('catalog_product', $codigo, $config);

$codigo = 'marketplaceName';
$config = array(
	'position' => 1,
	'required' => 0,
	'label'    => 'Nome da loja',
	'type'     => 'varchar',
	'input'    => 'text',
	'apply_to' => 'simple,bundle,grouped,configurable',
	'note'     => 'Informa o nome da loja que esta vendendo o produto no modelo de marketplace.'
);
$setup->addAttribute('catalog_product', $codigo, $config);

$setIds = $setup->getAllAttributeSetIds('catalog_product');

$attributes = array(
    'send_buscape',
    'barcode',
    'category_buscape',
    'declaredPrice',
    'handlingTimeDays',
    'isbn',
    'linkLomadee',
    'priceLomadee',
    'marketplace',
    'marketplaceName'
);

foreach ( $setIds as $setId ) {
    $setup->addAttributeGroup('catalog_product', $setId, 'Buscape', 3);
    $groupId = $setup->getAttributeGroupId('catalog_product', $setId, 'Buscape');
    foreach ( $attributes as $attribute ) {
        $attributeId = $setup->getAttributeId('catalog_product', $attribute);
        $setup->addAttributeToGroup('catalog_product', $setId, $groupId, $attributeId);
    }
}

$installer->endSetup();
