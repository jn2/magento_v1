<?php
/**
 *
 * @category   Jn2
 * @package    Jn2_BuscapeBp
 * @author     Josano Soares <josano@jn2.com.br>
 */

$installer = $this;
$installer->startSetup();

$codigo = 'description_buscape';

$config = array(
	'position'  => 1,
	'required'  => 1,
	'label'     => 'Descrição Buscapé',
	'type'      => 'text',
	'input'     => 'textarea',
	'apply_to'  => 'simple,bundle,grouped,configurable',
	'note' 	    => 'Descrição do produto no buscapé. (Tamanho máx. 4096 caracteres)',
	'group'     => 'Buscape'
);

$installer->addAttribute('catalog_product', $codigo, $config);

$installer->endSetup();