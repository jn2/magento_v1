<?php

/**
 *
 * @category   Jn2
 * @package    Jn2_BuscapeBp
 * @author     Raphael Mário <raphaellmario@gmail.com>
 */

class Jn2_BuscapeBp_IndexController extends Mage_Core_Controller_Front_Action{

  public function indexAction(){
    {
      try{
        // Busca os produtos habilitados a serem enviados para o buscape
        $products = Mage::helper("buscapebp/formatJson")->getAllProducts();
        // Realiza a chamada para a API buscape passando os produtos
        $results  = Mage::helper("buscapebp/methods")->postCollection($products);

        $totalProducts = 0;
        $totalSuccess  = 0;
        $totalErrors   = 0;

        // foreach ($results as $result) {
        //   foreach ($result as $object) {
        //     $totalProducts++;
        //     $product = json_decode(json_encode($object), True);
        //     if(isset($product['errors'])){
        //       $totalErrors++;
        //       // Salva o primeiro erro para apresentar o usuário
        //       if(isset($descriptionError)){
        //         $descriptionError = $product['errors'];
        //       }
        //     }elseif($product['status'] == 'SUCCESS'){
        //       $totalSuccess++;
        //     }
        //   }
        // }
        //
        // if($totalProducts == $totalSuccess ){
        //   $text = "Operação realizada com sucesso! {$totalSuccess} produto(s) integrados com Buscapé.";
        //   $type = 'success';
        // }elseif($totalErrors > 0){
        //   $text = "Erros ao integrar com o Buscapé! {$totalSuccess} produto(s) integrado(s), {$totalErrors} erro(s) de {$totalSuccess} produto(s) enviado(s).";
        //   $text .= "<br />";
        //   $text .= json_encode($descriptionError);
        //   $type = 'error';
        // }

        //$msg = '<ul class="messages"><li class="' . $type .'-msg"><ul><li>' . $text . '</li></ul></li></ul>';

        // echo '<script>';
        // echo    "window.opener.$('messages').update('{$msg}');";
        // echo    'window.close();';
        // echo '</script>';

        //Mage::getSingleton('core/session')->addSuccess('chegou');
        Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl("adminhtml/dashboard"));
        //return ;
      }catch(Exception $e){
        Mage::log(var_dump($e));
      }
    }
  }
}
