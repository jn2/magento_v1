<?php
class Jn2_BuscapeBp_Model_System_Config_Source_Attributes
{
	public static function toOptionArray()
	{
		$attributes = Mage::getModel('catalog/product')->getAttributes();
		$attributeArray = array();

		foreach($attributes as $a){
			foreach ($a->getEntityType()->getAttributeCodes() as $attributeName) {
				$attributeArray[] = array(
					'label' => $attributeName,
					'value' => $attributeName
				);
			}
			break;
		}
		return $attributeArray;
	}
}
