<?php

/**
 *
 * @category   Jn2
 * @package    Jn2_BuscapeBp
 * @author     Raphael Mário <raphaellmario@gmail.com>
 */
class Jn2_BuscapeBp_Model_Observer
{
    /**
     * Add botão enviar buscape em catalog/product
     * @param    object
     */
    public function addButtonBuscape($observer)
    {
        $container = $observer->getBlock();
        if (null !== $container && $container->getType() == 'adminhtml/catalog_product' && Mage::getStoreConfig('buscape/config/active')) {
            $data = array(
                'label' => 'Enviar Buscapé',
                'class' => 'scalable add',
                'onclick' => 'setLocation(\'' . Mage::getUrl('buscapebp/index') . '\')',
            );
            $container->addButton('button_send_buscape', $data);
        }
    }

    /**
     * Realiza uma chamada para a API buscapé enviando os dados atualizados do produto.
     * @param    object
     */
    public function afterSaveAttribute($observer)
    {
        $product = $observer->getEvent()->getProduct();
        if ($product->hasDataChanges()) {
            $product = Mage::getModel('catalog/product')->load($product->getId());
            if (Mage::getStoreConfig('buscape/config/active') && $product->getData('send_buscape')) {
                Mage::helper("buscapebp/methods")->putInventory($product);
            }
        }
    }

    /**
     * Realiza uma chamada para a API buscapé enviando os dados atualizados do produto após a compra.
     * @param    object
     */
    public function updateInventoryAfterOrder($observer)
    {
        $order = $observer->getOrder();
        $orderItems = $order->getAllVisibleItems();

        $products = array();
        foreach ($orderItems as $item) {
            $id = $item->getData('product_id');
            $products[] = Mage::getModel('catalog/product')->load($id);
        }

        foreach ($products as $product) {
            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            if ($stock->getQty() < 100) {
                $product = Mage::getModel('catalog/product')->load($product->getId());
                if (Mage::getStoreConfig('buscape/config/active') && $product->getData('send_buscape')) {
                    Mage::helper("buscapebp/methods")->putInventory($product);
                }
            }
        }
    }

    /**
     * Envia os dados do pedido concluído para a API do Buscapé.
     * @param $observer
     */
    public function notifyBuscapeCpa($observer)
    {
        if (Mage::getStoreConfig('buscape/config/cpa_charge')) {
            Mage::helper('buscapebp/methods')->postOrder($observer->getOrder());
            Mage::helper('buscapebp')->clearCustomerId();
        }
    }
}
