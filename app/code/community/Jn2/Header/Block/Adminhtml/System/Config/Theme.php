<?php

class Jn2_Header_Block_Adminhtml_System_Config_Theme extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * Override field method to add js
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return String
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        // Get the default HTML for this option
        $html = parent::_getElementHtml($element);
        $html = str_replace('title=','data-img-src=',$html);
        $html = str_replace('label=','data-img-label=',$html);
        $html .= '
        <script>
          jQuery("#custom_header_template_theme").imagepicker({show_label: true});
          jQuery("#custom_header_template_mini_cart_theme").imagepicker({show_label: true});
        </script>
        ';
        return $html;
    }
}
