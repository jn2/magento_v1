<?php
class Jn2_Header_Model_System_Config_Source_Themes
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(

            array('label' => 'Cabeçalho 1', 'value' => 'theme1', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w1.jpg')),
            array('label' => 'Cabeçalho 2', 'value' => 'theme2', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w2.jpg')),
            array('label' => 'Cabeçalho 3', 'value' => 'theme3', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w3.jpg')),
            array('label' => 'Cabeçalho 4', 'value' => 'theme4', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w4.jpg')),
            array('label' => 'Cabeçalho 5', 'value' => 'theme5', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w5.jpg')),
            array('label' => 'Cabeçalho 6', 'value' => 'theme6', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w6.jpg')),
            array('label' => 'Cabeçalho 7', 'value' => 'theme7', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w7.jpg')),
            array('label' => 'Cabeçalho 8', 'value' => 'theme8', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w8.jpg'))
        );
    }

}
