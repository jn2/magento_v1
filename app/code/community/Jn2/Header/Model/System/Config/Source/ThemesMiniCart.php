<?php
class Jn2_Header_Model_System_Config_Source_ThemesMiniCart
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(

            array('label' => 'Mini Cart 1', 'value' => 'theme1', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w1.jpg')),
            array('label' => 'Mini Cart 2', 'value' => 'theme2', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w2.jpg'))
        );
    }


}
