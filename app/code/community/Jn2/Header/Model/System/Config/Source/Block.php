<?php
class Jn2_Header_Model_System_Config_Source_Block
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    $block_collection = Mage::getResourceModel('cms/block_collection')
    ->load()
    ->toOptionArray();

    array_unshift($block_collection, array('value' => '', 'label' => ''));

    return $block_collection;
  }

}
