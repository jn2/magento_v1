<?php
class Jn2_Header_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function getTemplatePath() {

		if (Mage::getStoreConfig('custom_header/template/active')) {
			$theme = Mage::getStoreConfig('custom_header/template/theme');
			return "jn2/header/wireframes/{$theme}.phtml";
		} else {
			return '';
		}
	}

	public function getTemplatePathCart() {

		if (Mage::getStoreConfig('custom_header/template_mini_cart/active')) {
			$theme = Mage::getStoreConfig('custom_header/template_mini_cart/theme');
			return "jn2/header/minicart/{$theme}.phtml";
		} else {
			return '';
		}
	}

	public function getBlock($block_pos) {
		$block_id   = Mage::getStoreConfig("custom_header/blocks/block_{$block_pos}");

		if (!empty($block_id)) {
			$block      = Mage::app()->getLayout()->createBlock('cms/block')->setBlockId($block_id);
		} else {
			return '';
		}

		return $block->toHtml();
	}

	public function getLogoUrl(){
	    $logo_url = Mage::getBaseUrl()."media/logo/".Mage::getStoreConfig('custom_header/blocks/logo');
	    return $logo_url;
    }

}
