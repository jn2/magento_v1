<?php

class Jn2_ProductPage_Model_System_Config_Source_Themes {

  /**
   * Options getter
   *
   * @return array
   */
  public function toOptionArray()
  {
    return array(
        array('label' => 'Tema 1', 'value' => 'theme1', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w1.jpg')),
        array('label' => 'Tema 2', 'value' => 'theme2', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w2.jpg')),
        array('label' => 'Tema 3', 'value' => 'theme3', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w2.jpg'))
      );
  }
}
