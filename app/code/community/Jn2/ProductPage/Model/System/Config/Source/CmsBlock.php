<?php

class Jn2_ProductPage_Model_System_Config_Source_CmsBlock {
    protected $_options;

    public function toOptionArray()
    {


        if (!$this->_options) {
            $this->_options = Mage::getResourceModel('cms/block_collection')
                ->load()
                ->toOptionArray();

            array_unshift($this->_options, array('value' => '', 'label' => ''));
        }

        return $this->_options;
    }
}
