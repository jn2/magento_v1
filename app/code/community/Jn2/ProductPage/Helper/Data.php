<?php

class Jn2_ProductPage_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    public function getTemplatePath()
    {
        
        $theme = Mage::getStoreConfig( 'product_page/template/theme' );
        if ($theme) {
            return "jn2/productpage/catalog/product/themes/{$theme}.phtml";
        } else {
            return "jn2/productpage/catalog/product/view.phtml";
        }
    }
    
    public function attributeVideoExists()
    {
        $attribute = Mage::getModel( 'catalog/resource_eav_attribute' )->loadByCode( 'catalog_product', 'jn2_product_video' );
        
        if (null === $attribute->getId()) {
            return false;
        } else {
            return true;
        }
    }
    
    public function getEmbed($url)
    {
        if (strpos($url, 'vimeo')) {
            $vimeo_code = substr(parse_url($url, PHP_URL_PATH), 1);
            return "<script src='https://player.vimeo.com/api/player.js'></script>
                    <iframe src='https://player.vimeo.com/video/{$vimeo_code}?loop=1&autoplay=0' frameborder='0' allow='autoplay;' ></iframe>
                    
                    <script>
                        var iframe  = document.querySelector('iframe');
                        var player  = new Vimeo.Player(iframe);
                        var owl     = jQuery('.owl-carousel');
                        owl.on('changed.owl.carousel', function(event){
                            if (event.item.count == (event.item.index -1)) {
                                player.play();
                            } else {
                                player.pause();
                            }
                        });
                    </script>
                    
                    ";
        } else {
            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
            return  '<div id="player"></div>
                    <script>
                        // 2. This code loads the IFrame Player API code asynchronously.
                        var tag = document.createElement(\'script\');

                        tag.src = "https://www.youtube.com/iframe_api";
                        var firstScriptTag = document.getElementsByTagName(\'script\')[0];
                        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                        // 3. This function creates an <iframe> (and YouTube player)
                        //    after the API code downloads.
                        var player;
                        function onYouTubeIframeAPIReady() {
                            player = new YT.Player("player", {
                                videoId: "'.$match[1].'",
                                events: {
                                    "onReady": onPlayerReady,
                                    "onStateChange": onPlayerStateChange
                                }
                            });
                        }

                        function onPlayerReady(event) {
                            event.target.playVideo();
                        }

                        function onPlayerStateChange(event) {
                            if (event.data == YT.PlayerState.ENDED) {
                                player.playVideo();
                            }
                        }
                    </script>';
        }
    }
    
}
