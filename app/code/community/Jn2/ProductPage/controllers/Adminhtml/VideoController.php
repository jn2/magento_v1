<?php

class Jn2_ProductPage_Adminhtml_VideoController extends Mage_Adminhtml_Controller_Action
{
    
    public function indexAction()
    {
        $helper     = Mage::helper('productpage');
    
        if(!$helper->attributeVideoExists()) {
            $this->__createAttribute();
        }
        $this->_redirectReferer();
    }
    
    protected function _isAllowed()
    {
        return Mage::getSingleton( 'admin/session' )->isAllowed( 'jn2_video' );
    }
    
    protected function __createAttribute() {
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttribute('catalog_product', 'jn2_product_video', array(
            'type'                          => 'text',
            'backend'                       => '',
            'frontend'                      => '',
            'label'                         => 'Vídeo (youtube, vimeo)',
            'group'                         => 'General',
            'input'                         => 'text',
            'class'                         => '',
            'source'                        => '',
            'global'                        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible'                       => true,
            'required'                      => false,
            'user_defined'                  => true,
            'default'                       => '',
            'searchable'                    => false,
            'filterable'                    => false,
            'comparable'                    => false,
            'visible_on_front'              => true,
            'used_in_product_listing'       => false,
            'unique'                        => false,
            'apply_to'                      => array('simple', 'configurable', 'virtual', 'downloadable'),
            'is_configurable'               => false,
            'is_used_for_promo_rules'       => false,
        ));
    }
}
