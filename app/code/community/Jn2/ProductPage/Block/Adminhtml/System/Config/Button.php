<?php

class Jn2_ProductPage_Block_Adminhtml_System_Config_Button extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement( $element );
    
        $url        = Mage::helper("adminhtml")->getUrl("jn2_video/adminhtml_video");
        $html       = $this ->getLayout()->createBlock( 'adminhtml/widget_button' )
                            ->setType( 'button' )
                            ->setClass( 'scalable' )
                            ->setOnClick( "setLocation('{$url}')" );
        
        $helper     = Mage::helper('productpage');
            
        if(!$helper->attributeVideoExists()) {
            $label = $this->__('Criar atributo de vídeo');
        } else {
            $label = $this->__('Atributo criado');
            $html->setDisabled('disabled');
        }
        
        return $html->setLabel($label)->toHtml();;
    }
}