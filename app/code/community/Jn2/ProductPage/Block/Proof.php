<?php

class Jn2_ProductPage_Block_Proof extends Mage_Core_Block_Template {

  public function getBlockHTML($block) {

    $block_id     = Mage::getStoreConfig("product_page/blocks/{$block}");

    if ($block_id) {
      $static_block = $this->getLayout()->createBlock('cms/block')->setBlockId($block_id);
      return $static_block->toHtml();
    } else {
      return '';
    }

  }
}
