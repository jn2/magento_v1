<?php

class Jn2_ProductPage_Block_ProductInfoBar extends Mage_Catalog_Block_Product_Price {

  public function __construct() {
    $this->setData('active', Mage::getStoreConfig('product_page/general/active_product_info_bar'));
  }

}
