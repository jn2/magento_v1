<?php

/**
* Layout block footer
*
* @author Leandro Marques
*/
class Jn2_Footer_Block_Layout extends Mage_Core_Block_Template {


    public function __construct() {
        if (Mage::getStoreConfig('custom_footer/template/active')) {
            $theme = Mage::getStoreConfig('custom_footer/template/theme');
            $this->setTemplate("jn2/footer/wireframes/{$theme}.phtml");
        } else {
            return '';
        }
    }

    public function _prepareLayout() {
        $parent = parent::_prepareLayout();

        $theme = Mage::getStoreConfig('custom_footer/template/theme');
        $this->getLayout()->getUpdate()->addHandle($theme);

        return $parent;
    }

    public function getBlock( $block_pos ) {
        $block_id   = Mage::getStoreConfig("custom_footer/blocks/bloco_{$block_pos}");

        if ( is_numeric( $block_id ) && !empty( $block_id ) ) {
            $block      = $this->getLayout()->createBlock( 'cms/block' )->setBlockId( $block_id );
        } elseif( !empty( $block_id ) ) {
            $block_info =  Mage::helper( 'footer/data' )->getNoRelatedBlocks( $block_id );
            $block      = $this->getLayout()->createBlock( $block_info['type'] )->setBlockId( $block_id );

            if( !empty( $block_info['template'] ) ) {
                $block = $block->setTemplate( $block_info['template'] );
            }
        }

        return (isset($block)) ? $block->toHtml() : "";
    }

    public function getLogoSrc()
    {
        if (empty($this->_data['logo_src'])) {
            $this->_data['logo_src'] = Mage::getStoreConfig('design/header/logo_src');
        }
        if (Mage::getStoreConfig('custom_footer/blocks/logo')) {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'logo_footer'.DS.Mage::getStoreConfig('custom_footer/blocks/logo');
        } else {
            return $this->getSkinUrl($this->_data['logo_src']);
        }
    }

    public function getLogoSrcSmall()
    {
        if (empty($this->_data['logo_src_small'])) {
            $this->_data['logo_src_small'] = Mage::getStoreConfig('design/header/logo_src_small');
        }

        if (Mage::getStoreConfig('custom_footer/blocks/logo')) {
            return $this->getMediaUrl($this->_data['logo_src_small']);
        } else {
            return $this->getSkinUrl($this->_data['logo_src_small']);
        }
    }

    public function getLogoAlt()
    {
        if (empty($this->_data['logo_alt'])) {
            $this->_data['logo_alt'] = Mage::getStoreConfig('design/header/logo_alt');
        }
        return $this->_data['logo_alt'];
    }

    protected function _afterToHtml($html)
    {
      // Remove comments
      $html = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $html);
      // Remove space after colons
      $html = str_replace(': ', ':', $html);
      // Remove whitespace
      $html = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), ' ', $html);
      return trim($html);
     }

}
