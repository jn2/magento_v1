<?php

/**
* Layout block footer
*
* @author Leandro Marques
*/
class Jn2_Footer_Block_CheckoutFooter extends Jn2_Footer_Block_Layout {

	public function __construct() {
       
        $this->setTemplate("jn2/page/html/footer-checkout.phtml");

    }

    public function getBlock( $block_pos ) {
        $block_id   = Mage::getStoreConfig("custom_footer/blocks_footer_simple/bloco_{$block_pos}");

        if ( is_numeric( $block_id ) && !empty( $block_id ) ) {
            $block      = $this->getLayout()->createBlock( 'cms/block' )->setBlockId( $block_id );
        } elseif( !empty( $block_id ) ) {
            $block_info =  Mage::helper( 'footer/data' )->getNoRelatedBlocks( $block_id );
            $block      = $this->getLayout()->createBlock( $block_info['type'] )->setBlockId( $block_id );

            if( !empty( $block_info['template'] ) ) {
                $block = $block->setTemplate( $block_info['template'] );
            }
        }

        return (isset($block)) ? $block->toHtml() : "";
    }

}