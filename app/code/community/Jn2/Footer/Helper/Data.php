<?php
/**
* Footer Helper
*
* @author Leandro Marques
*/

class Jn2_Footer_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function getThemeCss() {
		$theme = Mage::getStoreConfig('custom_footer/template/theme');
		return "jn2/footer/wireframes/{$theme}.css";
	}

	public function getNoRelatedBlocks($key) {
			$a = array(
							'facebook' =>  array(
											'type' => 'facebook/page',
											'template' => 'jn2/facebook/page.phtml'
              ),
              'newsletter' => array(
                'type' => 'newsletter/subscribe',
                'template' => 'jn2/footer/subscribe.phtml'
              ),
              'logo' => array(
                'type' => 'jn2_footer/logo',
                'template' => 'jn2/footer/logo.phtml'
              ),
              'desc-store' => array(
                'type' => 'jn2_footer/descricao',
                'template' => 'jn2/footer/desc-store.phtml'
              )
			);

			return $a[$key];
	}


}
