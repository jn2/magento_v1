<?php
class Jn2_Footer_Model_System_Config_Source_Block
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    $block_collection = Mage::getResourceModel('cms/block_collection')
    ->load()
    ->toOptionArray();

    $outros[] = array('value' => 'facebook', 'label' => 'Facebook box');
    $outros[] = array('value' => 'newsletter', 'label' => 'Newsletter');
    $outros[] = array('value' => 'logo', 'label' => 'Logotipo');
    $outros[] = array('value' => 'desc-store', 'label' => 'Descrição Loja');
    
    $options = array(
      array('label' => 'Blocos', 'value' => $block_collection),
      array('label' => 'Outros', 'value' => $outros)
    );
    array_unshift($options, array('value' => '', 'label' => ''));
    return $options;
  }

}
