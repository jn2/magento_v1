<?php
class Jn2_Home_Model_Observer
{

  /**
  * Options getter
  *
  * @return array
  */
  public function loadThemeAssets($params)
  {
    $theme    = Mage::getStoreConfig('custom_home/template/home_theme');
    $request  = Mage::app()->getRequest();
    $layout   = $params['layout'];
    $layout->getUpdate()->addHandle($theme);
    $layout->getUpdate()->addHandle($theme.'_'.$request->getRouteName().'_'.$request->getControllerName().'_'.$request->getActionName());
  }

}
