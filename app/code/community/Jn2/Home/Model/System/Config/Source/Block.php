<?php
class Jn2_Home_Model_System_Config_Source_Block
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    $block_collection = Mage::getResourceModel('cms/block_collection')
    ->load()
    ->toOptionArray();

    $banner_collection = Mage::getResourceModel('ibanners/group_collection');
    $banner_options = array();

    foreach($banner_collection as $group) {
      $banner_options[] = array('value' => $group->getCode(), 'label' => $group->getTitle());
    }

    $options = array(
      array('label' => 'Blocos',
      'value' => $block_collection),
      array('label' => 'Banners',
      'value' => $banner_options)
    );
    array_unshift($options, array('value' => '', 'label' => ''));
    return $options;
  }

}
