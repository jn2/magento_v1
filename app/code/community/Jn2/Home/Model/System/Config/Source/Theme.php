<?php
class Jn2_Home_Model_System_Config_Source_Theme
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    return array(
        array('label' => 'Tema 1', 'value' => 'theme1', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w1.jpg')),
        array('label' => 'Tema 2', 'value' => 'theme2', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w2.jpg')),
        array('label' => 'Tema 3', 'value' => 'theme3', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w3.jpg')),
        array('label' => 'Tema 4', 'value' => 'theme4', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w4.jpg')),
        array('label' => 'Tema 5', 'value' => 'theme5', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w5.jpg')),
        array('label' => 'Tema 6', 'value' => 'theme6', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w6.jpg')),
        array('label' => 'Tema 7', 'value' => 'theme7', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w7.jpg')),
        array('label' => 'Tema 8', 'value' => 'theme8', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w8.jpg')),
        array('label' => 'Tema 9', 'value' => 'theme9', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w9.jpg')),
        array('label' => 'Tema 10', 'value' => 'theme10', 'title' => Mage::getDesign()->getSkinUrl('jn2/home/wireframes/w8.jpg'))
        
    );
  }

}
