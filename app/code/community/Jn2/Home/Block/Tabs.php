<?php

class Jn2_Home_Block_Tabs extends Mage_Core_Block_Template
{
  public function renderView() {
    // Lugar para criar variáveis ou funções ou tratar os dados.

    //este exemplo retorno a quantidade de itens, pode ser chamado no phtml atraves do $this->getCount();
    $this->setData('count', count($this->getTitles()));

    return parent::renderView();
  }

  // EXEMPLO DE FUNÇÃO QUE PODE SER CRIADA
  // public function getBatata()
  // {
  //   return 'batata';
  // }
}