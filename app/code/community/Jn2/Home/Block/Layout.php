<?php

/**
 * Description of Price
 *
 * @author reinaldo
 */
class Jn2_Home_Block_Layout extends Mage_Core_Block_Template {

    //put your code here
    public function __construct() {
      if (Mage::getStoreConfig('custom_home/template/active')) {
        $theme = Mage::getStoreConfig('custom_home/template/home_theme');
        //$this->setData('fluid', Mage::getStoreConfig('custom_theme/general/fluid')); //remover
        $this->setTemplate("jn2/home/wireframes/{$theme}.phtml");
      } else {
        return '';
      }
    }

    public function _prepareLayout() {
      $parent = parent::_prepareLayout();

      $theme = Mage::getStoreConfig('custom_home/template/home_theme');
      $this->getLayout()->getUpdate()->addHandle($theme);

      return $parent;
    }

    public function getBlock($block_pos) {
      $block_id   = Mage::getStoreConfig("custom_home/blocks/bloco_{$block_pos}");

      if (is_numeric($block_id) && !empty($block_id)) {
        $block      = $this->getLayout()->createBlock('cms/block')->setBlockId($block_id);
      } else {
        if (empty($block_id)) {
            return '';
        } else {
          $block      = $this->getLayout()->createBlock('ibanners/view')->setGroupCode($block_id);
        }
      }

      return $block->toHtml();
    }

    public function getProductsList($prod_pos) {
      $category_id = Mage::getStoreConfig("custom_home/blocks/products_{$prod_pos}");
      if (!empty($category_id)) {
        $category = new Mage_Catalog_Model_Category();
        $category->load($category_id);
        $collection = $category->getProductCollection();
        $collection->addAttributeToSelect('*');
        $qty = Mage::getStoreConfig("custom_home/blocks/qty_product_{$prod_pos}");
        $collection->getSelect()->limit($qty);
        return $collection;
      }
      return '';
    }

    public function getProductTabListHtml(array $positions) {
      $titles = [];
      $tabs = [];
      foreach ($positions as $position) {
        $category_id    = Mage::getStoreConfig("custom_home/blocks/products_{$position}");
        $category = Mage::getModel('catalog/category')->load($category_id);

        if ($category->getId()) {
          $titles[] = $category->getName();
          $ids[] = $category->getId();
          $tabs[] = $this->getProductListHtml($position);
        }
      }

      $layout = Mage::getSingleton('core/layout');
      $block = $layout->createBlock('jn2_home/tabs');
      $block->setData('titles', $titles);
      $block->setData('ids', $ids);
      $block->setData('tabs', $tabs);
      $block->setTemplate('jn2/home/tabs.phtml');

      return $block->toHtml();
    }

    public function getProductListHtml($prod_pos) {
      $theme          = Mage::getStoreConfig('custom_home/template/home_theme');
      $theme_list     = Mage::getStoreConfig('custom_home/template/list_theme');
      $category_id    = Mage::getStoreConfig("custom_home/blocks/products_{$prod_pos}");
      $qty            = Mage::getStoreConfig("custom_home/blocks/qty_product_{$prod_pos}");
      $column         = Mage::getStoreConfig("custom_home/blocks/column_product_{$prod_pos}");
      $show_category  = Mage::getStoreConfig("custom_home/blocks/products_show_category_{$prod_pos}");
      $is_carousel    = Mage::getStoreConfig("custom_home/blocks/carrousel_product_{$prod_pos}");

      if (!empty($category_id)) {
        $layout = Mage::getSingleton('core/layout');
        $toolbar_block_name = $theme.'_home_theme_'.$category_id;
        $toolbar = $layout->createBlock('catalog/product_list_toolbar',$toolbar_block_name);
        $block = $layout->createBlock('catalog/product_list')
                        ->append($toolbar)
                        ->setToolbarBlockName($toolbar_block_name);
                        $toolbar->setData('_current_limit', $qty);

        $block->setCategoryId($category_id);
        $block->setData('column', 12/$column);
        $block->setData('category', Mage::getModel('catalog/category')->load($category_id));
        $block->setData('show_category', $show_category);
        $block->setData('is_carousel', $is_carousel);
        $block->setData('position', $prod_pos);
        if (empty($theme_list) || Mage::getStoreConfig('design/package/name') == 'default') {
            $block->setTemplate('jn2/home/product-list.phtml');
        } else {
            $block->setTemplate('jn2/home/product-list-' . $theme_list . '.phtml');
        }
        $block->setColumnCount($column);

			   return $block->toHtml();
       } else {
         return '';
       }
    }

    public function getConfig($string) {
      return Mage::getStoreConfig($string);
    }
}
