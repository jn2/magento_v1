<?php
class Jn2_Contact_Model_System_Config_Source_Block
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    $block_collection = Mage::getResourceModel('cms/block_collection')
    ->load()
    ->toOptionArray();

    $first_option = array ( value => "", label => "Selecione..." );

    array_unshift($block_collection, $first_option);

    return $block_collection;
  }

}
