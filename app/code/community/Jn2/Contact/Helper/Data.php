<?php

class Jn2_Contact_Helper_Data extends Mage_Core_Helper_Abstract
{

  public function getBlock( $block_pos ) {
      $block_id   = Mage::getStoreConfig("custom_theme/contact/bloco_{$block_pos}");
      $block      = Mage::app()->getLayout()->createBlock( 'cms/block' )->setBlockId( $block_id );

      return $block->toHtml();
  }

}
