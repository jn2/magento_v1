<?php

class Jn2_Cache_Block_Adminhtml_Cache  extends Mage_Adminhtml_Block_Template {

  public function getPurgeEverythingUrl(){
      return $this->getUrl('*/cache/purgeEverything');
  }

}
