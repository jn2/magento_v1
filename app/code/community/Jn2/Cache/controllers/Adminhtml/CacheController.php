<?php

class Jn2_Cache_Adminhtml_CacheController extends Mage_Adminhtml_Controller_Action {

  public function purgeEverythingAction() {
    $cache_helper = Mage::helper('cache');

    $response = $cache_helper->purgeCache(Mage::getStoreConfig('cache/cloudflare/host'));

    $this->_redirect('*/cache/index');
  }

}
