<?php

/**
 * Trata as requisições get ajax
 * @see jn2_m_cache.js
 * 
 * @category   Jn2
 * @package    Jn2_Cache
 * @author     Reinaldo Mendes
 * 
 */
class Jn2_Cache_Model_Observer {

    protected function _isNonCachedContent($request) {
        return $request->getParam(Mage::Helper('cache/url')->getCacheBypassParamName(), false);
    }

    protected function _respondsWithNonCachedBlocks($evt) {

        $controller = $evt->getData('controller_action');
        $request = $controller->getRequest();
        if ($this->_isNonCachedContent($request)) {
            $response = $controller->getResponse();
            #$request->getParam('cache-get');
            $controller->loadLayout();
            $layout = $controller->getLayout();
            $result = array();
            foreach ($layout->getAllBlocks() as $block) {
                /* @var $block Mage_Core_Block_Abstract */
                if ($block && $block->getCacheBypass()) {
                    $html = $block->toHtml();
                    $alias = $block->getBlockAlias();
                    $alias = $alias ? $alias : $block->getNameInLayout();
                    $result[$alias] = $html;
                    $result[$block->getNameInLayout()] = $html;
                }
            }
            $keyName = $request->getParam(Mage::Helper('cache/url')->getCacheBypassParamName(), '_cache_blocks');
            $ret = array($keyName => $result);
            $ret['form_key'] = Mage::getSingleton('core/session')->getFormKey();

//            $headers = array();
//            foreach (headers_list() as $header) {
//                if (strpos(strtolower($header), 'set-cookie') !== false) {
//                    $header = explode(':', $header);
//                    $headers[] = $header;
//                }
//            }
//            $ret['_headers_'] = $headers;


            $response->setHeader('content-type', 'application/json')
                    ->setHeader('pragma', 'no-cache')
                    ->setHeader('Expires', '0')
                    ->setHeader('Cache-Control', 'private,no-cache,no-store,must-revalidate');
            $response->setBody(json_encode($ret));
        }
    }

    protected function _sanitizeHeaders($evt) {
        $controller = $evt->getData('controller_action');
        $request = $controller->getRequest();
        if (!$this->_isNonCachedContent($request)) {
            $actionName = strtolower($controller->getFullActionName());
            $config = Mage::app()->getConfig()->getNode('frontend/jn2cache/sanitize_headers/actions');
            if ($config->xpath($actionName)) {
                
                $offset = (int)(Mage::getStoreConfig('cache/cloudflare/ttl')); //10 min

                header("Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT");
                header("Cache-Control: max-age={$offset},s-max-age={$offset}");
                header_remove('Set-Cookie');
//                header_remove('Pragma');
            }
        }
    }

    public function preDispatch($evt) {
        $controller = $evt->getData('controller_action');
        $request = $controller->getRequest();
        if ($this->_isNonCachedContent($request)) {
            Mage::app()->getFrontController()->setNoRender(true); //disable render layout;        
        }
    }

    public function postDispatch($evt) {
        $this->_respondsWithNonCachedBlocks($evt);
        $this->_sanitizeHeaders($evt);
    }

    function processBlockToHtml($evt) {
        $block = $evt->getBlock();
        if ($block->getCacheBypass()) {
            $transport = $evt->getTransport();
            $content = $transport->getHtml();
//            if (strpos($content, '</') !== false || strpos($content, '/>') !== false) {//contem html
            $alias = $block->getBlockAlias();
            $alias = $alias ? $alias : $block->getNameInLayout();
            $transport->setHtml("<div class='loading-cache {$alias}' data-block-alias=\"{$alias}\" data-cache-bypass=\"1\">{$content}</div>");
//            }
        }
    }

}
