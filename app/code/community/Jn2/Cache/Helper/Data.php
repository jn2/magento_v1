<?php

class Jn2_Cache_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36';

    public function getDomainId($domain)
    {
        $email_domain = Mage::getStoreConfig('cache/cloudflare/domain');
        $key = $this->getKey();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.cloudflare.com/client/v4/zones?name={$domain}");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Auth-Email: {$email_domain}",
            "X-Auth-Key: {$key}",
            'Content-Type: application/json'
        ));
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result)->result[0]->id;
    }

    public function purgeCache()
    {
        $purged = array();
        $return = array();

        foreach (Mage::app()->getStores() as $store) {
            $domain = Mage::getStoreConfig('cache/cloudflare/host', $store);
            if (!in_array($domain, $purged)) {
                $return[] = $this->purgeCacheDomain($domain);
                $purged[] = $domain;
            }
        }

        return $return;
    }

    private function purgeCacheDomain($domain)
    {
        $email_domain = Mage::getStoreConfig('cache/cloudflare/domain');
        $key = $this->getKey();
        $identifier = $this->getDomainId($domain);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.cloudflare.com/client/v4/zones/{$identifier}/purge_cache");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Auth-Email: {$email_domain}",
            "X-Auth-Key: {$key}",
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{"purge_everything":true}');

        $result = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($result);

        if ($response->success) {
            Mage::getSingleton('core/session')->addSuccess('Cache de ' . $domain . ' liberado junto ao CloudFlare');
        } else {
            Mage::getSingleton('core/session')->addError('Falha ao limpar cache de ' . $domain . ' junto ao CloudFlare. Por favor verifique suas configurações');
        }
        foreach ($response->messages as $message) {
            Mage::getSingleton('core/session')->addNotice($message);
        }

        return json_decode($result);
    }

    public function getKey()
    {
        $key = Mage::getStoreConfig('cache/cloudflare/key');
        if (!empty($key)) {
            return $key;
        } else {
            $cloudflare_key_filename = Mage::getBaseDir('base') . '/cloudflare.txt';
            if (file_exists($cloudflare_key_filename)) {
                $handle = fopen($cloudflare_key_filename, "r"); // r = read mode, w = write mode, a = append mode
                $contents = fread($handle, filesize($cloudflare_key_filename));
                fclose($handle);
            }
            return trim($contents);
        }
    }

}
