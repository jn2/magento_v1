<?php

/**
 * @package Jn2_Cache
 *
 * @author Reinaldo Mendes
 * 
 */
class Jn2_Cache_Helper_Url extends Mage_Core_Helper_Url {

    public function getCurrentUrl() {
        $currentUrl = parent::getCurrentUrl();
        return preg_replace("@{$this->getCacheBypassParamName()}=[^#&]+&?@",'',$currentUrl);
    }
    
    public function getCacheBypassParamName(){
        return 'cache-get';
    }    

}
