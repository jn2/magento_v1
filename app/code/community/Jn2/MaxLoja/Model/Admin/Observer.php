<?php

class Jn2_MaxLoja_Model_Admin_Observer {

    protected $_willProcessPlan = false;

    /**
     * Plan config was changed or saved we need to reapply configs
     * @param $evt Varien_Event_Observer
     */
    public function markForReasignPlanConfigurations($evt) {
        $this->_willProcessPlan = true;

        #only to test all plans on save
        Mage::getSingleton('maxloja/agile')->getAllPlans();
    }

    /**
     * This This method will
     * Reapply module configurations on save config
     * @param $evt Varien_Event_Observer
     */
    public function doReasignPlanConfigAfterSent() {
        if ($this->_willProcessPlan) {
            #
            # do not use singleton to get new fresh information
            #

            Mage::getSingleton('maxloja/agile')->getProcessor()->process();
        }
    }

    /**
     * This This method will
     * Reaply module configurations after admin login
     * @param $evt Varien_Event_Observer
     */
    public function afterAdminLogin() {
        Mage::getSingleton('maxloja/agile')->getProcessor()->process();
    }

    /**
     *
     * @param Varien_Event $evt
     */
    public function registerOrphanNotification($evt) {
        $layout = Mage::getSingleton('core/layout');
        $agile = Mage::getSingleton('maxloja/agile');
        $orphanModules = $agile->getOrphanModules();
        $orphanModules = join(", ", $orphanModules);
        if ($orphanModules) {
            $globalAdapter = $agile->getGlobalAdapter();

            if (method_exists($globalAdapter, 'getXmlFile')) {
                $globalXmlFile = $globalAdapter->getXmlFile();
                $globalXmlFile = str_replace(Mage::getBaseDir() . '/', '', $globalXmlFile);
            }



            $outputNotification = <<<EOD
                <div class="notification-global notification-global-critical">
                <strong class="label">
                ALERTA JN2:
                </strong>
                O(s) módulo(s) ({$orphanModules}) não foram definidos no arquivo {$globalXmlFile}

                </div>
EOD;
            $layout->getBlock('notifications')->append(
                    $layout->createBlock('core/text')->setText($outputNotification)
            );
        }
    }

    /**
     * Max products exceed
     */
    public function avoidCreateProduct() {
        $helper = Mage::helper('maxloja');
        if (!$helper->canEditProduct()) {
            $maxProducts = $helper->getMaxProducts();
            Mage::throwException(Mage::helper('core')->__('Seu plano permite no máximo '.$maxProducts.' produtos. Remova um produto para continuar gerenciando os demais.'));
        }
    }

    /*     * ***********************************************************
     * Hide Configurations and disable Acl Resources
     * ********************************************************** */

    /**
     * Check url and username == jn2 to set cookie custom_theme
     *
     * This cookie will bypass disableAclResourceOnTheFly and BlockConfigurations
     */
    public function setCookieBypassByUrl() {
        $admin = Mage::getSingleton('admin/session')->getUser();
        if ($admin && $admin->getUsername() == 'jn2' && Mage::app()->getRequest()->getParam('theme') == true) {
            Mage::getSingleton('maxloja/agile')->getHelper()->setBypass(true);
        }
    }

    /**
     *
     * @param Varien_Event $evt
     */
    public function disableAclResourceOnTheFly($evt) {
        #check cookie
        $condition = Mage::getSingleton('maxloja/agile')->getHelper()->getIsByPass();
        if (!$condition) {
            $aclsToBlock = Mage::getSingleton('maxloja/agile')
                    ->getHelper()
                    ->getMergedBlockedFeatures()
                    ->getAcl();
            foreach ($aclsToBlock as $path) {
                $resources = Mage::getSingleton('admin/config')->getAdminhtmlConfig()->getNode("acl/resources");
                /* @var $resources Varien_Simplexml_Element */
                $path = str_replace('/', '/children/', trim($path, '/'));
                $node = current($resources->xpath("{$path}"));
                $node->disabled = 1; #disable node on the fly
                #unset($node->{0}, $node->{$node->getName()}); //remove simplexml
            }
        }
    }

    /**
     *
     * @param Varien_Event_Observer $observer
     * @return \Jn2_MaxLoja_Model_Admin_Observer
     */
    public function hideConfigurationsOnTheFly(Varien_Event_Observer $observer) {
        $helper = Mage::helper('maxloja');
        $config = $observer->getConfig();

        #hide forever HARDCODED
        // $helper->hideConfig($config->getNode('sections/advanced'));


        $condition = Mage::getSingleton('maxloja/agile')->getHelper()->getIsByPass();

        if (!$condition) {
            $blockedConfigurations = Mage::getSingleton('maxloja/agile')
                    ->getHelper()
                    ->getMergedBlockedFeatures()
                    ->getConfig();


            $configs = [];
            foreach ($blockedConfigurations as $path) {
                $node = $config->getNode($path);
                if ($node) {
                    $configs[] = $node;
                }
            }


            $mergedConfigs = array_merge($configs, $helper->getExtraHomeBlockedConfigs($observer));
            foreach ($mergedConfigs as $config) {
                if ($config) {
                    $helper->hideConfig($config);
                }
            }
        }

        return $this;
    }

    /**
     * Remove unwanted category tabs from category page
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function removeUnwantedCategoryTabs(Varien_Event_Observer $observer)
    {
        $condition = Mage::getSingleton('maxloja/agile')->getHelper()->getIsByPass();

        if (!$condition) {
            $blockedTabs = Mage::getSingleton('maxloja/agile')
                ->getHelper()
                ->getMergedBlockedFeatures()
                ->getCategoryTab();

            foreach ($blockedTabs as $tab) {
                    $tabs = $observer->getEvent()->getTabs();
                    $tabs->removeTab($tab);
            }
        }

        return $this;
    }

}
