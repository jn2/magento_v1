<?php

/**
 * Rewrite magento Admin Acl 
 * 
 * 
 * 
 * @package Jn2_MaxLoja
 * @author Reinaldo Mendes
 */
class Jn2_MaxLoja_Model_Admin_Acl extends Mage_Admin_Model_Acl {

    const ACL_REWRITE_KEY = 'maxloja/admin/acl/rewrite';

    /**
     * Get plan configuration to rewrite acls
     * @return array() - array(path/to/rewrite)
     */
    protected function _getAclRewrite() {
        if (null === Mage::registry(self::ACL_REWRITE_KEY)) {
            $rewrites = Mage::getSingleton('maxloja/agile')
                    ->getHelper()
                    ->getMergedBlockedFeatures()
                    ->getAcl();
            Mage::register(self::ACL_REWRITE_KEY, $rewrites);
        }
        return Mage::registry(self::ACL_REWRITE_KEY);
    }

    /**
     * 
     * Override Acl method isAllowed to block access acording with 
     * maxloja/Agile configuration
     * 
     * @param type $role
     * @param type $resource
     * @param type $privilege
     * @return boolean
     */
    public function isAllowed($role = null, $resource = null, $privilege = null) {
        $result = parent::isAllowed($role, $resource, $privilege);
        $isBypass = Mage::getSingleton('maxloja/agile')->getHelper()->getIsByPass();
        if (!$isBypass && $result) {            
            $aclsToBlock = $this->_getAclRewrite();
            if (in_array($resource, $aclsToBlock)) {                
                return false;
            }
        }
        return $result;
    }
    
    public function __wakeup(){        
        $this->_getAclRewrite();//load acls to throw exception in case of error instead show access denied        
    }

}
