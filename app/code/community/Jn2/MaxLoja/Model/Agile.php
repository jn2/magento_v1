<?php

class Jn2_MaxLoja_Model_Agile {

    const GLOBAL_FACTORY_NODE = 'global/maxloja/global/factory';
    const PLAN_FACTORY_NODE = 'global/maxloja/plan/factory';

    /**
     *
     * @var Jn2_MaxLoja_Model_Agile_Global_Adapter_Interface
     */
    protected $_globalAdapter = null;

    /**
     *
     * @var Jn2_MaxLoja_Model_Agile_Plan_Adapter_Interface
     */
    protected $_planAdapter = null;

    /**
     * 
     * @var array | null
     */
    protected $_orphanModules = null;

    /**
     * 
     * @return Jn2_MaxLoja_Model_Agile_Global_Adapter_Interface
     * 
     */
    protected function _getGlobalAdapter() {
        if (null === $this->_globalAdapter) {
            $node = Mage::app()->getConfig()->getNode(self::GLOBAL_FACTORY_NODE);
            $factory = Mage::getModel((string) $node->class);
            $this->_globalAdapter = $factory->createAdapter();
        }
        return $this->_globalAdapter;
    }

    /**
     * 
     * @return Jn2_MaxLoja_Model_Agile_Plan_Adapter_Interface
     */
    protected function _getPlanAdapter() {
        if (null === $this->_planAdapter) {
            $node = Mage::app()->getConfig()->getNode(self::PLAN_FACTORY_NODE);
            $factory = Mage::getModel((string) $node->class);
            $this->_planAdapter = $factory->createAdapter();
        }
        return $this->_planAdapter;
    }

    /**
     * 
     * @return Jn2_MaxLoja_Model_Agile_Global_Adapter_Interface
     */
    public function getGlobalAdapter(){
        return $this->_getGlobalAdapter();
    }
    
    
    public function getBase() {
        return $this->_getGlobalAdapter()->getBase();
    }

    public function getCustom() {
        return $this->_getGlobalAdapter()->getCustom();
    }

    /*
     * call Plan adapter methods
     * 
     */

    public function getAllPlans() {
        return $this->_getPlanAdapter()->getAllPlans();
    }

    public function listPlans() {
        return $this->_getPlanAdapter()->listPlans();
    }

    public function getPlan($name, $version = null) {
        return $this->_getPlanAdapter()->getPlan($name, $version);
    }

    /**
     * Modules that was not registered on global.xml
     */
    public function getOrphanModules() {
        if (null === $this->_orphanModules) {
            $nodeModules = Mage::getConfig()->getNode('modules');
            $allModules = array_keys((array) $nodeModules->children());
            $this->_orphanModules = array_diff($allModules, $this->getBase()->getModules(), $this->getCustom()->getModules());
            foreach($this->_orphanModules as $key => $moduleName){
                $originalActive = trim((string)$nodeModules->{$moduleName}->original_active);                                
                if(in_array($originalActive,array('','0','false'))){
                    unset($this->_orphanModules[$key]);   //discard disabled on xml                 
                }
            }
        }
        return $this->_orphanModules;
    }

    /**
     * Get processor
     * @return Jn2_MaxLoja_Model_Agile_Processor;
     */
    public function getProcessor() {
        return Mage::getSingleton('maxloja/agile_processor', array('facade' => $this));
    }

    /**
     * 
     * @return Jn2_MaxLoja_Helper_Agile
     */
    public function getHelper() {
        return Mage::helper('maxloja/agile');
    }

    /*
     * Get configurations of plans and custom módules
     */

    /**
     * 
     * @return Jn2_MaxLoja_Model_Agile_Datasource
     */
    public function getDataSource() {
        return Mage::getModel('maxloja/agile_datasource', array('facade' => $this));
    }

}
