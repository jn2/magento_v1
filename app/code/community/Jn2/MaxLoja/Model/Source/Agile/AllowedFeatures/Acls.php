<?php

class Jn2_MaxLoja_Model_Source_Agile_AllowedFeatures_Acls extends Mage_Adminhtml_Model_System_Config_Source_Admin_Page {

    protected $_optionsArray = null;

    public function toOptionArray() {
        if (null === $this->_optionsArray) {
            $this->_optionsArray = array(
            );
            $agile = Mage::getSingleton('maxloja/agile');
            $resources = Mage::getSingleton('admin/config')->getAdminhtmlConfig()->getNode("acl/resources");
            /* @var $resources Varien_Simplexml_Element */


            /* @var $node Varien_Simplexml_Element */

            #throw new Exception("test");

            foreach ($agile->getCustom()->getBlockedFeatures()->getAcl() as $path) {

                $locateNodePath = str_replace('/', '/children/', trim($path, '/'));
                $node = current($resources->xpath("{$locateNodePath}"));
                
                $label = (string) $node->title;


                $this->_optionsArray[] = array(
                    'label' => $label,
                    'value' => $path
                );
            }
        }
        return $this->_optionsArray;
    }

}
