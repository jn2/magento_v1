<?php

class Jn2_MaxLoja_Model_Source_Agile_Plans extends Mage_Adminhtml_Model_System_Config_Source_Admin_Page {

    protected $_optionsArray = null;

    public function toOptionArray() {
        if (null === $this->_optionsArray) {
            $this->_optionsArray = array(
                array(
                    'label' => " - "
                )
            );
            $agile = Mage::getSingleton('maxloja/agile');
            foreach ($agile->getAllPlans() as $planWithPrefix => $plan) {
                $version = $plan->getVersion();
                $this->_optionsArray[$planWithPrefix] = array(
                    'label' => $plan->getLabel(),
                    'value' => $planWithPrefix
                );
            }
        }
        return $this->_optionsArray;
    }

}
