<?php

class Jn2_MaxLoja_Model_Source_Agile_AllowedFeatures_Configs extends Mage_Adminhtml_Model_System_Config_Source_Admin_Page {

    protected $_optionsArray = null;

    public function toOptionArray() {
        if (null === $this->_optionsArray) {
            $this->_optionsArray = array(
            );
            $agile = Mage::getSingleton('maxloja/agile');
            $config = Mage::getConfig()->loadModulesConfiguration('system.xml')
            ->applyExtends();
            #throw new Exception("test");
            
            foreach ($agile->getCustom()->getBlockedFeatures()->getConfig() as $path) {
                $label = array();
                $ary = explode('/',$path);
                $aryPath = array();
                foreach($ary as $_p){
                    $aryPath[] = $_p;
                    $node = $config->getNode(join('/',$aryPath) . '/label');
                    $label[] = (string)$node;
                    
                }
                $label = array_filter($label);
                $label = join( ' -> ', $label);               
                $this->_optionsArray[] = array(
                    'label' => $label,
                    'value' => $path
                );
            }
        }
        return $this->_optionsArray;
    }

}
