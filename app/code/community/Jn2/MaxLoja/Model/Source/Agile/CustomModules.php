<?php

class Jn2_MaxLoja_Model_Source_Agile_CustomModules extends Mage_Adminhtml_Model_System_Config_Source_Admin_Page {

    protected $_optionsArray = null;

    public function toOptionArray() {
        if (null === $this->_optionsArray) {
            $this->_optionsArray = array(
            );
            $agile = Mage::getSingleton('maxloja/agile');
            $nodeModules = Mage::getConfig()->getNode('modules');
            foreach ($agile->getCustom()->getModules() as $module) {
                $originalActive = (string) $nodeModules->{$module}->original_active === '1' ? '' : " ( D )" ;
                $this->_optionsArray[] = array(
                    'label' => "{$module} {$originalActive}" ,
                    'value' => $module
                );
            }
        }

        return $this->_optionsArray;
    }

}
