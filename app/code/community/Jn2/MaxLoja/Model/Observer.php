<?php

class Jn2_MaxLoja_Model_Observer {

    public function blockStore() {
        $helper = Mage::helper('maxloja');
        $active_store = Mage::getStoreConfig('max_loja/config/active_store');
        $url = Mage::getUrl('maxloja', array('_secure' => false));
        $current_url = Mage::helper('core/url')->getCurrentUrl();
        if (!$helper->adminLogged() && !$active_store && strpos($current_url, $url) === false) {
            Mage::app()->getResponse()->setRedirect($url);
        }
    }
}
