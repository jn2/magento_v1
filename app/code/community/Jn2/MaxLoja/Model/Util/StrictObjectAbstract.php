<?php

/**
 * This class will validate attributes on construct
 * 
 * 
 * @package Jn2_MaxLoja
 * @author Reinaldo Mendes <reinaldo@jn2.com.br>
 */
class Jn2_MaxLoja_Model_Util_StrictObjectAbstract extends Varien_Object {

    /**
     * Valid but optional attributes
     * @var array
     */
    protected $_optionalAttributes = array(
    );

    /**
     * Required attributes if not present on construct will raise exception
     * @var array 
     */
    protected $_requiredAttributes = array();

    /**
     * @see Varien_Object::_construct
     */
    public function _construct() {
        $result = parent::_construct();

        $this->_initialize();

        return $result;
    }

    /**
     * Validates and call _initilize{$method}() if exists
     * @throws Exception
     */
    protected function _initialize() {
        $filter = new Zend_Filter_Word_UnderscoreToCamelCase();
        foreach ($this->_requiredAttributes as $required) {
            if (!$this->hasData($required)) {
                $selfClass = get_class($this);
                $msg = "Required '{$required}' not found on {$selfClass}";
                throw new Exception($msg);
            }
        }
        //valids are required + valids
        $allValids = array_merge((array) $this->_optionalAttributes, (array) $this->_requiredAttributes);
        foreach ($this->getData() as $key => $value) {
            if (!in_array($key, $allValids)) {
                $selfClass = get_class($this);
                $msg = sprintf("Inválid attribute '{$key}' only '%s' are valid on '%s'", join(', ', $allValids), $selfClass);
                throw new Exception($msg);
            }
            /**
             * if has method _initialize{Something}
             * we will call it
             */
            $ucKey = ucfirst($filter->filter($key));
            $method = "_initialize{$ucKey}";
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        
        //validate after initialize
        foreach ($this->getData() as $key => $value) {
            $ucKey = ucfirst($filter->filter($key));
            $method = "_validate{$ucKey}";
            if (method_exists($this, $method)) {
                $this->{$method}();
            }
            
        }
    }

}
