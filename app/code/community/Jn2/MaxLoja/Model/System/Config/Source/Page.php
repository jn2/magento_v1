<?php
class Jn2_MaxLoja_Model_System_Config_Source_Page
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    $page_collection = Mage::getResourceModel('cms/page_collection')
    ->load()
    ->toOptionArray();

    return $page_collection;
  }

}
