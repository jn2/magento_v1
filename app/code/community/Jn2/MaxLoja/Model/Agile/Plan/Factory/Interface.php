<?php

interface Jn2_MaxLoja_Model_Agile_Plan_Factory_Interface {

    /**
     * Retorna o adaptador que irá ler as informações dos planos
     * Caso seja implementação webservice aqui é um bom lugar para obter informações de login ou
     * algum outro parametro que seja necessário para configurar o Objeto que irá ler.
     * 
     *
     */
    public function createAdapter();
}
