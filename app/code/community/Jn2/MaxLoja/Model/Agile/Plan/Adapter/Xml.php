<?php

class Jn2_MaxLoja_Model_Agile_Plan_Adapter_Xml implements Jn2_MaxLoja_Model_Agile_Plan_Adapter_Interface {

    protected $_directory;

    /**
     * Contem os planos na seguinte disposição
     * array(
     *    PlanName => array('version' => xmlPlan)
     * )
     * @var null|array()
     */
    protected $_xmlPlans = null;

    /**
     * define o directório onde ele irá buscar os arquivos de xml dos planos
     * @param string $directory
     * @return \Jn2_MaxLoja_Model_Plan_Reader_XmlReader
     */
    public function setDirectory($directory) {
        $this->_directory = $directory;
        return $this;
    }

    /**
     *  Retorna lista de planos
     * @return array
     */
    public function getDirectory() {
        return $this->_directory;
    }

    /**
     * Realiza leitura e merge dos módulos em ordem
     * 
     * @return Mage_Core_Model_Config
     */
    protected function _getXmlPlans() {
        if (null === $this->_xmlPlans) {
            $this->_xmlPlans = array();

            $directory = $this->getDirectory();
            $xmlFiles = glob("$directory/*.xml");
            foreach ($xmlFiles as $file) {
                $xmlModel = Mage::getModel('core/config_base');
                /* @var $xmlModel Mage_Core_Model_Config_Base */
                if (!$xmlModel->loadFile($file)) {
                    throw new Exception("erro no xml {$file}");
                }
                foreach ($xmlModel->getXpath('/*/*') as $plan) {
                    /* @var $plan Mage_Core_Model_Config_ElementExtend */
                    if (!isset($this->_xmlPlans[$plan->getName()])) {
                        $this->_xmlPlans[$plan->getName()] = array();
                    }
                    $version = (string) $plan->version;
                    $version = trim($version) ? trim($version) : '0.0.0';
                    $this->_xmlPlans[$plan->getName()][$version] = $plan;
                }
            }
            //sort plans by version
            foreach ($this->_xmlPlans as $key => $planDefinition) {
                ksort($planDefinition);
                $this->_xmlPlans[$key] = $planDefinition;
            }
        }
        return $this->_xmlPlans;
    }

    /**
     *  Retrieve all plans with versions
     * @return array()
     */
    public function getAllPlans() {
        $result = array();
        $sep = self::PLAN_VERSION_SEPARATOR;
        foreach ($this->_getXmlPlans() as $plansWithVersion) {
            foreach ($plansWithVersion as $plan) {
                $version = (string) $plan->version;
                $result[$plan->getName() . $sep . $version] = $this->getPlan($plan->getName(), $version);
            }
        }
        return $result;
    }

    /**
     * Lê todos os xmls e retorna todos os planos
     * 
     * @todo Implementar lista de planos
     */
    public function listPlans() {
        return array_keys($this->_getXmlPlans());
    }

    /**
     * Lê configuração de um plano
     * 
     * @param string $name
     * @param string|null $version -  if null informed last version is returned
     * @return Jn2_MaxLoja_Model_Definition|null
     */
    public function getPlan($name, $version = null) {
        $plans = $this->_getXmlPlans();
        $result = $plans[$name];
        if (null === $version) {
            $result = end($result);
        } else {
            $result = $result[$version];
        }
        if ($result) {
            $resultArray = $result->asArray();
            $element = current($result->xpath('definition'));
            $definitionArray = Mage::helper('maxloja/xml')->toDefinitionArray($element);
            if ($definitionArray['blocked_features']) {
                throw new Exception("plans/*.xml only accepts 'allowed_features' but 'blocked_features' found in section base");
            }
            $resultArray['definition'] = $definitionArray;
            
            $resultArray['name'] = $result->getName();
            return Mage::getModel('maxloja/agile_plan', $resultArray);
        }
        return null;
    }

}
