<?php

interface Jn2_MaxLoja_Model_Agile_Plan_Adapter_Interface {

    const PLAN_VERSION_SEPARATOR = ':';

    /**
     * Retorna todos os planos com suas versões
     */
    public function getAllPlans();

    /**
     * Lê configuração de um plano
     * 
     * @param string $name
     * @param string|null $version -  if null informed last version is returned
     * @return Jn2_MaxLoja_Model_PLan
     */
    public function getPlan($name, $version = null);

    /**
     * @return array()
     */
    public function listPlans();
}
