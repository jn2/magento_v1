<?php

class Jn2_MaxLoja_Model_Agile_Plan_Factory implements Jn2_MaxLoja_Model_Agile_Plan_Factory_Interface {

    protected $_xmlNodeAdapter = 'global/maxloja/plan/adapter';

    /**
     * 
     * @return Jn2_MaxLoja_Model_Agile_Plan_Adapter_Interface
     * @throws Exception
     */
    public function createAdapter() {
        $node = Mage::app()->getConfig()->getNode($this->_xmlNodeAdapter);
        $xmlDirectory = Mage::getBaseDir() . DS . (string) $node->params->directory;
        if (!is_dir($xmlDirectory)) {
            throw new Exception("{$node->params->directory} Not Found");
        }
        $adapter = Mage::getModel((string) $node->class)
                ->setDirectory($xmlDirectory);


        #@todo Add Cache Decorator Observer
        # $adapter =  new CacheDecoratorAdapter($adapter);

        return $adapter;
    }

}
