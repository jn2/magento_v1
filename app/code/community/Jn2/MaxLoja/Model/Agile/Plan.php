<?php

/**
 * This class represents a Plan information as is defined in datasource - {xml}.
 * 
 * for any attribute we can add define _initialize{$method}(){}
 * ex: 
 * function _initializeName(){}
 * function _initializeDefinition(){}
 * 
 * @see Jn2_MaxLoja_Model_Util_StrictObjectAbstract::_initialize for more information
 * 
 * 
 * @package Jn2_MaxLoja
 * @author Reinaldo Mendes <reinaldo@jn2.com.br>
 */
class Jn2_MaxLoja_Model_Agile_Plan extends Jn2_MaxLoja_Model_Util_StrictObjectAbstract {

    /**
     * Required attributes
     * @see Jn2_MaxLoja_Model_Util_StrictObjectAbstract::$_requiredAttributes
     * @var array()
     */
    protected $_requiredAttributes = array(
        'name',
        'version',
        'label',
        'active',
        'definition'
    );

    /**
     * @see Jn2_MaxLoja_Model_Util_StrictObjectAbstract::$_optionalAttributes
     * @var array 
     */
    protected $_optionalAttributes = array(
//        'version',
//        'label',
//        'active',
//        'definition',
    );

    /**
     * Transform array in definition object
     * this was called on construct of object
     * @see Jn2_MaxLoja_Model_Util_StrictObjectAbstract::_initialize
     */
    public function _initializeDefinition(array $definition) {
        $this->setDefinition(Mage::getModel('maxloja/agile_definition', $definition));
    }

}
