<?php

/**
 * This class will get database custom configuration like extra módules
 * custom qty produts etc...
 * 
 *  This definition can be a base, custom or plan definition:
 * ex:
 * 


 * 
 * @package Jn2_MaxLoja
 * @author Reinaldo Mendes <reinaldo@jn2.com.br>
 */
class Jn2_MaxLoja_Model_Agile_Datasource {

    const CONFIG_AGILE_PLAN_PATH = 'max_loja/plans/current_plan';
    const CONFIG_AGILE_CUSTOM_MODULES_PATH = 'max_loja/plans/extra_modules';
    const CONFIG_AGILE_ALLOWED_FEATURES_CONFIG_PATH = 'max_loja/plans/allowed_feature_configs';
    const CONFIG_AGILE_ALLOWED_FEATURES_ACLS_PATH = 'max_loja/plans/allowed_feature_acls';
    const CONFIG_AGILE_CUSTOM_MAX_PRODUCTS_PATH = 'max_loja/plans/max_products';

    protected $_definition;

    /**
     *
     * @var Jn2_MaxLoja_Model_Agile
     */
    protected $_facade;

    public function __construct(array $options) {

        if (!isset($options['facade']) || !$options['facade'] instanceOf Jn2_MaxLoja_Model_Agile) {
            throw new Exception('Facade instance for Processor must be Jn2_MaxLoja_Model_Agile');
        }
        $this->_facade = $options['facade'];
    }

    /**
     * Returns definition of Modules, resources & configurations
     * 
     * @return Jn2_MaxLoja_Model_Agile_Definition
     * 
     */
    public function getDefinition() {
        $definition = array();
        if (null === $this->_definition) {
            $definition = $this->_getDefinitionOptions();
            $this->_definition = Mage::getModel('maxloja/agile_definition', $definition);
        }
        return $this->_definition;
    }

    /**
     * Mount array of definitions from database
     * @return array()
     */
    protected function _getDefinitionOptions() {
        $options = array();
        $extraModules = $this->_getExtraModules();
        if ($extraModules) {
            $options['modules'] = $extraModules;
        }
        $resources = $this->_getResources();
        if ($resources) {
            $options['resources'] = $resources;
        }
        $allowedConfigurations = $this->_getAllowedFeaturesConfigurations();
        $options['allowed_features'] = array();
        if ($allowedConfigurations) {
            $options['allowed_features']['config'] = $allowedConfigurations;
        }
        $allowedAcls = $this->_getAllowedFeaturesAcls();        
        if ($allowedAcls) {
            $options['allowed_features']['acl'] = $allowedAcls;
        }
        return $options;
    }

    /**
     * get Database Configuration of Other active modules
     * @return array()
     */
    protected function _getExtraModules() {
        $customSavedModules = Mage::getStoreConfig(self::CONFIG_AGILE_CUSTOM_MODULES_PATH, 0);
        $backend = Mage::getModel('maxloja/backend_agile_extraModules');
        $backend->setValue($customSavedModules)
                ->afterLoad();

        $customSavedModules = $backend->getValue();
        return array_intersect($this->_facade->getCustom()->getModules(), $customSavedModules);
    }

    /**
     * 
     * Get database configuration of resources
     * 
     * @return array - array('max_products' => '...', 'other_resource' => '...')
     */
    protected function _getResources() {
        $dataBaseResources = array();
        $maxProducts = intval(Mage::getStoreConfig(self::CONFIG_AGILE_CUSTOM_MAX_PRODUCTS_PATH, 0));
        if ($maxProducts) {
            $dataBaseResources['max_products'] = $maxProducts;
        }
        return $dataBaseResources;
    }

    /**
     * Returns database configurations to allow display some blocked
     * by custom rule configuration on global.xml
     * 
     * @return array()
     */
    protected function _getAllowedFeaturesConfigurations() {
        $allowedSavedFeaturesValue = Mage::getStoreConfig(self::CONFIG_AGILE_ALLOWED_FEATURES_CONFIG_PATH, 0);
        $backend = Mage::getModel('maxloja/backend_agile_allowedFeatures');
        $backend->setValue($allowedSavedFeaturesValue)
                ->afterLoad();
        $allowedSavedFeatures = $backend->getValue();
        //somente o que estiver bloqueado em custom será retornado como allowed configurations
        return array_intersect($this->_facade->getCustom()->getBlockedFeatures()->getConfig(), (array)$allowedSavedFeatures);
    }

    protected function _getAllowedFeaturesAcls() {
        $allowedSavedFeaturesValue = Mage::getStoreConfig(self::CONFIG_AGILE_ALLOWED_FEATURES_ACLS_PATH, 0);
        $backend = Mage::getModel('maxloja/backend_agile_allowedFeatures');
        $backend->setValue($allowedSavedFeaturesValue)
                ->afterLoad();
        $allowedSavedFeatures = $backend->getValue();        
        //somente o que estiver bloqueado em custom será retornado como allowed configurations
        return array_intersect((array)$this->_facade->getCustom()->getBlockedFeatures()->getAcl(), (array)$allowedSavedFeatures);
    }

}
