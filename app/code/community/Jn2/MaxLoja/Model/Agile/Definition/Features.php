<?php

/**
 * 
 * @package Jn2_MaxLoja
 * @author Reinaldo Mendes <reinaldo@jn2.com.br>
 */
class Jn2_MaxLoja_Model_Agile_Definition_Features extends Jn2_MaxLoja_Model_Util_StrictObjectAbstract {

    protected $_optionalAttributes = array(
        'config',
        'acl',
        'product_tab',
        'customer_tab',
        'category_tab'
    );    
   
}
