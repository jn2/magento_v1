<?php

class Jn2_MaxLoja_Model_Agile_Definition_Resources extends Jn2_MaxLoja_Model_Util_StrictObjectAbstract implements IteratorAggregate {

    protected $_optionalAttributes = array(
        'max_products',
    );

    public function getIterator() {        
        return new ArrayIterator($this->getData());
    }
    

}
