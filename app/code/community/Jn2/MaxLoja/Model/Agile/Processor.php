<?php

class Jn2_MaxLoja_Model_Agile_Processor {

    const BASE_MODULE_DISABLED_PATH = 'advanced/modules_disable_output/';
    const MODULE_DISABLED_TRUE = 1;
    const MODULE_DISABLED_FALSE = 0;

    /**
     *
     * @var Jn2_MaxLoja_Model_Agile
     */
    protected $_facade;

    public function __construct(array $options) {

        if (!isset($options['facade']) || !$options['facade'] instanceOf Jn2_MaxLoja_Model_Agile) {
            throw new Exception('Facade instance for Processor must be Jn2_MaxLoja_Model_Agile');
        }
        $this->_facade = $options['facade'];
    }

    protected function _processPreconditions() {
        //orphanModules need to be disabled
        foreach ($this->_facade->getOrphanModules() as $module) {
            $path = self::BASE_MODULE_DISABLED_PATH . $module;
            $config = Mage::getConfig();
            $config->saveConfig($path, self::MODULE_DISABLED_TRUE);
        }

        //base modules was always enabled
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $qry = "select path from core_config_data where path like 'advanced/modules_disable_output/%'";
        $modules = $readConnection->fetchCol($qry);

        foreach ($modules as $module) {
            $module = str_replace('advanced/modules_disable_output/','',$module);
            $path = self::BASE_MODULE_DISABLED_PATH . $module;
            $config = Mage::getConfig();
            $config->saveConfig($path, self::MODULE_DISABLED_FALSE);
        }

        //custom disabled by default
        foreach ($this->_facade->getCustom()->getModules() as $module) {
            $path = self::BASE_MODULE_DISABLED_PATH . $module;
            $config = Mage::getConfig();
            $config->saveConfig($path, self::MODULE_DISABLED_TRUE);
        }
    }

    protected function _processPlanModules() {

        $planDefinition = $this->_facade->getHelper()->getCurrentPlan()->getDefinition();

        #enable all plan modules
        foreach ($planDefinition->getModules() as $module) {
            $path = self::BASE_MODULE_DISABLED_PATH . $module;
            $config = Mage::getConfig();
            $config->saveConfig($path, self::MODULE_DISABLED_FALSE);
        }
    }

    protected function _doProcessExtraModules() {
        foreach ($this->_facade->getDataSource()->getDefinition()->getModules() as $module) {
            $path = self::BASE_MODULE_DISABLED_PATH . $module;
            $config = Mage::getConfig();
            $config->saveConfig($path, self::MODULE_DISABLED_FALSE);
        }
    }

    protected function _doProcess() {
        $this->_processPreconditions();

        $this->_processPlanModules();
        $this->_doProcessExtraModules();
        $this->_processPosConditions();
        //base enabled allways and ignore others
    }

    protected function _processPosConditions() {
        //custom disabled by default
/*        foreach ($this->_facade->getCustom()->getModules() as $module) {
            $path = self::BASE_MODULE_DISABLED_PATH . $module;
            $config = Mage::getConfig();
            $config->saveConfig($path, self::MODULE_DISABLED_FALSE);
        }*/
    }

    public function process() {
        $conn = Mage::getModel('core/resource')->getConnection('');
        /* @var $conn Magento_Db_Adapter_Pdo_Mysql */
        $conn->beginTransaction();
        try {
            $this->_doProcess();
            $conn->commit(); #apply all changes in database
        } catch (Exception $e) {
            $conn->rollback();
        }
    }

}
