<?php

interface Jn2_MaxLoja_Model_Agile_Global_Adapter_Interface {

    /**
     * Lê configuração de um plano
     * 
     * @param string $name
     * @param string|null $version -  if null informed last version is returned
     * @return Jn2_MaxLoja_Model_PLan
     */
    public function getCustom();

    /**
     * @return array()
     */
    public function getBase();

    
}
