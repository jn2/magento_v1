<?php

class Jn2_MaxLoja_Model_Agile_Global_Factory implements Jn2_MaxLoja_Model_Agile_Plan_Factory_Interface {

    protected $_xmlNodeAdapter = 'global/maxloja/global/adapter';

    /**
     * 
     * @return Jn2_MaxLoja_Model_Agile_Global_Adapter_Interface
     * @throws Exception
     */
    public function createAdapter() {
        $node = Mage::app()->getConfig()->getNode($this->_xmlNodeAdapter);        
        $xmlFile = Mage::getBaseDir() . DS . (string)$node->params->file;
        if(!is_file($xmlFile)){
            throw new Exception("{$node->params->file} Not Found");
        }
        $adapter = Mage::getModel((string) $node->class)
                ->setXmlFile($xmlFile);
        
        #@todo Add Cache Decorator Observer
        # $adapter =  new CacheDecoratorAdapter($adapter);

        return $adapter;
    }

}
