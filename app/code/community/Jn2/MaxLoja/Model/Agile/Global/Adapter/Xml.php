<?php

class Jn2_MaxLoja_Model_Agile_Global_Adapter_Xml implements Jn2_MaxLoja_Model_Agile_Global_Adapter_Interface {

    /**
     *
     * @var string
     */
    protected $_xmlFile;

    /**
     *
     * @var Mage_Core_Model_Config_Base
     */
    protected $_xml = null;

    /**
     * define o directório onde ele irá buscar os arquivos de xml dos planos
     * @param string $directory
     * @return \Jn2_MaxLoja_Model_Plan_Reader_XmlReader
     */
    public function setXmlFile($file) {
        $this->_xmlFile = $file;
        return $this;
    }

    /**
     *  Retorna lista de planos
     * @return array
     */
    public function getXmlFile() {
        return $this->_xmlFile;
    }

    /**
     * 
     * @return Mage_Core_Model_Config_Base
     */
    protected function _loadXml() {
        if (null === $this->_xml) {
            $this->_xml = Mage::getModel('core/config_base');
            $this->_xml->loadFile($this->getXmlFile());
            if (!$this->_xml) {
                throw new Exception("Não foi possível carregar o xml {$this->getXmlFile()}");
            }
        }
        return $this->_xml;
    }

    public function getBase() {
        $xml = $this->_loadXml();
        $definition = $xml->getNode('global/base/definition');
        $definitionArray = Mage::helper('maxloja/xml')->toDefinitionArray($definition);
        if ($definitionArray['allowed_features']) {
            throw new Exception("Global.xml only accepts 'blocked_features' but 'allowed_features' found in section base");
        }

        return Mage::getModel('maxloja/agile_definition', $definitionArray);
    }

    public function getCustom() {
        $xml = $this->_loadXml();
        $definition = $xml->getNode('global/custom/definition');
        $definitionArray = Mage::helper('maxloja/xml')->toDefinitionArray($definition);
        if ($definitionArray['allowed_features']) {
            throw new Exception("Global.xml only accepts 'blocked_features' but 'allowed_features' found in section custom");
        }
        return Mage::getModel('maxloja/agile_definition', $definitionArray);
    }

}
