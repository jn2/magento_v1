<?php

/**
 * This class represent a definition of configuration
 * 
 *  This definition can be a base, custom or plan definition:
 * ex:
 * 
 * <global>
 *   <base>
 *     <definition>..</definition> <!-- this is a definition-->
 *   </base>
 * </global>
 * 
 * 
 * for any attribute we can add define _initialize{$method}(){}
 * ex: 
 * function _initializeModules(){}
 * function _initializeResources(){}
 * 
 * @see Jn2_MaxLoja_Model_Util_StrictObjectAbstract::_initialize for more information
 * 
 * @package Jn2_MaxLoja
 * @author Reinaldo Mendes <reinaldo@jn2.com.br>
 */
class Jn2_MaxLoja_Model_Agile_Definition extends Jn2_MaxLoja_Model_Util_StrictObjectAbstract {

    protected $_optionalAttributes = array(
        'modules',
        'resources',
        #only one of blocked or allowed configurations are permited in same object
        'blocked_features',
        'allowed_features',
    );

    /**
     *
     * @var Jn2_MaxLoja_Model_Agile_Definition_Resources
     */
    protected $_resources = null;

    /**
     *
     * @var Jn2_MaxLoja_Model_Agile_Definition_Features
     */
    protected $_blockedFeatures = null;

    /**
     *
     * @var Jn2_MaxLoja_Model_Agile_Definition_Features
     */
    protected $_allowedFeatures = null;

    /**
     *
     * @var array
     */
    protected $_modules = null;

//   

    /**
     * 
     * @param array $resources
     */
    protected function _initializeResources(array $resources) {
        $this->_resources = Mage::getModel('maxloja/agile_definition_resources', $resources);
    }

    protected function _initializeBlockedFeatures(array $blockedFeatures) {
        $this->_blockedFeatures = Mage::getModel('maxloja/agile_definition_features', $blockedFeatures);
    }

    protected function _initializeAllowedFeatures(array $allowedFeatures) {
        $this->_allowedFeatures = Mage::getModel('maxloja/agile_definition_features', $allowedFeatures);
    }

    /**
     * Checks if have conflicts for allowed and blocked configurations
     * @param array $blockedConfigurations
     * @throws Exception
     */
    protected function _validateBlockedFeatures() {
        $blockedFeatures = $this->getBlockedFeatures()->getData();
        $allowedFeatures = $this->getAllowedFeatures()->getData();

        foreach ($allowedFeatures as $featureType => $allowedFeaturesDefinition) {

            $blockedFeaturesDefinition = $blockedFeatures[$featureType];
            if ($allowedFeaturesDefinition && $blockedFeaturesDefinition) {
                $this->_internalValidateFeatureType($allowedFeaturesDefinition, $blockedFeaturesDefinition, $featureType);
            }
        }
    }

    /**
     * Validate subscope of a features
     * @see _validateBlockedFeatures();
     * @param type $blockedFeatures
     * @param type $allowedFeatures
     * @throws Exception
     */
    private function _internalValidateFeatureType($allowedFeatures, $blockedFeatures, $featureType) {
        foreach ($blockedFeatures as $path) {
            $pos = array_search($path, $allowedFeatures);
            if ($pos !== false) {
                throw new Exception("Conflict 'allowed_features' and' blocked_features' with same scope '{$featureType}' with path '{$path}' was not permited");
            }
        }
    }

    /**
     * Get array(Module,module2)
     * @return array
     */
    public function getModules() {
        if (null === $this->_modules) {
            $return = parent::getModules();
            $this->_modules = array_filter($return);
            
        }
        return $this->_modules;
    }

    /**
     * 
     * @return Jn2_MaxLoja_Model_Agile_Definition_Resources
     */
    public function getResources() {
        return $this->_resources;
    }

    /**
     * 
     * @return Jn2_MaxLoja_Model_Agile_Definition_Features
     */
    public function getAllowedFeatures() {
        return $this->_allowedFeatures;
    }

    /**
     * 
     * @return Jn2_MaxLoja_Model_Agile_Definition_Features
     */
    public function getBlockedFeatures() {
        return $this->_blockedFeatures;
    }

}
