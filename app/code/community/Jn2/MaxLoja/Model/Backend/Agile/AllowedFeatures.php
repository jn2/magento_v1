<?php

class Jn2_MaxLoja_Model_Backend_Agile_AllowedFeatures extends Mage_Core_Model_Config_Data {

    const SEPARATOR = "\n";

    protected function _beforeSave() {
        parent::_beforeSave();
        $value = $this->getValue();
        $this->setValue(null);
        if ($value) {
            $this->setValue(join(self::SEPARATOR, $value));
        }
    }

    protected function _afterLoad() {
        parent::_afterLoad();
        $value = preg_split("@[\r\n]+@", $this->getValue());
        $value = array_map(function($v) {
            return trim($v);
        }, array_filter($value));

        $this->setValue($value);
    }

}
