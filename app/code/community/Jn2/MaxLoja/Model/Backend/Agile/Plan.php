<?php

class Jn2_MaxLoja_Model_Backend_Agile_Plan extends Mage_Core_Model_Config_Data {

    const SEPARATOR = "\n";

    protected function _beforeSave() {
        parent::_beforeSave();
        $agile = Mage::getSingleton('maxloja/agile');
        $helper = $agile->getHelper();

        $newPlan = $helper->getPlanByConfig($this->getValue());
        $isPlanActive = in_array($newPlan->getActive(), array('1', 'true'));
        $planIsNotActive = !$isPlanActive;
        $configChanged = $this->getValue() != $helper->getPlanConfig();

        if ($configChanged && $planIsNotActive) {
            Mage::throwException("Este plano encontra-se desativado, por isto não é possível selecioná-lo");
        }
    }

}
