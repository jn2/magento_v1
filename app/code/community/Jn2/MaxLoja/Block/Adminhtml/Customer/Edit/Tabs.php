<?php

class Jn2_MaxLoja_Block_Adminhtml_Customer_Edit_Tabs extends Mage_Adminhtml_Block_Customer_Edit_Tabs {

    public function addTab($tabId, $tab)
    {
        $tabs = Mage::getSingleton('maxloja/agile')
            ->getHelper()
            ->getMergedBlockedFeatures()
            ->getCustomerTab();

        if (!in_array($tabId, $tabs)){
            return parent::addTab($tabId, $tab);
        }

        return $this;
    }

}