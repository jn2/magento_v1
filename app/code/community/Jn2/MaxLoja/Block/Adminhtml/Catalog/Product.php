<?php
class Jn2_MaxLoja_Block_Adminhtml_Catalog_Product extends Mage_Adminhtml_Block_Catalog_Product {

	/**
	 * Prepare button and grid
	 *
	 * @return Mage_Adminhtml_Block_Catalog_Product
	 */
	protected function _prepareLayout() {
		$parent = parent::_prepareLayout();
		$helper = Mage::helper('maxloja');

		if (!$helper->canAddProduct()) {
			$this->_removeButton('add_new');
			$this->_addButton('add_new', array(
				'label'   => Mage::helper('catalog')->__('Limite de produtos atingido'),
				// 'onclick' => "setLocation('{$this->getUrl('*/*/new')}')",
				// 'class'   => 'add'
			));
		}

			return $parent;
	}

}
