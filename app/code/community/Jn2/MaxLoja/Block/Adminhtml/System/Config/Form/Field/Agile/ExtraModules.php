<?php

class Jn2_MaxLoja_Block_Adminhtml_System_Config_Form_Field_Agile_ExtraModules extends Mage_Adminhtml_Block_System_Config_Form_Field {

    public function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        $definition = Mage::getSingleton('maxloja/agile')->getHelper()->getCurrentPlan()->getDefinition();
        $html .= '<input type="hidden" name="' . $element->getName() . '" value="" />';

        $modules = $definition->getModules();        
        if ($modules) {
            $afterHtml = sprintf($this->__('Módulos já contemplados pelo plano atual.<br/> ( %s )'), join(',', $definition->getModules()));
        }else{
            $afterHtml = sprintf($this->__('Seu plano não contempla nenhum módulo por padrão'), join(',', $definition->getModules()));
        }

        return $html . parent::_getElementHtml($element) . $afterHtml;
    }

}
