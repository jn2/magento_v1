<?php

class Jn2_MaxLoja_Block_Adminhtml_System_Config_Form_Field_Agile_Plan extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected function _filterElementOptions(Varien_Data_Form_Element_Abstract $element) {
        $helper = Mage::helper('maxloja/agile');
        $currentPlanConfig = $helper->splitPlanConfig($element->getValue());


        //remove other versions of current plan
        $values = $element->getValues();
        $newValues = array();
        foreach ($values as $args) {
            $_ary = $helper->splitPlanConfig($args['value']);
            if ($currentPlanConfig['version']) {
                if ($_ary['plan'] == $currentPlanConfig['plan'] && $_ary['version'] !== $currentPlanConfig['version']) {
                    continue;
                }
            }
            $newValues[$_ary['plan']] = $args;
        }
        $element->setValues($newValues);
    }

    public function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {

        $separator = Jn2_MaxLoja_Model_Agile_Plan_Adapter_Interface::PLAN_VERSION_SEPARATOR;

        $agile = Mage::getSingleton('maxloja/agile');

        $helper = $agile->getHelper();
        $currentPlanObject = $helper->getCurrentPlan();

        $currentPlan = $currentPlanObject->getName();        
        $currentVersion = $currentPlanObject->getVersion();
        

        //update value in case of not plan in database with version
        $newValue = join($separator, array($currentPlan, $currentVersion));
        $element->setValue($newValue);

        //filter plan versions of object
        $this->_filterElementOptions($element);

        //get lastest version of plan
        $lastPlanVersion = $agile->getPlan($currentPlan, null);
        $lastVersion = $lastPlanVersion->getVersion();


        $result = parent::_getElementHtml($element);
        $appendHtml = "Versão Atual {$currentVersion}";
        if ($lastVersion != $currentVersion) {
            $elementId = $element->getHtmlId();
            $updateId = "{$updateId}_update";



            $appendHtml = <<<EOD
                    
                    <label id="{$updateId}">                    
                        {$appendHtml} <br/>
                        <input type='checkbox' name='{$element->getName()}' value='{$currentPlan}:{$lastVersion}' />
                            Atualizar para versão {$lastVersion}
                        <label>
                        <script type="text/javascript">
                            (function(){
                                var \$element = $('{$elementId}');
                                var \$updateElement = $('{$updateId}');
                                var separator='{$separator}';
                                \$element.observe('change',function(){
                                    if(this.value.split(separator)[0] === '{$currentPlan}'){
                                        \$updateElement.show();
                                        \$updateElement.select('input')[0].disabled=null;
                                    }else{
                                        \$updateElement.hide();
                                        \$updateElement.select('input')[0].disabled='disabled';
                                    }
                                });
                            })();
                            
                        </script>
EOD;
        }

        return $result . $appendHtml;
    }

}
