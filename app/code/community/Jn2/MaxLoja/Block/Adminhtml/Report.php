<?php

class Jn2_MaxLoja_Block_Adminhtml_Report extends Clean_SqlReports_Block_Adminhtml_Report
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $helper = Mage::helper('maxloja/agile');

        $this->_controller = 'adminhtml_customreport';
        $this->_blockGroup = 'cleansql';
        $this->_headerText = Mage::helper('core')->__('Gerenciador de Relatórios');
        $this->_addButtonLabel = Mage::helper('core')->__('Adicionar Relatório');
        if( !$helper->getIsBypass() ) {
            $this->_removeButton('add');
        }

    }

    protected function _prepareLayout()
    {
        return Mage_Adminhtml_Block_Widget_Container::_prepareLayout();
    }
}
