<?php
class Jn2_MaxLoja_IndexController extends Mage_Core_Controller_Front_Action{
  public function IndexAction() {
    $this->loadLayout();
    $this->getLayout()->getBlock("head")->setTitle($this->__("Loja em Manutenção"));
    $this->renderLayout();

    $helper       = Mage::helper('maxloja');
    $active_store = Mage::getStoreConfig('max_loja/config/active_store');

    if($helper->adminLogged() || $active_store) {
      Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl());
    }
    header('HTTP/1.1 503 Service Temporarily Unavailable');
    header('Status: 503 Service Temporarily Unavailable');
  }
}
