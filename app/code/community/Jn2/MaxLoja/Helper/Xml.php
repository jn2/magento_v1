<?php

class Jn2_MaxLoja_Helper_Xml extends Mage_Core_Helper_Abstract {

    public function toDefinitionArray(SimpleXMLElement $element) {
        $definitionArray = $element ? $element->asArray() : array();
        
        $definitionArray['modules'] = array_keys((array)$definitionArray['modules']);


        $definitionArray['blocked_features'] = array();
        
        foreach ($element->xpath('blocked_features/*') as $config) {
            $rows = (array)$definitionArray['blocked_features'][$config->getName()];            
            $rows[] = (string) current($config->xpath('@path'));
            $definitionArray['blocked_features'][$config->getName()] = $rows;
        }
        $definitionArray['allowed_features'] = array();
        
        foreach ($element->xpath('allowed_features/*') as $config) {            
            $rows = (array)$definitionArray['allowed_features'][$config->getName()];            
            $rows[] = (string) current($config->xpath('@path'));
            $definitionArray['allowed_features'][$config->getName()] = $rows;         
        }
        
        return $definitionArray;
    }

}
