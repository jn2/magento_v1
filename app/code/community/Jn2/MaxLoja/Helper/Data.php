<?php

class Jn2_MaxLoja_Helper_Data extends Mage_Core_Helper_Abstract {

    const TOTAL_THEME_BLOCKS = 10;
    const TOTAL_THEME_PRODUCTS_LISTS = 3;

    public function getMaxProducts() {
        $resources = Mage::getSingleton('maxloja/agile')->getHelper()->getMergedResources();
        return $resources->getMaxProducts();
    }

    public function canAddProduct() {

        $maxprods = $this->getMaxProducts();

        if ($maxprods > 0) {
            $products = Mage::getModel('catalog/product')->getCollection();
            $count = $products->getSize();
            if ($count < $maxprods) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function canEditProduct() {

        $maxprods = $this->getMaxProducts();

        if ($maxprods > 0) {
            $products = Mage::getModel('catalog/product')->getCollection();
            $count = $products->getSize();
            if ($count <= $maxprods) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function adminLogged() {
        $ses_id = isset($_COOKIE['adminhtml']) ? $_COOKIE['adminhtml'] : false;
        $session = false;
        if ($ses_id) {
            $session = Mage::getSingleton('core/resource_session')->read($ses_id);
        }
        $logged_in = false;
        if ($session) {
            if (stristr($session, 'Mage_Admin_Model_User')) {
                $logged_in = true;
            }
        }
        return $logged_in;
    }

    public function hideConfig( Varien_Simplexml_Element $config) {
            $config->show_in_default = 0;
            $config->show_in_website = 0;
            $config->show_in_store = 0;

    }

    /**
     * Configurações que não serão visíveis para o cliente
     * @param type $observer
     * @return array()
     */
//    public function getBlockedConfigs($observer) {
//
//
////        $configs[] = $config->getNode('sections/custom_theme/groups/footer/fields/active');
////        $configs[] = $config->getNode('sections/custom_theme/groups/footer/fields/footer_theme');
////        $configs[] = $config->getNode('sections/custom_theme/groups/home/fields/active');
////        $configs[] = $config->getNode('sections/custom_theme/groups/home/fields/home_theme');
////        $configs[] = $config->getNode('sections/custom_theme/groups/header/fields/active');
////        $configs[] = $config->getNode('sections/custom_theme/groups/header/fields/theme');
////        $configs[] = $config->getNode('sections/custom_theme/groups/colors');
////        $configs[] = $config->getNode('sections/custom_theme/groups/general/fields/custom');
////        $configs[] = $config->getNode('sections/custom_theme/groups/general/fields/font');
////        $configs[] = $config->gcanAddProductetNode('sections/custom_theme/groups/general/fields/background_img');
////        // $configs[] = $config->getNode('sections/megamenu/groups/config/fields/items_per_line');
////        $configs[] = $config->getNode('sections/megamenu/groups/config/fields/theme');
////        $configs[] = $config->getNode('sections/jn2_mobile/groups/colors');
////        $configs[] = $config->getNode('sections/design/groups/package');
////        $configs[] = $config->getNode('sections/design/groups/theme');
////        $configs[] = $config->getNode('sections/design/groups/theme');
////        $configs[] = $config->getNode('sections/cache');
////        $configs[] = $config->getNode('sections/advanced');
//
//        return $configs;
//    }

    /**
     * Os temas possuem números de blocos e de lista de produtos variáveis.
     * Por isto iremos ocultar as configurações desnecessárias para o cliente.
     *
     *
     * @param Varien_Event $observer
     * @return array Configurações que serão ocultadas para o cliente.
     */
    public function getExtraHomeBlockedConfigs($observer) {
        $config = $observer->getConfig();
        $configs = [];

        $theme = Mage::getStoreConfig('custom_home/template/home_theme');

        $blockQtys = [
            'theme1' => 3,
            'theme2' => 6,
            'theme3' => 7,
            'theme4' => 4,
            'theme5' => 5,
            'theme6' => 4,
            'theme7' => 5,
            'theme8' => 10,
            'theme9' => 4
        ];

        $productsQtys = [
            'theme1' => 1,
            'theme2' => 1,
            'theme3' => 1,
            'theme4' => 2,
            'theme5' => 2,
            'theme6' => 1,
            'theme7' => 1,
            'theme8' => 3,
            'theme9' => 2
        ];

        for ($i = $blockQtys[$theme] + 1; $i <= self::TOTAL_THEME_BLOCKS; $i++) {
            $configs[] = $config->getNode("sections/custom_home/groups/blocks/fields/bloco_{$i}");
            $configs[] = $config->getNode("sections/custom_home/groups/blocks/fields/bloco_{$i}_fluid");
        }

        for ($i = $productsQtys[$theme] + 1; $i <= self::TOTAL_THEME_PRODUCTS_LISTS; $i++) {
            $configs[] = $config->getNode("sections/custom_home/groups/blocks/fields/products_{$i}");
            $configs[] = $config->getNode("sections/custom_home/groups/blocks/fields/qty_product_{$i}");
            $configs[] = $config->getNode("sections/custom_home/groups/blocks/fields/column_product_{$i}");
            $configs[] = $config->getNode("sections/custom_home/groups/blocks/fields/products_show_category_{$i}");
            $configs[] = $config->getNode("sections/custom_home/groups/blocks/fields/carrousel_product_{$i}");
        }
        return $configs;
    }

}
