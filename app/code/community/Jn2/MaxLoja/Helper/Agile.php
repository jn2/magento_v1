<?php

class Jn2_MaxLoja_Helper_Agile extends Mage_Core_Helper_Abstract {

    const CONFIG_AGILE_PLAN_PATH = 'max_loja/plans/current_plan';

    /*
     * @var Jn2_MaxLoja_Model_Agile
     */

    protected $_facade;

    /**
     *
     * @var Jn2_MaxLoja_Model_Agile_Definition_Features 
     */
    protected $_mergedBlockedFeatures;

    /**
     * 
     * @return Jn2_MaxLoja_Model_Agile
     */
    public function getFacade() {
        $this->_facade = $this->_facade == null ? Mage::getSingleton('maxloja/agile') : $this->_facade;
        return $this->_facade;
    }

    /**
     * Get database config to a plan
     * @return string 'plan:version'
     */
    public function getPlanConfig() {
        return trim(Mage::getStoreConfig(self::CONFIG_AGILE_PLAN_PATH, 0));
    }

    /**
     * 
     * @param string $value - 'plan:version'
     * @return array - array('plan' =>  .., 'version' => ...)
     */
    public function splitPlanConfig($value) {
        list($currentPlan, $currentVersion) = explode(Jn2_MaxLoja_Model_Agile_Plan_Adapter_Interface::PLAN_VERSION_SEPARATOR, $value);
        return array(
            'plan' => $currentPlan,
            'version' => $currentVersion
        );
    }

    /**
     * Get plan by config string with version 'plan:version'
     * 
     * @return Jn2_MaxLoja_Model_Agile_Plan
     */
    public function getPlanByConfig($configValue) {
        $configs = $this->splitPlanConfig($configValue);
        return $this->getFacade()->getPlan($configs['plan'], $configs['version']);
    }

    /**
     * Get Current selected plan
     * 
     * @return Jn2_MaxLoja_Model_Agile_Plan
     */
    public function getCurrentPlan() {
        $return =  $this->getPlanByConfig($this->getPlanConfig());
        if(null == $return){
            throw new Exception("Plan not foud {$this->getPlanConfig()}");
        }
        return $return;
    }

    /**
     * #priority (x)  - Minor have more power
     * 
     * - getCustom() is the low level priority  -                (1000)
     * - getPlan() resource value is the most important after local configuration (100)     
     * - Database() Storekeeper configuration is the most important value after global - (10)
     * - getBase() is like !important is allways applied - priority (0)
     * 
     * 
     * Return resources definitions
     * @return Jn2_MaxLoja_Model_Agile_Definition_Features
     */
    public function getMergedBlockedFeatures() {

        $arrayWithPriority = array(
            #minimal priority
            $this->getFacade()->getCustom(),
            $this->getCurrentPlan()->getDefinition(),
            $this->getFacade()->getDataSource()->getDefinition(), //put database config here            
            #maximal priority
            $this->getFacade()->getBase()
        );

        # print_r($this->getFacade()->getDataSource()->getDefinition()->getAllowedFeatures()->getAcl());die;

        /**
         * Merge blocked configurations
         */
        $aryBlockedFeatures = array();
        foreach ($arrayWithPriority as $_auxDefinition) {
            if ($_auxDefinition) {
                $blockedFeatures = $_auxDefinition->getBlockedFeatures();
                $allowedFeatures = $_auxDefinition->getAllowedFeatures();
                $aryBlockedFeatures = $this->_mergeBlockedFeature($aryBlockedFeatures, $blockedFeatures, $allowedFeatures);
            }
        }

        return Mage::getModel('maxloja/agile_definition_features', $aryBlockedFeatures);
    }

    /**
     *  - Merge Acumulator array with more blocked features by type(Acl,config...)
     * -  Remove from acumulator features that are in $allowedFeatures;
     * 
     * 
     * @param array $rows - Acumulator -  array('config' => array(0 => 'sections...),'acl' => '..')
     * @param Jn2_MaxLoja_Model_Agile_Definition_Features $blockedFeatures
     * @param Jn2_MaxLoja_Model_Agile_Definition_Features $allowedFeatures
     * @return array - array('config' => array(0 => 'sections...),'acl' => '..')
     */
    protected function _mergeBlockedFeature(array &$acumulator, $blockedFeatures, $allowedFeatures) {
        if (is_object($blockedFeatures)) {
            $blockedFeatures = $blockedFeatures->getData();
        }
        if (is_object($allowedFeatures)) {
            $allowedFeatures = $allowedFeatures->getData();
        }
        foreach ((array) $blockedFeatures as $featureType => $blockedFeatureData) {
            $acumulator[$featureType] = array_merge((array) $acumulator[$featureType], (array) $blockedFeatureData); #more blocked configuration                
        }
        //remove allowed
        foreach ((array) $allowedFeatures as $featureType => $allowedFeatureData) {
            $acumulator[$featureType] = array_diff((array) $acumulator[$featureType], (array) $allowedFeatureData); #remove allowed configuration from blocked
        }

        return $acumulator;
    }

    /**
     * #priority (x)  - Minor have more power
     * 
     * - getCustom() is the low level priority  -                (1000)
     * - getPlan() resource value is the most important after local configuration (100)     
     * - Database() Storekeeper configuration is the most important value after global - (10)
     * - getBase() is like !important is allways applied - priority (0)
     * 
     * 
     * Return resources definitions
     * 
     */
    public function getMergedResources() {
        $arrayWithPriority = array(
            #minimal priority
            $this->getFacade()->getCustom()->getResources(),
            $this->getCurrentPlan()->getDefinition()->getResources(),
            $this->getFacade()->getDataSource()->getDefinition()->getResources(), //put database config here            
            #maximal priority
            $this->getFacade()->getBase()->getResources()
        );


        $aryResources = array();
        foreach ($arrayWithPriority as $_auxResource) {
            if ($_auxResource) {
                $aryResources = array_merge($aryResources, $_auxResource->getData()); # $_auxResource will overwrite if they have same keys                                         
            }
        }
        //é utilizado a calsse definition resources caso seja necessário criar objetos
        //para tratar diferentemente cada tipo de resource
        $resources = Mage::getModel('maxloja/agile_definition_resources', $aryResources);
        return $resources;
    }

    /**
     * @param boolean $value
     * @return Jn2_MaxLoja_Helper_Agile
     */
    public function setBypass($value = true) {
        Mage::getModel('core/cookie')->set('custom_theme', $value);
        $_COOKIE['custom_theme'] = $value; //set variable to avoid reload page to reload config
        return $this;
    }

    /**
     * 
     * @return boolean
     */
    public function getIsBypass() {        
        return (bool) Mage::getModel('core/cookie')->get('custom_theme');
    }

}
