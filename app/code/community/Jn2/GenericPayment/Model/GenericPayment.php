<?php

class Jn2_GenericPayment_Model_GenericPayment extends Mage_Payment_Model_Method_Cc {

    protected $_code = 'genericpayment';

    protected $_isGateway = false;

    protected $_canAuthorize = false;

    protected $_canCapture = false;

    protected $_canCapturePartial = false;

    protected $_canRefund = false;

    protected $_canVoid = false;

    protected $_canUseInternal = false;

    protected $_canUseCheckout = false;

    protected $_canUseForMultishipping  = false;

    protected $_canSaveCc = false;

    /**
     * Cash On Delivery payment block paths
     *
     * @var string
     */
//    protected $_formBlockType = 'payment/form_cashondelivery';
    protected $_infoBlockType = 'genericpayment/info';

    public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));
    }

}