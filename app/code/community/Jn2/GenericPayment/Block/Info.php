<?php
/**
 * Title
 *
 * @category
 * @package
 * @author      Gustavo Bacelar <gustavo@jn2.com.br>
 * @description
 */

class Jn2_GenericPayment_Block_Info extends Mage_Payment_Block_Info {

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('jn2/genericpayment/info.phtml');
    }

    public function getOrder() {
        $order_id   = Mage::app()->getRequest()->getParams()['order_id'];
        return Mage::getModel('sales/order')->load($order_id);
    }
}