<?php

/**
 * Class Jn2_Checkout_Model_Cron
 *
 * @author Thiago Machado <thiago.machado@jn2.com.br>
 * @version 1.0
 */
class Jn2_Checkout_Model_Cron extends Mage_Core_Model_Abstract
{
    /**
     * Job para corrigir o cadastro de endereços de clientes que estão com o nome completo no campo firstname.
     * Exemplo:
     * - Antes de executar o job:
     *      firstname: "João da Silva"
     *      lastname: ""
     *
     * - Depois de executar o job:
     *      firstname: "João"
     *      lastname: "da Silva"
     */
    public function fixCustomerAddressName()
    {
        /** @var Mage_Customer_Model_Resource_Address_Collection $collection */
        $collection = Mage::getModel('customer/address')->getCollection()
            ->addAttributeToSelect('firstname')
            ->addAttributeToSelect('lastname');

        foreach ($collection as $address) {
            if ($address->getFirstname() && !$address->getLastname()) {
                $parts = explode(' ', trim($address->getFirstname()));

                $address->setFirstname(array_shift($parts));
                $address->setLastname(implode(' ', $parts));
                $address->save();
            }
        }
    }
}