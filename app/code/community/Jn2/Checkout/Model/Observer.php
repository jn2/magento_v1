<?php

class Jn2_Checkout_Model_Observer
{
    public function splitName(Varien_Event_Observer $observer)
    {
        if (Mage::getStoreConfig('jn2_checkout/general/active') && Mage::app()->getRequest()->getParam('split_name')) {
            $customer = $observer->getCustomer();
            $parts = explode(' ', trim($customer->getFirstname()));

            $firstname = array_shift($parts);
            $lastname = implode(' ', $parts);

            if ($customer->getLastname() && empty($lastname)) {
                return;
            }
            
            $customer->setFirstname($firstname);
            $customer->setLastname($lastname);

            foreach ($customer->getAddresses() as $address) {
                $address->setFirstname($firstname);
                $address->setLastname($lastname);
            }
        }
    }
}
