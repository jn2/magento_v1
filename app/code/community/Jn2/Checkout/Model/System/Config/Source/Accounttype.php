<?php

/**
 * Class Jn2_Checkout_Model_System_Config_Source_Accounttype
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Checkout_Model_System_Config_Source_Accounttype
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'ambos',
                'label' => 'Ambos',
            ),
            array(
                'value' => 'fisica',
                'label' => 'Pessoa Física',
            ),
            array(
                'value' => 'juridica',
                'label' => 'Pessoa Jurídica',
            ),
        );
    }

}
