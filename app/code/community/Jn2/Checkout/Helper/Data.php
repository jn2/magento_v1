<?php

class Jn2_Checkout_Helper_Data extends Mage_Core_Helper_Abstract
{
    #TODO migrar essa configuração para o novo módulo
    const EXCLUDE_INCLUDE_IS_IM = 'onestepcheckout/exclude_include_fields/is_im';

    public function isIm($store = null)
    {
        return Mage::getStoreConfig(self::EXCLUDE_INCLUDE_IS_IM, $store);
    }
    
    public function setRegisterTemplate() {
        if (Mage::getStoreConfig("jn2_checkout/general/international_fields")) {
            return 'jn2/checkout/customer/form/international/register.phtml';
        } else {
            return 'jn2/checkout/customer/form/register.phtml';
        }
    }
}
