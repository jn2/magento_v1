<?php

class Jn2_Checkout_Block_Widget_Tipopessoa extends Mage_Customer_Block_Widget_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('jn2/checkout/customer/form/widget/tipopessoa.phtml');
    }

    public function isEnabled()
    {
        return (bool) $this->_getAttribute('tipopessoa')->getIsVisible();
    }

    public function isRequired()
    {
        return (bool) $this->_getAttribute('tipopessoa')->getIsRequired();
    }

    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
}
