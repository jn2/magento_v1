<?php

class Jn2_Checkout_Block_Widget_Dob extends Mage_Customer_Block_Widget_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('jn2/checkout/customer/form/widget/dob.phtml');
    }

    public function isEnabled()
    {
        return (bool) $this->_getAttribute('dob')->getIsVisible();
    }

    public function isRequired()
    {
        return (bool) $this->_getAttribute('dob')->getIsRequired();
    }

    public function setDate($date)
    {
        $this->setTime($date ? strtotime($date) : false);
        $this->setData('date', $date);
        return $this;
    }

    public function getDay()
    {
        return $this->getTime() ? date('d', $this->getTime()) : '';
    }

    public function getMonth()
    {
        return $this->getTime() ? date('m', $this->getTime()) : '';
    }

    public function getYear()
    {
        return $this->getTime() ? date('Y', $this->getTime()) : '';
    }

    public function getDateFormat()
    {
        return Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
    }

    public function setDateInput($code, $html)
    {
        $this->_dateInputs[$code] = $html;
    }

    public function getSortedDateInputs()
    {
        $strtr = array(
            '%b' => '%1$s',
            '%B' => '%1$s',
            '%m' => '%1$s',
            '%d' => '%2$s',
            '%e' => '%2$s',
            '%Y' => '%3$s',
            '%y' => '%3$s'
        );

        $dateFormat = preg_replace('/[^\%\w]/', '\\1', $this->getDateFormat());

        return sprintf(strtr($dateFormat, $strtr), $this->_dateInputs['m'], $this->_dateInputs['d'], $this->_dateInputs['y']);
    }

}
