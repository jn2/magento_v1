<?php

class Jn2_Checkout_Block_Widget_Taxvat extends Mage_Customer_Block_Widget_Taxvat
{
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('jn2/checkout/customer/form/widget/taxvat.phtml');
    }
}
