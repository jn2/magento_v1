<?php
class Jn2_Theme_Helper_Data extends Mage_Core_Helper_Abstract
{
  private $layouts;

  function __construct($value='')
  {
    $theme['header'] = array('path' => 'custom_header/template/theme','theme'   => 'theme4');
    $theme['home']   = array('path' => 'custom_home/template/home_theme','theme'=> 'theme3');
    $theme['footer'] = array('path' => 'custom_footer/template/theme','theme'   => 'theme1');
    $theme['menu']   = array('path' => 'megamenu/template/theme','theme'        => 'theme1');
    $this->layouts['theme1'] = $theme;

    $theme['header'] = array('path' => 'custom_header/template/theme','theme'   => 'theme3');
    $theme['home']   = array('path' => 'custom_home/template/home_theme','theme'=> 'theme2');
    $theme['footer'] = array('path' => 'custom_footer/template/theme','theme'   => 'theme2');
    $theme['menu']   = array('path' => 'megamenu/template/theme','theme'        => 'theme2');
    $this->layouts['theme2'] = $theme;
  }

  public function getLayout($theme)
  {
    return $this->layouts[$theme];
  }

  public function getPathCss()
  {    
    $theme = Mage::getStoreConfig('theme/config/layout');
    return 'jn2/theme/css/' . $theme . '.css';
  }

  public function getPathJs()
  {
    $theme = Mage::getStoreConfig('theme/config/layout');
    return 'jn2/theme/js/' . $theme . '.js';
  }
}
