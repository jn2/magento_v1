<?php
class Jn2_Theme_Model_System_Config_Source_Layout
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    return array(
      array('label' => 'Tema 1', 'value' => 'theme1'),
      array('label' => 'Tema 2', 'value' => 'theme2')
    );
  }

}
