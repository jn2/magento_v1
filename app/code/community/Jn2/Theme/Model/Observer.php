<?php
class Jn2_Theme_Model_Observer
{

  /**
  * Options getter
  *
  * @return array
  */
  public function loadLayouts($observer)
  {
    if(Mage::getStoreConfig('theme/config/active')){
      $theme  = Mage::getStoreConfig('theme/config/layout');
      $helper = Mage::helper('theme');
      $layout = $helper->getLayout($theme);

      foreach ($layout as $value) {
        $config = Mage::getStoreConfig($value['path'], Mage::app()->getStore());
        if ($config != $value['theme']) {
          Mage::getConfig()->saveConfig($value['path'], $value['theme']);
          Mage::getConfig()->reinit();
          Mage::app()->reinitStores();
        }
      }
    }
  }

  public function addHandle($observer)
  {
    if(Mage::getStoreConfig('theme/config/active')){
      $theme  = Mage::getStoreConfig('theme/config/layout');
      $request  = Mage::app()->getRequest();
      $layout   = $observer['layout'];
      $update = $layout->getUpdate();
      $handles = $update->getHandles();
      $pos = array_search('default',$handles);
      if($pos !== false){
        array_splice($handles, $pos+1, 0, 'theme_'.$theme);
        $update->resetHandles();

        foreach ($handles as $handle) {
          $update->addHandle($handle);
        }
      }
    }
  }

}
