<?php
class Jn2_CustomPage_Model_System_Config_Source_Theme
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    return array(
        array('label' => 'Tema 1', 'value' => 'theme1', 'title' => Mage::getDesign()->getSkinUrl('jn2/custompage/wireframes/w1.jpg')),                
        array('label' => 'Tema 2', 'value' => 'theme2', 'title' => Mage::getDesign()->getSkinUrl('jn2/custompage/wireframes/w2.jpg'))
    );
  }

}
