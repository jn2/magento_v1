<?php
class Jn2_CustomPage_Model_System_Config_Source_ProductsColumn
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    return array(
      array('label' => '2 Colunas', 'value' => '2'),
      array('label' => '3 Colunas', 'value' => '3'),
      array('label' => '4 Colunas', 'value' => '4'),
      array('label' => '6 Colunas', 'value' => '6')
    );
  }

}
