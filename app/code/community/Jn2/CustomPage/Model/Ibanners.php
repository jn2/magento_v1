<?php

class Jn2_CustomPage_Model_Ibanners extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{

    public function getAllOptions()
    {
        $banner_collection = Mage::getResourceModel('ibanners/group_collection');
        $options = array();

        foreach ($banner_collection as $group) {
            $options[] = array('value' => $group->getCode(), 'label' => $group->getTitle());
        }
        
        array_unshift($options, array('value' => '', 'label' => 'Selecione um banner...'));
        return $options;
    }

}
