<?php

class Jn2_CustomPage_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getTemplatePath()
    {

        if (Mage::getStoreConfig('custom_page/general/active')) {
            $theme = Mage::getStoreConfig('custom_page/theme/categoria_theme');                            
            return "jn2/custompage/wireframes/{$theme}.phtml";
        } else {
            return '';
        }
    }

}
