<?php

/**
 * Description of Price
 *
 * @author raphael
 */
class Jn2_CustomPage_Block_CategoryView extends Mage_Catalog_Block_Category_View
{

    public function __construct()
    {

    }

    public function getBanner($block_id)
    {
        $block = $this->getLayout()->createBlock('ibanners/view')->setGroupCode($block_id);
        return $block->toHtml();
    }

    public function getBlock($block_id)
    {
        if (is_numeric($block_id) && !empty($block_id)) {
            $block = $this->getLayout()->createBlock('cms/block')->setBlockId($block_id);
        } else {
            if (empty($block_id)) {
                return '';
            } else {
                $block = $this->getLayout()->createBlock('ibanners/view')->setGroupCode($block_id);
            }
        }
        return $block->toHtml();
    }

    /**
     * Função sobescrita para evitar o cache nesse bloco.
     * @inheritdoc
     */
    public function getCmsBlockHtml()
    {
        if (!$this->getData('cms_block_html')) {
            $html = $this->getLayout()->createBlock('cms/block')
                ->setBlockId($this->getCurrentCategory()->getLandingPage())
                ->setCacheLifetime(null)
                ->toHtml();
            $this->setData('cms_block_html', $html);
        }
        return $this->getData('cms_block_html');
    }
}
