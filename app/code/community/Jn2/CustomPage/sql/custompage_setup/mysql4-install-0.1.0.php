<?php

$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();

$setId = Mage::getSingleton('eav/config')->getEntityType('catalog_category')->getDefaultAttributeSetId();
$installer->addAttributeGroup('catalog_category', $setId, 'CustomPage');

$installer->addAttribute('catalog_category', "ibanner", array(
    "group" => "Pagina de Categoria",
    "type" => "varchar",
    "backend" => "",
    "frontend" => "",
    "label" => "Banner Principal",
    "input" => "select",
    "class" => "",
    "source" => "custompage/ibanners",
    "global" => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible" => true,
    "required" => false,
    "user_defined" => false,
    "default" => "",
    "searchable" => false,
    "filterable" => false,
    "comparable" => false,
    "used_in_product_listing" => true,
    "visible_on_front" => false,
    "unique" => false,
    "note" => ""
));

$installer->addAttribute('catalog_category', "bloco_1", array(
    "group" => "Pagina de Categoria",
    "type" => "varchar",
    "backend" => "",
    "frontend" => "",
    "label" => "Bloco CMS 1",
    "input" => "select",
    "class" => "",
    "source" => 'catalog/category_attribute_source_page',
    "global" => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible" => true,
    "required" => false,
    "user_defined" => false,
    "default" => "",
    "searchable" => false,
    "filterable" => false,
    "comparable" => false,
    "used_in_product_listing" => true,
    "visible_on_front" => false,
    "unique" => false,
    "note" => ""
));

$installer->addAttribute('catalog_category', "bloco_2", array(
    "group" => "Pagina de Categoria",
    "type" => "varchar",
    "backend" => "",
    "frontend" => "",
    "label" => "Bloco CMS 2",
    "input" => "select",
    "class" => "",
    "source" => 'catalog/category_attribute_source_page',
    "global" => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible" => true,
    "required" => false,
    "user_defined" => false,
    "default" => "",
    "searchable" => false,
    "filterable" => false,
    "comparable" => false,
    "used_in_product_listing" => true,
    "visible_on_front" => false,
    "unique" => false,
    "note" => ""
));

$installer->addAttribute('catalog_category', "bloco_3", array(
    "group" => "Pagina de Categoria",
    "type" => "varchar",
    "backend" => "",
    "frontend" => "",
    "label" => "Bloco CMS 3",
    "input" => "select",
    "class" => "",
    "source" => 'catalog/category_attribute_source_page',
    "global" => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible" => true,
    "required" => false,
    "user_defined" => false,
    "default" => "",
    "searchable" => false,
    "filterable" => false,
    "comparable" => false,
    "used_in_product_listing" => true,
    "visible_on_front" => false,
    "unique" => false,
    "note" => ""
));

$installer->addAttribute('catalog_category', "bloco_4", array(
    "group" => "Pagina de Categoria",
    "type" => "varchar",
    "backend" => "",
    "frontend" => "",
    "label" => "Bloco CMS 4",
    "input" => "select",
    "class" => "",
    "source" => 'catalog/category_attribute_source_page',
    "global" => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible" => true,
    "required" => false,
    "user_defined" => false,
    "default" => "",
    "searchable" => false,
    "filterable" => false,
    "comparable" => false,
    "used_in_product_listing" => true,
    "visible_on_front" => false,
    "unique" => false,
    "note" => ""
));

$installer->endSetup();
