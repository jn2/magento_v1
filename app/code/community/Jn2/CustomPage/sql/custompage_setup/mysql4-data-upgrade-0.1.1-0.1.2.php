<?php

/**
* Author Igor Gottschalg <igorgottsweb@gmail.com>
* 02/01/2017
**/

$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();

$setId            = Mage::getSingleton('eav/config')->getEntityType('catalog_category')->getDefaultAttributeSetId();
$installer->addAttributeGroup('catalog_category', $setId, 'CustomPage');

$installer->addAttribute("catalog_category", "top_category_image",  array(
  'group'        => 'General Information',
  "type"         => "text",
  "label"        => "Imagem da categoria no topo?",
  "input"        => "select",
  "source"       => "eav/entity_attribute_source_boolean",
  "global"       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
  "visible"      => true,
  "required"     => false,
  "user_defined" => false,
  "searchable"   => false,
  "filterable"   => false,
  "comparable"   => false,
  "used_in_product_listing" => true,
  'sort_order'   => 5
));

$installer->endSetup();
