<?php
require_once Mage::getModuleDir('controllers', 'Mage_Downloadable') . DS . 'DownloadController.php';

/**
 * Class Jn2_Downloadable_DownloadController
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Downloadable_DownloadController extends Mage_Downloadable_DownloadController
{

    /**
     * Método sobescrito para o Magento não fazer o download do arquivo
     * externo, mas redirecionar o usuário ao link final do arquivo.
     *
     * @param $resource
     * @param $resourceType
     */
    protected function _processDownload($resource, $resourceType)
    {
        if (Mage::getStoreConfig('catalog/downloadable/redirect_to_link')) {
            header("Location: {$resource}");
        } else {
            $helper = Mage::helper('downloadable/download');
            /* @var $helper Mage_Downloadable_Helper_Download */

            $helper->setResource($resource, $resourceType);

            $fileName = $helper->getFilename();
            $contentType = $helper->getContentType();

            $this->getResponse()
                ->setHttpResponseCode(200)
                ->setHeader('Pragma', 'public', true)
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
                ->setHeader('Content-type', $contentType, true);

            if ($fileSize = $helper->getFilesize()) {
                $this->getResponse()
                    ->setHeader('Content-Length', $fileSize);
            }

            if ($contentDisposition = $helper->getContentDisposition()) {
                $this->getResponse()
                    ->setHeader('Content-Disposition', $contentDisposition . '; filename=' . $fileName);
            }

            $this->getResponse()
                ->clearBody();
            $this->getResponse()
                ->sendHeaders();

            session_write_close();
            $helper->output();
        }
    }
}
