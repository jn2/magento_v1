<?php

class Jn2_Hint_Model_Observer {

    public function setHints() {
        $stores = array(Mage::app()->getStore());
        $stores[] = Mage::app()->getStore(0);

        if (isset($_GET['hint'])) {
            foreach ($stores as $store) {
                $store->setConfig(Mage_Core_Block_Template::XML_PATH_DEBUG_TEMPLATE_HINTS, 1, true);
                $store->setConfig(Mage_Core_Block_Template::XML_PATH_DEBUG_TEMPLATE_HINTS_BLOCKS, 1, true);
            }
        }

        if (isset($_GET['ActiveCielo'])) {
            setcookie('ActiveCielo', 1, null, '/');
            $_COOKIE['ActiveCielo'] = 1;
        }

        if (isset($_COOKIE['ActiveCielo']) && $_COOKIE['ActiveCielo']) {
//            print_r($_COOKIE); die();
            $current_store = $stores[0];
            $current_store->setConfig('payment/Maxima_Cielo_Cc/active', 1, true);
        }
        
        
        /*
        $translatePath = 'dev/translate_inline/active';
        $config = Mage::getConfig();

        if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {//somente local
            $_COOKIE['translate'] = isset($_COOKIE['translate']) ? $_COOKIE['translate'] : null;
            $_COOKIE['translate'] = isset($_GET['translate']) ? $_GET['translate'] : $_COOKIE['translate'];
            foreach ($stores as $store) {
                if ($_COOKIE['translate']) {
                    @setcookie('translate', 1, null, '/');
                    $store->setConfig($translatePath, 1, true);
                    # $config->saveConfig($translatePath, 1);
                    #$config = new Mage_Core_Model_Config();
                } else {
                    $store->setConfig($translatePath, null, true);
                    @setcookie('translate', null, null, '/');
                    # $config->saveConfig($translatePath, null);
                }
            }
        }
        */
    }

}
