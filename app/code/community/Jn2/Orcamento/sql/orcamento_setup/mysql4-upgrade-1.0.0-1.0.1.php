<?php

/**
 * mysql4-upgrade-1.0.0-1.0.1.php
 *
 * @author  Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */

$installer = $this;
$installer->startSetup();

$sql = <<<QUERY
CREATE TABLE {$installer->getTable('orcamento_pedido')} (
    id INT NOT NULL AUTO_INCREMENT,
    sku VARCHAR(255) NOT NULL,
    produto VARCHAR(255) NOT NULL,
    nome VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    telefone VARCHAR(255) NOT NULL,
    cep VARCHAR(255),
    estado VARCHAR(255),
    cidade VARCHAR(255),
    comentario TEXT,
    data_criacao DATETIME,
    PRIMARY KEY(id)
)
QUERY;

$installer->run($sql);
$installer->endSetup();
