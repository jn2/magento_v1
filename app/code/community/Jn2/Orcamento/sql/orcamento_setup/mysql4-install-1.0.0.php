<?php

/**
 * mysql4-install-1.0.0.php
 *
 * @author  Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */

$installer = $this;
$installer->startSetup();

$attributes = array(
    'price',
    'special_price',
    'special_from_date',
    'special_to_date',
    'minimal_price',
    'tax_class_id',
    'tier_price',
    'weight',
);

foreach ($attributes as $attributeCode) {
    $applyTo = explode(
        ',',
        $installer->getAttribute(
            Mage_Catalog_Model_Product::ENTITY,
            $attributeCode,
            'apply_to'
        )
    );

    if (!in_array('orcamento', $applyTo)) {
        $applyTo[] = 'orcamento';
        $installer->updateAttribute(
            Mage_Catalog_Model_Product::ENTITY,
            $attributeCode,
            'apply_to',
            join(',', $applyTo)
        );
    }
}

$installer->endSetup();
