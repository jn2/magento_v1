<?php

/**
 * Class Jn2_Orcamento_Block_Adminhtml_Pedido
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_Block_Adminhtml_Pedido extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_pedido';
        $this->_blockGroup = 'orcamento';
        $this->_headerText = 'Pedidos de Orçamento';
        parent::__construct();
        $this->removeButton('add');
    }
}
