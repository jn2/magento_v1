<?php

/**
 * Class Jn2_Orcamento_Block_Adminhtml_Pedido_Grid
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_Block_Adminhtml_Pedido_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('orcamentoGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('orcamento/pedido')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => 'ID',
            'type' => 'number',
            'index' => 'id'
        ));

        $this->addColumn('data_criacao', array(
            'header' => 'Data',
            'type' => 'datetime',
            'index' => 'data_criacao'
        ));

        $this->addColumn('nome', array(
            'header' => 'Nome',
            'type' => 'text',
            'index' => 'nome'
        ));

        $this->addColumn('email', array(
            'header' => 'E-mail',
            'type' => 'text',
            'index' => 'email'
        ));

        $this->addColumn('telefone', array(
            'header' => 'Telefone',
            'type' => 'text',
            'index' => 'telefone'
        ));

        $this->addColumn('cep', array(
            'header' => 'CEP',
            'type' => 'text',
            'index' => 'cep'
        ));

        $this->addColumn('estado', array(
            'header' => 'Estado',
            'type' => 'text',
            'index' => 'estado'
        ));

        $this->addColumn('cidade', array(
            'header' => 'Cidade',
            'type' => 'text',
            'index' => 'cidade'
        ));

        $this->addColumn('produto', array(
            'header' => 'Produto',
            'type' => 'text',
            'index' => 'produto'
        ));

        $this->addColumn('sku', array(
            'header' => 'SKU',
            'type' => 'text',
            'index' => 'sku'
        ));

        $this->addColumn('comentario', array(
            'header' => 'Comentários Adicionais',
            'type' => 'longtext',
            'index' => 'comentario'
            
        ));

        return parent::_prepareColumns();
    }
}
