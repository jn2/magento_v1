<?php

/**
 * Class Jn2_Orcamento_Block_Button
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_Block_Button extends Mage_Core_Block_Template
{
    public function getUrl()
    {
        $_product = Mage::registry('produto_orcamento');
        return $_product->getUrl();
    }
}
