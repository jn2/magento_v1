<?php

/**
 * Class Jn2_Orcamento_Block_Form
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_Block_Form extends Mage_Core_Block_Template
{
    public function getNome()
    {
        if ($user = $this->getUser()) {
            return $user->getFirstname() . ' ' . $user->getLastname();
        }

        return '';
    }

    public function getEmail()
    {
        if ($user = $this->getUser()) {
            return $user->getEmail();
        }

        return '';
    }

    public function getTelefone()
    {
        if ($address = $this->getAddress()) 
        {
            return $address->getPostcode();
        }

        return '';
    }

    public function getCep()
    {
        if ($address = $this->getAddress()) {
            return $address->getTelephone();
        }

        return '';
    }

    public function getEstado()
    {
        if ($address = $this->getAddress()) {
            return $address->getRegion();
        }

        return '';
    }

    public function getCidade()
    {
        if ($address = $this->getAddress()) {
            return $address->getCity();
        }

        return '';
    }

    public function getSku()
    {
        return Mage::registry('current_product')->getSku();
    }

    public function getProduto()
    {
        return Mage::registry('current_product')->getName();
    }

    private function getUser()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    private function getAddress()
    {
        if ($user = $this->getUser()) {
            return $user->getDefaultBillingAddress();
        }

        return null;
    }
}
