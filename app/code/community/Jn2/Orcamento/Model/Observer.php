<?php

/**
 * Class Jn2_Orcamento_Model_Observer
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_Model_Observer
{
    public function avoidAddToCart($observer)
    {
        $product = $observer->getProduct();

        if ($product->getTypeId() == Jn2_Orcamento_Model_Product_Type::TYPE_OR_PRODUCT) {
            $cart = Mage::helper('checkout/cart')->getCart();
            $cartItems = $cart->getItems();

            // bug do magento não permite recuperar o quote_item pelo id se só existe um item no carrinho
            // @see https://magento.stackexchange.com/questions/92867/removing-item-from-cart-on-observer-method
            if (count($cartItems) > 1) {
                foreach ($cart->getItems() as $cartItem) {
                    if ($cartItem->getProductId() == $product->getId()) {
                        $cart->removeItem($cartItem->getItemId());
                        $cart->save();
                    }
                }
            } else {
                $cart->removeItem(0);
                $cart->save();
            }
        }
    }
}
