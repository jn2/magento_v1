<?php

/**
 * Class Jn2_Orcamento_Model_Pedido
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_Model_Pedido extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('orcamento/pedido');
    }
}
