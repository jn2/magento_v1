<?php

/**
 * Class Jn2_Orcamento_Model_Product_Type
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_Model_Product_Type extends Mage_Catalog_Model_Product_Type_Simple
{
    const TYPE_OR_PRODUCT = 'orcamento';
}

