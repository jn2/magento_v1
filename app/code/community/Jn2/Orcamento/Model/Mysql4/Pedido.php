<?php

/**
 * Class Jn2_Orcamento_Model_Mysql4_Pedido
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_Model_Mysql4_Pedido extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('orcamento/pedido', 'id');
    }
}
