<?php

/**
 * Class Jn2_Orcamento_Model_Mysql4_Pedido_Collection
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_Model_Mysql4_Pedido_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('orcamento/pedido');
    }
}
