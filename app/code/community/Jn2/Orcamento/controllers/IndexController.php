<?php

/**
 * Class Jn2_Orcamento_IndexController
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_IndexController extends Mage_Core_Controller_Front_Action
{
    protected $_pedido;

    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle('Orçamento Recebido');

        if (!$this->getRequest()->getParam('cache-get')) {
            $this->saveOrder();
            $this->sendEmail();
        }

        $this->renderLayout();
    }

    protected function saveOrder()
    {
        $params = $this->getRequest()->getParam('orcamento');

        if (empty($params)) {
            Mage::getSingleton('core/session')->addError('Parâmetros não informados.');
            $this->getResponse()->setRedirect(Mage::getUrl());
        }

        $this->_pedido = Mage::getModel('orcamento/pedido');
        $this->_pedido->setData($params);
        $this->_pedido->setData('data_criacao', date('Y-m-d H:i:s'));
        $this->_pedido->save();
    }

    protected function sendEmail()
    {
        $nome = Mage::getStoreConfig('trans_email/ident_general/name');
        $email = Mage::getStoreConfig('trans_email/ident_general/email');

        try {
            Mage::getModel('core/email_template')
                ->setSenderName($nome)
                ->setSenderEmail($email)
                ->loadDefault('orcamento_pedido')
                ->send($email, 'Pedido de Orçamento', $this->_pedido->getData());
        } catch (Exception $e) {
            Mage::getSingleton('core/session')
                ->addError("Não conseguimos receber o seu e-mail. Por favor, entre em contato conosco no email {$email}. Obrigado!");
            $this->getResponse()->setRedirect(Mage::getUrl());
        }
    }
}
