<?php

/**
 * Class Jn2_Orcamento_Adminhtml_PedidoController
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 1.0.0
 */
class Jn2_Orcamento_Adminhtml_PedidoController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title('Pedidos de Orçamento');
        $this->_setActiveMenu('sales/orcamento_pedidos');
        $this->_addBreadcrumb('Pedidos de Orçamento', 'orcamento_pedidos');
        $this->renderLayout();
    }
}
