<?php

/**
 *
 */
class Jn2_CheckoutComment_Model_Observer
{

  function saveCustomerComment($observer) {
    $comments = Mage::app()->getRequest()->getParams();
    $comments = $comments['comments'];

    if (!empty($comments)) {
      $order = $observer->getEvent()->getOrder();
      $order->addStatusHistoryComment($comments);
      $order->save();
    }

  }
}
