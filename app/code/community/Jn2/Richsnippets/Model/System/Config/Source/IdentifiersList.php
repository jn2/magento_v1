<?php

/**
 * Class Jn2_Richsnippets_Model_System_Config_Source_IdentifiersList
 */
class Jn2_Richsnippets_Model_System_Config_Source_IdentifiersList
{

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        $options[] = ['label' => 'Nenhum', 'value' => ''];
        $options[] = ['label' => 'gtin', 'value' => 'gtin'];
        $options[] = ['label' => 'mpn', 'value' => 'mpn'];
        return $options;
    }

}