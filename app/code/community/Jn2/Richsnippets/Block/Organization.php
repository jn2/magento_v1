<?php

class Jn2_Richsnippets_Block_Organization extends Mage_Core_Block_Template {

    public function __construct() {
        $jn2Header = Mage::helper("header/data");
        $logoUrl = $jn2Header->getLogoUrl();
        $phone = "+55-".trim(Mage::getStoreConfig('general/store_information/phone'));

        $this->setData('url', Mage::getBaseUrl());
        $this->setData('logo', $logoUrl);
        $this->setData('phone', $phone);
    }

    public function canShow() {
        return !Mage::getStoreConfig('rich_sneppets/rich_sneppets/active');
    }

}
