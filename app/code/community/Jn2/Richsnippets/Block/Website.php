<?php

class Jn2_Richsnippets_Block_Website extends Mage_Core_Block_Template {

    public function __construct() {
        $searchUrl = Mage::getBaseUrl()."catalogsearch/result/?q={search_term_string}";
        $this->setData('url', Mage::getBaseUrl());
        $this->setData('searchUrl', $searchUrl);
    }

    public function canShow() {
        return !Mage::getStoreConfig('rich_sneppets/rich_sneppets/active');
    }

}
