<?php

class Jn2_Richsnippets_Block_Breadcrumb extends Mage_Catalog_Block_Product_Price {

    private $itemListElement;

    public function __construct() {
        $urlPage = Mage::getUrl('', array(
            '_current' => true,
            '_use_rewrite' => true
        ));
        $product = $this->getProduct();

        $itemListElement[] = [
            "@type" => "ListItem",
            "position" => 1,
            "name" => $product->getName(),
            "item" => $product->getUrl()
        ];
        $this->itemListElement = json_encode($itemListElement);
    }

    public function canShow() {
        return !Mage::getStoreConfig('rich_sneppets/rich_sneppets/active');
    }

    public function getItemListElement(){
        return $this->itemListElement;
    }

}
