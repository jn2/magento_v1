<?php

class Jn2_Richsnippets_Block_Product extends Mage_Catalog_Block_Product_Price {

  public function __construct() {
    $product  = $this->getProduct();

    $store_id     = Mage::app()->getStore()->getId();
    $summary_data = Mage::getModel('review/review_summary')->setStoreId($store_id)->load($product->getId());

    $review_count = $summary_data->getReviewsCount();
    $review_count = empty($review_count) ? 0 : $review_count;

    $urlProduct = Mage::getUrl('', array(
      '_current' => true,
      '_use_rewrite' => true
    ));

    $this->setData('review', (5*$summary_data->getRatingSummary())/100);
    $this->setData('review_count', $review_count);
    $this->setData('urlProduct', $urlProduct);
    $this->setData('product', $product);
  }

  public function getItemAvailability() {
    $product  = $this->getProduct();
    // $product = Mage::getModel('catalog/product')->load($this->getProduct()->getId());
    $in_stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getIsInStock();

    if ($in_stock) {
      return "http://schema.org/InStock";
    } else {
      return "http://schema.org/OutOfStock";
    }
  }

  public function getPrice() {
    $product  = $this->getProduct();
    return Mage::helper('core')->currency($product->getFinalPrice(), false, false);
  }

  public function getCurrency() {
    return Mage::app()->getStore()->getCurrentCurrencyCode();
  }

    public function getBrand(){
        $brand = Mage::getStoreConfig("rich_sneppets/rich_sneppets/field_brand");
        if(!empty($brand)){
            $nameBrand = $this->getProduct()->getData($brand);
            return $nameBrand;
        }else{
            return false;
        }
    }

  public function getIdentifier(){
      $identifier = Mage::getStoreConfig("rich_sneppets/rich_sneppets/identifier");
      if(!empty($identifier)){
          if($identifier == "gtin"){
              $valueField = $this->getProduct()->getData(Mage::getStoreConfig("rich_sneppets/rich_sneppets/field_identifier"));
              if(strlen($valueField) == "8"){
                  return "gtin8";
              }elseif(strlen($valueField) == "12"){
                  return "gtin12";
              }elseif(strlen($valueField) == "13"){
                  return "gtin13";
              }elseif(strlen($valueField) == "14"){
                  return "gtin14";
              }
          }else{
              return $identifier;
          }
      }else{
          return false;
      }
  }

  public function getFieldIdentifier(){
      $valueField = $this->getProduct()->getData(Mage::getStoreConfig("rich_sneppets/rich_sneppets/field_identifier"));
      if(!empty($valueField)){
          return $valueField;
      }else{
          return false;
      }
  }

  public function canShow() {
    return !Mage::getStoreConfig('rich_sneppets/rich_sneppets/active');
  }

}
