<?php

class Jn2_Richsnippets_Block_Breadcrumbcategory extends Mage_Core_Block_Template {

    private $itemListElement;

    public function __construct() {
        $urlPage = Mage::getUrl('', array(
            '_current' => true,
            '_use_rewrite' => true
        ));
        $currentCategory = Mage::registry('current_category');
        $parentCategories = $currentCategory->getParentCategories();
        $i = 1;
        foreach($parentCategories as $parent){
            $itemListElement[] = [
                "@type" => "ListItem",
                "position" => $i,
                "name" => $parent->getName(),
                "item" => $parent->getUrl()
            ];
            $i++;
        }
        $this->itemListElement = json_encode($itemListElement);
    }

    public function canShow() {
        return !Mage::getStoreConfig('rich_sneppets/rich_sneppets/active');
    }

    public function getItemListElement(){
        return $this->itemListElement;
    }

}
