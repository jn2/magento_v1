<?php
class Jn2_Marketplaces_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected $market_place;

	public function setMarketPlace($market_place) {
		$this->market_place = $market_place;
	}

	public function generateXML() {

		$filename = $this->market_place->getFilename();
        $filename = str_replace('.xml','',$filename);

        $allStores = Mage::app()->getStores();
        // Gera arquivo para cada loja, adicionando um valor sequencial ao nome,
        // uma vez que ID da Loja pode saltar (se alguma loja foi excluída em algum momento)
        if (count($allStores) > 1) {
            $numLoja = 1;
            foreach ($allStores as $storeId => $val) {
                $storeId = Mage::app()->getStore($storeId)->getId();
                $storeFilename = $filename . $numLoja;
                $storeFilename .= '.xml';
                $this->writeFile($storeFilename, $storeId);
                $numLoja++;
            }
        } else {
            $filename .= '.xml';
            $storeId = Mage::app()->getStore()->getStoreId();
            $this->writeFile($filename, $storeId);
        }
	}

	protected function writeFile($filename, $storeId) {
        $io = new Varien_Io_File();
        $io->setAllowCreateFolders(true);
        $io->checkAndCreateFolder($this->getPath());
        $io->open(array('path' => $this->getPath()));

        if ($io->fileExists($filename) && !$io->isWriteable($filename)) {
            Mage::throwException(Mage::helper('marketplaces')->__('Arquivo "%s" não pode ser salvo. Por favor, tenha certeza que o diretório tem as devidas permissões.', $filename));
        }

        if($io->fileExists($filename)) {
            $io->rm($filename);
        }

        $io->streamOpen($filename);
        $io->streamWrite('<?xml version="1.0" encoding="UTF-8"?>');

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addStoreFilter($storeId)
            ->addAttributeToSelect('*');

        $io->streamWrite($this->market_place->getXMLHead($storeId));

        foreach ($collection as $product) {
            if ($this->market_place->getCondition($product)) {
                continue;
            } else {
                try {
                    $xml = $this->market_place->getXMLTemplate($product, $storeId);
                    $xml .= "\n";
                    $io->streamWrite($xml);
                } catch(Exception $e) {
                    Mage::getSingleton('core/session')->addError($e->getMessage());
                    continue;
                }
            }
        }
        $io->streamWrite($this->market_place->getXMLFooter());
        $io->streamClose();
        return true;
    }

	public function getPath() {
		return $this->market_place->getPath();
	}

}
