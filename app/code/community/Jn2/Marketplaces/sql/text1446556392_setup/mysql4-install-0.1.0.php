<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'google_shopping_category', array(
	'type'							=>'text',
	'group' 						=> 'General',
	'label'							=> 'Categorias do Google Shopping',
	'comment' 					=> 'Você pode achar a lista de categorias do Google Shopping <a href="https://support.google.com/merchants/answer/160081?hl=pt-BR">aqui</a>',
	'required' 					=> 0,
	'searchable' 				=> 0,
	'filterable' 				=> 0,
	'visible_on_front' 	=> true
));

$installer->addAttribute('catalog_product', 'buscape_category', array(
	'type'							=>'text',
	'group' 						=> 'General',
	'label'							=> 'Categorias do Buscape',
	'comment' 					=> 'Você pode achar a lista de categorias do Buscape <a href="http://www.buscape.com.br/categorias">aqui</a>',
	'required' 					=> 0,
	'searchable' 				=> 0,
	'filterable' 				=> 0,
	'visible_on_front' 	=> true));

	$installer->endSetup();
