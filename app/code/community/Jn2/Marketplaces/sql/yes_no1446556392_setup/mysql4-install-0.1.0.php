<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'show_in_xml', array(
	'type'							=>'int',
	'input'    					=> 'boolean',
	'group' 						=> 'General',
	'label'							=> 'Exibir nos comparadores de preços?',
	'required' 					=> 0,
	'searchable' 				=> 0,
	'filterable' 				=> 0,
	'visible_on_front' 	=> false
));
$installer->endSetup();
