<?php

class Jn2_Marketplaces_Model_GoogleShopping extends Jn2_Marketplaces_Model_MarketPlace {

  public function __construct() {
    $this->code = 'googleshopping';
  }

  public function getXMLTemplate($product, $storeId = null) {
    $price    = number_format($product->getFinalPrice(), 2, ",", ".");
    $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
    $image_url = '';
    
    //Busca atributos gender, age_group, ean e size caso existam
    if (Mage::getStoreConfig('marketplaces/google_shopping/gender')) {
        $gender_attribute = $product->getResource()->getAttribute(Mage::getStoreConfig('marketplaces/google_shopping/gender'));
    } else {
        $gender_attribute = $product->getResource()->getAttribute('gender');
    }
    
    if (Mage::getStoreConfig('marketplaces/google_shopping/age_group')) {
        $age_group_attribute = $product->getResource()->getAttribute(Mage::getStoreConfig('marketplaces/google_shopping/age_group'));
    } else {
        $age_group_attribute = $product->getResource()->getAttribute('age_group');
    }
    
    if (Mage::getStoreConfig('marketplaces/google_shopping/gtin')) {
        $ean_attribute = $product->getResource()->getAttribute(Mage::getStoreConfig('marketplaces/google_shopping/gtin'));
    } else {
        $ean_attribute = $product->getResource()->getAttribute('ean');
    }
    
    $size_attribute = $product->getResource()->getAttribute('size');
    $color_attribute = $product->getResource()->getAttribute('color');

    //Pega valor dos atributos gender, age_group, ean e size caso existam
    $gender = ( $gender_attribute ? $gender_attribute->getFrontend()->getValue($product) : false );
    $age_group = ( $age_group_attribute ? $age_group_attribute->getFrontend()->getValue($product) : false );
    $ean = ( $ean_attribute ? $ean_attribute->getFrontend()->getValue($product) : false );
    $size = ( $size_attribute ? $size_attribute->getFrontend()->getValue($product) : false );
    $color = ( $color_attribute ? $color_attribute->getFrontend()->getValue($product) : false );


    //Usar MetaTitle se tiver preenchido.
    if (strlen($product->getMetaTitle()) == 0 ) {
      $titulo = $product->getName();
    } else { $titulo = $product->getMetaTitle(); }

    $template = '<item>
    <g:id>
      <![CDATA[ '.$product->getId().' ]]>
    </g:id>
    <title>
    <![CDATA[ '.$titulo.' ]]>
    </title>
    <link>
    <![CDATA[
      '.Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).$product->getUrlPath().'?utm_source=Google Shopping&utm_medium=CPC&utm_campaign='.$product->getName().'
      ]]>
      </link>
      <g:price>'.$price.' BRL</g:price>
      <g:online_only>y</g:online_only>
      <description>
      <![CDATA[
        '.$product->getShortDescription().'
        ]]>
        </description>
        <g:product_type>
          <![CDATA[ '.$product->getGoogleShoppingCategory().' ]]>
        </g:product_type>
        <g:google_product_category>
          <![CDATA[ '.$product->getGoogleShoppingCategory().' ]]>
        </g:google_product_category>
        <g:image_link>
        <![CDATA['.
            str_replace(Mage::app()->getStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA),
                        Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA),
                        Mage::helper('catalog/image')->init($product, 'image')->resize(600))
        .']]>
        </g:image_link>
        <g:brand>
        <![CDATA['.$product->getAttributeText('manufacturer').']]>
        </g:brand>
        <g:condition>new</g:condition>
        <g:availability>in stock</g:availability>
        <g:quantity>'.($stock->getQty() != "0.0000" ? $stock->getQty() : 1) .'</g:quantity>
        <g:mpn>'.$product->getSku().'</g:mpn>
        <g:identifier_exists>'. ( $ean ? "true" : "false" ) .'</g:identifier_exists>
        '. ( $gender ? "<g:gender>$gender</g:gender>" : ""  ) .'
        '. ( $age_group ? "<g:age_group>$age_group</g:age_group>" : "" ).'
        '.( $size ? "<g:size>$size</g:size>" : "" ).'
        '.( $color ? "<g:color>$color</g:color>" : "" ).'
        '.( $ean ? "<g:gtin>$ean</g:gtin>" : "" ).'
        </item>';
        // $category = $product->getGoogleShoppingCategory();
        // if (empty($category)){
        //   return '';
        // }
        // $brand = $product->getAttributeText('manufacturer');
        // if (empty($brand)) {
        //   return '';
        // }
        return $template;
      }

      public function getXMLHead($storeId = null) {
        $store = Mage::app()->getStore();
        return '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
        <channel>
        <title><![CDATA['.$store->getName(). " " . Mage::getModel('core/date')->date('d.m.Y H:i:s') .']]></title>
        <link>'.Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'</link>
        <description><![CDATA['. Mage::getStoreConfig('marketplaces/google_shopping/description') .']]></description>';
      }

      public function getXMLFooter() {
        return '</channel>
        </rss>';
      }

    }
