<?php

/**
 * Class Jn2_Marketplaces_Model_System_Config_Source_AttributeList
 */
class Jn2_Marketplaces_Model_System_Config_Source_AttributeList
{
    
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $attributes = Mage::getResourceModel( 'catalog/product_attribute_collection' )->getItems();
        $options = array();
        $options[] = ['label' => 'Nenhum', 'value' => ''];
        foreach ($attributes as $attribute) {
            if (!empty($attribute->getFrontendLabel())) {
                $options[] = ['label' => $attribute->getFrontendLabel(), 'value' => $attribute->getAttributecode()];
            }
        }

        return $options;
    }
    
}