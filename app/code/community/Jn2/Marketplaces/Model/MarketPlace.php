<?php

class Jn2_Marketplaces_Model_MarketPlace {

  protected $code;
  const DIRECTORY = 'sitemaps';

  public function getCode() {
    return $this->code;
  }

  public function setCode($code) {
    $this->code = $code;
  }

  public function getPath() {
    return Mage::getBaseDir('media'). DS . self::DIRECTORY;
  }

  public function getFilename() {
    return $this->getPath() . DS . $this->getCode() . '.xml';
  }

  public function getSitemapURL() {
    return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . self::DIRECTORY . DS . $this->getCode() . ".xml";
  }

  public function getCondition($product) {
    $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
    return ($stock->getIsInStock() == false) ||
    !$product->getShowInXml() ||
    $product->getStatus() != Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
  }

  public function getXMLTemplate($product){}
  public function getXMLHead(){}
  public function getXMLFooter(){}
}
