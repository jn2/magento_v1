<?php

class Jn2_Marketplaces_Model_Observer {

  public function generateXML() {
    $marketplaces = Mage::helper('marketplaces');

    if (Mage::getStoreConfig('marketplaces/google_shopping/active')) {
      $marketplaces->setMarketPlace(Mage::getModel('marketplaces/googleShopping'));
      $marketplaces->generateXML();
    }

    if (Mage::getStoreConfig('marketplaces/buscape/active')) {
      $marketplaces->setMarketPlace(Mage::getModel('marketplaces/buscape'));
      $marketplaces->generateXML();
    }

  }

}
