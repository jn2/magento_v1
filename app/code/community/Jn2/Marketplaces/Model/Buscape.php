<?php

class Jn2_Marketplaces_Model_Buscape extends Jn2_Marketplaces_Model_MarketPlace {

  public function __construct() {
    $this->code = 'buscape';
  }

  public function getXMLTemplate($product) {
    $price    = number_format($product->getFinalPrice(), 2, ",", ".");
    $payments = Mage::getSingleton('payment/config')->getActiveMethods();

    //Usar MetaTitle se tiver preenchido.
    if (strlen($product->getMetaTitle()) == 0 ) {
      $titulo = $product->getName();
    } else { $titulo = $product->getMetaTitle(); }

    $template = '<produto>
      <descricao><![CDATA['.$titulo.']]></descricao>
      <canal_buscape>
        <canal_url><![CDATA['.str_replace("https://","http://",$product->getProductUrl()).'?utm_source=Buscape&utm_medium=CPC&utm_campaign='.$product->getName().'
        ]]></canal_url>
    <valores>';

//    foreach ($payments as $code => $payment) {
//      $template .= '<valor>
//      <forma_de_pagamento>'. Mage::getStoreConfig('payment/'.$code.'/title').'</forma_de_pagamento>
//      <parcelamento>
//        <![CDATA[1x de R$ '.$price.']]>
//      </parcelamento>
//      <canal_preco>
//        <![CDATA[ R$ '.$price.']]>
//      </canal_preco>
//      </valor>';
//    }


      $template .= '<valor>
      <forma_de_pagamento>cartao_parcelado_sem_juros</forma_de_pagamento>
      <parcelamento>
        <![CDATA[1x de R$ '.$price.']]>
      </parcelamento>
      <canal_preco>
        <![CDATA[ R$ '.$price.']]>
      </canal_preco>
      </valor>';


    $images = '<imagem tipo="O">'.$product->getImageUrl().'</imagem>';

    $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);

    $template .= '</valores>
    </canal_buscape>
    <id_oferta>
      <![CDATA['.$product->getId().']]>
    </id_oferta>
    <imagens>'.$images.'</imagens>
    <categoria>
      <![CDATA['.$product->getBuscapeCategory().']]>
    </categoria>
    <isbn>
    <![CDATA['.$product->getId().']]>
    </isbn>
    <cod_barra>
    <![CDATA['.$product->getSku().']]>
    </cod_barra>
      <disponibilidade>'.($stock->getQty() != "0.0000" ? $stock->getQty() : 1).'</disponibilidade>
    </produto>';

    return $template;
  }

  public function getXMLHead() {
    return '<buscape>
    <data_atualizacao>'.date(DateTime::ATOM).'</data_atualizacao>
    <produtos>';
  }

  public function getXMLFooter() {
    return '</produtos>
    </buscape>';
  }

}
