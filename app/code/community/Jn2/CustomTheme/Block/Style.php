<?php

class Jn2_CustomTheme_Block_Style extends Mage_Core_Block_Template {

  public function __construct() {

    $this->setData('color_1', Mage::getStoreConfig('custom_theme/colors/color_1'));
    $this->setData('color_2', Mage::getStoreConfig('custom_theme/colors/color_2'));
    $this->setData('color_3', Mage::getStoreConfig('custom_theme/colors/color_3'));
    $this->setData('color_4', Mage::getStoreConfig('custom_theme/colors/color_4'));
    $this->setData('color_5', Mage::getStoreConfig('custom_theme/colors/color_5'));

    $this->setData('color_header_1', Mage::getStoreConfig('custom_theme/colors/color_header_1'));
    $this->setData('color_header_2', Mage::getStoreConfig('custom_theme/colors/color_header_2'));
    $this->setData('color_footer_1', Mage::getStoreConfig('custom_theme/colors/color_footer_1'));
    $this->setData('color_footer_2', Mage::getStoreConfig('custom_theme/colors/color_footer_2'));
    $this->setData('color_menu_1', Mage::getStoreConfig('custom_theme/colors/color_menu_1'));
    $this->setData('color_menu_2', Mage::getStoreConfig('custom_theme/colors/color_menu_2'));

    $this->setData('background_img', Mage::getStoreConfig('custom_theme/general/background_img'));
    $this->setData('background_img_url', Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'background_img/'.Mage::getStoreConfig('custom_theme/general/background_img'));
    $this->setData('show_button', Mage::getStoreConfig('product_grid/general/show_button'));
    $this->setData('fluid', Mage::getStoreConfig('custom_theme/general/fluid'));

    $this->setData('buy_button', Mage::getStoreConfig('custom_theme/colors/buy_button'));
    $this->setData('buy_button_text', Mage::getStoreConfig('custom_theme/colors/buy_button_text'));

    $this->setData('call_to_action_1', Mage::getStoreConfig('custom_theme/colors/call_to_action_color_1'));
    $this->setData('call_to_action_2', Mage::getStoreConfig('custom_theme/colors/call_to_action_color_2'));

    $this->setData('rating_star', Mage::getStoreConfig('custom_theme/colors/rating_star'));

    $this->setData('custom', Mage::getStoreConfig('custom_theme/general/custom'));
    $this->setData('logo', Mage::getStoreConfig('custom_header/blocks/logo'));
    $this->setData('logo_url', Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'logo'.DS.Mage::getStoreConfig('custom_header/blocks/logo'));
    $this->setData('logo_footer', Mage::getStoreConfig('custom_footer/blocks/logo'));
    $this->setData('logo_footer_url', Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'logo'.DS.Mage::getStoreConfig('custom_footer/blocks/logo'));
    $this->setData('font', Mage::getStoreConfig('custom_theme/general/font'));
  }

  public function ratingStar($color, $size) {
    $svg = "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'
      x='0px' y='0px' width='{$size}px' height='{$size}px'
      viewBox='0 0 26 26'
      enable-background='new 0 0 {$size} {$size}' xml:space='preserve'>
      <polygon fill='#{$color}' points='10,0 13.09,6.583 20,7.639 15,12.764 16.18,20 10,16.583 3.82,20 5,12.764 0,7.639 6.91,6.583 '/>
      </svg>";

      return base64_encode($svg);
  }

    public function getConfig($group, $config){
        return Mage::getStoreConfig("custom_theme/{$group}/{$config}");
    }

    protected function _afterToHtml($html)
    {
      // Remove comments
      $html = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $html);
      // Remove space after colons
      $html = str_replace(': ', ':', $html);
      // Remove whitespace
      $html = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), ' ', $html);
      return $html;
     }

}
