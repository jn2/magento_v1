<?php
class Jn2_CustomTheme_Model_Observer
{
  public function setColors() {

    if (Mage::getStoreConfig('custom_theme/colors/set_colors')) {
      $ch = curl_init();
      $url = 'http://pictaculous.com/api/1.0/';
      $fields = array('image'=>file_get_contents(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA).'/logo'.DS.Mage::getStoreConfig('custom_header/blocks/logo')));

      # Set some default CURL options
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
      curl_setopt($ch, CURLOPT_URL, $url);

      $json = json_decode(curl_exec($ch), true);

      $helper = Mage::helper('customtheme');
      $colors = $helper->cf_sort_hex_colors($json['info']['colors']);

      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_1', $colors[1]);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_2', $colors[2]);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_3', $colors[3]);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_4', $colors[0]);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_5', $colors[4]);

      $best_color_0 = $helper->getBestColor($colors[4]);

      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_header_1', $colors[4]);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_header_2', $best_color_0);

      $best_color_1 = $helper->getBestColor($colors[1]);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_footer_1', $colors[1]);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_footer_2', $best_color_1);

      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_menu_1', $colors[1]);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/color_menu_2', $best_color_1);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/set_colors', false);


      $contrast = $helper->getContrast($colors[1]);
      $call_action = $helper->getBestColor($contrast);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/buy_button', $contrast);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/buy_button_text', $call_action);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/call_to_action_color_1', $contrast);
      Mage::getModel('core/config')->saveConfig('custom_theme/colors/call_to_action_color_2', $call_action);
    }
  }
}
