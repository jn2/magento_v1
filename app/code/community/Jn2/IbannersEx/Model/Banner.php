<?php

/**
 * @category    Fishpig
 * @package     Fishpig_iBanners
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */
class Jn2_iBannersEx_Model_Banner extends Fishpig_iBanners_Model_Banner
{

    public function getResizedImageUrl($width, $height)
    {
        $_helper = Mage::helper('ibanners/image');
        $banner = $this;
        $imagePath = $banner->getImage();
        $_helper->init($banner);

        $info = getimagesize($_helper->getImagePath($imagePath));
        $img_width = $info[0];
        $img_height = $info[1];


        $width = $width ? $width : $img_width;
        $height = $height ? $height : $img_height;

        $factorW = $width / $img_width;

        //calc height based on width
        $factorCalcHeight = $factorW * $img_height;
        $finalheight = min($height, $factorCalcHeight);

        $factorH = $height / $finalheight;

        $factorCalcWidth = $factorH * $img_width;
        $finalwidth = min($width, $factorCalcWidth);
        $_url = $_helper->resize($finalwidth, $finalheight);
        $_url = str_replace(DIRECTORY_SEPARATOR, '/', $_url);
        return $_url;
    }

    public function getSmallImageUrl()
    {
        if (!$this->hasSmallImageUrl()) {
            $this->setSmallImageUrl(Mage::helper('ibanners/image')->getImageUrl($this->getSmallImage()));
        }

        return $this->getData('small_image_url');
    }

    public function getMediumImageUrl()
    {
        if (!$this->hasMediumImageUrl()) {
            $this->setMediumImageUrl(Mage::helper('ibanners/image')->getImageUrl($this->getMediumImage()));
        }

        return $this->getData('medium_image_url');
    }

}
