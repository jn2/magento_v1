<?php

class Jn2_IbannersEx_Block_View extends Fishpig_iBanners_Block_View {

    protected function _prepareLayout() {

        //do not add js and css
        return $this;
    }

    protected function _beforeToHtml() {


        if (!$this->getTemplate()) {
            $this->setTemplate('jn2/ibanners/default.phtml');
        }
        parent::_beforeToHtml();
        return $this;
    }

}
