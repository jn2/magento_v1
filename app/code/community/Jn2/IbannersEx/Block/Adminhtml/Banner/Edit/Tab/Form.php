<?php

class Jn2_IbannersEx_Block_Adminhtml_Banner_Edit_Tab_Form extends Fishpig_iBanners_Block_Adminhtml_Banner_Edit_Tab_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('banner_');
        $form->setFieldNameSuffix('banner');

        $this->setForm($form);

        $fieldset = $form->addFieldset('banner_general', array(
            'legend' => $this->__('General Information'),
            'class' => 'fieldset-wide',
        ));

        $this->_addElementTypes($fieldset);

        $fieldset->addField('group_id', 'select', array(
            'name' => 'group_id',
            'label' => $this->__('Group'),
            'title' => $this->__('Group'),
            'required' => true,
            'class' => 'required-entry',
            'values' => $this->_getGroups()
        ));

        $fieldset->addField('title', 'text', array(
            'name' => 'title',
            'label' => $this->__('Title'),
            'title' => $this->__('Title'),
            'required' => true,
            'class' => 'required-entry',
        ));

        $fieldset->addField('url', 'text', array(
            'name' => 'url',
            'label' => $this->__('URL'),
            'title' => $this->__('URL')
        ));

        $fieldset->addField('url_target', 'text', array(
            'name' => 'url_target',
            'label' => $this->__('URL Target'),
            'title' => $this->__('URL Target'),
            'comment' => $this->__('If empty, current window/tab will be used'),
        ));

        $fieldset->addField('alt_text', 'text', array(
            'name' => 'alt_text',
            'label' => $this->__('ALT Text'),
            'title' => $this->__('ALT Text'),
        ));

        $fieldset->addField('html', 'editor', array(
            'name' => 'html',
            'label' => $this->__('HTML'),
            'title' => $this->__('HTML'),
            'style' => 'height: 200px;',
            'wysiwyg' => true,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(array(
                'add_widgets' => true,
                'add_variables' => true,
                'add_image' => true,
                'files_browser_window_url' => $this->getUrl('adminhtml/cms_wysiwyg_images/index'))
        )));

        $fieldset->addField('image', 'image', array(
            'name' => 'image',
            'label' => 'Imagem para Desktop',
            'title' => 'Imagem para Desktop',
            'required' => true,
            'class' => 'required-entry',
            'note' => '<small>(>= 992 px)</small>',
        ));
        
        $fieldset->addField('medium_image', 'image', array(
            'name' => 'medium_image',
            'label' => 'Imagem para dispositivos médios',
            'title' => 'Imagem para dispositivos médios',
            'note' => '<small>(>= 768 px < 992 px)</small>',
        ));
        
        $fieldset->addField('small_image', 'image', array(
            'name' => 'small_image',
            'label' => 'Imagem para dispositivos pequenos',
            'title' => 'Imagem para dispositivos pequenos',
            'note' => '<small>(< 768 px)</small>',
        ));

        $fieldset->addField('sort_order', 'text', array(
            'name' => 'sort_order',
            'label' => $this->__('Sort Order'),
            'title' => $this->__('Sort Order'),
            'class' => 'validate-digits',
        ));

        $fieldset->addField('is_enabled', 'select', array(
            'name' => 'is_enabled',
            'title' => $this->__('Enabled'),
            'label' => $this->__('Enabled'),
            'required' => true,
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
        ));

        if ($banner = Mage::registry('ibanners_banner')) {
            $form->setValues($banner->getData());
        }
        
        return $this;
    }

}
