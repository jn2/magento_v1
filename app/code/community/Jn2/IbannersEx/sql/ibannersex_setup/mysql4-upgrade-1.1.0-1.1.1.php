<?php

$installer = $this->startSetup();

$this->getConnection()->addColumn($this->getTable('ibanners_group'), 'carousel_visible_slides_medium', " int(3) unsigned default NULL");
$this->getConnection()->addColumn($this->getTable('ibanners_group'), 'carousel_visible_slides_small', " int(3) unsigned default NULL");

$installer->run($sql)->endSetup();