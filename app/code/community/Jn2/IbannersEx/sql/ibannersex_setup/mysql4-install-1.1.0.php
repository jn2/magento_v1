<?php

$installer = $this->startSetup();

$sql = <<<SQL
    ALTER TABLE {$installer->getTable('ibanners_banner')} 
        ADD COLUMN `medium_image` VARCHAR(255),
        ADD COLUMN `small_image` VARCHAR(255);
SQL;

$installer
    ->run($sql)
    ->endSetup();