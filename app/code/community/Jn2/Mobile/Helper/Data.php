<?php

class Jn2_Mobile_Helper_Data extends Mage_Core_Helper_Abstract
{

  public function setMobileJn2() {
    //generate a key
    $now = microtime (true);
    $parts = explode('.', $now);
    $key = '_'.$parts[0].'_'.substr($parts[1], 0, 3);

    $regex_ua = array (
                        'regexp' => 'iPhone|iPod|BlackBerry|Palm|Googlebot-Mobile|Mobile|mobile|mobi|Windows Mobile|Safari Mobile|Android|Opera Mini',
                        'value' => 'jn2_mobile',
                      );

    //get ua_regexp config and unserialize it
    $store = Mage::app()->getStore();
    $config = unserialize($store->getConfig('design/package/ua_regexp'));

    //iterate an array searching an exception with value "jn2_mobile" to unset
    foreach($config as $config_key => $value) {
      if($value['value'] === "jn2_mobile") {
        unset($config[$config_key]);
      }
    }

    //add a default "jn2_mobile" exception in config
    $config[$key] = $regex_ua;
    Mage::getConfig()->saveConfig('design/package/ua_regexp', serialize($config), 'default', 0);
  }

  public function getMobileBlock( $block_pos ) {

    if(Mage::getStoreConfig("jn2_mobile/general/active")) {
      $block_id   = Mage::getStoreConfig("jn2_mobile/general/bloco_{$block_pos}");

      if ( is_numeric( $block_id ) && !empty( $block_id ) ) {
        $block      = Mage::app()->getLayout()->createBlock( 'cms/block' )->setBlockId( $block_id );
      }

      return $block->toHtml();
    }

  }

  public function getMobileBlockTitle($block_pos) {

    $block_id   = Mage::getStoreConfig("jn2_mobile/general/bloco_{$block_pos}");
    $block = Mage::getModel('cms/block')->load($block_id);

    return $block->getTitle();

  }

  public function getMobileLogo() {

    if (empty($this->_data['logo_src'])) {
      $this->_data['logo_src'] = Mage::getStoreConfig('design/header/logo_src');
    }

    if (Mage::getStoreConfig('jn2_mobile/general/active')) {
      return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'logo'.DS.Mage::getStoreConfig('jn2_mobile/general/logo');
    } else {
      return $this->getSkinUrl($this->_data['logo_src']);
    }

  }

  public function getMobileBanner() {
    $banner_config = Mage::getStoreConfig("jn2_mobile/general/banner_1");
    $banner      = Mage::app()->getLayout()->createBlock('ibanners/view')->setGroupCode($banner_config);

    return $banner->toHtml();
  }

  public function getMobileBannerId() {
    return Mage::getStoreConfig("jn2_mobile/general/banner_1");
  }


  public function getMobileProductCategoryId($prod_pos) {
    return Mage::getStoreConfig("jn2_mobile/general/products_{$prod_pos}");
  }

  public function getMobileProductCategoryQty() {
    return Mage::getStoreConfig("jn2_mobile/general/qty_products");
  }

  public function getColor($color) {
    return Mage::getStoreConfig("jn2_mobile/colors/{$color}");
  }

  public function getCategory($product_pos) {
    $category_id  = $this->getMobileProductCategoryId($product_pos);
    if ($category_id) {
      return Mage::getModel('catalog/category')->load($category_id);
    } else {
      return false;
    }
  }

}
