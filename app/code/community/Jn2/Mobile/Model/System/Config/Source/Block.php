<?php
class Jn2_Mobile_Model_System_Config_Source_Block
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    $options = Mage::getResourceModel('cms/block_collection')->load()->toOptionArray();
    array_unshift($options, array('value' => '', 'label' => ''));
    return $options;
  }

}
