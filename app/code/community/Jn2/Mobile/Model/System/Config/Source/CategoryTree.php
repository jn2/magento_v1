<?php

class Jn2_Mobile_Model_System_Config_Source_CategoryTree
{
    public function toOptionArray()
    {
        $collection = Mage::getResourceModel('catalog/category_collection');

        $collection->addAttributeToSelect('name')
            ->addFieldToFilter('path', array('neq' => '1'))
            ->load();

        $options = array();

        foreach ($collection as $category) {
            $depth = count(explode('/', $category->getPath())) - 2;
            $indent = str_repeat('. ', max($depth * 2, 0));
            $options[] = array(
               'label' => $indent . $category->getName(),
               'value' => $category->getId()
            );
        }

        array_unshift($options, array('value' => '', 'label' => ''));
        return $options;
    }
}
