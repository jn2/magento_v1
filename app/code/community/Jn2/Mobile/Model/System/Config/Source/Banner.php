<?php
class Jn2_Mobile_Model_System_Config_Source_Banner
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {

    $banner_collection = Mage::getResourceModel('ibanners/group_collection');
    $banner_options = array();

    foreach($banner_collection as $group) {
      $banner_options[] = array('value' => $group->getCode(), 'label' => $group->getTitle());
    }

    array_unshift($banner_options, array('value' => '', 'label' => ''));
    return $banner_options;
  }

}
