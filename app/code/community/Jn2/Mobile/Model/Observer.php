<?php
class Jn2_Mobile_Model_Observer{

	public function forceMobile(){
		

		if( $this->isMobileForce() ) {						
			 $_COOKIE['mobile-force'] = '1';
			 setcookie('mobile-force',1);
			 Mage::app()->getStore()->setConfig('design/package/name','jn2',true);
			 Mage::app()->getStore()->setConfig('design/theme/default','mobile',true);			

		}else{			
			 setcookie('mobile-force',0, 1);
			  $_COOKIE['mobile-force']  = 0;
		}

	}	

	public function isMobileForce(){		
		if((int)Mage::getStoreConfig("jn2_mobile/general/active_force")){			
			return $_GET['mobile-force'] === '1' || ($_COOKIE['mobile-force'] === '1' && !isset($_GET['mobile-force']));		
		}
		return false;
	}
}