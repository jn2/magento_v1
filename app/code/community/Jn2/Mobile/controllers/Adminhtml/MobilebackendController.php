<?php
class Jn2_Mobile_Adminhtml_MobilebackendController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction(){

		Mage::helper("mobile")->setMobileJn2();

		$this->_redirect('adminhtml/system_config/edit/section/jn2_mobile/');

	}

}
