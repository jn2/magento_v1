<?php

class Jn2_AuthLogin_Block_Index extends Mage_Core_Block_Template
{

    public function getBackground()
    {
        if (Mage::getStoreConfig('authlogin/general/background')) {
            return 'url(' . Mage::getBaseUrl('media') . 'authlogin/' . Mage::getStoreConfig('authlogin/general/background') . ')';
        } else {
            return '#CCC';
        }
    }

    public function getLogo()
    {
        return Mage::getBaseUrl('media') . '/logo/' . Mage::getStoreConfig('custom_theme/general/logo');
    }
    
    public function getRegistrationMessage()
    {
        if (Mage::getStoreConfig('authlogin/general/use_register_message') 
            && $message = Mage::getSingleton('core/session')->getCustomRegisterMessage()
        ) {
            Mage::getSingleton('core/session')->setCustomRegisterMessage(null);
            return $message;
        }
        
        return null;
    }
}
