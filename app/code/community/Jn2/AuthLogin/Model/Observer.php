<?php

class Jn2_AuthLogin_Model_Observer
{

    var $allowed_routes;

    public function __construct()
    {
        $this->allowed_routes = array(
            Mage::getUrl('authlogin'),
            Mage::getUrl('customer/account'),
            'api',
        );

        if (Mage::getStoreConfig('authlogin/general/show_register_link')) {
            $this->allowed_routes[] = Mage::helper('customer')->getRegisterUrl();
            $this->allowed_routes[] = '/onestepcheckout/ajax';
        }
    }

    public function requireUserLogin()
    {
        if (Mage::getStoreConfig('authlogin/general/active') 
            && !Mage::getSingleton('customer/session')->isLoggedIn() 
            && !$this->canAccessRoute()
        ) {
            Mage::app()->getResponse()->setRedirect(Mage::getUrl('authlogin'));
        }
    }
    
    public function addRegisterMessage()
    {
        if (Mage::getStoreConfig('authlogin/general/use_register_message')) {
            Mage::getSingleton('core/session')->setCustomRegisterMessage(Mage::getStoreConfig('authlogin/general/register_message'));
        }
    }

    protected function canAccessRoute()
    {
        $current_url = Mage::helper('core/url')->getCurrentUrl();

        foreach ($this->allowed_routes as $route) {
            if (strpos($current_url, $route) > -1) {
                return true;
            }
        }

        return false;
    }
}
