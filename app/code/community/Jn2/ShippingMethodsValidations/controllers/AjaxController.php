<?php
/**
 * Title
 *
 * @category
 * @package
 * @author      Gustavo Bacelar <gustavo@jn2.com.br>
 * @description
 */
class Jn2_ShippingMethodsValidations_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function validateAction()
    {

        $helper = Mage::helper('shippingmethodsvalidations');
        $cep = $this->getRequest()->getParam('cep');

        $result = $helper->checkZipCode($cep);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
    }
}