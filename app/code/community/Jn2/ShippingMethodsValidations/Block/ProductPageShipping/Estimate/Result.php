<?php
class Jn2_ShippingMethodsValidations_Block_ProductPageShipping_Estimate_Result extends EcomDev_ProductPageShipping_Block_Estimate_Result
{

	/**
	* Retrieve shipping price for current address and rate
	*
	* @param decimal $price
	* @param boolean $flag show include tax price flag
	* @return string
	*/
	public function getShippingPrice($price, $flag)
	{
		$helper = Mage::helper('shippingmethodsvalidations');
		if ($helper->checkZipCode($this->getEstimate()->getQuote()->getShippingAddress()->getPostcode())) {
			return $this->formatPrice($this->helper('tax')->getShippingPrice($price, $flag, $this->getEstimate()->getQuote()->getShippingAddress()));
		} else {
			return $this->__('Sorry, no quotes are available for this order at this time.');
		}
	}

}
