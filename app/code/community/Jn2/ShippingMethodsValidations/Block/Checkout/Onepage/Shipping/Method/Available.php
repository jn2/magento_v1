<?php
class Jn2_ShippingMethodsValidations_Block_Checkout_Onepage_Shipping_Method_Available extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{

	public function getShippingRates() {

		$helper = Mage::helper('shippingmethodsvalidations');
		if ($helper->checkZipCode($this->getQuote()->getShippingAddress()->getPostcode())) {
			if (empty($this->_rates)) {
				$this->getAddress()->collectShippingRates()->save();

				$groups = $this->getAddress()->getGroupedAllShippingRates();

				return $this->_rates = $groups;
			}

			return $this->_rates;
		}
	}

}
