<?php
class Jn2_ShippingMethodsValidations_Block_Onestepcheckout_Onestep_Form_Shippingmethod extends Inovarti_Onestepcheckout_Block_Onestep_Form_Shippingmethod
{

	public function getShippingRates()
	{
		$helper = Mage::helper('shippingmethodsvalidations');
		if ($helper->checkZipCode($this->getQuote()->getShippingAddress()->getPostcode())) {
			if (empty($this->_rates)) {
				$this->getAddress()->collectShippingRates()->save();
				$groups = $this->getAddress()->getGroupedAllShippingRates();
				return $this->_rates = $groups;
			}
			return $this->_rates;
		}

	}

}
