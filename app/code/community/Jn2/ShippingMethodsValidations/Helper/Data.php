<?php
class Jn2_ShippingMethodsValidations_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function checkZipCode($zipCode) {
		$arrayRanges = $this->rangesToArray();

		if ($arrayRanges) {
			$zipCode = str_replace('-', '', $zipCode);

			foreach ($arrayRanges as $range) {
				$ranges = str_replace('-', '', $range);
				$ranges = explode("|",$ranges);
				if ($zipCode >= $ranges[0] && $zipCode <= $ranges[1]) {
					return true;
				}
			}
			return false;

		}else {
			return true;
		}
	}

	public function rangesToArray() {
		$ranges = Mage::getStoreConfig("shipping/general/region");
		if (empty($ranges)) {
			return false;
		} else {
			$ranges = str_replace(' ', '', $ranges);
			$ranges = explode("\n",$ranges);
			return $ranges;
		}
	}

}
