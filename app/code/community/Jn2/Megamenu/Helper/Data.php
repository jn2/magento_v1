<?php
class Jn2_Megamenu_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function getTemplatePath() {
		$theme = Mage::getStoreConfig("megamenu/template/theme");
		return "jn2/megamenu/page/html/level1/{$theme}.phtml";
	}

	public function getPathCss(){
		$css = Mage::getStoreConfig("megamenu/template/theme");
		return "jn2/megamenu/css/jn2-mega-menu". substr($css,5) . ".css";
	}
}
