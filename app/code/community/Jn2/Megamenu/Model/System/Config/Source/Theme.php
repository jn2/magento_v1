<?php
class Jn2_Megamenu_Model_System_Config_Source_Theme
{

  /**
  * Options getter
  *
  * @return array
  */
  public function toOptionArray()
  {
    return array(
        array('label' => 'Tema 1', 'value' => 'theme1', 'title' => Mage::getDesign()->getSkinUrl('jn2/megamenu/wireframes/w1.jpg')),
        array('label' => 'Tema 2', 'value' => 'theme2', 'title' => Mage::getDesign()->getSkinUrl('jn2/megamenu/wireframes/w2.jpg')),
        array('label' => 'Tema 3', 'value' => 'theme3', 'title' => Mage::getDesign()->getSkinUrl('jn2/megamenu/wireframes/w2.jpg')),
        array('label' => 'Tema 4', 'value' => 'theme4', 'title' => Mage::getDesign()->getSkinUrl('jn2/megamenu/wireframes/w2.jpg')),
        array('label' => 'Tema 5', 'value' => 'theme5', 'title' => Mage::getDesign()->getSkinUrl('jn2/megamenu/wireframes/w2.jpg')),
        array('label' => 'Tema 6', 'value' => 'theme6', 'title' => Mage::getDesign()->getSkinUrl('jn2/megamenu/wireframes/w2.jpg'))
    );
  }

}
