<?php
class Jn2_Megamenu_Model_Observer
{

  /**
  * Options getter
  *
  * @return array
  */
  public function loadThemeAssets($params)
  {
    $theme    = Mage::getStoreConfig('megamenu/template/theme');
    $request  = Mage::app()->getRequest();
    $layout   = $params['layout'];
    $update = $layout->getUpdate();
    $handles = $update->getHandles();
    $pos = array_search('default',$handles);
    if($pos !== false){
      array_splice($handles, $pos+1, 0, 'megamenu_'.$theme);
      $update->resetHandles();

      foreach ($handles as $handle) {
          $update->addHandle($handle);
      }
    }
  }

}
