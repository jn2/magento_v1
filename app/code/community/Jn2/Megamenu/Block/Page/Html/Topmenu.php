<?php
class Jn2_Megamenu_Block_Page_Html_Topmenu extends Mage_Page_Block_Html_Topmenu {
    
  public function subCategory($category, $current_level=1) {


    $theme = Mage::getStoreConfig("megamenu/template/theme");
    $next_level = $current_level+1;
    $this->setData('parent_category', Mage::getModel('catalog/category')->load($category->getId()));
    $this->setData('items_per_line', Mage::getStoreConfig("megamenu/template/items_per_line"));
    $this->setData('sub_categories', $category->getChildrenCategories());

    $this->setTemplate("jn2/megamenu/page/html/level{$next_level}/{$theme}.phtml");
    return $this->renderView();
  }

}
