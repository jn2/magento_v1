<?php
$installer = $this;
$installer->startSetup();

//Categories typically only have one attribute set, this will retrieve its ID
$setId = Mage::getSingleton('eav/config')->getEntityType('catalog_category')->getDefaultAttributeSetId();

//Add group to entity & set
$installer->addAttributeGroup('catalog_category',$setId, 'Megamenu');

$installer->addAttribute("catalog_category", "category_icon_class",  array(
    "group"    => "Megamenu",
    "type"     => "varchar",
    "frontend" => "",
    "label"    => "Classe do ícone",
    "class"    => "",
    "source"   => "",
    "global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible"  => true,
    "required" => false,
    "user_defined"  => false,
    "default" => "",
    "searchable" => false,
    "filterable" => false,
    "comparable" => false,

    "used_in_product_listing" => true,
    "visible_on_front"  => false,
    "unique"     => false,
    "note"       => ""

	));
  $installer->endSetup();
