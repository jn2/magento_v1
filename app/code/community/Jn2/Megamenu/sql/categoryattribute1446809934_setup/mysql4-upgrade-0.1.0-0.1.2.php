<?php
$installer = $this;
$installer->startSetup();

//Add group to entity & set
$installer->addAttribute('catalog_category', 'image_cat_url', array(
    'group' => 'Megamenu',
    'input' => 'text',
    'type' => 'text',
    'label' => 'Url da imagem',
    'backend' => '',
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "used_in_product_listing" => true,
    "visible_on_front"  => true
));

$installer->endSetup();
