<?php
class Jn2_CustomButton_Helper_Data extends Mage_Core_Helper_Abstract
{
  public function getTheme() {
		$theme = Mage::getStoreConfig('custom_button/template/theme');
		return "jn2/custombutton/wireframes/{$theme}.phtml";
	}
}
