<?php
class Jn2_CustomButton_Model_System_Config_Source_Themes
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(

            array('label' => 'Botão 1', 'value' => 'theme1', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w1.jpg')),
            array('label' => 'Botão 2', 'value' => 'theme2', 'title' => Mage::getDesign()->getSkinUrl('jn2/header/wireframes/w2.jpg'))
        );
    }

}
