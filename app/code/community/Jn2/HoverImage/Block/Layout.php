<?php

/**
 * makes effect of hover in image
 *
 * @author raphael
 */
class Jn2_HoverImage_Block_Layout extends Mage_Core_Block_Template
{

    private $product;

    public function _prepareLayout()
    {
        $this->setTemplate("jn2/hoverimage/wireframes/template.phtml");
    }

    function setProduct($product)
    {
        $this->product = $product;
    }

    public function setTheme()
    {
        $this->setData('format', $this->getData('this'));
        $this->setData('product', $this->getData('product'));

        $imageSize = Mage::getStoreConfig('product_grid/general/product_image_dimension');
        $this->setData('resize', empty($imageSize) ? $this->getData('resize') : $imageSize);

        $existImageHover = $this->getData('product')['img_hover_product_list'] && !in_array($this->getData('product')['img_hover_product_list'], array('no_selection', $this->getData('product')['small_image']));
        
        if (Mage::getStoreConfig('custom_theme/general/hover_image') && $existImageHover) {
            $this->setTemplate('jn2/hoverimage/wireframes/theme1.phtml');
        } else {
            $this->setTemplate('jn2/hoverimage/wireframes/theme2.phtml');
        }

        return $this->renderView();
    }

    public function getResize() {
        $resize = Mage::getStoreConfig('product_grid/general/product_image_dimension');
        if (strpos($resize, 'x') !== false) {
            list($width, $height) = explode('x', $resize);
        } elseif (strpos($this->resize, ',') !== false) {
            list($width, $height) = explode(',', $resize);
        } else {
            $width = $height = '500';
        }
        
        return array($width, $height);
    }
    
    public function getMobileResize() {
        $resize = Mage::getStoreConfig('product_grid/general/product_image_mobile_dimension');
        if (strpos($resize, 'x') !== false) {
            list($width, $height) = explode('x', $resize);
        } elseif (strpos($this->resize, ',') !== false) {
            list($width, $height) = explode(',', $resize);
        } else {
            $width = $height = '200';
        }
        
        return array($width, $height);
    }
}
