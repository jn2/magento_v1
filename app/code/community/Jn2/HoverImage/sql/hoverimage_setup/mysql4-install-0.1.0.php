<?php

$installer = $this;
$installer->startSetup();

if (!$installer->getAttribute('catalog_product', 'img_hover_product_list', 'attribute_id')) {
    $installer->addAttribute('catalog_product', 'img_hover_product_list', array(
        'type' => 'varchar',
        'input' => 'media_image',
        'label' => 'Imagem Secundária',
        'required' => 0,
        'user_defined' => 1,
        'used_in_product_listing' => 1,
        'group' => 'General',
    ));
}

$installer->endSetup();
