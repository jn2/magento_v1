<?php

class Jn2_Facebook_Block_Page extends Mage_Core_Block_Template {

  public function __construct() {
    $this->setData('hide_cover_photo', Mage::getStoreConfig('facebook/page_plugin/hide_cover_photo') ? 'true' : 'false' );
    $this->setData('show_friends_face', Mage::getStoreConfig('facebook/page_plugin/show_friends_face') ? 'true' : 'false' );
    $this->setData('show_page_posts', Mage::getStoreConfig('facebook/page_plugin/show_page_posts') ? 'true' : 'false' );
    $this->setData('adapt_to_container_width', Mage::getStoreConfig('facebook/page_plugin/adapt_to_container_width') ? 'true' : 'false' );
    $this->setData('use_small_header', Mage::getStoreConfig('facebook/page_plugin/use_small_header') ? 'true' : 'false' );
    $this->setData('width', Mage::getStoreConfig('facebook/page_plugin/width'));
    $this->setData('height', Mage::getStoreConfig('facebook/page_plugin/height'));
    $this->setData('page_url', Mage::getStoreConfig('facebook/page_plugin/page_url'));
    $this->setData('show_page_posts', Mage::getStoreConfig('facebook/page_plugin/show_page_posts'));
  }

}
