<?php

class Jn2_ApiEx_Model_Observer {

    /**
     * Add state to status history
     * this event is called on Sales.Order -> info() Api v1
     * @param type $evt
     */
    public function statusHistoryCollectionAppendState($evt) {
        $orderStoreStatusCollection = $evt->getOrderStatusHistoryCollection();
        $statusCollection = Mage::getResourceModel('sales/order_status_collection')
                ->joinStates();
        $aryStatuState  = array();
        foreach ($statusCollection as $status) {
            $aryStatuState[$status->getStatus()] = $status->getState();
        }
        foreach($orderStoreStatusCollection as $orderStoreStatus){
            $state = $aryStatuState[$orderStoreStatus->getStatus()];
            $orderStoreStatus->setState($state);
        }
        
    }

}
