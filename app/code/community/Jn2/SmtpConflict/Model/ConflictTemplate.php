<?php

if (Mage::getStoreConfig('mandrill/general/active')) {
  class ResolveConflictTemplate extends Ebizmarts_Mandrill_Model_Email_Template {}
} else {
  class ResolveConflictTemplate extends Aschroder_SMTPPro_Model_Email_Template {}
}

class Jn2_SmtpConflict_Model_ConflictTemplate extends ResolveConflictTemplate {

}
