<?php

if (Mage::getStoreConfig('mandrill/general/active')) {
  class ResolveConflictQueue extends Ebizmarts_Mandrill_Model_Email_Queue {}
} else {
  class ResolveConflictQueue extends Aschroder_SMTPPro_Model_Email_Queue {}
}

class Jn2_SmtpConflict_Model_ConflictQueue extends ResolveConflictQueue {

}
