<?php
class Jn2_Base_Helper_Product extends Mage_Core_Helper_Abstract{
    
    public function getAttributeText($attributeCode,Mage_Catalog_Model_Product $product=null){
        if(!$product){
            $product = Mage::registry('current_product');
        }
        if($product){
            $array = $product->getAttributes();
            $attribute = $array[$attributeCode];
            $return = $value = $attribute->getFrontend()->getValue($product);                        
            if($attribute->usesSource()){
                $return = $attribute->getSourceModel()->getOptionText($value);                
            }
            
            return $return;            
        }
        return null;
        
    }
}