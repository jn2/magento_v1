<?php
class Jn2_Base_Helper_Data extends Mage_Core_Helper_Abstract {
    public function getVariableData($code, $type = null) {                
        return Mage::getModel('core/variable')->loadByCode($code)->getValue($type);        
    }
}