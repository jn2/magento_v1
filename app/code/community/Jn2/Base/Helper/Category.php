<?php

class Jn2_Base_Helper_Category extends Mage_Core_Helper_Abstract {
    /**
     * const int HOME_CATEGORY_ID_VAR
     */
    const HOME_CATEGORY_ID_VAR = 'base/default/home_category_id';
    
    /**
     *  const int HOME_SIDEBAR_ID_VAR
     */
    const HOME_SIDEBAR_CATEGORY_ID_VAR = 'base/default/home_sidebar_category_id';
    /**
     * 
     *  const int MEDIDA_CERTA_CATEGORY_ID_VAR
     * 
     */
    const HOME_GRID_LIMIT_VAR = 'base/default/home_grid_limit';

    /**
     *
     * @var Mage_Model_Catalog_Category
     */
    protected $_home_category;

    /**
     *
     * @var int
     */
    protected $_home_category_id;
    
    
    
    /**
     * 
     */
     /**
     *
     * @var Mage_Model_Catalog_Category
     */
    protected $_home_sidebar_category;

    /**
     *
     * @var int
     */
    protected $_home_sidebar_category_id;
    
    /**
     *
     * @var int 
     */
    protected $_home_grid_limit;
    
    

    /**
     * Single getHome Category
     * @return type 
     */
    public function getHomeCategory() {
        if (null === $this->_home_category) {
            $category = Mage::getModel('catalog/category')->load($this->getHomeCategoryId());
            $this->_home_category = $category;
        }
        return $this->_home_category;
    }

    /**
     * 
     * @return Mage_Model_Catalog_Category 
     */
    public function getHomeCategoryId() {
        if (null === $this->_home_category_id) {
            $categoryId = Mage::getStoreConfig(self::HOME_CATEGORY_ID_VAR);
            $this->_home_category_id = (string) $categoryId;
        }
        return $this->_home_category_id;
    }
    
    public function getHomeGridLimit(){
        if (null === $this->_home_grid_limit) {
            $this->_home_grid_limit = Mage::getStoreConfig(self::HOME_GRID_LIMIT_VAR);            
        }
        return $this->_home_grid_limit;
    }
    
    /**
     * 
     * Get sidebar
     * @return type 
     */
    public function getHomeSidebarCategoryId(){
       if (null === $this->_home_sidebar_category_id) {
            $categoryId = Mage::getStoreConfig(self::HOME_SIDEBAR_CATEGORY_ID_VAR);
            $this->_home_sidebar_category_id = (string) $categoryId;
        }
        return $this->_home_sidebar_category_id; 
    }
    
    /**
     * Single getHome sidebar Category
     * @return type 
     */
    public function getHomeSidebarCategory() {
        if (null === $this->_home_sidebar_category) {
            $category = Mage::getModel('catalog/category')->load($this->getHomeCategoryId());
            $this->_home_sidebar_category = $category;
        }
        return $this->_home_sidebar_category;
    }

}