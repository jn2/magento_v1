<?php

###############################################################
# Cushy boleto 
//
# Itau setup set 7 casas nosso numero
###############################################################
$installer = $this;


$installer->startSetup();
$arrayEntityModels = array(
    'sales/quote',
    'sales/order',
);
foreach ($arrayEntityModels as $entityModel) {
    $entityType = Mage::getModel('eav/entity_type')->load($entityModel, 'entity_model');
    $entityType->setData('increment_pad_length', 7);
    $entityType->save();
}

/* * *****************************************************************
 * Setup config Deivison arthur remove configs  inconvenientes 
 * **************************************************************** */

$configs = array(
    'onepagecheckout/address_form/cpfcnpjtaxvat',
    'onepagecheckout/address_form/cpfcnpj',
    'onepagecheckout/address_form/tipopessoa',
    'onepagecheckout/outrasopcoes/taxvatregistro',
    'onepagecheckout/outrasopcoes/cpfcnpjregistro',
    'onepagecheckout/outrasopcoes/ieregistro',
    'onepagecheckout/outrasopcoes/companyregistro',
    'onepagecheckout/outrasopcoes/rgiregistro',
    'onepagecheckout/outrasopcoes/rgientrega',
    'onepagecheckout/outrasopcoes/cpfentrega',
    'onepagecheckout/outrasopcoes/tipopessoaregistro',
        # 
);

foreach ($configs as $path) {
    $config = Mage::getModel('core/config_data')->load($path, 'path');
    $config->delete();
}

$configs = array(
    'onepagecheckout/general/taxvat' => '1',
    'customer/address/taxvat_show' => 'req'
);
foreach ($configs as $path => $value) {
    $config = Mage::getModel('core/config_data')->load($path, 'path');
    Mage::getModel('core/config')->saveConfig($path, $value);
}


$installer->endSetup();
