<?php

###############################################################
# Customer address edit form/ remove required firstname,lastname and telephone
###############################################################
$installer = $this;
$installer->startSetup();

$address = Mage::getModel('customer/address');
$addressForm = Mage::getModel('customer/form');
$addressForm->setFormCode('customer_address_edit')
        ->setEntity($address);


$attributes = $addressForm->getAttributes();

$list = array_intersect_key($attributes, array_flip(array('prefix','firstname','middlename','lastname','suffix','company','telephone','nomefantasia','cpfcnpj','vat_id')));
foreach($list as $attribute){
    
    $usedInForms = $attribute->getUsedInForms();
    $usedInForms = array_flip($usedInForms);
    unset($usedInForms['customer_address_edit']);
    #unset($usedInForms['customer_register_address']);
    $validateRules = $attribute->getValidateRules();     
    
    
    $usedInForms = array_flip($usedInForms);
    $attribute->setUsedInForms($usedInForms);
    $attribute->save();
    
    
    #$dataModel = new Mage_Eav_Model_Attribute_Data_Text;
    #$dataModel = Mage_Eav_Model_Attribute_Data::factory($attribute, $address);
    #echo $attribute->getIsRequired();die;
    #echo get_class($dataModel);die;
}

        

$installer->endSetup();
