<?php

/**
 * Class Jn2_Base_Block_Page_Html_Head
 */
class Jn2_Base_Block_Page_Html_Head extends Mage_Page_Block_Html_Head
{
    /**
     * adiciona atributo em scripts para manter no topo
     *
     * @return string
     */
    public function getIncludes()
    {
        if (empty($this->_data['includes'])) {
            $includes = Mage::getStoreConfig('design/head/includes');

            $includes = str_replace('<script', '<script data-footer-js-skip="true"', $includes);
            $this->_data['includes'] = $includes;
        }
        return $this->_data['includes'];
    }


    public function appendItem ($type, $name, $params = null, $if = null, $cond = null)
    {
        if ($type === 'skin_css' && empty ($params)) $params = 'media="all"';

        $_item ["$type/$name"] = array
        (
            'type'   => $type,
            'name'   => $name,
            'params' => $params,
            'if'     => $if,
            'cond'   => $cond,
        );

        $this->_data ['items'] = array_merge ($this->_data ['items'], $_item);

        return $this;
    }
}