<?php

/**
 * Description of Price
 *
 * @author reinaldo
 */
class Jn2_Base_Block_Catalog_Product_Price extends Mage_Catalog_Block_Product_Price {

    //put your code here
    public function getParcelasHtml() {
      $show_parcels = Mage::getStoreConfig('price/parcelas/active');
      $cielo_active = Mage::getStoreConfig('payment/Query_Cielo_Cc/active');

      if ($show_parcels && $cielo_active) {

        $text_before  = Mage::getStoreConfig('price/parcelas/text_before');
        $text_after   = Mage::getStoreConfig('price/parcelas/text_after');

        $this->setData('text_before', $text_before);
        $this->setData('text_after', $text_after);

        $this->setTemplate('jn2/catalog/product/parcelas.phtml');

        return $this->renderView();;
      } else {
        return '';
      }

    }

    public function getBilletHtml() {
      //Pegando configuraçes
      $rule_id  = Mage::getStoreConfig('price/show_billet/rule_id');
      $active   = Mage::getStoreConfig('price/show_billet/active');

      //Se a configuração estiver ativa
      if ($active && !empty($rule_id)) {

        //Carrega a regra
        $rule     = Mage::getModel('salesrule/rule')->load($rule_id);
        $product  = $this->getProduct();

        //Aceitar apenas promoções de porcentagem e não exibir em produtos agrupados
        if ($rule->getSimpleAction() != 'by_percent' || $product->getTypeId() == 'grouped') {
          return '';
        }

        //Obtendo valor final do produto
        $price    = $product->getFinalPrice();
        $discount = $rule->getDiscountAmount();

        //Calculando valor do desconto
        $end_price = number_format($price - (($price * $discount) / 100), 2, ",", ".");

        //Passando valores pro template
        $this->setData('description', $rule->getDescription());
        $this->setData('end_price', $end_price);

        //Definindo template
        $this->setTemplate('jn2/catalog/product/billet.phtml');

        //Retornando HTML renderizado
        return $this->renderView();
      } else {
        return '';
      }

    }
}
