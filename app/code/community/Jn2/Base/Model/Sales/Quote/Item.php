<?php

class Jn2_Base_Model_Sales_Quote_Item extends Mage_Sales_Model_Quote_Item
{

    /**
     * Corrige quantidade de produtos quando não tem estoque suficiente e não é permitido venda além do estoque
     *
     * @param float $qty
     * @return Mage_Sales_Model_Quote_Item
     */
    public function addQty($qty)
    {
        $product = $this->getProduct();

        $oldQty = $this->getQty();
        $stockItem = $product->getStockItem();
        $totalQtd = floatval($stockItem->getQty());
        $backorders = $stockItem->getBackorders();

        $qty = $this->_prepareQty($qty);

        // se não permitir venda sem estoque, altera quantidade para o máximo de estoque disponível
        if(!$product->isConfigurable() && !$product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {

            if ($totalQtd < $oldQty + $qty && empty($backorders)) {

                $newQty = $totalQtd - $oldQty;
                $qty = $newQty > 0 ? $newQty : 0;
                Mage::getSingleton('core/session')->setStockNotification('Quantidade de itens reajustada para o limite disponível.');

            }
        }


        /**
         * We can't modify quontity of existing items which have parent
         * This qty declared just once duering add process and is not editable
         */
        if (!$this->getParentItem() || !$this->getId()) {
            $this->setQtyToAdd($qty);
            $this->setQty($oldQty + $qty);
        }
        return $this;
    }

    /**
     * Corrige quantidade de produtos quando não tem estoque suficiente e não é permitido venda além do estoque
     *
     * @param float $qty
     * @return Mage_Sales_Model_Quote_Item
     */
    public function setQty($qty)
    {
        $product = $this->getProduct();
        $oldQty = $this->_getData('qty');


        $stockItem = $product->getStockItem();
        $totalQtd = floatval($stockItem->getQty());
        $backorders = $stockItem->getBackorders();

        // se não permitir venda sem estoque, altera quantidade para o máximo de estoque disponível
        if(!$product->isConfigurable() && !$product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {

            if($totalQtd < $qty && empty($backorders)) {

                $newQty = $totalQtd;
                $qty = $newQty > 0 ? $newQty : $totalQtd;
                Mage::getSingleton('core/session')->setStockNotification('Quantidade de itens reajustada para o limite disponível.');
            }
        }


        $qty = $this->_prepareQty($qty);
        $this->setData('qty', $qty);
        Mage::dispatchEvent('sales_quote_item_qty_set_after', array('item' => $this));

        if ($this->getQuote() && $this->getQuote()->getIgnoreOldQty()) {
            return $this;
        }
        if ($this->getUseOldQty()) {
            $this->setData('qty', $oldQty);
        }

        return $this;
    }
}
