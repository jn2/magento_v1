<?php

/**
 * Catalog product model
 *
 * @method Mage_Catalog_Model_Resource_Product getResource()
 * @method Mage_Catalog_Model_Resource_Product _getResource()
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Jn2_Base_Model_Catalog_Product extends Mage_Catalog_Model_Product {

    /**
     * Criar um array com os valores de parcelamento
     *
     * @return array
     */
    private function _getParcelamento() {
        $max_parcelas         = intval(Mage::getStoreConfig("payment/Query_Cielo_Cc/max_parcels_number"));

        $valor_minimo_parcela = floatval(Mage::getStoreConfig('payment/Query_Cielo_Cc/min_parcels_value'));
        $valor_minimo_parcela = max($valor_minimo_parcela, 1);

        $max_parcelas         = max(1, $max_parcelas);
        $preco                = max($this->getFinalPrice(), 1);

        $numero_parcelas      = max(1, intval(($preco / $valor_minimo_parcela)));


        if ($numero_parcelas > $max_parcelas) {
            $numero_parcelas = $max_parcelas;
        }


        $valor_parcela = $preco / $numero_parcelas;


        return array("numero_parcelas" => $numero_parcelas, "valor_parcela" => $valor_parcela);
    }

    public function getNumeroParcelas() {
        $parcelamento = $this->_getParcelamento();
        return $parcelamento["numero_parcelas"];
    }

    public function getValorParcela() {
        $parcelamento = $this->_getParcelamento();
        return "R$" . number_format($parcelamento["valor_parcela"], 2, ",", ".");
    }

    public function getValorBoleto() {
        $desconto = floatval(Mage::getStoreConfig('ipgintegracaodiretapagamento/itaushopline/desconto_avista'));
        $preco = $this->getFinalPrice();

        $preco_final = $preco - (($preco * $desconto) / 100);

        return "R$" . number_format($preco_final, 2, ",", ".");
    }

}
