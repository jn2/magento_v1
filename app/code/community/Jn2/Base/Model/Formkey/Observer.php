<?php
/**
 * Add formkey on all forms
 * @category   Jn2
 * @package    Jn2_Base
 * @author     Reinaldo Mendes
 * 
 */
class Jn2_Base_Model_Formkey_Observer {

    public function beforeSendResponse($evt) {
        $block = Mage::getSingleton('core/layout')->getBlock('formkey');
        if ($block) {
            $response = $evt->getResponse();
            $body = $response->getBody();
            $formkey = $block->toHtml();
            $body = strtr($body, array("</form>" => "{$formkey}</form>"));
            $formkey = preg_replace('@\r?\n@', '\\r\\n', $formkey);
            $formkey = preg_replace('@["]@', '\\"', $formkey);
            $body = strtr($body, array("<\\/form>" => "{$formkey}<\\/form>"));
            $response->setBody($body);
        }
    }

}
