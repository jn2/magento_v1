<?php

class Jn2_Base_Model_Customer_Flowpassword extends Mage_Customer_Model_Flowpassword
{
    /**
     * Check forgot password requests to times per hour from 1 IP
     *
     * @return bool
     */
    public function checkCustomerForgotPasswordFlowIp()
    {
        $helper        = Mage::helper('customer');
        $remoteAddr    = $this->getClientIP();

        $checkForgotPasswordFlowTypes = array(
            Mage_Adminhtml_Model_System_Config_Source_Customer_Forgotpassword::FORGOTPASS_FLOW_IP_EMAIL,
            Mage_Adminhtml_Model_System_Config_Source_Customer_Forgotpassword::FORGOTPASS_FLOW_IP
        );

        if (in_array($helper->getCustomerForgotPasswordFlowSecure(), $checkForgotPasswordFlowTypes) && $remoteAddr) {
            $forgotPassword = $this->getCollection()
                ->addFieldToFilter('ip', array('eq' => $remoteAddr))
                ->addFieldToFilter('requested_date',
                    array('gt' => Mage::getModel('core/date')->date(null, '-1 hour')));

            if ($forgotPassword->getSize() > $helper->getCustomerForgotPasswordIpTimes()) {
                return false;
            }
        }
        return true;
    }

    // Trata para retornar o IP do cliente, não do acesso do servidor
    protected function getClientIP()
    {
        if(!empty($_SERVER['HTTP_X_REAL_IP'])) {
            return $_SERVER['HTTP_X_REAL_IP'];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    // Trata para retornar o IP do cliente, não do acesso do servidor
    protected function _prepareData()
    {
        $this->setIp($this->getClientIP())
            ->setRequestedDate(Mage::getModel('core/date')->date());
        return $this;
    }
}