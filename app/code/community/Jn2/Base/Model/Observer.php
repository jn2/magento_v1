<?php

class Jn2_Base_Model_Observer {

    /**
     * Bug quote continua ativo após finalizar a compra
     * @param type $evt 
     */
    public function forceInactivateQuoteAfterSuccess($evt) {
        $order = $evt->getOrder();
        $quote = $evt->getQuote();
        $quote->setIsActive(false);
        $quote->save();
    }

    public function addNewHandleBeforeDefault($evt) {
        $updates = $evt->getUpdates();
        /* @var Mage_Core_Model_Layout */
        $layout = $evt->getLayout();
        
        /* @var Mage_Core_Model_Layout_Update*/
        $update = $layout->getUpdate();

        $handles = $update->getHandles();
        
        $update->resetHandles();
        array_unshift($handles, 'before_all_handle');
        
        $handles = $this->_addHandles($handles);
        
        $handles[] = 'after_all_handle';
        foreach ($handles as $handle) {            
            $update->addHandle($handle);
        }
        
        
    }

    public function _addHandles($handles) {        
        $controller = Mage::registry('controller');        
        //before and after action handlers
        $request = $controller->getRequest();
        $del = "_";
        $handle = $request->getRequestedRouteName() . $del .
                $request->getRequestedControllerName() . $del .
                $request->getRequestedActionName();
        
        
        $flip = array_flip($handles);        
        $lowerFlip = (array) $flip;
        $lowerFlip = array_change_key_case($lowerFlip, CASE_LOWER);
        $pos = isset($flip[$handle]) ? $flip[$handle] :  $lowerFlip[strtolower($handle)] ;
        $flip[$handle. $del . 'before' ] = $pos - 0.1 . '';
        $flip[$handle. $del . 'after' ] = $pos + 0.1 . '';
        
        $handles = array_flip($flip);
        ksort($handles);
        $handles = array_values($handles);        
        return $handles;

//        foreach($handles as $k => $v){
//            echo $v. "<br/>";
//        }
//        die;
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function checkModuleCatalogoVirtual()
    {
        $catalogoVirtual = Mage::getStoreConfig('catalogo_virtual/default/is_active');

        if($catalogoVirtual) {
            // Se o módulo não está mais instalado, reverte roles e remove a config para não checar novamente.
            if(!Mage::helper('core')->isModuleEnabled('Jn2_CatalogoVirtual')) {
                $_adminRole = Mage::getModel('admin/role');

                $superUsuario = $_adminRole->getCollection()
                    ->addFieldToFilter('parent_id', '0')
                    ->getFirstItem();

                // Retorna role catalogo-virtual
                $roleCatalogo = $_adminRole->getCollection()
                    ->addFieldToFilter('role_type', 'G')
                    ->addFieldToFilter('user_id', '0')
                    ->addFieldToFilter('role_name', 'catalogo-virtual')
                    ->getFirstItem();

                // Retorna usuários que eram do catálogo
                $roles = $_adminRole->getCollection()
                    ->addFieldToFilter('role_type', 'U')
                    ->addFieldToFilter('parent_id', $roleCatalogo->getId())
                    ->getItems();

                // Modifica usuários que eram do catálogo
                foreach($roles as $adminRole) {
                    $adminRole->setParentId($superUsuario->getId())
                        ->save();
                }

                // Ativa bloco de login
                Mage::getModel('cms/block')
                    ->load('footer_block_voce')
                    ->setIsActive('1')->save();

                // Remove do core_resource para caso instalar o módulo novamente, executar o setup que modifica os roles
                Mage::getSingleton('core/resource')
                    ->getConnection('core_write')
                    ->delete('core_resource', 'code = "jn2_catalogovirtual_setup"');

                // modifica a config para não executar essa checagem novamente
                Mage::getConfig()
                    ->saveConfig('catalogo_virtual/default/is_active', 0);
            }
        }
    }


    /**
     * @param Varien_Event_Observer $observer
     */
    public function checkModuleMagentoS3()
    {
        $mediaStorage = Mage::getStoreConfig('system/media_storage_configuration/media_storage');

        if($mediaStorage == 2) {
            // Se o módulo não está mais instalado, reverte roles e remove a config para não checar novamente.
            if(!Mage::helper('core')->isModuleEnabled('Thai_S3')) {

                // modifica a config para não executar essa checagem novamente e corrige pastas de media
                Mage::getConfig()
                    ->saveConfig('system/media_storage_configuration/media_storage', Mage_Core_Model_File_Storage::STORAGE_MEDIA_FILE_SYSTEM)
                    ->saveConfig('web/unsecure/base_media_url', Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'media/')
                    ->saveConfig('web/secure/base_media_url', Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'media/');

                // Remove do core_resource para caso instalar o módulo novamente, executar o setup
                Mage::getSingleton('core/resource')
                    ->getConnection('core_write')
                    ->delete('core_resource', 'code = "thai_s3_setup"');

            }
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function checkModuleAdminCaptcha()
    {
        $enable = Mage::getStoreConfig('admin/captcha/enable');

        if($enable) {
            // Se o módulo não está mais instalado, reverte roles e remove a config para não checar novamente.
            if(!Mage::helper('core')->isModuleEnabled('ProxiBlue_ReCaptcha')) {

                Mage::getSingleton('core/resource')
                    ->getConnection('core_write')
                    ->delete('core_config_data', "path like '%admin/captcha%'");

                // Remove do core_resource para caso instalar o módulo novamente, executar o setup
                Mage::getSingleton('core/resource')
                    ->getConnection('core_write')
                    ->delete('core_resource', 'code = "proxiblue_recaptcha_setup"');

            }
        }
    }
}
