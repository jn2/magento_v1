<?php
class Jn2_Base_Model_VarnishCache_Observer extends Phoenix_VarnishCache_Model_Observer {
    /**
     * @see Phoenix_VarnishCache_Model_Observer
     * @param Varien_Event_Observer $observer
     * @override showVclUpdateMessage
     * 
     * Doesn't display errors on save config admin
     */
    public function showVclUpdateMessage(Varien_Event_Observer $observer){
       
    }
}