<?php

class Jn2_Base_Model_Sources_ButtonType {

  const BUTTON_NONE     = '0';
  const BUTTON_BUY      = '1';
  //const BUTTON_DETAIL   = 'details';

  public function toOptionArray()
  {
    $options = array();

    $options = [
        self::BUTTON_NONE    => 'Não exibir',
        self::BUTTON_BUY     => 'Exibir botão comprar',
        //self::BUTTON_DETAIL  => 'Exibir botão detalhes',

    ];

    return $options;
  }

}

?>
