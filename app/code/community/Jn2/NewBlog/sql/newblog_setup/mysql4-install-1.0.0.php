<?php

$installer = $this;

$installer->startSetup();
try {
    $sql = "ALTER TABLE {$this->getTable('blog/blog')} ADD `post_image` VARCHAR( 255 ) NOT NULL DEFAULT '';";
    $installer->run($sql);
} catch (Exception $e) {
    Mage::logException($e);
}

$installer->endSetup();


