<?php
class Jn2_DetailedPrice_Helper_Data extends Mage_Core_Helper_Abstract {

   /**
   * Retorna o valor de uma parcela, dados o valor total a ser parcelado,
   * a taxa de juros e o numero de prestacoes
   *
   * @param   string $total
   * @param   string $interest
   * @param   string $periods
   * @return  string
   */
    public function calcInstallmentValueCielo($total, $interest, $periods) {
     /*
      * Formula do coeficiente:
      *
      * juros / ( 1 - 1 / (1 + i)^n )
      *
      */

     // confere se taxa de juros = 0
     if($interest <= 0) {
             return ($total / $periods);
     }

     // calcula o coeficiente, seguindo a formula acima
     $coefficient = pow((1 + $interest), $periods);
     $coefficient = 1 / $coefficient;
     $coefficient = 1 - $coefficient;
     $coefficient = $interest / $coefficient;

     // retorna o valor da parcela
     return ($total * $coefficient);
  }

  public function calcInstallmentValuePagarme($total, $interest, $periods) {
    return ($total + $total*$interest * $periods)/$periods;
  }
  
  public function calcInstallmentValueeRede($total, $interest, $periods) {
      if (Mage::helper('core')->isModuleEnabled('Jn2_Installmentrule')) {
          $installment = Mage::helper( 'installmentrule' )->calculateInstallmentFee( $total, $periods );
          $newGrantTotal = $total + $installment;
          return $newGrantTotal / $periods;
      } else {
          return $this->calcInstallmentValueCielo($total, $interest, $periods);
      }
  }
  
}
