<?php

class Jn2_DetailedPrice_Model_System_Config_Source_InstallmentType {
    public function toOptionArray() {
        if (Mage::helper('core')->isModuleEnabled('Jn2_Installmentrule')) {
            $options = array(
              'cielo'   => 'Juros Cielo',
              'pagarme' => 'Juros Pagar.me',
              'erede' => 'Juros eRede',
            );
        } else {
            $options = array(
              'cielo'   => 'Juros Cielo',
              'pagarme' => 'Juros Pagar.me'
            );
        }
        return $options;
    }
}
