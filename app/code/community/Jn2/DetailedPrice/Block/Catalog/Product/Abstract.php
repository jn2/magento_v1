<?php

/**
 * Class Jn2_DetailedPrice_Block_Catalog_Product_Installments
 *
 * @author Thiago Machado <thiago.machado@jn2.com.br>
 * @version 0.1.0
 */
abstract class Jn2_DetailedPrice_Block_Catalog_Product_Abstract extends Mage_Catalog_Block_Product_Price
{
    /**
     * Retorna as opções de parcelamento possíveis
     * @return array
     */
    public function getInstallmentOptions()
    {
        if ($this->canShowBlock()) {

            $product = $this->getProduct();
            $price = max($product->getFinalPrice(), 1);

            $max_installments = intval(Mage::getStoreConfig('detailedprice/installments_price/max_installments'));
            $min_installment_value = floatval(Mage::getStoreConfig('detailedprice/installments_price/min_installment_value'));
            $max_installments_without_interest = intval(Mage::getStoreConfig('detailedprice/installments_price/max_installments_without_interest'));
            $interest_value = floatval(str_replace(',', '.', Mage::getStoreConfig('detailedprice/installments_price/interest_value')));
            switch (Mage::getStoreConfig('detailedprice/installments_price/installment_type')) {
                case 'pagarme':
                    $interest_method = 'getInstallmenteRede';
                    break;
                case 'erede':
                    $interest_method = 'getInstallmenteRede';
                    break;
                case 'cielo':
                default:
                    $interest_method = 'getInstallmentCielo';
            }
            

            $min_installment_value = max($min_installment_value, 1);
            $max_installments = max(1, $max_installments);
            $total_installment = max(1, intval(($price / $min_installment_value)));

            if ($total_installment > $max_installments) {
                $total_installment = $max_installments;
            }

            $installments = array();

            for ($i = 1; $i <= $total_installment; $i++) {
                if ($i <= $max_installments_without_interest) {
                    $installments[$i] = array(
                        'value' => round($price / $i, 2),
                        'interest' => false,
                    );
                } else {
                    $installments[$i] = array(
                        'value' => round($this->$interest_method($price, $interest_value / 100, $i), 2),
                        'interest' => true
                    );
                }
            }

            return $installments;
        }

        return array();
    }

    public function getInstallmenteRede($total, $interest, $periods) {
        if (Mage::helper('core')->isModuleEnabled('Jn2_Installmentrule')) {
            $installment = Mage::helper( 'installmentrule' )->calculateInstallmentFee( $total, $periods );
        
            $newGrantTotal = $total + $installment;
            return $newGrantTotal / $periods;
        } else {
            return $this->getInstallmentCielo($total, $interest, $periods);
        }
    }
    
    /**
     * Calcula juros Cielo
     *
     * @param $total
     * @param $interest
     * @param $periods
     *
     * @return float
     */
    protected function getInstallmentCielo($total, $interest, $periods)
    {
        if ($interest <= 0) {
            return $total / $periods;
        }

        $coefficient = pow((1 + $interest), $periods);
        $coefficient = 1 / $coefficient;
        $coefficient = 1 - $coefficient;
        $coefficient = $interest / $coefficient;

        return $total * $coefficient;
    }

    /**
     * Calcula juros Pagarme
     *
     * @param $total
     * @param $interest
     * @param $periods
     *
     * @return float
     */
    protected function getInstallmentPagarme($total, $interest, $periods)
    {
        if ($interest <= 0) {
            return $total / $periods;
        }

        return ($total + $total * $interest * $periods) / $periods;
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->canShowBlock()) {
            return parent::_toHtml();
        }

        return '';
    }

    /**
     * Verifica se o bloco pode ser exibido
     * @return bool
     */
    protected function canShowBlock()
    {
        return Mage::getStoreConfig('detailedprice/installments_price/active');
    }
}
