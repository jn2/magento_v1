<?php

/**
 * Class Jn2_DetailedPrice_Block_Catalog_Product_Table
 *
 * @author Thiago Machado <thiago.machado@jn2.com.br>
 * @version 0.1.0
 */
class Jn2_DetailedPrice_Block_Catalog_Product_Table extends Jn2_DetailedPrice_Block_Catalog_Product_Abstract
{
    /**
     * @inheritdoc
     */
    protected function canShowBlock()
    {
        return Mage::getStoreConfig('detailedprice/installments_price/show_options_table') && parent::canShowBlock();
    }
}