<?php

class Jn2_DetailedPrice_Block_Catalog_Product_Price extends Mage_Catalog_Block_Product_Price
{

    public function getDetailedPriceHtml()
    {
        //Definindo template
        $this->setTemplate('jn2/detailedprice/catalog/product/detailedprice.phtml');
        return $this->renderView();
    }

    public function getInstalmentsHtml()
    {
        $show_instalments = Mage::getStoreConfig('detailedprice/installments_price/active');

        if ($show_instalments) {
            $instalments = $this->getInstalments();
            $instalment_value = number_format($instalments['instalment_value'], 2, ",", ".");
            $total_instalment = $instalments['total_instalment'];
            $instalment_total_value = number_format($instalments['instalment_value']*$total_instalment, 2, ",", ".");
            $instalment_value_without_interest = number_format($instalments['instalment_value_without_interest'], 2, ",", ".");
            $max_instalments_without_interest = $instalments['max_instalments_without_interest'];
            $instalment_price = number_format($instalments['instalment_price'], 2, ",", ".");
            $min_installment_value = $instalments['min_installment_value'];
            $max_periods = $instalments['max_periods'];

            if ($total_instalment <= Mage::getStoreConfig('detailedprice/installments_price/max_installments_without_interest')) {
                $description = Mage::getStoreConfig('detailedprice/installments_price/description_without_interest');
            } else {
                $description = Mage::getStoreConfig('detailedprice/installments_price/description');
                $interest_vars = ['{{numero_parcela_sem_juros}}','{{valor_parcela_sem_juros}}','{{total_sem_juros}}'];
                $instalments_vars = ['max_instalments_without_interest' => $max_instalments_without_interest,
                    'instalment_value_without_interest' => $instalment_value_without_interest,
                    'instalment_price' => $instalment_price];
            }

            $variables = array('{{valor_parcela}}', '{{numero_parcela}}', '{{total_parcelado}}');
            if (!empty($interest_vars)) {
                $variables = array_merge($variables, $interest_vars);
            }

            $instalments = array($instalment_value, $total_instalment, $instalment_total_value);
            if (!empty($instalments_vars)) {
                $instalments = array_merge($instalments, $instalments_vars);
            }

            $this->setData('total_instalments', $total_instalment);

            $this->setData('instalments_value', $instalment_value);
            $this->setData('description', $description);
            $this->setData('variables', $variables);
            $this->setData('instalments', $instalments);
            $this->setData('min_installment_value', $min_installment_value);
            $this->setData('max_periods', $max_periods);

            $this->setTemplate('jn2/detailedprice/catalog/product/instalments.phtml');

            return $this->renderView();
        } else {
            return '';
        }
    }

    public function getBilletHtml()
    {
        //Pegando configuraçes
        $active = Mage::getStoreConfig('detailedprice/billet_price/active');
        $rule_id = Mage::getStoreConfig('detailedprice/billet_price/rule_id');

        //Se a configuração estiver ativa
        if ($active && !empty($rule_id)) {

            //Carrega a regra
            $rule = Mage::getModel('salesrule/rule')->load($rule_id);
            $product = $this->getProduct();

            //Aceitar apenas promoções de porcentagem e não exibir em produtos agrupados
            if ($rule->getSimpleAction() != 'by_percent' || $product->getTypeId() == 'grouped') {
                return '';
            }

            //Obtendo valor final do produto
            $price = $product->getFinalPrice();
            $discount = $rule->getDiscountAmount();

            //Calculando valor do desconto
            $end_price = number_format($price - (($price * $discount) / 100), 2, ",", ".");

            //Passando valores pro template
            $this->setData('description', $rule->getDescription());
            $this->setData('end_price', $end_price);
            $this->setData('billet_discount', $discount);
            $this->setData('original_price', $price);

            //Definindo template
            $this->setTemplate('jn2/detailedprice/catalog/product/billet.phtml');

            //Retornando HTML renderizado
            return $this->renderView();
        } else {
            return '';
        }

    }

    /**
     * Criar um array com os valores de parcelamento
     *
     * @return array
     */
    private function getInstalments()
    {
        $prodcut = $this->getProduct();

        $max_installments = intval(Mage::getStoreConfig('detailedprice/installments_price/max_installments'));
        $min_installment_value = floatval(Mage::getStoreConfig('detailedprice/installments_price/min_installment_value'));
        $max_instalments_without_interest = intval(Mage::getStoreConfig('detailedprice/installments_price/max_installments_without_interest'));
        $interest_value = Mage::getStoreConfig('detailedprice/installments_price/interest_value');
        $interest_value = floatval(str_replace(',', '.', $interest_value));
        $interest_type = Mage::getStoreConfig('detailedprice/installments_price/installment_type');
        $price = max($prodcut->getFinalPrice(), 1);
        //Calculando total de parcelas
        $min_installment_value = max($min_installment_value, 1);
        $max_installments = max(1, $max_installments);
        $total_installment = max(1, intval(($price / $min_installment_value)));

        if ($total_installment > $max_installments) {
            $total_installment = $max_installments;
        }

        if ($interest_type == 'cielo' || !$interest_type) {
            $instalment_value_with_interest = round(Mage::helper('detailedprice')->calcInstallmentValueCielo($price, $interest_value / 100, $total_installment), 2);
        } elseif ($interest_type == 'pagarme') {
            $instalment_value_with_interest = round(Mage::helper('detailedprice')->calcInstallmentValuePagarme($price, $interest_value / 100, $total_installment), 2);
        } elseif ($interest_type = 'erede') {
            $instalment_value_with_interest = round(Mage::helper('detailedprice')->calcInstallmentValueeRede($price, $interest_value / 100, $total_installment), 2);
        }

        $instalment_value_without_interest = $price / $max_instalments_without_interest;

        //Calculando valor da parcela
        if ($total_installment > $max_instalments_without_interest) {
            $installment_value = $instalment_value_with_interest;
        } else {
            $installment_value = $price / $total_installment;;
        }
        
        if ($interest_type = 'erede') {
            $installment_value = $instalment_value_with_interest;
        }

        $this->setData('max_instalments_without_interest', $max_instalments_without_interest);
        $this->setData('instalment_value_without_interest', $instalment_value_without_interest);
        $this->setData('total_installment', $total_installment);
        $this->setData('installment_price', $price);
        $this->setData('interest_value', $interest_value);
        $this->setData('installment_type', $interest_type);

        return array(
            "max_instalments_without_interest" => $max_instalments_without_interest,
            "instalment_value_without_interest" => $instalment_value_without_interest,
            "total_instalment" => $total_installment,
            "instalment_value" => $installment_value,
            "min_installment_value" => $min_installment_value,
            "max_periods" => $max_installments,
            "instalment_price" => $price
        );
    }
}
