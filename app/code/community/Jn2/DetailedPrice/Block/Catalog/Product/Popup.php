<?php

/**
 * Class Jn2_DetailedPrice_Block_Catalog_Product_Popup
 *
 * @author Thiago Machado <thiago.machado@jn2.com.br>
 * @version 0.1.0
 */
class Jn2_DetailedPrice_Block_Catalog_Product_Popup extends Jn2_DetailedPrice_Block_Catalog_Product_Abstract
{
    /**
     * @inheritdoc
     */
    protected function canShowBlock()
    {
        return Mage::getStoreConfig('detailedprice/installments_price/show_options_popup') && parent::canShowBlock();
    }
}