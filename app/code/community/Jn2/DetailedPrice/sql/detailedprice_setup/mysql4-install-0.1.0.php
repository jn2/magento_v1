<?php

/**
 * mysql4-install-0.1.0.php
 *
 * @author Thiago Machado <thiagohenriqu3@gmail.com>
 * @version 0.1.0
 */
$installer = $this;
$installer->startSetup();

$sql = <<<SQL
UPDATE {$installer->getTable('core_config_data')}
  SET value = REPLACE(value, '{{numero_parcela}}', '<span class="periods">{{numero_parcela}}</span>')
  WHERE path IN ('detailedprice/installments_price/description', 'detailedprice/installments_price/description_without_interest')
  AND value NOT LIKE '%<span class="periods">%'
SQL;

$installer->run($sql);
$installer->endSetup();