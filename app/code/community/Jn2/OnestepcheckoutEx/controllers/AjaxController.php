<?php

require_once 'Inovarti/Onestepcheckout/controllers/AjaxController.php';
class Jn2_OnestepcheckoutEx_AjaxController extends Inovarti_Onestepcheckout_AjaxController {

    public function placeOrderAction() {
        if ($this->_expireAjax()) {
            return;
        }$result = array(
            'success' => true,
            'messages' => array(),
        );
        try {//TODO: re-factoring. Move to helpers
            if ($this->getRequest()->isPost()) {
                $billingData = $this->getRequest()->getPost('billing', array());
                // save checkout method
                if (!$this->getOnepage()->getCustomerSession()->isLoggedIn()) {
                    if (isset($billingData['create_account'])) {
                        $this->getOnepage()->saveCheckoutMethod(Mage_Checkout_Model_Type_Onepage::METHOD_REGISTER);
                    } else {
                        $this->getOnepage()->saveCheckoutMethod(Mage_Checkout_Model_Type_Onepage::METHOD_GUEST);
                    }
                }

                if (!$this->getOnepage()->getQuote()->getCustomerId() &&
                        Mage_Checkout_Model_Type_Onepage::METHOD_REGISTER == $this->getOnepage()->getQuote()->getCheckoutMethod()
                ) {
                    if ($this->_customerEmailExists($billingData['email'], Mage::app()->getWebsite()->getId())) {
                        $result['success'] = false;
                        $result['messages'][] = $this->__('There is already a customer registered using this email address. Please login using this email address or enter a different email address to register your account.');
                    }
                }


                // Se existe frete ativo e não está setado no pedido, retorna erro
                $shippingMethod = $this->getRequest()->getPost('shipping_method', null);

                if(empty($shippingMethod)) {
                    $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();

                    if(!empty($methods) && !$this->getOnepage()->getQuote()->isVirtual()) {
                        $result['success'] = false;
                        $result['messages'][] = $this->__(Mage::helper('checkout')->__('Nenhum método de entrega disponível para sua região.'));
                    }
                }

                $amdelivery = $this->getRequest()->getPost('amdeliverydate');
                if($amdelivery !== null && sizeof($amdelivery) > 2) {
                    $disabledTintervals = Mage::helper('amdeliverydate')->getDisabledTintervals();
                    $date = DateTime::createFromFormat("d/m/yy", $amdelivery['date'])->format("yy-m-d");
                    if(array_key_exists($date, $disabledTintervals)) {
                        $intervalId = $amdelivery['tinterval_id'];
                        if(array_key_exists($intervalId, $disabledTintervals[$date])) {
                            $result['success'] = false;
                            $result['messages'][] = $this->__("Time scheduled for delivery is no longer available");
                        }
                    }
                }

                if ($result['success']) {
                    // save billing address
                    $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);
                    if (isset($billingData['email'])) {
                        $billingData['email'] = trim($billingData['email']);
                    }
                    $saveBillingResult = $this->getOnepage()->saveBilling($billingData, $customerAddressId);

                    //save shipping address
                    if (!isset($billingData['use_for_shipping'])) {
                        $shippingData = $this->getRequest()->getPost('shipping', array());
                        $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
                        $saveShippingResult = $this->getOnepage()->saveShipping($shippingData, $customerAddressId);
                    }

                    // check errors
                    if (isset($saveShippingResult)) {
                        $saveResult = array_merge($saveBillingResult, $saveShippingResult);
                    } else {
                        $saveResult = $saveBillingResult;
                    }

                    if (isset($saveResult['error'])) {
                        $result['success'] = false;
                        $result['messages'] = array_merge($result['messages'], $saveResult['message']);
                    } else {
                        // check agreements
                        $requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds();
                        $postedAgreements = array_keys($this->getRequest()->getPost('inovarti_osc_agreement', array()));
                        if ($diff = array_diff($requiredAgreements, $postedAgreements)) {
                            $result['success'] = false;
                            $result['messages'][] = $this->__('Please agree to all the terms and conditions before placing the order.');
                        } else {
                            if ($data = $this->getRequest()->getPost('payment', false)) {
                                $this->getOnepage()->getQuote()->getPayment()->importData($data);
                            }

                            //save data for use after order save
                            $data = array(
                                'comments' => $this->getRequest()->getPost('comments', false),
                                'is_subscribed' => $this->getRequest()->getPost('is_subscribed', false),
                                'billing' => $this->getRequest()->getPost('billing', array()),
                                'segments_select' => $this->getRequest()->getPost('segments_select', array())
                            );
                            Mage::getSingleton('checkout/session')->setData('onestepcheckout_order_data', $data);

                            $redirectUrl = $this->getOnepage()->getQuote()->getPayment()->getCheckoutRedirectUrl();
                            if (!$redirectUrl) {
                                $this->getOnepage()->saveOrder();
                                $redirectUrl = $this->getOnepage()->getCheckout()->getRedirectUrl();
                            }
                        }
                    }
                }
            } else {
                $result['success'] = false;
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success'] = false;
            $result['messages'][] = $e->getMessage();
        }
        if ($result['success']) {
            $this->getOnepage()->getQuote()->save();
            if (isset($redirectUrl)) {
                $result['redirect'] = $redirectUrl;
            }
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * helper
     * @return null|Mage_Core_Controller_Front_Action
     */
    private function _getCustomerWishlistController($request, $response) {
        $fbIntegratorModuleName = 'Mage_Wishlist';
        $controllerName = 'index';

        return $this->_createController($fbIntegratorModuleName, $controllerName, $request, $response);
    }

    /**
     * helper
     * @return null|Mage_Core_Controller_Front_Action
     */
    private function _getProductCompareController($request, $response) {
        $fbIntegratorModuleName = 'Mage_Catalog';
        $controllerName = 'product_compare';

        return $this->_createController($fbIntegratorModuleName, $controllerName, $request, $response);
    }

    /**
     * helper
     * @param $moduleName
     * @param $controllerName
     * @param $request
     * @param $response
     *
     * @return Mage_Core_Controller_Front_Action|null
     */
    private function _createController($moduleName, $controllerName, $request, $response) {
        $router = Mage::app()->getFrontController()->getRouter('standard');
        $controllerFileName = $router->getControllerFileName($moduleName, $controllerName);
        if (!$router->validateControllerFileName($controllerFileName)) {
            return null;
        }
        $controllerClassName = $router->getControllerClassName($moduleName, $controllerName);
        if (!$controllerClassName) {
            return null;
        }

        if (!class_exists($controllerClassName, false)) {
            if (!file_exists($controllerFileName)) {
                return null;
            }
            include $controllerFileName;

            if (!class_exists($controllerClassName, false)) {
                return null;
            }
        }
        $controllerInstance = Mage::getControllerInstance(
                        $controllerClassName, $request, $response
        );
        return $controllerInstance;
    }


    private function _isEmailRegistered($email) {
        $model = Mage::getModel('customer/customer');
        $model->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email);

        if ($model->getId() == NULL) {
            return false;
        }

        return true;
    }

    /**
     * Payment method save
     */
    public function savePaymentMethodAction() {
        if ($this->_expireAjax()) {
            return;
        }
        $result = array(
            'success' => true,
            'messages' => array(),
            'blocks' => array(),
            'grand_total' => ""
        );

        try {
            if ($this->getRequest()->isPost()) {
                $data = $this->getRequest()->getPost('payment', array());

                $session = Mage::getSingleton('checkout/session');

                $saveResult = $this->getOnepage()->savePayment($data);
                if (isset($saveResult['error'])) {
                    $result['success'] = false;
                    $result['messages'][] = $saveResult['message'];
                }
                $this->getOnepage()->getQuote()->collectTotals()->save();
                $result['blocks'] = $this->getUpdater()->getBlocks();
                $result['grand_total'] = Mage::helper('onestepcheckout')->getGrandTotal($this->getOnepage()->getQuote());
            } else {
                $result['success'] = false;
                $result['messages'][] = $this->__('Please specify payment method.');
            }
        } catch (Exception $e) {
            #die($e);
            Mage::logException($e);
            $this->getOnepage()->getQuote()->collectTotals()->save();
            $result['blocks'] = $this->getUpdater()->getBlocks();
            $result['success'] = false;
            $result['error'][] = $this->__('Unable to set Payment Method.');
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
}
