<?php

class Jn2_OnestepcheckoutEx_Helper_Config extends Inovarti_Onestepcheckout_Helper_Config {
    /*
     * Display or hide Inscrição municipal
     */

    const EXCLUDE_INCLUDE_IS_IM = 'onestepcheckout/exclude_include_fields/is_im';

    public function isIm($store = null) {
        return Mage::getStoreConfig(self::EXCLUDE_INCLUDE_IS_IM, $store);
    }

}
