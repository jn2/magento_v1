<?php
###############################################################
# 
###############################################################
/* @var $this Jn2_OnestepcheckoutEx_Resource_Setup */
$installer = $this;
$installer->startSetup();
$customerEntityTypeCode = 'customer';
$customerEntityTypeId = Mage::getModel('eav/entity_type')
        ->load($customerEntityTypeCode, 'entity_type_code')
        ->getId();



$fieldsToDestroy = array(
    'cnpj',
    'insestadual',
    'nomefantasia'
);

foreach ($fieldsToDestroy as $code) {
    $installer->removeAttribute($customerEntityTypeCode, $code);
}

# RENAME TAXVAT TO CPF/CNPJ
$fieldsToRenameLabel = array(
    'taxvat' => 'CPF/CNPJ'
);
$conn = $installer->getConnection();
$eavAttributeTable = $installer->getTable('eav/attribute');
foreach ($fieldsToRenameLabel as $code => $label) {//  
    $attribute = Mage::getSingleton('eav/config')
            ->getAttribute($customerEntityTypeCode, $code);
    $attribute->setData('frontend_label', $label);
    $attribute->save();
}

#REMOVE OLD LISTED MODULES moip_onestepcheckout_setup
$resourcesInstallersToRemove = array('moip_onestepcheckout_setup');

$table = $this->getTable('core/resource');

$where = array(
    $conn->quoteInto('code in (?)', $resourcesInstallersToRemove)
);
$where = join(" AND ", $where);
$conn->delete($table, $where);



################################################
# Add inscrição municipal
################################################

$fieldsToAppend = array('im' => array(
        'label' => 'Inscrição Municipal',
        'type' => 'varchar', //backend_type
        'input' => 'text', //frontend_input        
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => true,
        'required' => false,
        'default' => '',
        'frontend' => '',
        'unique' => false,
        'note' => '',        
        'sort_order' => 200
        ));
foreach ($fieldsToAppend as $code => $options) {    
    $installer->addAttribute($customerEntityTypeCode, $code, $options);    
    $installer->updateAttribute($customerEntityTypeCode, $code, $options);
    Mage::getSingleton('eav/config')->getAttribute('customer', $code)->setData('used_in_forms', array(
        'adminhtml_customer', 'customer_account_create', 'customer_account_edit', 'checkout_register'
    ))->save();
}

$installer->endSetup();
