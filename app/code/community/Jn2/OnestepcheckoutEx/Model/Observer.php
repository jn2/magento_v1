<?php

class Jn2_OnestepcheckoutEx_Model_Observer {

    protected $_ignoreFields = array(
        'customer_password',
        'confirm_password'
    );
    protected $_monitoringChains = array(
        'billing',
        'shipping'
    );
    protected $_filter = null;

    protected function _getFilter() {
        if (null === $this->_filter) {
            $this->_filter = new Zend_Filter_StripTags();
        }
        return $this->_filter;
    }

    protected function _filterInput($value) {
        if (is_array($value)) {
            foreach ($value as $key => $_value) {
                if (array_search($key, $this->_ignoreFields,true) !== false) {
//                    echo in_array($key, $this->_ignoreFields). " --> " .
//                    "Ignoring {$key} #{$_value}\n";
                    continue;
                }
                $value[$key] = $this->_filterInput($_value);
            }
            return $value;
        } else {
            return $this->_getFilter()->filter($value);
        }
    }

    public function filterUserInputs($evt) {

        $controller = $evt->getData('controller_action');
        $request = $controller->getRequest();
        if ($request->isPost()) {

            foreach ($this->_monitoringChains as $chain) {
                $params = $request->getPost($chain);
                $filtered_params = $this->_filterInput($params);
                $request->setPost($chain, $filtered_params);
            }           
        }
    }

    /**
     * Save extra fields from checkout
     * @param Varien_Event $evt
     */
    public function copyCustomerExtraFields($evt) {
        $request = Mage::app()->getFrontController()->getRequest();
        $target = $evt->getTarget();
        $billingData = $request->getPost('billing'); //get billing data from post
        $saveToCustomerData = Mage::getConfig()->getNode('global/onestepcheckoutex/save_billing_data_customer');
        
        $extraFields = array();
        
        /*@var $xmlNode Varien_Simplexml_Element */
        foreach($saveToCustomerData->children() as $elm){            
            $to = trim((string) current($elm -> xpath('to')  ) );
            if('' === $to){
               $to =  $elm->getName();               
            }            
            $extraFields[$elm->getName()] = $to;
        }
        
       
        $values = array();
        foreach ($extraFields as $from => $to) {
            $value = $billingData[$from];
            if (!$target->hasData($to) && trim($value) !== '') {
                $values[$to] = $value;
                $target->setData($to, $value);
            }
        }   

        
    }

}
