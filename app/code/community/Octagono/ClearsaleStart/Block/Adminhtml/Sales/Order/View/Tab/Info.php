<?php
/**
 * Octagono Ecommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.octagonoecommerce.com.br/eula-licenca-usuario-final.html
 *
 *
 * @category   Clearsale Start
 * @package    Octagono_ClearsaleStart
 * @copyright  Copyright (c) 2009-2014 - Octagono Tecnologia - www.octagonotecnologia.com.br
 * @license    http://www.octagonotecnologia.com.br/eula-licenca-usuario-final.html
 */
class Octagono_ClearsaleStart_Block_Adminhtml_Sales_Order_View_Tab_Info extends Mage_Adminhtml_Block_Sales_Order_View_Tab_Info {

	public function  __construct() {
		parent::__construct();
	}

	public function getPaymentHtml()
	{
		$order = $this->getOrder();

		$clearsale = Mage::getModel('octagono_clearsalestart/gateway');	
		$url = $clearsale->getScoreUrl($order);

		$html = parent::getPaymentHtml();
		$html .= "<div style=\"margin-left:-8px;\"><iframe style=\"width:277px;height:96px;border:none;\" src=\"". $url ."\"></iframe></div>";

		return $html;
	}
}
