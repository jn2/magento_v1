<?php
/**
 * Octagono Ecommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.octagonoecommerce.com.br/eula-licenca-usuario-final.html
 *
 *
 * @category   Clearsale Start
 * @package    Octagono_ClearsaleStart
 * @copyright  Copyright (c) 2009-2014 - Octagono Tecnologia - www.octagonotecnologia.com.br
 * @license    http://www.octagonotecnologia.com.br/eula-licenca-usuario-final.html
 */
class Octagono_ClearsaleStart_Model_Source_Paymentmethod {
	
	public function toOptionArray() {

       $payments = Mage::getSingleton('payment/config')->getActiveMethods();
       $methods = array(array('value'=>'', 'label'=>Mage::helper('adminhtml')->__('--Please Select--')));

       foreach ($payments as $paymentCode=>$paymentModel) {
            $paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
            $methods[$paymentCode] = array(
                'label'   => $paymentTitle,
                'value' => $paymentCode,
            );
        }

        return $methods;
	}
}
