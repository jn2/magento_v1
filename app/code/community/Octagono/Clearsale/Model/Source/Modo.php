<?php
/**
 * Octagono Ecommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.octagonoecommerce.com.br/eula-licenca-usuario-final.html
 *
 *
 * @category   Clearsale
 * @package    Octagono_Clearsale
 * @copyright  Copyright (c) 2009-2011 - Octagono Ecommerce - www.octagonoecommerce.com.br
 * @license    http://www.octagonoecommerce.com.br/eula-licenca-usuario-final.html
 */
class Octagono_Clearsale_Model_Source_Modo
{
    public function toOptionArray()
    {
        return array(
            array('value' => '0', 'label' => 'Homologação'),
            array('value' => '1', 'label' => 'Produção')
        );
    }
}

