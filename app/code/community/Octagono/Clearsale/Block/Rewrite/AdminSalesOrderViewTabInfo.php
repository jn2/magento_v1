<?php
/**
 * Octagono Ecommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.octagonoecommerce.com.br/eula-licenca-usuario-final.html
 *
 *
 * @category   Clearsale
 * @package    Octagono_Clearsale
 * @copyright  Copyright (c) 2009-2011 - Octagono Ecommerce - www.octagonoecommerce.com.br
 * @license    http://www.octagonoecommerce.com.br/eula-licenca-usuario-final.html
 */
class Octagono_Clearsale_Block_Rewrite_AdminSalesOrderViewTabInfo extends Mage_Adminhtml_Block_Sales_Order_View_Tab_Info
{

	public function getLogPath()
	{
		$ret = Mage::getBaseDir() . '/var/log/clearsale.log';

		if (!file_exists(Mage::getBaseDir() . '/var/log/')) {
			mkdir(Mage::getBaseDir() . '/var/log/');
		}

		return $ret;
	}


	public function getDebug()
	{
		return(true);
	}

	public function produtos()
	{
		$_order 	 = $this->getOrder();

        $sArr = array();


        print_r($sArr);die();


        $rArr = array();
        foreach ($sArr as $k => $v) {
            // troca caractere '&' por 'e'
            $value =  str_replace("&", "e", $v);
            $rArr[$k] =  $value;
        }

        return $rArr;
	}


    protected function _afterToHtml($html)
    {


		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
		}

    	$html = parent::_afterToHtml($html);


		$_order 	 = $this->getOrder();
		$_order_data = $_order->getData();

        $metodoPag = $_order->getPayment()->getCcType();

		$modulo_ativo = Mage::getStoreConfig('modulos/clearsale/active');

		if( $modulo_ativo && ($metodoPag != NULL) )
		{

    	   	$endereco_cobranca = $_order->getBillingAddress();
    	   	$endereco_entrega  = $_order->getShippingAddress();

    	   	$customer_id = $_order_data['customer_id'];
    	   	$_customer   = Mage::getModel('customer/customer')->load($customer_id);

	    	$start  = strpos($html,'<!--Payment Method-->');
	        $start  = strpos($html,"</fieldset>",$start);
	        $html_1 = substr($html,0,$start);
	        $html_2 = substr($html,$start);
	        $html3 = NULL;

	        // Formatando telefone
	        $telephoneEntregaTemp = preg_replace("/[^0-9]/","", $endereco_entrega->getTelephone());
			$telTemp = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);

	        switch (strlen($telephoneEntregaTemp))
	        {
	        	case 8:
	        		$telefoneEntrega = $telephoneEntregaTemp;
	        		$dddEntrega      = '11';
	        	break;

	        	case 10:
	        		$telefoneEntrega = substr($telephoneEntregaTemp, -8);
	        		$dddEntrega      = substr($telephoneEntregaTemp, -10, 2);
	        	break;

				case 11:
					$telefoneEntrega = substr($telephoneEntregaTemp, -8);
					$dddEntrega      = substr($telephoneEntregaTemp, -10, 2);
				break;

	        	case 12:
	        		$telefoneEntrega = substr($telephoneEntregaTemp, -8);
	        		$dddEntrega      = substr($telephoneEntregaTemp, -10, 2);
	        	break;
				//Em caso de falha não deixar de consultar
				default:
					$telefoneEntrega = $telTemp;
					$dddEntrega      = '11';
				break;
	        }

	        // Prepara os metodos de Pagamento



	        switch ( $metodoPag )
	        {
	        	case 'diners':
	        		$metodoPagamentos = '1';
	        	break;

	        	case 'mastercard':
	        		$metodoPagamentos = '2';
	        	break;

	        	case 'visa':
	        		$metodoPagamentos = '3';
	        	break;

	        	case 'elo':
	        		$metodoPagamentos = '4';
	        	break;

	        	case 'amex':
	        		$metodoPagamentos = '5';
	        	break;
	        }


	        /*
	         * Alterado para suportar checkout as guest (sem objeto customer)
	         * Então pegamos taxvat do Order
	         */
	        $cpfComprador			  = $_order_data['customer_taxvat'];

	        $array_from = array( 'À', 'Á', 'Ã', 'Â', 'É', 'Ê', 'Í', 'Ó', 'Õ', 'Ô', 'Ú', 'Ü', 'Ç', 'à', 'á', 'ã', 'â', 'é', 'ê', 'í', 'ó', 'õ', 'ô', 'ú', 'ü', 'ç');
	        $array_to   = array( 'A', 'A', 'A', 'A', 'E', 'E', 'I', 'O', 'O', 'O', 'U', 'U', 'C', 'a', 'a', 'a', 'a', 'e', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'c' );

	        $cidadeComprador		  = strtoupper(str_ireplace(' ', '+', str_replace($array_from, $array_to, $endereco_cobranca->getCity())));
	        $ufComprador			  = $endereco_cobranca->getRegion();

	        $paisComprador		 	  = str_ireplace(' ', '+', $endereco_cobranca->getCountryId());

	        switch( $endereco_cobranca->getCountryId() )
	        {
	        	case "BR":
	        		$paisComprador = "BRA";
	        	break;
	        }

	        if ($_order->getCustomerFaturarContra() == "1")
	        {
	        	$nomeComprador = $endereco_cobranca->getCompany();
	        	$cpfComprador = $_order->getCustomerCnpj();
	        }

	        //Mage::log($_order->getBillingAddress());

	        $cepComprador			  = str_ireplace('-', '', $endereco_cobranca->getPostcode());
	        $emailComprador			  = str_ireplace(' ', '+', $_order->_data["customer_email"]);
	        $nomeEntrega			  = str_ireplace(' ', '+', $endereco_entrega->getName());
	        $ruaEntrega				  = str_ireplace(' ', '+', $endereco_entrega->getStreet(1));
	        $numeroEntrega            = $endereco_entrega->getStreet(2);
	        $cidadeEntrega  		  = strtoupper(str_ireplace(' ', '+', str_replace($array_from, $array_to, $endereco_entrega->getCity())));
	        $ufEntrega				  = $endereco_entrega->getRegion();

	        $paisEntrega			  = str_ireplace(' ', '+', $endereco_entrega->getCountryId());

			switch( $endereco_entrega->getCountryId() )
	        {
	        	case "BR":
	        		$paisEntrega = "BRA";
	        	break;
	        }

			$additionaldata = unserialize($_order->getPayment()->getData('additional_data'));


			switch ( $endereco_cobranca->getRegion() )
			{
				case 'Acre':
					$ufCobranca = 'AC';
					break;
				case 'Alagoas':
					$ufCobranca = 'AL';
					break;
				case 'Amapá':
					$ufCobranca = 'AP';
					break;
				case 'Amazonas':
					$ufCobranca = 'AM';
					break;
				case 'Bahia':
					$ufCobranca = 'BA';
					break;
				case 'Ceará':
					$ufCobranca = 'CE';
					break;
				case 'Distrito Federal':
					$ufCobranca = 'DF';
					break;
				case 'Espírito Santo':
					$ufCobranca = 'ES';
					break;
				case 'Goiás':
					$ufCobranca = 'GO';
					break;
				case 'Maranhão':
					$ufCobranca = 'MA';
					break;
				case 'Mato Grosso':
					$ufCobranca = 'MT';
					break;
				case 'Mato Grosso do Sul':
					$ufCobranca = 'MS';
					break;
				case 'Minas Gerais':
					$ufCobranca = 'MG';
					break;
				case 'Pará':
					$ufCobranca = 'PA';
					break;
				case 'Paraíba':
					$ufCobranca = 'PB';
					break;
				case 'Paraná':
					$ufCobranca = 'PR';
					break;
				case 'Pernambuco':
					$ufCobranca = 'PE';
					break;
				case 'Piauí':
					$ufCobranca = 'PI';
					break;
				case 'Rio de Janeiro':
					$ufCobranca = 'RJ';
					break;
				case 'Rio Grande do Norte':
					$ufCobranca = 'RN';
					break;
				case 'Rio Grande do Sul':
					$ufCobranca = 'RS';
					break;
				case 'Rondônia':
					$ufCobranca = 'RO';
					break;
				case 'Roraima':
					$ufCobranca = 'RR';
					break;
				case 'Santa Catarina':
					$ufCobranca = 'SC';
					break;
				case 'São Paulo':
					$ufCobranca = 'SP';
					break;
				case 'Sergipe':
					$ufCobranca = 'SE';
					break;
				case 'Tocantins':
					$ufCobranca = 'TO';
					break;
			}

			switch ( $endereco_entrega->getRegion() )
			{
				case 'Acre':
					$ufEntrega = 'AC';
					break;
				case 'Alagoas':
					$ufEntrega = 'AL';
					break;
				case 'Amapá':
					$ufEntrega = 'AP';
					break;
				case 'Amazonas':
					$ufEntrega = 'AM';
					break;
				case 'Bahia':
					$ufEntrega = 'BA';
					break;
				case 'Ceará':
					$ufEntrega = 'CE';
					break;
				case 'Distrito Federal':
					$ufEntrega = 'DF';
					break;
				case 'Espírito Santo':
					$ufEntrega = 'ES';
					break;
				case 'Goiás':
					$ufEntrega = 'GO';
					break;
				case 'Maranhão':
					$ufEntrega = 'MA';
					break;
				case 'Mato Grosso':
					$ufEntrega = 'MT';
					break;
				case 'Mato Grosso do Sul':
					$ufEntrega = 'MS';
					break;
				case 'Minas Gerais':
					$ufEntrega = 'MG';
					break;
				case 'Pará':
					$ufEntrega = 'PA';
					break;
				case 'Paraíba':
					$ufEntrega = 'PB';
					break;
				case 'Paraná':
					$ufEntrega = 'PR';
					break;
				case 'Pernambuco':
					$ufEntrega = 'PE';
					break;
				case 'Piauí':
					$ufEntrega = 'PI';
					break;
				case 'Rio de Janeiro':
					$ufEntrega = 'RJ';
					break;
				case 'Rio Grande do Norte':
					$ufEntrega = 'RN';
					break;
				case 'Rio Grande do Sul':
					$ufEntrega = 'RS';
					break;
				case 'Rondônia':
					$ufEntrega = 'RO';
					break;
				case 'Roraima':
					$ufEntrega = 'RR';
					break;
				case 'Santa Catarina':
					$ufEntrega = 'SC';
					break;
				case 'São Paulo':
					$ufEntrega = 'SP';
					break;
				case 'Sergipe':
					$ufEntrega = 'SE';
					break;
				case 'Tocantins':
					$ufEntrega = 'TO';
					break;
			}
			/* Dados Cobrança */
	        $nomeCobranca			  = str_ireplace(' ', '+', $endereco_cobranca->getName());
	        $cepCobranca			  = str_ireplace('-', '', $endereco_cobranca->getPostcode());
	        $emailCobranca		      = str_ireplace(' ', '+', $_order->_data["customer_email"]);
	        $ruaCobranca			  = str_ireplace(' ', '+', $endereco_cobranca->getStreet(1));
	        $numeroEntrega            = $endereco_entrega->getNum();
			$bairroCobranca           = $endereco_entrega->getStreet(3);
	        $complementoEntrega       = "";
	        $cidadeCobranca		      = strtoupper(str_ireplace(' ', '+', str_replace($array_from, $array_to, $endereco_cobranca->getCity())));
	        //$ufCobranca				  = $endereco_cobranca->getRegion();
	        $dddCobranca			  = $dddEntrega;      // So para manter a ordem de variaveis
	        $telefoneCobranca		  = $telefoneEntrega; // So para manter a ordem de variaveis
			$complementoCobranca	  = $endereco_entrega->getStreet(3);


			/* Dados Entrega */
	        $nomeEntrega			  = str_ireplace(' ', '+', $endereco_entrega->getName());
	        $cepEntrega				  = str_ireplace('-', '', $endereco_entrega->getPostcode());
	        $emailEntrega		      = str_ireplace(' ', '+', $_order->_data["customer_email"]);
	        $ruaEntrega		     	  = str_ireplace(' ', '+', $endereco_entrega->getStreet(1));
	        $numeroEntrega            = $endereco_entrega->getStreet(2);
	        $bairroEntrega            = $endereco_entrega->getStreet(3);
	        $complementoEntrega       = "";
	        $cidadeEntrega		      = strtoupper(str_ireplace(' ', '+', str_replace($array_from, $array_to, $endereco_entrega->getCity())));
	        //$ufEntrega				  = $endereco_entrega->getRegion();
	        $dddEntrega			 	  = $dddEntrega;      // So para manter a ordem de variaveis
	        $telefoneEntrega		  = $telefoneEntrega; // So para manter a ordem de variaveis

	        $dddEntrega				  = $dddEntrega;      // So para manter a ordem de variaveis
	        $telefoneEntrega		  = $telefoneEntrega; // So para manter a ordem de variaveis
	        $codigoPedido			  = $_order->getIncrementId();
	        $quantidadeItensDistintos = $quantidadeItensDistintos; // So para manter a ordem de variaveis
	        $quantidadeTotalItens     = $quantidadeTotalItens;     // So para manter a ordem de variaveis
	        $valorTotalCompra		  = str_ireplace('.', '.', number_format($_order->getBaseGrandTotal(), 2, ',', '.'));
	        $dataCompra				  = str_ireplace(' ', '+', $_order->getCreatedAtDate());
	        $metodoPagamentos		  = $metodoPagamentos; // So para manter a ordem de variaveis
	        $numeroParcelasPagamentos = '1'; 		       // Repensar
	        $valorPagamentos		  = $valorTotalCompra;


	        $ambiente				  = Mage::getStoreConfig('modulos/clearsale/ambiente');
	        $codIntegracao            = Mage::getStoreConfig('modulos/clearsale/codigo_integracao');
			$nomeCobranca			  = str_ireplace(' ', '+', $endereco_cobranca->getName());
			$ruaCobranca			  = str_ireplace(' ', '+', $endereco_cobranca->getStreet(1));
			$numeroCobranca	 		  = $endereco_cobranca->getStreet(2);
            $PedidoID                 = $_order->getIncrementId();
            $TipoPagamento 			  = "1";
            $PagamentoParcela 		  = $additionaldata["cc_parcelas"];

	        if( $ambiente == 1 )
        	{
				//$urlClearsale = "http://www.clearsale.com.br/integracaov2/FreeClearSale/frame.aspx";
				$urlClearsale = "https://www.clearsale.com.br/start/Entrada/EnviarPedido.aspx";
			}else{
				$urlClearsale = "https://homolog.clearsale.com.br/start/Entrada/EnviarPedido.aspx";
			}


		    $i = 1;
        	$items = $_order->getAllVisibleItems();

            $url =    $urlClearsale;
	        $url .=   '?CodigoIntegracao='.$codIntegracao;
            $url .=   '&PedidoID='.$PedidoID;
            $url .=   '&Data='.$dataCompra;
        	$url .=   '&Total='.$valorTotalCompra;
        	$url .=   '&TipoPagamento='.$TipoPagamento;
        	$url .=   '&Parcelas='.$PagamentoParcela;
        	$url .=   '&TipoCartao='.$metodoPagamentos;
        	$url .=   '&Cobranca_Nome='.$nomeCobranca;
        	$url .=   '&Cobranca_Email='.$emailCobranca;
        	$url .=   '&Cobranca_Documento='.$cpfComprador;
        	$url .=   '&Cobranca_Logradouro='.$ruaCobranca;
        	$url .=   '&Cobranca_Logradouro_Numero='.$numeroCobranca;
        	$url .=   '&Cobranca_Logradouro_Complemento='.$complementoCobranca;
        	$url .=   '&Cobranca_Bairro='.$bairroCobranca;
        	$url .=   '&Cobranca_Cidade='.$cidadeCobranca;
        	$url .=   '&Cobranca_Estado='.$ufCobranca;
        	$url .=   '&Cobranca_CEP='.$cepCobranca;
        	$url .=   '&Cobranca_Pais='.$paisEntrega;
        	$url .=   '&Cobranca_DDD_Telefone='.$dddCobranca;
        	$url .=   '&Cobranca_Telefone='.$telefoneCobranca;
        	$url .=   '&Entrega_Nome='.$nomeEntrega;
        	$url .=   '&Entrega_Email='.$emailEntrega;
        	$url .=   '&Entrega_Documento='.$cpfComprador;
        	$url .=   '&Entrega_Logradouro='.$ruaEntrega;
        	$url .=   '&Entrega_Logradouro_Numero='.$numeroEntrega;
        	$url .=   '&Entrega_Logradouro_Complemento='.$complementoEntrega;
        	$url .=   '&Entrega_Bairro='.$bairroEntrega;
        	$url .=   '&Entrega_Cidade='.$cidadeEntrega;
        	$url .=   '&Entrega_Estado='.$ufEntrega;
        	$url .=   '&Entrega_CEP='.$cepEntrega;
        	$url .=   '&Entrega_Pais='.$paisEntrega;
        	$url .=   '&Entrega_DDD_Telefone='.$dddEntrega;
        	$url .=   '&Entrega_Telefone='.$telefoneEntrega;

            if ($items) {
                foreach ($items as $item) {
                    $item_price = 0;
                    $item_qty = $item->getQtyOrdered() * 1;

                    if ($children = $item->getChildrenItems()) {
                        foreach ($children as $child) {
                            $item_price += $child->getBasePrice() * $child->getQtyOrdered() / $item_qty;
                        }
                        $item_price = $this->formatNumber($item_price);
                    }
                    if (!$item_price) {
        				$item_price = $this->formatNumber($item->getBasePrice());
                    }
                    $url .= '&item_descr_'.$i.'='.substr(str_ireplace(' ', '+', $item->getName()), 0, 100);
					$url .= '&item_nome_'.$i.'='.substr(str_ireplace(' ', '+', $item->getName()), 0, 100);
                    $url .= '&item_id_'.$i.'='.substr($item->getSku(), 0, 100);
                    $url .= '&item_Qtd_'.$i.'='.$item_qty;
                    $url .= '&item_valor_'.$i.'='.($item_price/100);
                    $i++;
					//Limita o numero maximo de itens - URL muito grande
					if ($i == 7) { break; }
                }
            }


            $html_meio = '<iframe height="85" frameborder="0" width="280" scrolling="no" src="'. $url .'"></iframe>';


	        return $html_1.$html_meio.$html_2;
		}
		return $html;
    }


	function formatNumber ($number)
	{
		return sprintf('%.2f', (double) $number) * 100;
	}

}

