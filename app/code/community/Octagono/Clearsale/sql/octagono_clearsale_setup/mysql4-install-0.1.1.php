<?php
/**
 * Octagono Ecommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.octagonoecommerce.com.br/eula-licenca-usuario-final.html
 *
 *
 * @category   Clearsale
 * @package    Octagono_Clearsale
 * @copyright  Copyright (c) 2009-2011 - Octagono Ecommerce - www.octagonoecommerce.com.br
 * @license    http://www.octagonoecommerce.com.br/eula-licenca-usuario-final.html
 */
$write 			   = Mage::getSingleton('core/resource')->getConnection('core_write');
$sql_enable_taxvat = "UPDATE core_config_data set value = 'req' WHERE path = 'customer/address/taxvat_show'";	
$update            = $write->query($sql_enable_taxvat);

$installer = $this;

$installer->startSetup();

$installer->endSetup();
