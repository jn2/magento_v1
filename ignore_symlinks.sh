#!/bin/bash

ignore="
#não salva a pasta desenvolvimento\n
/desenvolvimento\n
app/etc/modules/z-disable-modules.xml\n
.modgit/*/source/\n
/index-test.php\n


/nbproject/*\n
/app/etc/local.xml\n
/var/*\n
!/var/.gitkeep\n
!/var/tmp/.gitkeep\n
/errors/local.xml\n
/media/*\n
!/media/.gitkeep\n
/downloader/.cache/\n
.sass-cache\n
.thumbs\n
_paleta.scss\n
.DS_Store\n
/vendor\n
.idea\n
\n
skin/frontend/jn2-package/default/src/node_modules\n
skin/frontend/jn2-package/default/node_modules\n
\n
cloudflare.txt\n
\n
#apenas dump.php é válido\n
dump/*\n
!dump/dump.php\n
\n
#impede a criação de arquivos gigantes no projeto\n
*.tar\n
*.tar.gz\n
*.tar.bz\n
*.sql.gz\n
*.sql.bz\n
*.sql.7z\n
*.7z\n
*.zip\n
*.rar\n
*.tgz\n
*.tbz\n

#permite sql instalacao\n
!instalacao/setups/before/*.sql\n
!instalacao/themes/*/*.sql\n
#remoção thumbs desnecessários instalação medias\n
/instalacao/setups/before/media/wysiwyg/.thumbs/\n
/instalacao/themes/*/media/wysiwyg/.thumbs/\n
instalacao/setups/before/media/catalog/product/cache/*\n
instalacao/themes/*/media/catalog/product/cache/\n
instalacao/setups/before/media/ibanners/cache/*\n
instalacao/themes/*/media/ibanners/cache/*\n
\n
\n
#remoção módulos node do theme mobile jn2\n
skin/frontend/jn2/mobile/node_modules\n
\n
#arquivos de musica e video copiado de uma lista de mime types\n
*.mp3\n
*.wav\n
*.wmv\n
*.aac\n
*.f4a\n
*.oga\n
*.ogg\n
*.opus\n
*.ra\n
\n
*.avi\n
*.wmv\n
*.asf\n
*.asx\n
*.mng\n
*.flv\n
*.webm\n
*.mov\n
*.mp4\n
*.ogv\n
*.3gpp\n
*.3gp;\n
*.m4v\n
*.f4v\n
*.f4p\n
*.mpeg\n
*.mpg\n
*/**/node_modules/\n
\n
.modman\n
"

echo $ignore > .gitignore
#find * -type l -not -exec grep -q "^{}$" .gitignore \; -print >> .gitignore
