<?php
include_once '../lib/Aws/aws-autoloader.php';
include_once './abstract.php';
use Aws\S3\S3Client;
use Aws\Exception\AwsException;

Class Jn2_S3_Shell_Clearcache extends Mage_Shell_Abstract {
    public function run()
    {
        $store = Mage::app()->getStore();
        $s3_enable = Mage::getStoreConfig('system/media_storage_configuration/media_storage', $store);

        if(!empty($s3_enable)) {
            $access_key = Mage::getStoreConfig('thai_s3/general/access_key', $store);
            $secret_key = Mage::getStoreConfig('thai_s3/general/secret_key', $store);
            $prefix = Mage::getStoreConfig('thai_s3/general/prefix', $store);

            if(!empty($access_key) && !empty($secret_key)) {
                if(!empty($prefix)) {
                    $dirname = explode('/', getcwd());

                    if(count($dirname) >= 4) {
                        if($dirname[1] == 'mnt' && $dirname[2] == 'files') {
                            $prefix = $dirname[3];
                        }
                    }
                    else {
                        error_log('prefix não encontrado');
                        return false;
                    }
                }

                $bucketName = 'jn2-media';
                $key = 'catalog/product/cache';
                $credentials = new Aws\Credentials\Credentials($access_key, $secret_key);

                $client = new S3Client([
                    'region' => 'us-east-1',
                    'version' => 'latest',
                    'credentials' => $credentials
                ]);

                try {
                    $objects = $client->listObjectsV2([
                        'Bucket' => $bucketName,
                        'Prefix' => $prefix . '/' . $key
                    ]);

                    if(!empty($objects) && !empty($objects['Contents'])) {
                        $client->deleteObjects([
                            'Bucket' => $bucketName,
                            'Delete' => [
                                'Objects' => $objects['Contents']
                            ]
                        ]);
                    }
                } catch (AwsException $e) {
                    // output error message if fails
                    error_log($e->getMessage());
                }
            }
        }
    }
}

$shell = new Jn2_S3_Shell_Clearcache();
$shell->run();

