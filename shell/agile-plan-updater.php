<?php

define('MAGENTO_ROOT', dirname(__DIR__));

$mageFilename = MAGENTO_ROOT . '/app/Mage.php';

require MAGENTO_ROOT . '/app/bootstrap.php';
require_once $mageFilename;
if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    Mage::setIsDeveloperMode(true);
}
umask(0);



Mage::app('admin')->init();
$agile = Mage::getSingleton('maxloja/agile');
if (!$agile) {
    throw "Não foi possível encontrar maxloja/agile";
}
$processor = $agile->getProcessor();
if(!$processor){    
    throw "Não foi possível encontrar maxloja/agile_processor not found";
}
$processor->process();

echo "PROCESSADO COM SUCESSO!";
