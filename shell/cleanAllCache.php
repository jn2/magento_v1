<?php
date_default_timezone_set("America/Sao_Paulo");

echo "\n";
echo "Início limpeza dos caches às " . date("Y-m-d H:i:s") . "\n";
echo "\n";

ini_set("display_errors", 1);

require(__DIR__.'/../app/Mage.php');
Mage::app('admin')->setUseSessionInUrl(false);
Mage::getConfig()->init();

$types = Mage::app()->getCacheInstance()->getTypes();

try {
    echo "Limpando Data Cache...\n";
    flush();
    foreach ($types as $type => $data) {
        $clear = Mage::app()->getCacheInstance()->clean($data["tags"]) ? "[OK]" : "[ERROR]";
        echo "Removendo $type => ".$clear."\n";
    }
} catch (exception $e) {
    die("[ERROR:" . $e->getMessage() . "]");
}

try {
    $clear = "Limpando Stored Cache => ";
    flush();
    $clear.= Mage::app()->getCacheInstance()->clean() ? "[OK]" : "[ERROR]";
    echo $clear."\n";
    
} catch (exception $e) {
    die("[ERROR:" . $e->getMessage() . "]");
}

/*
try {
    echo "Limpando merged JS/CSS...";
    flush();
    Mage::getModel('core/design_package')->cleanMergedJsCss();
    Mage::dispatchEvent('clean_media_cache_after');
    echo "[OK]\n";
} catch (Exception $e) {
    die("[ERROR:" . $e->getMessage() . "]");
}

try {
    echo "Limpando image cache... ";
    flush();
    echo Mage::getModel('catalog/product_image')->clearCache();
    echo "[OK]\n";
} catch (exception $e) {
    die("[ERROR:" . $e->getMessage() . "]");
}*/
echo "\n";
