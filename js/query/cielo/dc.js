	function selectCardTypeDc(element)
	{
		// remove as bandeiras previamente selecionadas
		
		$$('img.card-type-image.selected').forEach(function(img)
		{
			img.removeClassName('selected');
		});
		
		// adiciona a bandeira atual como selecionada
		$(element).addClassName('selected');
		$('query-cielo-dc-type').value = $(element).getAttribute('ccType');
		
		// mostra formulario
		$$('li.card-data-form').forEach(function(li)
		{
			li.setStyle({"display": "block"});
		});
	}

	function denyNotNumberDc(field, event)
	{
        var keyCode = ('which' in event) ? event.which : event.keyCode;
		
		// teclas backspace e delete
        if(keyCode == 8 || keyCode == 46)
			return true;
		
		// tecla tab
        if(keyCode == 9)
			return true;
		
		// teclas <- e ->
        if(keyCode == 37 || keyCode == 39)
			return true;
		
		// teclas home e end
        if(keyCode == 36 || keyCode == 35)
			return true;
		
		// teclas numericas
        if((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105))
			return true;
        
		return false;
	}

	function showCardDataDc(show)
	{
		if(show)
		{
			$('card-data-dc').show();
		}
		else
		{
			$('card-data-dc').hide();
		}
	}
	
	function cleanDcForm(cardType)
	{

		var currentCcType = document.getElementById('cielo-current-dc-type').value;
		document.getElementById('cielo-current-dc-type').value = cardType;
		
		if(currentCcType != cardType)
		{
			document.getElementById('cielo-dc-card-number').value = "";
			document.getElementById('cielo-dc-security-code').value = "";
			document.getElementById('cielo-dc-card_expiration-mh').value = "";
			document.getElementById('cielo-dc-card_expiration-yr').value = "";
			document.getElementById('cielo-dc-card_owner').value = "";
		}
	}
	
	function queryCieloDcMaskCPF(event)
	{	
		var field = event.currentTarget;
		
		field.maxLength=14;
		
		if(queryCieloDcSpecialKeys(event))
		{
			return true;
		}
		
		if(!queryCieloDcNumberKeys(event))
		{
			Event.stop(event);
			return false;
		}
		
		if(event.which == 8)
			return;
		
		if(field.value.length == 3 || field.value.length == 7)
		{
			field.value += ".";
		}
		else if(field.value.length == 11)
		{
			field.value += "-";
		}
	}

	function queryCieloDcNumberKeys(event)
	{	
		var keyCode = ('which' in event) ? event.which : event.keyCode;
		
		// teclas numericas
		if((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105))
			return true;
			
		return false;
	}

	function queryCieloDcSpecialKeys(event)
	{
		var keyCode = ('which' in event) ? event.which : event.keyCode;
		
		// teclas backspace e delete
		if(keyCode == 8 || keyCode == 46)
			return true;
		
		// tecla tab
		if(keyCode == 9)
			return true;
		
		// teclas <- e ->
		if(keyCode == 37 || keyCode == 39)
			return true;
		
		// teclas home e end
		if(keyCode == 36 || keyCode == 35)
			return true;
			
		return false;
	}