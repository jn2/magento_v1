window.onload = function() {
    if(typeof tinyMCE !== 'undefined') {
        tinyMCE.onAddEditor.add(function(editorManager, editor) {
            oldSetContent = editor.setContent;
    
            editor.setContent = function(content, options) {
                if(content.indexOf('iframe')) 
                    options.format = 'raw';
                
                return oldSetContent.call(this, content, options);
            };
        });
    }
};