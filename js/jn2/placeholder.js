(function($){
    $.fn.placeholder = function(text,options){
        options = options || {}
        var wrap = options['wrap'] ? options['wrap'] : true;
        //var _self = this        
        this.each(function(){
            var inputs = null;
            if(!$(this).is('select,input,textarea')){
                inputs = $(this).find('input[type="text"],input[type="password"],select,textarea');
            }else{
                inputs = $(this)
            }
            inputs.each(function(){//todos inputs 1 por 1
                if(wrap){
                    $(this).parents('span.placeholder').replaceWith(this);
                    var _placeholder = $(this).wrap('<span class="placeholder"/>').parents('span.placeholder')
                    var _holder = _placeholder.append('<span class="holder" unselectable="on"/>').children('span.holder');
                    text = text ? text : function(){                        
                        return $(this).data('placeholder')
                    }
                    
                    //_placeholder.css('width',$(this).width()+'px');
                    
                    if(text instanceof Function){                                                                
                        _holder.html(text.call(this));
                    }else if( typeof(text) == 'string') {
                        _holder.html(text)
                    }else{
                        $.each(text,function(k,v){
                            _placeholder.find(k).next('span.holder').html(v)
                        })
                    }    
                }           
            })
        })
        if($.fn.placeholder._inited == undefined){//attach event somente 1 vez
            $.fn.placeholder._inited  = true
            var selector = 'span.placeholder > span.holder'
            var events = 'click'
            $(document.body).undelegate(selector,events).delegate(selector,events,function(evt){                            
                $(this).parents('span.placeholder').find('input,select,textarea').focus();
            });
            selector = 'span.placeholder > input,select,textarea';
            events = 'keydown mouseup';
            $(document.body).undelegate(selector,events).delegate(selector,events,function(){
                var _self = this;
                setTimeout(function(){
                    if($(_self).val().length > 0){
                        $(_self).parents('span.placeholder').eq(0).addClass('hasSome')
                    }else{
                        $(_self).parents('span.placeholder').eq(0).removeClass('hasSome')
                    }
                },50)
            
            })
            $(selector).trigger('keydown');
        }
        

    }
    
    /////////////////////////////////////////
    //placeholder
    /////////////////////////////////////////
    $(function(){
        setTimeout(function(){
          $('input[placeholder]').each(function(){                      
              $(this).placeholder($(this).attr('placeholder'));
              $(this).removeAttr('placeholder');
          }); 
          },100)
    });
    
})($jqueryJn2Basic);
