/*
 * Ativar o noconflic para o Jquery da JN2
 */
$jqueryJn2Basic = $j = jQuery.noConflict(false);
//Rotinas padrões
(function ($) {
    try {
        $(document).ready(function () {
            /*
             * Removendo o campos de país do checkout
             */
            
            setTimeout(function () {
                $(".checkout-cart-index #country").val('BR');
                $(".checkout-cart-index #country").css({
                    "display": "none"
                });
                $(".checkout-cart-index #region_id").val('523');
                $(".checkout-cart-index #region_id").css({
                    "display": "none"
                });
                $(".checkout-cart-index label[for='country']").css({
                    "display": "none"
                });
                $(".checkout-cart-index label[for='region_id']").css({
                    "display": "none"
                });
                /*
                 $(".checkout-cart-index #co-shipping-method-form .buttons-set").css({
                 "display": "none"
                 });
                 $(".checkout-cart-index #co-shipping-method-form input:radio").css({
                 "display": "none"
                 });
                 */
            }, 100);

            /* Retirar obrigatóriedade dos campos telefone e fax no onestepcheckout Davison Arthur*/
            (function () {
                var $wrapper = $('.onepagecheckout_block');
                var $inputs = $wrapper.find('input[name$="[telephone]"],input[name$="[celular]"]');
                $inputs.removeClass('required-entry');
                $inputs.parents('.short').find('em,sup').remove();

                var $inputRg = $('input[name$="[rg]"],input[name="rg"]');
                $inputRg.removeClass('validate-number');
            })();


        });


    } catch (e) {
        if (console) {
            console.log(e)
        }
    }

})($jqueryJn2Basic);