
jQuery( document ).ready(function() {

    if (window.jn2 === undefined) {
        window.jn2 = {};
    }

    jn2.addItems = function(url,button){
        sessionStorage.removeItem('jn2_cache');
        var valueInput = jQuery(button).siblings('.qty-wapper').find('input').val();
        setLocation(url+'qty/'+valueInput);
        jQuery(button).css("color","transparent");
        jQuery(button).addClass("loading-cache");
        jQuery(button).attr('disabled', 'disabled');
        setTimeout(function() {
            jQuery(button).prop("disabled", false);
        }, 1200);
    }

    jQuery(document).on("click", ".btn-minus", function() {
        if(jQuery(this).parent().find("input").val() > 1){
            jQuery(this).parent().find("input").val(parseInt(jQuery(this).parent().find("input").val())-1)
        }
    })
    jQuery(document).on("click", ".btn-plus", function() {
        jQuery(this).parent().find("input").val(parseInt(jQuery(this).parent().find("input").val())+1)
    });
});