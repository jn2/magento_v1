FROM 391244939540.dkr.ecr.us-east-1.amazonaws.com/jn2_magento_v1:latest

# Add application
COPY . /var/www/html/

# Diretotios que serao mapeados
RUN mkdir var/log && mkdir var/import && mkdir var/export && mkdir var/report

# Permissao var
RUN chmod -R 777 /var/www/html/var

